package be.lions.duckrace.startup.custom;

import be.lions.duckrace.util.StringUtils;


public enum LiferayArticle {

	HOME,
	
	UNDER_CONSTRUCTION,
	
	ORG_LIONS,
	ORG_HART_VOOR_LIMBURG,
	
	INFO_EVENEMENT,
	INFO_REGLEMENT,
	INFO_RACEVERLOOP,
	INFO_HOOFDPRIJS,
	INFO_OPBRENGST,
	
	LOGOS(LiferayStructure.LOGOS),
	DUCKTYPE_MAIN_SPONSORS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_100_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_50_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_20_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_10_DUCKS(LiferayStructure.DUCKTYPE),
	
	FOTO_ALBUM(LiferayStructure.GALLERY),
	PERSCONFERENTIES,
	REACTIES,
	
	EVENT_PROGRAMMA,
	EVENT_ACTS,
	EVENT_BEVERAGE,
	EVENT_BIJSTAND;
	
	private LiferayStructure structure;
	
	private LiferayArticle() {
		this(null);
	}
	
	private LiferayArticle(LiferayStructure structure) {
		this.structure = structure;
	}
	
	public String getStructureId() {
		return structure == null ? null : structure.name();
	}
	
	public boolean hasStructure() {
		return !StringUtils.isEmpty(getStructureId());
	}
	
	public String getId() {
		return name();
	}
	
}
