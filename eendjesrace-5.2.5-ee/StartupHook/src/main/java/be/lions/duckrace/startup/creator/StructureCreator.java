package be.lions.duckrace.startup.creator;

import be.lions.duckrace.startup.util.StartupUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateStructureIdException;
import com.liferay.portlet.journal.model.JournalStructure;
import com.liferay.portlet.journal.service.JournalStructureLocalServiceUtil;

public class StructureCreator extends ResourceCreator {

	private static final String STRUCTURE_PATH = "/wcm/structures/";

	@Override
	void doInsert(String filename, String fileContent) throws PortalException, SystemException {
		String structureId = filename;
		boolean autoStructureId = false;
		String parentStructureId = null;
		String name = filename;
		String description = filename;

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddCommunityPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		try {
			JournalStructureLocalServiceUtil.addStructure(StartupUtil.getAdminId(), StartupUtil.getGroupId(), structureId,
				autoStructureId, parentStructureId, name, description, fileContent, serviceContext);
			log.info("Structure '" + structureId + "' created");
		} catch (DuplicateStructureIdException e) {
			log.warn("Structure '" + structureId + "' already existed, updating XML...");
			JournalStructure structure = JournalStructureLocalServiceUtil.getStructure(StartupUtil.getGroupId(), structureId);
			structure.setXsd(fileContent);
			JournalStructureLocalServiceUtil.updateJournalStructure(structure);
			log.info("Structure '" + structureId + "' XML updated");
		}
	}

	@Override
	String getResourceFolder() {
		return STRUCTURE_PATH;
	}

	private static Log log = LogFactoryUtil.getLog(StructureCreator.class);

}