package be.lions.duckrace.startup.type;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

public class LiferayCategorySet extends LiferayVocabulary {

	protected LiferayCategorySet(LiferayGroup group, String name) throws PortalException, SystemException {
		super(group, name);
	}

	@Override
	protected String getName() {
		return "categoryset";
	}

	@Override
	protected boolean isFolksonomy() {
		return false;
	}

}
