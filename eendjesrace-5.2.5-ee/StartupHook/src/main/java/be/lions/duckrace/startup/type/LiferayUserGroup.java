package be.lions.duckrace.startup.type;

import com.liferay.portal.DuplicateUserGroupException;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;

public class LiferayUserGroup extends LiferayGroup {

	LiferayUserGroup(LiferayCompany company, String name) throws PortalException, SystemException {
		super(company, name);
	}

	@Override
	public long getGroupId() throws PortalException, SystemException {
		return UserGroupLocalServiceUtil.getUserGroup(getCompanyId(), getName()).getGroup().getGroupId();
	}

	@Override
	protected void persist() throws PortalException, SystemException {
		try {
			UserGroupLocalServiceUtil.addUserGroup(getDefaultUserId(), getCompanyId(), getName(), getName());
			log.info("User group '"+getName()+"' successfully created.");
		} catch(DuplicateUserGroupException e) {
			log.warn("User group '"+getName()+"' already existed.");
		}

	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayUserGroup.class);

}
