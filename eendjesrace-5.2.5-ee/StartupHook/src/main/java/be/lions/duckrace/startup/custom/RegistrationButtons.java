package be.lions.duckrace.startup.custom;

import be.lions.duckrace.startup.type.LiferayPortlet;

public class RegistrationButtons extends LiferayPortlet {

	public RegistrationButtons() {
		super("ReservationButtonsPortlet_WAR_registrationportlet");
	}

}
