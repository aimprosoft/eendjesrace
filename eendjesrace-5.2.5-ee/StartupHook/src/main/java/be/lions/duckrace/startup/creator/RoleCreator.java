package be.lions.duckrace.startup.creator;

import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayRole;
import be.lions.duckrace.startup.type.LiferayRole.RoleScope;
import be.lions.duckrace.types.DuckRaceRole;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.imagegallery.model.IGFolder;
import com.liferay.portlet.imagegallery.model.IGImage;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.tags.model.TagsEntry;
import com.liferay.portlet.tags.model.TagsVocabulary;

import static com.liferay.portal.security.permission.ActionKeys.*;

import static com.liferay.portal.util.PortletKeys.*;

public class RoleCreator {

	private LiferayCompany company;
	
	public RoleCreator(LiferayCompany company) {
		this.company = company;
	}

	public void insertRoles() throws PortalException, SystemException {
		editGuestRole();
		createLionsMemberRole();
		createWebmasterRole();
	}

	private void editGuestRole() throws PortalException, SystemException {
		LiferayRole guest = company.getGuestRole();
		// Allow comments
		guest.setGlobalPermissions(JournalArticle.class, VIEW, ADD_DISCUSSION);
		guest.setGlobalPermissions(BlogsEntry.class, VIEW, ADD_DISCUSSION);
	}

	private void createWebmasterRole() throws PortalException, SystemException {
		LiferayRole webmaster = company.addRole(DuckRaceRole.WEBMASTER.getName(), RoleScope.COMPANY);
		// Control panel
		webmaster.addControlPanelAccess(JOURNAL, IMAGE_GALLERY, DOCUMENT_LIBRARY, BLOGS);
		// Web content
		webmaster.setGlobalPermissions("com.liferay.portlet.journal", ADD_ARTICLE, APPROVE_ARTICLE);
		webmaster.setGlobalPermissions(JournalArticle.class, VIEW, DELETE, EXPIRE, UPDATE, DELETE_DISCUSSION, UPDATE_DISCUSSION);
		// Image gallery
		webmaster.setGlobalPermissions("com.liferay.portlet.imagegallery", ADD_FOLDER);
		webmaster.setGlobalPermissions(IGFolder.class, UPDATE, DELETE, ADD_SUBFOLDER, ADD_IMAGE, VIEW);
		webmaster.setGlobalPermissions(IGImage.class, UPDATE, DELETE, VIEW);
		// Document library
		webmaster.setGlobalPermissions("com.liferay.portlet.documentlibrary", ADD_FOLDER);
		webmaster.setGlobalPermissions(DLFolder.class, UPDATE, DELETE, ADD_SUBFOLDER, ADD_DOCUMENT, VIEW);
		webmaster.setGlobalPermissions(DLFileEntry.class, UPDATE, DELETE, VIEW);
		// Blogs
		webmaster.setGlobalPermissions("com.liferay.portlet.blogs", ADD_ENTRY);
		webmaster.setGlobalPermissions(BlogsEntry.class, DELETE, DELETE_DISCUSSION, UPDATE, UPDATE_DISCUSSION);
		// Tags and categories
		webmaster.setGlobalPermissions(TagsEntry.class, VIEW);
		webmaster.setGlobalPermissions(TagsVocabulary.class, VIEW);
	}

	private void createLionsMemberRole() throws PortalException, SystemException {
		company.addRole(DuckRaceRole.LIONS_MEMBER.getName(), RoleScope.COMPANY);
	}
	
}
