package be.lions.duckrace.startup.type.role;

import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayRole;
import be.lions.duckrace.types.DuckRaceRole;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.imagegallery.model.IGFolder;
import com.liferay.portlet.imagegallery.model.IGImage;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.tags.model.TagsEntry;
import com.liferay.portlet.tags.model.TagsVocabulary;
import static com.liferay.portal.security.permission.ActionKeys.*;

public class WebmasterRole extends LiferayRole {

	public WebmasterRole() throws PortalException, SystemException {
		super(LiferayCompany.getFirst(), DuckRaceRole.WEBMASTER.getName(), RoleScope.COMPANY);
		addWebContentPermissions();
		addImageGalleryPermissions();
		addDocumentLibraryPermissions();
		addTagPermissions();
		addBlogsPermissions();
	}
	
	private void addWebContentPermissions() throws SystemException, PortalException {
		addControlPanelAccess(PortletKeys.JOURNAL);
		setGlobalPermissions("com.liferay.portlet.journal", ADD_ARTICLE, APPROVE_ARTICLE);
		setGlobalPermissions(JournalArticle.class.getName(), VIEW, DELETE, EXPIRE, UPDATE);
		setGlobalPermissions(JournalArticle.class.getName(), DELETE_DISCUSSION, UPDATE_DISCUSSION);
	}
	
	private void addImageGalleryPermissions() throws PortalException, SystemException {
		addControlPanelAccess(PortletKeys.IMAGE_GALLERY);
		setGlobalPermissions("com.liferay.portlet.imagegallery", ADD_FOLDER);
		setGlobalPermissions(IGFolder.class.getName(), UPDATE, DELETE, ADD_SUBFOLDER, ADD_IMAGE, VIEW);
		setGlobalPermissions(IGImage.class.getName(), UPDATE, DELETE, VIEW);
	}
	
	private void addDocumentLibraryPermissions() throws PortalException, SystemException {
		addControlPanelAccess(PortletKeys.DOCUMENT_LIBRARY);
		setGlobalPermissions("com.liferay.portlet.documentlibrary", ADD_FOLDER);
		setGlobalPermissions(DLFolder.class.getName(), UPDATE, DELETE, ADD_SUBFOLDER, ADD_DOCUMENT, VIEW);
		setGlobalPermissions(DLFileEntry.class.getName(), UPDATE, DELETE, VIEW);
	}
	
	private void addBlogsPermissions() throws SystemException, PortalException {
		addControlPanelAccess(PortletKeys.BLOGS);
		setGlobalPermissions("com.liferay.portlet.blogs", ADD_ENTRY);
		setGlobalPermissions(BlogsEntry.class.getName(), DELETE, DELETE_DISCUSSION, UPDATE, UPDATE_DISCUSSION);
	}
	
	private void addTagPermissions() throws PortalException, SystemException {
		setGlobalPermissions(TagsEntry.class.getName(), VIEW);
		setGlobalPermissions(TagsVocabulary.class.getName(), VIEW);
	}
	
}
