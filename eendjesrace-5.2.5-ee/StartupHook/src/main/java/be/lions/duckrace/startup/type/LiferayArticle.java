package be.lions.duckrace.startup.type;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateArticleIdException;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.util.PwdGenerator;

public class LiferayArticle {

	private LiferayTemplate template;
	private String id;
	
	LiferayArticle(LiferayTemplate template, String id, String title, String description, Map<String, String> values, boolean override) throws PortalException, SystemException {
		this.template = template;
		this.id = id;
		persist(title, description, values, override);
	}

	private void persist(String title, String description, Map<String, String> values, boolean override) throws PortalException, SystemException {
		boolean autoArticleId = false;
		
		long userId = getStructure().getGroup().getDefaultUserId();
		long groupId = getStructure().getGroup().getGroupId();
		String type = "general";
		String structureId = getStructure().getId();
		String templateId = template.getId();
		String content = generateXml(values);
		int displayDateMonth = Calendar.JANUARY;
		int displayDateDay = 1;
		int displayDateYear = 1900;
		int displayDateHour = 12;
		int displayDateMinute = 0;
		int expirationDateMonth = Calendar.JANUARY;
		int expirationDateDay = 1;
		int expirationDateYear = 2200;
		int expirationDateHour = 12;
		int expirationDateMinute = 0;
		boolean neverExpire = true;
		int reviewDateMonth = Calendar.JANUARY;
		int reviewDateDay = 1;
		int reviewDateYear = 2200;
		int reviewDateHour = 12;
		int reviewDateMinute = 0;
		boolean neverReview = true;
		boolean indexable = true;
		ServiceContext sc = new ServiceContext();
		try {
			JournalArticle article = JournalArticleLocalServiceUtil.addArticle(userId, groupId, id, autoArticleId, title, description, content, type,
					structureId, templateId, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay,
					expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute,
					neverReview, indexable, false, "", null, null, null, sc);
			JournalArticleLocalServiceUtil.approveArticle(userId, groupId, id, article.getVersion(), null, sc);
			log.info("Article '" + title + "' successfully created.");
		} catch (DuplicateArticleIdException e) {
			log.info("Article '" + title + "' already existed.");
			if(override) {
				JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId, id);
				JournalArticleLocalServiceUtil.updateArticle(userId, groupId, id, article.getVersion(), false, content);
				JournalArticleLocalServiceUtil.approveArticle(userId, groupId, id, article.getVersion(), null, sc);
				log.info("Article '"+title+"' successfully overwritten.");
			}
		}
	}

	private LiferayStructure getStructure() {
		return template.getStructure();
	}
	
	private String generateXml(Map<String, String> values) {
		List<LiferayStructureField> fields = getStructure().getFields();
		StringBuffer result = new StringBuffer();
		result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n");
		for (LiferayStructureField field: fields) {
			String value = values.get(field.getName());
			result.append("\t<dynamic-element instance-id=\"");
			result.append(generateInstanceId());
			result.append("\" name=\"");
			result.append(field.getName());
			result.append("\" type=\"");
			result.append(field.getTypeId());
			result.append("\n\t\t<dynamic-content><![CDATA[");
			if(value != null) {
				result.append(value);
			}
			result.append("]]></dynamic-content>\n\t</dynamic-element>");
		}
		return result.toString();
	}
	
	private String generateInstanceId() {
		StringBuffer result = new StringBuffer();
		String pool = PwdGenerator.KEY1 + PwdGenerator.KEY2 + PwdGenerator.KEY3;
		for(int i=0; i<8; i++) {
			int pos = (int) Math.floor(Math.random() * pool.length());
			result.append(pool.charAt(pos));
		}
		return result.toString();
	}

	private JournalArticle getJournalArticle() throws PortalException, SystemException {
		return JournalArticleLocalServiceUtil.getArticle(getGroupId(), id);
	}
	
	private long getGroupId() throws PortalException, SystemException {
		return getStructure().getGroup().getGroupId();
	}
	
	public void setContent(String content) throws PortalException, SystemException {
		JournalArticleLocalServiceUtil.updateContent(getGroupId(), id, getJournalArticle().getVersion(), content);
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayArticle.class);

}
