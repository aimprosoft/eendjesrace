package be.lions.duckrace.startup.type;

import java.util.List;

import be.lions.duckrace.startup.type.LiferayTemplate.Language;
import be.lions.duckrace.startup.type.LiferayStructureField.FieldType;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateStructureIdException;
import com.liferay.portlet.journal.service.JournalStructureLocalServiceUtil;
import com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil;

/**
 * TODO:
 * - Nested fields
 * - Structure inheritance
 */
public class LiferayStructure {

	private static final Language DEFAULT_SCRIPT_LANGUAGE = Language.VELOCITY;
	
	private LiferayGroup group;
	private String id;
	private LiferayStructureFieldList fieldList;
	
	LiferayStructure(LiferayGroup group, String id, LiferayStructureFieldList fieldList) throws SystemException, PortalException {
		this.group = group;
		this.id = id;
		if(fieldList == null || fieldList.isEmpty()) {
			this.fieldList = new LiferayStructureFieldList();
			this.fieldList.addField("dummy", FieldType.TEXTFIELD);
		} else {
			this.fieldList = fieldList;
		}
		persist();
	}
	
	public LiferayTemplate addTemplate(Language lang, String script, boolean cached) throws PortalException, SystemException {
		int nbTemplates = JournalTemplateLocalServiceUtil.getStructureTemplates(group.getGroupId(), id).size();
		String templateId = nbTemplates == 0 ? id : id+"_"+nbTemplates;
		return new LiferayTemplate(this, templateId, lang, script, cached);
	}
	
	public LiferayTemplate addTemplate(String script, boolean cached) throws PortalException, SystemException {
		return addTemplate(DEFAULT_SCRIPT_LANGUAGE, script, cached);
	}
	
	LiferayGroup getGroup() {
		return group;
	}
	
	String getId() {
		return id;
	}
	
	public List<LiferayStructureField> getFields() {
		return fieldList.getFields();
	}

	private void persist() throws SystemException, PortalException {
		long userId = group.getDefaultUserId();
		long groupId = group.getGroupId();
		boolean autoId = false;
		String parent = null;
		String xml = fieldList.getXml();
		ServiceContext sc = new ServiceContext();
		sc.setAddCommunityPermissions(true);
		sc.setAddGuestPermissions(true);

		try {
			JournalStructureLocalServiceUtil.addStructure(userId, groupId, id, autoId, parent, id, id, xml, sc);
			log.info("Structure '" + id + "' created.");
		} catch (DuplicateStructureIdException e) {
			log.warn("Structure '" + id + "' already existed, updating...");
			JournalStructureLocalServiceUtil.updateStructure(groupId, id, parent, id, id, xml, sc);
			log.info("Structure '" + id + "' updated.");
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayStructure.class);
	
}
