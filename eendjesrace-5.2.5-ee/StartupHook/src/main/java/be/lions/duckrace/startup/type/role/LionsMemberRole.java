package be.lions.duckrace.startup.type.role;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayRole;
import be.lions.duckrace.types.DuckRaceRole;

public class LionsMemberRole extends LiferayRole {

	public LionsMemberRole() throws PortalException, SystemException {
		super(LiferayCompany.getFirst(), DuckRaceRole.LIONS_MEMBER.getName(), RoleScope.COMPANY);
	}
	
}
