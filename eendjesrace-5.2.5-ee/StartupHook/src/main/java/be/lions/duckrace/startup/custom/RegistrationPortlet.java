package be.lions.duckrace.startup.custom;

import be.lions.duckrace.startup.type.LiferayPortlet;

public class RegistrationPortlet extends LiferayPortlet {

	public RegistrationPortlet() {
		super("RegistrationPortlet_WAR_registrationportlet");
	}

}
