package be.lions.duckrace.startup.type;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.tags.DuplicateEntryException;
import com.liferay.portlet.tags.DuplicateVocabularyException;
import com.liferay.portlet.tags.service.TagsEntryLocalServiceUtil;
import com.liferay.portlet.tags.service.TagsVocabularyLocalServiceUtil;

public abstract class LiferayVocabulary {

	private String setName;
	private LiferayGroup group;
	
	protected LiferayVocabulary(LiferayGroup group, String name) throws PortalException, SystemException {
		this.group = group;
		this.setName = name;
		persist();
	}
	
	private void persist() throws PortalException, SystemException {
		try {
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(group.getGroupId());
			TagsVocabularyLocalServiceUtil.addVocabulary(group.getDefaultUserId(), setName, isFolksonomy(), sc);
			log.info(getName()+" '"+setName+"' was created.");
		} catch(DuplicateVocabularyException e) {
			log.warn(getName()+" '"+setName+"' already existed.");
		}
	}
	
	public void addEntry(String entryName) throws PortalException, SystemException {
		try {
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(group.getGroupId());
			TagsEntryLocalServiceUtil.addEntry(group.getDefaultUserId(), null, entryName, setName, new String[0], sc);
			log.info("Entry '"+entryName+"' was created inside vocabulary '"+setName+"'.");
		} catch(DuplicateEntryException e) {
			log.warn("Entry '"+entryName+"' already existed in vocabulary '"+setName+"'.");
		}
	}
	
	protected abstract boolean isFolksonomy();
	protected abstract String getName();
	
	private static Log log = LogFactoryUtil.getLog(LiferayVocabulary.class);
	
}
