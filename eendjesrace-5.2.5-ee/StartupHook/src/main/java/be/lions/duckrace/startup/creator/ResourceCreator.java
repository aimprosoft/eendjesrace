package be.lions.duckrace.startup.creator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.util.FileUtil;

public abstract class ResourceCreator {

	abstract String getResourceFolder();

	abstract void doInsert(String filename, String fileContent) throws PortalException, SystemException;

	public void insert() throws URISyntaxException, IOException, PortalException, SystemException {
		String[] fileNames = findFilenames();
		for (String fileName: fileNames) {
			doInsert(getResourceId(fileName), getContent(fileName));
		}
	}

	private String getResourceId(String fileName) {
		return fileName.substring(0, fileName.indexOf(".")).toUpperCase();
	}

	private String[] findFilenames() throws URISyntaxException {
		URL pathToFolder = getClass().getClassLoader().getResource(getResourceFolder());
		return new File(pathToFolder.toURI()).list();
	}

	private String getContent(String fileName) throws IOException {
		String path = getResourceFolder() + fileName;
		return new String(FileUtil.getBytes(getClass().getClassLoader().getResourceAsStream(path)));
	}

}
