package be.lions.duckrace.startup.type;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import com.liferay.portal.LayoutFriendlyURLException;
import com.liferay.portal.NoSuchResourceException;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutConstants;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.model.Resource;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.PermissionLocalServiceUtil;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;

public class LiferayPage {

	private long parentPageId; 
	private String resourceKey;
	private String friendlyUrl;
	private String layoutTpl;
	private long groupId;
	private boolean hidden;
	private String themeId;
	private String colorSchemeId;
	
	private static final String DEFAULT_LAYOUT_TPL = "1_column";
	private static final int DEFAULT_COLUMN = 1;
	
	LiferayPage(long groupId, String resourceKey, String friendlyUrl, String layoutTpl) throws PortalException, SystemException {
		this(groupId, null, resourceKey, friendlyUrl, layoutTpl, false, null, null);
	}
	
	LiferayPage(long groupId, String resourceKey, String friendlyUrl, boolean hidden) throws PortalException, SystemException {
		this(groupId, null, resourceKey, friendlyUrl, DEFAULT_LAYOUT_TPL, hidden, null, null);
	}
	
	public LiferayPage addChildPage(String resourceKey, String friendlyUrl, String layoutTpl, String themeId, String colorSchemeId) throws PortalException, SystemException {
		return new LiferayPage(groupId, this, resourceKey, friendlyUrl, layoutTpl, false, themeId, colorSchemeId);
	}
	
	public LiferayPage addChildPage(String resourceKey, String friendlyUrl) throws PortalException, SystemException {
		return addChildPage(resourceKey, friendlyUrl, DEFAULT_LAYOUT_TPL, null, null);
	}
	
	public LiferayPage addChildPage(String resourceKey, String friendlyUrl, String themeId, String colorSchemeId) throws PortalException, SystemException {
		return addChildPage(resourceKey, friendlyUrl, DEFAULT_LAYOUT_TPL, themeId, colorSchemeId);
	}
	
	public void linkToPage(LiferayPage page) throws PortalException, SystemException {
		Layout layout = getLayout();
		layout.setType("link_to_layout");
		layout.setTypeSettings("linkToLayoutId="+page.getLayoutId());
		LayoutLocalServiceUtil.updateLayout(layout, false);
		log.info("Page '"+getFriendlyUrl()+"' was linked to '"+page.getFriendlyUrl()+"'");
	}
	
	private LiferayPage(long groupId, LiferayPage parent, String resourceKey, String friendlyUrl, String layoutTpl, boolean hidden, String themeId, String colorSchemeId) throws PortalException, SystemException {
		this.resourceKey = resourceKey;
		this.friendlyUrl = friendlyUrl;
		this.layoutTpl = layoutTpl;
		if(parent == null) {
			this.parentPageId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		} else {
			this.parentPageId = parent.getLayout().getLayoutId();
		}
		this.groupId = groupId;
		this.hidden = hidden;
		this.colorSchemeId = colorSchemeId;
		this.themeId = themeId;
		persist();
	}
	
	LiferayPage(long groupId, String resourceKey, String friendlyUrl, String themeId, String colorSchemeId) throws PortalException, SystemException {
		this(groupId, null, resourceKey, friendlyUrl, DEFAULT_LAYOUT_TPL, false, themeId, colorSchemeId);
	}

	private Layout persist() throws PortalException, SystemException {
		String description = StringPool.BLANK;
		String type = LayoutConstants.TYPE_PORTLET;
		Layout page = null;
		try {
			// Create the page
			page = LayoutLocalServiceUtil.addLayout(getUserId(), groupId, false, parentPageId,
				createLocalesMap(resourceKey), new HashMap<Locale, String>(), description, type, hidden, friendlyUrl);
			page.setThemeId(themeId);
			page.setColorSchemeId(colorSchemeId);
			// Set the layout
			LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) page.getLayoutType();
			layoutTypePortlet.setLayoutTemplateId(getUserId(), layoutTpl, false);
			page = LayoutLocalServiceUtil.updateLayout(page, false);
			log.info("Page '"+friendlyUrl+"' created");
		} catch(LayoutFriendlyURLException e) {
			log.warn("Page '"+friendlyUrl+"' already existed");
		}
		return page;
	}
	
	private Map<Locale, String> createLocalesMap(String resourceKey) {
		Locale[] locales = LanguageUtil.getAvailableLocales();
		Map<Locale, String> result = new HashMap<Locale, String>();
		for (Locale locale : locales) {
			result.put(locale, LanguageUtil.get(locale, resourceKey));
		}
		return result;
	}
	
	public Layout getLayout() throws PortalException, SystemException {
		return LayoutLocalServiceUtil.getFriendlyURLLayout(groupId, false, getFriendlyUrl());
	}
	
	public long getLayoutId() throws PortalException, SystemException {
		return getLayout().getLayoutId();
	}
	
	long getParentPageId() {
		return parentPageId;
	}
	String getResourceKey() {
		return resourceKey;
	}
	String getFriendlyUrl() {
		return friendlyUrl;
	}
	String getLayoutTpl() {
		return layoutTpl;
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayPage.class);

	public String addPortlet(LiferayPortlet portlet) throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		return addPortlet(portlet, DEFAULT_COLUMN);
	}
	
	public String addPortlet(LiferayPortlet portlet, int column) throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		Layout page = getLayout();
		String portletInstanceId = ((LayoutTypePortlet) page.getLayoutType()).addPortletId(getUserId(), portlet.getPortletId(), "column-"+column, -1, false);
		LayoutLocalServiceUtil.updateLayout(page, false);
		log.info("Portlet '" + portletInstanceId + "' added on page '" + friendlyUrl + "'");
		addPreferences(portlet, portletInstanceId);
		return portletInstanceId;
	}

	private long getUserId() throws PortalException, SystemException {
		return UserLocalServiceUtil.getDefaultUserId(GroupLocalServiceUtil.getGroup(groupId).getCompanyId());
	}
	
	private void addPreferences(LiferayPortlet portlet, String portletInstanceId) throws SystemException, ReadOnlyException, IOException, ValidatorException, PortalException {
		PortletPreferences prefs = PortletPreferencesFactoryUtil.getLayoutPortletSetup(getLayout(), portletInstanceId);
		Map<String, String> preferences = portlet.getPreferences();
		for(String prefKey: preferences.keySet()) {
			prefs.setValue(prefKey, preferences.get(prefKey));
		}
		prefs.store();
		log.info("Preferences for portlet '"+portletInstanceId+"' configured.");
	}
	
	public void addViewPermissions(String... roleNames) throws PortalException, SystemException {
		setViewPermissions(true, roleNames);
	}
	
	public void removeViewPermissions(String... roleNames) throws PortalException, SystemException {
		setViewPermissions(false, roleNames);
	}

	private void setViewPermissions(boolean add, String... roleNames) throws PortalException, SystemException {
		String action = ActionKeys.VIEW;
		int scope = ResourceConstants.SCOPE_INDIVIDUAL;
		String name = Layout.class.getName();
		String primKey = String.valueOf(getLayout().getPlid());
		for (String roleName: roleNames) {
			long roleId = RoleLocalServiceUtil.getRole(getLayout().getCompanyId(), roleName).getRoleId();
			Resource resource = getOrCreateResource(name, primKey, scope);
			if(add) {
				PermissionLocalServiceUtil.setRolePermissions(roleId, new String[] {action}, resource.getResourceId());
			} else {
				PermissionLocalServiceUtil.unsetRolePermission(roleId, getLayout().getCompanyId(), name, scope, primKey, action);
			}
		}
	}
	
	private Resource getOrCreateResource(String name, String primKey, int scope) throws PortalException, SystemException {
		try {
			return ResourceLocalServiceUtil.getResource(getLayout().getCompanyId(), name, scope, primKey);
		} catch(NoSuchResourceException e) {
			return ResourceLocalServiceUtil.addResource(getLayout().getCompanyId(), name, scope, primKey);
		}
	}
	
}
