package be.lions.duckrace.startup.type;

import java.io.File;
import java.util.Map;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateTemplateIdException;
import com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil;

public class LiferayTemplate {

	private String id;
	private LiferayStructure structure;
	
	LiferayTemplate(LiferayStructure structure, String id, Language scriptingLanguage, String script, boolean cached) throws PortalException, SystemException {
		this.id = id;
		this.structure = structure;
		persist(scriptingLanguage, script, cached);
	}
	
	public LiferayArticle addArticle(String id, String title, String description, Map<String, String> values, boolean override) throws PortalException, SystemException {
		return new LiferayArticle(this, id, title, description, values, override);
	}
	
	private void persist(Language scriptingLanguage, String script, boolean cached) throws PortalException, SystemException {
		LiferayGroup group = structure.getGroup();
		long userId = group.getDefaultUserId();
		long groupId = group.getGroupId();
		boolean autoTemplateId = false;
		boolean formatXsl = true;
		String langType = scriptingLanguage.getId();
		boolean smallImage = false;
		String smallImageURL = null;
		File smallFile = null;

		ServiceContext sc = new ServiceContext();
		sc.setAddCommunityPermissions(true);
		sc.setAddGuestPermissions(true);

		try {
			JournalTemplateLocalServiceUtil.addTemplate(userId, groupId, id, autoTemplateId, 
				structure.getId(), id, id, script, formatXsl, langType, cached,
				smallImage, smallImageURL, smallFile, sc);
			log.info("Template '" + id + "' created.");
		} catch (DuplicateTemplateIdException e) {
			log.warn("Template '" + id + "' already existed, updating...");
			JournalTemplateLocalServiceUtil.updateTemplate(groupId, id, 
				structure.getId(), id, id, script, formatXsl, langType, cached, 
				smallImage, smallImageURL, smallFile, sc);
			log.info("Template '" + id + "' updated.");
		}
	}
	
	String getId() {
		return id;
	}
	
	LiferayStructure getStructure() {
		return structure;
	}

	public enum Language {
		VELOCITY("vm"),
		XSL("xsl"),
		CSS("css");
		
		private String id;
		private Language(String id) {
			this.id = id;
		}
		public String getId() {
			return id;
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayTemplate.class);
	
}
