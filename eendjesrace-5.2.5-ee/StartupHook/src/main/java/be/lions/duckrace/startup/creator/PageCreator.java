package be.lions.duckrace.startup.creator;

import java.io.IOException;

import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import be.lions.duckrace.startup.custom.DuckRaceTheme;
import be.lions.duckrace.startup.custom.LiferayArticle;
import be.lions.duckrace.startup.custom.QueryRegisteredDucksPortlet;
import be.lions.duckrace.startup.custom.RegistrationButtons;
import be.lions.duckrace.startup.custom.RegistrationPortlet;
import be.lions.duckrace.startup.type.LiferayCommunity;
import be.lions.duckrace.startup.type.LiferayPage;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet;
import be.lions.duckrace.startup.type.portlet.LoginPortlet;
import be.lions.duckrace.startup.type.portlet.WebContentPortlet;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet.AssetType;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet.DisplayStyle;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet.OrderByField;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet.OrderByType;
import be.lions.duckrace.startup.type.portlet.AssetPublisherPortlet.Pagination;
import be.lions.duckrace.types.DuckRaceTag;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.model.RoleConstants;

public class PageCreator {

	private static final String THEME_ID = "eendjes_WAR_eendjestheme";
	
	private LiferayCommunity guest;

	public PageCreator(LiferayCommunity guest) {
		this.guest = guest;
	}

	public void insertPages() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		guest.deleteAllPages();
		guest.setThemeId(THEME_ID);
		
		createHome();
		createOrganisatoren();
		createInfo();
		createSponsors();
		createNieuws();
		createEvent();
		createRegistreren();
		createMijnEendjesnummers();
		
		createAdmin();
	}

	private void createHome() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage page = guest.addPage("Home", "/home", "2_columns_iii");
		page.addPortlet(createWebContent(LiferayArticle.HOME), 1);
		page.addPortlet(new RegistrationButtons(), 2);
	}

	private void createOrganisatoren() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage parent = guest.addPage("Organisatie", "/organisatie");
		LiferayPage child = parent.addChildPage("Lions Oudenaarde Vlaamse Ardennen", "/organisatoren/lions-oudenaarde");
		child.addPortlet(createWebContent(LiferayArticle.ORG_LIONS));
		parent.linkToPage(child);
	}

	private void createInfo() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage parent = guest.addPage("Info", "/info");
		LiferayPage child = parent.addChildPage("Evenement", "/info/evenement");
		child.addPortlet(createWebContent(LiferayArticle.INFO_EVENEMENT));
		parent.linkToPage(child);
		child = parent.addChildPage("Reglement", "/info/reglement");
		child.addPortlet(createWebContent(LiferayArticle.INFO_REGLEMENT));
		child = parent.addChildPage("Race verloop", "/info/race-verloop");
		child.addPortlet(createWebContent(LiferayArticle.INFO_RACEVERLOOP));
		child = parent.addChildPage("Hoofdprijs", "/info/hoofdprijs");
		child.addPortlet(createWebContent(LiferayArticle.INFO_HOOFDPRIJS));
		child = parent.addChildPage("Opbrengst", "/info/opbrengst");
		child.addPortlet(createWebContent(LiferayArticle.INFO_OPBRENGST));
	}

	private void createSponsors() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage parent = guest.addPage("Sponsors", "/sponsors", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		parent.addPortlet(createWebContent(LiferayArticle.LOGOS));
		// 100 eendjes sponsors
		LiferayPage child = parent.addChildPage("Sponsors (goud)", "/sponsors/goud", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		child.addPortlet(createWebContent(LiferayArticle.DUCKTYPE_100_DUCKS));
		child.addPortlet(createAssetPublisher(DuckRaceTag.SPONSOR_4_STARS));
		// 50 eendjes sponsors
		child = parent.addChildPage("Sponsors (zilver)", "/sponsors/zilver", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		child.addPortlet(createWebContent(LiferayArticle.DUCKTYPE_50_DUCKS));
		child.addPortlet(createAssetPublisher(DuckRaceTag.SPONSOR_3_STARS));
		// 20 eendjes sponsors
		child = parent.addChildPage("Sponsors (brons)", "/sponsors/brons", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		child.addPortlet(createWebContent(LiferayArticle.DUCKTYPE_20_DUCKS));
		child.addPortlet(createAssetPublisher(DuckRaceTag.SPONSOR_2_STARS));
		// 10 eendjes sponsors
		child = parent.addChildPage("Sponsors (uit sympathie)", "/sponsors/uit-sympathie", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		child.addPortlet(createWebContent(LiferayArticle.DUCKTYPE_10_DUCKS));
		child.addPortlet(createAssetPublisher(DuckRaceTag.SPONSOR_1_STAR));
		// merchants
		child = parent.addChildPage("Deelnemende handelaars", "/sponsors/handelaars", DuckRaceTheme.THEME.getId(), DuckRaceTheme.BANNERLESS_SCHEME.getId());
		child.addPortlet(createAssetPublisher(DuckRaceTag.MERCHANT));
	}

	private void createNieuws() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage parent = guest.addPage("Nieuws", "/nieuws");
		LiferayPage child = parent.addChildPage("Blogs", "/nieuws/blogs");
		child.addPortlet(createBlog());
		parent.linkToPage(child);
		child = parent.addChildPage("Foto-album", "/nieuws/foto-album");
		child.addPortlet(createWebContent(LiferayArticle.FOTO_ALBUM));
		child = parent.addChildPage("Persconferenties", "/nieuws/persconferenties");
		child.addPortlet(createWebContent(LiferayArticle.PERSCONFERENTIES));
		child = parent.addChildPage("Reacties", "/nieuws/reacties");
		child.addPortlet(createWebContent(LiferayArticle.REACTIES, true));
	}
	
	private void createEvent() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage parent = guest.addPage("26-05-2013", "/26-05-2013");
		LiferayPage child = parent.addChildPage("Programma", "/26-05-2013/programma");
		child.addPortlet(createWebContent(LiferayArticle.EVENT_PROGRAMMA));
		parent.linkToPage(child);
		child = parent.addChildPage("Activiteiten", "/26-05-2013/activiteiten");
		child.addPortlet(createWebContent(LiferayArticle.EVENT_ACTS));
		child = parent.addChildPage("Eten & Drinken", "/26-05-2013/eten-en-drinken");
		child.addPortlet(createWebContent(LiferayArticle.EVENT_BEVERAGE));
	}

	private void createRegistreren() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage page = guest.addPage("Registratie", "/registratie");
		page.addPortlet(new RegistrationPortlet());
	}
	
	private void createMijnEendjesnummers() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage page = guest.addPage("Mijn eendjes", "/mijn-eendjes");
		page.addPortlet(new QueryRegisteredDucksPortlet());
	}

	private void createAdmin() throws PortalException, SystemException, ReadOnlyException, ValidatorException, IOException {
		LiferayPage page = guest.addHiddenPage("Admin", "/admin");
		page.addPortlet(new LoginPortlet());
		page.removeViewPermissions(RoleConstants.GUEST);
		page.addViewPermissions(RoleConstants.USER);
	}

	private WebContentPortlet createWebContent(LiferayArticle article) throws PortalException, SystemException {
		return createWebContent(article, false);
	}
	
	private WebContentPortlet createWebContent(LiferayArticle article, boolean comments) throws PortalException, SystemException {
		WebContentPortlet result = new WebContentPortlet(guest.getGroupId(), article.name());
		result.setComments(comments);
		return result;
	}
	
	private AssetPublisherPortlet createBlog() {
		AssetPublisherPortlet result = new AssetPublisherPortlet();
		result.setMergeWithUrlTags(true);
		result.setDisplayStyle(DisplayStyle.FULL_CONTENT);
		result.setAssetType(AssetType.BLOGS);
		result.setOrderByField(OrderByField.PUBLISH_DATE);
		result.setOrderByType(OrderByType.DESC);
		result.setNbItemsPerPage(5);
		result.setPagination(Pagination.SIMPLE);
		return result;
	}
	
	private AssetPublisherPortlet createAssetPublisher(DuckRaceTag tag) {
		AssetPublisherPortlet result = new AssetPublisherPortlet();
		result.setEntries(tag.getName());
		result.setDisplayStyle(DisplayStyle.FULL_CONTENT);
		result.setAssetType(AssetType.WEB_CONTENT);
		result.setOrderByField(OrderByField.TITLE);
		result.setOrderByType(OrderByType.ASC);
		result.setNbItemsPerPage(100);
		return result;
	}
	
}