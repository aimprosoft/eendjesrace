package be.lions.duckrace.startup.type;

import com.liferay.portal.DuplicateGroupException;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;

public class LiferayCommunity extends LiferayGroup {

	LiferayCommunity(LiferayCompany company, String name) throws PortalException, SystemException {
		super(company, name);
	}

	protected void persist() throws PortalException, SystemException {
		long userId = UserLocalServiceUtil.getDefaultUserId(getCompanyId());
		String className = Group.class.getName();
		long classPK = 0;
		String description = getName();
		int type = GroupConstants.TYPE_COMMUNITY_OPEN;
		String friendlyURL = null;
		boolean active = true;
		ServiceContext sc = null;
		try {
			GroupLocalServiceUtil.addGroup(userId, className, classPK, getName(), description, type, friendlyURL, active, sc);
			log.info("Community '"+getName()+"' created.");
		} catch(DuplicateGroupException e) {
			log.warn("Community '"+getName()+"' already existed.");
		}
	}

	public long getGroupId() throws PortalException, SystemException {
		return GroupLocalServiceUtil.getGroup(getCompanyId(), getName()).getGroupId();
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayCommunity.class);

}
