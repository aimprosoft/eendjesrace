package be.lions.duckrace.startup.type;


import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;

public abstract class LiferayGroup {
	
	private String name;
	private LiferayCompany company;
	
	LiferayGroup(LiferayCompany company, String name) throws PortalException, SystemException {
		this.company = company;
		this.name = name;
		persist();
	}
	
	String getName() {
		return name;
	}
	
	long getCompanyId() {
		return company.getCompanyId();
	}
	
	long getDefaultUserId() throws PortalException, SystemException {
		return company.getDefaultUserId();
	}
	
	protected abstract void persist() throws PortalException, SystemException;
	public abstract long getGroupId() throws PortalException, SystemException;
	
	/*
	 * Page management
	 */
	
	public LiferayPage addPage(String resourceKey, String friendlyUrl, String themeId, String colorSchemeId) throws PortalException, SystemException {
		return new LiferayPage(getGroupId(), resourceKey, friendlyUrl, themeId, colorSchemeId);
	}
	
	public LiferayPage addPage(String resourceKey, String friendlyUrl, String layoutTpl) throws PortalException, SystemException {
		return new LiferayPage(getGroupId(), resourceKey, friendlyUrl, layoutTpl);
	}
	
	public LiferayPage addPage(String resourceKey, String friendlyUrl) throws PortalException, SystemException {
		return new LiferayPage(getGroupId(), resourceKey, friendlyUrl, false);
	}
	
	public LiferayPage addHiddenPage(String resourceKey, String friendlyUrl) throws PortalException, SystemException {
		return new LiferayPage(getGroupId(), resourceKey, friendlyUrl, true);
	}

	public void deleteAllPages() throws PortalException, SystemException {
		LayoutLocalServiceUtil.deleteLayouts(getGroupId(), false);
		log.info("All pages for group '"+name+"' were deleted");
	}
	
	/*
	 * Web content
	 */
	
	public LiferayStructure addStructure(String id, LiferayStructureFieldList fieldList) throws SystemException, PortalException {
		return new LiferayStructure(this, id, fieldList);
	}
	
	/*
	 * Images and documents
	 */
	
	public LiferayDocumentFolder addDocumentFolder(String name, String description) throws PortalException, SystemException {
		return new LiferayDocumentFolder(this, name, description);
	}
	
	public LiferayDocumentFolder addDocumentFolder(String name) throws PortalException, SystemException {
		return addDocumentFolder(name, name);
	}
	
	public LiferayImageFolder addImageFolder(String name, String description) throws PortalException, SystemException {
		return new LiferayImageFolder(this, name, description);
	}
	
	public LiferayImageFolder addImageFolder(String name) throws PortalException, SystemException {
		return addImageFolder(name, name);
	}
	
	/*
	 * Tags and categories
	 */
	
	public LiferayTagSet addTagSet(String name) throws PortalException, SystemException {
		return new LiferayTagSet(this, name);
	}
	
	public LiferayCategorySet addCategorySet(String name) throws PortalException, SystemException {
		return new LiferayCategorySet(this, name);
	}
	
	/*
	 * Look-and-feel
	 */
	
	public void setThemeId(String themeId) throws PortalException, SystemException {
		LayoutSetLocalServiceUtil.updateLookAndFeel(getGroupId(), themeId, "01", "", false);
		log.info("Theme for group '"+name+"' was set to '"+themeId+"'");
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayGroup.class);
	
}
