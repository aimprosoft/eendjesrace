package be.lions.duckrace.startup.type;

import com.liferay.portal.DuplicateRoleException;
import com.liferay.portal.NoSuchResourceException;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Resource;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import static com.liferay.portal.security.permission.ActionKeys.*;

import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.PermissionLocalServiceUtil;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import static com.liferay.portal.util.PortletKeys.*;

import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.imagegallery.model.IGFolder;
import com.liferay.portlet.imagegallery.model.IGImage;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.tags.model.TagsEntry;
import com.liferay.portlet.tags.model.TagsVocabulary;

public class LiferayRole {

	private LiferayCompany company;
	private String roleName;
	private RoleScope scope;
	
	protected LiferayRole(LiferayCompany company, String roleName, RoleScope scope) throws PortalException, SystemException {
		this.company = company;
		this.roleName = roleName;
		this.scope = scope;
		persist();
	}
	
	public void addControlPanelAccess(String... portletIds) throws SystemException, PortalException {
		for (String portletId: portletIds) {
			setGlobalPermissions(portletId, ActionKeys.ACCESS_IN_CONTROL_PANEL);
		}
	}
	
	public void setGlobalPermissions(String name, String... actionIds) throws SystemException, PortalException {
		int scope = ResourceConstants.SCOPE_COMPANY;
		String primKey = String.valueOf(getCompanyId());
		setPermissions(scope, primKey, name, actionIds);
	}
	
	public void setGlobalPermissions(Class<?> clazz, String...actionIds) throws SystemException, PortalException {
		setGlobalPermissions(clazz.getName(), actionIds);
	}
	
	private void setPermissions(int scope, String primKey, String name, String[] actionIds) throws PortalException, SystemException {
		Resource resource = getOrCreateResource(scope, primKey, name);
		PermissionLocalServiceUtil.setRolePermissions(getRole().getRoleId(), actionIds, resource.getResourceId());
	}
	
	private Resource getOrCreateResource(int scope, String primKey, String name) throws PortalException, SystemException {
		try {
			return ResourceLocalServiceUtil.getResource(getCompanyId(), name, scope, primKey);
		} catch(NoSuchResourceException e) {
			return ResourceLocalServiceUtil.addResource(getCompanyId(), name, scope, primKey);
		}
	}

	private long getCompanyId() {
		return company.getCompanyId();
	}
	
	private void persist() throws PortalException, SystemException {
		long creatorId = company.getDefaultUserId();
		try {
			RoleLocalServiceUtil.addRole(creatorId, getCompanyId(), roleName, roleName, scope.getType());
			log.info("Role " + roleName + " was created.");
		} catch(DuplicateRoleException e) {
			log.warn("Role " + roleName + " already existed.");
		}
	}

	public Role getRole() throws PortalException, SystemException {
		return RoleLocalServiceUtil.getRole(getCompanyId(), roleName);
	}
	
	public enum RoleScope {
		COMPANY(RoleConstants.TYPE_REGULAR),
		COMMUNITY(RoleConstants.TYPE_COMMUNITY),
		ORGANIZATION(RoleConstants.TYPE_ORGANIZATION);
		private int type;
		private RoleScope(int type) {
			this.type = type;
		}
		public int getType() {
			return type;
		}
	}
	
	public enum Permission {
		// Web content
		CONTENT_CONTROL_PANEL(JOURNAL_CONTENT, ACCESS_IN_CONTROL_PANEL),
		CREATE_ARTICLE("com.liferay.portlet.journal", ADD_ARTICLE),
		APPROVE_ARTICLE("com.liferay.portlet.journal", ActionKeys.APPROVE_ARTICLE),
		VIEW_ARTICLE(JournalArticle.class, VIEW),
		DELETE_ARTICLE(JournalArticle.class, DELETE),
		EXPIRE_ARTICLE(JournalArticle.class, EXPIRE),
		UPDATE_ARTICLE(JournalArticle.class, UPDATE),
		DELETE_ARTICLE_COMMENT(JournalArticle.class, DELETE_DISCUSSION),
		UPDATE_ARTICLE_COMMENT(JournalArticle.class, UPDATE_DISCUSSION),
		
		// Image Gallery
		IMAGES_CONTROL_PANEL(IMAGE_GALLERY, ACCESS_IN_CONTROL_PANEL),
		ADD_IMG_FOLDER("com.liferay.portlet.imagegallery", ADD_FOLDER),
		UPDATE_IMG_FOLDER(IGFolder.class, UPDATE),
		DELETE_IMG_FOLDER(IGFolder.class, DELETE),
		VIEW_IMG_FOLDER(IGFolder.class, VIEW),
		ADD_IMG_SUBFOLDER(IGFolder.class, ADD_SUBFOLDER),
		ADD_IMAGE(IGFolder.class, ActionKeys.ADD_IMAGE),
		UPDATE_IMAGE(IGImage.class, UPDATE),
		DELETE_IMAGE(IGImage.class, DELETE),
		VIEW_IMAGE(IGImage.class, VIEW),
		
		// Document library
		DOCUMENTS_CONTROL_PANEL(DOCUMENT_LIBRARY, ACCESS_IN_CONTROL_PANEL),
		ADD_DOC_FOLDER("com.liferay.portlet.documentlibrary", ADD_FOLDER),
		UPDATE_DOC_FOLDER(DLFolder.class, UPDATE),
		DELETE_DOC_FOLDER(DLFolder.class, DELETE),
		ADD_DOC_SUBFOLDER(DLFolder.class, ADD_SUBFOLDER),
		VIEW_DOC_FOLDER(DLFolder.class, VIEW),
		ADD_DOCUMENT(DLFolder.class, ActionKeys.ADD_DOCUMENT),
		UPDATE_DOCUMENT(DLFileEntry.class, UPDATE),
		DELETE_DOCUMENT(DLFileEntry.class, DELETE),
		VIEW_DOCUMENT(DLFileEntry.class, VIEW),
		
		// Blogs
		BLOGS_CONTROL_PANEL(BLOGS, ACCESS_IN_CONTROL_PANEL),
		ADD_BLOG("com.liferay.portlet.blogs", ADD_ENTRY),
		DELETE_BLOG(BlogsEntry.class, DELETE),
		DELETE_BLOG_COMMENT(BlogsEntry.class, DELETE_DISCUSSION),
		UPDATE_BLOG(BlogsEntry.class, UPDATE),
		UPDATE_BLOG_COMMENT(BlogsEntry.class, UPDATE_DISCUSSION),
		
		// Tags and categories
		TAGS_CONTROL_PANEL(TAGS_ADMIN, ACCESS_IN_CONTROL_PANEL),
		VIEW_TAGS(TagsEntry.class, VIEW),
		VIEW_TAGSETS(TagsVocabulary.class, VIEW);
		
		private String subject;
		private String action;
		private Permission(String subject, String action) {
			this.subject = subject;
			this.action = action;
		}
		private Permission(Class<?> clazz, String action) {
			this(clazz.getName(), action);
		}
		public String getSubject() {
			return subject;
		}
		public String getAction() {
			return action;
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayRole.class);
	
}
