package be.lions.duckrace.startup.custom;

import be.lions.duckrace.startup.type.LiferayPortlet;

public class QueryRegisteredDucksPortlet extends LiferayPortlet {

	public QueryRegisteredDucksPortlet() {
		super("QueryRegistratedDucksPortlet_WAR_registrationportlet");
	}

}
