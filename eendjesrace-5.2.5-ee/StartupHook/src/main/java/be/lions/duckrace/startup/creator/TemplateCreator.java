package be.lions.duckrace.startup.creator;

import java.io.File;

import be.lions.duckrace.startup.util.StartupUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateTemplateIdException;
import com.liferay.portlet.journal.model.JournalTemplate;
import com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil;

public class TemplateCreator extends ResourceCreator {

	private static final String TEMPLATE_PATH = "/wcm/templates/";

	@Override
	void doInsert(String filename, String fileContent) throws PortalException, SystemException {
		String templateId = filename;
		boolean autoTemplateId = false;
		String structureId = filename.toUpperCase();
		String name = filename;
		String description = filename;
		String xsl = fileContent;
		boolean formatXsl = false;
		String langType = "vm";
		boolean cacheable = false;
		boolean smallImage = false;
		String smallImageURL = null;
		File smallFile = null;

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddCommunityPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		try {
			JournalTemplateLocalServiceUtil.addTemplate(StartupUtil.getAdminId(), StartupUtil.getGroupId(),
				templateId, autoTemplateId, structureId, name, description, xsl, formatXsl, langType, cacheable,
				smallImage, smallImageURL, smallFile, serviceContext);
			log.info("Template '" + templateId + "' inserted.");
		} catch (DuplicateTemplateIdException e) {
			log.warn("Template '" + templateId + "' already existed, start update.");
			JournalTemplate template = JournalTemplateLocalServiceUtil.getTemplate(StartupUtil.getGroupId(), templateId);
			template.setXsl(fileContent);
			JournalTemplateLocalServiceUtil.updateJournalTemplate(template, false);
		}
	}

	@Override
	String getResourceFolder() {
		return TEMPLATE_PATH;
	}

	private static Log log = LogFactoryUtil.getLog(TemplateCreator.class);

}