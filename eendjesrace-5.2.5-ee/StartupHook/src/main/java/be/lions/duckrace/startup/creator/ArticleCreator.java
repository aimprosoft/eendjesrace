package be.lions.duckrace.startup.creator;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import be.lions.duckrace.startup.custom.LiferayArticle;
import be.lions.duckrace.startup.util.StartupUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.DuplicateArticleIdException;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

public class ArticleCreator {

	private static Log log = LogFactoryUtil.getLog(ArticleCreator.class);

	public void insertArticles() throws PortalException, SystemException, IOException {
		for (LiferayArticle article: LiferayArticle.values()) {
			insertArticle(article);
		}
	}
	
	private void insertArticle(LiferayArticle article) throws IOException, PortalException, SystemException {
		String articleId = article.name();
		boolean autoArticleId = false;
		String title = articleId;
		String description = null;
		String content = getContent(article);
		
		long userId = StartupUtil.getAdminId();
		long groupId = StartupUtil.getGroupId();
		String type = "general";
		String structureId = article.getStructureId();
		String templateId = article.getStructureId();
		int displayDateMonth = Calendar.JANUARY;
		int displayDateDay = 1;
		int displayDateYear = 1900;
		int displayDateHour = 12;
		int displayDateMinute = 0;
		int expirationDateMonth = Calendar.JANUARY;
		int expirationDateDay = 1;
		int expirationDateYear = 2200;
		int expirationDateHour = 12;
		int expirationDateMinute = 0;
		boolean neverExpire = true;
		int reviewDateMonth = Calendar.JANUARY;
		int reviewDateDay = 1;
		int reviewDateYear = 2200;
		int reviewDateHour = 12;
		int reviewDateMinute = 0;
		boolean neverReview = true;
		boolean indexable = true;
		boolean smallImage = false;
		String smallImageURL = "";
		File smallFile = null;
		Map<String, byte[]> images = null;
		String articleURL = null;
		ServiceContext serviceContext = new ServiceContext();
		try {
			JournalArticle article1 = JournalArticleLocalServiceUtil.addArticle(userId, groupId, articleId, autoArticleId, title, description, content, type,
					structureId, templateId, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay,
					expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute,
					neverReview, indexable, smallImage, smallImageURL, smallFile, images, articleURL, serviceContext);
		
			JournalArticleLocalServiceUtil.approveArticle(userId, groupId, articleId, article1.getVersion(), null, serviceContext);
			log.info("Article '" + articleId + "' successfully created.");
		} catch (DuplicateArticleIdException e) {
			log.info("Article '" + articleId + "' already existed.");
		}
	}

	private String getContent(LiferayArticle article) throws IOException, PortalException, SystemException {
		if(!article.hasStructure()) {
			String fileContent = null;
			try {
				fileContent = StringUtil.read(getClass().getClassLoader(), "wcm/articles/"+article.name()+".html");
			} catch(IOException e) {
				fileContent = "";
			}
			fileContent = StringUtil.replace(fileContent, "[$GROUP_ID$]", ""+StartupUtil.getGroupId());
			StringBuffer articleContentXml = new StringBuffer();
			articleContentXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			articleContentXml.append("<root available-locales=\"en_US\" default-locale=\"en_US\">");
			articleContentXml.append("<static-content language-id=\"en_US\"><![CDATA[" + fileContent + "]]></static-content>");
			articleContentXml.append("</root>");
			return articleContentXml.toString();
		}
		return StringUtil.read(getClass().getClassLoader(), "wcm/articles/"+article.getStructureId().toLowerCase()+"_dummy.html");
	}

}