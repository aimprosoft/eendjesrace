package be.lions.duckrace.domain;

import static junit.framework.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.service.InternalRangeOverlapException;
import be.lions.duckrace.service.InvalidRangeException;
import be.lions.duckrace.service.LotRangeService;

public class LotRangeServiceValidateTest {

	private static final int MAX = 10;
	
	private LotRange range1;
	private LotRange range2;
	private LotRange range3;
	
	private LotRangeService service;
	
	@Before
	public void setup() {
		service = new LotRangeService();
		range1 = createRange(1, 3);
		range2 = createRange(5, 8);
		range3 = createRange(4, 6);
	}
	
	@Test
	public void nonOverlappingRangesThrowsNoException() throws InternalRangeOverlapException, InvalidRangeException {
		List<LotRange> ranges = Arrays.asList(range1, range2);
		service.validate(ranges, MAX);
	}
	
	@Test
	public void overlappingRangesThrowsException() throws InvalidRangeException {
		List<LotRange> ranges = Arrays.asList(range1, range2, range3);
		try {
			service.validate(ranges, MAX);
		} catch(InternalRangeOverlapException e) {
			assertTrue(e.getClashingRanges().contains(range2));
			assertTrue(e.getClashingRanges().contains(range3));
		}
	}

	private LotRange createRange(int from, int to) {
		LotRange range = new LotRange();
		range.setFrom(from);
		range.setTo(to);
		return range;
	}
	
}
