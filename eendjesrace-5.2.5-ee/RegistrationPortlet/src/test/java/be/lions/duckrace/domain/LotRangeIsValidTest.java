package be.lions.duckrace.domain;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import be.lions.duckrace.model.LotRange;

public class LotRangeIsValidTest {

	private static final int MAX = 10;
	private LotRange range;
	
	@Before
	public void setup() {
		range = new LotRange();
		range.setFrom(5);
		range.setTo(9);
	}

	@Test
	public void toIndexGreaterThanFromIndexIsValid() {
		assertTrue(range.isValid(MAX));
	}
	
	@Test
	public void negativeFromIndexIsInvalid() {
		range.setFrom(-1);
		assertFalse(range.isValid(MAX));
	}
	
	@Test
	public void negativeToIndexIsInvalid() {
		range.setTo(-1);
		assertFalse(range.isValid(MAX));
	}
	
	@Test
	public void zeroFromIndexIsInvalid() {
		range.setFrom(0);
		assertFalse(range.isValid(MAX));
	}
	
	@Test
	public void zeroToIndexIsInvalid() {
		range.setTo(0);
		assertFalse(range.isValid(MAX));
	}
	
	@Test
	public void toIndexSmallerThanFromIndexIsInvalid() {
		range.setTo(3);
		assertFalse(range.isValid(MAX));
	}
	
	@Test
	public void singletonRangeIsValid() {
		range.setTo(5);
		assertTrue(range.isValid(MAX));
	}
	
	@Test
	public void toIndexGreaterThanMaxIsInvalid() {
		range.setTo(MAX+1);
		assertFalse(range.isValid(MAX));
	}
	
}
