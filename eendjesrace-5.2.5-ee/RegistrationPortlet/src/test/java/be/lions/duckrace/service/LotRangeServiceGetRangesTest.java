package be.lions.duckrace.service;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotRange;

@RunWith(MockitoJUnitRunner.class)
public class LotRangeServiceGetRangesTest {

	@Mock private Lot lot1;
	@Mock private Lot lot2;
	@Mock private Lot lot3;
	@Mock private Lot lot4;
	@Mock private Lot lot5;
	@Mock private Lot lot6;
	@Mock private Lot lot7;
	@Mock private Lot lot8;
	@Mock private Lot lot9;
	
	private LotRangeService converter;
	
	@Before
	public void setup() {
		converter = new LotRangeService();
		when(lot1.getLotNumber()).thenReturn(1);
		when(lot2.getLotNumber()).thenReturn(2);
		when(lot3.getLotNumber()).thenReturn(3);
		when(lot4.getLotNumber()).thenReturn(4);
		when(lot5.getLotNumber()).thenReturn(5);
		when(lot6.getLotNumber()).thenReturn(6);
		when(lot7.getLotNumber()).thenReturn(7);
		when(lot8.getLotNumber()).thenReturn(8);
		when(lot9.getLotNumber()).thenReturn(9);
	}
	
	@Test
	public void emptyLotsListReturnsEmptyRangeList() {
		List<Lot> input = Collections.emptyList();
		assertEquals(Collections.emptyList(), converter.getRanges(input));
	}
	
	@Test
	public void oneRangeOfLotsReturnsOneLotRange() {
		List<Lot> input = Arrays.asList(lot3, lot4, lot5, lot6);
		LotRange expected = new LotRange();
		expected.setFrom(3);
		expected.setTo(6);
		assertEquals(Arrays.asList(expected), converter.getRanges(input));
	}
	
	@Test
	public void twoRangesOfLotsReturnsTwoLotRanges() {
		List<Lot> input = Arrays.asList(lot2, lot3, lot4, lot8, lot9);
		LotRange expected1 = new LotRange();
		expected1.setFrom(2);
		expected1.setTo(4);
		LotRange expected2 = new LotRange();
		expected2.setFrom(8);
		expected2.setTo(9);
		assertEquals(Arrays.asList(expected1, expected2), converter.getRanges(input));
	}
	
	@Test
	public void singletonRanges() {
		List<Lot> input = Arrays.asList(lot2, lot9);
		LotRange expected1 = new LotRange();
		expected1.setFrom(2);
		expected1.setTo(2);
		LotRange expected2 = new LotRange();
		expected2.setFrom(9);
		expected2.setTo(9);
		assertEquals(Arrays.asList(expected1, expected2), converter.getRanges(input));
		
	}
	
}
