package be.lions.duckrace.domain;

import static junit.framework.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import be.lions.duckrace.model.LotRange;

public class LotRangeGetIndicesTest {

	private LotRange range;
	
	@Before
	public void setup() {
		range = new LotRange();
	}
	
	@Test
	public void singletonRangeReturnsOneIndex() {
		range.setFrom(4);
		range.setTo(4);
		assertEquals(Arrays.asList(4), range.getIndices());
	}
	
	@Test
	public void normalRangeReturnsMultipleIndices() {
		range.setFrom(4);
		range.setTo(8);
		assertEquals(Arrays.asList(4, 5, 6, 7, 8), range.getIndices());
	}
	
}
