package be.lions.duckrace.bean;

import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.util.PropKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, MessageUtil.class })
public class NavigationBeanTest {

	private static final String DEFAULT_URL = "http://www.example.com/%LOCALE%/test";
	private static final String DEFAULT_URL_NL = "http://www.example.com/nl/test";
	private static final String DEFAULT_LANGUAGE = "nl";

	@Before
	public void setup() {
		PowerMockito.mockStatic(PropsUtil.class);
		when(PropsUtil.get(PropKeys.BEHAPPY2_ORDER_URL)).thenReturn(DEFAULT_URL);

		PowerMockito.mockStatic(MessageUtil.class);
		when(MessageUtil.getLocale()).thenReturn(new Locale(DEFAULT_LANGUAGE));
	}

	@Test
	public void paymentURLUsesCorrectLanguage() {
		NavigationBean navigationBean = new NavigationBean();

		String url = navigationBean.getPaymentModuleURL();

		assertEquals(DEFAULT_URL_NL, url);
	}
}
