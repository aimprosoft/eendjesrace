package be.lions.duckrace.domain;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import be.lions.duckrace.model.LotRange;

public class LotRangeOverlapsWithTest {

	private LotRange range1;
	private LotRange range2;
	
	@Before
	public void setup() {
		range1 = new LotRange();
		range2 = new LotRange();
	}
	
	@Test
	public void equalSingletonsDoOverlap() {
		setRanges(4, 4, 4, 4);
		assertTrue(range1.overlapsWith(range2));
		assertTrue(range2.overlapsWith(range1));
	}

	@Test
	public void chainedRangesDoNotOverlap() {
		setRanges(4, 5, 6, 8);
		assertFalse(range1.overlapsWith(range2));
		assertFalse(range2.overlapsWith(range1));
	}
	
	@Test
	public void overlappingRangesReturnTrue() {
		setRanges(4, 8, 6, 9);
		assertTrue(range1.overlapsWith(range2));
		assertTrue(range2.overlapsWith(range1));
	}
	
	@Test
	public void subsetRangesAreOverlapping() {
		setRanges(4, 8, 6, 7);
		assertTrue(range1.overlapsWith(range2));
		assertTrue(range2.overlapsWith(range1));
	}
	
	@Test
	public void noOverlappingRangesReturnFalse() {
		setRanges(1, 3, 6, 7);
		assertFalse(range1.overlapsWith(range2));
		assertFalse(range2.overlapsWith(range1));
	}
	
	private void setRanges(int from1, int to1, int from2, int to2) {
		range1.setFrom(from1);
		range1.setTo(to1);
		range2.setFrom(from2);
		range2.setTo(to2);
	}

}
