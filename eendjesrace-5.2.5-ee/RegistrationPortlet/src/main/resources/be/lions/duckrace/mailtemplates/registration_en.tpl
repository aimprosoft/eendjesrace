<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Dear Sir / Madam,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
You have registered a duck for $title.<br/>
Carefully keep the following information:
</p>
<ul style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Code: <b>$lot.code</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Verification code: <b>$lot.checksum</b></li>
	<li style="margin-bottom:0px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Duck number: <b>$lot.number</b></li>
</ul>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
An SMS message will be sent to <b>$lot.participant.formattedMobileNumber</b> if you are the winner.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Kind regards,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>