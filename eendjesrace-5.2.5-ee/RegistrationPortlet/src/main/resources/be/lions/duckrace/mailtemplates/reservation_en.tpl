<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
#set ($participant = $reservation.participant)
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Dear Sir / Madam,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Thank you for your registration for $title.<br/>
Registration details:
</p>
<ul style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Full name: <b>$participant.fullName</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Email: <b>$participant.emailAddress</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Mobile number: <b>$participant.formattedMobileNumber</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Number of ducks: <b>$reservation.numberLots</b></li>
	<li style="margin-bottom:0px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Price: <b>&euro; $reservation.totalCost</b></li>
</ul>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Please transfer the amount of <b>&euro; $reservation.totalCost</b> to the account <b>$accountNoBelgium</b> with the following notice: <b>EEND $reservation.reservationId</b>.</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
(IBAN code: $accountNoIban - Swift code: $accountNoSwift)
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
We regularly check the status of payments. When your payment is received correctly,
your registration will be approved and your ticket numbers will be sent to this email address.<br/>
Caution! If we have not received your payment within 2 weeks, the reservation will be canceled.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Kind regards,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>