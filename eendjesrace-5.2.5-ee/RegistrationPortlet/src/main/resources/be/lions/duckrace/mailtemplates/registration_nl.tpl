<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Beste $lot.participant.fullName,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
U heeft zonet een eendje geregistreerd voor de $title.<br/>
Houdt volgende gegevens zorgvuldig bij:
</p>
<ul style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Code: <b>$lot.code</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Controlecode: <b>$lot.checksum</b></li>
	<li style="margin-bottom:0px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Eendjesnummer: <b>$lot.number</b></li>
</ul>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Een SMS wordt gestuurd naar het nummer <b>$lot.participant.formattedMobileNumber</b> als u de winnaar bent.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Met vriendelijke groeten,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>