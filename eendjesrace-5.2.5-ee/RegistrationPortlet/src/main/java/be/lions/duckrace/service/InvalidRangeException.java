package be.lions.duckrace.service;

import be.lions.duckrace.model.LotRange;

public class InvalidRangeException extends Exception {

	private int max;
	private LotRange range;
	
	public InvalidRangeException(LotRange range, int max) {
		this.range = range;
		this.max = max;
	}
	
	@Override
	public String getMessage() {
		return String.format("Het interval %s moet cijfers tussen 1 en %d bevatten en het tweede cijfer moet hoger zijn dan het eerste", range, max);
	}
	
}
