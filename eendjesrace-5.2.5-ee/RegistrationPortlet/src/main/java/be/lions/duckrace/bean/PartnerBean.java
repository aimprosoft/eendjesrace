package be.lions.duckrace.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import be.lions.duckrace.LotsAlreadyAssignedToOtherPartnersException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.InternalRangeOverlapException;
import be.lions.duckrace.service.InvalidRangeException;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.LotRangeService;
import be.lions.duckrace.service.PartnerLocalServiceUtil;
import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.vo.PartnerVO;
import be.lions.duckrace.vo.PartnerVOConverter;

import com.icesoft.faces.component.ext.HtmlDataTable;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

public class PartnerBean {

	private LotRangeService lotRangeService = new LotRangeService();
	private PartnerVOConverter converter = new PartnerVOConverter();
	
	private List<PartnerVO> partners;
	private List<SelectItem> categories;
	private PartnerVO partner;
	private String category;
	private PartnerComparator partnerComparator;
	
	public List<PartnerVO> getPartners() {
		if(partners == null) {
			search();
		}
		Collections.sort(partners, partnerComparator);
		return partners;
	}
	
	public void search(ValueChangeEvent event) {
		category = (String) event.getNewValue();
		search();
	}
	
	public void search() {
		try {
			List<Partner> dbPartners = null;
			if(StringUtils.isBlank(category)) {
				dbPartners = PartnerLocalServiceUtil.getPartners();
			} else {
				dbPartners = PartnerLocalServiceUtil.getPartners(category);
			}
			partners = converter.fromPartners(dbPartners);
		} catch(SystemException e) {
			MessageUtil.unexpectedError(e);
		}
	}
	
	public String newPartner() {
		partner = new PartnerVO();
		return "edit-partner";
	}
	
	public String editPartner() {
		partner = getCurrentPartner();
		try {
			partner = converter.extendWithRangeInformation(partner);
			return "edit-partner";
		} catch (SystemException e) {
			MessageUtil.unexpectedError(e);
		} catch (PortalException e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String addRange() {
		partner.getAssignedRanges().add(new LotRange());
		return null;
	}
	
	public String deleteRange() {
		partner.getAssignedRanges().remove(getCurrentLotRange());
		return null;
	}
	
	public String save() {
		try {
			if(!partner.isAssignedValid()) {
				MessageUtil.error("Aantal teruggegeven lotjes moet groter of gelijk zijn aan aantal toegewezen lotjes.");
				return null;
			}
			int max = LotLocalServiceUtil.getPressedLotsCount();
			List<LotRange> lotRanges = partner.getAssignedRanges();
			lotRangeService.validate(lotRanges, max);
			lotRanges = lotRangeService.normalize(lotRanges);
			PartnerLocalServiceUtil.save(converter.fromPartnerVO(partner), lotRanges);
			MessageUtil.info("Partner werd succesvol opgeslagen!");
			return goBack();
		} catch (LotsAlreadyAssignedToOtherPartnersException e) {
			MessageUtil.error(e.getMessage());
		} catch (NoSuchLotException e) {
			MessageUtil.unexpectedError(e);
		} catch (InvalidRangeException e) {
			MessageUtil.error(e.getMessage());
		} catch (InternalRangeOverlapException e) {
			MessageUtil.error(e.getMessage());
		} catch (SystemException e) {
			MessageUtil.unexpectedError(e);
		} catch (PortalException e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String goBack() {
		clearAll();
		return "partners";
	}

	public PartnerVO getPartner() {
		return partner;
	}

	public void setPartner(PartnerVO partner) {
		this.partner = partner;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	private PartnerVO getCurrentPartner() {
		return (PartnerVO) partnerTable.getRowData();
	}
	
	private LotRange getCurrentLotRange() {
		return (LotRange) lotRangeTable.getRowData();
	}
	
	private HtmlDataTable lotRangeTable;
	
	public HtmlDataTable getLotRangeTable() {
		return lotRangeTable;
	}

	public void setLotRangeTable(HtmlDataTable lotRangeTable) {
		this.lotRangeTable = lotRangeTable;
	}

	private HtmlDataTable partnerTable;
	
	public HtmlDataTable getPartnerTable() {
		return partnerTable;
	}

	public void setPartnerTable(HtmlDataTable partnerTable) {
		this.partnerTable = partnerTable;
	}

	private void clearAll() {
		partner = null;
		partners = null;
		categories = null;
	}
	
	public List<SelectItem> getCategories() {
		if(categories == null) {
			categories = new ArrayList<SelectItem>();
			try {
				List<String> categoryNames = PartnerLocalServiceUtil.getCategories();
				for(String categoryName: categoryNames) {
					categories.add(new SelectItem(categoryName));
				}
			} catch (SystemException e) {
			}
		}
		return categories;
	}

	public PartnerComparator getPartnerComparator() {
		return partnerComparator;
	}

	public void setPartnerComparator(PartnerComparator partnerComparator) {
		this.partnerComparator = partnerComparator;
	}

}
