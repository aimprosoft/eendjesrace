package be.lions.duckrace.service;

import java.util.ArrayList;
import java.util.List;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotRange;

public class LotRangeService {

	public List<LotRange> getRanges(List<Lot> lots) {
		List<Integer> indices = new ArrayList<Integer>();
		for(Lot lot: lots) {
			indices.add(lot.getLotNumber());
		}
		return getLotRanges(indices);
	}
	
	private List<LotRange> getLotRanges(List<Integer> lotNumbers) {
		List<LotRange> ranges = new ArrayList<LotRange>();
		for(int i=0; i<lotNumbers.size(); i++) {
			int start = lotNumbers.get(i);
			int j = start;
			for(; i<lotNumbers.size() && j==lotNumbers.get(i); i++, j++){
				
			}
			int end = j-1;
			i--;
			LotRange range = new LotRange();
			range.setFrom(start);
			range.setTo(end);
			ranges.add(range);
		}
		return ranges;
	}
	
	public void validate(List<LotRange> ranges, int max) throws InternalRangeOverlapException, InvalidRangeException {
		for(LotRange range1: ranges) {
			if(!range1.isValid(max)) {
				throw new InvalidRangeException(range1, max);
			}
			for(LotRange range2: ranges) {
				if(!range1.equals(range2) && range1.overlapsWith(range2)) {
					throw new InternalRangeOverlapException(range1, range2);
				}
			}
		}
	}

	public List<Integer> getNumbers(List<LotRange> lotRanges) {
		List<Integer> result = new ArrayList<Integer>();
		for(LotRange range: lotRanges) {
			result.addAll(range.getIndices());
		}
		return result;
	}
	
	public List<LotRange> normalize(List<LotRange> lotRanges) {
		return getLotRanges(getNumbers(lotRanges));
	}

}
