package be.lions.duckrace.bean;

public class ContestBean {

	private boolean contestParticipant;
	private Boolean mercedesDriver;
	private String carType;
	private String otherCarType;
	private boolean mercedesOld;

	public boolean isMercedesDriverChosen() {
		return mercedesDriver != null;
	}
	
	public String getEffectiveCarType() {
		if(carType != null && carType.equals("Andere")) {
			return otherCarType;
		}
		return carType;
	}
	
	public boolean isEffectiveMercedesDriver() {
		return mercedesDriver != null && mercedesDriver;
	}
	
	public boolean isContestParticipant() {
		return contestParticipant;
	}

	public void setContestParticipant(boolean contestParticipant) {
		this.contestParticipant = contestParticipant;
	}

	public Boolean getMercedesDriver() {
		return mercedesDriver;
	}

	public void setMercedesDriver(Boolean mercedesDriver) {
		this.mercedesDriver = mercedesDriver;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getOtherCarType() {
		return otherCarType;
	}

	public void setOtherCarType(String otherCarType) {
		this.otherCarType = otherCarType;
	}

	public boolean isMercedesOld() {
		return mercedesOld;
	}

	public void setMercedesOld(boolean mercedesOld) {
		this.mercedesOld = mercedesOld;
	}

}
