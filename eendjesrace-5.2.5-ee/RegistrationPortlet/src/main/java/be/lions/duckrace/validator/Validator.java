package be.lions.duckrace.validator;

import be.lions.duckrace.util.StringUtils;

public class Validator {

	public static void validateRequired(String... params) throws RequiredException {
		for (String param : params) {
			if(StringUtils.isEmpty(param)) {
				throw new RequiredException();
			}
		}
	}
	
	public static void validateEmailAddress(String emailAddress) throws InvalidEmailAddressException {
		if(!StringUtils.isValidEmailAddress(emailAddress)) {
			throw new InvalidEmailAddressException();
		}
	}
	
	public static void validateMobileNumber(String mobileNumber) throws InvalidMobileNumberException {
		if(!StringUtils.isValidMobileNumber(mobileNumber)) {
			throw new InvalidMobileNumberException();
		}
	}

}
