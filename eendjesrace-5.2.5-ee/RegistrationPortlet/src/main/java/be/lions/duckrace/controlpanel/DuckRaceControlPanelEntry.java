package be.lions.duckrace.controlpanel;

import be.lions.duckrace.service.DuckRaceLocalServiceUtil;

import com.liferay.portal.model.Portlet;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portlet.BaseControlPanelEntry;

public class DuckRaceControlPanelEntry extends BaseControlPanelEntry {

	@Override
	public boolean isVisible(PermissionChecker pc, Portlet p) throws Exception {
		return pc.isCompanyAdmin() || pc.isOmniadmin() ||
			DuckRaceLocalServiceUtil.isLionsMember(pc.getUserId());
	}

}
