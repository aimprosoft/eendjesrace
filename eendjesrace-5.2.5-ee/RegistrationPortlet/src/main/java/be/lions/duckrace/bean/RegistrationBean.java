package be.lions.duckrace.bean;

import be.lions.duckrace.*;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.util.FacesUtil;
import be.lions.duckrace.util.MailUtil;
import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.util.StringUtils;
import be.lions.duckrace.validator.InvalidEmailAddressException;
import be.lions.duckrace.validator.InvalidMobileNumberException;
import be.lions.duckrace.validator.RequiredException;
import be.lions.duckrace.validator.Validator;
import com.liferay.portal.SystemException;

import java.util.Map;

import static be.lions.duckrace.service.DuckRaceLocalServiceUtil.*;

public class RegistrationBean {

	private ContestBean contestBean;
	
	public String toEnterCode() {
		return participantFieldsValid() ? "enter-code" : null;
	}
	
	public boolean isAdmin() {
		long userId = FacesUtil.getUserId();
		try {
			return DuckRaceLocalServiceUtil.isLionsMember(userId);
		} catch (Exception e) {
			MessageUtil.unexpectedError(e);
			return false;
		}
	}

	private boolean participantFieldsValid() {
		try {
			Validator.validateRequired(firstName, lastName, emailAddress, mobileNumber);
			Validator.validateEmailAddress(emailAddress);
			Validator.validateMobileNumber(mobileNumber);
			ParticipantLocalServiceUtil.getMatchingParticipant(getFormattedMobileNumber(), getTrimmedFirstName(), getTrimmedLastName(), getTrimmedEmailAddress());
			return true;
		} catch(RequiredException e) {
			MessageUtil.error(MessageUtil.getMessage("please_fill_out_all_required_fields"));
		} catch(InvalidEmailAddressException e) {
			MessageUtil.error(MessageUtil.getMessage("please_enter_a_valid_email_address"));
		} catch(InvalidMobileNumberException e) {
			MessageUtil.error(MessageUtil.getMessage("please_enter_a_valid_mobile_number"));
		} catch (MobileNumberAlreadyUsedInvalidDataException e) {
			MessageUtil.error(MessageUtil.getMessage("mobile_number_already_in_use"));
		} catch (MobileNumberAlreadyUsedPartlyValidDataException e) {
			Participant p = e.getParticipant();
			MessageUtil.error(MessageUtil.getMessage("mobile_number_already_in_use_with_other_details", p.getFullName(), p.getEmailAddress()));
		} catch(SystemException e) {
			MessageUtil.unexpectedError(e);
		}
		return false;
	}
	
	public String verifyCode() {
		try {
			boolean codeExists = LotLocalServiceUtil.codeExists(getCode());
			if(!codeExists) {
				MessageUtil.error(MessageUtil.getMessage("invalid_code_please_try_again"));
			} else {
				setCodeOk(true);
			}
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String verifyChecksum() {
		try {
			Lot lot = LotLocalServiceUtil.registerLot(
				getFormattedMobileNumber(), 
				getTrimmedFirstName(), 
				getTrimmedLastName(), 
				getTrimmedEmailAddress(),
				contestBean.isContestParticipant(),
				contestBean.isEffectiveMercedesDriver(),
				contestBean.getEffectiveCarType(),
				contestBean.isMercedesOld(),
				getCode(), getChecksum(),
				MessageUtil.getLocale().getLanguage());
			setChecksumOk(true);
			sendRegistrationMail(lot);
			Partner partner = lot.getPartner();
			String partnerThanks = "";
			if(partner != null) {
				partnerThanks = MessageUtil.getMessage("thank_you_for_your_contribution", partner.getName()) + "<br/>";
			}
			MessageUtil.info(MessageUtil.getMessage("lot_with_code_x_successfully_registered", getCode())+"<br/>"+partnerThanks+MessageUtil.getMessage("an_email_was_sent_with_registration_data"));
		} catch(InvalidChecksumException e) {
			MessageUtil.error(MessageUtil.getMessage("wrong_control_code"));
		} catch(LotLockoutException e) {
			MessageUtil.error(MessageUtil.getMessage("lot_was_blocked_try_again_later", getCode()));
		} catch(LotAlreadyRegisteredException e) {
			MessageUtil.error(MessageUtil.getMessage("lot_was_already_registered", getCode()));
		} catch(NoSuchLotException e) {
			MessageUtil.error(MessageUtil.getMessage("lot_was_already_registered", getCode()));
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	private void sendRegistrationMail(Lot lot) {
		try {
			String subject = MessageUtil.getMessage("registration")+" "+DuckRaceLocalServiceUtil.getSiteTitle();
			String content = MailUtil.getTplContent("registration", MessageUtil.getLocale().getLanguage());
			Map<String, Object> params = MailUtil.createParamMap(
				new String[] {"lot", "title", "url"}, 
				new Object[] {lot, DuckRaceLocalServiceUtil.getSiteTitle(), DuckRaceLocalServiceUtil.getHostName()});
			sendMail(FacesUtil.getUser(), content, params, subject, lot.getParticipant());
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
	}
	
	public String createReservation() {
		if(duckAmount <= 0) {
			MessageUtil.error("number_of_ducks_invalid");
			return null;
		}
		if(!participantFieldsValid()) {
			return null;
		}
		try {
			reservation = LotReservationLocalServiceUtil.createReservation(
				getFormattedMobileNumber(), 
				getTrimmedFirstName(), 
				getTrimmedLastName(), 
				getTrimmedEmailAddress(),
				contestBean.isContestParticipant(),
				contestBean.isEffectiveMercedesDriver(),
				contestBean.getEffectiveCarType(),
				contestBean.isMercedesOld(),
				getDuckAmount(), MessageUtil.getLocale().getLanguage());
			sendReservationMail();
			MessageUtil.info(MessageUtil.getMessage("reservation_successful"));
			return "show-account";
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}

	private void sendReservationMail() {
		try {
			String subject = MessageUtil.getMessage("your_reservation_for_x", DuckRaceLocalServiceUtil.getSiteTitle());
			String content = MailUtil.getTplContent("reservation", MessageUtil.getLocale().getLanguage());
			Map<String, Object> params = MailUtil.createParamMap(
				new String[] {"reservation", "accountNoIban", "accountNoBelgium", "accountNoSwift", "title", "url"}, 
				new Object[] {reservation, getAccountNoIban(), getAccountNoBelgium(), getAccountNoSwift(), DuckRaceLocalServiceUtil.getSiteTitle(), DuckRaceLocalServiceUtil.getHostName()});
			sendMail(FacesUtil.getUser(), content, params, subject, reservation.getParticipant());
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
	}

	public String goBack() {
		resetCodes();
		return "has-lot";
	}

	private void resetCodes() {
		setCodeOk(false);
		setCode(null);
		setChecksumOk(false);
		setChecksum(null);
	}
	
	public String registerAnother() {
		resetCodes();
		return null;
	}
	
	/**
	 * Helper properties
	 */
	
	private boolean codeOk;
	
	public boolean isCodeOk() {
		return codeOk;
	}

	public void setCodeOk(boolean codeOk) {
		this.codeOk = codeOk;
	}
	
	private boolean checksumOk;
	
	public boolean isChecksumOk() {
		return checksumOk;
	}

	public void setChecksumOk(boolean checksumOk) {
		this.checksumOk = checksumOk;
	}
	
	public String getFormattedMobileNumber() {
		return StringUtils.getStrippedMobileNumber(getMobileNumber());
	}
	
	public float getTotalCost() {
		return getDuckAmount() * getLotPrice();
	}

	/**
	 * Bean properties
	 */
	
	private LotReservation reservation;
	
	public ContestBean getContestBean() {
		return contestBean;
	}

	public void setContestBean(ContestBean contestBean) {
		this.contestBean = contestBean;
	}

	public LotReservation getReservation() {
		return reservation;
	}

	public void setReservation(LotReservation reservation) {
		this.reservation = reservation;
	}
	
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String mobileNumber;
	private String code;
	private String checksum;
	private int duckAmount;
	
	public String getTrimmedFirstName() {
		return StringUtils.trim(getFirstName());
	}
	
	public String getTrimmedLastName() {
		return StringUtils.trim(getLastName());
	}
	
	public String getTrimmedEmailAddress() {
		return StringUtils.trim(getEmailAddress());
	}
	
	public int getDuckAmount() {
		return duckAmount;
	}
	public void setDuckAmount(int duckAmount) {
		this.duckAmount = duckAmount;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code==null ? null : code.toUpperCase();
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
}
