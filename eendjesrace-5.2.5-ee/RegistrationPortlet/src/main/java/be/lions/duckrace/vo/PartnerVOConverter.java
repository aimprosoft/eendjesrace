package be.lions.duckrace.vo;

import java.util.ArrayList;
import java.util.List;

import be.lions.duckrace.NoSuchPartnerException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.LotRangeService;
import be.lions.duckrace.service.PartnerLocalServiceUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

public class PartnerVOConverter {

	private LotRangeService lotRangeService = new LotRangeService();
	
	public PartnerVO fromPartner(Partner partner) throws SystemException {
		PartnerVO result = new PartnerVO();
		result.setId(partner.getPartnerId());
		result.setName(partner.getName());
		result.setCategory(partner.getCategory());
		result.setNbAssigned(partner.getAssignedLotsCount());
		result.setNbRegistered(partner.getRegisteredLotsCount());
		result.setNbReturned(partner.getReturnedLotsCount());
		return result;
	}
	
	public PartnerVO extendWithRangeInformation(PartnerVO partnerVO) throws SystemException, PortalException {
		return fromPartnerWithRanges(fromPartnerVO(partnerVO));
	}
	
	public PartnerVO fromPartnerWithRanges(Partner partner) throws SystemException {
		PartnerVO result = fromPartner(partner);
		List<Lot> lots = partner.getAssignedLots();
		List<LotRange> ranges = lotRangeService.getRanges(lots);
		result.setAssignedRanges(ranges);
		return result;
	}
	
	public Partner fromPartnerVO(PartnerVO partnerVO) throws PortalException, SystemException {
		Partner partner = null;
		try {
			partner = PartnerLocalServiceUtil.getPartner(partnerVO.getId());
		} catch(NoSuchPartnerException e) {
			partner = PartnerLocalServiceUtil.createPartner();
		}
		partner.setName(partnerVO.getName());
		partner.setCategory(partnerVO.getCategory());
		partner.setReturnedLotsCount(partnerVO.getNbReturned());
		return partner;
	}
	
	public List<PartnerVO> fromPartners(List<Partner> partners) throws SystemException {
		List<PartnerVO> result = new ArrayList<PartnerVO>();
		for(Partner partner: partners) {
			result.add(fromPartnerWithRanges(partner));
		}
		return result;
	}
	
}
