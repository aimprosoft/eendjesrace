package be.lions.duckrace.service;

import java.util.Arrays;
import java.util.List;

import be.lions.duckrace.model.LotRange;

public class InternalRangeOverlapException extends Exception {

	private List<LotRange> clashingRanges;
	
	public InternalRangeOverlapException(LotRange range1, LotRange range2) {
		clashingRanges = Arrays.asList(range1, range2);
	}
	
	public List<LotRange> getClashingRanges() {
		return clashingRanges;
	}
	
	@Override
	public String getMessage() {
		return String.format("Intervallen %s en %s overlappen", clashingRanges.get(0), clashingRanges.get(1));
	}
	
}
