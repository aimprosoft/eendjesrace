package be.lions.duckrace.bean;

import java.util.List;

import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.util.StringUtils;

public class RegistrationQueryBean {

	private List<Integer> lotNumbers;
	
	public String findLotNumbers() {
		try {
			Participant p = ParticipantLocalServiceUtil.getParticipantByMobileNumber(getStrippedMobileNumber());
			lotNumbers = LotLocalServiceUtil.getLotNumbersForParticipant(p.getParticipantId());
		} catch(NoSuchParticipantException e) {
			MessageUtil.error(MessageUtil.getMessage("no_participant_with_that_mobile_number"));
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public List<Integer> getLotNumbers() {
		return lotNumbers;
	}
	
	/**
	 * Helper methods
	 */
	
	public String getStrippedMobileNumber() {
		return StringUtils.getStrippedMobileNumber(getMobileNumber());
	}
	
	public String getFormattedMobileNumber() {
		return StringUtils.getFormattedMobileNumber(getMobileNumber());
	}
	
	/**
	 * Bean properties
	 */
	
	private String mobileNumber;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
}
