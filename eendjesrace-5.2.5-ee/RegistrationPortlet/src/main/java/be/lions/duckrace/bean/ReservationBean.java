package be.lions.duckrace.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;
import be.lions.duckrace.util.FacesUtil;
import be.lions.duckrace.util.MailUtil;
import be.lions.duckrace.util.MessageUtil;

import com.icesoft.faces.component.ext.HtmlDataTable;
import com.liferay.portal.SystemException;

public class ReservationBean {

	private static final int NB_UNAPPROVED_PER_PAGE = 10;
	private static final int NB_APPROVED_PER_PAGE = 20;
	
	private List<LotReservation> approved;
	private List<LotReservation> unapproved;

	public List<LotReservation> getUnapprovedReservations() {
		if(approved == null) {
			initUnapprovedReservations();
		}
		return approved;
	}
	
	private void initUnapprovedReservations() {
		try {
			approved = LotReservationLocalServiceUtil.getUnapprovedReservations();
		} catch (SystemException e) {
			MessageUtil.unexpectedError(e);
			approved = Collections.emptyList();
		}
	}

	public List<LotReservation> getApprovedReservations() {
		if(unapproved == null) {
			initApprovedReservations();
		}
		return unapproved;
	}
	
	private void initApprovedReservations() {
		try {
			unapproved = LotReservationLocalServiceUtil.getApprovedReservations();
		} catch(SystemException e) {
			MessageUtil.unexpectedError(e);
			unapproved = Collections.emptyList();
		}
	}

	public int getNbUnapprovedPerPage() {
		return NB_UNAPPROVED_PER_PAGE;
	}
	
	public boolean isEnoughUnapproved() {
		return getUnapprovedReservations().size() > getNbUnapprovedPerPage();
	}
	
	public int getNbApprovedPerPage() {
		return NB_APPROVED_PER_PAGE;
	}
	
	public boolean isEnoughApproved() {
		return getApprovedReservations().size() > getNbApprovedPerPage();
	}
	
	public String approve() {
		try {
			LotReservation reservation = LotReservationLocalServiceUtil.approveReservation(getReservation().getReservationId());
			sendApprovalMail(reservation);
			MessageUtil.info("De reservatie van "+reservation.getParticipant().getFullName()+" ("+reservation.getNumberLots()+" lotjes) werd succesvol goedgekeurd.<br/>"+
				"Een e-mail werd gestuurd naar "+reservation.getParticipant().getEmailAddress()+" met de toegewezen codes.");
			reset();
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	private void sendApprovalMail(LotReservation reservation) {
		try {
			Participant to = reservation.getParticipant();
			String subject = MessageUtil.getTranslatedMessage(to.getLanguage(), "your_reservation_for_x", DuckRaceLocalServiceUtil.getSiteTitle());
			String content = MailUtil.getTplContent("approval-no-pdf", to.getLanguage());
			List<File> attachments = new ArrayList<File>();
			Map<String, Object> params = MailUtil.createParamMap(
				new String[] {"reservation", "title", "url"}, 
				new Object[] {reservation, DuckRaceLocalServiceUtil.getSiteTitle(), DuckRaceLocalServiceUtil.getHostName()});
			//TODO: maak hier constante van
			if(reservation.getNumberLots() >= 20) {
				content = MailUtil.getTplContent("approval-pdf", to.getLanguage());
				attachments.add(reservation.generatePdf());
			}
			DuckRaceLocalServiceUtil.sendMail(FacesUtil.getUser(), content, params, subject, to, attachments);
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
	}

	public String delete() {
		try {
			LotReservationLocalServiceUtil.deleteLotReservation(getReservation());
			MessageUtil.info("De reservatie werd succesvol verwijderd.");
			reset();
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	private void reset() {
		approved = null;
		unapproved = null;
	}

	/**
	 * Helper methods
	 */
	
	private LotReservation getReservation() {
		return (LotReservation) getDataTable().getRowData();
	}
	
	/**
	 * Binding
	 */
	
	private HtmlDataTable dataTable;

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	
}
