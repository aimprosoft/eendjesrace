package be.lions.duckrace.bean;

import java.util.Collections;
import java.util.List;

import com.icesoft.faces.component.ext.HtmlDataTable;

import be.lions.duckrace.DuplicateMobileNumberException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.util.StringUtils;
import be.lions.duckrace.validator.InvalidEmailAddressException;
import be.lions.duckrace.validator.InvalidMobileNumberException;
import be.lions.duckrace.validator.RequiredException;
import be.lions.duckrace.validator.Validator;

public class ParticipantsBean {

	private static final int NB_PARTICIPANTS_PER_PAGE = 15;
	
	public int getNbParticipantsPerPage() {
		return NB_PARTICIPANTS_PER_PAGE;
	}
	
	public boolean isEnoughParticipants() {
		return getParticipants().size() > getNbParticipantsPerPage();
	}
	
	private List<Participant> participants;
	
	public List<Participant> getParticipants() {
		if(participants == null) {
			initParticipants();
		}
		return participants;
	}

	private void initParticipants() {
		try {
			String mobileNumber = StringUtils.getStrippedMobileNumber(formattedMobileNumber);
			participants = ParticipantLocalServiceUtil.searchParticipants(mobileNumber, firstName, lastName, emailAddress);
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
			participants = Collections.emptyList();
		}
	}
	
	// Actions
	public String toParticipant() {
		Participant p = getCurrent();
		firstName = p.getFirstName();
		lastName = p.getLastName();
		formattedMobileNumber = p.getFormattedMobileNumber();
		emailAddress = p.getEmailAddress();
		id = p.getParticipantId();
		return "edit-participant";
	}
	
	public String save() {
		try {
			Validator.validateRequired(firstName, lastName, emailAddress, formattedMobileNumber);
			Validator.validateEmailAddress(emailAddress);
			Validator.validateMobileNumber(formattedMobileNumber);
			String strippedMobileNumber = StringUtils.getStrippedMobileNumber(formattedMobileNumber);
			ParticipantLocalServiceUtil.updateParticipant(id, strippedMobileNumber, emailAddress, firstName, lastName);
			MessageUtil.info("De deelnemer werd succesvol opgeslagen.");
		} catch(RequiredException e) {
			MessageUtil.error("Gelieve alle velden in te vullen.");
		} catch(InvalidEmailAddressException e) {
			MessageUtil.error("Het opgegeven e-mailadres is ongeldig. Gelieve een geldig e-mailadres in te voeren.");
		} catch(InvalidMobileNumberException e) {
			MessageUtil.error("Het opgegeven gsm-nummer is ongeldig. Gelieve een geldig gsm-nummer in te voeren.");
		} catch(DuplicateMobileNumberException e) {
			MessageUtil.error("Het opgegeven gsm-nummer is reeds aan een andere deelnemer verbonden.");
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String goBack() {
		reset();
		participants = null;
		return "participants";
	}
	
	public String search() {
		participants = null;
		MessageUtil.info("Zoekopdracht voltooid.");
		return null;
	}
	
	// Helpers
	private Participant getCurrent() {
		return (Participant) getTable().getRowData();
	}
	
	private HtmlDataTable table;
	
	public HtmlDataTable getTable() {
		return table;
	}

	public void setTable(HtmlDataTable table) {
		this.table = table;
	}
	
	public void reset() {
		firstName = null;
		lastName = null;
		formattedMobileNumber = null;
		emailAddress = null;
	}
	
	// Properties
	private long id;
	private String firstName;
	private String lastName;
	private String formattedMobileNumber;
	private String emailAddress;

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFormattedMobileNumber() {
		return formattedMobileNumber;
	}

	public void setFormattedMobileNumber(String formattedMobileNumber) {
		this.formattedMobileNumber = formattedMobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
}
