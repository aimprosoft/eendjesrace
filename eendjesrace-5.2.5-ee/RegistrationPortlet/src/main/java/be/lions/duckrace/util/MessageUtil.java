package be.lions.duckrace.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

public class MessageUtil {

	private static final Log LOGGER = LogFactoryUtil.getLog(MessageUtil.class);
	
	public static void info(String msg) {
		addMessage(FacesMessage.SEVERITY_INFO, msg);
	}
	
	public static void error(String msg) {
		addMessage(FacesMessage.SEVERITY_ERROR, msg);
	}

	private static void addMessage(Severity severity, String msg) {
		fc().addMessage(null, new FacesMessage(severity, msg, msg));
	}

	public static void unexpectedError(Exception e) {
		error(getMessage("unexpected_error"));
		LOGGER.error(e);
	}
	
	public static String getTranslatedMessage(String languageId, String key, Object... params) {
		return getMessage(new Locale(languageId), key, params);
	}
	
	public static String getMessage(String key, Object... params) {
		return getMessage(getLocale(), key, params);
	}
	
	public static String getMessage(Locale locale, String key, Object... params) {
		String result = "???" + key + "???";
		ResourceBundle rb = getMessageBundle(fc(), locale);
		if (rb == null) {
			return result;
		}
		try {
			result = rb.getString(key);
		} catch (MissingResourceException e) {
			return result;
		}
		if (params != null) {
			result = new MessageFormat(result, locale).format(params);
		}
		return result;
	}
	
	private static ResourceBundle getMessageBundle(FacesContext context, Locale locale) {
		Application application = context.getApplication();
		String baseName = application.getMessageBundle();
		try {
			return ResourceBundle.getBundle(baseName, locale);
		} catch (NullPointerException e) {
			if (LOGGER.isErrorEnabled()) {
				LOGGER.error("No message bundle defined in faces-config.xml", e);
			}
		} catch (MissingResourceException e) {
			if (LOGGER.isErrorEnabled()) {
				LOGGER.error(String.format("No resource bundle found for base name '%s'", baseName));
			}
		}
		return null;
	}
	
	public static Locale getLocale() {
		return getThemeDisplay().getLocale();
	}
	
	public static ThemeDisplay getThemeDisplay() {
		return (ThemeDisplay) ec().getRequestMap().get(WebKeys.THEME_DISPLAY);
	}
	
	private static ExternalContext ec() {
		return fc().getExternalContext();
	}

	private static FacesContext fc() {
		return FacesContext.getCurrentInstance();
	}

}
