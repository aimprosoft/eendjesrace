package be.aca.ticketrace;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class RandomCodeGenerator {
	
	private Random rand;
	
	private Set<String> generatedStrings;
	private Set<String> generatedNumbers;
	
	public RandomCodeGenerator() {
		rand = new SecureRandom();
		generatedStrings = new HashSet<String>();
		generatedNumbers = new HashSet<String>();
	}
	
	public String nextString(int length) {
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<length; i++) {
			sb.append((char)((int)(rand.nextDouble()*26)+97));
		}
		String result = sb.toString();
		if(generatedStrings.contains(result)) {
			System.err.println(result+" is duplicate! ("+generatedStrings.size()+")");
			return nextString(length);
		}
		generatedStrings.add(result);
		return result;
	}
	
	public String nextNumber(int length) {
		String result = "";
		for(int i=0; i<length; i++) {
			result += rand.nextInt(10);
		}
		generatedNumbers.add(result);
		return result;
	}
	
}
