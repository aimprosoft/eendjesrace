package be.lions.duckrace.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface PartnerPersistence extends BasePersistence {
    public void cacheResult(be.lions.duckrace.model.Partner partner);

    public void cacheResult(
        java.util.List<be.lions.duckrace.model.Partner> partners);

    public void clearCache();

    public be.lions.duckrace.model.Partner create(long partnerId);

    public be.lions.duckrace.model.Partner remove(long partnerId)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner remove(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(Partner partner, boolean merge)</code>.
     */
    public be.lions.duckrace.model.Partner update(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                partner the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when partner is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public be.lions.duckrace.model.Partner update(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner updateImpl(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner findByPrimaryKey(long partnerId)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner fetchByPrimaryKey(long partnerId)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category, int start, int end)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner findByCategory_First(
        java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner findByCategory_Last(
        java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner[] findByCategory_PrevAndNext(
        long partnerId, java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findAll(int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Partner> findAll(int start,
        int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeByCategory(java.lang.String category)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countByCategory(java.lang.String category)
        throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
