package be.lions.duckrace.service;


/**
 * <a href="LotReservationLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>be.lions.duckrace.service.LotReservationLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.LotReservationLocalService
 *
 */
public class LotReservationLocalServiceUtil {
    private static LotReservationLocalService _service;

    public static be.lions.duckrace.model.LotReservation addLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException {
        return getService().addLotReservation(lotReservation);
    }

    public static be.lions.duckrace.model.LotReservation createLotReservation(
        long reservationId) {
        return getService().createLotReservation(reservationId);
    }

    public static void deleteLotReservation(long reservationId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteLotReservation(reservationId);
    }

    public static void deleteLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException {
        getService().deleteLotReservation(lotReservation);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static be.lions.duckrace.model.LotReservation getLotReservation(
        long reservationId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getLotReservation(reservationId);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> getLotReservations(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getLotReservations(start, end);
    }

    public static int getLotReservationsCount()
        throws com.liferay.portal.SystemException {
        return getService().getLotReservationsCount();
    }

    public static be.lions.duckrace.model.LotReservation updateLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException {
        return getService().updateLotReservation(lotReservation);
    }

    public static be.lions.duckrace.model.LotReservation updateLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateLotReservation(lotReservation, merge);
    }

    public static be.lions.duckrace.model.LotReservation createReservation(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld, int numberLots,
        java.lang.String language)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getService()
                   .createReservation(mobileNumber, firstName, lastName,
            emailAddress, contestParticipant, mercedesDriver, carBrand,
            mercedesOld, numberLots, language);
    }

    public static be.lions.duckrace.model.LotReservation approveReservation(
        long reservationId)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.NoSuchLotException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().approveReservation(reservationId);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> getUnapprovedReservations()
        throws com.liferay.portal.SystemException {
        return getService().getUnapprovedReservations();
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> getApprovedReservations()
        throws com.liferay.portal.SystemException {
        return getService().getApprovedReservations();
    }

    public static LotReservationLocalService getService() {
        if (_service == null) {
            throw new RuntimeException("LotReservationLocalService is not set");
        }

        return _service;
    }

    public void setService(LotReservationLocalService service) {
        _service = service;
    }
}
