package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="DuckRaceLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.DuckRaceLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface DuckRaceLocalService {
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getCompanyId() throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.Company getCompany()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getHostName()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getSiteTitle()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getAdminId()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getGroupId()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<String> getTagsForArticle(java.lang.String articleId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getNbStars(java.lang.String articleId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    public void sendMail(java.lang.String content,
        java.util.Map<String, Object> params, java.lang.String subject,
        be.lions.duckrace.model.Participant to) throws java.lang.Exception;

    public void sendMail(com.liferay.portal.model.User registerer,
        java.lang.String content, java.util.Map<String, Object> params,
        java.lang.String subject, be.lions.duckrace.model.Participant to)
        throws java.lang.Exception;

    public void sendMail(com.liferay.portal.model.User registerer,
        java.lang.String content, java.util.Map<String, Object> params,
        java.lang.String subject, be.lions.duckrace.model.Participant to,
        java.util.List<java.io.File> attachments) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getProperty(java.lang.String key);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String[] getProperties(java.lang.String key);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getAccountNoIban();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getAccountNoBelgium();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.String getAccountNoSwift();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public float getLotPrice();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.liferay.portlet.imagegallery.model.IGFolder> getGalleryFolders()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.liferay.portlet.imagegallery.model.IGImage> getGalleryImages(
        com.liferay.portlet.imagegallery.model.IGFolder folder)
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isLionsMember(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isAdministrator(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isWebmaster(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getRoleId(java.lang.String roleName)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.Date getAdminRegistrationDeadline()
        throws java.text.ParseException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.Date getAdminReservationDeadline()
        throws java.text.ParseException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isRegistrationPossible(long userId, java.util.Date date)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException, java.text.ParseException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isReservationPossible(long userId, java.util.Date date)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException, java.text.ParseException;

    public java.util.List reverseList(java.util.List input);
}
