package be.lions.duckrace.service.persistence;

public class LotReservationUtil {
    private static LotReservationPersistence _persistence;

    public static void cacheResult(
        be.lions.duckrace.model.LotReservation lotReservation) {
        getPersistence().cacheResult(lotReservation);
    }

    public static void cacheResult(
        java.util.List<be.lions.duckrace.model.LotReservation> lotReservations) {
        getPersistence().cacheResult(lotReservations);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static be.lions.duckrace.model.LotReservation create(
        long reservationId) {
        return getPersistence().create(reservationId);
    }

    public static be.lions.duckrace.model.LotReservation remove(
        long reservationId)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(reservationId);
    }

    public static be.lions.duckrace.model.LotReservation remove(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(lotReservation);
    }

    /**
     * @deprecated Use <code>update(LotReservation lotReservation, boolean merge)</code>.
     */
    public static be.lions.duckrace.model.LotReservation update(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(lotReservation);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lotReservation the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lotReservation is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static be.lions.duckrace.model.LotReservation update(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(lotReservation, merge);
    }

    public static be.lions.duckrace.model.LotReservation updateImpl(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(lotReservation, merge);
    }

    public static be.lions.duckrace.model.LotReservation findByPrimaryKey(
        long reservationId)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(reservationId);
    }

    public static be.lions.duckrace.model.LotReservation fetchByPrimaryKey(
        long reservationId) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(reservationId);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved) throws com.liferay.portal.SystemException {
        return getPersistence().findByApproved(approved);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved, int start, int end)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByApproved(approved, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByApproved(approved, start, end, obc);
    }

    public static be.lions.duckrace.model.LotReservation findByApproved_First(
        boolean approved, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException {
        return getPersistence().findByApproved_First(approved, obc);
    }

    public static be.lions.duckrace.model.LotReservation findByApproved_Last(
        boolean approved, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException {
        return getPersistence().findByApproved_Last(approved, obc);
    }

    public static be.lions.duckrace.model.LotReservation[] findByApproved_PrevAndNext(
        long reservationId, boolean approved,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException {
        return getPersistence()
                   .findByApproved_PrevAndNext(reservationId, approved, obc);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<be.lions.duckrace.model.LotReservation> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeByApproved(boolean approved)
        throws com.liferay.portal.SystemException {
        getPersistence().removeByApproved(approved);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countByApproved(boolean approved)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByApproved(approved);
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static LotReservationPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(LotReservationPersistence persistence) {
        _persistence = persistence;
    }
}
