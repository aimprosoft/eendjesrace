package be.lions.duckrace.service.persistence;

public class LotUtil {
    private static LotPersistence _persistence;

    public static void cacheResult(be.lions.duckrace.model.Lot lot) {
        getPersistence().cacheResult(lot);
    }

    public static void cacheResult(
        java.util.List<be.lions.duckrace.model.Lot> lots) {
        getPersistence().cacheResult(lots);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static be.lions.duckrace.model.Lot create(java.lang.String code) {
        return getPersistence().create(code);
    }

    public static be.lions.duckrace.model.Lot remove(java.lang.String code)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(code);
    }

    public static be.lions.duckrace.model.Lot remove(
        be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(lot);
    }

    /**
     * @deprecated Use <code>update(Lot lot, boolean merge)</code>.
     */
    public static be.lions.duckrace.model.Lot update(
        be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(lot);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lot the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lot is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static be.lions.duckrace.model.Lot update(
        be.lions.duckrace.model.Lot lot, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(lot, merge);
    }

    public static be.lions.duckrace.model.Lot updateImpl(
        be.lions.duckrace.model.Lot lot, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(lot, merge);
    }

    public static be.lions.duckrace.model.Lot findByPrimaryKey(
        java.lang.String code)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(code);
    }

    public static be.lions.duckrace.model.Lot fetchByPrimaryKey(
        java.lang.String code) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(code);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId) throws com.liferay.portal.SystemException {
        return getPersistence().findByParticipant(participantId);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId, int start, int end)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByParticipant(participantId, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByParticipant(participantId, start, end, obc);
    }

    public static be.lions.duckrace.model.Lot findByParticipant_First(
        long participantId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByParticipant_First(participantId, obc);
    }

    public static be.lions.duckrace.model.Lot findByParticipant_Last(
        long participantId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByParticipant_Last(participantId, obc);
    }

    public static be.lions.duckrace.model.Lot[] findByParticipant_PrevAndNext(
        java.lang.String code, long participantId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence()
                   .findByParticipant_PrevAndNext(code, participantId, obc);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId) throws com.liferay.portal.SystemException {
        return getPersistence().findByReservation(reservationId);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId, int start, int end)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByReservation(reservationId, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByReservation(reservationId, start, end, obc);
    }

    public static be.lions.duckrace.model.Lot findByReservation_First(
        long reservationId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByReservation_First(reservationId, obc);
    }

    public static be.lions.duckrace.model.Lot findByReservation_Last(
        long reservationId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByReservation_Last(reservationId, obc);
    }

    public static be.lions.duckrace.model.Lot[] findByReservation_PrevAndNext(
        java.lang.String code, long reservationId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence()
                   .findByReservation_PrevAndNext(code, reservationId, obc);
    }

    public static be.lions.duckrace.model.Lot findByLotNumber(int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByLotNumber(lotNumber);
    }

    public static be.lions.duckrace.model.Lot fetchByLotNumber(int lotNumber)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByLotNumber(lotNumber);
    }

    public static be.lions.duckrace.model.Lot fetchByLotNumber(int lotNumber,
        boolean retrieveFromCache) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByLotNumber(lotNumber, retrieveFromCache);
    }

    public static be.lions.duckrace.model.Lot findByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByNumber(number);
    }

    public static be.lions.duckrace.model.Lot fetchByNumber(int number)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByNumber(number);
    }

    public static be.lions.duckrace.model.Lot fetchByNumber(int number,
        boolean retrieveFromCache) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByNumber(number, retrieveFromCache);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId) throws com.liferay.portal.SystemException {
        return getPersistence().findByPartner(partnerId);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId, int start, int end)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByPartner(partnerId, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByPartner(partnerId, start, end, obc);
    }

    public static be.lions.duckrace.model.Lot findByPartner_First(
        long partnerId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPartner_First(partnerId, obc);
    }

    public static be.lions.duckrace.model.Lot findByPartner_Last(
        long partnerId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPartner_Last(partnerId, obc);
    }

    public static be.lions.duckrace.model.Lot[] findByPartner_PrevAndNext(
        java.lang.String code, long partnerId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPartner_PrevAndNext(code, partnerId, obc);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeByParticipant(long participantId)
        throws com.liferay.portal.SystemException {
        getPersistence().removeByParticipant(participantId);
    }

    public static void removeByReservation(long reservationId)
        throws com.liferay.portal.SystemException {
        getPersistence().removeByReservation(reservationId);
    }

    public static void removeByLotNumber(int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        getPersistence().removeByLotNumber(lotNumber);
    }

    public static void removeByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        getPersistence().removeByNumber(number);
    }

    public static void removeByPartner(long partnerId)
        throws com.liferay.portal.SystemException {
        getPersistence().removeByPartner(partnerId);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countByParticipant(long participantId)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByParticipant(participantId);
    }

    public static int countByReservation(long reservationId)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByReservation(reservationId);
    }

    public static int countByLotNumber(int lotNumber)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByLotNumber(lotNumber);
    }

    public static int countByNumber(int number)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByNumber(number);
    }

    public static int countByPartner(long partnerId)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByPartner(partnerId);
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static LotPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(LotPersistence persistence) {
        _persistence = persistence;
    }
}
