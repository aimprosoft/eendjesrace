package be.lions.duckrace.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface LotReservationPersistence extends BasePersistence {
    public void cacheResult(
        be.lions.duckrace.model.LotReservation lotReservation);

    public void cacheResult(
        java.util.List<be.lions.duckrace.model.LotReservation> lotReservations);

    public void clearCache();

    public be.lions.duckrace.model.LotReservation create(long reservationId);

    public be.lions.duckrace.model.LotReservation remove(long reservationId)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation remove(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(LotReservation lotReservation, boolean merge)</code>.
     */
    public be.lions.duckrace.model.LotReservation update(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lotReservation the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lotReservation is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public be.lions.duckrace.model.LotReservation update(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation updateImpl(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation findByPrimaryKey(
        long reservationId)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation fetchByPrimaryKey(
        long reservationId) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved, int start, int end)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findByApproved(
        boolean approved, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation findByApproved_First(
        boolean approved, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation findByApproved_Last(
        boolean approved, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation[] findByApproved_PrevAndNext(
        long reservationId, boolean approved,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotReservationException,
            com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findAll(
        int start, int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.LotReservation> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeByApproved(boolean approved)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countByApproved(boolean approved)
        throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
