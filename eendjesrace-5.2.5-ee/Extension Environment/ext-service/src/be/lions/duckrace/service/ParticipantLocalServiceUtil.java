package be.lions.duckrace.service;


/**
 * <a href="ParticipantLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>be.lions.duckrace.service.ParticipantLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.ParticipantLocalService
 *
 */
public class ParticipantLocalServiceUtil {
    private static ParticipantLocalService _service;

    public static be.lions.duckrace.model.Participant addParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException {
        return getService().addParticipant(participant);
    }

    public static be.lions.duckrace.model.Participant createParticipant(
        long participantId) {
        return getService().createParticipant(participantId);
    }

    public static void deleteParticipant(long participantId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteParticipant(participantId);
    }

    public static void deleteParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException {
        getService().deleteParticipant(participant);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static be.lions.duckrace.model.Participant getParticipant(
        long participantId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getParticipant(participantId);
    }

    public static java.util.List<be.lions.duckrace.model.Participant> getParticipants(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getParticipants(start, end);
    }

    public static int getParticipantsCount()
        throws com.liferay.portal.SystemException {
        return getService().getParticipantsCount();
    }

    public static be.lions.duckrace.model.Participant updateParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException {
        return getService().updateParticipant(participant);
    }

    public static be.lions.duckrace.model.Participant updateParticipant(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateParticipant(participant, merge);
    }

    public static be.lions.duckrace.model.Participant getOrCreateParticipant(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld,
        java.lang.String language)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getService()
                   .getOrCreateParticipant(mobileNumber, firstName, lastName,
            emailAddress, contestParticipant, mercedesDriver, carBrand,
            mercedesOld, language);
    }

    public static be.lions.duckrace.model.Participant getMatchingParticipant(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress)
        throws be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            com.liferay.portal.SystemException {
        return getService()
                   .getMatchingParticipant(mobileNumber, firstName, lastName,
            emailAddress);
    }

    public static be.lions.duckrace.model.Participant getParticipantByMobileNumber(
        java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getService().getParticipantByMobileNumber(mobileNumber);
    }

    public static be.lions.duckrace.model.Participant getParticipantByLotNumber(
        int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getParticipantByLotNumber(lotNumber);
    }

    public static be.lions.duckrace.model.Participant updateParticipant(
        long id, java.lang.String mobileNumber, java.lang.String emailAddress,
        java.lang.String firstName, java.lang.String lastName)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService()
                   .updateParticipant(id, mobileNumber, emailAddress,
            firstName, lastName);
    }

    public static java.util.List<be.lions.duckrace.model.Participant> searchParticipants(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress)
        throws com.liferay.portal.SystemException {
        return getService()
                   .searchParticipants(mobileNumber, firstName, lastName,
            emailAddress);
    }

    public static ParticipantLocalService getService() {
        if (_service == null) {
            throw new RuntimeException("ParticipantLocalService is not set");
        }

        return _service;
    }

    public void setService(ParticipantLocalService service) {
        _service = service;
    }
}
