package be.lions.duckrace.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface LotPersistence extends BasePersistence {
    public void cacheResult(be.lions.duckrace.model.Lot lot);

    public void cacheResult(java.util.List<be.lions.duckrace.model.Lot> lots);

    public void clearCache();

    public be.lions.duckrace.model.Lot create(java.lang.String code);

    public be.lions.duckrace.model.Lot remove(java.lang.String code)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot remove(be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(Lot lot, boolean merge)</code>.
     */
    public be.lions.duckrace.model.Lot update(be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lot the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lot is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public be.lions.duckrace.model.Lot update(be.lions.duckrace.model.Lot lot,
        boolean merge) throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot updateImpl(
        be.lions.duckrace.model.Lot lot, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByPrimaryKey(java.lang.String code)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot fetchByPrimaryKey(java.lang.String code)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId, int start, int end)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByParticipant(
        long participantId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByParticipant_First(
        long participantId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByParticipant_Last(
        long participantId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot[] findByParticipant_PrevAndNext(
        java.lang.String code, long participantId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId, int start, int end)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByReservation(
        long reservationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByReservation_First(
        long reservationId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByReservation_Last(
        long reservationId, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot[] findByReservation_PrevAndNext(
        java.lang.String code, long reservationId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByLotNumber(int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot fetchByLotNumber(int lotNumber)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot fetchByLotNumber(int lotNumber,
        boolean retrieveFromCache) throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot fetchByNumber(int number)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot fetchByNumber(int number,
        boolean retrieveFromCache) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId, int start, int end)
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findByPartner(
        long partnerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByPartner_First(long partnerId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot findByPartner_Last(long partnerId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot[] findByPartner_PrevAndNext(
        java.lang.String code, long partnerId,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findAll(int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> findAll(int start,
        int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeByParticipant(long participantId)
        throws com.liferay.portal.SystemException;

    public void removeByReservation(long reservationId)
        throws com.liferay.portal.SystemException;

    public void removeByLotNumber(int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public void removeByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    public void removeByPartner(long partnerId)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countByParticipant(long participantId)
        throws com.liferay.portal.SystemException;

    public int countByReservation(long reservationId)
        throws com.liferay.portal.SystemException;

    public int countByLotNumber(int lotNumber)
        throws com.liferay.portal.SystemException;

    public int countByNumber(int number)
        throws com.liferay.portal.SystemException;

    public int countByPartner(long partnerId)
        throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
