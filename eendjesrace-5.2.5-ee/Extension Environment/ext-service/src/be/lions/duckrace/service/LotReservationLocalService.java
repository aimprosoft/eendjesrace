package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="LotReservationLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>be.lions.duckrace.service.impl.LotReservationLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.LotReservationLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface LotReservationLocalService {
    public be.lions.duckrace.model.LotReservation addLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation createLotReservation(
        long reservationId);

    public void deleteLotReservation(long reservationId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deleteLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.LotReservation getLotReservation(
        long reservationId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.LotReservation> getLotReservations(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getLotReservationsCount()
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation updateLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation updateLotReservation(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation createReservation(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld, int numberLots,
        java.lang.String language)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.LotReservation approveReservation(
        long reservationId)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.NoSuchLotException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.LotReservation> getUnapprovedReservations()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.LotReservation> getApprovedReservations()
        throws com.liferay.portal.SystemException;
}
