package be.lions.duckrace.service;


/**
 * <a href="LotLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>be.lions.duckrace.service.LotLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.LotLocalService
 *
 */
public class LotLocalServiceUtil {
    private static LotLocalService _service;

    public static be.lions.duckrace.model.Lot addLot(
        be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException {
        return getService().addLot(lot);
    }

    public static be.lions.duckrace.model.Lot createLot(java.lang.String code) {
        return getService().createLot(code);
    }

    public static void deleteLot(java.lang.String code)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteLot(code);
    }

    public static void deleteLot(be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException {
        getService().deleteLot(lot);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static be.lions.duckrace.model.Lot getLot(java.lang.String code)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getLot(code);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> getLots(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getLots(start, end);
    }

    public static int getLotsCount() throws com.liferay.portal.SystemException {
        return getService().getLotsCount();
    }

    public static be.lions.duckrace.model.Lot updateLot(
        be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException {
        return getService().updateLot(lot);
    }

    public static be.lions.duckrace.model.Lot updateLot(
        be.lions.duckrace.model.Lot lot, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateLot(lot, merge);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> getLotsForPartner(long partnerId)
        throws com.liferay.portal.SystemException {
        return getService().getLotsForPartner(partnerId);
    }

    public static be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        java.lang.String orderNumber, java.lang.String language)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService()
                   .registerLot(mobileNumber, firstName, lastName,
            emailAddress, orderNumber, language);
    }

    public static int getLotsForPartnerCount(long partnerId)
        throws com.liferay.portal.SystemException {
        return getService().getLotsForPartnerCount(partnerId);
    }

    public static int getRegisteredLotsForPartnerCount(long partnerId)
        throws com.liferay.portal.SystemException {
        return getService().getRegisteredLotsForPartnerCount(partnerId);
    }

    public static void assignLotsToPartner(
        be.lions.duckrace.model.Partner partner,
        java.util.List<be.lions.duckrace.model.LotRange> lotRanges)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().assignLotsToPartner(partner, lotRanges);
    }

    public static int getPressedLotsCount()
        throws com.liferay.portal.SystemException {
        return getService().getPressedLotsCount();
    }

    public static be.lions.duckrace.model.Lot createLot(java.lang.String code,
        java.lang.String checksum) throws com.liferay.portal.SystemException {
        return getService().createLot(code, checksum);
    }

    public static boolean codeExists(java.lang.String code)
        throws com.liferay.portal.SystemException {
        return getService().codeExists(code);
    }

    public static be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().registerLot(mobileNumber);
    }

    public static int getCurrentNumber()
        throws com.liferay.portal.SystemException {
        return getService().getCurrentNumber();
    }

    public static be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld, java.lang.String code,
        java.lang.String checksum, java.lang.String language)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService()
                   .registerLot(mobileNumber, firstName, lastName,
            emailAddress, contestParticipant, mercedesDriver, carBrand,
            mercedesOld, code, checksum, language);
    }

    public static be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String code,
        java.lang.String checksum)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().registerLot(mobileNumber, code, checksum);
    }

    public static be.lions.duckrace.model.Lot getAvailableLot(boolean pressed)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getService().getAvailableLot(pressed);
    }

    public static java.util.List<Integer> getLotNumbersForParticipant(
        long participantId) throws com.liferay.portal.SystemException {
        return getService().getLotNumbersForParticipant(participantId);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> getLotsForParticipant(long participantId)
        throws com.liferay.portal.SystemException {
        return getService().getLotsForParticipant(participantId);
    }

    public static java.util.List<be.lions.duckrace.model.Lot> getLotsForReservation(long reservationId)
        throws com.liferay.portal.SystemException {
        return getService().getLotsForReservation(reservationId);
    }

    public static void assignFreeLotToParticipant(
        be.lions.duckrace.model.LotReservation reservation)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().assignFreeLotToParticipant(reservation);
    }

    public static be.lions.duckrace.model.Lot getLotByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException {
        return getService().getLotByNumber(number);
    }

    public static int getShortSmsLotCount()
        throws com.liferay.portal.SystemException {
        return getService().getShortSmsLotCount();
    }

    public static int getLongSmsLotCount()
        throws com.liferay.portal.SystemException {
        return getService().getLongSmsLotCount();
    }

    public static int getWebsiteRegistrationLotCount()
        throws com.liferay.portal.SystemException {
        return getService().getWebsiteRegistrationLotCount();
    }

    public static int getRegisteredLotCount()
        throws com.liferay.portal.SystemException {
        return getService().getRegisteredLotCount();
    }

    public static int getWebsiteReservationLotCount()
        throws com.liferay.portal.SystemException {
        return getService().getWebsiteReservationLotCount();
    }

    public static LotLocalService getService() {
        if (_service == null) {
            throw new RuntimeException("LotLocalService is not set");
        }

        return _service;
    }

    public void setService(LotLocalService service) {
        _service = service;
    }
}
