package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="PartnerLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>be.lions.duckrace.service.impl.PartnerLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.PartnerLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface PartnerLocalService {
    public be.lions.duckrace.model.Partner addPartner(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner createPartner(long partnerId);

    public void deletePartner(long partnerId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deletePartner(be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Partner getPartner(long partnerId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Partner> getPartners(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getPartnersCount() throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner updatePartner(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner updatePartner(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner createPartner()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Partner> getPartners(
        java.lang.String category) throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner save(
        be.lions.duckrace.model.Partner partner,
        java.util.List<be.lions.duckrace.model.LotRange> lotRanges)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Partner> getPartners()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<String> getCategories()
        throws com.liferay.portal.SystemException;
}
