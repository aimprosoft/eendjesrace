package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="LotLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>be.lions.duckrace.service.impl.LotLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.LotLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface LotLocalService {
    public be.lions.duckrace.model.Lot addLot(be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot createLot(java.lang.String code);

    public void deleteLot(java.lang.String code)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deleteLot(be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Lot getLot(java.lang.String code)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Lot> getLots(int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getLotsCount() throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot updateLot(
        be.lions.duckrace.model.Lot lot)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot updateLot(
        be.lions.duckrace.model.Lot lot, boolean merge)
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Lot> getLotsForPartner(long partnerId)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        java.lang.String orderNumber, java.lang.String language)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getLotsForPartnerCount(long partnerId)
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getRegisteredLotsForPartnerCount(long partnerId)
        throws com.liferay.portal.SystemException;

    public void assignLotsToPartner(be.lions.duckrace.model.Partner partner,
        java.util.List<be.lions.duckrace.model.LotRange> lotRanges)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getPressedLotsCount() throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot createLot(java.lang.String code,
        java.lang.String checksum) throws com.liferay.portal.SystemException;

    public boolean codeExists(java.lang.String code)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getCurrentNumber() throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld, java.lang.String code,
        java.lang.String checksum, java.lang.String language)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Lot registerLot(
        java.lang.String mobileNumber, java.lang.String code,
        java.lang.String checksum)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Lot getAvailableLot(boolean pressed)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<Integer> getLotNumbersForParticipant(
        long participantId) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Lot> getLotsForParticipant(long participantId)
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Lot> getLotsForReservation(long reservationId)
        throws com.liferay.portal.SystemException;

    public void assignFreeLotToParticipant(
        be.lions.duckrace.model.LotReservation reservation)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Lot getLotByNumber(int number)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getShortSmsLotCount() throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getLongSmsLotCount() throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getWebsiteRegistrationLotCount()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getRegisteredLotCount()
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getWebsiteReservationLotCount()
        throws com.liferay.portal.SystemException;
}
