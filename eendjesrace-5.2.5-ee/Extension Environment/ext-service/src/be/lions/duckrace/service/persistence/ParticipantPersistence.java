package be.lions.duckrace.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface ParticipantPersistence extends BasePersistence {
    public void cacheResult(be.lions.duckrace.model.Participant participant);

    public void cacheResult(
        java.util.List<be.lions.duckrace.model.Participant> participants);

    public void clearCache();

    public be.lions.duckrace.model.Participant create(long participantId);

    public be.lions.duckrace.model.Participant remove(long participantId)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant remove(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(Participant participant, boolean merge)</code>.
     */
    public be.lions.duckrace.model.Participant update(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                participant the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when participant is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public be.lions.duckrace.model.Participant update(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant updateImpl(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant findByPrimaryKey(
        long participantId)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant fetchByPrimaryKey(
        long participantId) throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant findByMobileNumber(
        java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant fetchByMobileNumber(
        java.lang.String mobileNumber)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant fetchByMobileNumber(
        java.lang.String mobileNumber, boolean retrieveFromCache)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Participant> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Participant> findAll(
        int start, int end) throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Participant> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeByMobileNumber(java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countByMobileNumber(java.lang.String mobileNumber)
        throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
