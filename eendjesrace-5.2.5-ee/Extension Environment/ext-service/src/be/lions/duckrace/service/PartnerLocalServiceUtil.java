package be.lions.duckrace.service;


/**
 * <a href="PartnerLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>be.lions.duckrace.service.PartnerLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.PartnerLocalService
 *
 */
public class PartnerLocalServiceUtil {
    private static PartnerLocalService _service;

    public static be.lions.duckrace.model.Partner addPartner(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException {
        return getService().addPartner(partner);
    }

    public static be.lions.duckrace.model.Partner createPartner(long partnerId) {
        return getService().createPartner(partnerId);
    }

    public static void deletePartner(long partnerId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletePartner(partnerId);
    }

    public static void deletePartner(be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException {
        getService().deletePartner(partner);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static be.lions.duckrace.model.Partner getPartner(long partnerId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getPartner(partnerId);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> getPartners(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getPartners(start, end);
    }

    public static int getPartnersCount()
        throws com.liferay.portal.SystemException {
        return getService().getPartnersCount();
    }

    public static be.lions.duckrace.model.Partner updatePartner(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException {
        return getService().updatePartner(partner);
    }

    public static be.lions.duckrace.model.Partner updatePartner(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatePartner(partner, merge);
    }

    public static be.lions.duckrace.model.Partner createPartner()
        throws com.liferay.portal.SystemException {
        return getService().createPartner();
    }

    public static java.util.List<be.lions.duckrace.model.Partner> getPartners(
        java.lang.String category) throws com.liferay.portal.SystemException {
        return getService().getPartners(category);
    }

    public static be.lions.duckrace.model.Partner save(
        be.lions.duckrace.model.Partner partner,
        java.util.List<be.lions.duckrace.model.LotRange> lotRanges)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().save(partner, lotRanges);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> getPartners()
        throws com.liferay.portal.SystemException {
        return getService().getPartners();
    }

    public static java.util.List<String> getCategories()
        throws com.liferay.portal.SystemException {
        return getService().getCategories();
    }

    public static PartnerLocalService getService() {
        if (_service == null) {
            throw new RuntimeException("PartnerLocalService is not set");
        }

        return _service;
    }

    public void setService(PartnerLocalService service) {
        _service = service;
    }
}
