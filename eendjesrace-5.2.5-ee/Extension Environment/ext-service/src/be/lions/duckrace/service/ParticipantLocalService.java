package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="ParticipantLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>be.lions.duckrace.service.impl.ParticipantLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.ParticipantLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface ParticipantLocalService {
    public be.lions.duckrace.model.Participant addParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant createParticipant(
        long participantId);

    public void deleteParticipant(long participantId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deleteParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Participant getParticipant(
        long participantId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Participant> getParticipants(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getParticipantsCount() throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant updateParticipant(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant updateParticipant(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Participant getOrCreateParticipant(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress,
        boolean contestParticipant, boolean mercedesDriver,
        java.lang.String carBrand, boolean mercedesOld,
        java.lang.String language)
        throws be.lions.duckrace.InvalidMobileNumberException,
            be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Participant getMatchingParticipant(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress)
        throws be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException,
            be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Participant getParticipantByMobileNumber(
        java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public be.lions.duckrace.model.Participant getParticipantByLotNumber(
        int lotNumber)
        throws be.lions.duckrace.NoSuchLotException,
            com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Participant updateParticipant(long id,
        java.lang.String mobileNumber, java.lang.String emailAddress,
        java.lang.String firstName, java.lang.String lastName)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<be.lions.duckrace.model.Participant> searchParticipants(
        java.lang.String mobileNumber, java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress)
        throws com.liferay.portal.SystemException;
}
