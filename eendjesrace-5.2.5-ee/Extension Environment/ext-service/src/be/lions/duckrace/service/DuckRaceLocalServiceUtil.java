package be.lions.duckrace.service;


/**
 * <a href="DuckRaceLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>be.lions.duckrace.service.DuckRaceLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.DuckRaceLocalService
 *
 */
public class DuckRaceLocalServiceUtil {
    private static DuckRaceLocalService _service;

    public static long getCompanyId() throws com.liferay.portal.SystemException {
        return getService().getCompanyId();
    }

    public static com.liferay.portal.model.Company getCompany()
        throws com.liferay.portal.SystemException {
        return getService().getCompany();
    }

    public static java.lang.String getHostName()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getHostName();
    }

    public static java.lang.String getSiteTitle()
        throws com.liferay.portal.SystemException {
        return getService().getSiteTitle();
    }

    public static long getAdminId()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getAdminId();
    }

    public static long getGroupId()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getGroupId();
    }

    public static java.util.List<String> getTagsForArticle(
        java.lang.String articleId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getTagsForArticle(articleId);
    }

    public static int getNbStars(java.lang.String articleId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getNbStars(articleId);
    }

    public static void sendMail(java.lang.String content,
        java.util.Map<String, Object> params, java.lang.String subject,
        be.lions.duckrace.model.Participant to) throws java.lang.Exception {
        getService().sendMail(content, params, subject, to);
    }

    public static void sendMail(com.liferay.portal.model.User registerer,
        java.lang.String content, java.util.Map<String, Object> params,
        java.lang.String subject, be.lions.duckrace.model.Participant to)
        throws java.lang.Exception {
        getService().sendMail(registerer, content, params, subject, to);
    }

    public static void sendMail(com.liferay.portal.model.User registerer,
        java.lang.String content, java.util.Map<String, Object> params,
        java.lang.String subject, be.lions.duckrace.model.Participant to,
        java.util.List<java.io.File> attachments) throws java.lang.Exception {
        getService()
            .sendMail(registerer, content, params, subject, to, attachments);
    }

    public static java.lang.String getProperty(java.lang.String key) {
        return getService().getProperty(key);
    }

    public static java.lang.String[] getProperties(java.lang.String key) {
        return getService().getProperties(key);
    }

    public static java.lang.String getAccountNoIban() {
        return getService().getAccountNoIban();
    }

    public static java.lang.String getAccountNoBelgium() {
        return getService().getAccountNoBelgium();
    }

    public static java.lang.String getAccountNoSwift() {
        return getService().getAccountNoSwift();
    }

    public static float getLotPrice() {
        return getService().getLotPrice();
    }

    public static java.util.List<com.liferay.portlet.imagegallery.model.IGFolder> getGalleryFolders()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getGalleryFolders();
    }

    public static java.util.List<com.liferay.portlet.imagegallery.model.IGImage> getGalleryImages(
        com.liferay.portlet.imagegallery.model.IGFolder folder)
        throws com.liferay.portal.SystemException {
        return getService().getGalleryImages(folder);
    }

    public static boolean isLionsMember(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().isLionsMember(userId);
    }

    public static boolean isAdministrator(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().isAdministrator(userId);
    }

    public static boolean isWebmaster(long userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().isWebmaster(userId);
    }

    public static long getRoleId(java.lang.String roleName)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getRoleId(roleName);
    }

    public static java.util.Date getAdminRegistrationDeadline()
        throws java.text.ParseException {
        return getService().getAdminRegistrationDeadline();
    }

    public static java.util.Date getAdminReservationDeadline()
        throws java.text.ParseException {
        return getService().getAdminReservationDeadline();
    }

    public static boolean isRegistrationPossible(long userId,
        java.util.Date date)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException, java.text.ParseException {
        return getService().isRegistrationPossible(userId, date);
    }

    public static boolean isReservationPossible(long userId, java.util.Date date)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException, java.text.ParseException {
        return getService().isReservationPossible(userId, date);
    }

    public static java.util.List reverseList(java.util.List input) {
        return getService().reverseList(input);
    }

    public static DuckRaceLocalService getService() {
        if (_service == null) {
            throw new RuntimeException("DuckRaceLocalService is not set");
        }

        return _service;
    }

    public void setService(DuckRaceLocalService service) {
        _service = service;
    }
}
