package be.lions.duckrace.service.persistence;

public class ParticipantUtil {
    private static ParticipantPersistence _persistence;

    public static void cacheResult(
        be.lions.duckrace.model.Participant participant) {
        getPersistence().cacheResult(participant);
    }

    public static void cacheResult(
        java.util.List<be.lions.duckrace.model.Participant> participants) {
        getPersistence().cacheResult(participants);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static be.lions.duckrace.model.Participant create(long participantId) {
        return getPersistence().create(participantId);
    }

    public static be.lions.duckrace.model.Participant remove(long participantId)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(participantId);
    }

    public static be.lions.duckrace.model.Participant remove(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(participant);
    }

    /**
     * @deprecated Use <code>update(Participant participant, boolean merge)</code>.
     */
    public static be.lions.duckrace.model.Participant update(
        be.lions.duckrace.model.Participant participant)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(participant);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                participant the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when participant is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static be.lions.duckrace.model.Participant update(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(participant, merge);
    }

    public static be.lions.duckrace.model.Participant updateImpl(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(participant, merge);
    }

    public static be.lions.duckrace.model.Participant findByPrimaryKey(
        long participantId)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(participantId);
    }

    public static be.lions.duckrace.model.Participant fetchByPrimaryKey(
        long participantId) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(participantId);
    }

    public static be.lions.duckrace.model.Participant findByMobileNumber(
        java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        return getPersistence().findByMobileNumber(mobileNumber);
    }

    public static be.lions.duckrace.model.Participant fetchByMobileNumber(
        java.lang.String mobileNumber)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByMobileNumber(mobileNumber);
    }

    public static be.lions.duckrace.model.Participant fetchByMobileNumber(
        java.lang.String mobileNumber, boolean retrieveFromCache)
        throws com.liferay.portal.SystemException {
        return getPersistence()
                   .fetchByMobileNumber(mobileNumber, retrieveFromCache);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Participant> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<be.lions.duckrace.model.Participant> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Participant> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeByMobileNumber(java.lang.String mobileNumber)
        throws be.lions.duckrace.NoSuchParticipantException,
            com.liferay.portal.SystemException {
        getPersistence().removeByMobileNumber(mobileNumber);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countByMobileNumber(java.lang.String mobileNumber)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByMobileNumber(mobileNumber);
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static ParticipantPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(ParticipantPersistence persistence) {
        _persistence = persistence;
    }
}
