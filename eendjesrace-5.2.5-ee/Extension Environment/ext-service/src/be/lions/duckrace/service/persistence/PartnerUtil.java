package be.lions.duckrace.service.persistence;

public class PartnerUtil {
    private static PartnerPersistence _persistence;

    public static void cacheResult(be.lions.duckrace.model.Partner partner) {
        getPersistence().cacheResult(partner);
    }

    public static void cacheResult(
        java.util.List<be.lions.duckrace.model.Partner> partners) {
        getPersistence().cacheResult(partners);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static be.lions.duckrace.model.Partner create(long partnerId) {
        return getPersistence().create(partnerId);
    }

    public static be.lions.duckrace.model.Partner remove(long partnerId)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(partnerId);
    }

    public static be.lions.duckrace.model.Partner remove(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(partner);
    }

    /**
     * @deprecated Use <code>update(Partner partner, boolean merge)</code>.
     */
    public static be.lions.duckrace.model.Partner update(
        be.lions.duckrace.model.Partner partner)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(partner);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                partner the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when partner is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static be.lions.duckrace.model.Partner update(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(partner, merge);
    }

    public static be.lions.duckrace.model.Partner updateImpl(
        be.lions.duckrace.model.Partner partner, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(partner, merge);
    }

    public static be.lions.duckrace.model.Partner findByPrimaryKey(
        long partnerId)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(partnerId);
    }

    public static be.lions.duckrace.model.Partner fetchByPrimaryKey(
        long partnerId) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(partnerId);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category) throws com.liferay.portal.SystemException {
        return getPersistence().findByCategory(category);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category, int start, int end)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByCategory(category, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findByCategory(
        java.lang.String category, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findByCategory(category, start, end, obc);
    }

    public static be.lions.duckrace.model.Partner findByCategory_First(
        java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException {
        return getPersistence().findByCategory_First(category, obc);
    }

    public static be.lions.duckrace.model.Partner findByCategory_Last(
        java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException {
        return getPersistence().findByCategory_Last(category, obc);
    }

    public static be.lions.duckrace.model.Partner[] findByCategory_PrevAndNext(
        long partnerId, java.lang.String category,
        com.liferay.portal.kernel.util.OrderByComparator obc)
        throws be.lions.duckrace.NoSuchPartnerException,
            com.liferay.portal.SystemException {
        return getPersistence()
                   .findByCategory_PrevAndNext(partnerId, category, obc);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<be.lions.duckrace.model.Partner> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeByCategory(java.lang.String category)
        throws com.liferay.portal.SystemException {
        getPersistence().removeByCategory(category);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countByCategory(java.lang.String category)
        throws com.liferay.portal.SystemException {
        return getPersistence().countByCategory(category);
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static PartnerPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(PartnerPersistence persistence) {
        _persistence = persistence;
    }
}
