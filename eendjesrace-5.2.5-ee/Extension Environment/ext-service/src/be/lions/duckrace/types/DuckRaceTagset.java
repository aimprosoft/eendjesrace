package be.lions.duckrace.types;

import be.lions.duckrace.util.StringUtils;

public enum DuckRaceTagset {

	SPONSOR;
	
	public String getName() {
		return StringUtils.fromEnumToName(name());
	}
	
}
