package be.lions.duckrace.types;


public enum DuckRaceRole {

	WEBMASTER("Webmaster"), 
	LIONS_MEMBER("Lions Lid");
	
	private String name;
	
	private DuckRaceRole(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}
