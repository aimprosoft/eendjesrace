package be.lions.duckrace.types;

import be.lions.duckrace.util.StringUtils;

public enum DuckRaceTag {

	MAIN_SPONSOR(0), 
	SPONSOR_4_STARS(4), 
	SPONSOR_3_STARS(3), 
	SPONSOR_2_STARS(2),
	SPONSOR_1_STAR(1),
	MERCHANT(0);
	
	private int stars;
	
	private DuckRaceTag(int stars) {
		this.stars = stars;
	}
	
	private int getStars() {
		return stars;
	}
	
	public String getName() {
		return StringUtils.fromEnumToName(name());
	}
	
	public static int getStarsForTag(String tagName) {
		try {
			return valueOf(StringUtils.fromNameToEnum(tagName)).getStars();
		} catch(Exception e) {
			return 0;
		}
	}
	
}
