package be.lions.duckrace.types;

import be.lions.duckrace.util.StringUtils;

public enum DuckRaceFolder {

	ALBUM;
	
	public String getName() {
		return StringUtils.lowerCaseFirstCapital(name());
	}
	
}
