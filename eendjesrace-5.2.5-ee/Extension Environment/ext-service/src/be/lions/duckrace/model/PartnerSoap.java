package be.lions.duckrace.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="PartnerSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>be.lions.duckrace.service.http.PartnerServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.http.PartnerServiceSoap
 *
 */
public class PartnerSoap implements Serializable {
    private long _partnerId;
    private String _category;
    private String _name;
    private int _returnedLotsCount;

    public PartnerSoap() {
    }

    public static PartnerSoap toSoapModel(Partner model) {
        PartnerSoap soapModel = new PartnerSoap();

        soapModel.setPartnerId(model.getPartnerId());
        soapModel.setCategory(model.getCategory());
        soapModel.setName(model.getName());
        soapModel.setReturnedLotsCount(model.getReturnedLotsCount());

        return soapModel;
    }

    public static PartnerSoap[] toSoapModels(Partner[] models) {
        PartnerSoap[] soapModels = new PartnerSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PartnerSoap[][] toSoapModels(Partner[][] models) {
        PartnerSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PartnerSoap[models.length][models[0].length];
        } else {
            soapModels = new PartnerSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PartnerSoap[] toSoapModels(List<Partner> models) {
        List<PartnerSoap> soapModels = new ArrayList<PartnerSoap>(models.size());

        for (Partner model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PartnerSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _partnerId;
    }

    public void setPrimaryKey(long pk) {
        setPartnerId(pk);
    }

    public long getPartnerId() {
        return _partnerId;
    }

    public void setPartnerId(long partnerId) {
        _partnerId = partnerId;
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(String category) {
        _category = category;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public int getReturnedLotsCount() {
        return _returnedLotsCount;
    }

    public void setReturnedLotsCount(int returnedLotsCount) {
        _returnedLotsCount = returnedLotsCount;
    }
}
