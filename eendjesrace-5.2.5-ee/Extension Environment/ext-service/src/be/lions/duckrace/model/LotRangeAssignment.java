package be.lions.duckrace.model;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

import be.lions.duckrace.service.PartnerLocalServiceUtil;

public class LotRangeAssignment {

	private LotRange lotRange;
	private long partnerId;

	public LotRange getLotRange() {
		return lotRange;
	}

	public void setLotRange(LotRange lotRange) {
		this.lotRange = lotRange;
	}

	public long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(long partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerName() {
		try {
			return PartnerLocalServiceUtil.getPartner(partnerId).getName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return String.valueOf(partnerId);
	}

}
