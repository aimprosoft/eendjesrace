package be.lions.duckrace.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <a href="LotReservationSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>be.lions.duckrace.service.http.LotReservationServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.http.LotReservationServiceSoap
 *
 */
public class LotReservationSoap implements Serializable {
    private long _reservationId;
    private long _participantId;
    private int _numberLots;
    private boolean _approved;
    private Date _reservationDate;

    public LotReservationSoap() {
    }

    public static LotReservationSoap toSoapModel(LotReservation model) {
        LotReservationSoap soapModel = new LotReservationSoap();

        soapModel.setReservationId(model.getReservationId());
        soapModel.setParticipantId(model.getParticipantId());
        soapModel.setNumberLots(model.getNumberLots());
        soapModel.setApproved(model.getApproved());
        soapModel.setReservationDate(model.getReservationDate());

        return soapModel;
    }

    public static LotReservationSoap[] toSoapModels(LotReservation[] models) {
        LotReservationSoap[] soapModels = new LotReservationSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static LotReservationSoap[][] toSoapModels(LotReservation[][] models) {
        LotReservationSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new LotReservationSoap[models.length][models[0].length];
        } else {
            soapModels = new LotReservationSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static LotReservationSoap[] toSoapModels(List<LotReservation> models) {
        List<LotReservationSoap> soapModels = new ArrayList<LotReservationSoap>(models.size());

        for (LotReservation model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new LotReservationSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _reservationId;
    }

    public void setPrimaryKey(long pk) {
        setReservationId(pk);
    }

    public long getReservationId() {
        return _reservationId;
    }

    public void setReservationId(long reservationId) {
        _reservationId = reservationId;
    }

    public long getParticipantId() {
        return _participantId;
    }

    public void setParticipantId(long participantId) {
        _participantId = participantId;
    }

    public int getNumberLots() {
        return _numberLots;
    }

    public void setNumberLots(int numberLots) {
        _numberLots = numberLots;
    }

    public boolean getApproved() {
        return _approved;
    }

    public boolean isApproved() {
        return _approved;
    }

    public void setApproved(boolean approved) {
        _approved = approved;
    }

    public Date getReservationDate() {
        return _reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        _reservationDate = reservationDate;
    }
}
