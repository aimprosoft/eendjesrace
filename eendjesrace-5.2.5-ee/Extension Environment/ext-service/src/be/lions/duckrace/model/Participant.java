package be.lions.duckrace.model;


/**
 * <a href="Participant.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Participant</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>be.lions.duckrace.model.impl.ParticipantImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.ParticipantModel
 * @see be.lions.duckrace.model.impl.ParticipantImpl
 * @see be.lions.duckrace.model.impl.ParticipantModelImpl
 *
 */
public interface Participant extends ParticipantModel {
    public java.lang.String getFullName();

    public java.util.List<be.lions.duckrace.model.Lot> getLots()
        throws com.liferay.portal.SystemException;

    public java.lang.String getFormattedMobileNumber();

    public java.lang.String getTransactionSafeEmailAddress();

    public boolean matchesOneOf(java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress);

    public boolean matchesAll(java.lang.String firstName,
        java.lang.String lastName, java.lang.String emailAddress);

    public void validate();
}
