package be.lions.duckrace.model;


/**
 * <a href="Partner.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Partner</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>be.lions.duckrace.model.impl.PartnerImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.PartnerModel
 * @see be.lions.duckrace.model.impl.PartnerImpl
 * @see be.lions.duckrace.model.impl.PartnerModelImpl
 *
 */
public interface Partner extends PartnerModel {
    public int getAssignedLotsCount() throws com.liferay.portal.SystemException;

    public int getRegisteredLotsCount()
        throws com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> getAssignedLots()
        throws com.liferay.portal.SystemException;
}
