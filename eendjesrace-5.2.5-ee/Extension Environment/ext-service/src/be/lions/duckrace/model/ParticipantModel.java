package be.lions.duckrace.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="ParticipantModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Participant</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Participant
 * @see be.lions.duckrace.model.impl.ParticipantImpl
 * @see be.lions.duckrace.model.impl.ParticipantModelImpl
 *
 */
public interface ParticipantModel extends BaseModel<Participant> {
    public long getPrimaryKey();

    public void setPrimaryKey(long pk);

    public long getParticipantId();

    public void setParticipantId(long participantId);

    public String getEmailAddress();

    public void setEmailAddress(String emailAddress);

    public String getMobileNumber();

    public void setMobileNumber(String mobileNumber);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public boolean getContestParticipant();

    public boolean isContestParticipant();

    public void setContestParticipant(boolean contestParticipant);

    public boolean getMercedesDriver();

    public boolean isMercedesDriver();

    public void setMercedesDriver(boolean mercedesDriver);

    public String getCarBrand();

    public void setCarBrand(String carBrand);

    public boolean getMercedesOld();

    public boolean isMercedesOld();

    public void setMercedesOld(boolean mercedesOld);

    public String getLanguage();

    public void setLanguage(String language);

    public Participant toEscapedModel();
}
