package be.lions.duckrace.model;


/**
 * <a href="Lot.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Lot</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>be.lions.duckrace.model.impl.LotImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.LotModel
 * @see be.lions.duckrace.model.impl.LotImpl
 * @see be.lions.duckrace.model.impl.LotModelImpl
 *
 */
public interface Lot extends LotModel {
    public boolean isLocked();

    public java.util.Date getUnlockDate();

    public boolean isLockOverdue();

    public be.lions.duckrace.model.Participant getParticipant()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    public be.lions.duckrace.model.Partner getPartner()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;
}
