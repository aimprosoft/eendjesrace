package be.lions.duckrace.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="PartnerModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Partner</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Partner
 * @see be.lions.duckrace.model.impl.PartnerImpl
 * @see be.lions.duckrace.model.impl.PartnerModelImpl
 *
 */
public interface PartnerModel extends BaseModel<Partner> {
    public long getPrimaryKey();

    public void setPrimaryKey(long pk);

    public long getPartnerId();

    public void setPartnerId(long partnerId);

    public String getCategory();

    public void setCategory(String category);

    public String getName();

    public void setName(String name);

    public int getReturnedLotsCount();

    public void setReturnedLotsCount(int returnedLotsCount);

    public Partner toEscapedModel();
}
