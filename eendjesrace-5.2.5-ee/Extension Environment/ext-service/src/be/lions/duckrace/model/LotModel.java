package be.lions.duckrace.model;

import com.liferay.portal.model.BaseModel;

import java.util.Date;


/**
 * <a href="LotModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>Lot</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Lot
 * @see be.lions.duckrace.model.impl.LotImpl
 * @see be.lions.duckrace.model.impl.LotModelImpl
 *
 */
public interface LotModel extends BaseModel<Lot> {
    public String getPrimaryKey();

    public void setPrimaryKey(String pk);

    public String getCode();

    public void setCode(String code);

    public String getChecksum();

    public void setChecksum(String checksum);

    public int getLotNumber();

    public void setLotNumber(int lotNumber);

    public int getNumber();

    public void setNumber(int number);

    public boolean getPressed();

    public boolean isPressed();

    public void setPressed(boolean pressed);

    public int getNbTries();

    public void setNbTries(int nbTries);

    public Date getLockoutDate();

    public void setLockoutDate(Date lockoutDate);

    public long getParticipantId();

    public void setParticipantId(long participantId);

    public long getReservationId();

    public void setReservationId(long reservationId);

    public long getPartnerId();

    public void setPartnerId(long partnerId);

    public String getOrderNumber();

    public void setOrderNumber(String orderNumber);

    public Lot toEscapedModel();
}
