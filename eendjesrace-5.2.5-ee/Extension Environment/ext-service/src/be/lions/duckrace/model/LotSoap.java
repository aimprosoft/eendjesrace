package be.lions.duckrace.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <a href="LotSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>be.lions.duckrace.service.http.LotServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.service.http.LotServiceSoap
 *
 */
public class LotSoap implements Serializable {
    private String _code;
    private String _checksum;
    private int _lotNumber;
    private int _number;
    private boolean _pressed;
    private int _nbTries;
    private Date _lockoutDate;
    private long _participantId;
    private long _reservationId;
    private long _partnerId;
    private String _orderNumber;

    public LotSoap() {
    }

    public static LotSoap toSoapModel(Lot model) {
        LotSoap soapModel = new LotSoap();

        soapModel.setCode(model.getCode());
        soapModel.setChecksum(model.getChecksum());
        soapModel.setLotNumber(model.getLotNumber());
        soapModel.setNumber(model.getNumber());
        soapModel.setPressed(model.getPressed());
        soapModel.setNbTries(model.getNbTries());
        soapModel.setLockoutDate(model.getLockoutDate());
        soapModel.setParticipantId(model.getParticipantId());
        soapModel.setReservationId(model.getReservationId());
        soapModel.setPartnerId(model.getPartnerId());
        soapModel.setOrderNumber(model.getOrderNumber());

        return soapModel;
    }

    public static LotSoap[] toSoapModels(Lot[] models) {
        LotSoap[] soapModels = new LotSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static LotSoap[][] toSoapModels(Lot[][] models) {
        LotSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new LotSoap[models.length][models[0].length];
        } else {
            soapModels = new LotSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static LotSoap[] toSoapModels(List<Lot> models) {
        List<LotSoap> soapModels = new ArrayList<LotSoap>(models.size());

        for (Lot model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new LotSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _code;
    }

    public void setPrimaryKey(String pk) {
        setCode(pk);
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    public String getChecksum() {
        return _checksum;
    }

    public void setChecksum(String checksum) {
        _checksum = checksum;
    }

    public int getLotNumber() {
        return _lotNumber;
    }

    public void setLotNumber(int lotNumber) {
        _lotNumber = lotNumber;
    }

    public int getNumber() {
        return _number;
    }

    public void setNumber(int number) {
        _number = number;
    }

    public boolean getPressed() {
        return _pressed;
    }

    public boolean isPressed() {
        return _pressed;
    }

    public void setPressed(boolean pressed) {
        _pressed = pressed;
    }

    public int getNbTries() {
        return _nbTries;
    }

    public void setNbTries(int nbTries) {
        _nbTries = nbTries;
    }

    public Date getLockoutDate() {
        return _lockoutDate;
    }

    public void setLockoutDate(Date lockoutDate) {
        _lockoutDate = lockoutDate;
    }

    public long getParticipantId() {
        return _participantId;
    }

    public void setParticipantId(long participantId) {
        _participantId = participantId;
    }

    public long getReservationId() {
        return _reservationId;
    }

    public void setReservationId(long reservationId) {
        _reservationId = reservationId;
    }

    public long getPartnerId() {
        return _partnerId;
    }

    public void setPartnerId(long partnerId) {
        _partnerId = partnerId;
    }

    public String getOrderNumber() {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        _orderNumber = orderNumber;
    }
}
