package be.lions.duckrace.model;


/**
 * <a href="LotReservation.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>LotReservation</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>be.lions.duckrace.model.impl.LotReservationImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.LotReservationModel
 * @see be.lions.duckrace.model.impl.LotReservationImpl
 * @see be.lions.duckrace.model.impl.LotReservationModelImpl
 *
 */
public interface LotReservation extends LotReservationModel {
    public int getTotalCost();

    public be.lions.duckrace.model.Participant getParticipant()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException;

    public java.util.List<be.lions.duckrace.model.Lot> getLots()
        throws com.liferay.portal.SystemException;

    public java.io.File generatePdf()
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException,
            com.lowagie.text.DocumentException, java.io.IOException;
}
