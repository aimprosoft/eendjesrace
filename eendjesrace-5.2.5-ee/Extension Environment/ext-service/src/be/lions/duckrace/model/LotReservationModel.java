package be.lions.duckrace.model;

import com.liferay.portal.model.BaseModel;

import java.util.Date;


/**
 * <a href="LotReservationModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>LotReservation</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.LotReservation
 * @see be.lions.duckrace.model.impl.LotReservationImpl
 * @see be.lions.duckrace.model.impl.LotReservationModelImpl
 *
 */
public interface LotReservationModel extends BaseModel<LotReservation> {
    public long getPrimaryKey();

    public void setPrimaryKey(long pk);

    public long getReservationId();

    public void setReservationId(long reservationId);

    public long getParticipantId();

    public void setParticipantId(long participantId);

    public int getNumberLots();

    public void setNumberLots(int numberLots);

    public boolean getApproved();

    public boolean isApproved();

    public void setApproved(boolean approved);

    public Date getReservationDate();

    public void setReservationDate(Date reservationDate);

    public LotReservation toEscapedModel();
}
