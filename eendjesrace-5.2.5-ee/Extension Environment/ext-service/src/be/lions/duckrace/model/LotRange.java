package be.lions.duckrace.model;

import java.util.ArrayList;
import java.util.List;

public class LotRange implements Comparable<LotRange> {

	private int from;
	private int to;

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}
	
	/**
	 * Precondition: both ranges are valid.
	 */
	public boolean overlapsWith(LotRange range) {
		return range.getTo() >= from && range.getTo() <= to ||
				to >= range.getFrom() && from <= range.getTo();
	}
	
	public boolean isValid(int max) {
		return from > 0 && to > 0 && to >= from && to <= max;
	}
	
	public List<Integer> getIndices() {
		List<Integer> result = new ArrayList<Integer>();
		for(int i=from; i<=to; i++) {
			result.add(i);
		}
		return result;
	}
	
	public int compareTo(LotRange other) {
		return from - other.getFrom();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + from;
		result = prime * result + to;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LotRange other = (LotRange) obj;
		if (from != other.from)
			return false;
		if (to != other.to)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("[%d,%d]", from, to);
	}
	
}
