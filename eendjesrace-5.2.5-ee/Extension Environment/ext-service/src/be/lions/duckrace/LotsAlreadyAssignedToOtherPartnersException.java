/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package be.lions.duckrace;

import java.util.List;

import be.lions.duckrace.model.LotRangeAssignment;

import com.liferay.portal.PortalException;

/**
 * <a href="LotAlreadyRegisteredException.java.html"><b><i>View Source</i></b></a>
 *
 * @author Brian Wing Shun Chan
 *
 */
public class LotsAlreadyAssignedToOtherPartnersException extends PortalException {

	public LotsAlreadyAssignedToOtherPartnersException(List<LotRangeAssignment> assignments) {
		super(createMessage(assignments));
	}

	private static String createMessage(List<LotRangeAssignment> assignments) {
		StringBuilder sb = new StringBuilder();
		for(LotRangeAssignment assignment: assignments) {
			int from = assignment.getLotRange().getFrom();
			int to = assignment.getLotRange().getTo();
			String partnerName = assignment.getPartnerName();
			sb.append("Lotjes met lotnummers "+from + " tot "+ to+ " zijn reeds toegewezen aan partner "+partnerName+".<br/>");
		}
		return sb.toString();
	}
	
}