package be.lions.duckrace.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


public class HttpUtils {

	public static boolean isHttps(HttpServletRequest req) {
		return req.isSecure();
	}
	
	public static String getHost(HttpServletRequest req) {
		return req.getRemoteHost();
	}
	
	public static void setResponseString(HttpServletResponse resp, String value) {
		try {
			PrintWriter writer = resp.getWriter();
			writer.print(value);
			writer.close();
		} catch (IOException e) {
			log.error(e);
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(HttpUtils.class);
	
}
