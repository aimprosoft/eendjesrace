package be.lions.duckrace.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

	public static Date createDate(int day, Month month, int year, int hour) {
		return new GregorianCalendar(year, month.getId(), day, hour, 0).getTime();
	}
	
	public static boolean before(Date one, Date other) {
		if(one == null || other == null) {
			throw new IllegalArgumentException();
		}
		return one.before(other);
	}
	
	public enum Month {
		JANUARY(Calendar.JANUARY),
		FEBRUARY(Calendar.FEBRUARY),
		MARCH(Calendar.MARCH),
		APRIL(Calendar.APRIL),
		MAY(Calendar.MAY),
		JUNE(Calendar.JUNE),
		JULY(Calendar.JULY),
		AUGUST(Calendar.AUGUST),
		SEPTEMBER(Calendar.SEPTEMBER),
		OCTOBER(Calendar.OCTOBER),
		NOVEMBER(Calendar.NOVEMBER),
		DECEMBER(Calendar.DECEMBER);
		
		private int id;
		
		private Month(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
	}
	
}
