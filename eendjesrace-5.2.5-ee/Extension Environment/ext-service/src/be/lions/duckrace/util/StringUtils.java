package be.lions.duckrace.util;

import java.util.regex.Pattern;

import com.liferay.portal.kernel.util.StringUtil;

public class StringUtils {

	public static boolean isEmpty(String str) {
		return str == null || "".equals(str);
	}
	
	public static boolean isValidMobileNumber(String mobileNumber) {
		if(isEmpty(mobileNumber)) {
			return false;
		}
		String formatted = getStrippedMobileNumber(mobileNumber);
		int size = formatted.length();
		if(size < 11) {
			return false;
		}
		try {
			long nb = Long.parseLong(formatted);
			return nb > 0;
		} catch(NumberFormatException e) {
			return false;
		}
	}
	
	public static String getStrippedMobileNumber(String mobileNumber) {
		if(mobileNumber == null) {
			return null;
		}
		String digitsOnly = StringUtil.extractDigits(mobileNumber);
		if(digitsOnly.startsWith("0")) {
			return "32"+digitsOnly.substring(1);
		}
		return digitsOnly;
	}
	
	public static String getFormattedMobileNumber(String mobileNumber) {
		if(!isValidMobileNumber(mobileNumber)) {
			return null;
		}
		String stripped = getStrippedMobileNumber(mobileNumber);
		return "(+"+stripped.substring(0, 2)+")"+stripped.substring(2, 5)+"/"+
			stripped.substring(5, 7)+"."+stripped.substring(7, 9)+"."+stripped.substring(9);
	}
	
	public static String fromEnumToName(String enumName) {
		if(enumName == null) {
			return null;
		}
		return enumName.toLowerCase().replace("_", "-");
	}

	public static String fromNameToEnum(String tagName) {
		if(tagName == null) {
			return null;
		}
		return tagName.replace("-", "_").toUpperCase();
	}

	public static String lowerCaseFirstCapital(String name) {
		if(name == null) {
			return null;
		}
		if(name.length() <= 1) {
			return name.toUpperCase();
		}
		String lower = name.toLowerCase();
		return String.valueOf(lower.charAt(0)).toUpperCase()+lower.substring(1);
	}

	public static boolean isValidEmailAddress(String emailAddress) {
		if(StringUtils.isEmpty(emailAddress)) {
			return false;
		}
		String pattern = "[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}";
		return Pattern.compile(pattern).matcher(emailAddress.toUpperCase().trim()).matches();
	}

	public static String trim(String firstName) {
		if(firstName == null) {
			return null;
		}
		return firstName.trim();
	}
	
}
