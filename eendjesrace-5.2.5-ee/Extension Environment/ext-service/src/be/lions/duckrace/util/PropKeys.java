package be.lions.duckrace.util;

public class PropKeys {

	// Rekeningnummers
	public static final String ACCOUNT_NO_IBAN = "account.no.iban";
	public static final String ACCOUNT_NO_BE = "account.no.be";
	public static final String ACCOUNT_NO_SWIFT = "account.no.swift";
	
	// Email info
	public static final String MAIL_FROM_ADDRESS = "mail.from.address";
	public static final String MAIL_REPLY_TO_ADDRESS = "mail.reply.to.address";
	public static final String MAIL_BCC_ADDRESSES = "mail.bcc.addresses";
	
	// Deadlines
	public static final String REGISTRATION_DEADLINE = "deadline.registration";
	public static final String RESERVATION_DEADLINE = "deadline.reservation";
	public static final String ADMIN_REGISTRATION_DEADLINE = "admin.deadline.registration";
	public static final String ADMIN_RESERVATION_DEADLINE = "admin.deadline.reservation";
	
	// General
	public static final String SITE_TITLE = "site.title";
	public static final String LOT_PRICE = "lot.price";

	// BeHappy2
	public static final String BEHAPPY2_ORDER_URL = "behappy2.order.url";
	public static final String BEHAPPY2_TRUSTED_HOSTS = "behappy2.trusted.hosts";
}
