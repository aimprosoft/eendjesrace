package be.lions.duckrace.util;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

public class StringUtilsGetStrippedMobileNumberTest {

	@Test
	public void localNumberIsConverterToCountryCodeFormat() {
		String input = "0472314354";
		
		String expected = "32472314354";
		String actual = StringUtils.getStrippedMobileNumber(input);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void numberWithBelgianCountryCodeIsNotModified() {
		String input = "32472314354";
		
		String actual = StringUtils.getStrippedMobileNumber(input);
		
		assertEquals(input, actual);
	}
	
	@Test
	public void numberWithOtherCountryCodeIsNotModified() {
		String input = "31472314354";
		
		String actual = StringUtils.getStrippedMobileNumber(input);
		
		assertEquals(input, actual);
	}
	
	@Test
	public void formattedNumberWithCountryCodeIsConvertedToNormalizedFormat() {
		String input = "(+32)472/31.43.54";
		
		String expected = "32472314354";
		String actual = StringUtils.getStrippedMobileNumber(input);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void formattedNumberWithoutCountryCodeIsConvertedToNormalizedFormat() {
		String input = "0472/31.43.54";
		
		String expected = "32472314354";
		String actual = StringUtils.getStrippedMobileNumber(input);
		
		assertEquals(expected, actual);
	}
	
}
