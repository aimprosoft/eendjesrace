package be.lions.duckrace.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.util.ArrayUtils;
import be.lions.duckrace.util.HttpUtils;
import be.lions.duckrace.util.StringUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PropsUtil;

@SuppressWarnings("serial")
public class LotRegistrationServlet extends HttpServlet {

	private static final String TRUSTED_HOSTS = "paratel.trusted.hosts";
	
	protected static final String PARAM_PARTICIPANT = "p";
	protected static final String PARAM_CODE = "c";
	protected static final String PARAM_CHECKSUM = "t";
	
	protected static final String UNEXPECTED_ERROR = "NOK-0";
	protected static final String INCORRECT_CODE_ERROR = "NOK-1";
	protected static final String ALREADY_REGISTERED_ERROR = "NOK-2";
	
	private static final String OK_NUMBER_PARAM = "{NUMBER}";
	private static final String OK_CODE_PARAM = "{CODE}";
	private static final String OK_CHECKSUM_PARAM = "{CHECKSUM}";
	private static final String OK_MSG_TPL = "OK-"+OK_NUMBER_PARAM+"-"+OK_CODE_PARAM+"-"+OK_CHECKSUM_PARAM;
	
	private HttpServletRequest req;
	private HttpServletResponse resp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] trustedHosts = PropsUtil.getArray(TRUSTED_HOSTS);
		if(ArrayUtils.isEmpty(trustedHosts)) {
			throw new ServletException("No trusted IP was configured");
		}
		
		if(!HttpUtils.isHttps(req)) {
			throw new ServletException("Someone tried to reach the servlet via normal HTTP");
		}
		
		String host = HttpUtils.getHost(req);
		if(!ArrayUtils.contains(trustedHosts, host)) {
			throw new ServletException("Remote host '"+host+"' is not a trusted host");
		}
		this.req = req;
		this.resp = resp;
	}
	
	protected String getMobileNo() {
		return StringUtils.getStrippedMobileNumber(req.getParameter(PARAM_PARTICIPANT));
	}
	
	protected String getCode() {
		return req.getParameter(PARAM_CODE);
	}
	
	protected String getChecksum() {
		return req.getParameter(PARAM_CHECKSUM);
	}
	
	protected String getOkResponseCode(Lot lot) {
		return OK_MSG_TPL
			.replace(OK_NUMBER_PARAM, String.valueOf(lot.getNumber()))
			.replace(OK_CODE_PARAM, lot.getCode())
			.replace(OK_CHECKSUM_PARAM, lot.getChecksum());
	}
	
	private static Log log = LogFactoryUtil.getLog(LotRegistrationServlet.class);
	
}
