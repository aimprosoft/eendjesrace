package be.lions.duckrace.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.lions.duckrace.InvalidChecksumException;
import be.lions.duckrace.LotAlreadyRegisteredException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.util.HttpUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@SuppressWarnings("serial")
public class CodeRegistrationServlet extends LotRegistrationServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			super.doGet(req, resp);
			
			Lot lot = LotLocalServiceUtil.registerLot(getMobileNo(), getCode(), getChecksum());
			HttpUtils.setResponseString(resp, getOkResponseCode(lot));
			log.info("Succesful request!");
		} catch(ServletException e) {
			log.warn(e.getMessage());
		} catch(LotAlreadyRegisteredException e) {
			log.warn("Lot with code '"+getCode()+"' was already registered!");
			HttpUtils.setResponseString(resp, ALREADY_REGISTERED_ERROR);
		} catch(NoSuchLotException e) {
			log.info("Lot code '"+getCode()+"' invalid! (mobile no: "+getMobileNo()+")");
			HttpUtils.setResponseString(resp, INCORRECT_CODE_ERROR);
		} catch(InvalidChecksumException e) {
			log.info("Invalid checksum! (mobile no: "+getMobileNo()+")");
			HttpUtils.setResponseString(resp, INCORRECT_CODE_ERROR);
		} catch(Exception e) {
			log.error(e);
			HttpUtils.setResponseString(resp, UNEXPECTED_ERROR);
		}
		
	}
	
	private static Log log = LogFactoryUtil.getLog(CodeRegistrationServlet.class);
	
}
