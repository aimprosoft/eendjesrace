package be.lions.duckrace.model.impl;

import java.util.List;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.util.StringUtils;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;


public class ParticipantImpl extends ParticipantModelImpl implements Participant {
    
    public String getFullName() {
    	return getFirstName() + " " + getLastName();
    }
    
    public List<Lot> getLots() throws SystemException {
    	return LotLocalServiceUtil.getLotsForParticipant(getParticipantId());
    }
    
    public String getFormattedMobileNumber() {
    	return StringUtils.getFormattedMobileNumber(getMobileNumber());
    }
    
    public String getTransactionSafeEmailAddress() {
    	return StringUtil.replace(getEmailAddress(), 
    			new String[] {StringPool.AT, StringPool.PERIOD}, 
    			new String[] {"_at_", "_dot_"});
    }
    
    public boolean matchesOneOf(String firstName, String lastName, String emailAddress) {
    	return (firstName != null && firstName.equals(getFirstName())) || 
    		(lastName != null && lastName.equals(getLastName())) || 
    		(emailAddress != null && emailAddress.equals(getEmailAddress()));
    }
    
    public boolean matchesAll(String firstName, String lastName, String emailAddress) {
    	return (firstName != null && firstName.equals(getFirstName())) && 
			(lastName != null && lastName.equals(getLastName())) && 
			(emailAddress != null && emailAddress.equals(getEmailAddress()));
    }
    
    public void validate() {
    	
    }
    
}
