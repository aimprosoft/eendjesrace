package be.lions.duckrace.model.impl;

import be.lions.duckrace.model.Partner;
import be.lions.duckrace.model.PartnerSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.impl.ExpandoBridgeImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="PartnerModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>Partner</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Partner
 * @see be.lions.duckrace.model.PartnerModel
 * @see be.lions.duckrace.model.impl.PartnerImpl
 *
 */
public class PartnerModelImpl extends BaseModelImpl<Partner> {
    public static final String TABLE_NAME = "Partner";
    public static final Object[][] TABLE_COLUMNS = {
            { "partnerId", new Integer(Types.BIGINT) },
            

            { "category", new Integer(Types.VARCHAR) },
            

            { "name", new Integer(Types.VARCHAR) },
            

            { "returnedLotsCount", new Integer(Types.INTEGER) }
        };
    public static final String TABLE_SQL_CREATE = "create table Partner (partnerId LONG not null primary key,category VARCHAR(75) null,name VARCHAR(75) null,returnedLotsCount INTEGER)";
    public static final String TABLE_SQL_DROP = "drop table Partner";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.entity.cache.enabled.be.lions.duckrace.model.Partner"),
            false);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.finder.cache.enabled.be.lions.duckrace.model.Partner"),
            false);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.portal.util.PropsUtil.get(
                "lock.expiration.time.be.lions.duckrace.model.Partner"));
    private long _partnerId;
    private String _category;
    private String _name;
    private int _returnedLotsCount;
    private transient ExpandoBridge _expandoBridge;

    public PartnerModelImpl() {
    }

    public static Partner toModel(PartnerSoap soapModel) {
        Partner model = new PartnerImpl();

        model.setPartnerId(soapModel.getPartnerId());
        model.setCategory(soapModel.getCategory());
        model.setName(soapModel.getName());
        model.setReturnedLotsCount(soapModel.getReturnedLotsCount());

        return model;
    }

    public static List<Partner> toModels(PartnerSoap[] soapModels) {
        List<Partner> models = new ArrayList<Partner>(soapModels.length);

        for (PartnerSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public long getPrimaryKey() {
        return _partnerId;
    }

    public void setPrimaryKey(long pk) {
        setPartnerId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_partnerId);
    }

    public long getPartnerId() {
        return _partnerId;
    }

    public void setPartnerId(long partnerId) {
        _partnerId = partnerId;
    }

    public String getCategory() {
        return GetterUtil.getString(_category);
    }

    public void setCategory(String category) {
        _category = category;
    }

    public String getName() {
        return GetterUtil.getString(_name);
    }

    public void setName(String name) {
        _name = name;
    }

    public int getReturnedLotsCount() {
        return _returnedLotsCount;
    }

    public void setReturnedLotsCount(int returnedLotsCount) {
        _returnedLotsCount = returnedLotsCount;
    }

    public Partner toEscapedModel() {
        if (isEscapedModel()) {
            return (Partner) this;
        } else {
            Partner model = new PartnerImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setPartnerId(getPartnerId());
            model.setCategory(HtmlUtil.escape(getCategory()));
            model.setName(HtmlUtil.escape(getName()));
            model.setReturnedLotsCount(getReturnedLotsCount());

            model = (Partner) Proxy.newProxyInstance(Partner.class.getClassLoader(),
                    new Class[] { Partner.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public ExpandoBridge getExpandoBridge() {
        if (_expandoBridge == null) {
            _expandoBridge = new ExpandoBridgeImpl(Partner.class.getName(),
                    getPrimaryKey());
        }

        return _expandoBridge;
    }

    public Object clone() {
        PartnerImpl clone = new PartnerImpl();

        clone.setPartnerId(getPartnerId());
        clone.setCategory(getCategory());
        clone.setName(getName());
        clone.setReturnedLotsCount(getReturnedLotsCount());

        return clone;
    }

    public int compareTo(Partner partner) {
        int value = 0;

        value = getCategory().compareTo(partner.getCategory());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        Partner partner = null;

        try {
            partner = (Partner) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        long pk = partner.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int) getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{partnerId=");
        sb.append(getPartnerId());
        sb.append(", category=");
        sb.append(getCategory());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", returnedLotsCount=");
        sb.append(getReturnedLotsCount());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("be.lions.duckrace.model.Partner");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>partnerId</column-name><column-value><![CDATA[");
        sb.append(getPartnerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>category</column-name><column-value><![CDATA[");
        sb.append(getCategory());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>returnedLotsCount</column-name><column-value><![CDATA[");
        sb.append(getReturnedLotsCount());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
