package be.lions.duckrace.model.impl;

import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.ParticipantSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.impl.ExpandoBridgeImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="ParticipantModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>Participant</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Participant
 * @see be.lions.duckrace.model.ParticipantModel
 * @see be.lions.duckrace.model.impl.ParticipantImpl
 *
 */
public class ParticipantModelImpl extends BaseModelImpl<Participant> {
    public static final String TABLE_NAME = "Participant";
    public static final Object[][] TABLE_COLUMNS = {
            { "participantId", new Integer(Types.BIGINT) },
            

            { "emailAddress", new Integer(Types.VARCHAR) },
            

            { "mobileNumber", new Integer(Types.VARCHAR) },
            

            { "firstName", new Integer(Types.VARCHAR) },
            

            { "lastName", new Integer(Types.VARCHAR) },
            

            { "contestParticipant", new Integer(Types.BOOLEAN) },
            

            { "mercedesDriver", new Integer(Types.BOOLEAN) },
            

            { "carBrand", new Integer(Types.VARCHAR) },
            

            { "mercedesOld", new Integer(Types.BOOLEAN) },
            

            { "language", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table Participant (participantId LONG not null primary key,emailAddress VARCHAR(75) null,mobileNumber VARCHAR(75) null,firstName VARCHAR(75) null,lastName VARCHAR(75) null,contestParticipant BOOLEAN,mercedesDriver BOOLEAN,carBrand VARCHAR(75) null,mercedesOld BOOLEAN,language VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table Participant";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.entity.cache.enabled.be.lions.duckrace.model.Participant"),
            false);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.finder.cache.enabled.be.lions.duckrace.model.Participant"),
            false);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.portal.util.PropsUtil.get(
                "lock.expiration.time.be.lions.duckrace.model.Participant"));
    private long _participantId;
    private String _emailAddress;
    private String _mobileNumber;
    private String _originalMobileNumber;
    private String _firstName;
    private String _lastName;
    private boolean _contestParticipant;
    private boolean _mercedesDriver;
    private String _carBrand;
    private boolean _mercedesOld;
    private String _language;
    private transient ExpandoBridge _expandoBridge;

    public ParticipantModelImpl() {
    }

    public static Participant toModel(ParticipantSoap soapModel) {
        Participant model = new ParticipantImpl();

        model.setParticipantId(soapModel.getParticipantId());
        model.setEmailAddress(soapModel.getEmailAddress());
        model.setMobileNumber(soapModel.getMobileNumber());
        model.setFirstName(soapModel.getFirstName());
        model.setLastName(soapModel.getLastName());
        model.setContestParticipant(soapModel.getContestParticipant());
        model.setMercedesDriver(soapModel.getMercedesDriver());
        model.setCarBrand(soapModel.getCarBrand());
        model.setMercedesOld(soapModel.getMercedesOld());
        model.setLanguage(soapModel.getLanguage());

        return model;
    }

    public static List<Participant> toModels(ParticipantSoap[] soapModels) {
        List<Participant> models = new ArrayList<Participant>(soapModels.length);

        for (ParticipantSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public long getPrimaryKey() {
        return _participantId;
    }

    public void setPrimaryKey(long pk) {
        setParticipantId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_participantId);
    }

    public long getParticipantId() {
        return _participantId;
    }

    public void setParticipantId(long participantId) {
        _participantId = participantId;
    }

    public String getEmailAddress() {
        return GetterUtil.getString(_emailAddress);
    }

    public void setEmailAddress(String emailAddress) {
        _emailAddress = emailAddress;
    }

    public String getMobileNumber() {
        return GetterUtil.getString(_mobileNumber);
    }

    public void setMobileNumber(String mobileNumber) {
        _mobileNumber = mobileNumber;

        if (_originalMobileNumber == null) {
            _originalMobileNumber = mobileNumber;
        }
    }

    public String getOriginalMobileNumber() {
        return GetterUtil.getString(_originalMobileNumber);
    }

    public String getFirstName() {
        return GetterUtil.getString(_firstName);
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    public String getLastName() {
        return GetterUtil.getString(_lastName);
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    public boolean getContestParticipant() {
        return _contestParticipant;
    }

    public boolean isContestParticipant() {
        return _contestParticipant;
    }

    public void setContestParticipant(boolean contestParticipant) {
        _contestParticipant = contestParticipant;
    }

    public boolean getMercedesDriver() {
        return _mercedesDriver;
    }

    public boolean isMercedesDriver() {
        return _mercedesDriver;
    }

    public void setMercedesDriver(boolean mercedesDriver) {
        _mercedesDriver = mercedesDriver;
    }

    public String getCarBrand() {
        return GetterUtil.getString(_carBrand);
    }

    public void setCarBrand(String carBrand) {
        _carBrand = carBrand;
    }

    public boolean getMercedesOld() {
        return _mercedesOld;
    }

    public boolean isMercedesOld() {
        return _mercedesOld;
    }

    public void setMercedesOld(boolean mercedesOld) {
        _mercedesOld = mercedesOld;
    }

    public String getLanguage() {
        return GetterUtil.getString(_language);
    }

    public void setLanguage(String language) {
        _language = language;
    }

    public Participant toEscapedModel() {
        if (isEscapedModel()) {
            return (Participant) this;
        } else {
            Participant model = new ParticipantImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setParticipantId(getParticipantId());
            model.setEmailAddress(HtmlUtil.escape(getEmailAddress()));
            model.setMobileNumber(HtmlUtil.escape(getMobileNumber()));
            model.setFirstName(HtmlUtil.escape(getFirstName()));
            model.setLastName(HtmlUtil.escape(getLastName()));
            model.setContestParticipant(getContestParticipant());
            model.setMercedesDriver(getMercedesDriver());
            model.setCarBrand(HtmlUtil.escape(getCarBrand()));
            model.setMercedesOld(getMercedesOld());
            model.setLanguage(HtmlUtil.escape(getLanguage()));

            model = (Participant) Proxy.newProxyInstance(Participant.class.getClassLoader(),
                    new Class[] { Participant.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public ExpandoBridge getExpandoBridge() {
        if (_expandoBridge == null) {
            _expandoBridge = new ExpandoBridgeImpl(Participant.class.getName(),
                    getPrimaryKey());
        }

        return _expandoBridge;
    }

    public Object clone() {
        ParticipantImpl clone = new ParticipantImpl();

        clone.setParticipantId(getParticipantId());
        clone.setEmailAddress(getEmailAddress());
        clone.setMobileNumber(getMobileNumber());
        clone.setFirstName(getFirstName());
        clone.setLastName(getLastName());
        clone.setContestParticipant(getContestParticipant());
        clone.setMercedesDriver(getMercedesDriver());
        clone.setCarBrand(getCarBrand());
        clone.setMercedesOld(getMercedesOld());
        clone.setLanguage(getLanguage());

        return clone;
    }

    public int compareTo(Participant participant) {
        long pk = participant.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        Participant participant = null;

        try {
            participant = (Participant) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        long pk = participant.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int) getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{participantId=");
        sb.append(getParticipantId());
        sb.append(", emailAddress=");
        sb.append(getEmailAddress());
        sb.append(", mobileNumber=");
        sb.append(getMobileNumber());
        sb.append(", firstName=");
        sb.append(getFirstName());
        sb.append(", lastName=");
        sb.append(getLastName());
        sb.append(", contestParticipant=");
        sb.append(getContestParticipant());
        sb.append(", mercedesDriver=");
        sb.append(getMercedesDriver());
        sb.append(", carBrand=");
        sb.append(getCarBrand());
        sb.append(", mercedesOld=");
        sb.append(getMercedesOld());
        sb.append(", language=");
        sb.append(getLanguage());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("be.lions.duckrace.model.Participant");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>participantId</column-name><column-value><![CDATA[");
        sb.append(getParticipantId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>emailAddress</column-name><column-value><![CDATA[");
        sb.append(getEmailAddress());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>mobileNumber</column-name><column-value><![CDATA[");
        sb.append(getMobileNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>firstName</column-name><column-value><![CDATA[");
        sb.append(getFirstName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lastName</column-name><column-value><![CDATA[");
        sb.append(getLastName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>contestParticipant</column-name><column-value><![CDATA[");
        sb.append(getContestParticipant());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>mercedesDriver</column-name><column-value><![CDATA[");
        sb.append(getMercedesDriver());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>carBrand</column-name><column-value><![CDATA[");
        sb.append(getCarBrand());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>mercedesOld</column-name><column-value><![CDATA[");
        sb.append(getMercedesOld());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>language</column-name><column-value><![CDATA[");
        sb.append(getLanguage());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
