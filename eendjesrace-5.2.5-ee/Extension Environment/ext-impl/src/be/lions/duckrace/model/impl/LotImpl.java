package be.lions.duckrace.model.impl;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.NoSuchPartnerException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.service.PartnerLocalServiceUtil;


public class LotImpl extends LotModelImpl implements Lot {
    
	public static final int LOCK_DURATION_HOURS = 24;
	
    public boolean isLocked() {
    	return getLockoutDate() != null;
    }
    
    public Date getUnlockDate() {
    	if(getLockoutDate() == null) {
    		return null;
    	}
    	return DateUtils.addHours(getLockoutDate(), LOCK_DURATION_HOURS);
    }
    
    public boolean isLockOverdue() {
    	return new Date().compareTo(getUnlockDate()) > 0;
    }
    
    public Participant getParticipant() throws PortalException, SystemException {
    	try {
    		return ParticipantLocalServiceUtil.getParticipant(getParticipantId());
    	} catch(NoSuchParticipantException e) {
    		return null;
    	}
    }
    
    public Partner getPartner() throws PortalException, SystemException {
    	try {
			return PartnerLocalServiceUtil.getPartner(getPartnerId());
		} catch (NoSuchPartnerException e) {
			return null;
		}
    }
    
}
