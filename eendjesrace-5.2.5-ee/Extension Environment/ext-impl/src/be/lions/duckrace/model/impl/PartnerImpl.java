package be.lions.duckrace.model.impl;

import java.util.List;

import com.liferay.portal.SystemException;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.LotLocalServiceUtil;


public class PartnerImpl extends PartnerModelImpl implements Partner {
    
	public int getAssignedLotsCount() throws SystemException {
		return LotLocalServiceUtil.getLotsForPartnerCount(getPartnerId());
	}
	
	public int getRegisteredLotsCount() throws SystemException {
		return LotLocalServiceUtil.getRegisteredLotsForPartnerCount(getPartnerId());
	}
	
	public List<Lot> getAssignedLots() throws SystemException {
		return LotLocalServiceUtil.getLotsForPartner(getPartnerId());
	}
    
}
