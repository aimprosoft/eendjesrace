package be.lions.duckrace.model.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.util.SystemProperties;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class LotReservationImpl extends LotReservationModelImpl implements LotReservation {
    
	public int getTotalCost() {
    	return (int) (getNumberLots() * DuckRaceLocalServiceUtil.getLotPrice());
    }
	
	public Participant getParticipant() throws SystemException, PortalException {
		try {
			return ParticipantLocalServiceUtil.getParticipant(getParticipantId());
		} catch(NoSuchParticipantException e) {
			return null;
		}
	}
	
	public List<Lot> getLots() throws SystemException {
		return LotLocalServiceUtil.getLotsForReservation(getReservationId());
	}
	
	public File generatePdf() throws DocumentException, SystemException, PortalException, IOException {
		File result = new File(getTempFilePath());
		OutputStream fos = new FileOutputStream(result);
		Document doc = new Document(PageSize.A4);
		PdfWriter.getInstance(doc, fos);
		doc.open();
		doc.add(createLotTable());
		doc.close();
		fos.close();
		return result;
	}

	private Element createLotTable() throws SystemException, PortalException {
		PdfPTable result = new PdfPTable(5);
		result.setWidthPercentage(100);
		result.getDefaultCell().setPadding(10);
		result.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		for (Lot lot: getLots()) {
			PdfPCell cell = new PdfPCell();
			cell.addElement(getParagraph("Nummer", ""+lot.getNumber()));
			cell.addElement(getParagraph("Code", lot.getCode()+lot.getChecksum()));
			cell.addElement(getDots());
			cell.addElement(getDots());
			result.addCell(cell);
		}
		return result;
	}

	private String getTempFilePath() throws SystemException, PortalException {
		StringBuilder path = new StringBuilder();
    	path.append(SystemProperties.get(SystemProperties.TMP_DIR));
    	path.append(StringPool.SLASH);
    	path.append(getReservationId()+"-eendjes");
    	path.append(StringPool.PERIOD);
    	path.append("pdf");
		return path.toString();
	}
	
	private Element getParagraph(String label, String boldValue) {
		Paragraph result = new Paragraph();
		result.add(new Phrase(label+": ", getFont(false)));
		result.add(new Phrase(boldValue, getFont(true)));
		return result;
	}
	
	private Element getDots() {
		return new Paragraph("................................", getFont(false));
	}
	
	private Font getFont(boolean bold) {
		int fontType = bold ? Font.BOLD : Font.NORMAL;
		return new Font(Font.HELVETICA, 10, fontType);
	}
	
}
