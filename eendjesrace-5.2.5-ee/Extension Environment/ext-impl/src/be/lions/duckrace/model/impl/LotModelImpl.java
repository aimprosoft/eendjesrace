package be.lions.duckrace.model.impl;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <a href="LotModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>Lot</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.Lot
 * @see be.lions.duckrace.model.LotModel
 * @see be.lions.duckrace.model.impl.LotImpl
 *
 */
public class LotModelImpl extends BaseModelImpl<Lot> {
    public static final String TABLE_NAME = "Lot";
    public static final Object[][] TABLE_COLUMNS = {
            { "code_", new Integer(Types.VARCHAR) },
            

            { "checksum", new Integer(Types.VARCHAR) },
            

            { "lotNumber", new Integer(Types.INTEGER) },
            

            { "number_", new Integer(Types.INTEGER) },
            

            { "pressed", new Integer(Types.BOOLEAN) },
            

            { "nbTries", new Integer(Types.INTEGER) },
            

            { "lockoutDate", new Integer(Types.TIMESTAMP) },
            

            { "participantId", new Integer(Types.BIGINT) },
            

            { "reservationId", new Integer(Types.BIGINT) },
            

            { "partnerId", new Integer(Types.BIGINT) },
            

            { "orderNumber", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table Lot (code_ VARCHAR(75) not null primary key,checksum VARCHAR(75) null,lotNumber INTEGER,number_ INTEGER,pressed BOOLEAN,nbTries INTEGER,lockoutDate DATE null,participantId LONG,reservationId LONG,partnerId LONG,orderNumber VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table Lot";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.entity.cache.enabled.be.lions.duckrace.model.Lot"),
            false);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.finder.cache.enabled.be.lions.duckrace.model.Lot"),
            false);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.portal.util.PropsUtil.get(
                "lock.expiration.time.be.lions.duckrace.model.Lot"));
    private String _code;
    private String _checksum;
    private int _lotNumber;
    private int _originalLotNumber;
    private boolean _setOriginalLotNumber;
    private int _number;
    private int _originalNumber;
    private boolean _setOriginalNumber;
    private boolean _pressed;
    private int _nbTries;
    private Date _lockoutDate;
    private long _participantId;
    private long _reservationId;
    private long _partnerId;
    private String _orderNumber;

    public LotModelImpl() {
    }

    public static Lot toModel(LotSoap soapModel) {
        Lot model = new LotImpl();

        model.setCode(soapModel.getCode());
        model.setChecksum(soapModel.getChecksum());
        model.setLotNumber(soapModel.getLotNumber());
        model.setNumber(soapModel.getNumber());
        model.setPressed(soapModel.getPressed());
        model.setNbTries(soapModel.getNbTries());
        model.setLockoutDate(soapModel.getLockoutDate());
        model.setParticipantId(soapModel.getParticipantId());
        model.setReservationId(soapModel.getReservationId());
        model.setPartnerId(soapModel.getPartnerId());
        model.setOrderNumber(soapModel.getOrderNumber());

        return model;
    }

    public static List<Lot> toModels(LotSoap[] soapModels) {
        List<Lot> models = new ArrayList<Lot>(soapModels.length);

        for (LotSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public String getPrimaryKey() {
        return _code;
    }

    public void setPrimaryKey(String pk) {
        setCode(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return _code;
    }

    public String getCode() {
        return GetterUtil.getString(_code);
    }

    public void setCode(String code) {
        _code = code;
    }

    public String getChecksum() {
        return GetterUtil.getString(_checksum);
    }

    public void setChecksum(String checksum) {
        _checksum = checksum;
    }

    public int getLotNumber() {
        return _lotNumber;
    }

    public void setLotNumber(int lotNumber) {
        _lotNumber = lotNumber;

        if (!_setOriginalLotNumber) {
            _setOriginalLotNumber = true;

            _originalLotNumber = lotNumber;
        }
    }

    public int getOriginalLotNumber() {
        return _originalLotNumber;
    }

    public int getNumber() {
        return _number;
    }

    public void setNumber(int number) {
        _number = number;

        if (!_setOriginalNumber) {
            _setOriginalNumber = true;

            _originalNumber = number;
        }
    }

    public int getOriginalNumber() {
        return _originalNumber;
    }

    public boolean getPressed() {
        return _pressed;
    }

    public boolean isPressed() {
        return _pressed;
    }

    public void setPressed(boolean pressed) {
        _pressed = pressed;
    }

    public int getNbTries() {
        return _nbTries;
    }

    public void setNbTries(int nbTries) {
        _nbTries = nbTries;
    }

    public Date getLockoutDate() {
        return _lockoutDate;
    }

    public void setLockoutDate(Date lockoutDate) {
        _lockoutDate = lockoutDate;
    }

    public long getParticipantId() {
        return _participantId;
    }

    public void setParticipantId(long participantId) {
        _participantId = participantId;
    }

    public long getReservationId() {
        return _reservationId;
    }

    public void setReservationId(long reservationId) {
        _reservationId = reservationId;
    }

    public long getPartnerId() {
        return _partnerId;
    }

    public void setPartnerId(long partnerId) {
        _partnerId = partnerId;
    }

    public String getOrderNumber() {
        return GetterUtil.getString(_orderNumber);
    }

    public void setOrderNumber(String orderNumber) {
        _orderNumber = orderNumber;
    }

    public Lot toEscapedModel() {
        if (isEscapedModel()) {
            return (Lot) this;
        } else {
            Lot model = new LotImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setCode(HtmlUtil.escape(getCode()));
            model.setChecksum(HtmlUtil.escape(getChecksum()));
            model.setLotNumber(getLotNumber());
            model.setNumber(getNumber());
            model.setPressed(getPressed());
            model.setNbTries(getNbTries());
            model.setLockoutDate(getLockoutDate());
            model.setParticipantId(getParticipantId());
            model.setReservationId(getReservationId());
            model.setPartnerId(getPartnerId());
            model.setOrderNumber(HtmlUtil.escape(getOrderNumber()));

            model = (Lot) Proxy.newProxyInstance(Lot.class.getClassLoader(),
                    new Class[] { Lot.class }, new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        LotImpl clone = new LotImpl();

        clone.setCode(getCode());
        clone.setChecksum(getChecksum());
        clone.setLotNumber(getLotNumber());
        clone.setNumber(getNumber());
        clone.setPressed(getPressed());
        clone.setNbTries(getNbTries());
        clone.setLockoutDate(getLockoutDate());
        clone.setParticipantId(getParticipantId());
        clone.setReservationId(getReservationId());
        clone.setPartnerId(getPartnerId());
        clone.setOrderNumber(getOrderNumber());

        return clone;
    }

    public int compareTo(Lot lot) {
        String pk = lot.getPrimaryKey();

        return getPrimaryKey().compareTo(pk);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        Lot lot = null;

        try {
            lot = (Lot) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        String pk = lot.getPrimaryKey();

        if (getPrimaryKey().equals(pk)) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{code=");
        sb.append(getCode());
        sb.append(", checksum=");
        sb.append(getChecksum());
        sb.append(", lotNumber=");
        sb.append(getLotNumber());
        sb.append(", number=");
        sb.append(getNumber());
        sb.append(", pressed=");
        sb.append(getPressed());
        sb.append(", nbTries=");
        sb.append(getNbTries());
        sb.append(", lockoutDate=");
        sb.append(getLockoutDate());
        sb.append(", participantId=");
        sb.append(getParticipantId());
        sb.append(", reservationId=");
        sb.append(getReservationId());
        sb.append(", partnerId=");
        sb.append(getPartnerId());
        sb.append(", orderNumber=");
        sb.append(getOrderNumber());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("be.lions.duckrace.model.Lot");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>code</column-name><column-value><![CDATA[");
        sb.append(getCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>checksum</column-name><column-value><![CDATA[");
        sb.append(getChecksum());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lotNumber</column-name><column-value><![CDATA[");
        sb.append(getLotNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>number</column-name><column-value><![CDATA[");
        sb.append(getNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pressed</column-name><column-value><![CDATA[");
        sb.append(getPressed());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nbTries</column-name><column-value><![CDATA[");
        sb.append(getNbTries());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lockoutDate</column-name><column-value><![CDATA[");
        sb.append(getLockoutDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>participantId</column-name><column-value><![CDATA[");
        sb.append(getParticipantId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>reservationId</column-name><column-value><![CDATA[");
        sb.append(getReservationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>partnerId</column-name><column-value><![CDATA[");
        sb.append(getPartnerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>orderNumber</column-name><column-value><![CDATA[");
        sb.append(getOrderNumber());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
