package be.lions.duckrace.model.impl;

import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.LotReservationSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.impl.ExpandoBridgeImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <a href="LotReservationModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>LotReservation</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see be.lions.duckrace.model.LotReservation
 * @see be.lions.duckrace.model.LotReservationModel
 * @see be.lions.duckrace.model.impl.LotReservationImpl
 *
 */
public class LotReservationModelImpl extends BaseModelImpl<LotReservation> {
    public static final String TABLE_NAME = "LotReservation";
    public static final Object[][] TABLE_COLUMNS = {
            { "reservationId", new Integer(Types.BIGINT) },
            

            { "participantId", new Integer(Types.BIGINT) },
            

            { "numberLots", new Integer(Types.INTEGER) },
            

            { "approved", new Integer(Types.BOOLEAN) },
            

            { "reservationDate", new Integer(Types.TIMESTAMP) }
        };
    public static final String TABLE_SQL_CREATE = "create table LotReservation (reservationId LONG not null primary key,participantId LONG,numberLots INTEGER,approved BOOLEAN,reservationDate DATE null)";
    public static final String TABLE_SQL_DROP = "drop table LotReservation";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.entity.cache.enabled.be.lions.duckrace.model.LotReservation"),
            false);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get(
                "value.object.finder.cache.enabled.be.lions.duckrace.model.LotReservation"),
            false);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.portal.util.PropsUtil.get(
                "lock.expiration.time.be.lions.duckrace.model.LotReservation"));
    private long _reservationId;
    private long _participantId;
    private int _numberLots;
    private boolean _approved;
    private Date _reservationDate;
    private transient ExpandoBridge _expandoBridge;

    public LotReservationModelImpl() {
    }

    public static LotReservation toModel(LotReservationSoap soapModel) {
        LotReservation model = new LotReservationImpl();

        model.setReservationId(soapModel.getReservationId());
        model.setParticipantId(soapModel.getParticipantId());
        model.setNumberLots(soapModel.getNumberLots());
        model.setApproved(soapModel.getApproved());
        model.setReservationDate(soapModel.getReservationDate());

        return model;
    }

    public static List<LotReservation> toModels(LotReservationSoap[] soapModels) {
        List<LotReservation> models = new ArrayList<LotReservation>(soapModels.length);

        for (LotReservationSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public long getPrimaryKey() {
        return _reservationId;
    }

    public void setPrimaryKey(long pk) {
        setReservationId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_reservationId);
    }

    public long getReservationId() {
        return _reservationId;
    }

    public void setReservationId(long reservationId) {
        _reservationId = reservationId;
    }

    public long getParticipantId() {
        return _participantId;
    }

    public void setParticipantId(long participantId) {
        _participantId = participantId;
    }

    public int getNumberLots() {
        return _numberLots;
    }

    public void setNumberLots(int numberLots) {
        _numberLots = numberLots;
    }

    public boolean getApproved() {
        return _approved;
    }

    public boolean isApproved() {
        return _approved;
    }

    public void setApproved(boolean approved) {
        _approved = approved;
    }

    public Date getReservationDate() {
        return _reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        _reservationDate = reservationDate;
    }

    public LotReservation toEscapedModel() {
        if (isEscapedModel()) {
            return (LotReservation) this;
        } else {
            LotReservation model = new LotReservationImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setReservationId(getReservationId());
            model.setParticipantId(getParticipantId());
            model.setNumberLots(getNumberLots());
            model.setApproved(getApproved());
            model.setReservationDate(getReservationDate());

            model = (LotReservation) Proxy.newProxyInstance(LotReservation.class.getClassLoader(),
                    new Class[] { LotReservation.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public ExpandoBridge getExpandoBridge() {
        if (_expandoBridge == null) {
            _expandoBridge = new ExpandoBridgeImpl(LotReservation.class.getName(),
                    getPrimaryKey());
        }

        return _expandoBridge;
    }

    public Object clone() {
        LotReservationImpl clone = new LotReservationImpl();

        clone.setReservationId(getReservationId());
        clone.setParticipantId(getParticipantId());
        clone.setNumberLots(getNumberLots());
        clone.setApproved(getApproved());
        clone.setReservationDate(getReservationDate());

        return clone;
    }

    public int compareTo(LotReservation lotReservation) {
        int value = 0;

        value = DateUtil.compareTo(getReservationDate(),
                lotReservation.getReservationDate());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        LotReservation lotReservation = null;

        try {
            lotReservation = (LotReservation) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        long pk = lotReservation.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int) getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{reservationId=");
        sb.append(getReservationId());
        sb.append(", participantId=");
        sb.append(getParticipantId());
        sb.append(", numberLots=");
        sb.append(getNumberLots());
        sb.append(", approved=");
        sb.append(getApproved());
        sb.append(", reservationDate=");
        sb.append(getReservationDate());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("be.lions.duckrace.model.LotReservation");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>reservationId</column-name><column-value><![CDATA[");
        sb.append(getReservationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>participantId</column-name><column-value><![CDATA[");
        sb.append(getParticipantId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numberLots</column-name><column-value><![CDATA[");
        sb.append(getNumberLots());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>approved</column-name><column-value><![CDATA[");
        sb.append(getApproved());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>reservationDate</column-name><column-value><![CDATA[");
        sb.append(getReservationDate());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
