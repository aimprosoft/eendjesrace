package be.lions.duckrace.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public final class DuckOrderUtil {

	private static final int VALID_PARAMETER_SIZE_WITHOUT_LANGUAGE = 6;
	private static final int VALID_PARAMETER_SIZE_WITH_LANGUAGE = 7;
	private static final String ERROR_INVALID_PARAMETERS_SIZE = "Invalid number of parameters provided";
	private static final String ERROR_REQUIRED_PARAMETERS_MISSING = "Some required parameters are missing";
	private static final String ERROR_NO_TRUSTED_HOSTS = "No trusted IPs were configured";
	private static final String ERROR_NON_HTTPS_REQUEST = "Someone tried to reach the servlet via normal HTTP";
	private static final String ERROR_UNTRUSTED_HOST_TPL = "Remote host %s was not trusted";
	private static final String PARAM_NAME = "name";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_CELL_PHONE = "cellPhone";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_DUCK_COUNT = "duckCount";
	private static final String PARAM_ORDER_NUMBER = "orderNumber";
	private static final String PARAM_LANGUAGE = "language";
	private static final String[] REQUIRED = { PARAM_NAME, PARAM_FIRST_NAME, PARAM_CELL_PHONE, PARAM_EMAIL,
			PARAM_DUCK_COUNT, PARAM_ORDER_NUMBER };

	private DuckOrderUtil() {
		// Private constructor
	}

	public static boolean isValidParameterMap(Map<String, String> parameters, HttpServletResponse response)
			throws IOException {
		if (parameters.size() != VALID_PARAMETER_SIZE_WITHOUT_LANGUAGE
				&& !(parameters.size() == VALID_PARAMETER_SIZE_WITH_LANGUAGE && parameters.containsKey(PARAM_LANGUAGE))) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_INVALID_PARAMETERS_SIZE);
			return false;
		}

		for (String param : REQUIRED) {
			if (!parameters.containsKey(param)) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
				return false;
			}
		}

		return true;
	}

	public static boolean isTrustedHost(String[] trustedHosts, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (ArrayUtils.isEmpty(trustedHosts)) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_NO_TRUSTED_HOSTS);
			return false;
		}

		if (!HttpUtils.isHttps(request)) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_NON_HTTPS_REQUEST);
			return false;
		}

		String host = HttpUtils.getHost(request);
		if (!isKnownHost(trustedHosts, host)) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					ERROR_UNTRUSTED_HOST_TPL.replace("%s", request.getRemoteHost()));
			return false;
		}

		return true;
	}

	private static boolean isKnownHost(String[] trustedHosts, String host) {
		for (int i = 0; i < trustedHosts.length; i++) {
			if (host.startsWith(trustedHosts[i])) {
				return true;
			}
		}

		return false;
	}
}
