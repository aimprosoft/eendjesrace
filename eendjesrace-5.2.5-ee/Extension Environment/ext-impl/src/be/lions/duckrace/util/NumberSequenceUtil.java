package be.lions.duckrace.util;

import java.util.List;

public final class NumberSequenceUtil {
	private NumberSequenceUtil() {
		// Private
	}

	public static int getFirstGapNumber(List<Integer> sequence) {
		if(sequence.isEmpty()) {
			return 1;
		}

		int current = 0;
		for (int i = 0; i < sequence.size() ; i++) {
			int next = sequence.get(i);

			if((next - current) > 1) {
				return (current + 1);
			}

			current = next;
		}

		return (current + 1);
	}
}
