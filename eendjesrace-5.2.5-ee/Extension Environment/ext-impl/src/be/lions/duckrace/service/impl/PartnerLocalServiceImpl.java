package be.lions.duckrace.service.impl;

import java.util.List;

import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.model.impl.PartnerImpl;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.PartnerLocalServiceUtil;
import be.lions.duckrace.service.base.PartnerLocalServiceBaseImpl;
import be.lions.duckrace.service.persistence.PartnerUtil;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;


public class PartnerLocalServiceImpl extends PartnerLocalServiceBaseImpl {
	
	public Partner createPartner() throws SystemException {
		long id = CounterLocalServiceUtil.increment();
		return createPartner(id);
	}
	
	public List<Partner> getPartners(String category) throws SystemException {
		return PartnerUtil.findByCategory(category);
	}
	
	public Partner save(Partner partner, List<LotRange> lotRanges) throws SystemException, PortalException {
		partner = updatePartner(partner, false);
		LotLocalServiceUtil.assignLotsToPartner(partner, lotRanges);
		return partner;
	}
	
	public List<Partner> getPartners() throws SystemException {
		return PartnerUtil.findAll();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getCategories() throws SystemException {
		DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Partner.class, PartnerImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader());
		dq.addOrder(OrderFactoryUtil.asc("category"));
		dq.setProjection(ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("category")));
		return (List) PartnerLocalServiceUtil.dynamicQuery(dq);
	}
	
}
