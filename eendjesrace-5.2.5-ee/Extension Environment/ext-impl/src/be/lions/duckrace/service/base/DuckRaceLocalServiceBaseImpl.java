package be.lions.duckrace.service.base;

import be.lions.duckrace.service.DuckRaceLocalService;
import be.lions.duckrace.service.LotLocalService;
import be.lions.duckrace.service.LotReservationLocalService;
import be.lions.duckrace.service.ParticipantLocalService;
import be.lions.duckrace.service.PartnerLocalService;
import be.lions.duckrace.service.persistence.LotPersistence;
import be.lions.duckrace.service.persistence.LotReservationPersistence;
import be.lions.duckrace.service.persistence.ParticipantPersistence;
import be.lions.duckrace.service.persistence.PartnerPersistence;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.util.PortalUtil;


public abstract class DuckRaceLocalServiceBaseImpl
    implements DuckRaceLocalService {
    @BeanReference(name = "be.lions.duckrace.service.LotLocalService.impl")
    protected LotLocalService lotLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.ParticipantLocalService.impl")
    protected ParticipantLocalService participantLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.LotReservationLocalService.impl")
    protected LotReservationLocalService lotReservationLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.PartnerLocalService.impl")
    protected PartnerLocalService partnerLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected PartnerPersistence partnerPersistence;
    @BeanReference(name = "be.lions.duckrace.service.DuckRaceLocalService.impl")
    protected DuckRaceLocalService duckRaceLocalService;

    public LotLocalService getLotLocalService() {
        return lotLocalService;
    }

    public void setLotLocalService(LotLocalService lotLocalService) {
        this.lotLocalService = lotLocalService;
    }

    public LotPersistence getLotPersistence() {
        return lotPersistence;
    }

    public void setLotPersistence(LotPersistence lotPersistence) {
        this.lotPersistence = lotPersistence;
    }

    public ParticipantLocalService getParticipantLocalService() {
        return participantLocalService;
    }

    public void setParticipantLocalService(
        ParticipantLocalService participantLocalService) {
        this.participantLocalService = participantLocalService;
    }

    public ParticipantPersistence getParticipantPersistence() {
        return participantPersistence;
    }

    public void setParticipantPersistence(
        ParticipantPersistence participantPersistence) {
        this.participantPersistence = participantPersistence;
    }

    public LotReservationLocalService getLotReservationLocalService() {
        return lotReservationLocalService;
    }

    public void setLotReservationLocalService(
        LotReservationLocalService lotReservationLocalService) {
        this.lotReservationLocalService = lotReservationLocalService;
    }

    public LotReservationPersistence getLotReservationPersistence() {
        return lotReservationPersistence;
    }

    public void setLotReservationPersistence(
        LotReservationPersistence lotReservationPersistence) {
        this.lotReservationPersistence = lotReservationPersistence;
    }

    public PartnerLocalService getPartnerLocalService() {
        return partnerLocalService;
    }

    public void setPartnerLocalService(PartnerLocalService partnerLocalService) {
        this.partnerLocalService = partnerLocalService;
    }

    public PartnerPersistence getPartnerPersistence() {
        return partnerPersistence;
    }

    public void setPartnerPersistence(PartnerPersistence partnerPersistence) {
        this.partnerPersistence = partnerPersistence;
    }

    public DuckRaceLocalService getDuckRaceLocalService() {
        return duckRaceLocalService;
    }

    public void setDuckRaceLocalService(
        DuckRaceLocalService duckRaceLocalService) {
        this.duckRaceLocalService = duckRaceLocalService;
    }

    protected void runSQL(String sql) throws SystemException {
        try {
            PortalUtil.runSQL(sql);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
