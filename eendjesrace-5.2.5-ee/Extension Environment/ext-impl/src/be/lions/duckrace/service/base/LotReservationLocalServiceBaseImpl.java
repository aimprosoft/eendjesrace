package be.lions.duckrace.service.base;

import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.service.DuckRaceLocalService;
import be.lions.duckrace.service.LotLocalService;
import be.lions.duckrace.service.LotReservationLocalService;
import be.lions.duckrace.service.ParticipantLocalService;
import be.lions.duckrace.service.PartnerLocalService;
import be.lions.duckrace.service.persistence.LotPersistence;
import be.lions.duckrace.service.persistence.LotReservationPersistence;
import be.lions.duckrace.service.persistence.ParticipantPersistence;
import be.lions.duckrace.service.persistence.PartnerPersistence;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.util.PortalUtil;

import java.util.List;


public abstract class LotReservationLocalServiceBaseImpl
    implements LotReservationLocalService {
    @BeanReference(name = "be.lions.duckrace.service.LotLocalService.impl")
    protected LotLocalService lotLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.ParticipantLocalService.impl")
    protected ParticipantLocalService participantLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.LotReservationLocalService.impl")
    protected LotReservationLocalService lotReservationLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.PartnerLocalService.impl")
    protected PartnerLocalService partnerLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected PartnerPersistence partnerPersistence;
    @BeanReference(name = "be.lions.duckrace.service.DuckRaceLocalService.impl")
    protected DuckRaceLocalService duckRaceLocalService;

    public LotReservation addLotReservation(LotReservation lotReservation)
        throws SystemException {
        lotReservation.setNew(true);

        return lotReservationPersistence.update(lotReservation, false);
    }

    public LotReservation createLotReservation(long reservationId) {
        return lotReservationPersistence.create(reservationId);
    }

    public void deleteLotReservation(long reservationId)
        throws PortalException, SystemException {
        lotReservationPersistence.remove(reservationId);
    }

    public void deleteLotReservation(LotReservation lotReservation)
        throws SystemException {
        lotReservationPersistence.remove(lotReservation);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return lotReservationPersistence.findWithDynamicQuery(dynamicQuery);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery, int start,
        int end) throws SystemException {
        return lotReservationPersistence.findWithDynamicQuery(dynamicQuery,
            start, end);
    }

    public LotReservation getLotReservation(long reservationId)
        throws PortalException, SystemException {
        return lotReservationPersistence.findByPrimaryKey(reservationId);
    }

    public List<LotReservation> getLotReservations(int start, int end)
        throws SystemException {
        return lotReservationPersistence.findAll(start, end);
    }

    public int getLotReservationsCount() throws SystemException {
        return lotReservationPersistence.countAll();
    }

    public LotReservation updateLotReservation(LotReservation lotReservation)
        throws SystemException {
        lotReservation.setNew(false);

        return lotReservationPersistence.update(lotReservation, true);
    }

    public LotReservation updateLotReservation(LotReservation lotReservation,
        boolean merge) throws SystemException {
        lotReservation.setNew(false);

        return lotReservationPersistence.update(lotReservation, merge);
    }

    public LotLocalService getLotLocalService() {
        return lotLocalService;
    }

    public void setLotLocalService(LotLocalService lotLocalService) {
        this.lotLocalService = lotLocalService;
    }

    public LotPersistence getLotPersistence() {
        return lotPersistence;
    }

    public void setLotPersistence(LotPersistence lotPersistence) {
        this.lotPersistence = lotPersistence;
    }

    public ParticipantLocalService getParticipantLocalService() {
        return participantLocalService;
    }

    public void setParticipantLocalService(
        ParticipantLocalService participantLocalService) {
        this.participantLocalService = participantLocalService;
    }

    public ParticipantPersistence getParticipantPersistence() {
        return participantPersistence;
    }

    public void setParticipantPersistence(
        ParticipantPersistence participantPersistence) {
        this.participantPersistence = participantPersistence;
    }

    public LotReservationLocalService getLotReservationLocalService() {
        return lotReservationLocalService;
    }

    public void setLotReservationLocalService(
        LotReservationLocalService lotReservationLocalService) {
        this.lotReservationLocalService = lotReservationLocalService;
    }

    public LotReservationPersistence getLotReservationPersistence() {
        return lotReservationPersistence;
    }

    public void setLotReservationPersistence(
        LotReservationPersistence lotReservationPersistence) {
        this.lotReservationPersistence = lotReservationPersistence;
    }

    public PartnerLocalService getPartnerLocalService() {
        return partnerLocalService;
    }

    public void setPartnerLocalService(PartnerLocalService partnerLocalService) {
        this.partnerLocalService = partnerLocalService;
    }

    public PartnerPersistence getPartnerPersistence() {
        return partnerPersistence;
    }

    public void setPartnerPersistence(PartnerPersistence partnerPersistence) {
        this.partnerPersistence = partnerPersistence;
    }

    public DuckRaceLocalService getDuckRaceLocalService() {
        return duckRaceLocalService;
    }

    public void setDuckRaceLocalService(
        DuckRaceLocalService duckRaceLocalService) {
        this.duckRaceLocalService = duckRaceLocalService;
    }

    protected void runSQL(String sql) throws SystemException {
        try {
            PortalUtil.runSQL(sql);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
