package be.lions.duckrace.service.persistence;

import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.impl.LotImpl;
import be.lions.duckrace.model.impl.LotModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class LotPersistenceImpl extends BasePersistenceImpl
    implements LotPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = LotImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_BY_PARTICIPANT = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByParticipant", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_OBC_PARTICIPANT = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByParticipant",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_COUNT_BY_PARTICIPANT = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByParticipant", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_RESERVATION = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByReservation", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_OBC_RESERVATION = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByReservation",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_COUNT_BY_RESERVATION = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByReservation", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FETCH_BY_LOTNUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_ENTITY,
            "fetchByLotNumber", new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_COUNT_BY_LOTNUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByLotNumber", new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_FETCH_BY_NUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_ENTITY,
            "fetchByNumber", new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_COUNT_BY_NUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByNumber", new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_PARTNER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByPartner", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_OBC_PARTNER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByPartner",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_COUNT_BY_PARTNER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByPartner", new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(LotPersistenceImpl.class);
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected be.lions.duckrace.service.persistence.ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected be.lions.duckrace.service.persistence.PartnerPersistence partnerPersistence;

    public void cacheResult(Lot lot) {
        EntityCacheUtil.putResult(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotImpl.class, lot.getPrimaryKey(), lot);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
            new Object[] { new Integer(lot.getLotNumber()) }, lot);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
            new Object[] { new Integer(lot.getNumber()) }, lot);
    }

    public void cacheResult(List<Lot> lots) {
        for (Lot lot : lots) {
            if (EntityCacheUtil.getResult(LotModelImpl.ENTITY_CACHE_ENABLED,
                        LotImpl.class, lot.getPrimaryKey(), this) == null) {
                cacheResult(lot);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(LotImpl.class.getName());
        EntityCacheUtil.clearCache(LotImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public Lot create(String code) {
        Lot lot = new LotImpl();

        lot.setNew(true);
        lot.setPrimaryKey(code);

        return lot;
    }

    public Lot remove(String code) throws NoSuchLotException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Lot lot = (Lot) session.get(LotImpl.class, code);

            if (lot == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No Lot exists with the primary key " + code);
                }

                throw new NoSuchLotException(
                    "No Lot exists with the primary key " + code);
            }

            return remove(lot);
        } catch (NoSuchLotException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public Lot remove(Lot lot) throws SystemException {
        for (ModelListener<Lot> listener : listeners) {
            listener.onBeforeRemove(lot);
        }

        lot = removeImpl(lot);

        for (ModelListener<Lot> listener : listeners) {
            listener.onAfterRemove(lot);
        }

        return lot;
    }

    protected Lot removeImpl(Lot lot) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (lot.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(LotImpl.class,
                        lot.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(lot);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        LotModelImpl lotModelImpl = (LotModelImpl) lot;

        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
            new Object[] { new Integer(lotModelImpl.getOriginalLotNumber()) });

        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NUMBER,
            new Object[] { new Integer(lotModelImpl.getOriginalNumber()) });

        EntityCacheUtil.removeResult(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotImpl.class, lot.getPrimaryKey());

        return lot;
    }

    /**
     * @deprecated Use <code>update(Lot lot, boolean merge)</code>.
     */
    public Lot update(Lot lot) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(Lot lot) method. Use update(Lot lot, boolean merge) instead.");
        }

        return update(lot, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lot the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lot is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public Lot update(Lot lot, boolean merge) throws SystemException {
        boolean isNew = lot.isNew();

        for (ModelListener<Lot> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(lot);
            } else {
                listener.onBeforeUpdate(lot);
            }
        }

        lot = updateImpl(lot, merge);

        for (ModelListener<Lot> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(lot);
            } else {
                listener.onAfterUpdate(lot);
            }
        }

        return lot;
    }

    public Lot updateImpl(be.lions.duckrace.model.Lot lot, boolean merge)
        throws SystemException {
        boolean isNew = lot.isNew();

        LotModelImpl lotModelImpl = (LotModelImpl) lot;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, lot, merge);

            lot.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(LotModelImpl.ENTITY_CACHE_ENABLED,
            LotImpl.class, lot.getPrimaryKey(), lot);

        if (!isNew &&
                (lot.getLotNumber() != lotModelImpl.getOriginalLotNumber())) {
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                new Object[] { new Integer(lotModelImpl.getOriginalLotNumber()) });
        }

        if (isNew ||
                (lot.getLotNumber() != lotModelImpl.getOriginalLotNumber())) {
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                new Object[] { new Integer(lot.getLotNumber()) }, lot);
        }

        if (!isNew && (lot.getNumber() != lotModelImpl.getOriginalNumber())) {
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NUMBER,
                new Object[] { new Integer(lotModelImpl.getOriginalNumber()) });
        }

        if (isNew || (lot.getNumber() != lotModelImpl.getOriginalNumber())) {
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
                new Object[] { new Integer(lot.getNumber()) }, lot);
        }

        return lot;
    }

    public Lot findByPrimaryKey(String code)
        throws NoSuchLotException, SystemException {
        Lot lot = fetchByPrimaryKey(code);

        if (lot == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No Lot exists with the primary key " + code);
            }

            throw new NoSuchLotException("No Lot exists with the primary key " +
                code);
        }

        return lot;
    }

    public Lot fetchByPrimaryKey(String code) throws SystemException {
        Lot lot = (Lot) EntityCacheUtil.getResult(LotModelImpl.ENTITY_CACHE_ENABLED,
                LotImpl.class, code, this);

        if (lot == null) {
            Session session = null;

            try {
                session = openSession();

                lot = (Lot) session.get(LotImpl.class, code);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (lot != null) {
                    cacheResult(lot);
                }

                closeSession(session);
            }
        }

        return lot;
    }

    public List<Lot> findByParticipant(long participantId)
        throws SystemException {
        Object[] finderArgs = new Object[] { new Long(participantId) };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_PARTICIPANT,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.participantId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(participantId);

                list = q.list();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_PARTICIPANT,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public List<Lot> findByParticipant(long participantId, int start, int end)
        throws SystemException {
        return findByParticipant(participantId, start, end, null);
    }

    public List<Lot> findByParticipant(long participantId, int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                new Long(participantId),
                
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_OBC_PARTICIPANT,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.participantId = ?");

                query.append(" ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lot.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(participantId);

                list = (List<Lot>) QueryUtil.list(q, getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_OBC_PARTICIPANT,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public Lot findByParticipant_First(long participantId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        List<Lot> list = findByParticipant(participantId, 0, 1, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("participantId=" + participantId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot findByParticipant_Last(long participantId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        int count = countByParticipant(participantId);

        List<Lot> list = findByParticipant(participantId, count - 1, count, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("participantId=" + participantId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot[] findByParticipant_PrevAndNext(String code, long participantId,
        OrderByComparator obc) throws NoSuchLotException, SystemException {
        Lot lot = findByPrimaryKey(code);

        int count = countByParticipant(participantId);

        Session session = null;

        try {
            session = openSession();

            StringBuilder query = new StringBuilder();

            query.append("SELECT lot FROM Lot lot WHERE ");

            query.append("lot.participantId = ?");

            query.append(" ");

            if (obc != null) {
                query.append("ORDER BY ");

                String[] orderByFields = obc.getOrderByFields();

                for (int i = 0; i < orderByFields.length; i++) {
                    query.append("lot.");
                    query.append(orderByFields[i]);

                    if (obc.isAscending()) {
                        query.append(" ASC");
                    } else {
                        query.append(" DESC");
                    }

                    if ((i + 1) < orderByFields.length) {
                        query.append(", ");
                    }
                }
            }

            Query q = session.createQuery(query.toString());

            QueryPos qPos = QueryPos.getInstance(q);

            qPos.add(participantId);

            Object[] objArray = QueryUtil.getPrevAndNext(q, count, obc, lot);

            Lot[] array = new LotImpl[3];

            array[0] = (Lot) objArray[0];
            array[1] = (Lot) objArray[1];
            array[2] = (Lot) objArray[2];

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Lot> findByReservation(long reservationId)
        throws SystemException {
        Object[] finderArgs = new Object[] { new Long(reservationId) };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_RESERVATION,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.reservationId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(reservationId);

                list = q.list();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_RESERVATION,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public List<Lot> findByReservation(long reservationId, int start, int end)
        throws SystemException {
        return findByReservation(reservationId, start, end, null);
    }

    public List<Lot> findByReservation(long reservationId, int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                new Long(reservationId),
                
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_OBC_RESERVATION,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.reservationId = ?");

                query.append(" ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lot.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(reservationId);

                list = (List<Lot>) QueryUtil.list(q, getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_OBC_RESERVATION,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public Lot findByReservation_First(long reservationId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        List<Lot> list = findByReservation(reservationId, 0, 1, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("reservationId=" + reservationId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot findByReservation_Last(long reservationId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        int count = countByReservation(reservationId);

        List<Lot> list = findByReservation(reservationId, count - 1, count, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("reservationId=" + reservationId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot[] findByReservation_PrevAndNext(String code, long reservationId,
        OrderByComparator obc) throws NoSuchLotException, SystemException {
        Lot lot = findByPrimaryKey(code);

        int count = countByReservation(reservationId);

        Session session = null;

        try {
            session = openSession();

            StringBuilder query = new StringBuilder();

            query.append("SELECT lot FROM Lot lot WHERE ");

            query.append("lot.reservationId = ?");

            query.append(" ");

            if (obc != null) {
                query.append("ORDER BY ");

                String[] orderByFields = obc.getOrderByFields();

                for (int i = 0; i < orderByFields.length; i++) {
                    query.append("lot.");
                    query.append(orderByFields[i]);

                    if (obc.isAscending()) {
                        query.append(" ASC");
                    } else {
                        query.append(" DESC");
                    }

                    if ((i + 1) < orderByFields.length) {
                        query.append(", ");
                    }
                }
            }

            Query q = session.createQuery(query.toString());

            QueryPos qPos = QueryPos.getInstance(q);

            qPos.add(reservationId);

            Object[] objArray = QueryUtil.getPrevAndNext(q, count, obc, lot);

            Lot[] array = new LotImpl[3];

            array[0] = (Lot) objArray[0];
            array[1] = (Lot) objArray[1];
            array[2] = (Lot) objArray[2];

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public Lot findByLotNumber(int lotNumber)
        throws NoSuchLotException, SystemException {
        Lot lot = fetchByLotNumber(lotNumber);

        if (lot == null) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("lotNumber=" + lotNumber);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchLotException(msg.toString());
        }

        return lot;
    }

    public Lot fetchByLotNumber(int lotNumber) throws SystemException {
        return fetchByLotNumber(lotNumber, true);
    }

    public Lot fetchByLotNumber(int lotNumber, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { new Integer(lotNumber) };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                    finderArgs, this);
        }

        if (result == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.lotNumber = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(lotNumber);

                List<Lot> list = q.list();

                result = list;

                Lot lot = null;

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                        finderArgs, list);
                } else {
                    lot = list.get(0);

                    cacheResult(lot);

                    if ((lot.getLotNumber() != lotNumber)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                            finderArgs, lot);
                    }
                }

                return lot;
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (result == null) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
                        finderArgs, new ArrayList<Lot>());
                }

                closeSession(session);
            }
        } else {
            if (result instanceof List) {
                return null;
            } else {
                return (Lot) result;
            }
        }
    }

    public Lot findByNumber(int number)
        throws NoSuchLotException, SystemException {
        Lot lot = fetchByNumber(number);

        if (lot == null) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("number=" + number);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchLotException(msg.toString());
        }

        return lot;
    }

    public Lot fetchByNumber(int number) throws SystemException {
        return fetchByNumber(number, true);
    }

    public Lot fetchByNumber(int number, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { new Integer(number) };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_NUMBER,
                    finderArgs, this);
        }

        if (result == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.number = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(number);

                List<Lot> list = q.list();

                result = list;

                Lot lot = null;

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
                        finderArgs, list);
                } else {
                    lot = list.get(0);

                    cacheResult(lot);

                    if ((lot.getNumber() != number)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
                            finderArgs, lot);
                    }
                }

                return lot;
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (result == null) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
                        finderArgs, new ArrayList<Lot>());
                }

                closeSession(session);
            }
        } else {
            if (result instanceof List) {
                return null;
            } else {
                return (Lot) result;
            }
        }
    }

    public List<Lot> findByPartner(long partnerId) throws SystemException {
        Object[] finderArgs = new Object[] { new Long(partnerId) };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_PARTNER,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.partnerId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(partnerId);

                list = q.list();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_PARTNER,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public List<Lot> findByPartner(long partnerId, int start, int end)
        throws SystemException {
        return findByPartner(partnerId, start, end, null);
    }

    public List<Lot> findByPartner(long partnerId, int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                new Long(partnerId),
                
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_OBC_PARTNER,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot WHERE ");

                query.append("lot.partnerId = ?");

                query.append(" ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lot.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(partnerId);

                list = (List<Lot>) QueryUtil.list(q, getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_OBC_PARTNER,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public Lot findByPartner_First(long partnerId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        List<Lot> list = findByPartner(partnerId, 0, 1, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("partnerId=" + partnerId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot findByPartner_Last(long partnerId, OrderByComparator obc)
        throws NoSuchLotException, SystemException {
        int count = countByPartner(partnerId);

        List<Lot> list = findByPartner(partnerId, count - 1, count, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Lot exists with the key {");

            msg.append("partnerId=" + partnerId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Lot[] findByPartner_PrevAndNext(String code, long partnerId,
        OrderByComparator obc) throws NoSuchLotException, SystemException {
        Lot lot = findByPrimaryKey(code);

        int count = countByPartner(partnerId);

        Session session = null;

        try {
            session = openSession();

            StringBuilder query = new StringBuilder();

            query.append("SELECT lot FROM Lot lot WHERE ");

            query.append("lot.partnerId = ?");

            query.append(" ");

            if (obc != null) {
                query.append("ORDER BY ");

                String[] orderByFields = obc.getOrderByFields();

                for (int i = 0; i < orderByFields.length; i++) {
                    query.append("lot.");
                    query.append(orderByFields[i]);

                    if (obc.isAscending()) {
                        query.append(" ASC");
                    } else {
                        query.append(" DESC");
                    }

                    if ((i + 1) < orderByFields.length) {
                        query.append(", ");
                    }
                }
            }

            Query q = session.createQuery(query.toString());

            QueryPos qPos = QueryPos.getInstance(q);

            qPos.add(partnerId);

            Object[] objArray = QueryUtil.getPrevAndNext(q, count, obc, lot);

            Lot[] array = new LotImpl[3];

            array[0] = (Lot) objArray[0];
            array[1] = (Lot) objArray[1];
            array[2] = (Lot) objArray[2];

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Lot> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<Lot> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    public List<Lot> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Lot> list = (List<Lot>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT lot FROM Lot lot ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lot.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<Lot>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Lot>) QueryUtil.list(q, getDialect(), start,
                            end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Lot>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeByParticipant(long participantId)
        throws SystemException {
        for (Lot lot : findByParticipant(participantId)) {
            remove(lot);
        }
    }

    public void removeByReservation(long reservationId)
        throws SystemException {
        for (Lot lot : findByReservation(reservationId)) {
            remove(lot);
        }
    }

    public void removeByLotNumber(int lotNumber)
        throws NoSuchLotException, SystemException {
        Lot lot = findByLotNumber(lotNumber);

        remove(lot);
    }

    public void removeByNumber(int number)
        throws NoSuchLotException, SystemException {
        Lot lot = findByNumber(number);

        remove(lot);
    }

    public void removeByPartner(long partnerId) throws SystemException {
        for (Lot lot : findByPartner(partnerId)) {
            remove(lot);
        }
    }

    public void removeAll() throws SystemException {
        for (Lot lot : findAll()) {
            remove(lot);
        }
    }

    public int countByParticipant(long participantId) throws SystemException {
        Object[] finderArgs = new Object[] { new Long(participantId) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PARTICIPANT,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lot) ");
                query.append("FROM Lot lot WHERE ");

                query.append("lot.participantId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(participantId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PARTICIPANT,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countByReservation(long reservationId) throws SystemException {
        Object[] finderArgs = new Object[] { new Long(reservationId) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_RESERVATION,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lot) ");
                query.append("FROM Lot lot WHERE ");

                query.append("lot.reservationId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(reservationId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_RESERVATION,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countByLotNumber(int lotNumber) throws SystemException {
        Object[] finderArgs = new Object[] { new Integer(lotNumber) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LOTNUMBER,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lot) ");
                query.append("FROM Lot lot WHERE ");

                query.append("lot.lotNumber = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(lotNumber);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LOTNUMBER,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countByNumber(int number) throws SystemException {
        Object[] finderArgs = new Object[] { new Integer(number) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_NUMBER,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lot) ");
                query.append("FROM Lot lot WHERE ");

                query.append("lot.number = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(number);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NUMBER,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countByPartner(long partnerId) throws SystemException {
        Object[] finderArgs = new Object[] { new Long(partnerId) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PARTNER,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lot) ");
                query.append("FROM Lot lot WHERE ");

                query.append("lot.partnerId = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(partnerId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PARTNER,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery("SELECT COUNT(lot) FROM Lot lot");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.portal.util.PropsUtil.get(
                        "value.object.listener.be.lions.duckrace.model.Lot")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Lot>> listenersList = new ArrayList<ModelListener<Lot>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Lot>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
