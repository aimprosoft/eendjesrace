package be.lions.duckrace.service.persistence;

import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.impl.ParticipantImpl;
import be.lions.duckrace.model.impl.ParticipantModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ParticipantPersistenceImpl extends BasePersistenceImpl
    implements ParticipantPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = ParticipantImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FETCH_BY_MOBILENUMBER = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_ENTITY, "fetchByMobileNumber",
            new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_COUNT_BY_MOBILENUMBER = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByMobileNumber", new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(ParticipantPersistenceImpl.class);
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected be.lions.duckrace.service.persistence.ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected be.lions.duckrace.service.persistence.PartnerPersistence partnerPersistence;

    public void cacheResult(Participant participant) {
        EntityCacheUtil.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantImpl.class, participant.getPrimaryKey(), participant);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
            new Object[] { participant.getMobileNumber() }, participant);
    }

    public void cacheResult(List<Participant> participants) {
        for (Participant participant : participants) {
            if (EntityCacheUtil.getResult(
                        ParticipantModelImpl.ENTITY_CACHE_ENABLED,
                        ParticipantImpl.class, participant.getPrimaryKey(), this) == null) {
                cacheResult(participant);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(ParticipantImpl.class.getName());
        EntityCacheUtil.clearCache(ParticipantImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public Participant create(long participantId) {
        Participant participant = new ParticipantImpl();

        participant.setNew(true);
        participant.setPrimaryKey(participantId);

        return participant;
    }

    public Participant remove(long participantId)
        throws NoSuchParticipantException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Participant participant = (Participant) session.get(ParticipantImpl.class,
                    new Long(participantId));

            if (participant == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No Participant exists with the primary key " +
                        participantId);
                }

                throw new NoSuchParticipantException(
                    "No Participant exists with the primary key " +
                    participantId);
            }

            return remove(participant);
        } catch (NoSuchParticipantException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public Participant remove(Participant participant)
        throws SystemException {
        for (ModelListener<Participant> listener : listeners) {
            listener.onBeforeRemove(participant);
        }

        participant = removeImpl(participant);

        for (ModelListener<Participant> listener : listeners) {
            listener.onAfterRemove(participant);
        }

        return participant;
    }

    protected Participant removeImpl(Participant participant)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (participant.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(ParticipantImpl.class,
                        participant.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(participant);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        ParticipantModelImpl participantModelImpl = (ParticipantModelImpl) participant;

        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
            new Object[] { participantModelImpl.getOriginalMobileNumber() });

        EntityCacheUtil.removeResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantImpl.class, participant.getPrimaryKey());

        return participant;
    }

    /**
     * @deprecated Use <code>update(Participant participant, boolean merge)</code>.
     */
    public Participant update(Participant participant)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(Participant participant) method. Use update(Participant participant, boolean merge) instead.");
        }

        return update(participant, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                participant the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when participant is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public Participant update(Participant participant, boolean merge)
        throws SystemException {
        boolean isNew = participant.isNew();

        for (ModelListener<Participant> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(participant);
            } else {
                listener.onBeforeUpdate(participant);
            }
        }

        participant = updateImpl(participant, merge);

        for (ModelListener<Participant> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(participant);
            } else {
                listener.onAfterUpdate(participant);
            }
        }

        return participant;
    }

    public Participant updateImpl(
        be.lions.duckrace.model.Participant participant, boolean merge)
        throws SystemException {
        boolean isNew = participant.isNew();

        ParticipantModelImpl participantModelImpl = (ParticipantModelImpl) participant;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, participant, merge);

            participant.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
            ParticipantImpl.class, participant.getPrimaryKey(), participant);

        if (!isNew &&
                (!Validator.equals(participant.getMobileNumber(),
                    participantModelImpl.getOriginalMobileNumber()))) {
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                new Object[] { participantModelImpl.getOriginalMobileNumber() });
        }

        if (isNew ||
                (!Validator.equals(participant.getMobileNumber(),
                    participantModelImpl.getOriginalMobileNumber()))) {
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                new Object[] { participant.getMobileNumber() }, participant);
        }

        return participant;
    }

    public Participant findByPrimaryKey(long participantId)
        throws NoSuchParticipantException, SystemException {
        Participant participant = fetchByPrimaryKey(participantId);

        if (participant == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No Participant exists with the primary key " +
                    participantId);
            }

            throw new NoSuchParticipantException(
                "No Participant exists with the primary key " + participantId);
        }

        return participant;
    }

    public Participant fetchByPrimaryKey(long participantId)
        throws SystemException {
        Participant participant = (Participant) EntityCacheUtil.getResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
                ParticipantImpl.class, participantId, this);

        if (participant == null) {
            Session session = null;

            try {
                session = openSession();

                participant = (Participant) session.get(ParticipantImpl.class,
                        new Long(participantId));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (participant != null) {
                    cacheResult(participant);
                }

                closeSession(session);
            }
        }

        return participant;
    }

    public Participant findByMobileNumber(String mobileNumber)
        throws NoSuchParticipantException, SystemException {
        Participant participant = fetchByMobileNumber(mobileNumber);

        if (participant == null) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Participant exists with the key {");

            msg.append("mobileNumber=" + mobileNumber);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchParticipantException(msg.toString());
        }

        return participant;
    }

    public Participant fetchByMobileNumber(String mobileNumber)
        throws SystemException {
        return fetchByMobileNumber(mobileNumber, true);
    }

    public Participant fetchByMobileNumber(String mobileNumber,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { mobileNumber };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                    finderArgs, this);
        }

        if (result == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append(
                    "SELECT participant FROM Participant participant WHERE ");

                if (mobileNumber == null) {
                    query.append("participant.mobileNumber IS NULL");
                } else {
                    query.append("participant.mobileNumber = ?");
                }

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                if (mobileNumber != null) {
                    qPos.add(mobileNumber);
                }

                List<Participant> list = q.list();

                result = list;

                Participant participant = null;

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                        finderArgs, list);
                } else {
                    participant = list.get(0);

                    cacheResult(participant);

                    if ((participant.getMobileNumber() == null) ||
                            !participant.getMobileNumber().equals(mobileNumber)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                            finderArgs, participant);
                    }
                }

                return participant;
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (result == null) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
                        finderArgs, new ArrayList<Participant>());
                }

                closeSession(session);
            }
        } else {
            if (result instanceof List) {
                return null;
            } else {
                return (Participant) result;
            }
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Participant> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<Participant> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<Participant> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Participant> list = (List<Participant>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT participant FROM Participant participant ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("participant.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<Participant>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Participant>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Participant>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeByMobileNumber(String mobileNumber)
        throws NoSuchParticipantException, SystemException {
        Participant participant = findByMobileNumber(mobileNumber);

        remove(participant);
    }

    public void removeAll() throws SystemException {
        for (Participant participant : findAll()) {
            remove(participant);
        }
    }

    public int countByMobileNumber(String mobileNumber)
        throws SystemException {
        Object[] finderArgs = new Object[] { mobileNumber };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_MOBILENUMBER,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(participant) ");
                query.append("FROM Participant participant WHERE ");

                if (mobileNumber == null) {
                    query.append("participant.mobileNumber IS NULL");
                } else {
                    query.append("participant.mobileNumber = ?");
                }

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                if (mobileNumber != null) {
                    qPos.add(mobileNumber);
                }

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_MOBILENUMBER,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(participant) FROM Participant participant");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.portal.util.PropsUtil.get(
                        "value.object.listener.be.lions.duckrace.model.Participant")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Participant>> listenersList = new ArrayList<ModelListener<Participant>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Participant>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
