package be.lions.duckrace.service.persistence;

import be.lions.duckrace.NoSuchPartnerException;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.model.impl.PartnerImpl;
import be.lions.duckrace.model.impl.PartnerModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PartnerPersistenceImpl extends BasePersistenceImpl
    implements PartnerPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = PartnerImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_BY_CATEGORY = new FinderPath(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByCategory", new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_OBC_CATEGORY = new FinderPath(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findByCategory",
            new String[] {
                String.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORY = new FinderPath(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countByCategory", new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(PartnerPersistenceImpl.class);
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected be.lions.duckrace.service.persistence.ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected be.lions.duckrace.service.persistence.PartnerPersistence partnerPersistence;

    public void cacheResult(Partner partner) {
        EntityCacheUtil.putResult(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerImpl.class, partner.getPrimaryKey(), partner);
    }

    public void cacheResult(List<Partner> partners) {
        for (Partner partner : partners) {
            if (EntityCacheUtil.getResult(
                        PartnerModelImpl.ENTITY_CACHE_ENABLED,
                        PartnerImpl.class, partner.getPrimaryKey(), this) == null) {
                cacheResult(partner);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(PartnerImpl.class.getName());
        EntityCacheUtil.clearCache(PartnerImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public Partner create(long partnerId) {
        Partner partner = new PartnerImpl();

        partner.setNew(true);
        partner.setPrimaryKey(partnerId);

        return partner;
    }

    public Partner remove(long partnerId)
        throws NoSuchPartnerException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Partner partner = (Partner) session.get(PartnerImpl.class,
                    new Long(partnerId));

            if (partner == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No Partner exists with the primary key " +
                        partnerId);
                }

                throw new NoSuchPartnerException(
                    "No Partner exists with the primary key " + partnerId);
            }

            return remove(partner);
        } catch (NoSuchPartnerException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public Partner remove(Partner partner) throws SystemException {
        for (ModelListener<Partner> listener : listeners) {
            listener.onBeforeRemove(partner);
        }

        partner = removeImpl(partner);

        for (ModelListener<Partner> listener : listeners) {
            listener.onAfterRemove(partner);
        }

        return partner;
    }

    protected Partner removeImpl(Partner partner) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (partner.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(PartnerImpl.class,
                        partner.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(partner);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerImpl.class, partner.getPrimaryKey());

        return partner;
    }

    /**
     * @deprecated Use <code>update(Partner partner, boolean merge)</code>.
     */
    public Partner update(Partner partner) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(Partner partner) method. Use update(Partner partner, boolean merge) instead.");
        }

        return update(partner, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                partner the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when partner is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public Partner update(Partner partner, boolean merge)
        throws SystemException {
        boolean isNew = partner.isNew();

        for (ModelListener<Partner> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(partner);
            } else {
                listener.onBeforeUpdate(partner);
            }
        }

        partner = updateImpl(partner, merge);

        for (ModelListener<Partner> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(partner);
            } else {
                listener.onAfterUpdate(partner);
            }
        }

        return partner;
    }

    public Partner updateImpl(be.lions.duckrace.model.Partner partner,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, partner, merge);

            partner.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(PartnerModelImpl.ENTITY_CACHE_ENABLED,
            PartnerImpl.class, partner.getPrimaryKey(), partner);

        return partner;
    }

    public Partner findByPrimaryKey(long partnerId)
        throws NoSuchPartnerException, SystemException {
        Partner partner = fetchByPrimaryKey(partnerId);

        if (partner == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No Partner exists with the primary key " +
                    partnerId);
            }

            throw new NoSuchPartnerException(
                "No Partner exists with the primary key " + partnerId);
        }

        return partner;
    }

    public Partner fetchByPrimaryKey(long partnerId) throws SystemException {
        Partner partner = (Partner) EntityCacheUtil.getResult(PartnerModelImpl.ENTITY_CACHE_ENABLED,
                PartnerImpl.class, partnerId, this);

        if (partner == null) {
            Session session = null;

            try {
                session = openSession();

                partner = (Partner) session.get(PartnerImpl.class,
                        new Long(partnerId));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (partner != null) {
                    cacheResult(partner);
                }

                closeSession(session);
            }
        }

        return partner;
    }

    public List<Partner> findByCategory(String category)
        throws SystemException {
        Object[] finderArgs = new Object[] { category };

        List<Partner> list = (List<Partner>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_CATEGORY,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT partner FROM Partner partner WHERE ");

                if (category == null) {
                    query.append("partner.category IS NULL");
                } else {
                    query.append("partner.lower(category) = ?");
                }

                query.append(" ");

                query.append("ORDER BY ");

                query.append("partner.category ASC");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                if (category != null) {
                    qPos.add(category);
                }

                list = q.list();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Partner>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_CATEGORY,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public List<Partner> findByCategory(String category, int start, int end)
        throws SystemException {
        return findByCategory(category, start, end, null);
    }

    public List<Partner> findByCategory(String category, int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                category,
                
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Partner> list = (List<Partner>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_OBC_CATEGORY,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT partner FROM Partner partner WHERE ");

                if (category == null) {
                    query.append("partner.category IS NULL");
                } else {
                    query.append("partner.lower(category) = ?");
                }

                query.append(" ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("partner.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }
                else {
                    query.append("ORDER BY ");

                    query.append("partner.category ASC");
                }

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                if (category != null) {
                    qPos.add(category);
                }

                list = (List<Partner>) QueryUtil.list(q, getDialect(), start,
                        end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Partner>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_OBC_CATEGORY,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public Partner findByCategory_First(String category, OrderByComparator obc)
        throws NoSuchPartnerException, SystemException {
        List<Partner> list = findByCategory(category, 0, 1, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Partner exists with the key {");

            msg.append("category=" + category);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchPartnerException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Partner findByCategory_Last(String category, OrderByComparator obc)
        throws NoSuchPartnerException, SystemException {
        int count = countByCategory(category);

        List<Partner> list = findByCategory(category, count - 1, count, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No Partner exists with the key {");

            msg.append("category=" + category);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchPartnerException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public Partner[] findByCategory_PrevAndNext(long partnerId,
        String category, OrderByComparator obc)
        throws NoSuchPartnerException, SystemException {
        Partner partner = findByPrimaryKey(partnerId);

        int count = countByCategory(category);

        Session session = null;

        try {
            session = openSession();

            StringBuilder query = new StringBuilder();

            query.append("SELECT partner FROM Partner partner WHERE ");

            if (category == null) {
                query.append("partner.category IS NULL");
            } else {
                query.append("partner.lower(category) = ?");
            }

            query.append(" ");

            if (obc != null) {
                query.append("ORDER BY ");

                String[] orderByFields = obc.getOrderByFields();

                for (int i = 0; i < orderByFields.length; i++) {
                    query.append("partner.");
                    query.append(orderByFields[i]);

                    if (obc.isAscending()) {
                        query.append(" ASC");
                    } else {
                        query.append(" DESC");
                    }

                    if ((i + 1) < orderByFields.length) {
                        query.append(", ");
                    }
                }
            }
            else {
                query.append("ORDER BY ");

                query.append("partner.category ASC");
            }

            Query q = session.createQuery(query.toString());

            QueryPos qPos = QueryPos.getInstance(q);

            if (category != null) {
                qPos.add(category);
            }

            Object[] objArray = QueryUtil.getPrevAndNext(q, count, obc, partner);

            Partner[] array = new PartnerImpl[3];

            array[0] = (Partner) objArray[0];
            array[1] = (Partner) objArray[1];
            array[2] = (Partner) objArray[2];

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Partner> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<Partner> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    public List<Partner> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<Partner> list = (List<Partner>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT partner FROM Partner partner ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("partner.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }
                else {
                    query.append("ORDER BY ");

                    query.append("partner.category ASC");
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<Partner>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Partner>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<Partner>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeByCategory(String category) throws SystemException {
        for (Partner partner : findByCategory(category)) {
            remove(partner);
        }
    }

    public void removeAll() throws SystemException {
        for (Partner partner : findAll()) {
            remove(partner);
        }
    }

    public int countByCategory(String category) throws SystemException {
        Object[] finderArgs = new Object[] { category };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_CATEGORY,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(partner) ");
                query.append("FROM Partner partner WHERE ");

                if (category == null) {
                    query.append("partner.category IS NULL");
                } else {
                    query.append("partner.lower(category) = ?");
                }

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                if (category != null) {
                    qPos.add(category);
                }

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CATEGORY,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(partner) FROM Partner partner");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.portal.util.PropsUtil.get(
                        "value.object.listener.be.lions.duckrace.model.Partner")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Partner>> listenersList = new ArrayList<ModelListener<Partner>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Partner>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
