package be.lions.duckrace.service.impl;

import java.util.Date;
import java.util.List;

import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException;
import be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.base.LotReservationLocalServiceBaseImpl;

import com.liferay.counter.service.persistence.CounterUtil;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;


public class LotReservationLocalServiceImpl extends LotReservationLocalServiceBaseImpl {
	
	public LotReservation createReservation(String mobileNumber, String firstName, String lastName, String emailAddress, boolean contestParticipant, boolean mercedesDriver, String carBrand, boolean mercedesOld, int numberLots, String language) throws SystemException, InvalidMobileNumberException, NoSuchParticipantException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		Participant participant = participantLocalService.getOrCreateParticipant(mobileNumber, firstName, lastName, emailAddress, contestParticipant, mercedesDriver, carBrand, mercedesOld, language);
		
		long id = CounterUtil.increment();
		LotReservation reservation = lotReservationPersistence.create(id);
		reservation.setParticipantId(participant.getParticipantId());
		reservation.setNumberLots(numberLots);
		reservation.setApproved(false);
		reservation.setReservationDate(new Date());
		return lotReservationPersistence.update(reservation, false);
	}
	
	public LotReservation approveReservation(long reservationId) throws SystemException, InvalidMobileNumberException, NoSuchLotException, NoSuchParticipantException, PortalException {
		LotReservation reservation = lotReservationPersistence.findByPrimaryKey(reservationId);
		if(reservation.isApproved()) {
			return reservation;
		}
		for (int i=0; i<reservation.getNumberLots(); i++) {
			lotLocalService.assignFreeLotToParticipant(reservation);
		}
		reservation.setApproved(true);
		return lotReservationPersistence.update(reservation, false);
	}
	
	public List<LotReservation> getUnapprovedReservations() throws SystemException {
		return lotReservationPersistence.findByApproved(false);
	}
	
	public List<LotReservation> getApprovedReservations() throws SystemException {
		return lotReservationPersistence.findByApproved(true);
	}
	
}
