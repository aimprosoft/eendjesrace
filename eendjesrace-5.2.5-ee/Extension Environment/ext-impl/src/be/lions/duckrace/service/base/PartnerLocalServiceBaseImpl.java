package be.lions.duckrace.service.base;

import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.DuckRaceLocalService;
import be.lions.duckrace.service.LotLocalService;
import be.lions.duckrace.service.LotReservationLocalService;
import be.lions.duckrace.service.ParticipantLocalService;
import be.lions.duckrace.service.PartnerLocalService;
import be.lions.duckrace.service.persistence.LotPersistence;
import be.lions.duckrace.service.persistence.LotReservationPersistence;
import be.lions.duckrace.service.persistence.ParticipantPersistence;
import be.lions.duckrace.service.persistence.PartnerPersistence;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.util.PortalUtil;

import java.util.List;


public abstract class PartnerLocalServiceBaseImpl implements PartnerLocalService {
    @BeanReference(name = "be.lions.duckrace.service.LotLocalService.impl")
    protected LotLocalService lotLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.ParticipantLocalService.impl")
    protected ParticipantLocalService participantLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.LotReservationLocalService.impl")
    protected LotReservationLocalService lotReservationLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.PartnerLocalService.impl")
    protected PartnerLocalService partnerLocalService;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected PartnerPersistence partnerPersistence;
    @BeanReference(name = "be.lions.duckrace.service.DuckRaceLocalService.impl")
    protected DuckRaceLocalService duckRaceLocalService;

    public Partner addPartner(Partner partner) throws SystemException {
        partner.setNew(true);

        return partnerPersistence.update(partner, false);
    }

    public Partner createPartner(long partnerId) {
        return partnerPersistence.create(partnerId);
    }

    public void deletePartner(long partnerId)
        throws PortalException, SystemException {
        partnerPersistence.remove(partnerId);
    }

    public void deletePartner(Partner partner) throws SystemException {
        partnerPersistence.remove(partner);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return partnerPersistence.findWithDynamicQuery(dynamicQuery);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery, int start,
        int end) throws SystemException {
        return partnerPersistence.findWithDynamicQuery(dynamicQuery, start, end);
    }

    public Partner getPartner(long partnerId)
        throws PortalException, SystemException {
        return partnerPersistence.findByPrimaryKey(partnerId);
    }

    public List<Partner> getPartners(int start, int end)
        throws SystemException {
        return partnerPersistence.findAll(start, end);
    }

    public int getPartnersCount() throws SystemException {
        return partnerPersistence.countAll();
    }

    public Partner updatePartner(Partner partner) throws SystemException {
        partner.setNew(false);

        return partnerPersistence.update(partner, true);
    }

    public Partner updatePartner(Partner partner, boolean merge)
        throws SystemException {
        partner.setNew(false);

        return partnerPersistence.update(partner, merge);
    }

    public LotLocalService getLotLocalService() {
        return lotLocalService;
    }

    public void setLotLocalService(LotLocalService lotLocalService) {
        this.lotLocalService = lotLocalService;
    }

    public LotPersistence getLotPersistence() {
        return lotPersistence;
    }

    public void setLotPersistence(LotPersistence lotPersistence) {
        this.lotPersistence = lotPersistence;
    }

    public ParticipantLocalService getParticipantLocalService() {
        return participantLocalService;
    }

    public void setParticipantLocalService(
        ParticipantLocalService participantLocalService) {
        this.participantLocalService = participantLocalService;
    }

    public ParticipantPersistence getParticipantPersistence() {
        return participantPersistence;
    }

    public void setParticipantPersistence(
        ParticipantPersistence participantPersistence) {
        this.participantPersistence = participantPersistence;
    }

    public LotReservationLocalService getLotReservationLocalService() {
        return lotReservationLocalService;
    }

    public void setLotReservationLocalService(
        LotReservationLocalService lotReservationLocalService) {
        this.lotReservationLocalService = lotReservationLocalService;
    }

    public LotReservationPersistence getLotReservationPersistence() {
        return lotReservationPersistence;
    }

    public void setLotReservationPersistence(
        LotReservationPersistence lotReservationPersistence) {
        this.lotReservationPersistence = lotReservationPersistence;
    }

    public PartnerLocalService getPartnerLocalService() {
        return partnerLocalService;
    }

    public void setPartnerLocalService(PartnerLocalService partnerLocalService) {
        this.partnerLocalService = partnerLocalService;
    }

    public PartnerPersistence getPartnerPersistence() {
        return partnerPersistence;
    }

    public void setPartnerPersistence(PartnerPersistence partnerPersistence) {
        this.partnerPersistence = partnerPersistence;
    }

    public DuckRaceLocalService getDuckRaceLocalService() {
        return duckRaceLocalService;
    }

    public void setDuckRaceLocalService(
        DuckRaceLocalService duckRaceLocalService) {
        this.duckRaceLocalService = duckRaceLocalService;
    }

    protected void runSQL(String sql) throws SystemException {
        try {
            PortalUtil.runSQL(sql);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
