package be.lions.duckrace.service.impl;

import java.util.List;

import be.lions.duckrace.DuplicateMobileNumberException;
import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException;
import be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.base.ParticipantLocalServiceBaseImpl;
import be.lions.duckrace.util.StringUtils;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

public class ParticipantLocalServiceImpl extends ParticipantLocalServiceBaseImpl {
	
	private Participant createParticipant(String mobileNumber) throws SystemException, InvalidMobileNumberException, DuplicateMobileNumberException {
		validate(mobileNumber);
		if(isMobileNumberAlreadyTaken(mobileNumber)) {
			throw new DuplicateMobileNumberException();
		}
		long participantId = CounterLocalServiceUtil.increment();
		Participant participant = participantPersistence.create(participantId);
		participant.setMobileNumber(StringUtils.getStrippedMobileNumber(mobileNumber));
		return participantPersistence.update(participant, false);
	}

	private void validate(String mobileNumber) throws InvalidMobileNumberException {
		if(!StringUtils.isValidMobileNumber(mobileNumber)) {
			throw new InvalidMobileNumberException();
		}
	}
	
	public Participant getOrCreateParticipant(String mobileNumber, String firstName, String lastName, String emailAddress, boolean contestParticipant, boolean mercedesDriver, String carBrand, boolean mercedesOld, String language) throws InvalidMobileNumberException, SystemException, NoSuchParticipantException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		Participant p = getMatchingParticipant(mobileNumber, firstName, lastName, emailAddress);
		if(p == null) {
			try {
				Participant participant = createParticipant(mobileNumber);
				participant.setFirstName(firstName);
				participant.setLastName(lastName);
				participant.setEmailAddress(emailAddress);
				participant.setContestParticipant(contestParticipant);
				participant.setMercedesDriver(mercedesDriver);
				participant.setCarBrand(carBrand);
				participant.setMercedesOld(mercedesOld);
				participant.setLanguage(language);
				return participantPersistence.update(participant, false);
			} catch(DuplicateMobileNumberException e) {
				//cannot occur
			}
		}
		return p;
	}
	
	public Participant getMatchingParticipant(String mobileNumber, String firstName, String lastName, String emailAddress) throws MobileNumberAlreadyUsedPartlyValidDataException, SystemException, MobileNumberAlreadyUsedInvalidDataException {
		try {
			Participant p = getParticipantByMobileNumber(mobileNumber);
			if(p.matchesAll(firstName, lastName, emailAddress)) {
				return p;
			}
			if(p.matchesOneOf(firstName, lastName, emailAddress)) {
				throw new MobileNumberAlreadyUsedPartlyValidDataException(p);
			}
			throw new MobileNumberAlreadyUsedInvalidDataException();
		} catch(NoSuchParticipantException e) {
			return null;
		}
	}
	
	public Participant getParticipantByMobileNumber(String mobileNumber) throws NoSuchParticipantException, SystemException {
		return participantPersistence.findByMobileNumber(StringUtils.getStrippedMobileNumber(mobileNumber));
	}
	
	private boolean isMobileNumberAlreadyTaken(String mobileNumber) throws SystemException {
		try {
			getParticipantByMobileNumber(mobileNumber);
			return true;
		} catch(NoSuchParticipantException e) {
			return false;
		}
	}
	
	public Participant getParticipantByLotNumber(int lotNumber) throws NoSuchLotException, PortalException, SystemException {
		return LotLocalServiceUtil.getLotByNumber(lotNumber).getParticipant();
	}
	
	public Participant updateParticipant(long id, String mobileNumber, String emailAddress, String firstName, String lastName) throws SystemException, PortalException {
		String normalizedMobileNumber = StringUtils.getStrippedMobileNumber(mobileNumber);
		Participant result = getParticipant(id);
		if(!result.getMobileNumber().equals(normalizedMobileNumber) && isMobileNumberAlreadyTaken(normalizedMobileNumber)) {
			throw new DuplicateMobileNumberException();
		}
		result.setMobileNumber(normalizedMobileNumber);
		result.setEmailAddress(emailAddress);
		result.setFirstName(firstName);
		result.setLastName(lastName);
		return updateParticipant(result, false);
	}
	
	@SuppressWarnings("unchecked")
	public List<Participant> searchParticipants(String mobileNumber, String firstName, String lastName, String emailAddress) throws SystemException {
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(Participant.class, PortalClassLoaderUtil.getClassLoader());
		if(!StringUtils.isEmpty(mobileNumber)) {
			query.add(PropertyFactoryUtil.forName("mobileNumber").like("%"+StringUtils.getStrippedMobileNumber(mobileNumber)+"%"));
		}
		if(!StringUtils.isEmpty(emailAddress)) {
			query.add(PropertyFactoryUtil.forName("emailAddress").like("%"+emailAddress+"%"));
		}
		if(!StringUtils.isEmpty(firstName)) {
			query.add(PropertyFactoryUtil.forName("firstName").like("%"+firstName+"%"));
		}
		if(!StringUtils.isEmpty(lastName)) {
			query.add(PropertyFactoryUtil.forName("lastName").like("%"+lastName+"%"));
		}
		return (List<Participant>)((List)dynamicQuery(query));
	}
	
}
