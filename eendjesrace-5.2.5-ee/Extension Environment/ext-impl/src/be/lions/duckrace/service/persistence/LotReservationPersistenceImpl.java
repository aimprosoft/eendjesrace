package be.lions.duckrace.service.persistence;

import be.lions.duckrace.NoSuchLotReservationException;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.impl.LotReservationImpl;
import be.lions.duckrace.model.impl.LotReservationModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class LotReservationPersistenceImpl extends BasePersistenceImpl
    implements LotReservationPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = LotReservationImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_BY_APPROVED = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findByApproved",
            new String[] { Boolean.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_BY_OBC_APPROVED = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findByApproved",
            new String[] {
                Boolean.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_COUNT_BY_APPROVED = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countByApproved",
            new String[] { Boolean.class.getName() });
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(LotReservationPersistenceImpl.class);
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotPersistence lotPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.ParticipantPersistence.impl")
    protected be.lions.duckrace.service.persistence.ParticipantPersistence participantPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.LotReservationPersistence.impl")
    protected be.lions.duckrace.service.persistence.LotReservationPersistence lotReservationPersistence;
    @BeanReference(name = "be.lions.duckrace.service.persistence.PartnerPersistence.impl")
    protected be.lions.duckrace.service.persistence.PartnerPersistence partnerPersistence;

    public void cacheResult(LotReservation lotReservation) {
        EntityCacheUtil.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationImpl.class, lotReservation.getPrimaryKey(),
            lotReservation);
    }

    public void cacheResult(List<LotReservation> lotReservations) {
        for (LotReservation lotReservation : lotReservations) {
            if (EntityCacheUtil.getResult(
                        LotReservationModelImpl.ENTITY_CACHE_ENABLED,
                        LotReservationImpl.class,
                        lotReservation.getPrimaryKey(), this) == null) {
                cacheResult(lotReservation);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(LotReservationImpl.class.getName());
        EntityCacheUtil.clearCache(LotReservationImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public LotReservation create(long reservationId) {
        LotReservation lotReservation = new LotReservationImpl();

        lotReservation.setNew(true);
        lotReservation.setPrimaryKey(reservationId);

        return lotReservation;
    }

    public LotReservation remove(long reservationId)
        throws NoSuchLotReservationException, SystemException {
        Session session = null;

        try {
            session = openSession();

            LotReservation lotReservation = (LotReservation) session.get(LotReservationImpl.class,
                    new Long(reservationId));

            if (lotReservation == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No LotReservation exists with the primary key " +
                        reservationId);
                }

                throw new NoSuchLotReservationException(
                    "No LotReservation exists with the primary key " +
                    reservationId);
            }

            return remove(lotReservation);
        } catch (NoSuchLotReservationException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public LotReservation remove(LotReservation lotReservation)
        throws SystemException {
        for (ModelListener<LotReservation> listener : listeners) {
            listener.onBeforeRemove(lotReservation);
        }

        lotReservation = removeImpl(lotReservation);

        for (ModelListener<LotReservation> listener : listeners) {
            listener.onAfterRemove(lotReservation);
        }

        return lotReservation;
    }

    protected LotReservation removeImpl(LotReservation lotReservation)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (lotReservation.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(LotReservationImpl.class,
                        lotReservation.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(lotReservation);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationImpl.class, lotReservation.getPrimaryKey());

        return lotReservation;
    }

    /**
     * @deprecated Use <code>update(LotReservation lotReservation, boolean merge)</code>.
     */
    public LotReservation update(LotReservation lotReservation)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(LotReservation lotReservation) method. Use update(LotReservation lotReservation, boolean merge) instead.");
        }

        return update(lotReservation, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                lotReservation the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when lotReservation is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public LotReservation update(LotReservation lotReservation, boolean merge)
        throws SystemException {
        boolean isNew = lotReservation.isNew();

        for (ModelListener<LotReservation> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(lotReservation);
            } else {
                listener.onBeforeUpdate(lotReservation);
            }
        }

        lotReservation = updateImpl(lotReservation, merge);

        for (ModelListener<LotReservation> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(lotReservation);
            } else {
                listener.onAfterUpdate(lotReservation);
            }
        }

        return lotReservation;
    }

    public LotReservation updateImpl(
        be.lions.duckrace.model.LotReservation lotReservation, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, lotReservation, merge);

            lotReservation.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
            LotReservationImpl.class, lotReservation.getPrimaryKey(),
            lotReservation);

        return lotReservation;
    }

    public LotReservation findByPrimaryKey(long reservationId)
        throws NoSuchLotReservationException, SystemException {
        LotReservation lotReservation = fetchByPrimaryKey(reservationId);

        if (lotReservation == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No LotReservation exists with the primary key " +
                    reservationId);
            }

            throw new NoSuchLotReservationException(
                "No LotReservation exists with the primary key " +
                reservationId);
        }

        return lotReservation;
    }

    public LotReservation fetchByPrimaryKey(long reservationId)
        throws SystemException {
        LotReservation lotReservation = (LotReservation) EntityCacheUtil.getResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
                LotReservationImpl.class, reservationId, this);

        if (lotReservation == null) {
            Session session = null;

            try {
                session = openSession();

                lotReservation = (LotReservation) session.get(LotReservationImpl.class,
                        new Long(reservationId));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (lotReservation != null) {
                    cacheResult(lotReservation);
                }

                closeSession(session);
            }
        }

        return lotReservation;
    }

    public List<LotReservation> findByApproved(boolean approved)
        throws SystemException {
        Object[] finderArgs = new Object[] { Boolean.valueOf(approved) };

        List<LotReservation> list = (List<LotReservation>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_APPROVED,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append(
                    "SELECT lotReservation FROM LotReservation lotReservation WHERE ");

                query.append("lotReservation.approved = ?");

                query.append(" ");

                query.append("ORDER BY ");

                query.append("lotReservation.reservationDate DESC");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(approved);

                list = q.list();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<LotReservation>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_APPROVED,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public List<LotReservation> findByApproved(boolean approved, int start,
        int end) throws SystemException {
        return findByApproved(approved, start, end, null);
    }

    public List<LotReservation> findByApproved(boolean approved, int start,
        int end, OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                Boolean.valueOf(approved),
                
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<LotReservation> list = (List<LotReservation>) FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_OBC_APPROVED,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append(
                    "SELECT lotReservation FROM LotReservation lotReservation WHERE ");

                query.append("lotReservation.approved = ?");

                query.append(" ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lotReservation.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }
                else {
                    query.append("ORDER BY ");

                    query.append("lotReservation.reservationDate DESC");
                }

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(approved);

                list = (List<LotReservation>) QueryUtil.list(q, getDialect(),
                        start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<LotReservation>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_OBC_APPROVED,
                    finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public LotReservation findByApproved_First(boolean approved,
        OrderByComparator obc)
        throws NoSuchLotReservationException, SystemException {
        List<LotReservation> list = findByApproved(approved, 0, 1, obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No LotReservation exists with the key {");

            msg.append("approved=" + approved);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotReservationException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public LotReservation findByApproved_Last(boolean approved,
        OrderByComparator obc)
        throws NoSuchLotReservationException, SystemException {
        int count = countByApproved(approved);

        List<LotReservation> list = findByApproved(approved, count - 1, count,
                obc);

        if (list.isEmpty()) {
            StringBuilder msg = new StringBuilder();

            msg.append("No LotReservation exists with the key {");

            msg.append("approved=" + approved);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            throw new NoSuchLotReservationException(msg.toString());
        } else {
            return list.get(0);
        }
    }

    public LotReservation[] findByApproved_PrevAndNext(long reservationId,
        boolean approved, OrderByComparator obc)
        throws NoSuchLotReservationException, SystemException {
        LotReservation lotReservation = findByPrimaryKey(reservationId);

        int count = countByApproved(approved);

        Session session = null;

        try {
            session = openSession();

            StringBuilder query = new StringBuilder();

            query.append(
                "SELECT lotReservation FROM LotReservation lotReservation WHERE ");

            query.append("lotReservation.approved = ?");

            query.append(" ");

            if (obc != null) {
                query.append("ORDER BY ");

                String[] orderByFields = obc.getOrderByFields();

                for (int i = 0; i < orderByFields.length; i++) {
                    query.append("lotReservation.");
                    query.append(orderByFields[i]);

                    if (obc.isAscending()) {
                        query.append(" ASC");
                    } else {
                        query.append(" DESC");
                    }

                    if ((i + 1) < orderByFields.length) {
                        query.append(", ");
                    }
                }
            }
            else {
                query.append("ORDER BY ");

                query.append("lotReservation.reservationDate DESC");
            }

            Query q = session.createQuery(query.toString());

            QueryPos qPos = QueryPos.getInstance(q);

            qPos.add(approved);

            Object[] objArray = QueryUtil.getPrevAndNext(q, count, obc,
                    lotReservation);

            LotReservation[] array = new LotReservationImpl[3];

            array[0] = (LotReservation) objArray[0];
            array[1] = (LotReservation) objArray[1];
            array[2] = (LotReservation) objArray[2];

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<LotReservation> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<LotReservation> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<LotReservation> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<LotReservation> list = (List<LotReservation>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append(
                    "SELECT lotReservation FROM LotReservation lotReservation ");

                if (obc != null) {
                    query.append("ORDER BY ");

                    String[] orderByFields = obc.getOrderByFields();

                    for (int i = 0; i < orderByFields.length; i++) {
                        query.append("lotReservation.");
                        query.append(orderByFields[i]);

                        if (obc.isAscending()) {
                            query.append(" ASC");
                        } else {
                            query.append(" DESC");
                        }

                        if ((i + 1) < orderByFields.length) {
                            query.append(", ");
                        }
                    }
                }
                else {
                    query.append("ORDER BY ");

                    query.append("lotReservation.reservationDate DESC");
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<LotReservation>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<LotReservation>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<LotReservation>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeByApproved(boolean approved) throws SystemException {
        for (LotReservation lotReservation : findByApproved(approved)) {
            remove(lotReservation);
        }
    }

    public void removeAll() throws SystemException {
        for (LotReservation lotReservation : findAll()) {
            remove(lotReservation);
        }
    }

    public int countByApproved(boolean approved) throws SystemException {
        Object[] finderArgs = new Object[] { Boolean.valueOf(approved) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_APPROVED,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("SELECT COUNT(lotReservation) ");
                query.append("FROM LotReservation lotReservation WHERE ");

                query.append("lotReservation.approved = ?");

                query.append(" ");

                Query q = session.createQuery(query.toString());

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(approved);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_APPROVED,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(lotReservation) FROM LotReservation lotReservation");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.portal.util.PropsUtil.get(
                        "value.object.listener.be.lions.duckrace.model.LotReservation")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<LotReservation>> listenersList = new ArrayList<ModelListener<LotReservation>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<LotReservation>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
