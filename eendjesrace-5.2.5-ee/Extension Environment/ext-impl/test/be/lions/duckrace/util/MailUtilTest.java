package be.lions.duckrace.util;

import com.liferay.portal.kernel.util.StringUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class MailUtilTest {

	private static final String DEFAULT_FILE_NAME = "test";
	private static final String DEFAULT_LANGUAGE = "nl";
	private static final String DEFAULT_FILE_PATH = "be/lions/duckrace/mailtemplates/test_nl.tpl";
	private static final String DEFAULT_CONTENT = "content";

	@Before
	public void setup() throws IOException {
		mockStatic(StringUtil.class);
	}

	@Test
	public void correctTplTemplateIsRetrieved() throws IOException {
		when(StringUtil.read(MailUtil.class.getClassLoader(), DEFAULT_FILE_PATH)).thenReturn(DEFAULT_CONTENT);

		MailUtil.getTplContent(DEFAULT_FILE_NAME, DEFAULT_LANGUAGE);

		verifyStatic();
	}

	@Test(expected = IOException.class)
	public void ioExceptionTplRetrievalIsDelegated() throws IOException {
		when(StringUtil.read(MailUtil.class.getClassLoader(), DEFAULT_FILE_PATH)).thenThrow(new IOException());

		MailUtil.getTplContent(DEFAULT_FILE_NAME, DEFAULT_LANGUAGE);
	}

	@Test
	public void parameterMapContainsAllKeyValuePairs() {
		Map<String, Object> expected = new HashMap<String, Object>();
		expected.put("key1", "value1");
		expected.put("key2", "value2");

		Map<String, Object> result = MailUtil.createParamMap(new String[] { "key1", "key2" }, new String[] { "value1",
				"value2" });

		assertEquals(expected, result);
	}

	@Test
	public void parameterMapContainsKeyValuePair() {
		Map<String, Object> expected = new HashMap<String, Object>();
		expected.put("key1", "value1");

		Map<String, Object> result = MailUtil.createParamMap("key1", "value1");

		assertEquals(expected, result);
	}
}
