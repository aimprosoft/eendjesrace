package be.lions.duckrace.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DuckOrderUtilAuthorizationTest {

	private static final String ERROR_NO_TRUSTED_HOSTS = "No trusted IPs were configured";
	private static final String ERROR_NON_HTTPS_REQUEST = "Someone tried to reach the servlet via normal HTTP";
	private static final String ERROR_UNTRUSTED_HOST = "Remote host 127.0.0.1 was not trusted";
	private static final String ERROR_UNTRUSTED_HOST2 = "Remote host 192.168.2.3 was not trusted";
	private static final String[] TRUSTED_HOSTS = new String[] { "127.0.0.10", "192.168.1." };
	private static final String UNTRUSTED_REMOTE_HOST = "127.0.0.1";
	private static final String UNTRUSTED_REMOTE_HOST2 = "192.168.2.3";
	private static final String TRUSTED_REMOTE_HOST = "127.0.0.10";
	private static final String TRUSTED_REMOTE_HOST2 = "192.168.1.3";

	@Mock private HttpServletRequest request;
	@Mock private HttpServletResponse response;

	@Before
	public void setup() {
		when(request.getRemoteHost()).thenReturn(TRUSTED_REMOTE_HOST);
		when(request.isSecure()).thenReturn(true);
	}

	@Test
	public void emptyTrustedHostsListReturnsFalseAndAddsErrorCode() throws IOException {
		boolean isValid = DuckOrderUtil.isTrustedHost(null, request, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_NO_TRUSTED_HOSTS);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void nonHttpsRequestReturnsFalseAndAddsErrorCode() throws IOException {
		when(request.isSecure()).thenReturn(false);

		boolean isValid = DuckOrderUtil.isTrustedHost(TRUSTED_HOSTS, request, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_NON_HTTPS_REQUEST);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void unknownHostRequestReturnsFalseAndAddsErrorCode() throws IOException {
		when(request.getRemoteHost()).thenReturn(UNTRUSTED_REMOTE_HOST);

		boolean isValid = DuckOrderUtil.isTrustedHost(TRUSTED_HOSTS, request, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_UNTRUSTED_HOST);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void knownHostReturnsTrueAndAddsNoErrorCode() throws IOException {
		boolean isValid = DuckOrderUtil.isTrustedHost(TRUSTED_HOSTS, request, response);

		assertTrue(isValid);
		verifyZeroInteractions(response);
	}

	@Test
	public void knownHostRangeReturnsTrueAndAddsNoErrorCode() throws IOException {
		when(request.getRemoteHost()).thenReturn(TRUSTED_REMOTE_HOST2);

		boolean isValid = DuckOrderUtil.isTrustedHost(TRUSTED_HOSTS, request, response);

		assertTrue(isValid);
		verifyZeroInteractions(response);
	}

	@Test
	public void unknownHostRangeReturnsFalseAndAddsErrorCode() throws IOException {
		when(request.getRemoteHost()).thenReturn(UNTRUSTED_REMOTE_HOST2);

		boolean isValid = DuckOrderUtil.isTrustedHost(TRUSTED_HOSTS, request, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED, ERROR_UNTRUSTED_HOST2);
		verifyNoMoreInteractions(response);
	}
}
