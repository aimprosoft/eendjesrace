package be.lions.duckrace.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NumberSequenceUtilTest {

	@Test
	public void nonInterruptedSequenceReturnsMaxPlus1() {
		List<Integer> sequence = Arrays.asList(1, 2, 3, 4, 5, 6, 7);

		int number = NumberSequenceUtil.getFirstGapNumber(sequence);

		assertEquals(8, number);
	}

	@Test
	public void interruptedSequenceWithOneGapReturnsFirstGapNumber() {
		List<Integer> sequence = Arrays.asList(1, 2, 4, 5, 6, 7);

		int number = NumberSequenceUtil.getFirstGapNumber(sequence);

		assertEquals(3, number);
	}

	@Test
	public void interruptedSequenceWithMoreGapsReturnsFirstGapNumber() {
		List<Integer> sequence = Arrays.asList(1, 2, 3, 6, 7, 9, 10, 11);

		int number = NumberSequenceUtil.getFirstGapNumber(sequence);

		assertEquals(4, number);
	}

	@Test
	public void emptySequenceReturnsOne() {
		int number = NumberSequenceUtil.getFirstGapNumber(Collections.<Integer> emptyList());

		assertEquals(1, number);
	}

	@Test
	public void singletonSequenceReturnsElementPlusOne() {
		int number = NumberSequenceUtil.getFirstGapNumber(Collections.<Integer> singletonList(1));

		assertEquals(2, number);
	}

	@Test
	public void singletonZeroSequenceReturnsElementPlusOne() {
		int number = NumberSequenceUtil.getFirstGapNumber(Collections.<Integer> singletonList(0));

		assertEquals(1, number);
	}

	@Test
	public void leadingGapIsDetected() {
		List<Integer> sequence = Arrays.asList(2, 3, 6, 7, 9, 10, 11);

		int number = NumberSequenceUtil.getFirstGapNumber(sequence);

		assertEquals(1, number);
	}

	@Test
	public void sequenceWithLeadingZeroReturnsFirstGapNumber() {
		List<Integer> sequence = Arrays.asList(0, 1, 3, 4, 5, 6, 7);

		int number = NumberSequenceUtil.getFirstGapNumber(sequence);

		assertEquals(2, number);
	}
}
