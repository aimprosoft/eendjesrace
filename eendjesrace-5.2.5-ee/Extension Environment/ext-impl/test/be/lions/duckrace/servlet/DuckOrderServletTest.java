package be.lions.duckrace.servlet;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.util.MailUtil;
import be.lions.duckrace.util.MessageUtil;
import be.lions.duckrace.util.PropKeys;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, LotLocalServiceUtil.class, MessageUtil.class, DuckRaceLocalServiceUtil.class,
		MailUtil.class })
public class DuckOrderServletTest {

	private static final String PARAM_NAME = "name";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_CELL_PHONE = "cellPhone";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_DUCK_COUNT = "duckCount";
	private static final String PARAM_ORDER_NUMBER = "orderNumber";
	private static final String PARAM_LANGUAGE = "language";
	private static final String KEY_PURCHASE = "purchase";
	private static final String TEMPLATE_NAME = "behappy2";
	private static final HashMap<String, String> INVALID_PARAMETER_MAP = new HashMap<String, String>();

	private static final String DEFAULT_CELL_PHONE = "0123456789";
	private static final String DEFAULT_FIRST_NAME = "test";
	private static final String DEFAULT_LAST_NAME = "tester";
	private static final String DEFAULT_EMAIL = "test@test.com";
	private static final String DEFAULT_ORDER_NUMBER = "BH12345";
	private static final String DEFAULT_LANGUAGE_NL = "nl";
	private static final String DEFAULT_LANGUAGE_EN = "en";
	private static final int DEFAULT_LOT_NUMBER = 123;
	private static final int DEFAULT_DUCK_COUNT_0 = 0;
	private static final int DEFAULT_DUCK_COUNT_1 = 1;
	private static final int DEFAULT_DUCK_COUNT_3 = 3;
	private static final String DUCK_NUMBERS_1 = "123";
	private static final String DUCK_NUMBERS_3 = "123,123,123";
	private static final String DEFAULT_PURCHASE = "Purchase";
	private static final String DEFAULT_SITE_TITLE = "siteTitle";
	private static final String DEFAULT_CONTENT = "tplContent";
	private static final String DEFAULT_FULL_NAME = "fullName";
	private static final String DEFAULT_SUBJECT = DEFAULT_PURCHASE + " " + DEFAULT_SITE_TITLE;
	private static final String PARAM_LOTS = "lots";
	private static final String PARAM_TITLE = "title";
	private static final String PARAM_FULL_NAME = "fullName";

	private final Map<String, String> VALID_PARAMETER_MAP = new HashMap<String, String>();
	private Map<String, Object> DEFAULT_MAIL_PARAMS;
	private static final String[] TRUSTED_HOSTS = new String[] { "127.0.0.10" };
	private static final String UNTRUSTED_REMOTE_HOST = "127.0.0.1";
	private static final String TRUSTED_REMOTE_HOST = "127.0.0.10";

	@Mock private HttpServletRequest request;
	@Mock private HttpServletResponse response;
	@Mock private Participant participant;
	@Mock private Lot lot;
	@Mock private PrintWriter writer;

	@Before
	public void setup() throws IOException, SystemException, PortalException {
		VALID_PARAMETER_MAP.put(PARAM_NAME, DEFAULT_LAST_NAME);
		VALID_PARAMETER_MAP.put(PARAM_FIRST_NAME, DEFAULT_FIRST_NAME);
		VALID_PARAMETER_MAP.put(PARAM_CELL_PHONE, DEFAULT_CELL_PHONE);
		VALID_PARAMETER_MAP.put(PARAM_EMAIL, DEFAULT_EMAIL);
		VALID_PARAMETER_MAP.put(PARAM_DUCK_COUNT, String.valueOf(DEFAULT_DUCK_COUNT_3));
		VALID_PARAMETER_MAP.put(PARAM_ORDER_NUMBER, DEFAULT_ORDER_NUMBER);
		VALID_PARAMETER_MAP.put(PARAM_LANGUAGE, DEFAULT_LANGUAGE_EN);
		when(request.getParameterMap()).thenReturn(VALID_PARAMETER_MAP);

		DEFAULT_MAIL_PARAMS = new HashMap<String, Object>();
		DEFAULT_MAIL_PARAMS.put(PARAM_LOTS, Arrays.asList(lot, lot, lot));
		DEFAULT_MAIL_PARAMS.put(PARAM_TITLE, DEFAULT_SITE_TITLE);
		DEFAULT_MAIL_PARAMS.put(PARAM_FULL_NAME, DEFAULT_FULL_NAME);

		mockStatic(PropsUtil.class);
		when(PropsUtil.getArray(eq(PropKeys.BEHAPPY2_TRUSTED_HOSTS))).thenReturn(TRUSTED_HOSTS);

		mockStatic(LotLocalServiceUtil.class);
		when(
				LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME,
						DEFAULT_EMAIL, DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_NL)).thenReturn(lot);
		when(
				LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME,
						DEFAULT_EMAIL, DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_EN)).thenReturn(lot);

		when(participant.getFullName()).thenReturn(DEFAULT_FULL_NAME);
		when(participant.getLanguage()).thenReturn(DEFAULT_LANGUAGE_EN);

		when(lot.getNumber()).thenReturn(DEFAULT_LOT_NUMBER);
		when(lot.getParticipant()).thenReturn(participant);

		when(request.getRemoteHost()).thenReturn(TRUSTED_REMOTE_HOST);
		when(request.isSecure()).thenReturn(true);
		when(request.getParameter(PARAM_CELL_PHONE)).thenReturn(DEFAULT_CELL_PHONE);
		when(request.getParameter(PARAM_FIRST_NAME)).thenReturn(DEFAULT_FIRST_NAME);
		when(request.getParameter(PARAM_NAME)).thenReturn(DEFAULT_LAST_NAME);
		when(request.getParameter(PARAM_EMAIL)).thenReturn(DEFAULT_EMAIL);
		when(request.getParameter(PARAM_ORDER_NUMBER)).thenReturn(DEFAULT_ORDER_NUMBER);
		when(request.getParameter(PARAM_DUCK_COUNT)).thenReturn(String.valueOf(DEFAULT_DUCK_COUNT_3));
		when(request.getParameter(PARAM_LANGUAGE)).thenReturn(DEFAULT_LANGUAGE_EN);

		when(response.getWriter()).thenReturn(writer);

		mockStatic(MessageUtil.class);
		when(MessageUtil.getMessage(DEFAULT_LANGUAGE_EN,KEY_PURCHASE)).thenReturn(DEFAULT_PURCHASE);

		mockStatic(DuckRaceLocalServiceUtil.class);
		when(DuckRaceLocalServiceUtil.getSiteTitle()).thenReturn(DEFAULT_SITE_TITLE);

		mockStatic(MailUtil.class);
		when(MailUtil.getTplContent(TEMPLATE_NAME, DEFAULT_LANGUAGE_EN)).thenReturn(DEFAULT_CONTENT);
		when(
				MailUtil.createParamMap(new String[] { PARAM_LOTS, PARAM_TITLE, PARAM_FULL_NAME }, new Object[] {
						Arrays.asList(lot, lot, lot), DEFAULT_SITE_TITLE, DEFAULT_FULL_NAME })).thenReturn(
				DEFAULT_MAIL_PARAMS);
	}

	@Test
	public void invalidParameterMapSendsBadRequestResponse() throws IOException {
		when(request.getParameterMap()).thenReturn(INVALID_PARAMETER_MAP);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
	}

	@Test
	public void untrustedHostRequestSendsUnauthorizedResponse() throws IOException {
		when(request.getRemoteHost()).thenReturn(UNTRUSTED_REMOTE_HOST);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_UNAUTHORIZED), anyString());
	}

	@Test
	public void internalServerErrorResponseThrownWhenSystemExceptionThrown() throws IOException, SystemException,
			PortalException {
		when(
				LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME,
						DEFAULT_EMAIL, DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_EN)).thenThrow(new SystemException());
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_INTERNAL_SERVER_ERROR), anyString());
	}

	@Test
	public void internalServerErrorResponseThrownWhenPortalExceptionThrown() throws IOException, SystemException,
			PortalException {
		when(
				LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME,
						DEFAULT_EMAIL, DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_EN)).thenThrow(new PortalException());
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_INTERNAL_SERVER_ERROR), anyString());
	}

	@Test
	public void validRequestWithDuckCount1SendsValidResponse() throws IOException, SystemException, PortalException {
		when(request.getParameter(PARAM_DUCK_COUNT)).thenReturn(String.valueOf(DEFAULT_DUCK_COUNT_1));
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verifyStatic(times(DEFAULT_DUCK_COUNT_1));
		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_EN);
		verify(response).setStatus(HttpServletResponse.SC_OK);
		verify(response).getWriter();
		verify(writer).println(DUCK_NUMBERS_1);
	}

	@Test
	public void defaultLanguageParameterValueUsedWhenLanguageNotAvailable() throws IOException, SystemException,
			PortalException {
		when(request.getParameter(PARAM_DUCK_COUNT)).thenReturn(String.valueOf(DEFAULT_DUCK_COUNT_1));
		when(request.getParameter(PARAM_LANGUAGE)).thenReturn(null);
		VALID_PARAMETER_MAP.put(PARAM_LANGUAGE, null);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verifyStatic(times(DEFAULT_DUCK_COUNT_1));
		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_NL);
		verify(response).setStatus(HttpServletResponse.SC_OK);
		verify(response).getWriter();
		verify(writer).println(DUCK_NUMBERS_1);
	}

	@Test
	public void validRequestWithDuckCount3SendsValidResponse() throws IOException, SystemException, PortalException {
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verifyStatic(times(DEFAULT_DUCK_COUNT_3));
		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE_EN);
		verify(response).setStatus(HttpServletResponse.SC_OK);
		verify(response).getWriter();
		verify(writer).println(DUCK_NUMBERS_3);
	}

	@Test
	public void validRequestSendsRegistrationEmail() throws Exception {
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verifyStatic();
		DuckRaceLocalServiceUtil.sendMail(DEFAULT_CONTENT, DEFAULT_MAIL_PARAMS, DEFAULT_SUBJECT, participant);
	}

	@Test
	public void validRequestWithZeroLotsSendsNoRegistrationEmail() throws Exception {
		when(request.getParameter(PARAM_DUCK_COUNT)).thenReturn(String.valueOf(DEFAULT_DUCK_COUNT_0));
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verifyStatic(never());
		DuckRaceLocalServiceUtil.sendMail(DEFAULT_CONTENT, DEFAULT_MAIL_PARAMS, DEFAULT_SUBJECT, participant);
	}

	@Test(expected = Exception.class)
	public void internalServerErrorResponseThrownWhenSendMailThrowsException() throws Exception {
		doNothing().doThrow(Exception.class).when(DuckRaceLocalServiceUtil.class);
		DuckRaceLocalServiceUtil.sendMail(DEFAULT_CONTENT, DEFAULT_MAIL_PARAMS, DEFAULT_SUBJECT, participant);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_INTERNAL_SERVER_ERROR), anyString());
	}
}
