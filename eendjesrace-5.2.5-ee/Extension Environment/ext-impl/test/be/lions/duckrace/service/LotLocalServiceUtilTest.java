package be.lions.duckrace.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LotLocalServiceUtilTest {

	private static final String DEFAULT_CELL_PHONE = "0123456789";
	private static final String DEFAULT_FIRST_NAME = "test";
	private static final String DEFAULT_LAST_NAME = "tester";
	private static final String DEFAULT_EMAIL = "test@test.com";
	private static final String DEFAULT_ORDER_NUMBER = "BH12345";
	private static final String DEFAULT_LANGUAGE = "nl";

	@Mock LotLocalService lotLocalService;

	@Before
	public void setup() {
		LotLocalServiceUtil util = new LotLocalServiceUtil();
		util.setService(lotLocalService);
	}

	@Test
	public void serviceCalledWhenUtilMethodInvoked() throws SystemException, PortalException {
		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE);

		verify(lotLocalService).registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE);
		verifyNoMoreInteractions(lotLocalService);
	}

	@Test(expected = SystemException.class)
	public void systemExceptionThrownWhenServiceThrowsSystemException() throws SystemException, PortalException {
		when(
				lotLocalService.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
						DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE)).thenThrow(new SystemException());

		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE);
	}

	@Test(expected = PortalException.class)
	public void portalExceptionThrownWhenServiceThrowsPortalException() throws SystemException, PortalException {
		when(
				lotLocalService.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
						DEFAULT_ORDER_NUMBER,DEFAULT_LANGUAGE)).thenThrow(new PortalException());

		LotLocalServiceUtil.registerLot(DEFAULT_CELL_PHONE, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME, DEFAULT_EMAIL,
				DEFAULT_ORDER_NUMBER, DEFAULT_LANGUAGE);
	}
}
