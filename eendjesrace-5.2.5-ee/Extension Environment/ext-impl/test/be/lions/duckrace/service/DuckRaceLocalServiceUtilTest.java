package be.lions.duckrace.service;

import be.lions.duckrace.model.Participant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DuckRaceLocalServiceUtilTest {

	private static final String DEFAULT_CONTENT = "content";
	private static final String DEFAULT_SUBJECT = "subject";

	@Mock private DuckRaceLocalService duckRaceLocalService;
	@Mock private Map<String, Object> parameters;
	@Mock private Participant participant;

	@Before
	public void setup() {
		DuckRaceLocalServiceUtil util = new DuckRaceLocalServiceUtil();
		util.setService(duckRaceLocalService);
	}

	@Test
	public void serviceCalledWhenUtilMethodInvoked() throws Exception {
		DuckRaceLocalServiceUtil.sendMail(DEFAULT_CONTENT, parameters, DEFAULT_SUBJECT, participant);

		verify(duckRaceLocalService).sendMail(DEFAULT_CONTENT, parameters, DEFAULT_SUBJECT, participant);
		verifyNoMoreInteractions(duckRaceLocalService);
	}

	@Test(expected = Exception.class)
	public void exceptionThrownWhenServiceThrowsException() throws Exception {
		doThrow(new Exception()).when(duckRaceLocalService).sendMail(DEFAULT_CONTENT, parameters, DEFAULT_SUBJECT,
				participant);

		DuckRaceLocalServiceUtil.sendMail(DEFAULT_CONTENT, parameters, DEFAULT_SUBJECT, participant);
	}
}
