database sysmaster;
drop database lportal;
create database lportal WITH LOG;

create procedure 'lportal'.isnull(test_string varchar)
returning boolean;
IF test_string IS NULL THEN
	RETURN 't';
ELSE
	RETURN 'f';
END IF
end procedure;


create table Account_ (
	accountId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentAccountId int8,
	name varchar(75),
	legalName varchar(75),
	legalId varchar(75),
	legalType varchar(75),
	sicCode varchar(75),
	tickerSymbol varchar(75),
	industry varchar(75),
	type_ varchar(75),
	size_ varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Address (
	addressId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	street1 varchar(75),
	street2 varchar(75),
	street3 varchar(75),
	city varchar(75),
	zip varchar(75),
	regionId int8,
	countryId int8,
	typeId int,
	mailing boolean,
	primary_ boolean
)
extent size 16 next size 16
lock mode row;

create table AnnouncementsDelivery (
	deliveryId int8 not null primary key,
	companyId int8,
	userId int8,
	type_ varchar(75),
	email boolean,
	sms boolean,
	website boolean
)
extent size 16 next size 16
lock mode row;

create table AnnouncementsEntry (
	uuid_ varchar(75),
	entryId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	title varchar(75),
	content lvarchar,
	url lvarchar,
	type_ varchar(75),
	displayDate datetime YEAR TO FRACTION,
	expirationDate datetime YEAR TO FRACTION,
	priority int,
	alert boolean
)
extent size 16 next size 16
lock mode row;

create table AnnouncementsFlag (
	flagId int8 not null primary key,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	entryId int8,
	value int
)
extent size 16 next size 16
lock mode row;

create table BlogsEntry (
	uuid_ varchar(75),
	entryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	title varchar(150),
	urlTitle varchar(150),
	content text,
	displayDate datetime YEAR TO FRACTION,
	draft boolean,
	allowTrackbacks boolean,
	trackbacks text
)
extent size 16 next size 16
lock mode row;

create table BlogsStatsUser (
	statsUserId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	entryCount int,
	lastPostDate datetime YEAR TO FRACTION,
	ratingsTotalEntries int,
	ratingsTotalScore float,
	ratingsAverageScore float
)
extent size 16 next size 16
lock mode row;

create table BookmarksEntry (
	uuid_ varchar(75),
	entryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	folderId int8,
	name varchar(255),
	url lvarchar,
	comments lvarchar,
	visits int,
	priority int
)
extent size 16 next size 16
lock mode row;

create table BookmarksFolder (
	uuid_ varchar(75),
	folderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentFolderId int8,
	name varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table BrowserTracker (
	browserTrackerId int8 not null primary key,
	userId int8,
	browserKey int8
)
extent size 16 next size 16
lock mode row;

create table CalEvent (
	uuid_ varchar(75),
	eventId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	title varchar(75),
	description lvarchar,
	startDate datetime YEAR TO FRACTION,
	endDate datetime YEAR TO FRACTION,
	durationHour int,
	durationMinute int,
	allDay boolean,
	timeZoneSensitive boolean,
	type_ varchar(75),
	repeating boolean,
	recurrence text,
	remindBy int,
	firstReminder int,
	secondReminder int
)
extent size 16 next size 16
lock mode row;

create table ClassName_ (
	classNameId int8 not null primary key,
	value varchar(200)
)
extent size 16 next size 16
lock mode row;

create table Company (
	companyId int8 not null primary key,
	accountId int8,
	webId varchar(75),
	key_ text,
	virtualHost varchar(75),
	mx varchar(75),
	homeURL lvarchar,
	logoId int8,
	system boolean
)
extent size 16 next size 16
lock mode row;

create table Contact_ (
	contactId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	accountId int8,
	parentContactId int8,
	firstName varchar(75),
	middleName varchar(75),
	lastName varchar(75),
	prefixId int,
	suffixId int,
	male boolean,
	birthday datetime YEAR TO FRACTION,
	smsSn varchar(75),
	aimSn varchar(75),
	facebookSn varchar(75),
	icqSn varchar(75),
	jabberSn varchar(75),
	msnSn varchar(75),
	mySpaceSn varchar(75),
	skypeSn varchar(75),
	twitterSn varchar(75),
	ymSn varchar(75),
	employeeStatusId varchar(75),
	employeeNumber varchar(75),
	jobTitle varchar(100),
	jobClass varchar(75),
	hoursOfOperation varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Counter (
	name varchar(75) not null primary key,
	currentId int8
)
extent size 16 next size 16
lock mode row;

create table Country (
	countryId int8 not null primary key,
	name varchar(75),
	a2 varchar(75),
	a3 varchar(75),
	number_ varchar(75),
	idd_ varchar(75),
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table Customer (
	customerId int not null primary key,
	name varchar(75)
)
extent size 16 next size 16
lock mode row;

create table CyrusUser (
	userId varchar(75) not null primary key,
	password_ varchar(75) not null
)
extent size 16 next size 16
lock mode row;

create table CyrusVirtual (
	emailAddress varchar(75) not null primary key,
	userId varchar(75) not null
)
extent size 16 next size 16
lock mode row;

create table DLFileEntry (
	uuid_ varchar(75),
	fileEntryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	versionUserId int8,
	versionUserName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	folderId int8,
	name varchar(255),
	title varchar(255),
	description lvarchar,
	version float,
	size_ int,
	readCount int,
	extraSettings text
)
extent size 16 next size 16
lock mode row;

create table DLFileRank (
	fileRankId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	folderId int8,
	name varchar(255)
)
extent size 16 next size 16
lock mode row;

create table DLFileShortcut (
	uuid_ varchar(75),
	fileShortcutId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	folderId int8,
	toFolderId int8,
	toName varchar(255)
)
extent size 16 next size 16
lock mode row;

create table DLFileVersion (
	fileVersionId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	folderId int8,
	name varchar(255),
	version float,
	size_ int
)
extent size 16 next size 16
lock mode row;

create table DLFolder (
	uuid_ varchar(75),
	folderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentFolderId int8,
	name varchar(100),
	description lvarchar,
	lastPostDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table EmailAddress (
	emailAddressId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	address varchar(75),
	typeId int,
	primary_ boolean
)
extent size 16 next size 16
lock mode row;

create table ExpandoColumn (
	columnId int8 not null primary key,
	companyId int8,
	tableId int8,
	name varchar(75),
	type_ int,
	defaultData lvarchar,
	typeSettings lvarchar(4096)
)
extent size 16 next size 16
lock mode row;

create table ExpandoRow (
	rowId_ int8 not null primary key,
	companyId int8,
	tableId int8,
	classPK int8
)
extent size 16 next size 16
lock mode row;

create table ExpandoTable (
	tableId int8 not null primary key,
	companyId int8,
	classNameId int8,
	name varchar(75)
)
extent size 16 next size 16
lock mode row;

create table ExpandoValue (
	valueId int8 not null primary key,
	companyId int8,
	tableId int8,
	columnId int8,
	rowId_ int8,
	classNameId int8,
	classPK int8,
	data_ lvarchar
)
extent size 16 next size 16
lock mode row;

create table Group_ (
	groupId int8 not null primary key,
	companyId int8,
	creatorUserId int8,
	classNameId int8,
	classPK int8,
	parentGroupId int8,
	liveGroupId int8,
	name varchar(75),
	description lvarchar,
	type_ int,
	typeSettings lvarchar,
	friendlyURL varchar(100),
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table Groups_Orgs (
	groupId int8 not null,
	organizationId int8 not null,
	primary key (groupId, organizationId)
)
extent size 16 next size 16
lock mode row;

create table Groups_Permissions (
	groupId int8 not null,
	permissionId int8 not null,
	primary key (groupId, permissionId)
)
extent size 16 next size 16
lock mode row;

create table Groups_Roles (
	groupId int8 not null,
	roleId int8 not null,
	primary key (groupId, roleId)
)
extent size 16 next size 16
lock mode row;

create table Groups_UserGroups (
	groupId int8 not null,
	userGroupId int8 not null,
	primary key (groupId, userGroupId)
)
extent size 16 next size 16
lock mode row;

create table IGFolder (
	uuid_ varchar(75),
	folderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentFolderId int8,
	name varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table IGImage (
	uuid_ varchar(75),
	imageId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	folderId int8,
	name varchar(75),
	description lvarchar,
	smallImageId int8,
	largeImageId int8,
	custom1ImageId int8,
	custom2ImageId int8
)
extent size 16 next size 16
lock mode row;

create table Image (
	imageId int8 not null primary key,
	modifiedDate datetime YEAR TO FRACTION,
	text_ text,
	type_ varchar(75),
	height int,
	width int,
	size_ int
)
extent size 16 next size 16
lock mode row;

create table Item (
	number_ int not null primary key,
	code_ varchar(75),
	checksum varchar(75),
	lockoutDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table JournalArticle (
	uuid_ varchar(75),
	id_ int8 not null primary key,
	resourcePrimKey int8,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	articleId varchar(75),
	version float,
	title varchar(100),
	urlTitle varchar(150),
	description lvarchar,
	content text,
	type_ varchar(75),
	structureId varchar(75),
	templateId varchar(75),
	displayDate datetime YEAR TO FRACTION,
	approved boolean,
	approvedByUserId int8,
	approvedByUserName varchar(75),
	approvedDate datetime YEAR TO FRACTION,
	expired boolean,
	expirationDate datetime YEAR TO FRACTION,
	reviewDate datetime YEAR TO FRACTION,
	indexable boolean,
	smallImage boolean,
	smallImageId int8,
	smallImageURL varchar(75)
)
extent size 16 next size 16
lock mode row;

create table JournalArticleImage (
	articleImageId int8 not null primary key,
	groupId int8,
	articleId varchar(75),
	version float,
	elInstanceId varchar(75),
	elName varchar(75),
	languageId varchar(75),
	tempImage boolean
)
extent size 16 next size 16
lock mode row;

create table JournalArticleResource (
	resourcePrimKey int8 not null primary key,
	groupId int8,
	articleId varchar(75)
)
extent size 16 next size 16
lock mode row;

create table JournalContentSearch (
	contentSearchId int8 not null primary key,
	groupId int8,
	companyId int8,
	privateLayout boolean,
	layoutId int8,
	portletId varchar(200),
	articleId varchar(75)
)
extent size 16 next size 16
lock mode row;

create table JournalFeed (
	uuid_ varchar(75),
	id_ int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	feedId varchar(75),
	name varchar(75),
	description lvarchar,
	type_ varchar(75),
	structureId varchar(75),
	templateId varchar(75),
	rendererTemplateId varchar(75),
	delta int,
	orderByCol varchar(75),
	orderByType varchar(75),
	targetLayoutFriendlyUrl varchar(75),
	targetPortletId varchar(75),
	contentField varchar(75),
	feedType varchar(75),
	feedVersion float
)
extent size 16 next size 16
lock mode row;

create table JournalStructure (
	uuid_ varchar(75),
	id_ int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	structureId varchar(75),
	parentStructureId varchar(75),
	name varchar(75),
	description lvarchar,
	xsd text
)
extent size 16 next size 16
lock mode row;

create table JournalTemplate (
	uuid_ varchar(75),
	id_ int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	templateId varchar(75),
	structureId varchar(75),
	name varchar(75),
	description lvarchar,
	xsl text,
	langType varchar(75),
	cacheable boolean,
	smallImage boolean,
	smallImageId int8,
	smallImageURL varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Layout (
	plid int8 not null primary key,
	groupId int8,
	companyId int8,
	privateLayout boolean,
	layoutId int8,
	parentLayoutId int8,
	name lvarchar,
	title lvarchar,
	description lvarchar,
	type_ varchar(75),
	typeSettings lvarchar(4096),
	hidden_ boolean,
	friendlyURL varchar(100),
	iconImage boolean,
	iconImageId int8,
	themeId varchar(75),
	colorSchemeId varchar(75),
	wapThemeId varchar(75),
	wapColorSchemeId varchar(75),
	css lvarchar,
	priority int,
	dlFolderId int8
)
extent size 16 next size 16
lock mode row;

create table LayoutSet (
	layoutSetId int8 not null primary key,
	groupId int8,
	companyId int8,
	privateLayout boolean,
	logo boolean,
	logoId int8,
	themeId varchar(75),
	colorSchemeId varchar(75),
	wapThemeId varchar(75),
	wapColorSchemeId varchar(75),
	css lvarchar,
	pageCount int,
	virtualHost varchar(75)
)
extent size 16 next size 16
lock mode row;

create table ListType (
	listTypeId int not null primary key,
	name varchar(75),
	type_ varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Lot (
	code_ varchar(75) not null primary key,
	checksum varchar(75),
	lotNumber int,
	number_ int,
	pressed boolean,
	nbTries int,
	lockoutDate datetime YEAR TO FRACTION,
	participantId int8,
	reservationId int8,
	partnerId int8
)
extent size 16 next size 16
lock mode row;

create table LotReservation (
	reservationId int8 not null primary key,
	participantId int8,
	numberLots int,
	approved boolean,
	reservationDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table MBBan (
	banId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	banUserId int8
)
extent size 16 next size 16
lock mode row;

create table MBCategory (
	uuid_ varchar(75),
	categoryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentCategoryId int8,
	name varchar(75),
	description lvarchar,
	threadCount int,
	messageCount int,
	lastPostDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table MBDiscussion (
	discussionId int8 not null primary key,
	classNameId int8,
	classPK int8,
	threadId int8
)
extent size 16 next size 16
lock mode row;

create table MBMailingList (
	uuid_ varchar(75),
	mailingListId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	categoryId int8,
	emailAddress varchar(75),
	inProtocol varchar(75),
	inServerName varchar(75),
	inServerPort int,
	inUseSSL boolean,
	inUserName varchar(75),
	inPassword varchar(75),
	inReadInterval int,
	outEmailAddress varchar(75),
	outCustom boolean,
	outServerName varchar(75),
	outServerPort int,
	outUseSSL boolean,
	outUserName varchar(75),
	outPassword varchar(75),
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table MBMessage (
	uuid_ varchar(75),
	messageId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	categoryId int8,
	threadId int8,
	parentMessageId int8,
	subject varchar(75),
	body text,
	attachments boolean,
	anonymous boolean,
	priority float
)
extent size 16 next size 16
lock mode row;

create table MBMessageFlag (
	messageFlagId int8 not null primary key,
	userId int8,
	modifiedDate datetime YEAR TO FRACTION,
	threadId int8,
	messageId int8,
	flag int
)
extent size 16 next size 16
lock mode row;

create table MBStatsUser (
	statsUserId int8 not null primary key,
	groupId int8,
	userId int8,
	messageCount int,
	lastPostDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table MBThread (
	threadId int8 not null primary key,
	groupId int8,
	categoryId int8,
	rootMessageId int8,
	messageCount int,
	viewCount int,
	lastPostByUserId int8,
	lastPostDate datetime YEAR TO FRACTION,
	priority float
)
extent size 16 next size 16
lock mode row;

create table MembershipRequest (
	membershipRequestId int8 not null primary key,
	companyId int8,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	groupId int8,
	comments lvarchar,
	replyComments lvarchar,
	replyDate datetime YEAR TO FRACTION,
	replierUserId int8,
	statusId int
)
extent size 16 next size 16
lock mode row;

create table Organization_ (
	organizationId int8 not null primary key,
	companyId int8,
	parentOrganizationId int8,
	leftOrganizationId int8,
	rightOrganizationId int8,
	name varchar(100),
	type_ varchar(75),
	recursable boolean,
	regionId int8,
	countryId int8,
	statusId int,
	comments lvarchar
)
extent size 16 next size 16
lock mode row;

create table OrgGroupPermission (
	organizationId int8 not null,
	groupId int8 not null,
	permissionId int8 not null,
	primary key (organizationId, groupId, permissionId)
)
extent size 16 next size 16
lock mode row;

create table OrgGroupRole (
	organizationId int8 not null,
	groupId int8 not null,
	roleId int8 not null,
	primary key (organizationId, groupId, roleId)
)
extent size 16 next size 16
lock mode row;

create table OrgLabor (
	orgLaborId int8 not null primary key,
	organizationId int8,
	typeId int,
	sunOpen int,
	sunClose int,
	monOpen int,
	monClose int,
	tueOpen int,
	tueClose int,
	wedOpen int,
	wedClose int,
	thuOpen int,
	thuClose int,
	friOpen int,
	friClose int,
	satOpen int,
	satClose int
)
extent size 16 next size 16
lock mode row;

create table Participant (
	participantId int8 not null primary key,
	emailAddress varchar(75),
	mobileNumber varchar(75),
	firstName varchar(75),
	lastName varchar(75),
	contestParticipant boolean,
	mercedesDriver boolean,
	carBrand varchar(75),
	mercedesOld boolean
)
extent size 16 next size 16
lock mode row;

create table Participant_Lots (
	participantId int8 not null,
	number_ int8 not null,
	primary key (participantId, number)
)
extent size 16 next size 16
lock mode row;

create table Partner (
	partnerId int8 not null primary key,
	category varchar(75),
	name varchar(75),
	returnedLotsCount int
)
extent size 16 next size 16
lock mode row;

create table PasswordPolicy (
	passwordPolicyId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	defaultPolicy boolean,
	name varchar(75),
	description lvarchar,
	changeable boolean,
	changeRequired boolean,
	minAge int8,
	checkSyntax boolean,
	allowDictionaryWords boolean,
	minLength int,
	history boolean,
	historyCount int,
	expireable boolean,
	maxAge int8,
	warningTime int8,
	graceLimit int,
	lockout boolean,
	maxFailure int,
	lockoutDuration int8,
	requireUnlock boolean,
	resetFailureCount int8
)
extent size 16 next size 16
lock mode row;

create table PasswordPolicyRel (
	passwordPolicyRelId int8 not null primary key,
	passwordPolicyId int8,
	classNameId int8,
	classPK int8
)
extent size 16 next size 16
lock mode row;

create table PasswordTracker (
	passwordTrackerId int8 not null primary key,
	userId int8,
	createDate datetime YEAR TO FRACTION,
	password_ varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Permission_ (
	permissionId int8 not null primary key,
	companyId int8,
	actionId varchar(75),
	resourceId int8
)
extent size 16 next size 16
lock mode row;

create table Phone (
	phoneId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	number_ varchar(75),
	extension varchar(75),
	typeId int,
	primary_ boolean
)
extent size 16 next size 16
lock mode row;

create table PluginSetting (
	pluginSettingId int8 not null primary key,
	companyId int8,
	pluginId varchar(75),
	pluginType varchar(75),
	roles lvarchar,
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table PollsChoice (
	uuid_ varchar(75),
	choiceId int8 not null primary key,
	questionId int8,
	name varchar(75),
	description lvarchar(1000)
)
extent size 16 next size 16
lock mode row;

create table PollsQuestion (
	uuid_ varchar(75),
	questionId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	title lvarchar(500),
	description lvarchar,
	expirationDate datetime YEAR TO FRACTION,
	lastVoteDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table PollsVote (
	voteId int8 not null primary key,
	userId int8,
	questionId int8,
	choiceId int8,
	voteDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table Portlet (
	id_ int8 not null primary key,
	companyId int8,
	portletId varchar(200),
	roles lvarchar,
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table PortletItem (
	portletItemId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	portletId varchar(75),
	classNameId int8
)
extent size 16 next size 16
lock mode row;

create table PortletPreferences (
	portletPreferencesId int8 not null primary key,
	ownerId int8,
	ownerType int,
	plid int8,
	portletId varchar(200),
	preferences text
)
extent size 16 next size 16
lock mode row;

create table RatingsEntry (
	entryId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	score float
)
extent size 16 next size 16
lock mode row;

create table RatingsStats (
	statsId int8 not null primary key,
	classNameId int8,
	classPK int8,
	totalEntries int,
	totalScore float,
	averageScore float
)
extent size 16 next size 16
lock mode row;

create table Region (
	regionId int8 not null primary key,
	countryId int8,
	regionCode varchar(75),
	name varchar(75),
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table Release_ (
	releaseId int8 not null primary key,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	buildNumber int,
	buildDate datetime YEAR TO FRACTION,
	verified boolean,
	testString lvarchar(1024)
)
extent size 16 next size 16
lock mode row;

create table Resource_ (
	resourceId int8 not null primary key,
	codeId int8,
	primKey varchar(255)
)
extent size 16 next size 16
lock mode row;

create table ResourceAction (
	resourceActionId int8 not null primary key,
	name varchar(75),
	actionId varchar(75),
	bitwiseValue int8
)
extent size 16 next size 16
lock mode row;

create table ResourceCode (
	codeId int8 not null primary key,
	companyId int8,
	name varchar(255),
	scope int
)
extent size 16 next size 16
lock mode row;

create table ResourcePermission (
	resourcePermissionId int8 not null primary key,
	companyId int8,
	name varchar(255),
	scope int,
	primKey varchar(255),
	roleId int8,
	actionIds int8
)
extent size 16 next size 16
lock mode row;

create table Role_ (
	roleId int8 not null primary key,
	companyId int8,
	classNameId int8,
	classPK int8,
	name varchar(75),
	title lvarchar,
	description lvarchar,
	type_ int,
	subtype varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Roles_Permissions (
	roleId int8 not null,
	permissionId int8 not null,
	primary key (roleId, permissionId)
)
extent size 16 next size 16
lock mode row;

create table SCFrameworkVersi_SCProductVers (
	frameworkVersionId int8 not null,
	productVersionId int8 not null,
	primary key (frameworkVersionId, productVersionId)
)
extent size 16 next size 16
lock mode row;

create table SCFrameworkVersion (
	frameworkVersionId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	url lvarchar,
	active_ boolean,
	priority int
)
extent size 16 next size 16
lock mode row;

create table SCLicense (
	licenseId int8 not null primary key,
	name varchar(75),
	url lvarchar,
	openSource boolean,
	active_ boolean,
	recommended boolean
)
extent size 16 next size 16
lock mode row;

create table SCLicenses_SCProductEntries (
	licenseId int8 not null,
	productEntryId int8 not null,
	primary key (licenseId, productEntryId)
)
extent size 16 next size 16
lock mode row;

create table SCProductEntry (
	productEntryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	type_ varchar(75),
	tags varchar(255),
	shortDescription lvarchar,
	longDescription lvarchar,
	pageURL lvarchar,
	author varchar(75),
	repoGroupId varchar(75),
	repoArtifactId varchar(75)
)
extent size 16 next size 16
lock mode row;

create table SCProductScreenshot (
	productScreenshotId int8 not null primary key,
	companyId int8,
	groupId int8,
	productEntryId int8,
	thumbnailId int8,
	fullImageId int8,
	priority int
)
extent size 16 next size 16
lock mode row;

create table SCProductVersion (
	productVersionId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	productEntryId int8,
	version varchar(75),
	changeLog lvarchar,
	downloadPageURL lvarchar,
	directDownloadURL varchar(2000),
	repoStoreArtifact boolean
)
extent size 16 next size 16
lock mode row;

create table ServiceComponent (
	serviceComponentId int8 not null primary key,
	buildNamespace varchar(75),
	buildNumber int8,
	buildDate int8,
	data_ text
)
extent size 16 next size 16
lock mode row;

create table Shard (
	shardId int8 not null primary key,
	classNameId int8,
	classPK int8,
	name varchar(75)
)
extent size 16 next size 16
lock mode row;

create table ShoppingCart (
	cartId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	itemIds lvarchar,
	couponCodes varchar(75),
	altShipping int,
	insure boolean
)
extent size 16 next size 16
lock mode row;

create table ShoppingCategory (
	categoryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentCategoryId int8,
	name varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table ShoppingCoupon (
	couponId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	code_ varchar(75),
	name varchar(75),
	description lvarchar,
	startDate datetime YEAR TO FRACTION,
	endDate datetime YEAR TO FRACTION,
	active_ boolean,
	limitCategories lvarchar,
	limitSkus lvarchar,
	minOrder float,
	discount float,
	discountType varchar(75)
)
extent size 16 next size 16
lock mode row;

create table ShoppingItem (
	itemId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	categoryId int8,
	sku varchar(75),
	name varchar(200),
	description lvarchar,
	properties lvarchar,
	fields_ boolean,
	fieldsQuantities lvarchar,
	minQuantity int,
	maxQuantity int,
	price float,
	discount float,
	taxable boolean,
	shipping float,
	useShippingFormula boolean,
	requiresShipping boolean,
	stockQuantity int,
	featured_ boolean,
	sale_ boolean,
	smallImage boolean,
	smallImageId int8,
	smallImageURL varchar(75),
	mediumImage boolean,
	mediumImageId int8,
	mediumImageURL varchar(75),
	largeImage boolean,
	largeImageId int8,
	largeImageURL varchar(75)
)
extent size 16 next size 16
lock mode row;

create table ShoppingItemField (
	itemFieldId int8 not null primary key,
	itemId int8,
	name varchar(75),
	values_ lvarchar,
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table ShoppingItemPrice (
	itemPriceId int8 not null primary key,
	itemId int8,
	minQuantity int,
	maxQuantity int,
	price float,
	discount float,
	taxable boolean,
	shipping float,
	useShippingFormula boolean,
	status int
)
extent size 16 next size 16
lock mode row;

create table ShoppingOrder (
	orderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	number_ varchar(75),
	tax float,
	shipping float,
	altShipping varchar(75),
	requiresShipping boolean,
	insure boolean,
	insurance float,
	couponCodes varchar(75),
	couponDiscount float,
	billingFirstName varchar(75),
	billingLastName varchar(75),
	billingEmailAddress varchar(75),
	billingCompany varchar(75),
	billingStreet varchar(75),
	billingCity varchar(75),
	billingState varchar(75),
	billingZip varchar(75),
	billingCountry varchar(75),
	billingPhone varchar(75),
	shipToBilling boolean,
	shippingFirstName varchar(75),
	shippingLastName varchar(75),
	shippingEmailAddress varchar(75),
	shippingCompany varchar(75),
	shippingStreet varchar(75),
	shippingCity varchar(75),
	shippingState varchar(75),
	shippingZip varchar(75),
	shippingCountry varchar(75),
	shippingPhone varchar(75),
	ccName varchar(75),
	ccType varchar(75),
	ccNumber varchar(75),
	ccExpMonth int,
	ccExpYear int,
	ccVerNumber varchar(75),
	comments lvarchar,
	ppTxnId varchar(75),
	ppPaymentStatus varchar(75),
	ppPaymentGross float,
	ppReceiverEmail varchar(75),
	ppPayerEmail varchar(75),
	sendOrderEmail boolean,
	sendShippingEmail boolean
)
extent size 16 next size 16
lock mode row;

create table ShoppingOrderItem (
	orderItemId int8 not null primary key,
	orderId int8,
	itemId varchar(75),
	sku varchar(75),
	name varchar(200),
	description lvarchar,
	properties lvarchar,
	price float,
	quantity int,
	shippedDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table SocialActivity (
	activityId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate int8,
	mirrorActivityId int8,
	classNameId int8,
	classPK int8,
	type_ int,
	extraData lvarchar,
	receiverUserId int8
)
extent size 16 next size 16
lock mode row;

create table SocialRelation (
	uuid_ varchar(75),
	relationId int8 not null primary key,
	companyId int8,
	createDate int8,
	userId1 int8,
	userId2 int8,
	type_ int
)
extent size 16 next size 16
lock mode row;

create table SocialRequest (
	uuid_ varchar(75),
	requestId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	createDate int8,
	modifiedDate int8,
	classNameId int8,
	classPK int8,
	type_ int,
	extraData lvarchar,
	receiverUserId int8,
	status int
)
extent size 16 next size 16
lock mode row;

create table Subscription (
	subscriptionId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	frequency varchar(75)
)
extent size 16 next size 16
lock mode row;

create table TagsAsset (
	assetId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	visible boolean,
	startDate datetime YEAR TO FRACTION,
	endDate datetime YEAR TO FRACTION,
	publishDate datetime YEAR TO FRACTION,
	expirationDate datetime YEAR TO FRACTION,
	mimeType varchar(75),
	title varchar(255),
	description lvarchar,
	summary lvarchar,
	url lvarchar,
	height int,
	width int,
	priority float,
	viewCount int
)
extent size 16 next size 16
lock mode row;

create table TagsAssets_TagsEntries (
	assetId int8 not null,
	entryId int8 not null,
	primary key (assetId, entryId)
)
extent size 16 next size 16
lock mode row;

create table TagsEntry (
	entryId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentEntryId int8,
	name varchar(75),
	vocabularyId int8
)
extent size 16 next size 16
lock mode row;

create table TagsProperty (
	propertyId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	entryId int8,
	key_ varchar(75),
	value varchar(255)
)
extent size 16 next size 16
lock mode row;

create table TagsSource (
	sourceId int8 not null primary key,
	parentSourceId int8,
	name varchar(75),
	acronym varchar(75)
)
extent size 16 next size 16
lock mode row;

create table TagsVocabulary (
	vocabularyId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	description varchar(75),
	folksonomy boolean
)
extent size 16 next size 16
lock mode row;

create table TasksProposal (
	proposalId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK varchar(75),
	name varchar(75),
	description lvarchar,
	publishDate datetime YEAR TO FRACTION,
	dueDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table TasksReview (
	reviewId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	proposalId int8,
	assignedByUserId int8,
	assignedByUserName varchar(75),
	stage int,
	completed boolean,
	rejected boolean
)
extent size 16 next size 16
lock mode row;

create table User_ (
	uuid_ varchar(75),
	userId int8 not null primary key,
	companyId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	defaultUser boolean,
	contactId int8,
	password_ varchar(75),
	passwordEncrypted boolean,
	passwordReset boolean,
	passwordModifiedDate datetime YEAR TO FRACTION,
	reminderQueryQuestion varchar(75),
	reminderQueryAnswer varchar(75),
	graceLoginCount int,
	screenName varchar(75),
	emailAddress varchar(75),
	openId lvarchar(1024),
	portraitId int8,
	languageId varchar(75),
	timeZoneId varchar(75),
	greeting varchar(255),
	comments lvarchar,
	firstName varchar(75),
	middleName varchar(75),
	lastName varchar(75),
	jobTitle varchar(100),
	loginDate datetime YEAR TO FRACTION,
	loginIP varchar(75),
	lastLoginDate datetime YEAR TO FRACTION,
	lastLoginIP varchar(75),
	lastFailedLoginDate datetime YEAR TO FRACTION,
	failedLoginAttempts int,
	lockout boolean,
	lockoutDate datetime YEAR TO FRACTION,
	agreedToTermsOfUse boolean,
	active_ boolean
)
extent size 16 next size 16
lock mode row;

create table UserGroup (
	userGroupId int8 not null primary key,
	companyId int8,
	parentUserGroupId int8,
	name varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table UserGroupGroupRole (
	userGroupId int8 not null,
	groupId int8 not null,
	roleId int8 not null,
	primary key (userGroupId, groupId, roleId)
)
extent size 16 next size 16
lock mode row;

create table UserGroupRole (
	userId int8 not null,
	groupId int8 not null,
	roleId int8 not null,
	primary key (userId, groupId, roleId)
)
extent size 16 next size 16
lock mode row;

create table UserIdMapper (
	userIdMapperId int8 not null primary key,
	userId int8,
	type_ varchar(75),
	description varchar(75),
	externalUserId varchar(75)
)
extent size 16 next size 16
lock mode row;

create table Users_Groups (
	userId int8 not null,
	groupId int8 not null,
	primary key (userId, groupId)
)
extent size 16 next size 16
lock mode row;

create table Users_Orgs (
	userId int8 not null,
	organizationId int8 not null,
	primary key (userId, organizationId)
)
extent size 16 next size 16
lock mode row;

create table Users_Permissions (
	userId int8 not null,
	permissionId int8 not null,
	primary key (userId, permissionId)
)
extent size 16 next size 16
lock mode row;

create table Users_Roles (
	userId int8 not null,
	roleId int8 not null,
	primary key (userId, roleId)
)
extent size 16 next size 16
lock mode row;

create table Users_UserGroups (
	userGroupId int8 not null,
	userId int8 not null,
	primary key (userGroupId, userId)
)
extent size 16 next size 16
lock mode row;

create table UserTracker (
	userTrackerId int8 not null primary key,
	companyId int8,
	userId int8,
	modifiedDate datetime YEAR TO FRACTION,
	sessionId varchar(200),
	remoteAddr varchar(75),
	remoteHost varchar(75),
	userAgent varchar(200)
)
extent size 16 next size 16
lock mode row;

create table UserTrackerPath (
	userTrackerPathId int8 not null primary key,
	userTrackerId int8,
	path_ lvarchar,
	pathDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table Vocabulary (
	vocabularyId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	description varchar(75),
	folksonomy boolean
)
extent size 16 next size 16
lock mode row;

create table WebDAVProps (
	webDavPropsId int8 not null primary key,
	companyId int8,
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	props text
)
extent size 16 next size 16
lock mode row;

create table Website (
	websiteId int8 not null primary key,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	url lvarchar,
	typeId int,
	primary_ boolean
)
extent size 16 next size 16
lock mode row;

create table WikiNode (
	uuid_ varchar(75),
	nodeId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	name varchar(75),
	description lvarchar,
	lastPostDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table WikiPage (
	uuid_ varchar(75),
	pageId int8 not null primary key,
	resourcePrimKey int8,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	nodeId int8,
	title varchar(255),
	version float,
	minorEdit boolean,
	content text,
	summary lvarchar,
	format varchar(75),
	head boolean,
	parentTitle varchar(75),
	redirectTitle varchar(75)
)
extent size 16 next size 16
lock mode row;

create table WikiPageResource (
	resourcePrimKey int8 not null primary key,
	nodeId int8,
	title varchar(75)
)
extent size 16 next size 16
lock mode row;



insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (1, 'Canada', 'CA', 'CAN', '124', '001', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (2, 'China', 'CN', 'CHN', '156', '086', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (3, 'France', 'FR', 'FRA', '250', '033', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (4, 'Germany', 'DE', 'DEU', '276', '049', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (5, 'Hong Kong', 'HK', 'HKG', '344', '852', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (6, 'Hungary', 'HU', 'HUN', '348', '036', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (7, 'Israel', 'IL', 'ISR', '376', '972', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (8, 'Italy', 'IT', 'ITA', '380', '039', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (9, 'Japan', 'JP', 'JPN', '392', '081', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (10, 'South Korea', 'KR', 'KOR', '410', '082', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (11, 'Netherlands', 'NL', 'NLD', '528', '031', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (12, 'Portugal', 'PT', 'PRT', '620', '351', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (13, 'Russia', 'RU', 'RUS', '643', '007', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (14, 'Singapore', 'SG', 'SGP', '702', '065', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (15, 'Spain', 'ES', 'ESP', '724', '034', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (16, 'Turkey', 'TR', 'TUR', '792', '090', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (17, 'Vietnam', 'VM', 'VNM', '704', '084', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (18, 'United Kingdom', 'GB', 'GBR', '826', '044', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (19, 'United States', 'US', 'USA', '840', '001', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (20, 'Afghanistan', 'AF', 'AFG', '4', '093', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (21, 'Albania', 'AL', 'ALB', '8', '355', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (22, 'Algeria', 'DZ', 'DZA', '12', '213', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (23, 'American Samoa', 'AS', 'ASM', '16', '684', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (24, 'Andorra', 'AD', 'AND', '20', '376', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (25, 'Angola', 'AO', 'AGO', '24', '244', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (26, 'Anguilla', 'AI', 'AIA', '660', '264', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (27, 'Antarctica', 'AQ', 'ATA', '10', '672', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (28, 'Antigua', 'AG', 'ATG', '28', '268', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (29, 'Argentina', 'AR', 'ARG', '32', '054', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (30, 'Armenia', 'AM', 'ARM', '51', '374', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (31, 'Aruba', 'AW', 'ABW', '533', '297', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (32, 'Australia', 'AU', 'AUS', '36', '061', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (33, 'Austria', 'AT', 'AUT', '40', '043', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (34, 'Azerbaijan', 'AZ', 'AZE', '31', '994', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (35, 'Bahamas', 'BS', 'BHS', '44', '242', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (36, 'Bahrain', 'BH', 'BHR', '48', '973', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (37, 'Bangladesh', 'BD', 'BGD', '50', '880', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (38, 'Barbados', 'BB', 'BRB', '52', '246', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (39, 'Belarus', 'BY', 'BLR', '112', '375', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (40, 'Belgium', 'BE', 'BEL', '56', '032', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (41, 'Belize', 'BZ', 'BLZ', '84', '501', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (42, 'Benin', 'BJ', 'BEN', '204', '229', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (43, 'Bermuda', 'BM', 'BMU', '60', '441', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (44, 'Bhutan', 'BT', 'BTN', '64', '975', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (45, 'Bolivia', 'BO', 'BOL', '68', '591', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (46, 'Bosnia-Herzegovina', 'BA', 'BIH', '70', '387', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (47, 'Botswana', 'BW', 'BWA', '72', '267', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (48, 'Brazil', 'BR', 'BRA', '76', '055', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (49, 'British Virgin Islands', 'VG', 'VGB', '92', '284', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (50, 'Brunei', 'BN', 'BRN', '96', '673', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (51, 'Bulgaria', 'BG', 'BGR', '100', '359', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (52, 'Burkina Faso', 'BF', 'BFA', '854', '226', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (53, 'Burma (Myanmar)', 'MM', 'MMR', '104', '095', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (54, 'Burundi', 'BI', 'BDI', '108', '257', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (55, 'Cambodia', 'KH', 'KHM', '116', '855', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (56, 'Cameroon', 'CM', 'CMR', '120', '237', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (57, 'Cape Verde Island', 'CV', 'CPV', '132', '238', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (58, 'Cayman Islands', 'KY', 'CYM', '136', '345', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (59, 'Central African Republic', 'CF', 'CAF', '140', '236', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (60, 'Chad', 'TD', 'TCD', '148', '235', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (61, 'Chile', 'CL', 'CHL', '152', '056', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (62, 'Christmas Island', 'CX', 'CXR', '162', '061', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (63, 'Cocos Islands', 'CC', 'CCK', '166', '061', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (64, 'Colombia', 'CO', 'COL', '170', '057', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (65, 'Comoros', 'KM', 'COM', '174', '269', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (66, 'Republic of Congo', 'CD', 'COD', '180', '242', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (67, 'Democratic Republic of Congo', 'CG', 'COG', '178', '243', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (68, 'Cook Islands', 'CK', 'COK', '184', '682', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (69, 'Costa Rica', 'CR', 'CRI', '188', '506', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (70, 'Croatia', 'HR', 'HRV', '191', '385', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (71, 'Cuba', 'CU', 'CUB', '192', '053', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (72, 'Cyprus', 'CY', 'CYP', '196', '357', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (73, 'Czech Republic', 'CZ', 'CZE', '203', '420', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (74, 'Denmark', 'DK', 'DNK', '208', '045', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (75, 'Djibouti', 'DJ', 'DJI', '262', '253', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (76, 'Dominica', 'DM', 'DMA', '212', '767', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (77, 'Dominican Republic', 'DO', 'DOM', '214', '809', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (78, 'Ecuador', 'EC', 'ECU', '218', '593', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (79, 'Egypt', 'EG', 'EGY', '818', '020', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (80, 'El Salvador', 'SV', 'SLV', '222', '503', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (81, 'Equatorial Guinea', 'GQ', 'GNQ', '226', '240', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (82, 'Eritrea', 'ER', 'ERI', '232', '291', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (83, 'Estonia', 'EE', 'EST', '233', '372', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (84, 'Ethiopia', 'ET', 'ETH', '231', '251', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (85, 'Faeroe Islands', 'FO', 'FRO', '234', '298', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (86, 'Falkland Islands', 'FK', 'FLK', '238', '500', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (87, 'Fiji Islands', 'FJ', 'FJI', '242', '679', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (88, 'Finland', 'FI', 'FIN', '246', '358', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (89, 'French Guiana', 'GF', 'GUF', '254', '594', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (90, 'French Polynesia', 'PF', 'PYF', '258', '689', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (91, 'Gabon', 'GA', 'GAB', '266', '241', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (92, 'Gambia', 'GM', 'GMB', '270', '220', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (93, 'Georgia', 'GE', 'GEO', '268', '995', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (94, 'Ghana', 'GH', 'GHA', '288', '233', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (95, 'Gibraltar', 'GI', 'GIB', '292', '350', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (96, 'Greece', 'GR', 'GRC', '300', '030', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (97, 'Greenland', 'GL', 'GRL', '304', '299', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (98, 'Grenada', 'GD', 'GRD', '308', '473', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (99, 'Guadeloupe', 'GP', 'GLP', '312', '590', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (100, 'Guam', 'GU', 'GUM', '316', '671', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (101, 'Guatemala', 'GT', 'GTM', '320', '502', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (102, 'Guinea', 'GN', 'GIN', '324', '224', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (103, 'Guinea-Bissau', 'GW', 'GNB', '624', '245', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (104, 'Guyana', 'GY', 'GUY', '328', '592', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (105, 'Haiti', 'HT', 'HTI', '332', '509', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (106, 'Honduras', 'HN', 'HND', '340', '504', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (107, 'Iceland', 'IS', 'ISL', '352', '354', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (108, 'India', 'IN', 'IND', '356', '091', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (109, 'Indonesia', 'ID', 'IDN', '360', '062', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (110, 'Iran', 'IR', 'IRN', '364', '098', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (111, 'Iraq', 'IQ', 'IRQ', '368', '964', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (112, 'Ireland', 'IE', 'IRL', '372', '353', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (113, 'Ivory Coast', 'CI', 'CIV', '384', '225', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (114, 'Jamaica', 'JM', 'JAM', '388', '876', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (115, 'Jordan', 'JO', 'JOR', '400', '962', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (116, 'Kazakhstan', 'KZ', 'KAZ', '398', '007', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (117, 'Kenya', 'KE', 'KEN', '404', '254', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (118, 'Kiribati', 'KI', 'KIR', '408', '686', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (119, 'Kuwait', 'KW', 'KWT', '414', '965', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (120, 'North Korea', 'KP', 'PRK', '408', '850', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (121, 'Kyrgyzstan', 'KG', 'KGZ', '471', '996', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (122, 'Laos', 'LA', 'LAO', '418', '856', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (123, 'Latvia', 'LV', 'LVA', '428', '371', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (124, 'Lebanon', 'LB', 'LBN', '422', '961', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (125, 'Lesotho', 'LS', 'LSO', '426', '266', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (126, 'Liberia', 'LR', 'LBR', '430', '231', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (127, 'Libya', 'LY', 'LBY', '434', '218', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (128, 'Liechtenstein', 'LI', 'LIE', '438', '423', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (129, 'Lithuania', 'LT', 'LTU', '440', '370', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (130, 'Luxembourg', 'LU', 'LUX', '442', '352', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (131, 'Macau', 'MO', 'MAC', '446', '853', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (132, 'Macedonia', 'MK', 'MKD', '807', '389', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (133, 'Madagascar', 'MG', 'MDG', '450', '261', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (134, 'Malawi', 'MW', 'MWI', '454', '265', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (135, 'Malaysia', 'MY', 'MYS', '458', '060', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (136, 'Maldives', 'MV', 'MDV', '462', '960', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (137, 'Mali', 'ML', 'MLI', '466', '223', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (138, 'Malta', 'MT', 'MLT', '470', '356', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (139, 'Marshall Islands', 'MH', 'MHL', '584', '692', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (140, 'Martinique', 'MQ', 'MTQ', '474', '596', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (141, 'Mauritania', 'MR', 'MRT', '478', '222', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (142, 'Mauritius', 'MU', 'MUS', '480', '230', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (143, 'Mayotte Island', 'YT', 'MYT', '175', '269', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (144, 'Mexico', 'MX', 'MEX', '484', '052', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (145, 'Micronesia', 'FM', 'FSM', '583', '691', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (146, 'Moldova', 'MD', 'MDA', '498', '373', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (147, 'Monaco', 'MC', 'MCO', '492', '377', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (148, 'Mongolia', 'MN', 'MNG', '496', '976', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (149, 'Montenegro', 'ME', 'MNE', '499', '382', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (150, 'Montserrat', 'MS', 'MSR', '500', '664', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (151, 'Morocco', 'MA', 'MAR', '504', '212', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (152, 'Mozambique', 'MZ', 'MOZ', '508', '258', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (153, 'Namibia', 'NA', 'NAM', '516', '264', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (154, 'Nauru', 'NR', 'NRU', '520', '674', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (155, 'Nepal', 'NP', 'NPL', '524', '977', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (156, 'Netherlands Antilles', 'AN', 'ANT', '530', '599', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (157, 'New Caledonia', 'NC', 'NCL', '540', '687', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (158, 'New Zealand', 'NZ', 'NZL', '554', '064', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (159, 'Nicaragua', 'NI', 'NIC', '558', '505', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (160, 'Niger', 'NE', 'NER', '562', '227', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (161, 'Nigeria', 'NG', 'NGA', '566', '234', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (162, 'Niue', 'NU', 'NIU', '570', '683', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (163, 'Norfolk Island', 'NF', 'NFK', '574', '672', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (164, 'Norway', 'NO', 'NOR', '578', '047', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (165, 'Oman', 'OM', 'OMN', '512', '968', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (166, 'Pakistan', 'PK', 'PAK', '586', '092', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (167, 'Palau', 'PW', 'PLW', '585', '680', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (168, 'Palestine', 'PS', 'PSE', '275', '970', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (169, 'Panama', 'PA', 'PAN', '591', '507', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (170, 'Papua New Guinea', 'PG', 'PNG', '598', '675', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (171, 'Paraguay', 'PY', 'PRY', '600', '595', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (172, 'Peru', 'PE', 'PER', '604', '051', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (173, 'Philippines', 'PH', 'PHL', '608', '063', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (174, 'Poland', 'PL', 'POL', '616', '048', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (175, 'Puerto Rico', 'PR', 'PRI', '630', '787', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (176, 'Qatar', 'QA', 'QAT', '634', '974', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (177, 'Reunion Island', 'RE', 'REU', '638', '262', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (178, 'Romania', 'RO', 'ROU', '642', '040', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (179, 'Rwanda', 'RW', 'RWA', '646', '250', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (180, 'St. Helena', 'SH', 'SHN', '654', '290', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (181, 'St. Kitts', 'KN', 'KNA', '659', '869', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (182, 'St. Lucia', 'LC', 'LCA', '662', '758', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (183, 'St. Pierre & Miquelon', 'PM', 'SPM', '666', '508', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (184, 'St. Vincent', 'VC', 'VCT', '670', '784', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (185, 'San Marino', 'SM', 'SMR', '674', '378', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (186, 'Sao Tome & Principe', 'ST', 'STP', '678', '239', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (187, 'Saudi Arabia', 'SA', 'SAU', '682', '966', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (188, 'Senegal', 'SN', 'SEN', '686', '221', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (189, 'Serbia', 'RS', 'SRB', '688', '381', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (190, 'Seychelles', 'SC', 'SYC', '690', '248', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (191, 'Sierra Leone', 'SL', 'SLE', '694', '249', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (192, 'Slovakia', 'SK', 'SVK', '703', '421', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (193, 'Slovenia', 'SI', 'SVN', '705', '386', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (194, 'Solomon Islands', 'SB', 'SLB', '90', '677', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (195, 'Somalia', 'SO', 'SOM', '706', '252', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (196, 'South Africa', 'ZA', 'ZAF', '710', '027', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (197, 'Sri Lanka', 'LK', 'LKA', '144', '094', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (198, 'Sudan', 'SD', 'SDN', '736', '095', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (199, 'Suriname', 'SR', 'SUR', '740', '597', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (200, 'Swaziland', 'SZ', 'SWZ', '748', '268', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (201, 'Sweden', 'SE', 'SWE', '752', '046', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (202, 'Switzerland', 'CH', 'CHE', '756', '041', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (203, 'Syria', 'SY', 'SYR', '760', '963', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (204, 'Taiwan', 'TW', 'TWN', '158', '886', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (205, 'Tajikistan', 'TJ', 'TJK', '762', '992', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (206, 'Tanzania', 'TZ', 'TZA', '834', '255', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (207, 'Thailand', 'TH', 'THA', '764', '066', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (208, 'Togo', 'TG', 'TGO', '768', '228', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (209, 'Tonga', 'TO', 'TON', '776', '676', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (210, 'Trinidad & Tobago', 'TT', 'TTO', '780', '868', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (211, 'Tunisia', 'TN', 'TUN', '788', '216', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (212, 'Turkmenistan', 'TM', 'TKM', '795', '993', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (213, 'Turks & Caicos', 'TC', 'TCA', '796', '649', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (214, 'Tuvalu', 'TV', 'TUV', '798', '688', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (215, 'Uganda', 'UG', 'UGA', '800', '256', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (216, 'Ukraine', 'UA', 'UKR', '804', '380', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (217, 'United Arab Emirates', 'AE', 'ARE', '784', '971', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (218, 'Uruguay', 'UY', 'URY', '858', '598', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (219, 'Uzbekistan', 'UZ', 'UZB', '860', '998', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (220, 'Vanuatu', 'VU', 'VUT', '548', '678', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (221, 'Vatican City', 'VA', 'VAT', '336', '039', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (222, 'Venezuela', 'VE', 'VEN', '862', '058', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (223, 'Wallis & Futuna', 'WF', 'WLF', '876', '681', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (224, 'Western Samoa', 'EH', 'ESH', '732', '685', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (225, 'Yemen', 'YE', 'YEM', '887', '967', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (226, 'Zambia', 'ZM', 'ZMB', '894', '260', 'T');
insert into Country (countryId, name, a2, a3, number_, idd_, active_) values (227, 'Zimbabwe', 'ZW', 'ZWE', '716', '263', 'T');

insert into Region (regionId, countryId, regionCode, name, active_) values (1001, 1, 'AB', 'Alberta', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1002, 1, 'BC', 'British Columbia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1003, 1, 'MB', 'Manitoba', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1004, 1, 'NB', 'New Brunswick', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1005, 1, 'NL', 'Newfoundland and Labrador', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1006, 1, 'NT', 'Northwest Territories', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1007, 1, 'NS', 'Nova Scotia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1008, 1, 'NU', 'Nunavut', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1009, 1, 'ON', 'Ontario', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1010, 1, 'PE', 'Prince Edward Island', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1011, 1, 'QC', 'Quebec', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1012, 1, 'SK', 'Saskatchewan', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (1013, 1, 'YT', 'Yukon', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3001, 3, 'A', 'Alsace', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3002, 3, 'B', 'Aquitaine', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3003, 3, 'C', 'Auvergne', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3004, 3, 'P', 'Basse-Normandie', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3005, 3, 'D', 'Bourgogne', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3006, 3, 'E', 'Bretagne', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3007, 3, 'F', 'Centre', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3008, 3, 'G', 'Champagne-Ardenne', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3009, 3, 'H', 'Corse', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3010, 3, 'GF', 'Guyane', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3011, 3, 'I', 'Franche Comté', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3012, 3, 'GP', 'Guadeloupe', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3013, 3, 'Q', 'Haute-Normandie', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3014, 3, 'J', 'Île-de-France', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3015, 3, 'K', 'Languedoc-Roussillon', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3016, 3, 'L', 'Limousin', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3017, 3, 'M', 'Lorraine', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3018, 3, 'MQ', 'Martinique', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3019, 3, 'N', 'Midi-Pyrénées', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3020, 3, 'O', 'Nord Pas de Calais', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3021, 3, 'R', 'Pays de la Loire', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3022, 3, 'S', 'Picardie', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3023, 3, 'T', 'Poitou-Charentes', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3024, 3, 'U', 'Provence-Alpes-Côte-d\'Azur', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3025, 3, 'RE', 'Réunion', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (3026, 3, 'V', 'Rhône-Alpes', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4001, 4, 'BW', 'Baden-Württemberg', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4002, 4, 'BY', 'Bayern', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4003, 4, 'BE', 'Berlin', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4004, 4, 'BR', 'Brandenburg', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4005, 4, 'HB', 'Bremen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4006, 4, 'HH', 'Hamburg', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4007, 4, 'HE', 'Hessen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4008, 4, 'MV', 'Mecklenburg-Vorpommern', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4009, 4, 'NI', 'Niedersachsen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4010, 4, 'NW', 'Nordrhein-Westfalen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4011, 4, 'RP', 'Rheinland-Pfalz', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4012, 4, 'SL', 'Saarland', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4013, 4, 'SN', 'Sachsen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4014, 4, 'ST', 'Sachsen-Anhalt', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4015, 4, 'SH', 'Schleswig-Holstein', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (4016, 4, 'TH', 'Thüringen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8001, 8, 'AG', 'Agrigento', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8002, 8, 'AL', 'Alessandria', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8003, 8, 'AN', 'Ancona', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8004, 8, 'AO', 'Aosta', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8005, 8, 'AR', 'Arezzo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8006, 8, 'AP', 'Ascoli Piceno', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8007, 8, 'AT', 'Asti', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8008, 8, 'AV', 'Avellino', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8009, 8, 'BA', 'Bari', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8010, 8, 'BT', 'Barletta-Andria-Trani', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8011, 8, 'BL', 'Belluno', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8012, 8, 'BN', 'Benevento', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8013, 8, 'BG', 'Bergamo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8014, 8, 'BI', 'Biella', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8015, 8, 'BO', 'Bologna', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8016, 8, 'BZ', 'Bolzano', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8017, 8, 'BS', 'Brescia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8018, 8, 'BR', 'Brindisi', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8019, 8, 'CA', 'Cagliari', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8020, 8, 'CL', 'Caltanissetta', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8021, 8, 'CB', 'Campobasso', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8022, 8, 'CI', 'Carbonia-Iglesias', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8023, 8, 'CE', 'Caserta', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8024, 8, 'CT', 'Catania', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8025, 8, 'CZ', 'Catanzaro', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8026, 8, 'CH', 'Chieti', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8027, 8, 'CO', 'Como', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8028, 8, 'CS', 'Cosenza', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8029, 8, 'CR', 'Cremona', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8030, 8, 'KR', 'Crotone', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8031, 8, 'CN', 'Cuneo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8032, 8, 'EN', 'Enna', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8033, 8, 'FM', 'Fermo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8034, 8, 'FE', 'Ferrara', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8035, 8, 'FI', 'Firenze', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8036, 8, 'FG', 'Foggia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8037, 8, 'FC', 'Forli-Cesena', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8038, 8, 'FR', 'Frosinone', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8039, 8, 'GE', 'Genova', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8040, 8, 'GO', 'Gorizia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8041, 8, 'GR', 'Grosseto', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8042, 8, 'IM', 'Imperia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8043, 8, 'IS', 'Isernia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8044, 8, 'AQ', 'L''Aquila', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8045, 8, 'SP', 'La Spezia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8046, 8, 'LT', 'Latina', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8047, 8, 'LE', 'Lecce', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8048, 8, 'LC', 'Lecco', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8049, 8, 'LI', 'Livorno', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8050, 8, 'LO', 'Lodi', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8051, 8, 'LU', 'Lucca', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8052, 8, 'MC', 'Macerata', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8053, 8, 'MN', 'Mantova', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8054, 8, 'MS', 'Massa-Carrara', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8055, 8, 'MT', 'Matera', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8056, 8, 'MA', 'Medio Campidano', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8057, 8, 'ME', 'Messina', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8058, 8, 'MI', 'Milano', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8059, 8, 'MO', 'Modena', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8060, 8, 'MZ', 'Monza', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8061, 8, 'NA', 'Napoli', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8062, 8, 'NO', 'Novara', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8063, 8, 'NU', 'Nuoro', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8064, 8, 'OG', 'Ogliastra', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8065, 8, 'OT', 'Olbia-Tempio', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8066, 8, 'OR', 'Oristano', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8067, 8, 'PD', 'Padova', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8068, 8, 'PA', 'Palermo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8069, 8, 'PR', 'Parma', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8070, 8, 'PV', 'Pavia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8071, 8, 'PG', 'Perugia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8072, 8, 'PU', 'Pesaro e Urbino', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8073, 8, 'PE', 'Pescara', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8074, 8, 'PC', 'Piacenza', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8075, 8, 'PI', 'Pisa', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8076, 8, 'PT', 'Pistoia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8077, 8, 'PN', 'Pordenone', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8078, 8, 'PZ', 'Potenza', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8079, 8, 'PO', 'Prato', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8080, 8, 'RG', 'Ragusa', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8081, 8, 'RA', 'Ravenna', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8082, 8, 'RC', 'Reggio Calabria', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8083, 8, 'RE', 'Reggio Emilia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8084, 8, 'RI', 'Rieti', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8085, 8, 'RN', 'Rimini', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8086, 8, 'RM', 'Roma', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8087, 8, 'RO', 'Rovigo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8088, 8, 'SA', 'Salerno', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8089, 8, 'SS', 'Sassari', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8090, 8, 'SV', 'Savona', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8091, 8, 'SI', 'Siena', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8092, 8, 'SR', 'Siracusa', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8093, 8, 'SO', 'Sondrio', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8094, 8, 'TA', 'Taranto', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8095, 8, 'TE', 'Teramo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8096, 8, 'TR', 'Terni', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8097, 8, 'TO', 'Torino', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8098, 8, 'TP', 'Trapani', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8099, 8, 'TN', 'Trento', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8100, 8, 'TV', 'Treviso', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8101, 8, 'TS', 'Trieste', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8102, 8, 'UD', 'Udine', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8103, 8, 'VA', 'Varese', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8104, 8, 'VE', 'Venezia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8105, 8, 'VB', 'Verbano-Cusio-Ossola', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8106, 8, 'VC', 'Vercelli', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8107, 8, 'VR', 'Verona', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8108, 8, 'VV', 'Vibo Valentia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8109, 8, 'VI', 'Vicenza', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (8110, 8, 'VT', 'Viterbo', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15001, 15, 'AN', 'Andalusia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15002, 15, 'AR', 'Aragon', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15003, 15, 'AS', 'Asturias', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15004, 15, 'IB', 'Balearic Islands', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15005, 15, 'PV', 'Basque Country', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15006, 15, 'CN', 'Canary Islands', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15007, 15, 'CB', 'Cantabria', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15008, 15, 'CL', 'Castile and Leon', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15009, 15, 'CM', 'Castile-La Mancha', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15010, 15, 'CT', 'Catalonia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15011, 15, 'CE', 'Ceuta', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15012, 15, 'EX', 'Extremadura', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15013, 15, 'GA', 'Galicia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15014, 15, 'LO', 'La Rioja', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15015, 15, 'M', 'Madrid', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15016, 15, 'ML', 'Melilla', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15017, 15, 'MU', 'Murcia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15018, 15, 'NA', 'Navarra', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (15019, 15, 'VC', 'Valencia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19001, 19, 'AL', 'Alabama', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19002, 19, 'AK', 'Alaska', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19003, 19, 'AZ', 'Arizona', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19004, 19, 'AR', 'Arkansas', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19005, 19, 'CA', 'California', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19006, 19, 'CO', 'Colorado', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19007, 19, 'CT', 'Connecticut', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19008, 19, 'DC', 'District of Columbia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19009, 19, 'DE', 'Delaware', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19010, 19, 'FL', 'Florida', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19011, 19, 'GA', 'Georgia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19012, 19, 'HI', 'Hawaii', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19013, 19, 'ID', 'Idaho', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19014, 19, 'IL', 'Illinois', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19015, 19, 'IN', 'Indiana', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19016, 19, 'IA', 'Iowa', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19017, 19, 'KS', 'Kansas', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19018, 19, 'KY', 'Kentucky ', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19019, 19, 'LA', 'Louisiana ', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19020, 19, 'ME', 'Maine', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19021, 19, 'MD', 'Maryland', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19022, 19, 'MA', 'Massachusetts', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19023, 19, 'MI', 'Michigan', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19024, 19, 'MN', 'Minnesota', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19025, 19, 'MS', 'Mississippi', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19026, 19, 'MO', 'Missouri', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19027, 19, 'MT', 'Montana', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19028, 19, 'NE', 'Nebraska', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19029, 19, 'NV', 'Nevada', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19030, 19, 'NH', 'New Hampshire', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19031, 19, 'NJ', 'New Jersey', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19032, 19, 'NM', 'New Mexico', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19033, 19, 'NY', 'New York', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19034, 19, 'NC', 'North Carolina', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19035, 19, 'ND', 'North Dakota', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19036, 19, 'OH', 'Ohio', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19037, 19, 'OK', 'Oklahoma ', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19038, 19, 'OR', 'Oregon', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19039, 19, 'PA', 'Pennsylvania', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19040, 19, 'PR', 'Puerto Rico', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19041, 19, 'RI', 'Rhode Island', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19042, 19, 'SC', 'South Carolina', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19043, 19, 'SD', 'South Dakota', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19044, 19, 'TN', 'Tennessee', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19045, 19, 'TX', 'Texas', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19046, 19, 'UT', 'Utah', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19047, 19, 'VT', 'Vermont', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19048, 19, 'VA', 'Virginia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19049, 19, 'WA', 'Washington', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19050, 19, 'WV', 'West Virginia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19051, 19, 'WI', 'Wisconsin', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (19052, 19, 'WY', 'Wyoming', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32001, 32, 'ACT', 'Australian Capital Territory', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32002, 32, 'NSW', 'New South Wales', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32003, 32, 'NT', 'Northern Territory', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32004, 32, 'QLD', 'Queensland', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32005, 32, 'SA', 'South Australia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32006, 32, 'TAS', 'Tasmania', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32007, 32, 'VIC', 'Victoria', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (32008, 32, 'WA', 'Western Australia', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202001, 202, 'AG', 'Aargau', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202002, 202, 'AR', 'Appenzell Ausserrhoden', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202003, 202, 'AI', 'Appenzell Innerrhoden', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202004, 202, 'BL', 'Basel-Landschaft', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202005, 202, 'BS', 'Basel-Stadt', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202006, 202, 'BE', 'Bern', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202007, 202, 'FR', 'Fribourg', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202008, 202, 'GE', 'Geneva', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202009, 202, 'GL', 'Glarus', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202010, 202, 'GR', 'Graubünden', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202011, 202, 'JU', 'Jura', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202012, 202, 'LU', 'Lucerne', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202013, 202, 'NE', 'Neuchâtel', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202014, 202, 'NW', 'Nidwalden', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202015, 202, 'OW', 'Obwalden', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202016, 202, 'SH', 'Schaffhausen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202017, 202, 'SZ', 'Schwyz', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202018, 202, 'SO', 'Solothurn', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202019, 202, 'SG', 'St. Gallen', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202020, 202, 'TG', 'Thurgau', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202021, 202, 'TI', 'Ticino', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202022, 202, 'UR', 'Uri', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202023, 202, 'VS', 'Valais', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202024, 202, 'VD', 'Vaud', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202025, 202, 'ZG', 'Zug', 'T');
insert into Region (regionId, countryId, regionCode, name, active_) values (202026, 202, 'ZH', 'Zürich', 'T');

--
-- List types for accounts
--

insert into ListType (listTypeId, name, type_) values (10000, 'Billing', 'com.liferay.portal.model.Account.address');
insert into ListType (listTypeId, name, type_) values (10001, 'Other', 'com.liferay.portal.model.Account.address');
insert into ListType (listTypeId, name, type_) values (10002, 'P.O. Box', 'com.liferay.portal.model.Account.address');
insert into ListType (listTypeId, name, type_) values (10003, 'Shipping', 'com.liferay.portal.model.Account.address');

insert into ListType (listTypeId, name, type_) values (10004, 'E-mail', 'com.liferay.portal.model.Account.emailAddress');
insert into ListType (listTypeId, name, type_) values (10005, 'E-mail 2', 'com.liferay.portal.model.Account.emailAddress');
insert into ListType (listTypeId, name, type_) values (10006, 'E-mail 3', 'com.liferay.portal.model.Account.emailAddress');

insert into ListType (listTypeId, name, type_) values (10007, 'Fax', 'com.liferay.portal.model.Account.phone');
insert into ListType (listTypeId, name, type_) values (10008, 'Local', 'com.liferay.portal.model.Account.phone');
insert into ListType (listTypeId, name, type_) values (10009, 'Other', 'com.liferay.portal.model.Account.phone');
insert into ListType (listTypeId, name, type_) values (10010, 'Toll-Free', 'com.liferay.portal.model.Account.phone');
insert into ListType (listTypeId, name, type_) values (10011, 'TTY', 'com.liferay.portal.model.Account.phone');

insert into ListType (listTypeId, name, type_) values (10012, 'Intranet', 'com.liferay.portal.model.Account.website');
insert into ListType (listTypeId, name, type_) values (10013, 'Public', 'com.liferay.portal.model.Account.website');

--
-- List types for contacts
--

insert into ListType (listTypeId, name, type_) values (11000, 'Business', 'com.liferay.portal.model.Contact.address');
insert into ListType (listTypeId, name, type_) values (11001, 'Other', 'com.liferay.portal.model.Contact.address');
insert into ListType (listTypeId, name, type_) values (11002, 'Personal', 'com.liferay.portal.model.Contact.address');

insert into ListType (listTypeId, name, type_) values (11003, 'E-mail', 'com.liferay.portal.model.Contact.emailAddress');
insert into ListType (listTypeId, name, type_) values (11004, 'E-mail 2', 'com.liferay.portal.model.Contact.emailAddress');
insert into ListType (listTypeId, name, type_) values (11005, 'E-mail 3', 'com.liferay.portal.model.Contact.emailAddress');

insert into ListType (listTypeId, name, type_) values (11006, 'Business', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11007, 'Business Fax', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11008, 'Mobile', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11009, 'Other', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11010, 'Pager', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11011, 'Personal', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11012, 'Personal Fax', 'com.liferay.portal.model.Contact.phone');
insert into ListType (listTypeId, name, type_) values (11013, 'TTY', 'com.liferay.portal.model.Contact.phone');

insert into ListType (listTypeId, name, type_) values (11014, 'Dr.', 'com.liferay.portal.model.Contact.prefix');
insert into ListType (listTypeId, name, type_) values (11015, 'Mr.', 'com.liferay.portal.model.Contact.prefix');
insert into ListType (listTypeId, name, type_) values (11016, 'Mrs.', 'com.liferay.portal.model.Contact.prefix');
insert into ListType (listTypeId, name, type_) values (11017, 'Ms.', 'com.liferay.portal.model.Contact.prefix');

insert into ListType (listTypeId, name, type_) values (11020, 'II', 'com.liferay.portal.model.Contact.suffix');
insert into ListType (listTypeId, name, type_) values (11021, 'III', 'com.liferay.portal.model.Contact.suffix');
insert into ListType (listTypeId, name, type_) values (11022, 'IV', 'com.liferay.portal.model.Contact.suffix');
insert into ListType (listTypeId, name, type_) values (11023, 'Jr.', 'com.liferay.portal.model.Contact.suffix');
insert into ListType (listTypeId, name, type_) values (11024, 'PhD.', 'com.liferay.portal.model.Contact.suffix');
insert into ListType (listTypeId, name, type_) values (11025, 'Sr.', 'com.liferay.portal.model.Contact.suffix');

insert into ListType (listTypeId, name, type_) values (11026, 'Blog', 'com.liferay.portal.model.Contact.website');
insert into ListType (listTypeId, name, type_) values (11027, 'Business', 'com.liferay.portal.model.Contact.website');
insert into ListType (listTypeId, name, type_) values (11028, 'Other', 'com.liferay.portal.model.Contact.website');
insert into ListType (listTypeId, name, type_) values (11029, 'Personal', 'com.liferay.portal.model.Contact.website');

--
-- List types for organizations
--

insert into ListType (listTypeId, name, type_) values (12000, 'Billing', 'com.liferay.portal.model.Organization.address');
insert into ListType (listTypeId, name, type_) values (12001, 'Other', 'com.liferay.portal.model.Organization.address');
insert into ListType (listTypeId, name, type_) values (12002, 'P.O. Box', 'com.liferay.portal.model.Organization.address');
insert into ListType (listTypeId, name, type_) values (12003, 'Shipping', 'com.liferay.portal.model.Organization.address');

insert into ListType (listTypeId, name, type_) values (12004, 'E-mail', 'com.liferay.portal.model.Organization.emailAddress');
insert into ListType (listTypeId, name, type_) values (12005, 'E-mail 2', 'com.liferay.portal.model.Organization.emailAddress');
insert into ListType (listTypeId, name, type_) values (12006, 'E-mail 3', 'com.liferay.portal.model.Organization.emailAddress');

insert into ListType (listTypeId, name, type_) values (12007, 'Fax', 'com.liferay.portal.model.Organization.phone');
insert into ListType (listTypeId, name, type_) values (12008, 'Local', 'com.liferay.portal.model.Organization.phone');
insert into ListType (listTypeId, name, type_) values (12009, 'Other', 'com.liferay.portal.model.Organization.phone');
insert into ListType (listTypeId, name, type_) values (12010, 'Toll-Free', 'com.liferay.portal.model.Organization.phone');
insert into ListType (listTypeId, name, type_) values (12011, 'TTY', 'com.liferay.portal.model.Organization.phone');

insert into ListType (listTypeId, name, type_) values (12012, 'Administrative', 'com.liferay.portal.model.Organization.service');
insert into ListType (listTypeId, name, type_) values (12013, 'Contracts', 'com.liferay.portal.model.Organization.service');
insert into ListType (listTypeId, name, type_) values (12014, 'Donation', 'com.liferay.portal.model.Organization.service');
insert into ListType (listTypeId, name, type_) values (12015, 'Retail', 'com.liferay.portal.model.Organization.service');
insert into ListType (listTypeId, name, type_) values (12016, 'Training', 'com.liferay.portal.model.Organization.service');

insert into ListType (listTypeId, name, type_) values (12017, 'Full Member', 'com.liferay.portal.model.Organization.status');
insert into ListType (listTypeId, name, type_) values (12018, 'Provisional Member', 'com.liferay.portal.model.Organization.status');

insert into ListType (listTypeId, name, type_) values (12019, 'Intranet', 'com.liferay.portal.model.Organization.website');
insert into ListType (listTypeId, name, type_) values (12020, 'Public', 'com.liferay.portal.model.Organization.website');



insert into Counter values ('com.liferay.counter.model.Counter', 10000);






insert into Company (companyId, accountId, webId, virtualHost, mx) values (1, 7, 'liferay.com', 'localhost', 'liferay.com');
insert into Account_ (accountId, companyId, userId, userName, createDate, modifiedDate, parentAccountId, name, legalName, legalId, legalType, sicCode, tickerSymbol, industry, type_, size_) values (7, 1, 5, '', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 0, 'Liferay', 'Liferay, Inc.', '', '', '', '', '', '', '');


insert into ClassName_ (classNameId, value) values (8, 'com.liferay.portal.model.Company');
insert into ClassName_ (classNameId, value) values (9, 'com.liferay.portal.model.Group');
insert into ClassName_ (classNameId, value) values (10, 'com.liferay.portal.model.Organization');
insert into ClassName_ (classNameId, value) values (11, 'com.liferay.portal.model.Role');
insert into ClassName_ (classNameId, value) values (12, 'com.liferay.portal.model.User');

insert into Shard (shardId, classNameId, classPK, name) values (13, 8, 1, 'default');


insert into Role_ (roleId, companyId, classNameId, classPK, name, description, type_) values (14, 1, 11, 14, 'Administrator', '', 1);
insert into Role_ (roleId, companyId, classNameId, classPK, name, description, type_) values (15, 1, 11, 15, 'Guest', '', 1);
insert into Role_ (roleId, companyId, classNameId, classPK, name, description, type_) values (16, 1, 11, 16, 'Power User', '', 1);
insert into Role_ (roleId, companyId, classNameId, classPK, name, description, type_) values (17, 1, 11, 17, 'User', '', 1);


insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (18, 1, 5, 9, 18, 0, 0, 'Guest', '/guest', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (19, 1, 18, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (20, 1, 18, 'F', 'F', 'classic', '01', 0);


insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (21, 1, 0, 'Liferay, Inc.', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (22, 1, 5, 10, 21, 0, 0, '22', '/22', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (23, 1, 22, 'T', 'F', 'classic', '01', 1);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (24, 1, 22, 'F', 'F', 'classic', '01', 1);
insert into Layout (plid, groupId, companyId, privateLayout, layoutId, parentLayoutId, name, type_, typeSettings, hidden_, friendlyURL, priority) values (25, 22, 1, 'F', 1, 0, '<?xml version="1.0"?>\n\n<root>\n  <name>Liferay, Inc. Extranet</name>\n</root>', 'portlet', 'layout-template-id=2_columns_ii\ncolumn-1=3,\ncolumn-2=19,', 'F', '/1', 0);
insert into Layout (plid, groupId, companyId, privateLayout, layoutId, parentLayoutId, name, type_, typeSettings, hidden_, friendlyURL, priority) values (26, 22, 1, 'T', 1, 0, '<?xml version="1.0"?>\n\n<root>\n  <name>Liferay, Inc. Intranet</name>\n</root>', 'portlet', 'layout-template-id=2_columns_ii\ncolumn-1=3,\ncolumn-2=19,', 'F', '/1', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (27, 1, 21, 'Liferay Engineering', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (28, 1, 5, 10, 27, 0, 0, '28', '/28', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (29, 1, 28, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (30, 1, 28, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (31, 1, 21, 'Liferay Consulting', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (32, 1, 5, 10, 31, 0, 0, '32', '/32', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (33, 1, 32, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (34, 1, 32, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (35, 1, 21, 'Liferay Support', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (36, 1, 5, 10, 35, 0, 0, '36', '/36', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (37, 1, 36, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (38, 1, 36, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (39, 1, 21, 'Liferay Sales', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (40, 1, 5, 10, 39, 0, 0, '40', '/40', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (41, 1, 40, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (42, 1, 40, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (43, 1, 21, 'Liferay Marketing', 'regular-organization', 'T', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (44, 1, 5, 10, 43, 0, 0, '44', '/44', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (45, 1, 44, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (46, 1, 44, 'F', 'F', 'classic', '01', 0);


insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (47, 1, 21, 'Liferay Los Angeles', 'location', 'F', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (48, 1, 5, 10, 47, 0, 0, '48', '/48', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (49, 1, 48, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (50, 1, 48, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (51, 1, 21, 'Liferay San Francisco', 'location', 'F', 5, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (52, 1, 5, 10, 51, 0, 0, '52', '/52', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (53, 1, 52, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (54, 1, 52, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (55, 1, 21, 'Liferay Chicago', 'location', 'F', 14, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (56, 1, 5, 10, 55, 0, 0, '56', '/56', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (57, 1, 56, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (58, 1, 56, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (59, 1, 21, 'Liferay New York', 'location', 'F', 33, 19, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (60, 1, 5, 10, 59, 0, 0, '60', '/60', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (61, 1, 60, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (62, 1, 60, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (63, 1, 21, 'Liferay Sao Paulo', 'location', 'F', 0, 48, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (64, 1, 5, 10, 63, 0, 0, '64', '/64', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (65, 1, 64, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (66, 1, 64, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (67, 1, 21, 'Liferay Frankfurt', 'location', 'F', 0, 4, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (68, 1, 5, 10, 67, 0, 0, '68', '/68', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (69, 1, 68, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (70, 1, 68, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (71, 1, 21, 'Liferay Madrid', 'location', 'F', 0, 15, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (72, 1, 5, 10, 71, 0, 0, '72', '/72', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (73, 1, 72, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (74, 1, 72, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (75, 1, 21, 'Liferay Dalian', 'location', 'F', 0, 2, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (76, 1, 5, 10, 75, 0, 0, '76', '/76', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (77, 1, 76, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (78, 1, 76, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (79, 1, 21, 'Liferay Hong Kong', 'location', 'F', 0, 2, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (80, 1, 5, 10, 79, 0, 0, '80', '/80', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (81, 1, 80, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (82, 1, 80, 'F', 'F', 'classic', '01', 0);

insert into Organization_ (organizationId, companyId, parentOrganizationId, name, type_, recursable, regionId, countryId, statusId, comments) values (83, 1, 21, 'Liferay Kuala Lumpur', 'location', 'F', 0, 135, 12017, '');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (84, 1, 5, 10, 83, 0, 0, '84', '/84', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (85, 1, 84, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (86, 1, 84, 'F', 'F', 'classic', '01', 0);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (5, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'T', 6, 'password', 'F', 'F', '5', 'default@liferay.com', 'Welcome!', '', '', '', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (6, 1, 5, '', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, '', '', '', 'T', '1970-01-01 00:00:00.0');

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (2, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 3, 'test', 'F', 'F', 'joebloggs', 'test@liferay.com', 'Welcome Joe Bloggs!', 'Joe', '', 'Bloggs', CURRENT YEAR TO FRACTION, 0, 'F', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (3, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Joe', '', 'Bloggs', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (87, 1, 2, 12, 2, 0, 0, '87', '/joebloggs', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (88, 1, 87, 'T', 'F', 'classic', '01', 2);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (89, 1, 87, 'F', 'F', 'classic', '01', 0);
insert into Layout (plid, groupId, companyId, privateLayout, layoutId, parentLayoutId, name, type_, typeSettings, hidden_, friendlyURL, priority) values (90, 87, 1, 'T', 1, 0, '<?xml version="1.0"?>\n\n<root>\n  <name>Home</name>\n</root>', 'portlet', 'column-1=71_INSTANCE_OY0d,82,23,61,\ncolumn-2=9,79,29,8,19,\nlayout-template-id=2_columns_ii\n', 'F', '/home', 0);
insert into Layout (plid, groupId, companyId, privateLayout, layoutId, parentLayoutId, name, type_, typeSettings, hidden_, friendlyURL, priority) values (91, 87, 1, 'T', 2, 0, '<?xml version="1.0"?>\n\n<root>\n  <name>Plugins</name>\n</root>', 'portlet', 'column-1=\ncolumn-2=111,\nlayout-template-id=2_columns_ii\n', 'F', '/plugins', 1);

insert into Users_Groups values (2, 18);

insert into Users_Orgs (userId, organizationId) values (2, 21);
insert into Users_Orgs (userId, organizationId) values (2, 47);

insert into Users_Roles values (2, 14);
insert into Users_Roles values (2, 16);
insert into Users_Roles values (2, 17);


insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (92, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 93, 'test', 'F', 'F', 'lax1', 'test.lax.1@liferay.com', 'Welcome Test LAX 1!', 'Test', '', 'LAX 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (93, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (94, 1, 92, 12, 92, 0, 0, '94', '/lax1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (95, 1, 94, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (96, 1, 94, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (92, 18);

insert into Users_Orgs (userId, organizationId) values (92, 21);
insert into Users_Orgs (userId, organizationId) values (92, 47);

insert into Users_Roles values (92, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (97, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 98, 'test', 'F', 'F', 'lax2', 'test.lax.2@liferay.com', 'Welcome Test LAX 2!', 'Test', '', 'LAX 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (98, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (99, 1, 97, 12, 97, 0, 0, '99', '/lax2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (100, 1, 99, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (101, 1, 99, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (97, 18);

insert into Users_Orgs (userId, organizationId) values (97, 21);
insert into Users_Orgs (userId, organizationId) values (97, 47);

insert into Users_Roles values (97, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (102, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 103, 'test', 'F', 'F', 'lax3', 'test.lax.3@liferay.com', 'Welcome Test LAX 3!', 'Test', '', 'LAX 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (103, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (104, 1, 102, 12, 102, 0, 0, '104', '/lax3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (105, 1, 104, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (106, 1, 104, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (102, 18);

insert into Users_Orgs (userId, organizationId) values (102, 21);
insert into Users_Orgs (userId, organizationId) values (102, 47);

insert into Users_Roles values (102, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (107, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 108, 'test', 'F', 'F', 'lax4', 'test.lax.4@liferay.com', 'Welcome Test LAX 4!', 'Test', '', 'LAX 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (108, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (109, 1, 107, 12, 107, 0, 0, '109', '/lax4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (110, 1, 109, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (111, 1, 109, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (107, 18);

insert into Users_Orgs (userId, organizationId) values (107, 21);
insert into Users_Orgs (userId, organizationId) values (107, 47);

insert into Users_Roles values (107, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (112, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 113, 'test', 'F', 'F', 'lax5', 'test.lax.5@liferay.com', 'Welcome Test LAX 5!', 'Test', '', 'LAX 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (113, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (114, 1, 112, 12, 112, 0, 0, '114', '/lax5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (115, 1, 114, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (116, 1, 114, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (112, 18);

insert into Users_Orgs (userId, organizationId) values (112, 21);
insert into Users_Orgs (userId, organizationId) values (112, 47);

insert into Users_Roles values (112, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (117, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 118, 'test', 'F', 'F', 'lax6', 'test.lax.6@liferay.com', 'Welcome Test LAX 6!', 'Test', '', 'LAX 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (118, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (119, 1, 117, 12, 117, 0, 0, '119', '/lax6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (120, 1, 119, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (121, 1, 119, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (117, 18);

insert into Users_Orgs (userId, organizationId) values (117, 21);
insert into Users_Orgs (userId, organizationId) values (117, 47);

insert into Users_Roles values (117, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (122, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 123, 'test', 'F', 'F', 'lax7', 'test.lax.7@liferay.com', 'Welcome Test LAX 7!', 'Test', '', 'LAX 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (123, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (124, 1, 122, 12, 122, 0, 0, '124', '/lax7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (125, 1, 124, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (126, 1, 124, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (122, 18);

insert into Users_Orgs (userId, organizationId) values (122, 21);
insert into Users_Orgs (userId, organizationId) values (122, 47);

insert into Users_Roles values (122, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (127, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 128, 'test', 'F', 'F', 'lax8', 'test.lax.8@liferay.com', 'Welcome Test LAX 8!', 'Test', '', 'LAX 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (128, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (129, 1, 127, 12, 127, 0, 0, '129', '/lax8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (130, 1, 129, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (131, 1, 129, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (127, 18);

insert into Users_Orgs (userId, organizationId) values (127, 21);
insert into Users_Orgs (userId, organizationId) values (127, 47);

insert into Users_Roles values (127, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (132, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 133, 'test', 'F', 'F', 'lax9', 'test.lax.9@liferay.com', 'Welcome Test LAX 9!', 'Test', '', 'LAX 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (133, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (134, 1, 132, 12, 132, 0, 0, '134', '/lax9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (135, 1, 134, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (136, 1, 134, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (132, 18);

insert into Users_Orgs (userId, organizationId) values (132, 21);
insert into Users_Orgs (userId, organizationId) values (132, 47);

insert into Users_Roles values (132, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (137, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 138, 'test', 'F', 'F', 'lax10', 'test.lax.10@liferay.com', 'Welcome Test LAX 10!', 'Test', '', 'LAX 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (138, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (139, 1, 137, 12, 137, 0, 0, '139', '/lax10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (140, 1, 139, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (141, 1, 139, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (137, 18);

insert into Users_Orgs (userId, organizationId) values (137, 21);
insert into Users_Orgs (userId, organizationId) values (137, 47);

insert into Users_Roles values (137, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (142, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 143, 'test', 'F', 'F', 'lax11', 'test.lax.11@liferay.com', 'Welcome Test LAX 11!', 'Test', '', 'LAX 11', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (143, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 11', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (144, 1, 142, 12, 142, 0, 0, '144', '/lax11', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (145, 1, 144, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (146, 1, 144, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (142, 18);

insert into Users_Orgs (userId, organizationId) values (142, 21);
insert into Users_Orgs (userId, organizationId) values (142, 47);

insert into Users_Roles values (142, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (147, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 148, 'test', 'F', 'F', 'lax12', 'test.lax.12@liferay.com', 'Welcome Test LAX 12!', 'Test', '', 'LAX 12', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (148, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 12', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (149, 1, 147, 12, 147, 0, 0, '149', '/lax12', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (150, 1, 149, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (151, 1, 149, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (147, 18);

insert into Users_Orgs (userId, organizationId) values (147, 21);
insert into Users_Orgs (userId, organizationId) values (147, 47);

insert into Users_Roles values (147, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (152, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 153, 'test', 'F', 'F', 'lax13', 'test.lax.13@liferay.com', 'Welcome Test LAX 13!', 'Test', '', 'LAX 13', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (153, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 13', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (154, 1, 152, 12, 152, 0, 0, '154', '/lax13', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (155, 1, 154, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (156, 1, 154, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (152, 18);

insert into Users_Orgs (userId, organizationId) values (152, 21);
insert into Users_Orgs (userId, organizationId) values (152, 47);

insert into Users_Roles values (152, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (157, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 158, 'test', 'F', 'F', 'lax14', 'test.lax.14@liferay.com', 'Welcome Test LAX 14!', 'Test', '', 'LAX 14', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (158, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 14', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (159, 1, 157, 12, 157, 0, 0, '159', '/lax14', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (160, 1, 159, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (161, 1, 159, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (157, 18);

insert into Users_Orgs (userId, organizationId) values (157, 21);
insert into Users_Orgs (userId, organizationId) values (157, 47);

insert into Users_Roles values (157, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (162, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 163, 'test', 'F', 'F', 'lax15', 'test.lax.15@liferay.com', 'Welcome Test LAX 15!', 'Test', '', 'LAX 15', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (163, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 15', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (164, 1, 162, 12, 162, 0, 0, '164', '/lax15', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (165, 1, 164, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (166, 1, 164, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (162, 18);

insert into Users_Orgs (userId, organizationId) values (162, 21);
insert into Users_Orgs (userId, organizationId) values (162, 47);

insert into Users_Roles values (162, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (167, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 168, 'test', 'F', 'F', 'lax16', 'test.lax.16@liferay.com', 'Welcome Test LAX 16!', 'Test', '', 'LAX 16', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (168, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 16', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (169, 1, 167, 12, 167, 0, 0, '169', '/lax16', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (170, 1, 169, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (171, 1, 169, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (167, 18);

insert into Users_Orgs (userId, organizationId) values (167, 21);
insert into Users_Orgs (userId, organizationId) values (167, 47);

insert into Users_Roles values (167, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (172, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 173, 'test', 'F', 'F', 'lax17', 'test.lax.17@liferay.com', 'Welcome Test LAX 17!', 'Test', '', 'LAX 17', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (173, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 17', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (174, 1, 172, 12, 172, 0, 0, '174', '/lax17', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (175, 1, 174, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (176, 1, 174, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (172, 18);

insert into Users_Orgs (userId, organizationId) values (172, 21);
insert into Users_Orgs (userId, organizationId) values (172, 47);

insert into Users_Roles values (172, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (177, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 178, 'test', 'F', 'F', 'lax18', 'test.lax.18@liferay.com', 'Welcome Test LAX 18!', 'Test', '', 'LAX 18', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (178, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 18', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (179, 1, 177, 12, 177, 0, 0, '179', '/lax18', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (180, 1, 179, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (181, 1, 179, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (177, 18);

insert into Users_Orgs (userId, organizationId) values (177, 21);
insert into Users_Orgs (userId, organizationId) values (177, 47);

insert into Users_Roles values (177, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (182, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 183, 'test', 'F', 'F', 'lax19', 'test.lax.19@liferay.com', 'Welcome Test LAX 19!', 'Test', '', 'LAX 19', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (183, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 19', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (184, 1, 182, 12, 182, 0, 0, '184', '/lax19', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (185, 1, 184, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (186, 1, 184, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (182, 18);

insert into Users_Orgs (userId, organizationId) values (182, 21);
insert into Users_Orgs (userId, organizationId) values (182, 47);

insert into Users_Roles values (182, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (187, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 188, 'test', 'F', 'F', 'lax20', 'test.lax.20@liferay.com', 'Welcome Test LAX 20!', 'Test', '', 'LAX 20', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (188, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 20', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (189, 1, 187, 12, 187, 0, 0, '189', '/lax20', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (190, 1, 189, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (191, 1, 189, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (187, 18);

insert into Users_Orgs (userId, organizationId) values (187, 21);
insert into Users_Orgs (userId, organizationId) values (187, 47);

insert into Users_Roles values (187, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (192, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 193, 'test', 'F', 'F', 'lax21', 'test.lax.21@liferay.com', 'Welcome Test LAX 21!', 'Test', '', 'LAX 21', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (193, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 21', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (194, 1, 192, 12, 192, 0, 0, '194', '/lax21', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (195, 1, 194, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (196, 1, 194, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (192, 18);

insert into Users_Orgs (userId, organizationId) values (192, 21);
insert into Users_Orgs (userId, organizationId) values (192, 47);

insert into Users_Roles values (192, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (197, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 198, 'test', 'F', 'F', 'lax22', 'test.lax.22@liferay.com', 'Welcome Test LAX 22!', 'Test', '', 'LAX 22', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (198, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 22', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (199, 1, 197, 12, 197, 0, 0, '199', '/lax22', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (200, 1, 199, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (201, 1, 199, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (197, 18);

insert into Users_Orgs (userId, organizationId) values (197, 21);
insert into Users_Orgs (userId, organizationId) values (197, 47);

insert into Users_Roles values (197, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (202, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 203, 'test', 'F', 'F', 'lax23', 'test.lax.23@liferay.com', 'Welcome Test LAX 23!', 'Test', '', 'LAX 23', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (203, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 23', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (204, 1, 202, 12, 202, 0, 0, '204', '/lax23', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (205, 1, 204, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (206, 1, 204, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (202, 18);

insert into Users_Orgs (userId, organizationId) values (202, 21);
insert into Users_Orgs (userId, organizationId) values (202, 47);

insert into Users_Roles values (202, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (207, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 208, 'test', 'F', 'F', 'lax24', 'test.lax.24@liferay.com', 'Welcome Test LAX 24!', 'Test', '', 'LAX 24', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (208, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 24', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (209, 1, 207, 12, 207, 0, 0, '209', '/lax24', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (210, 1, 209, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (211, 1, 209, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (207, 18);

insert into Users_Orgs (userId, organizationId) values (207, 21);
insert into Users_Orgs (userId, organizationId) values (207, 47);

insert into Users_Roles values (207, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (212, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 213, 'test', 'F', 'F', 'lax25', 'test.lax.25@liferay.com', 'Welcome Test LAX 25!', 'Test', '', 'LAX 25', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (213, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 25', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (214, 1, 212, 12, 212, 0, 0, '214', '/lax25', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (215, 1, 214, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (216, 1, 214, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (212, 18);

insert into Users_Orgs (userId, organizationId) values (212, 21);
insert into Users_Orgs (userId, organizationId) values (212, 47);

insert into Users_Roles values (212, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (217, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 218, 'test', 'F', 'F', 'lax26', 'test.lax.26@liferay.com', 'Welcome Test LAX 26!', 'Test', '', 'LAX 26', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (218, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 26', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (219, 1, 217, 12, 217, 0, 0, '219', '/lax26', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (220, 1, 219, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (221, 1, 219, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (217, 18);

insert into Users_Orgs (userId, organizationId) values (217, 21);
insert into Users_Orgs (userId, organizationId) values (217, 47);

insert into Users_Roles values (217, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (222, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 223, 'test', 'F', 'F', 'lax27', 'test.lax.27@liferay.com', 'Welcome Test LAX 27!', 'Test', '', 'LAX 27', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (223, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 27', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (224, 1, 222, 12, 222, 0, 0, '224', '/lax27', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (225, 1, 224, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (226, 1, 224, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (222, 18);

insert into Users_Orgs (userId, organizationId) values (222, 21);
insert into Users_Orgs (userId, organizationId) values (222, 47);

insert into Users_Roles values (222, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (227, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 228, 'test', 'F', 'F', 'lax28', 'test.lax.28@liferay.com', 'Welcome Test LAX 28!', 'Test', '', 'LAX 28', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (228, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 28', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (229, 1, 227, 12, 227, 0, 0, '229', '/lax28', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (230, 1, 229, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (231, 1, 229, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (227, 18);

insert into Users_Orgs (userId, organizationId) values (227, 21);
insert into Users_Orgs (userId, organizationId) values (227, 47);

insert into Users_Roles values (227, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (232, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 233, 'test', 'F', 'F', 'lax29', 'test.lax.29@liferay.com', 'Welcome Test LAX 29!', 'Test', '', 'LAX 29', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (233, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 29', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (234, 1, 232, 12, 232, 0, 0, '234', '/lax29', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (235, 1, 234, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (236, 1, 234, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (232, 18);

insert into Users_Orgs (userId, organizationId) values (232, 21);
insert into Users_Orgs (userId, organizationId) values (232, 47);

insert into Users_Roles values (232, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (237, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 238, 'test', 'F', 'F', 'lax30', 'test.lax.30@liferay.com', 'Welcome Test LAX 30!', 'Test', '', 'LAX 30', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (238, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 30', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (239, 1, 237, 12, 237, 0, 0, '239', '/lax30', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (240, 1, 239, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (241, 1, 239, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (237, 18);

insert into Users_Orgs (userId, organizationId) values (237, 21);
insert into Users_Orgs (userId, organizationId) values (237, 47);

insert into Users_Roles values (237, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (242, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 243, 'test', 'F', 'F', 'lax31', 'test.lax.31@liferay.com', 'Welcome Test LAX 31!', 'Test', '', 'LAX 31', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (243, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 31', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (244, 1, 242, 12, 242, 0, 0, '244', '/lax31', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (245, 1, 244, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (246, 1, 244, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (242, 18);

insert into Users_Orgs (userId, organizationId) values (242, 21);
insert into Users_Orgs (userId, organizationId) values (242, 47);

insert into Users_Roles values (242, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (247, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 248, 'test', 'F', 'F', 'lax32', 'test.lax.32@liferay.com', 'Welcome Test LAX 32!', 'Test', '', 'LAX 32', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (248, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 32', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (249, 1, 247, 12, 247, 0, 0, '249', '/lax32', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (250, 1, 249, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (251, 1, 249, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (247, 18);

insert into Users_Orgs (userId, organizationId) values (247, 21);
insert into Users_Orgs (userId, organizationId) values (247, 47);

insert into Users_Roles values (247, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (252, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 253, 'test', 'F', 'F', 'lax33', 'test.lax.33@liferay.com', 'Welcome Test LAX 33!', 'Test', '', 'LAX 33', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (253, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 33', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (254, 1, 252, 12, 252, 0, 0, '254', '/lax33', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (255, 1, 254, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (256, 1, 254, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (252, 18);

insert into Users_Orgs (userId, organizationId) values (252, 21);
insert into Users_Orgs (userId, organizationId) values (252, 47);

insert into Users_Roles values (252, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (257, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 258, 'test', 'F', 'F', 'lax34', 'test.lax.34@liferay.com', 'Welcome Test LAX 34!', 'Test', '', 'LAX 34', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (258, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 34', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (259, 1, 257, 12, 257, 0, 0, '259', '/lax34', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (260, 1, 259, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (261, 1, 259, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (257, 18);

insert into Users_Orgs (userId, organizationId) values (257, 21);
insert into Users_Orgs (userId, organizationId) values (257, 47);

insert into Users_Roles values (257, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (262, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 263, 'test', 'F', 'F', 'lax35', 'test.lax.35@liferay.com', 'Welcome Test LAX 35!', 'Test', '', 'LAX 35', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (263, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 35', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (264, 1, 262, 12, 262, 0, 0, '264', '/lax35', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (265, 1, 264, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (266, 1, 264, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (262, 18);

insert into Users_Orgs (userId, organizationId) values (262, 21);
insert into Users_Orgs (userId, organizationId) values (262, 47);

insert into Users_Roles values (262, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (267, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 268, 'test', 'F', 'F', 'lax36', 'test.lax.36@liferay.com', 'Welcome Test LAX 36!', 'Test', '', 'LAX 36', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (268, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 36', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (269, 1, 267, 12, 267, 0, 0, '269', '/lax36', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (270, 1, 269, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (271, 1, 269, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (267, 18);

insert into Users_Orgs (userId, organizationId) values (267, 21);
insert into Users_Orgs (userId, organizationId) values (267, 47);

insert into Users_Roles values (267, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (272, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 273, 'test', 'F', 'F', 'lax37', 'test.lax.37@liferay.com', 'Welcome Test LAX 37!', 'Test', '', 'LAX 37', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (273, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 37', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (274, 1, 272, 12, 272, 0, 0, '274', '/lax37', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (275, 1, 274, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (276, 1, 274, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (272, 18);

insert into Users_Orgs (userId, organizationId) values (272, 21);
insert into Users_Orgs (userId, organizationId) values (272, 47);

insert into Users_Roles values (272, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (277, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 278, 'test', 'F', 'F', 'lax38', 'test.lax.38@liferay.com', 'Welcome Test LAX 38!', 'Test', '', 'LAX 38', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (278, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 38', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (279, 1, 277, 12, 277, 0, 0, '279', '/lax38', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (280, 1, 279, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (281, 1, 279, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (277, 18);

insert into Users_Orgs (userId, organizationId) values (277, 21);
insert into Users_Orgs (userId, organizationId) values (277, 47);

insert into Users_Roles values (277, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (282, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 283, 'test', 'F', 'F', 'lax39', 'test.lax.39@liferay.com', 'Welcome Test LAX 39!', 'Test', '', 'LAX 39', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (283, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 39', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (284, 1, 282, 12, 282, 0, 0, '284', '/lax39', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (285, 1, 284, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (286, 1, 284, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (282, 18);

insert into Users_Orgs (userId, organizationId) values (282, 21);
insert into Users_Orgs (userId, organizationId) values (282, 47);

insert into Users_Roles values (282, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (287, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 288, 'test', 'F', 'F', 'lax40', 'test.lax.40@liferay.com', 'Welcome Test LAX 40!', 'Test', '', 'LAX 40', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (288, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 40', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (289, 1, 287, 12, 287, 0, 0, '289', '/lax40', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (290, 1, 289, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (291, 1, 289, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (287, 18);

insert into Users_Orgs (userId, organizationId) values (287, 21);
insert into Users_Orgs (userId, organizationId) values (287, 47);

insert into Users_Roles values (287, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (292, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 293, 'test', 'F', 'F', 'lax41', 'test.lax.41@liferay.com', 'Welcome Test LAX 41!', 'Test', '', 'LAX 41', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (293, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 41', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (294, 1, 292, 12, 292, 0, 0, '294', '/lax41', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (295, 1, 294, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (296, 1, 294, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (292, 18);

insert into Users_Orgs (userId, organizationId) values (292, 21);
insert into Users_Orgs (userId, organizationId) values (292, 47);

insert into Users_Roles values (292, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (297, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 298, 'test', 'F', 'F', 'lax42', 'test.lax.42@liferay.com', 'Welcome Test LAX 42!', 'Test', '', 'LAX 42', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (298, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 42', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (299, 1, 297, 12, 297, 0, 0, '299', '/lax42', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (300, 1, 299, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (301, 1, 299, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (297, 18);

insert into Users_Orgs (userId, organizationId) values (297, 21);
insert into Users_Orgs (userId, organizationId) values (297, 47);

insert into Users_Roles values (297, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (302, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 303, 'test', 'F', 'F', 'lax43', 'test.lax.43@liferay.com', 'Welcome Test LAX 43!', 'Test', '', 'LAX 43', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (303, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 43', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (304, 1, 302, 12, 302, 0, 0, '304', '/lax43', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (305, 1, 304, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (306, 1, 304, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (302, 18);

insert into Users_Orgs (userId, organizationId) values (302, 21);
insert into Users_Orgs (userId, organizationId) values (302, 47);

insert into Users_Roles values (302, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (307, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 308, 'test', 'F', 'F', 'lax44', 'test.lax.44@liferay.com', 'Welcome Test LAX 44!', 'Test', '', 'LAX 44', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (308, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 44', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (309, 1, 307, 12, 307, 0, 0, '309', '/lax44', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (310, 1, 309, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (311, 1, 309, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (307, 18);

insert into Users_Orgs (userId, organizationId) values (307, 21);
insert into Users_Orgs (userId, organizationId) values (307, 47);

insert into Users_Roles values (307, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (312, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 313, 'test', 'F', 'F', 'lax45', 'test.lax.45@liferay.com', 'Welcome Test LAX 45!', 'Test', '', 'LAX 45', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (313, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 45', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (314, 1, 312, 12, 312, 0, 0, '314', '/lax45', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (315, 1, 314, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (316, 1, 314, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (312, 18);

insert into Users_Orgs (userId, organizationId) values (312, 21);
insert into Users_Orgs (userId, organizationId) values (312, 47);

insert into Users_Roles values (312, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (317, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 318, 'test', 'F', 'F', 'lax46', 'test.lax.46@liferay.com', 'Welcome Test LAX 46!', 'Test', '', 'LAX 46', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (318, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 46', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (319, 1, 317, 12, 317, 0, 0, '319', '/lax46', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (320, 1, 319, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (321, 1, 319, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (317, 18);

insert into Users_Orgs (userId, organizationId) values (317, 21);
insert into Users_Orgs (userId, organizationId) values (317, 47);

insert into Users_Roles values (317, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (322, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 323, 'test', 'F', 'F', 'lax47', 'test.lax.47@liferay.com', 'Welcome Test LAX 47!', 'Test', '', 'LAX 47', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (323, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 47', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (324, 1, 322, 12, 322, 0, 0, '324', '/lax47', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (325, 1, 324, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (326, 1, 324, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (322, 18);

insert into Users_Orgs (userId, organizationId) values (322, 21);
insert into Users_Orgs (userId, organizationId) values (322, 47);

insert into Users_Roles values (322, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (327, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 328, 'test', 'F', 'F', 'lax48', 'test.lax.48@liferay.com', 'Welcome Test LAX 48!', 'Test', '', 'LAX 48', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (328, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 48', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (329, 1, 327, 12, 327, 0, 0, '329', '/lax48', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (330, 1, 329, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (331, 1, 329, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (327, 18);

insert into Users_Orgs (userId, organizationId) values (327, 21);
insert into Users_Orgs (userId, organizationId) values (327, 47);

insert into Users_Roles values (327, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (332, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 333, 'test', 'F', 'F', 'lax49', 'test.lax.49@liferay.com', 'Welcome Test LAX 49!', 'Test', '', 'LAX 49', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (333, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 49', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (334, 1, 332, 12, 332, 0, 0, '334', '/lax49', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (335, 1, 334, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (336, 1, 334, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (332, 18);

insert into Users_Orgs (userId, organizationId) values (332, 21);
insert into Users_Orgs (userId, organizationId) values (332, 47);

insert into Users_Roles values (332, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (337, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 338, 'test', 'F', 'F', 'lax50', 'test.lax.50@liferay.com', 'Welcome Test LAX 50!', 'Test', '', 'LAX 50', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (338, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 50', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (339, 1, 337, 12, 337, 0, 0, '339', '/lax50', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (340, 1, 339, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (341, 1, 339, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (337, 18);

insert into Users_Orgs (userId, organizationId) values (337, 21);
insert into Users_Orgs (userId, organizationId) values (337, 47);

insert into Users_Roles values (337, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (342, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 343, 'test', 'F', 'F', 'lax51', 'test.lax.51@liferay.com', 'Welcome Test LAX 51!', 'Test', '', 'LAX 51', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (343, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 51', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (344, 1, 342, 12, 342, 0, 0, '344', '/lax51', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (345, 1, 344, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (346, 1, 344, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (342, 18);

insert into Users_Orgs (userId, organizationId) values (342, 21);
insert into Users_Orgs (userId, organizationId) values (342, 47);

insert into Users_Roles values (342, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (347, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 348, 'test', 'F', 'F', 'lax52', 'test.lax.52@liferay.com', 'Welcome Test LAX 52!', 'Test', '', 'LAX 52', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (348, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 52', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (349, 1, 347, 12, 347, 0, 0, '349', '/lax52', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (350, 1, 349, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (351, 1, 349, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (347, 18);

insert into Users_Orgs (userId, organizationId) values (347, 21);
insert into Users_Orgs (userId, organizationId) values (347, 47);

insert into Users_Roles values (347, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (352, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 353, 'test', 'F', 'F', 'lax53', 'test.lax.53@liferay.com', 'Welcome Test LAX 53!', 'Test', '', 'LAX 53', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (353, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 53', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (354, 1, 352, 12, 352, 0, 0, '354', '/lax53', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (355, 1, 354, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (356, 1, 354, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (352, 18);

insert into Users_Orgs (userId, organizationId) values (352, 21);
insert into Users_Orgs (userId, organizationId) values (352, 47);

insert into Users_Roles values (352, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (357, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 358, 'test', 'F', 'F', 'lax54', 'test.lax.54@liferay.com', 'Welcome Test LAX 54!', 'Test', '', 'LAX 54', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (358, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 54', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (359, 1, 357, 12, 357, 0, 0, '359', '/lax54', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (360, 1, 359, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (361, 1, 359, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (357, 18);

insert into Users_Orgs (userId, organizationId) values (357, 21);
insert into Users_Orgs (userId, organizationId) values (357, 47);

insert into Users_Roles values (357, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (362, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 363, 'test', 'F', 'F', 'lax55', 'test.lax.55@liferay.com', 'Welcome Test LAX 55!', 'Test', '', 'LAX 55', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (363, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 55', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (364, 1, 362, 12, 362, 0, 0, '364', '/lax55', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (365, 1, 364, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (366, 1, 364, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (362, 18);

insert into Users_Orgs (userId, organizationId) values (362, 21);
insert into Users_Orgs (userId, organizationId) values (362, 47);

insert into Users_Roles values (362, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (367, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 368, 'test', 'F', 'F', 'lax56', 'test.lax.56@liferay.com', 'Welcome Test LAX 56!', 'Test', '', 'LAX 56', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (368, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 56', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (369, 1, 367, 12, 367, 0, 0, '369', '/lax56', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (370, 1, 369, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (371, 1, 369, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (367, 18);

insert into Users_Orgs (userId, organizationId) values (367, 21);
insert into Users_Orgs (userId, organizationId) values (367, 47);

insert into Users_Roles values (367, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (372, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 373, 'test', 'F', 'F', 'lax57', 'test.lax.57@liferay.com', 'Welcome Test LAX 57!', 'Test', '', 'LAX 57', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (373, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 57', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (374, 1, 372, 12, 372, 0, 0, '374', '/lax57', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (375, 1, 374, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (376, 1, 374, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (372, 18);

insert into Users_Orgs (userId, organizationId) values (372, 21);
insert into Users_Orgs (userId, organizationId) values (372, 47);

insert into Users_Roles values (372, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (377, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 378, 'test', 'F', 'F', 'lax58', 'test.lax.58@liferay.com', 'Welcome Test LAX 58!', 'Test', '', 'LAX 58', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (378, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 58', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (379, 1, 377, 12, 377, 0, 0, '379', '/lax58', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (380, 1, 379, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (381, 1, 379, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (377, 18);

insert into Users_Orgs (userId, organizationId) values (377, 21);
insert into Users_Orgs (userId, organizationId) values (377, 47);

insert into Users_Roles values (377, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (382, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 383, 'test', 'F', 'F', 'lax59', 'test.lax.59@liferay.com', 'Welcome Test LAX 59!', 'Test', '', 'LAX 59', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (383, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 59', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (384, 1, 382, 12, 382, 0, 0, '384', '/lax59', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (385, 1, 384, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (386, 1, 384, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (382, 18);

insert into Users_Orgs (userId, organizationId) values (382, 21);
insert into Users_Orgs (userId, organizationId) values (382, 47);

insert into Users_Roles values (382, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (387, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 388, 'test', 'F', 'F', 'lax60', 'test.lax.60@liferay.com', 'Welcome Test LAX 60!', 'Test', '', 'LAX 60', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (388, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 60', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (389, 1, 387, 12, 387, 0, 0, '389', '/lax60', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (390, 1, 389, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (391, 1, 389, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (387, 18);

insert into Users_Orgs (userId, organizationId) values (387, 21);
insert into Users_Orgs (userId, organizationId) values (387, 47);

insert into Users_Roles values (387, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (392, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 393, 'test', 'F', 'F', 'lax61', 'test.lax.61@liferay.com', 'Welcome Test LAX 61!', 'Test', '', 'LAX 61', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (393, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 61', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (394, 1, 392, 12, 392, 0, 0, '394', '/lax61', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (395, 1, 394, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (396, 1, 394, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (392, 18);

insert into Users_Orgs (userId, organizationId) values (392, 21);
insert into Users_Orgs (userId, organizationId) values (392, 47);

insert into Users_Roles values (392, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (397, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 398, 'test', 'F', 'F', 'lax62', 'test.lax.62@liferay.com', 'Welcome Test LAX 62!', 'Test', '', 'LAX 62', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (398, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 62', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (399, 1, 397, 12, 397, 0, 0, '399', '/lax62', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (400, 1, 399, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (401, 1, 399, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (397, 18);

insert into Users_Orgs (userId, organizationId) values (397, 21);
insert into Users_Orgs (userId, organizationId) values (397, 47);

insert into Users_Roles values (397, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (402, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 403, 'test', 'F', 'F', 'lax63', 'test.lax.63@liferay.com', 'Welcome Test LAX 63!', 'Test', '', 'LAX 63', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (403, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 63', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (404, 1, 402, 12, 402, 0, 0, '404', '/lax63', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (405, 1, 404, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (406, 1, 404, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (402, 18);

insert into Users_Orgs (userId, organizationId) values (402, 21);
insert into Users_Orgs (userId, organizationId) values (402, 47);

insert into Users_Roles values (402, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (407, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 408, 'test', 'F', 'F', 'lax64', 'test.lax.64@liferay.com', 'Welcome Test LAX 64!', 'Test', '', 'LAX 64', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (408, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 64', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (409, 1, 407, 12, 407, 0, 0, '409', '/lax64', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (410, 1, 409, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (411, 1, 409, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (407, 18);

insert into Users_Orgs (userId, organizationId) values (407, 21);
insert into Users_Orgs (userId, organizationId) values (407, 47);

insert into Users_Roles values (407, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (412, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 413, 'test', 'F', 'F', 'lax65', 'test.lax.65@liferay.com', 'Welcome Test LAX 65!', 'Test', '', 'LAX 65', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (413, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 65', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (414, 1, 412, 12, 412, 0, 0, '414', '/lax65', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (415, 1, 414, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (416, 1, 414, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (412, 18);

insert into Users_Orgs (userId, organizationId) values (412, 21);
insert into Users_Orgs (userId, organizationId) values (412, 47);

insert into Users_Roles values (412, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (417, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 418, 'test', 'F', 'F', 'lax66', 'test.lax.66@liferay.com', 'Welcome Test LAX 66!', 'Test', '', 'LAX 66', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (418, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 66', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (419, 1, 417, 12, 417, 0, 0, '419', '/lax66', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (420, 1, 419, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (421, 1, 419, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (417, 18);

insert into Users_Orgs (userId, organizationId) values (417, 21);
insert into Users_Orgs (userId, organizationId) values (417, 47);

insert into Users_Roles values (417, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (422, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 423, 'test', 'F', 'F', 'lax67', 'test.lax.67@liferay.com', 'Welcome Test LAX 67!', 'Test', '', 'LAX 67', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (423, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 67', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (424, 1, 422, 12, 422, 0, 0, '424', '/lax67', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (425, 1, 424, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (426, 1, 424, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (422, 18);

insert into Users_Orgs (userId, organizationId) values (422, 21);
insert into Users_Orgs (userId, organizationId) values (422, 47);

insert into Users_Roles values (422, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (427, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 428, 'test', 'F', 'F', 'lax68', 'test.lax.68@liferay.com', 'Welcome Test LAX 68!', 'Test', '', 'LAX 68', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (428, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 68', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (429, 1, 427, 12, 427, 0, 0, '429', '/lax68', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (430, 1, 429, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (431, 1, 429, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (427, 18);

insert into Users_Orgs (userId, organizationId) values (427, 21);
insert into Users_Orgs (userId, organizationId) values (427, 47);

insert into Users_Roles values (427, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (432, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 433, 'test', 'F', 'F', 'lax69', 'test.lax.69@liferay.com', 'Welcome Test LAX 69!', 'Test', '', 'LAX 69', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (433, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 69', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (434, 1, 432, 12, 432, 0, 0, '434', '/lax69', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (435, 1, 434, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (436, 1, 434, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (432, 18);

insert into Users_Orgs (userId, organizationId) values (432, 21);
insert into Users_Orgs (userId, organizationId) values (432, 47);

insert into Users_Roles values (432, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (437, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 438, 'test', 'F', 'F', 'lax70', 'test.lax.70@liferay.com', 'Welcome Test LAX 70!', 'Test', '', 'LAX 70', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (438, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 70', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (439, 1, 437, 12, 437, 0, 0, '439', '/lax70', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (440, 1, 439, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (441, 1, 439, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (437, 18);

insert into Users_Orgs (userId, organizationId) values (437, 21);
insert into Users_Orgs (userId, organizationId) values (437, 47);

insert into Users_Roles values (437, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (442, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 443, 'test', 'F', 'F', 'lax71', 'test.lax.71@liferay.com', 'Welcome Test LAX 71!', 'Test', '', 'LAX 71', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (443, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 71', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (444, 1, 442, 12, 442, 0, 0, '444', '/lax71', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (445, 1, 444, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (446, 1, 444, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (442, 18);

insert into Users_Orgs (userId, organizationId) values (442, 21);
insert into Users_Orgs (userId, organizationId) values (442, 47);

insert into Users_Roles values (442, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (447, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 448, 'test', 'F', 'F', 'lax72', 'test.lax.72@liferay.com', 'Welcome Test LAX 72!', 'Test', '', 'LAX 72', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (448, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 72', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (449, 1, 447, 12, 447, 0, 0, '449', '/lax72', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (450, 1, 449, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (451, 1, 449, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (447, 18);

insert into Users_Orgs (userId, organizationId) values (447, 21);
insert into Users_Orgs (userId, organizationId) values (447, 47);

insert into Users_Roles values (447, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (452, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 453, 'test', 'F', 'F', 'lax73', 'test.lax.73@liferay.com', 'Welcome Test LAX 73!', 'Test', '', 'LAX 73', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (453, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 73', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (454, 1, 452, 12, 452, 0, 0, '454', '/lax73', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (455, 1, 454, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (456, 1, 454, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (452, 18);

insert into Users_Orgs (userId, organizationId) values (452, 21);
insert into Users_Orgs (userId, organizationId) values (452, 47);

insert into Users_Roles values (452, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (457, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 458, 'test', 'F', 'F', 'lax74', 'test.lax.74@liferay.com', 'Welcome Test LAX 74!', 'Test', '', 'LAX 74', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (458, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 74', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (459, 1, 457, 12, 457, 0, 0, '459', '/lax74', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (460, 1, 459, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (461, 1, 459, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (457, 18);

insert into Users_Orgs (userId, organizationId) values (457, 21);
insert into Users_Orgs (userId, organizationId) values (457, 47);

insert into Users_Roles values (457, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (462, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 463, 'test', 'F', 'F', 'lax75', 'test.lax.75@liferay.com', 'Welcome Test LAX 75!', 'Test', '', 'LAX 75', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (463, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 75', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (464, 1, 462, 12, 462, 0, 0, '464', '/lax75', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (465, 1, 464, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (466, 1, 464, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (462, 18);

insert into Users_Orgs (userId, organizationId) values (462, 21);
insert into Users_Orgs (userId, organizationId) values (462, 47);

insert into Users_Roles values (462, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (467, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 468, 'test', 'F', 'F', 'lax76', 'test.lax.76@liferay.com', 'Welcome Test LAX 76!', 'Test', '', 'LAX 76', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (468, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 76', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (469, 1, 467, 12, 467, 0, 0, '469', '/lax76', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (470, 1, 469, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (471, 1, 469, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (467, 18);

insert into Users_Orgs (userId, organizationId) values (467, 21);
insert into Users_Orgs (userId, organizationId) values (467, 47);

insert into Users_Roles values (467, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (472, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 473, 'test', 'F', 'F', 'lax77', 'test.lax.77@liferay.com', 'Welcome Test LAX 77!', 'Test', '', 'LAX 77', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (473, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 77', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (474, 1, 472, 12, 472, 0, 0, '474', '/lax77', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (475, 1, 474, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (476, 1, 474, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (472, 18);

insert into Users_Orgs (userId, organizationId) values (472, 21);
insert into Users_Orgs (userId, organizationId) values (472, 47);

insert into Users_Roles values (472, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (477, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 478, 'test', 'F', 'F', 'lax78', 'test.lax.78@liferay.com', 'Welcome Test LAX 78!', 'Test', '', 'LAX 78', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (478, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 78', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (479, 1, 477, 12, 477, 0, 0, '479', '/lax78', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (480, 1, 479, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (481, 1, 479, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (477, 18);

insert into Users_Orgs (userId, organizationId) values (477, 21);
insert into Users_Orgs (userId, organizationId) values (477, 47);

insert into Users_Roles values (477, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (482, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 483, 'test', 'F', 'F', 'lax79', 'test.lax.79@liferay.com', 'Welcome Test LAX 79!', 'Test', '', 'LAX 79', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (483, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 79', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (484, 1, 482, 12, 482, 0, 0, '484', '/lax79', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (485, 1, 484, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (486, 1, 484, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (482, 18);

insert into Users_Orgs (userId, organizationId) values (482, 21);
insert into Users_Orgs (userId, organizationId) values (482, 47);

insert into Users_Roles values (482, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (487, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 488, 'test', 'F', 'F', 'lax80', 'test.lax.80@liferay.com', 'Welcome Test LAX 80!', 'Test', '', 'LAX 80', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (488, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 80', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (489, 1, 487, 12, 487, 0, 0, '489', '/lax80', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (490, 1, 489, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (491, 1, 489, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (487, 18);

insert into Users_Orgs (userId, organizationId) values (487, 21);
insert into Users_Orgs (userId, organizationId) values (487, 47);

insert into Users_Roles values (487, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (492, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 493, 'test', 'F', 'F', 'lax81', 'test.lax.81@liferay.com', 'Welcome Test LAX 81!', 'Test', '', 'LAX 81', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (493, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 81', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (494, 1, 492, 12, 492, 0, 0, '494', '/lax81', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (495, 1, 494, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (496, 1, 494, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (492, 18);

insert into Users_Orgs (userId, organizationId) values (492, 21);
insert into Users_Orgs (userId, organizationId) values (492, 47);

insert into Users_Roles values (492, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (497, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 498, 'test', 'F', 'F', 'lax82', 'test.lax.82@liferay.com', 'Welcome Test LAX 82!', 'Test', '', 'LAX 82', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (498, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 82', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (499, 1, 497, 12, 497, 0, 0, '499', '/lax82', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (500, 1, 499, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (501, 1, 499, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (497, 18);

insert into Users_Orgs (userId, organizationId) values (497, 21);
insert into Users_Orgs (userId, organizationId) values (497, 47);

insert into Users_Roles values (497, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (502, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 503, 'test', 'F', 'F', 'lax83', 'test.lax.83@liferay.com', 'Welcome Test LAX 83!', 'Test', '', 'LAX 83', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (503, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 83', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (504, 1, 502, 12, 502, 0, 0, '504', '/lax83', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (505, 1, 504, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (506, 1, 504, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (502, 18);

insert into Users_Orgs (userId, organizationId) values (502, 21);
insert into Users_Orgs (userId, organizationId) values (502, 47);

insert into Users_Roles values (502, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (507, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 508, 'test', 'F', 'F', 'lax84', 'test.lax.84@liferay.com', 'Welcome Test LAX 84!', 'Test', '', 'LAX 84', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (508, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 84', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (509, 1, 507, 12, 507, 0, 0, '509', '/lax84', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (510, 1, 509, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (511, 1, 509, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (507, 18);

insert into Users_Orgs (userId, organizationId) values (507, 21);
insert into Users_Orgs (userId, organizationId) values (507, 47);

insert into Users_Roles values (507, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (512, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 513, 'test', 'F', 'F', 'lax85', 'test.lax.85@liferay.com', 'Welcome Test LAX 85!', 'Test', '', 'LAX 85', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (513, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 85', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (514, 1, 512, 12, 512, 0, 0, '514', '/lax85', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (515, 1, 514, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (516, 1, 514, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (512, 18);

insert into Users_Orgs (userId, organizationId) values (512, 21);
insert into Users_Orgs (userId, organizationId) values (512, 47);

insert into Users_Roles values (512, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (517, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 518, 'test', 'F', 'F', 'lax86', 'test.lax.86@liferay.com', 'Welcome Test LAX 86!', 'Test', '', 'LAX 86', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (518, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 86', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (519, 1, 517, 12, 517, 0, 0, '519', '/lax86', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (520, 1, 519, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (521, 1, 519, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (517, 18);

insert into Users_Orgs (userId, organizationId) values (517, 21);
insert into Users_Orgs (userId, organizationId) values (517, 47);

insert into Users_Roles values (517, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (522, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 523, 'test', 'F', 'F', 'lax87', 'test.lax.87@liferay.com', 'Welcome Test LAX 87!', 'Test', '', 'LAX 87', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (523, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 87', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (524, 1, 522, 12, 522, 0, 0, '524', '/lax87', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (525, 1, 524, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (526, 1, 524, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (522, 18);

insert into Users_Orgs (userId, organizationId) values (522, 21);
insert into Users_Orgs (userId, organizationId) values (522, 47);

insert into Users_Roles values (522, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (527, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 528, 'test', 'F', 'F', 'lax88', 'test.lax.88@liferay.com', 'Welcome Test LAX 88!', 'Test', '', 'LAX 88', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (528, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 88', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (529, 1, 527, 12, 527, 0, 0, '529', '/lax88', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (530, 1, 529, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (531, 1, 529, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (527, 18);

insert into Users_Orgs (userId, organizationId) values (527, 21);
insert into Users_Orgs (userId, organizationId) values (527, 47);

insert into Users_Roles values (527, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (532, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 533, 'test', 'F', 'F', 'lax89', 'test.lax.89@liferay.com', 'Welcome Test LAX 89!', 'Test', '', 'LAX 89', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (533, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 89', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (534, 1, 532, 12, 532, 0, 0, '534', '/lax89', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (535, 1, 534, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (536, 1, 534, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (532, 18);

insert into Users_Orgs (userId, organizationId) values (532, 21);
insert into Users_Orgs (userId, organizationId) values (532, 47);

insert into Users_Roles values (532, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (537, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 538, 'test', 'F', 'F', 'lax90', 'test.lax.90@liferay.com', 'Welcome Test LAX 90!', 'Test', '', 'LAX 90', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (538, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 90', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (539, 1, 537, 12, 537, 0, 0, '539', '/lax90', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (540, 1, 539, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (541, 1, 539, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (537, 18);

insert into Users_Orgs (userId, organizationId) values (537, 21);
insert into Users_Orgs (userId, organizationId) values (537, 47);

insert into Users_Roles values (537, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (542, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 543, 'test', 'F', 'F', 'lax91', 'test.lax.91@liferay.com', 'Welcome Test LAX 91!', 'Test', '', 'LAX 91', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (543, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 91', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (544, 1, 542, 12, 542, 0, 0, '544', '/lax91', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (545, 1, 544, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (546, 1, 544, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (542, 18);

insert into Users_Orgs (userId, organizationId) values (542, 21);
insert into Users_Orgs (userId, organizationId) values (542, 47);

insert into Users_Roles values (542, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (547, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 548, 'test', 'F', 'F', 'lax92', 'test.lax.92@liferay.com', 'Welcome Test LAX 92!', 'Test', '', 'LAX 92', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (548, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 92', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (549, 1, 547, 12, 547, 0, 0, '549', '/lax92', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (550, 1, 549, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (551, 1, 549, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (547, 18);

insert into Users_Orgs (userId, organizationId) values (547, 21);
insert into Users_Orgs (userId, organizationId) values (547, 47);

insert into Users_Roles values (547, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (552, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 553, 'test', 'F', 'F', 'lax93', 'test.lax.93@liferay.com', 'Welcome Test LAX 93!', 'Test', '', 'LAX 93', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (553, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 93', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (554, 1, 552, 12, 552, 0, 0, '554', '/lax93', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (555, 1, 554, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (556, 1, 554, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (552, 18);

insert into Users_Orgs (userId, organizationId) values (552, 21);
insert into Users_Orgs (userId, organizationId) values (552, 47);

insert into Users_Roles values (552, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (557, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 558, 'test', 'F', 'F', 'lax94', 'test.lax.94@liferay.com', 'Welcome Test LAX 94!', 'Test', '', 'LAX 94', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (558, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 94', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (559, 1, 557, 12, 557, 0, 0, '559', '/lax94', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (560, 1, 559, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (561, 1, 559, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (557, 18);

insert into Users_Orgs (userId, organizationId) values (557, 21);
insert into Users_Orgs (userId, organizationId) values (557, 47);

insert into Users_Roles values (557, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (562, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 563, 'test', 'F', 'F', 'lax95', 'test.lax.95@liferay.com', 'Welcome Test LAX 95!', 'Test', '', 'LAX 95', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (563, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 95', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (564, 1, 562, 12, 562, 0, 0, '564', '/lax95', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (565, 1, 564, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (566, 1, 564, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (562, 18);

insert into Users_Orgs (userId, organizationId) values (562, 21);
insert into Users_Orgs (userId, organizationId) values (562, 47);

insert into Users_Roles values (562, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (567, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 568, 'test', 'F', 'F', 'lax96', 'test.lax.96@liferay.com', 'Welcome Test LAX 96!', 'Test', '', 'LAX 96', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (568, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 96', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (569, 1, 567, 12, 567, 0, 0, '569', '/lax96', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (570, 1, 569, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (571, 1, 569, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (567, 18);

insert into Users_Orgs (userId, organizationId) values (567, 21);
insert into Users_Orgs (userId, organizationId) values (567, 47);

insert into Users_Roles values (567, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (572, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 573, 'test', 'F', 'F', 'lax97', 'test.lax.97@liferay.com', 'Welcome Test LAX 97!', 'Test', '', 'LAX 97', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (573, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 97', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (574, 1, 572, 12, 572, 0, 0, '574', '/lax97', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (575, 1, 574, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (576, 1, 574, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (572, 18);

insert into Users_Orgs (userId, organizationId) values (572, 21);
insert into Users_Orgs (userId, organizationId) values (572, 47);

insert into Users_Roles values (572, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (577, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 578, 'test', 'F', 'F', 'lax98', 'test.lax.98@liferay.com', 'Welcome Test LAX 98!', 'Test', '', 'LAX 98', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (578, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 98', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (579, 1, 577, 12, 577, 0, 0, '579', '/lax98', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (580, 1, 579, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (581, 1, 579, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (577, 18);

insert into Users_Orgs (userId, organizationId) values (577, 21);
insert into Users_Orgs (userId, organizationId) values (577, 47);

insert into Users_Roles values (577, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (582, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 583, 'test', 'F', 'F', 'lax99', 'test.lax.99@liferay.com', 'Welcome Test LAX 99!', 'Test', '', 'LAX 99', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (583, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 99', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (584, 1, 582, 12, 582, 0, 0, '584', '/lax99', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (585, 1, 584, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (586, 1, 584, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (582, 18);

insert into Users_Orgs (userId, organizationId) values (582, 21);
insert into Users_Orgs (userId, organizationId) values (582, 47);

insert into Users_Roles values (582, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (587, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 588, 'test', 'F', 'F', 'lax100', 'test.lax.100@liferay.com', 'Welcome Test LAX 100!', 'Test', '', 'LAX 100', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (588, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'LAX 100', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (589, 1, 587, 12, 587, 0, 0, '589', '/lax100', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (590, 1, 589, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (591, 1, 589, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (587, 18);

insert into Users_Orgs (userId, organizationId) values (587, 21);
insert into Users_Orgs (userId, organizationId) values (587, 47);

insert into Users_Roles values (587, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (592, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 593, 'test', 'F', 'F', 'sfo1', 'test.sfo.1@liferay.com', 'Welcome Test SFO 1!', 'Test', '', 'SFO 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (593, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (594, 1, 592, 12, 592, 0, 0, '594', '/sfo1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (595, 1, 594, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (596, 1, 594, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (592, 18);

insert into Users_Orgs (userId, organizationId) values (592, 21);
insert into Users_Orgs (userId, organizationId) values (592, 51);

insert into Users_Roles values (592, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (597, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 598, 'test', 'F', 'F', 'sfo2', 'test.sfo.2@liferay.com', 'Welcome Test SFO 2!', 'Test', '', 'SFO 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (598, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (599, 1, 597, 12, 597, 0, 0, '599', '/sfo2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (600, 1, 599, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (601, 1, 599, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (597, 18);

insert into Users_Orgs (userId, organizationId) values (597, 21);
insert into Users_Orgs (userId, organizationId) values (597, 51);

insert into Users_Roles values (597, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (602, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 603, 'test', 'F', 'F', 'sfo3', 'test.sfo.3@liferay.com', 'Welcome Test SFO 3!', 'Test', '', 'SFO 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (603, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (604, 1, 602, 12, 602, 0, 0, '604', '/sfo3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (605, 1, 604, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (606, 1, 604, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (602, 18);

insert into Users_Orgs (userId, organizationId) values (602, 21);
insert into Users_Orgs (userId, organizationId) values (602, 51);

insert into Users_Roles values (602, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (607, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 608, 'test', 'F', 'F', 'sfo4', 'test.sfo.4@liferay.com', 'Welcome Test SFO 4!', 'Test', '', 'SFO 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (608, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (609, 1, 607, 12, 607, 0, 0, '609', '/sfo4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (610, 1, 609, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (611, 1, 609, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (607, 18);

insert into Users_Orgs (userId, organizationId) values (607, 21);
insert into Users_Orgs (userId, organizationId) values (607, 51);

insert into Users_Roles values (607, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (612, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 613, 'test', 'F', 'F', 'sfo5', 'test.sfo.5@liferay.com', 'Welcome Test SFO 5!', 'Test', '', 'SFO 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (613, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (614, 1, 612, 12, 612, 0, 0, '614', '/sfo5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (615, 1, 614, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (616, 1, 614, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (612, 18);

insert into Users_Orgs (userId, organizationId) values (612, 21);
insert into Users_Orgs (userId, organizationId) values (612, 51);

insert into Users_Roles values (612, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (617, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 618, 'test', 'F', 'F', 'sfo6', 'test.sfo.6@liferay.com', 'Welcome Test SFO 6!', 'Test', '', 'SFO 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (618, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (619, 1, 617, 12, 617, 0, 0, '619', '/sfo6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (620, 1, 619, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (621, 1, 619, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (617, 18);

insert into Users_Orgs (userId, organizationId) values (617, 21);
insert into Users_Orgs (userId, organizationId) values (617, 51);

insert into Users_Roles values (617, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (622, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 623, 'test', 'F', 'F', 'sfo7', 'test.sfo.7@liferay.com', 'Welcome Test SFO 7!', 'Test', '', 'SFO 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (623, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (624, 1, 622, 12, 622, 0, 0, '624', '/sfo7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (625, 1, 624, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (626, 1, 624, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (622, 18);

insert into Users_Orgs (userId, organizationId) values (622, 21);
insert into Users_Orgs (userId, organizationId) values (622, 51);

insert into Users_Roles values (622, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (627, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 628, 'test', 'F', 'F', 'sfo8', 'test.sfo.8@liferay.com', 'Welcome Test SFO 8!', 'Test', '', 'SFO 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (628, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (629, 1, 627, 12, 627, 0, 0, '629', '/sfo8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (630, 1, 629, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (631, 1, 629, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (627, 18);

insert into Users_Orgs (userId, organizationId) values (627, 21);
insert into Users_Orgs (userId, organizationId) values (627, 51);

insert into Users_Roles values (627, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (632, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 633, 'test', 'F', 'F', 'sfo9', 'test.sfo.9@liferay.com', 'Welcome Test SFO 9!', 'Test', '', 'SFO 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (633, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (634, 1, 632, 12, 632, 0, 0, '634', '/sfo9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (635, 1, 634, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (636, 1, 634, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (632, 18);

insert into Users_Orgs (userId, organizationId) values (632, 21);
insert into Users_Orgs (userId, organizationId) values (632, 51);

insert into Users_Roles values (632, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (637, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 638, 'test', 'F', 'F', 'sfo10', 'test.sfo.10@liferay.com', 'Welcome Test SFO 10!', 'Test', '', 'SFO 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (638, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'SFO 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (639, 1, 637, 12, 637, 0, 0, '639', '/sfo10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (640, 1, 639, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (641, 1, 639, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (637, 18);

insert into Users_Orgs (userId, organizationId) values (637, 21);
insert into Users_Orgs (userId, organizationId) values (637, 51);

insert into Users_Roles values (637, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (642, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 643, 'test', 'F', 'F', 'ord1', 'test.ord.1@liferay.com', 'Welcome Test ORD 1!', 'Test', '', 'ORD 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (643, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (644, 1, 642, 12, 642, 0, 0, '644', '/ord1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (645, 1, 644, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (646, 1, 644, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (642, 18);

insert into Users_Orgs (userId, organizationId) values (642, 21);
insert into Users_Orgs (userId, organizationId) values (642, 55);

insert into Users_Roles values (642, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (647, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 648, 'test', 'F', 'F', 'ord2', 'test.ord.2@liferay.com', 'Welcome Test ORD 2!', 'Test', '', 'ORD 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (648, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (649, 1, 647, 12, 647, 0, 0, '649', '/ord2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (650, 1, 649, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (651, 1, 649, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (647, 18);

insert into Users_Orgs (userId, organizationId) values (647, 21);
insert into Users_Orgs (userId, organizationId) values (647, 55);

insert into Users_Roles values (647, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (652, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 653, 'test', 'F', 'F', 'ord3', 'test.ord.3@liferay.com', 'Welcome Test ORD 3!', 'Test', '', 'ORD 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (653, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (654, 1, 652, 12, 652, 0, 0, '654', '/ord3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (655, 1, 654, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (656, 1, 654, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (652, 18);

insert into Users_Orgs (userId, organizationId) values (652, 21);
insert into Users_Orgs (userId, organizationId) values (652, 55);

insert into Users_Roles values (652, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (657, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 658, 'test', 'F', 'F', 'ord4', 'test.ord.4@liferay.com', 'Welcome Test ORD 4!', 'Test', '', 'ORD 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (658, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (659, 1, 657, 12, 657, 0, 0, '659', '/ord4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (660, 1, 659, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (661, 1, 659, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (657, 18);

insert into Users_Orgs (userId, organizationId) values (657, 21);
insert into Users_Orgs (userId, organizationId) values (657, 55);

insert into Users_Roles values (657, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (662, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 663, 'test', 'F', 'F', 'ord5', 'test.ord.5@liferay.com', 'Welcome Test ORD 5!', 'Test', '', 'ORD 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (663, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (664, 1, 662, 12, 662, 0, 0, '664', '/ord5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (665, 1, 664, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (666, 1, 664, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (662, 18);

insert into Users_Orgs (userId, organizationId) values (662, 21);
insert into Users_Orgs (userId, organizationId) values (662, 55);

insert into Users_Roles values (662, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (667, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 668, 'test', 'F', 'F', 'ord6', 'test.ord.6@liferay.com', 'Welcome Test ORD 6!', 'Test', '', 'ORD 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (668, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (669, 1, 667, 12, 667, 0, 0, '669', '/ord6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (670, 1, 669, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (671, 1, 669, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (667, 18);

insert into Users_Orgs (userId, organizationId) values (667, 21);
insert into Users_Orgs (userId, organizationId) values (667, 55);

insert into Users_Roles values (667, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (672, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 673, 'test', 'F', 'F', 'ord7', 'test.ord.7@liferay.com', 'Welcome Test ORD 7!', 'Test', '', 'ORD 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (673, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (674, 1, 672, 12, 672, 0, 0, '674', '/ord7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (675, 1, 674, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (676, 1, 674, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (672, 18);

insert into Users_Orgs (userId, organizationId) values (672, 21);
insert into Users_Orgs (userId, organizationId) values (672, 55);

insert into Users_Roles values (672, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (677, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 678, 'test', 'F', 'F', 'ord8', 'test.ord.8@liferay.com', 'Welcome Test ORD 8!', 'Test', '', 'ORD 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (678, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (679, 1, 677, 12, 677, 0, 0, '679', '/ord8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (680, 1, 679, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (681, 1, 679, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (677, 18);

insert into Users_Orgs (userId, organizationId) values (677, 21);
insert into Users_Orgs (userId, organizationId) values (677, 55);

insert into Users_Roles values (677, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (682, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 683, 'test', 'F', 'F', 'ord9', 'test.ord.9@liferay.com', 'Welcome Test ORD 9!', 'Test', '', 'ORD 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (683, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (684, 1, 682, 12, 682, 0, 0, '684', '/ord9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (685, 1, 684, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (686, 1, 684, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (682, 18);

insert into Users_Orgs (userId, organizationId) values (682, 21);
insert into Users_Orgs (userId, organizationId) values (682, 55);

insert into Users_Roles values (682, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (687, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 688, 'test', 'F', 'F', 'ord10', 'test.ord.10@liferay.com', 'Welcome Test ORD 10!', 'Test', '', 'ORD 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (688, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'ORD 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (689, 1, 687, 12, 687, 0, 0, '689', '/ord10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (690, 1, 689, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (691, 1, 689, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (687, 18);

insert into Users_Orgs (userId, organizationId) values (687, 21);
insert into Users_Orgs (userId, organizationId) values (687, 55);

insert into Users_Roles values (687, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (692, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 693, 'test', 'F', 'F', 'nyc1', 'test.nyc.1@liferay.com', 'Welcome Test NYC 1!', 'Test', '', 'NYC 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (693, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (694, 1, 692, 12, 692, 0, 0, '694', '/nyc1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (695, 1, 694, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (696, 1, 694, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (692, 18);

insert into Users_Orgs (userId, organizationId) values (692, 21);
insert into Users_Orgs (userId, organizationId) values (692, 59);

insert into Users_Roles values (692, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (697, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 698, 'test', 'F', 'F', 'nyc2', 'test.nyc.2@liferay.com', 'Welcome Test NYC 2!', 'Test', '', 'NYC 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (698, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (699, 1, 697, 12, 697, 0, 0, '699', '/nyc2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (700, 1, 699, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (701, 1, 699, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (697, 18);

insert into Users_Orgs (userId, organizationId) values (697, 21);
insert into Users_Orgs (userId, organizationId) values (697, 59);

insert into Users_Roles values (697, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (702, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 703, 'test', 'F', 'F', 'nyc3', 'test.nyc.3@liferay.com', 'Welcome Test NYC 3!', 'Test', '', 'NYC 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (703, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (704, 1, 702, 12, 702, 0, 0, '704', '/nyc3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (705, 1, 704, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (706, 1, 704, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (702, 18);

insert into Users_Orgs (userId, organizationId) values (702, 21);
insert into Users_Orgs (userId, organizationId) values (702, 59);

insert into Users_Roles values (702, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (707, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 708, 'test', 'F', 'F', 'nyc4', 'test.nyc.4@liferay.com', 'Welcome Test NYC 4!', 'Test', '', 'NYC 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (708, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (709, 1, 707, 12, 707, 0, 0, '709', '/nyc4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (710, 1, 709, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (711, 1, 709, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (707, 18);

insert into Users_Orgs (userId, organizationId) values (707, 21);
insert into Users_Orgs (userId, organizationId) values (707, 59);

insert into Users_Roles values (707, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (712, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 713, 'test', 'F', 'F', 'nyc5', 'test.nyc.5@liferay.com', 'Welcome Test NYC 5!', 'Test', '', 'NYC 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (713, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (714, 1, 712, 12, 712, 0, 0, '714', '/nyc5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (715, 1, 714, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (716, 1, 714, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (712, 18);

insert into Users_Orgs (userId, organizationId) values (712, 21);
insert into Users_Orgs (userId, organizationId) values (712, 59);

insert into Users_Roles values (712, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (717, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 718, 'test', 'F', 'F', 'nyc6', 'test.nyc.6@liferay.com', 'Welcome Test NYC 6!', 'Test', '', 'NYC 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (718, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (719, 1, 717, 12, 717, 0, 0, '719', '/nyc6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (720, 1, 719, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (721, 1, 719, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (717, 18);

insert into Users_Orgs (userId, organizationId) values (717, 21);
insert into Users_Orgs (userId, organizationId) values (717, 59);

insert into Users_Roles values (717, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (722, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 723, 'test', 'F', 'F', 'nyc7', 'test.nyc.7@liferay.com', 'Welcome Test NYC 7!', 'Test', '', 'NYC 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (723, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (724, 1, 722, 12, 722, 0, 0, '724', '/nyc7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (725, 1, 724, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (726, 1, 724, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (722, 18);

insert into Users_Orgs (userId, organizationId) values (722, 21);
insert into Users_Orgs (userId, organizationId) values (722, 59);

insert into Users_Roles values (722, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (727, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 728, 'test', 'F', 'F', 'nyc8', 'test.nyc.8@liferay.com', 'Welcome Test NYC 8!', 'Test', '', 'NYC 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (728, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (729, 1, 727, 12, 727, 0, 0, '729', '/nyc8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (730, 1, 729, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (731, 1, 729, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (727, 18);

insert into Users_Orgs (userId, organizationId) values (727, 21);
insert into Users_Orgs (userId, organizationId) values (727, 59);

insert into Users_Roles values (727, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (732, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 733, 'test', 'F', 'F', 'nyc9', 'test.nyc.9@liferay.com', 'Welcome Test NYC 9!', 'Test', '', 'NYC 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (733, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (734, 1, 732, 12, 732, 0, 0, '734', '/nyc9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (735, 1, 734, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (736, 1, 734, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (732, 18);

insert into Users_Orgs (userId, organizationId) values (732, 21);
insert into Users_Orgs (userId, organizationId) values (732, 59);

insert into Users_Roles values (732, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (737, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 738, 'test', 'F', 'F', 'nyc10', 'test.nyc.10@liferay.com', 'Welcome Test NYC 10!', 'Test', '', 'NYC 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (738, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'NYC 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (739, 1, 737, 12, 737, 0, 0, '739', '/nyc10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (740, 1, 739, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (741, 1, 739, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (737, 18);

insert into Users_Orgs (userId, organizationId) values (737, 21);
insert into Users_Orgs (userId, organizationId) values (737, 59);

insert into Users_Roles values (737, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (742, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 743, 'test', 'F', 'F', 'gru1', 'test.gru.1@liferay.com', 'Welcome Test GRU 1!', 'Test', '', 'GRU 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (743, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (744, 1, 742, 12, 742, 0, 0, '744', '/gru1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (745, 1, 744, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (746, 1, 744, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (742, 18);

insert into Users_Orgs (userId, organizationId) values (742, 21);
insert into Users_Orgs (userId, organizationId) values (742, 63);

insert into Users_Roles values (742, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (747, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 748, 'test', 'F', 'F', 'gru2', 'test.gru.2@liferay.com', 'Welcome Test GRU 2!', 'Test', '', 'GRU 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (748, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (749, 1, 747, 12, 747, 0, 0, '749', '/gru2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (750, 1, 749, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (751, 1, 749, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (747, 18);

insert into Users_Orgs (userId, organizationId) values (747, 21);
insert into Users_Orgs (userId, organizationId) values (747, 63);

insert into Users_Roles values (747, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (752, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 753, 'test', 'F', 'F', 'gru3', 'test.gru.3@liferay.com', 'Welcome Test GRU 3!', 'Test', '', 'GRU 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (753, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (754, 1, 752, 12, 752, 0, 0, '754', '/gru3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (755, 1, 754, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (756, 1, 754, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (752, 18);

insert into Users_Orgs (userId, organizationId) values (752, 21);
insert into Users_Orgs (userId, organizationId) values (752, 63);

insert into Users_Roles values (752, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (757, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 758, 'test', 'F', 'F', 'gru4', 'test.gru.4@liferay.com', 'Welcome Test GRU 4!', 'Test', '', 'GRU 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (758, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (759, 1, 757, 12, 757, 0, 0, '759', '/gru4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (760, 1, 759, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (761, 1, 759, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (757, 18);

insert into Users_Orgs (userId, organizationId) values (757, 21);
insert into Users_Orgs (userId, organizationId) values (757, 63);

insert into Users_Roles values (757, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (762, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 763, 'test', 'F', 'F', 'gru5', 'test.gru.5@liferay.com', 'Welcome Test GRU 5!', 'Test', '', 'GRU 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (763, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (764, 1, 762, 12, 762, 0, 0, '764', '/gru5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (765, 1, 764, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (766, 1, 764, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (762, 18);

insert into Users_Orgs (userId, organizationId) values (762, 21);
insert into Users_Orgs (userId, organizationId) values (762, 63);

insert into Users_Roles values (762, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (767, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 768, 'test', 'F', 'F', 'gru6', 'test.gru.6@liferay.com', 'Welcome Test GRU 6!', 'Test', '', 'GRU 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (768, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (769, 1, 767, 12, 767, 0, 0, '769', '/gru6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (770, 1, 769, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (771, 1, 769, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (767, 18);

insert into Users_Orgs (userId, organizationId) values (767, 21);
insert into Users_Orgs (userId, organizationId) values (767, 63);

insert into Users_Roles values (767, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (772, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 773, 'test', 'F', 'F', 'gru7', 'test.gru.7@liferay.com', 'Welcome Test GRU 7!', 'Test', '', 'GRU 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (773, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (774, 1, 772, 12, 772, 0, 0, '774', '/gru7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (775, 1, 774, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (776, 1, 774, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (772, 18);

insert into Users_Orgs (userId, organizationId) values (772, 21);
insert into Users_Orgs (userId, organizationId) values (772, 63);

insert into Users_Roles values (772, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (777, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 778, 'test', 'F', 'F', 'gru8', 'test.gru.8@liferay.com', 'Welcome Test GRU 8!', 'Test', '', 'GRU 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (778, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (779, 1, 777, 12, 777, 0, 0, '779', '/gru8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (780, 1, 779, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (781, 1, 779, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (777, 18);

insert into Users_Orgs (userId, organizationId) values (777, 21);
insert into Users_Orgs (userId, organizationId) values (777, 63);

insert into Users_Roles values (777, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (782, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 783, 'test', 'F', 'F', 'gru9', 'test.gru.9@liferay.com', 'Welcome Test GRU 9!', 'Test', '', 'GRU 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (783, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (784, 1, 782, 12, 782, 0, 0, '784', '/gru9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (785, 1, 784, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (786, 1, 784, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (782, 18);

insert into Users_Orgs (userId, organizationId) values (782, 21);
insert into Users_Orgs (userId, organizationId) values (782, 63);

insert into Users_Roles values (782, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (787, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 788, 'test', 'F', 'F', 'gru10', 'test.gru.10@liferay.com', 'Welcome Test GRU 10!', 'Test', '', 'GRU 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (788, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'GRU 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (789, 1, 787, 12, 787, 0, 0, '789', '/gru10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (790, 1, 789, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (791, 1, 789, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (787, 18);

insert into Users_Orgs (userId, organizationId) values (787, 21);
insert into Users_Orgs (userId, organizationId) values (787, 63);

insert into Users_Roles values (787, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (792, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 793, 'test', 'F', 'F', 'fra1', 'test.fra.1@liferay.com', 'Welcome Test FRA 1!', 'Test', '', 'FRA 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (793, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (794, 1, 792, 12, 792, 0, 0, '794', '/fra1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (795, 1, 794, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (796, 1, 794, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (792, 18);

insert into Users_Orgs (userId, organizationId) values (792, 21);
insert into Users_Orgs (userId, organizationId) values (792, 67);

insert into Users_Roles values (792, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (797, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 798, 'test', 'F', 'F', 'fra2', 'test.fra.2@liferay.com', 'Welcome Test FRA 2!', 'Test', '', 'FRA 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (798, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (799, 1, 797, 12, 797, 0, 0, '799', '/fra2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (800, 1, 799, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (801, 1, 799, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (797, 18);

insert into Users_Orgs (userId, organizationId) values (797, 21);
insert into Users_Orgs (userId, organizationId) values (797, 67);

insert into Users_Roles values (797, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (802, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 803, 'test', 'F', 'F', 'fra3', 'test.fra.3@liferay.com', 'Welcome Test FRA 3!', 'Test', '', 'FRA 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (803, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (804, 1, 802, 12, 802, 0, 0, '804', '/fra3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (805, 1, 804, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (806, 1, 804, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (802, 18);

insert into Users_Orgs (userId, organizationId) values (802, 21);
insert into Users_Orgs (userId, organizationId) values (802, 67);

insert into Users_Roles values (802, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (807, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 808, 'test', 'F', 'F', 'fra4', 'test.fra.4@liferay.com', 'Welcome Test FRA 4!', 'Test', '', 'FRA 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (808, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (809, 1, 807, 12, 807, 0, 0, '809', '/fra4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (810, 1, 809, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (811, 1, 809, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (807, 18);

insert into Users_Orgs (userId, organizationId) values (807, 21);
insert into Users_Orgs (userId, organizationId) values (807, 67);

insert into Users_Roles values (807, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (812, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 813, 'test', 'F', 'F', 'fra5', 'test.fra.5@liferay.com', 'Welcome Test FRA 5!', 'Test', '', 'FRA 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (813, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (814, 1, 812, 12, 812, 0, 0, '814', '/fra5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (815, 1, 814, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (816, 1, 814, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (812, 18);

insert into Users_Orgs (userId, organizationId) values (812, 21);
insert into Users_Orgs (userId, organizationId) values (812, 67);

insert into Users_Roles values (812, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (817, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 818, 'test', 'F', 'F', 'fra6', 'test.fra.6@liferay.com', 'Welcome Test FRA 6!', 'Test', '', 'FRA 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (818, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (819, 1, 817, 12, 817, 0, 0, '819', '/fra6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (820, 1, 819, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (821, 1, 819, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (817, 18);

insert into Users_Orgs (userId, organizationId) values (817, 21);
insert into Users_Orgs (userId, organizationId) values (817, 67);

insert into Users_Roles values (817, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (822, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 823, 'test', 'F', 'F', 'fra7', 'test.fra.7@liferay.com', 'Welcome Test FRA 7!', 'Test', '', 'FRA 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (823, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (824, 1, 822, 12, 822, 0, 0, '824', '/fra7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (825, 1, 824, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (826, 1, 824, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (822, 18);

insert into Users_Orgs (userId, organizationId) values (822, 21);
insert into Users_Orgs (userId, organizationId) values (822, 67);

insert into Users_Roles values (822, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (827, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 828, 'test', 'F', 'F', 'fra8', 'test.fra.8@liferay.com', 'Welcome Test FRA 8!', 'Test', '', 'FRA 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (828, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (829, 1, 827, 12, 827, 0, 0, '829', '/fra8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (830, 1, 829, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (831, 1, 829, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (827, 18);

insert into Users_Orgs (userId, organizationId) values (827, 21);
insert into Users_Orgs (userId, organizationId) values (827, 67);

insert into Users_Roles values (827, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (832, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 833, 'test', 'F', 'F', 'fra9', 'test.fra.9@liferay.com', 'Welcome Test FRA 9!', 'Test', '', 'FRA 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (833, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (834, 1, 832, 12, 832, 0, 0, '834', '/fra9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (835, 1, 834, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (836, 1, 834, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (832, 18);

insert into Users_Orgs (userId, organizationId) values (832, 21);
insert into Users_Orgs (userId, organizationId) values (832, 67);

insert into Users_Roles values (832, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (837, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 838, 'test', 'F', 'F', 'fra10', 'test.fra.10@liferay.com', 'Welcome Test FRA 10!', 'Test', '', 'FRA 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (838, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'FRA 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (839, 1, 837, 12, 837, 0, 0, '839', '/fra10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (840, 1, 839, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (841, 1, 839, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (837, 18);

insert into Users_Orgs (userId, organizationId) values (837, 21);
insert into Users_Orgs (userId, organizationId) values (837, 67);

insert into Users_Roles values (837, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (842, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 843, 'test', 'F', 'F', 'mad1', 'test.mad.1@liferay.com', 'Welcome Test MAD 1!', 'Test', '', 'MAD 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (843, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (844, 1, 842, 12, 842, 0, 0, '844', '/mad1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (845, 1, 844, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (846, 1, 844, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (842, 18);

insert into Users_Orgs (userId, organizationId) values (842, 21);
insert into Users_Orgs (userId, organizationId) values (842, 71);

insert into Users_Roles values (842, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (847, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 848, 'test', 'F', 'F', 'mad2', 'test.mad.2@liferay.com', 'Welcome Test MAD 2!', 'Test', '', 'MAD 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (848, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (849, 1, 847, 12, 847, 0, 0, '849', '/mad2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (850, 1, 849, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (851, 1, 849, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (847, 18);

insert into Users_Orgs (userId, organizationId) values (847, 21);
insert into Users_Orgs (userId, organizationId) values (847, 71);

insert into Users_Roles values (847, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (852, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 853, 'test', 'F', 'F', 'mad3', 'test.mad.3@liferay.com', 'Welcome Test MAD 3!', 'Test', '', 'MAD 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (853, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (854, 1, 852, 12, 852, 0, 0, '854', '/mad3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (855, 1, 854, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (856, 1, 854, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (852, 18);

insert into Users_Orgs (userId, organizationId) values (852, 21);
insert into Users_Orgs (userId, organizationId) values (852, 71);

insert into Users_Roles values (852, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (857, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 858, 'test', 'F', 'F', 'mad4', 'test.mad.4@liferay.com', 'Welcome Test MAD 4!', 'Test', '', 'MAD 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (858, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (859, 1, 857, 12, 857, 0, 0, '859', '/mad4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (860, 1, 859, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (861, 1, 859, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (857, 18);

insert into Users_Orgs (userId, organizationId) values (857, 21);
insert into Users_Orgs (userId, organizationId) values (857, 71);

insert into Users_Roles values (857, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (862, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 863, 'test', 'F', 'F', 'mad5', 'test.mad.5@liferay.com', 'Welcome Test MAD 5!', 'Test', '', 'MAD 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (863, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (864, 1, 862, 12, 862, 0, 0, '864', '/mad5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (865, 1, 864, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (866, 1, 864, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (862, 18);

insert into Users_Orgs (userId, organizationId) values (862, 21);
insert into Users_Orgs (userId, organizationId) values (862, 71);

insert into Users_Roles values (862, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (867, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 868, 'test', 'F', 'F', 'mad6', 'test.mad.6@liferay.com', 'Welcome Test MAD 6!', 'Test', '', 'MAD 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (868, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (869, 1, 867, 12, 867, 0, 0, '869', '/mad6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (870, 1, 869, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (871, 1, 869, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (867, 18);

insert into Users_Orgs (userId, organizationId) values (867, 21);
insert into Users_Orgs (userId, organizationId) values (867, 71);

insert into Users_Roles values (867, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (872, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 873, 'test', 'F', 'F', 'mad7', 'test.mad.7@liferay.com', 'Welcome Test MAD 7!', 'Test', '', 'MAD 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (873, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (874, 1, 872, 12, 872, 0, 0, '874', '/mad7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (875, 1, 874, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (876, 1, 874, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (872, 18);

insert into Users_Orgs (userId, organizationId) values (872, 21);
insert into Users_Orgs (userId, organizationId) values (872, 71);

insert into Users_Roles values (872, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (877, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 878, 'test', 'F', 'F', 'mad8', 'test.mad.8@liferay.com', 'Welcome Test MAD 8!', 'Test', '', 'MAD 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (878, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (879, 1, 877, 12, 877, 0, 0, '879', '/mad8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (880, 1, 879, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (881, 1, 879, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (877, 18);

insert into Users_Orgs (userId, organizationId) values (877, 21);
insert into Users_Orgs (userId, organizationId) values (877, 71);

insert into Users_Roles values (877, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (882, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 883, 'test', 'F', 'F', 'mad9', 'test.mad.9@liferay.com', 'Welcome Test MAD 9!', 'Test', '', 'MAD 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (883, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (884, 1, 882, 12, 882, 0, 0, '884', '/mad9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (885, 1, 884, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (886, 1, 884, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (882, 18);

insert into Users_Orgs (userId, organizationId) values (882, 21);
insert into Users_Orgs (userId, organizationId) values (882, 71);

insert into Users_Roles values (882, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (887, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 888, 'test', 'F', 'F', 'mad10', 'test.mad.10@liferay.com', 'Welcome Test MAD 10!', 'Test', '', 'MAD 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (888, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'MAD 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (889, 1, 887, 12, 887, 0, 0, '889', '/mad10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (890, 1, 889, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (891, 1, 889, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (887, 18);

insert into Users_Orgs (userId, organizationId) values (887, 21);
insert into Users_Orgs (userId, organizationId) values (887, 71);

insert into Users_Roles values (887, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (892, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 893, 'test', 'F', 'F', 'dlc1', 'test.dlc.1@liferay.com', 'Welcome Test DLC 1!', 'Test', '', 'DLC 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (893, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (894, 1, 892, 12, 892, 0, 0, '894', '/dlc1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (895, 1, 894, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (896, 1, 894, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (892, 18);

insert into Users_Orgs (userId, organizationId) values (892, 21);
insert into Users_Orgs (userId, organizationId) values (892, 75);

insert into Users_Roles values (892, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (897, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 898, 'test', 'F', 'F', 'dlc2', 'test.dlc.2@liferay.com', 'Welcome Test DLC 2!', 'Test', '', 'DLC 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (898, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (899, 1, 897, 12, 897, 0, 0, '899', '/dlc2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (900, 1, 899, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (901, 1, 899, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (897, 18);

insert into Users_Orgs (userId, organizationId) values (897, 21);
insert into Users_Orgs (userId, organizationId) values (897, 75);

insert into Users_Roles values (897, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (902, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 903, 'test', 'F', 'F', 'dlc3', 'test.dlc.3@liferay.com', 'Welcome Test DLC 3!', 'Test', '', 'DLC 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (903, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (904, 1, 902, 12, 902, 0, 0, '904', '/dlc3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (905, 1, 904, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (906, 1, 904, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (902, 18);

insert into Users_Orgs (userId, organizationId) values (902, 21);
insert into Users_Orgs (userId, organizationId) values (902, 75);

insert into Users_Roles values (902, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (907, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 908, 'test', 'F', 'F', 'dlc4', 'test.dlc.4@liferay.com', 'Welcome Test DLC 4!', 'Test', '', 'DLC 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (908, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (909, 1, 907, 12, 907, 0, 0, '909', '/dlc4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (910, 1, 909, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (911, 1, 909, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (907, 18);

insert into Users_Orgs (userId, organizationId) values (907, 21);
insert into Users_Orgs (userId, organizationId) values (907, 75);

insert into Users_Roles values (907, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (912, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 913, 'test', 'F', 'F', 'dlc5', 'test.dlc.5@liferay.com', 'Welcome Test DLC 5!', 'Test', '', 'DLC 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (913, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (914, 1, 912, 12, 912, 0, 0, '914', '/dlc5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (915, 1, 914, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (916, 1, 914, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (912, 18);

insert into Users_Orgs (userId, organizationId) values (912, 21);
insert into Users_Orgs (userId, organizationId) values (912, 75);

insert into Users_Roles values (912, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (917, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 918, 'test', 'F', 'F', 'dlc6', 'test.dlc.6@liferay.com', 'Welcome Test DLC 6!', 'Test', '', 'DLC 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (918, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (919, 1, 917, 12, 917, 0, 0, '919', '/dlc6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (920, 1, 919, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (921, 1, 919, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (917, 18);

insert into Users_Orgs (userId, organizationId) values (917, 21);
insert into Users_Orgs (userId, organizationId) values (917, 75);

insert into Users_Roles values (917, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (922, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 923, 'test', 'F', 'F', 'dlc7', 'test.dlc.7@liferay.com', 'Welcome Test DLC 7!', 'Test', '', 'DLC 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (923, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (924, 1, 922, 12, 922, 0, 0, '924', '/dlc7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (925, 1, 924, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (926, 1, 924, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (922, 18);

insert into Users_Orgs (userId, organizationId) values (922, 21);
insert into Users_Orgs (userId, organizationId) values (922, 75);

insert into Users_Roles values (922, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (927, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 928, 'test', 'F', 'F', 'dlc8', 'test.dlc.8@liferay.com', 'Welcome Test DLC 8!', 'Test', '', 'DLC 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (928, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (929, 1, 927, 12, 927, 0, 0, '929', '/dlc8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (930, 1, 929, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (931, 1, 929, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (927, 18);

insert into Users_Orgs (userId, organizationId) values (927, 21);
insert into Users_Orgs (userId, organizationId) values (927, 75);

insert into Users_Roles values (927, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (932, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 933, 'test', 'F', 'F', 'dlc9', 'test.dlc.9@liferay.com', 'Welcome Test DLC 9!', 'Test', '', 'DLC 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (933, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (934, 1, 932, 12, 932, 0, 0, '934', '/dlc9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (935, 1, 934, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (936, 1, 934, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (932, 18);

insert into Users_Orgs (userId, organizationId) values (932, 21);
insert into Users_Orgs (userId, organizationId) values (932, 75);

insert into Users_Roles values (932, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (937, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 938, 'test', 'F', 'F', 'dlc10', 'test.dlc.10@liferay.com', 'Welcome Test DLC 10!', 'Test', '', 'DLC 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (938, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'DLC 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (939, 1, 937, 12, 937, 0, 0, '939', '/dlc10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (940, 1, 939, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (941, 1, 939, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (937, 18);

insert into Users_Orgs (userId, organizationId) values (937, 21);
insert into Users_Orgs (userId, organizationId) values (937, 75);

insert into Users_Roles values (937, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (942, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 943, 'test', 'F', 'F', 'hkg1', 'test.hkg.1@liferay.com', 'Welcome Test HKG 1!', 'Test', '', 'HKG 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (943, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (944, 1, 942, 12, 942, 0, 0, '944', '/hkg1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (945, 1, 944, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (946, 1, 944, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (942, 18);

insert into Users_Orgs (userId, organizationId) values (942, 21);
insert into Users_Orgs (userId, organizationId) values (942, 79);

insert into Users_Roles values (942, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (947, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 948, 'test', 'F', 'F', 'hkg2', 'test.hkg.2@liferay.com', 'Welcome Test HKG 2!', 'Test', '', 'HKG 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (948, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (949, 1, 947, 12, 947, 0, 0, '949', '/hkg2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (950, 1, 949, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (951, 1, 949, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (947, 18);

insert into Users_Orgs (userId, organizationId) values (947, 21);
insert into Users_Orgs (userId, organizationId) values (947, 79);

insert into Users_Roles values (947, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (952, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 953, 'test', 'F', 'F', 'hkg3', 'test.hkg.3@liferay.com', 'Welcome Test HKG 3!', 'Test', '', 'HKG 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (953, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (954, 1, 952, 12, 952, 0, 0, '954', '/hkg3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (955, 1, 954, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (956, 1, 954, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (952, 18);

insert into Users_Orgs (userId, organizationId) values (952, 21);
insert into Users_Orgs (userId, organizationId) values (952, 79);

insert into Users_Roles values (952, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (957, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 958, 'test', 'F', 'F', 'hkg4', 'test.hkg.4@liferay.com', 'Welcome Test HKG 4!', 'Test', '', 'HKG 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (958, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (959, 1, 957, 12, 957, 0, 0, '959', '/hkg4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (960, 1, 959, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (961, 1, 959, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (957, 18);

insert into Users_Orgs (userId, organizationId) values (957, 21);
insert into Users_Orgs (userId, organizationId) values (957, 79);

insert into Users_Roles values (957, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (962, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 963, 'test', 'F', 'F', 'hkg5', 'test.hkg.5@liferay.com', 'Welcome Test HKG 5!', 'Test', '', 'HKG 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (963, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (964, 1, 962, 12, 962, 0, 0, '964', '/hkg5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (965, 1, 964, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (966, 1, 964, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (962, 18);

insert into Users_Orgs (userId, organizationId) values (962, 21);
insert into Users_Orgs (userId, organizationId) values (962, 79);

insert into Users_Roles values (962, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (967, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 968, 'test', 'F', 'F', 'hkg6', 'test.hkg.6@liferay.com', 'Welcome Test HKG 6!', 'Test', '', 'HKG 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (968, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (969, 1, 967, 12, 967, 0, 0, '969', '/hkg6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (970, 1, 969, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (971, 1, 969, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (967, 18);

insert into Users_Orgs (userId, organizationId) values (967, 21);
insert into Users_Orgs (userId, organizationId) values (967, 79);

insert into Users_Roles values (967, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (972, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 973, 'test', 'F', 'F', 'hkg7', 'test.hkg.7@liferay.com', 'Welcome Test HKG 7!', 'Test', '', 'HKG 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (973, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (974, 1, 972, 12, 972, 0, 0, '974', '/hkg7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (975, 1, 974, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (976, 1, 974, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (972, 18);

insert into Users_Orgs (userId, organizationId) values (972, 21);
insert into Users_Orgs (userId, organizationId) values (972, 79);

insert into Users_Roles values (972, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (977, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 978, 'test', 'F', 'F', 'hkg8', 'test.hkg.8@liferay.com', 'Welcome Test HKG 8!', 'Test', '', 'HKG 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (978, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (979, 1, 977, 12, 977, 0, 0, '979', '/hkg8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (980, 1, 979, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (981, 1, 979, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (977, 18);

insert into Users_Orgs (userId, organizationId) values (977, 21);
insert into Users_Orgs (userId, organizationId) values (977, 79);

insert into Users_Roles values (977, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (982, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 983, 'test', 'F', 'F', 'hkg9', 'test.hkg.9@liferay.com', 'Welcome Test HKG 9!', 'Test', '', 'HKG 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (983, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (984, 1, 982, 12, 982, 0, 0, '984', '/hkg9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (985, 1, 984, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (986, 1, 984, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (982, 18);

insert into Users_Orgs (userId, organizationId) values (982, 21);
insert into Users_Orgs (userId, organizationId) values (982, 79);

insert into Users_Roles values (982, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (987, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 988, 'test', 'F', 'F', 'hkg10', 'test.hkg.10@liferay.com', 'Welcome Test HKG 10!', 'Test', '', 'HKG 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (988, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'HKG 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (989, 1, 987, 12, 987, 0, 0, '989', '/hkg10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (990, 1, 989, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (991, 1, 989, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (987, 18);

insert into Users_Orgs (userId, organizationId) values (987, 21);
insert into Users_Orgs (userId, organizationId) values (987, 79);

insert into Users_Roles values (987, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (992, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 993, 'test', 'F', 'F', 'kul1', 'test.kul.1@liferay.com', 'Welcome Test KUL 1!', 'Test', '', 'KUL 1', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (993, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 1', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (994, 1, 992, 12, 992, 0, 0, '994', '/kul1', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (995, 1, 994, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (996, 1, 994, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (992, 18);

insert into Users_Orgs (userId, organizationId) values (992, 21);
insert into Users_Orgs (userId, organizationId) values (992, 83);

insert into Users_Roles values (992, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (997, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 998, 'test', 'F', 'F', 'kul2', 'test.kul.2@liferay.com', 'Welcome Test KUL 2!', 'Test', '', 'KUL 2', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (998, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 2', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (999, 1, 997, 12, 997, 0, 0, '999', '/kul2', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1000, 1, 999, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1001, 1, 999, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (997, 18);

insert into Users_Orgs (userId, organizationId) values (997, 21);
insert into Users_Orgs (userId, organizationId) values (997, 83);

insert into Users_Roles values (997, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1002, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1003, 'test', 'F', 'F', 'kul3', 'test.kul.3@liferay.com', 'Welcome Test KUL 3!', 'Test', '', 'KUL 3', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1003, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 3', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1004, 1, 1002, 12, 1002, 0, 0, '1004', '/kul3', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1005, 1, 1004, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1006, 1, 1004, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1002, 18);

insert into Users_Orgs (userId, organizationId) values (1002, 21);
insert into Users_Orgs (userId, organizationId) values (1002, 83);

insert into Users_Roles values (1002, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1007, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1008, 'test', 'F', 'F', 'kul4', 'test.kul.4@liferay.com', 'Welcome Test KUL 4!', 'Test', '', 'KUL 4', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1008, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 4', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1009, 1, 1007, 12, 1007, 0, 0, '1009', '/kul4', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1010, 1, 1009, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1011, 1, 1009, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1007, 18);

insert into Users_Orgs (userId, organizationId) values (1007, 21);
insert into Users_Orgs (userId, organizationId) values (1007, 83);

insert into Users_Roles values (1007, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1012, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1013, 'test', 'F', 'F', 'kul5', 'test.kul.5@liferay.com', 'Welcome Test KUL 5!', 'Test', '', 'KUL 5', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1013, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 5', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1014, 1, 1012, 12, 1012, 0, 0, '1014', '/kul5', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1015, 1, 1014, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1016, 1, 1014, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1012, 18);

insert into Users_Orgs (userId, organizationId) values (1012, 21);
insert into Users_Orgs (userId, organizationId) values (1012, 83);

insert into Users_Roles values (1012, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1017, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1018, 'test', 'F', 'F', 'kul6', 'test.kul.6@liferay.com', 'Welcome Test KUL 6!', 'Test', '', 'KUL 6', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1018, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 6', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1019, 1, 1017, 12, 1017, 0, 0, '1019', '/kul6', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1020, 1, 1019, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1021, 1, 1019, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1017, 18);

insert into Users_Orgs (userId, organizationId) values (1017, 21);
insert into Users_Orgs (userId, organizationId) values (1017, 83);

insert into Users_Roles values (1017, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1022, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1023, 'test', 'F', 'F', 'kul7', 'test.kul.7@liferay.com', 'Welcome Test KUL 7!', 'Test', '', 'KUL 7', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1023, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 7', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1024, 1, 1022, 12, 1022, 0, 0, '1024', '/kul7', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1025, 1, 1024, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1026, 1, 1024, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1022, 18);

insert into Users_Orgs (userId, organizationId) values (1022, 21);
insert into Users_Orgs (userId, organizationId) values (1022, 83);

insert into Users_Roles values (1022, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1027, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1028, 'test', 'F', 'F', 'kul8', 'test.kul.8@liferay.com', 'Welcome Test KUL 8!', 'Test', '', 'KUL 8', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1028, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 8', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1029, 1, 1027, 12, 1027, 0, 0, '1029', '/kul8', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1030, 1, 1029, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1031, 1, 1029, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1027, 18);

insert into Users_Orgs (userId, organizationId) values (1027, 21);
insert into Users_Orgs (userId, organizationId) values (1027, 83);

insert into Users_Roles values (1027, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1032, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1033, 'test', 'F', 'F', 'kul9', 'test.kul.9@liferay.com', 'Welcome Test KUL 9!', 'Test', '', 'KUL 9', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1033, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 9', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1034, 1, 1032, 12, 1032, 0, 0, '1034', '/kul9', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1035, 1, 1034, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1036, 1, 1034, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1032, 18);

insert into Users_Orgs (userId, organizationId) values (1032, 21);
insert into Users_Orgs (userId, organizationId) values (1032, 83);

insert into Users_Roles values (1032, 17);

insert into User_ (userId, companyId, createDate, modifiedDate, defaultUser, contactId, password_, passwordEncrypted, passwordReset, screenName, emailAddress, greeting, firstName, middleName, lastName, loginDate, failedLoginAttempts, agreedToTermsOfUse, active_) values (1037, 1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 'F', 1038, 'test', 'F', 'F', 'kul10', 'test.kul.10@liferay.com', 'Welcome Test KUL 10!', 'Test', '', 'KUL 10', CURRENT YEAR TO FRACTION, 0, 'T', 'T');
insert into Contact_ (contactId, companyId, userId, userName, createDate, modifiedDate, accountId, parentContactId, firstName, middleName, lastName, male, birthday) values (1038, 1, 2, 'Joe Bloggs', CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 7, 0, 'Test', '', 'KUL 10', 'T', '1970-01-01 00:00:00.0');

insert into Group_ (groupId, companyId, creatorUserId, classNameId, classPK, parentGroupId, liveGroupId, name, friendlyURL, active_) values (1039, 1, 1037, 12, 1037, 0, 0, '1039', '/kul10', 'T');
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1040, 1, 1039, 'T', 'F', 'classic', '01', 0);
insert into LayoutSet (layoutSetId, companyId, groupId, privateLayout, logo, themeId, colorSchemeId, pageCount) values (1041, 1, 1039, 'F', 'F', 'classic', '01', 0);

insert into Users_Groups values (1037, 18);

insert into Users_Orgs (userId, organizationId) values (1037, 21);
insert into Users_Orgs (userId, organizationId) values (1037, 83);

insert into Users_Roles values (1037, 17);












insert into Release_ (releaseId, createDate, modifiedDate, buildNumber, verified) values (1, CURRENT YEAR TO FRACTION, CURRENT YEAR TO FRACTION, 5205, 'F');


create table QUARTZ_JOB_DETAILS (
	JOB_NAME varchar(80) not null,
	JOB_GROUP varchar(80) not null,
	DESCRIPTION varchar(120),
	JOB_CLASS_NAME varchar(128) not null,
	IS_DURABLE boolean not null,
	IS_VOLATILE boolean not null,
	IS_STATEFUL boolean not null,
	REQUESTS_RECOVERY boolean not null,
	JOB_DATA byte in table,
	primary key (JOB_NAME, JOB_GROUP)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_JOB_LISTENERS (
	JOB_NAME varchar(80) not null,
	JOB_GROUP varchar(80) not null,
	JOB_LISTENER varchar(80) not null,
	primary key (JOB_NAME, JOB_GROUP, JOB_LISTENER)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_TRIGGERS (
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	JOB_NAME varchar(80) not null,
	JOB_GROUP varchar(80) not null,
	IS_VOLATILE boolean not null,
	DESCRIPTION varchar(120),
	NEXT_FIRE_TIME int8,
	PREV_FIRE_TIME int8,
	PRIORITY int,
	TRIGGER_STATE varchar(16) not null,
	TRIGGER_TYPE varchar(8) not null,
	START_TIME int8 not null,
	END_TIME int8,
	CALENDAR_NAME varchar(80),
	MISFIRE_INSTR int,
	JOB_DATA byte in table,
	primary key (TRIGGER_NAME, TRIGGER_GROUP)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_SIMPLE_TRIGGERS (
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	REPEAT_COUNT int8 not null,
	REPEAT_INTERVAL int8 not null,
	TIMES_TRIGGERED int8 not null,
	primary key (TRIGGER_NAME, TRIGGER_GROUP)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_CRON_TRIGGERS (
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	CRON_EXPRESSION varchar(80) not null,
	TIME_ZONE_ID varchar(80),
	primary key (TRIGGER_NAME, TRIGGER_GROUP)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_BLOB_TRIGGERS (
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	BLOB_DATA byte in table,
	primary key (TRIGGER_NAME, TRIGGER_GROUP)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_TRIGGER_LISTENERS (
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	TRIGGER_LISTENER varchar(80) not null,
	primary key (TRIGGER_NAME, TRIGGER_GROUP, TRIGGER_LISTENER)
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_CALENDARS (
	CALENDAR_NAME varchar(80) not null primary key,
	CALENDAR byte in table not null
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_PAUSED_TRIGGER_GRPS (
	TRIGGER_GROUP varchar(80) not null primary key
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_FIRED_TRIGGERS (
	ENTRY_ID varchar(95) not null primary key,
	TRIGGER_NAME varchar(80) not null,
	TRIGGER_GROUP varchar(80) not null,
	IS_VOLATILE boolean not null,
	INSTANCE_NAME varchar(80) not null,
	FIRED_TIME int8 not null,
	PRIORITY int not null,
	STATE varchar(16) not null,
	JOB_NAME varchar(80),
	JOB_GROUP varchar(80),
	IS_STATEFUL boolean,
	REQUESTS_RECOVERY boolean
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_SCHEDULER_STATE (
	INSTANCE_NAME varchar(80) not null primary key,
	LAST_CHECKIN_TIME int8 not null,
	CHECKIN_INTERVAL int8 not null
)
extent size 16 next size 16
lock mode row;

create table QUARTZ_LOCKS (
	LOCK_NAME varchar(40) not null primary key
)
extent size 16 next size 16
lock mode row;



insert into QUARTZ_LOCKS values('TRIGGER_ACCESS');
insert into QUARTZ_LOCKS values('JOB_ACCESS');
insert into QUARTZ_LOCKS values('CALENDAR_ACCESS');
insert into QUARTZ_LOCKS values('STATE_ACCESS');
insert into QUARTZ_LOCKS values('MISFIRE_ACCESS');

create index IX_F7655CC3 on QUARTZ_TRIGGERS (NEXT_FIRE_TIME);
create index IX_9955EFB5 on QUARTZ_TRIGGERS (TRIGGER_STATE);
create index IX_8040C593 on QUARTZ_TRIGGERS (TRIGGER_STATE, NEXT_FIRE_TIME);
create index IX_804154AF on QUARTZ_FIRED_TRIGGERS (INSTANCE_NAME);
create index IX_BAB9A1F7 on QUARTZ_FIRED_TRIGGERS (JOB_GROUP);
create index IX_ADEE6A17 on QUARTZ_FIRED_TRIGGERS (JOB_NAME);
create index IX_64B194F2 on QUARTZ_FIRED_TRIGGERS (TRIGGER_GROUP);
create index IX_5FEABBC on QUARTZ_FIRED_TRIGGERS (TRIGGER_NAME);
create index IX_20D8706C on QUARTZ_FIRED_TRIGGERS (TRIGGER_NAME, TRIGGER_GROUP);






create index IX_93D5AD4E on Address (companyId);
create index IX_ABD7DAC0 on Address (companyId, classNameId);
create index IX_71CB1123 on Address (companyId, classNameId, classPK);
create index IX_923BD178 on Address (companyId, classNameId, classPK, mailing);
create index IX_9226DBB4 on Address (companyId, classNameId, classPK, primary_);
create index IX_5BC8B0D4 on Address (userId);

create index IX_6EDB9600 on AnnouncementsDelivery (userId);
create unique index IX_BA4413D5 on AnnouncementsDelivery (userId, type_);

create index IX_A6EF0B81 on AnnouncementsEntry (classNameId, classPK);
create index IX_14F06A6B on AnnouncementsEntry (classNameId, classPK, alert);
create index IX_D49C2E66 on AnnouncementsEntry (userId);
create index IX_1AFBDE08 on AnnouncementsEntry (uuid_);

create index IX_9C7EB9F on AnnouncementsFlag (entryId);
create unique index IX_4539A99C on AnnouncementsFlag (userId, entryId, value);

create index IX_72EF6041 on BlogsEntry (companyId);
create index IX_E0D90212 on BlogsEntry (companyId, displayDate, draft);
create index IX_81A50303 on BlogsEntry (groupId);
create index IX_DA53AFD4 on BlogsEntry (groupId, displayDate, draft);
create unique index IX_DB780A20 on BlogsEntry (groupId, urlTitle);
create index IX_C07CA83D on BlogsEntry (groupId, userId);
create index IX_B88E740E on BlogsEntry (groupId, userId, displayDate, draft);
create index IX_69157A4D on BlogsEntry (uuid_);
create unique index IX_1B1040FD on BlogsEntry (uuid_, groupId);

create index IX_90CDA39A on BlogsStatsUser (companyId, entryCount);
create index IX_43840EEB on BlogsStatsUser (groupId);
create index IX_28C78D5C on BlogsStatsUser (groupId, entryCount);
create unique index IX_82254C25 on BlogsStatsUser (groupId, userId);
create index IX_BB51F1D9 on BlogsStatsUser (userId);

create index IX_443BDC38 on BookmarksEntry (folderId);
create index IX_E52FF7EF on BookmarksEntry (groupId);
create index IX_E2E9F129 on BookmarksEntry (groupId, userId);
create index IX_B670BA39 on BookmarksEntry (uuid_);
create unique index IX_EAA02A91 on BookmarksEntry (uuid_, groupId);

create index IX_2ABA25D7 on BookmarksFolder (companyId);
create index IX_7F703619 on BookmarksFolder (groupId);
create index IX_967799C0 on BookmarksFolder (groupId, parentFolderId);
create index IX_451E7AE3 on BookmarksFolder (uuid_);
create unique index IX_DC2F8927 on BookmarksFolder (uuid_, groupId);

create unique index IX_E7B95510 on BrowserTracker (userId);

create index IX_D6FD9496 on CalEvent (companyId);
create index IX_12EE4898 on CalEvent (groupId);
create index IX_4FDDD2BF on CalEvent (groupId, repeating);
create index IX_FCD7C63D on CalEvent (groupId, type_);
create index IX_F6006202 on CalEvent (remindBy);
create index IX_C1AD2122 on CalEvent (uuid_);
create unique index IX_5CCE79C8 on CalEvent (uuid_, groupId);

create unique index IX_B27A301F on ClassName_ (value);

create index IX_38EFE3FD on Company (logoId);
create index IX_12566EC2 on Company (mx);
create index IX_35E3E7C6 on Company (system);
create unique index IX_975996C0 on Company (virtualHost);
create unique index IX_EC00543C on Company (webId);

create index IX_66D496A3 on Contact_ (companyId);

create unique index IX_717B97E1 on Country (a2);
create unique index IX_717B9BA2 on Country (a3);
create index IX_25D734CD on Country (active_);
create unique index IX_19DA007B on Country (name);

create index IX_4CB1B2B4 on DLFileEntry (companyId);
create index IX_24A846D1 on DLFileEntry (folderId);
create unique index IX_8F6C75D0 on DLFileEntry (folderId, name);
create index IX_A9951F17 on DLFileEntry (folderId, title);
create index IX_F4AF5636 on DLFileEntry (groupId);
create index IX_43261870 on DLFileEntry (groupId, userId);
create index IX_64F0FE40 on DLFileEntry (uuid_);
create unique index IX_BC2E7E6A on DLFileEntry (uuid_, groupId);

create unique index IX_CE705D48 on DLFileRank (companyId, userId, folderId, name);
create index IX_40B56512 on DLFileRank (folderId, name);
create index IX_BAFB116E on DLFileRank (groupId, userId);
create index IX_EED06670 on DLFileRank (userId);

create index IX_E56EC6AD on DLFileShortcut (folderId);
create index IX_CA2708A2 on DLFileShortcut (toFolderId, toName);
create index IX_4831EBE4 on DLFileShortcut (uuid_);
create unique index IX_FDB4A946 on DLFileShortcut (uuid_, groupId);

create index IX_9CD91DB6 on DLFileVersion (folderId, name);
create unique index IX_6C5E6512 on DLFileVersion (folderId, name, version);

create index IX_A74DB14C on DLFolder (companyId);
create index IX_F2EA1ACE on DLFolder (groupId);
create index IX_49C37475 on DLFolder (groupId, parentFolderId);
create unique index IX_902FD874 on DLFolder (groupId, parentFolderId, name);
create index IX_51556082 on DLFolder (parentFolderId, name);
create index IX_CBC408D8 on DLFolder (uuid_);
create unique index IX_3CC1DED2 on DLFolder (uuid_, groupId);

create index IX_1BB072CA on EmailAddress (companyId);
create index IX_49D2DEC4 on EmailAddress (companyId, classNameId);
create index IX_551A519F on EmailAddress (companyId, classNameId, classPK);
create index IX_2A2CB130 on EmailAddress (companyId, classNameId, classPK, primary_);
create index IX_7B43CD8 on EmailAddress (userId);

create index IX_A8C0CBE8 on ExpandoColumn (tableId);
create unique index IX_FEFC8DA7 on ExpandoColumn (tableId, name);

create index IX_D3F5D7AE on ExpandoRow (tableId);
create unique index IX_81EFBFF5 on ExpandoRow (tableId, classPK);

create index IX_B5AE8A85 on ExpandoTable (companyId, classNameId);
create unique index IX_37562284 on ExpandoTable (companyId, classNameId, name);

create index IX_B29FEF17 on ExpandoValue (classNameId, classPK);
create index IX_F7DD0987 on ExpandoValue (columnId);
create unique index IX_9DDD21E5 on ExpandoValue (columnId, rowId_);
create index IX_9112A7A0 on ExpandoValue (rowId_);
create index IX_F0566A77 on ExpandoValue (tableId);
create index IX_1BD3F4C on ExpandoValue (tableId, classPK);
create index IX_CA9AFB7C on ExpandoValue (tableId, columnId);
create unique index IX_D27B03E7 on ExpandoValue (tableId, columnId, classPK);
create index IX_B71E92D5 on ExpandoValue (tableId, rowId_);

create unique index IX_D0D5E397 on Group_ (companyId, classNameId, classPK);
create unique index IX_5DE0BE11 on Group_ (companyId, classNameId, liveGroupId, name);
create unique index IX_5BDDB872 on Group_ (companyId, friendlyURL);
create unique index IX_BBCA55B on Group_ (companyId, liveGroupId, name);
create unique index IX_5AA68501 on Group_ (companyId, name);
create index IX_16218A38 on Group_ (liveGroupId);
create index IX_7B590A7A on Group_ (type_, active_);

create index IX_75267DCA on Groups_Orgs (groupId);
create index IX_6BBB7682 on Groups_Orgs (organizationId);

create index IX_C48736B on Groups_Permissions (groupId);
create index IX_EC97689D on Groups_Permissions (permissionId);

create index IX_84471FD2 on Groups_Roles (groupId);
create index IX_3103EF3D on Groups_Roles (roleId);

create index IX_31FB749A on Groups_UserGroups (groupId);
create index IX_3B69160F on Groups_UserGroups (userGroupId);

create index IX_60214CF6 on IGFolder (companyId);
create index IX_206498F8 on IGFolder (groupId);
create index IX_1A605E9F on IGFolder (groupId, parentFolderId);
create unique index IX_9BBAFB1E on IGFolder (groupId, parentFolderId, name);
create index IX_F73C0982 on IGFolder (uuid_);
create unique index IX_B10EFD68 on IGFolder (uuid_, groupId);

create index IX_E597322D on IGImage (custom1ImageId);
create index IX_D9E0A34C on IGImage (custom2ImageId);
create index IX_4438CA80 on IGImage (folderId);
create index IX_BCB13A3F on IGImage (folderId, name);
create index IX_63820A7 on IGImage (groupId);
create index IX_BE79E1E1 on IGImage (groupId, userId);
create index IX_64F0B572 on IGImage (largeImageId);
create index IX_D3D32126 on IGImage (smallImageId);
create index IX_265BB0F1 on IGImage (uuid_);
create unique index IX_E97342D9 on IGImage (uuid_, groupId);

create index IX_6A925A4D on Image (size_);

create index IX_3D3BB5E9 on Item (code_);

create index IX_DFF98523 on JournalArticle (companyId);
create index IX_9356F865 on JournalArticle (groupId);
create index IX_68C0F69C on JournalArticle (groupId, articleId);
create index IX_8DBF1387 on JournalArticle (groupId, articleId, approved);
create unique index IX_85C52EEC on JournalArticle (groupId, articleId, version);
create index IX_2E207659 on JournalArticle (groupId, structureId);
create index IX_8DEAE14E on JournalArticle (groupId, templateId);
create index IX_22882D02 on JournalArticle (groupId, urlTitle);
create index IX_76186981 on JournalArticle (resourcePrimKey, approved);
create index IX_EF9B7028 on JournalArticle (smallImageId);
create index IX_F029602F on JournalArticle (uuid_);
create unique index IX_3463D95B on JournalArticle (uuid_, groupId);

create index IX_3B51BB68 on JournalArticleImage (groupId);
create index IX_158B526F on JournalArticleImage (groupId, articleId, version);
create unique index IX_103D6207 on JournalArticleImage (groupId, articleId, version, elInstanceId, elName, languageId);
create index IX_D4121315 on JournalArticleImage (tempImage);

create index IX_F8433677 on JournalArticleResource (groupId);
create unique index IX_88DF994A on JournalArticleResource (groupId, articleId);

create index IX_6838E427 on JournalContentSearch (groupId, articleId);
create index IX_20962903 on JournalContentSearch (groupId, privateLayout);
create index IX_7CC7D73E on JournalContentSearch (groupId, privateLayout, articleId);
create index IX_B3B318DC on JournalContentSearch (groupId, privateLayout, layoutId);
create index IX_7ACC74C9 on JournalContentSearch (groupId, privateLayout, layoutId, portletId);
create unique index IX_C3AA93B8 on JournalContentSearch (groupId, privateLayout, layoutId, portletId, articleId);

create index IX_35A2DB2F on JournalFeed (groupId);
create unique index IX_65576CBC on JournalFeed (groupId, feedId);
create index IX_50C36D79 on JournalFeed (uuid_);
create unique index IX_39031F51 on JournalFeed (uuid_, groupId);

create index IX_B97F5608 on JournalStructure (groupId);
create index IX_CA0BD48C on JournalStructure (groupId, parentStructureId);
create unique index IX_AB6E9996 on JournalStructure (groupId, structureId);
create index IX_8831E4FC on JournalStructure (structureId);
create index IX_6702CA92 on JournalStructure (uuid_);
create unique index IX_42E86E58 on JournalStructure (uuid_, groupId);

create index IX_77923653 on JournalTemplate (groupId);
create index IX_1701CB2B on JournalTemplate (groupId, structureId);
create unique index IX_E802AA3C on JournalTemplate (groupId, templateId);
create index IX_25FFB6FA on JournalTemplate (smallImageId);
create index IX_1B12CA20 on JournalTemplate (templateId);
create index IX_2857419D on JournalTemplate (uuid_);
create unique index IX_62D1B3AD on JournalTemplate (uuid_, groupId);

create index IX_C7FBC998 on Layout (companyId);
create index IX_FAD05595 on Layout (dlFolderId);
create index IX_C099D61A on Layout (groupId);
create index IX_705F5AA3 on Layout (groupId, privateLayout);
create unique index IX_BC2C4231 on Layout (groupId, privateLayout, friendlyURL);
create unique index IX_7162C27C on Layout (groupId, privateLayout, layoutId);
create index IX_6DE88B06 on Layout (groupId, privateLayout, parentLayoutId);
create index IX_1A1B61D2 on Layout (groupId, privateLayout, type_);
create index IX_23922F7D on Layout (iconImageId);

create index IX_A40B8BEC on LayoutSet (groupId);
create unique index IX_48550691 on LayoutSet (groupId, privateLayout);
create index IX_5ABC2905 on LayoutSet (virtualHost);

create index IX_2932DD37 on ListType (type_);

create index IX_850E17AB on Lot (code_);
create unique index IX_C8D24C13 on Lot (lotNumber);
create unique index IX_1F8E7F0F on Lot (number_);
create index IX_892CB147 on Lot (participantId);
create index IX_8426D7DC on Lot (partnerId);
create index IX_42E61160 on Lot (reservationId);

create index IX_9B40CB8C on LotReservation (approved);

create index IX_69951A25 on MBBan (banUserId);
create index IX_5C3FF12A on MBBan (groupId);
create unique index IX_8ABC4E3B on MBBan (groupId, banUserId);
create index IX_48814BBA on MBBan (userId);

create index IX_BC735DCF on MBCategory (companyId);
create index IX_BB870C11 on MBCategory (groupId);
create index IX_ED292508 on MBCategory (groupId, parentCategoryId);
create index IX_C2626EDB on MBCategory (uuid_);
create unique index IX_F7D28C2F on MBCategory (uuid_, groupId);

create index IX_79D0120B on MBDiscussion (classNameId);
create unique index IX_33A4DE38 on MBDiscussion (classNameId, classPK);
create unique index IX_B5CA2DC on MBDiscussion (threadId);

create index IX_BFEB984F on MBMailingList (active_);
create unique index IX_ADA16FE7 on MBMailingList (categoryId);
create index IX_4115EC7A on MBMailingList (uuid_);
create unique index IX_E858F170 on MBMailingList (uuid_, groupId);

create index IX_3C865EE5 on MBMessage (categoryId);
create index IX_138C7F1E on MBMessage (categoryId, threadId);
create index IX_51A8D44D on MBMessage (classNameId, classPK);
create index IX_B1432D30 on MBMessage (companyId);
create index IX_5B153FB2 on MBMessage (groupId);
create index IX_8EB8C5EC on MBMessage (groupId, userId);
create index IX_75B95071 on MBMessage (threadId);
create index IX_A7038CD7 on MBMessage (threadId, parentMessageId);
create index IX_C57B16BC on MBMessage (uuid_);
create unique index IX_8D12316E on MBMessage (uuid_, groupId);

create index IX_D180D4AE on MBMessageFlag (messageId);
create index IX_A6973A8E on MBMessageFlag (messageId, flag);
create index IX_C1C9A8FD on MBMessageFlag (threadId);
create index IX_3CFD579D on MBMessageFlag (threadId, flag);
create index IX_7B2917BE on MBMessageFlag (userId);
create unique index IX_E9EB6194 on MBMessageFlag (userId, messageId, flag);
create index IX_2EA537D7 on MBMessageFlag (userId, threadId, flag);

create index IX_A00A898F on MBStatsUser (groupId);
create index IX_FAB5A88B on MBStatsUser (groupId, messageCount);
create unique index IX_9168E2C9 on MBStatsUser (groupId, userId);
create index IX_847F92B5 on MBStatsUser (userId);

create index IX_CB854772 on MBThread (categoryId);
create index IX_19D8B60A on MBThread (categoryId, lastPostDate);
create index IX_95C0EA45 on MBThread (groupId);

create index IX_8A1CC4B on MembershipRequest (groupId);
create index IX_C28C72EC on MembershipRequest (groupId, statusId);
create index IX_66D70879 on MembershipRequest (userId);

create index IX_A425F71A on OrgGroupPermission (groupId);
create index IX_6C53DA4E on OrgGroupPermission (permissionId);

create index IX_4A527DD3 on OrgGroupRole (groupId);
create index IX_AB044D1C on OrgGroupRole (roleId);

create index IX_6AF0D434 on OrgLabor (organizationId);

create index IX_834BCEB6 on Organization_ (companyId);
create unique index IX_E301BDF5 on Organization_ (companyId, name);
create index IX_418E4522 on Organization_ (companyId, parentOrganizationId);

create unique index IX_89ABCC18 on Participant (mobileNumber);

create index IX_576905D1 on Participant_Lots (number);
create index IX_A12E10A on Participant_Lots (participantId);

create index IX_589BCD60 on Partner (category);

create index IX_2C1142E on PasswordPolicy (companyId, defaultPolicy);
create unique index IX_3FBFA9F4 on PasswordPolicy (companyId, name);

create index IX_C3A17327 on PasswordPolicyRel (classNameId, classPK);
create index IX_ED7CF243 on PasswordPolicyRel (passwordPolicyId, classNameId, classPK);

create index IX_326F75BD on PasswordTracker (userId);

create unique index IX_4D19C2B8 on Permission_ (actionId, resourceId);
create index IX_F090C113 on Permission_ (resourceId);

create index IX_9F704A14 on Phone (companyId);
create index IX_A2E4AFBA on Phone (companyId, classNameId);
create index IX_9A53569 on Phone (companyId, classNameId, classPK);
create index IX_812CE07A on Phone (companyId, classNameId, classPK, primary_);
create index IX_F202B9CE on Phone (userId);

create index IX_B9746445 on PluginSetting (companyId);
create unique index IX_7171B2E8 on PluginSetting (companyId, pluginId, pluginType);

create index IX_EC370F10 on PollsChoice (questionId);
create unique index IX_D76DD2CF on PollsChoice (questionId, name);
create index IX_6660B399 on PollsChoice (uuid_);

create index IX_9FF342EA on PollsQuestion (groupId);
create index IX_51F087F4 on PollsQuestion (uuid_);
create unique index IX_F3C9F36 on PollsQuestion (uuid_, groupId);

create index IX_D5DF7B54 on PollsVote (choiceId);
create index IX_12112599 on PollsVote (questionId);
create unique index IX_1BBFD4D3 on PollsVote (questionId, userId);

create index IX_80CC9508 on Portlet (companyId);
create unique index IX_12B5E51D on Portlet (companyId, portletId);

create index IX_96BDD537 on PortletItem (groupId, classNameId);
create index IX_D699243F on PortletItem (groupId, name, portletId, classNameId);
create index IX_2C61314E on PortletItem (groupId, portletId);
create index IX_E922D6C0 on PortletItem (groupId, portletId, classNameId);
create index IX_8E71167F on PortletItem (groupId, portletId, classNameId, name);
create index IX_33B8CE8D on PortletItem (groupId, portletId, name);

create index IX_E4F13E6E on PortletPreferences (ownerId, ownerType, plid);
create unique index IX_C7057FF7 on PortletPreferences (ownerId, ownerType, plid, portletId);
create index IX_F15C1C4F on PortletPreferences (plid);
create index IX_D340DB76 on PortletPreferences (plid, portletId);

create index IX_16184D57 on RatingsEntry (classNameId, classPK);
create unique index IX_B47E3C11 on RatingsEntry (userId, classNameId, classPK);

create unique index IX_A6E99284 on RatingsStats (classNameId, classPK);

create index IX_2D9A426F on Region (active_);
create index IX_16D87CA7 on Region (countryId);
create index IX_11FB3E42 on Region (countryId, active_);

create index IX_81F2DB09 on ResourceAction (name);
create unique index IX_EDB9986E on ResourceAction (name, actionId);

create index IX_717FDD47 on ResourceCode (companyId);
create unique index IX_A32C097E on ResourceCode (companyId, name, scope);
create index IX_AACAFF40 on ResourceCode (name);

create index IX_60B99860 on ResourcePermission (companyId, name, scope);
create index IX_2200AA69 on ResourcePermission (companyId, name, scope, primKey);
create unique index IX_8D83D0CE on ResourcePermission (companyId, name, scope, primKey, roleId);
create index IX_A37A0588 on ResourcePermission (roleId);

create index IX_2578FBD3 on Resource_ (codeId);
create unique index IX_67DE7856 on Resource_ (codeId, primKey);

create index IX_449A10B9 on Role_ (companyId);
create unique index IX_A88E424E on Role_ (companyId, classNameId, classPK);
create unique index IX_EBC931B8 on Role_ (companyId, name);
create index IX_5EB4E2FB on Role_ (subtype);
create index IX_CBE204 on Role_ (type_, subtype);

create index IX_7A3619C6 on Roles_Permissions (permissionId);
create index IX_E04E486D on Roles_Permissions (roleId);

create index IX_3BB93ECA on SCFrameworkVersi_SCProductVers (frameworkVersionId);
create index IX_E8D33FF9 on SCFrameworkVersi_SCProductVers (productVersionId);

create index IX_C98C0D78 on SCFrameworkVersion (companyId);
create index IX_272991FA on SCFrameworkVersion (groupId);
create index IX_6E1764F on SCFrameworkVersion (groupId, active_);

create index IX_1C841592 on SCLicense (active_);
create index IX_5327BB79 on SCLicense (active_, recommended);

create index IX_27006638 on SCLicenses_SCProductEntries (licenseId);
create index IX_D7710A66 on SCLicenses_SCProductEntries (productEntryId);

create index IX_5D25244F on SCProductEntry (companyId);
create index IX_72F87291 on SCProductEntry (groupId);
create index IX_98E6A9CB on SCProductEntry (groupId, userId);
create index IX_7311E812 on SCProductEntry (repoGroupId, repoArtifactId);

create index IX_AE8224CC on SCProductScreenshot (fullImageId);
create index IX_467956FD on SCProductScreenshot (productEntryId);
create index IX_DA913A55 on SCProductScreenshot (productEntryId, priority);
create index IX_6C572DAC on SCProductScreenshot (thumbnailId);

create index IX_7020130F on SCProductVersion (directDownloadURL);
create index IX_8377A211 on SCProductVersion (productEntryId);

create index IX_7338606F on ServiceComponent (buildNamespace);
create unique index IX_4F0315B8 on ServiceComponent (buildNamespace, buildNumber);

create index IX_DA5F4359 on Shard (classNameId, classPK);
create index IX_941BA8C3 on Shard (name);

create index IX_C28B41DC on ShoppingCart (groupId);
create unique index IX_FC46FE16 on ShoppingCart (groupId, userId);
create index IX_54101CC8 on ShoppingCart (userId);

create index IX_5F615D3E on ShoppingCategory (groupId);
create index IX_1E6464F5 on ShoppingCategory (groupId, parentCategoryId);

create unique index IX_DC60CFAE on ShoppingCoupon (code_);
create index IX_3251AF16 on ShoppingCoupon (groupId);

create index IX_C8EACF2E on ShoppingItem (categoryId);
create unique index IX_1C717CA6 on ShoppingItem (companyId, sku);
create index IX_903DC750 on ShoppingItem (largeImageId);
create index IX_D217AB30 on ShoppingItem (mediumImageId);
create index IX_FF203304 on ShoppingItem (smallImageId);

create index IX_6D5F9B87 on ShoppingItemField (itemId);

create index IX_EA6FD516 on ShoppingItemPrice (itemId);

create index IX_1D15553E on ShoppingOrder (groupId);
create index IX_119B5630 on ShoppingOrder (groupId, userId, ppPaymentStatus);
create unique index IX_D7D6E87A on ShoppingOrder (number_);
create index IX_F474FD89 on ShoppingOrder (ppTxnId);

create index IX_B5F82C7A on ShoppingOrderItem (orderId);

create index IX_82E39A0C on SocialActivity (classNameId);
create index IX_A853C757 on SocialActivity (classNameId, classPK);
create index IX_64B1BC66 on SocialActivity (companyId);
create index IX_2A2468 on SocialActivity (groupId);
create unique index IX_8F32DEC9 on SocialActivity (groupId, userId, createDate, classNameId, classPK, type_, receiverUserId);
create index IX_1271F25F on SocialActivity (mirrorActivityId);
create index IX_1F00C374 on SocialActivity (mirrorActivityId, classNameId, classPK);
create index IX_121CA3CB on SocialActivity (receiverUserId);
create index IX_3504B8BC on SocialActivity (userId);

create index IX_61171E99 on SocialRelation (companyId);
create index IX_95135D1C on SocialRelation (companyId, type_);
create index IX_C31A64C6 on SocialRelation (type_);
create index IX_5A40CDCC on SocialRelation (userId1);
create index IX_4B52BE89 on SocialRelation (userId1, type_);
create unique index IX_12A92145 on SocialRelation (userId1, userId2, type_);
create index IX_5A40D18D on SocialRelation (userId2);
create index IX_3F9C2FA8 on SocialRelation (userId2, type_);
create index IX_F0CA24A5 on SocialRelation (uuid_);

create index IX_D3425487 on SocialRequest (classNameId, classPK, type_, receiverUserId, status);
create index IX_A90FE5A0 on SocialRequest (companyId);
create index IX_32292ED1 on SocialRequest (receiverUserId);
create index IX_D9380CB7 on SocialRequest (receiverUserId, status);
create index IX_80F7A9C2 on SocialRequest (userId);
create unique index IX_36A90CA7 on SocialRequest (userId, classNameId, classPK, type_, receiverUserId);
create index IX_CC86A444 on SocialRequest (userId, classNameId, classPK, type_, status);
create index IX_AB5906A8 on SocialRequest (userId, status);
create index IX_49D5872C on SocialRequest (uuid_);
create unique index IX_4F973EFE on SocialRequest (uuid_, groupId);

create index IX_786D171A on Subscription (companyId, classNameId, classPK);
create unique index IX_2E1A92D4 on Subscription (companyId, userId, classNameId, classPK);
create index IX_54243AFD on Subscription (userId);
create index IX_E8F34171 on Subscription (userId, classNameId);

create unique index IX_1AB6D6D2 on TagsAsset (classNameId, classPK);
create index IX_AB3D8BCB on TagsAsset (companyId);

create index IX_B22F3A1 on TagsAssets_TagsEntries (assetId);
create index IX_A02A8023 on TagsAssets_TagsEntries (entryId);

create index IX_EE55ED49 on TagsEntry (parentEntryId, vocabularyId);
create index IX_28E8954 on TagsEntry (vocabularyId);

create index IX_C134234 on TagsProperty (companyId);
create index IX_EB974D08 on TagsProperty (companyId, key_);
create index IX_5200A629 on TagsProperty (entryId);
create unique index IX_F505253D on TagsProperty (entryId, key_);

create index IX_E0D51848 on TagsVocabulary (companyId, folksonomy);
create index IX_9F26308A on TagsVocabulary (groupId, folksonomy);
create unique index IX_F9E51044 on TagsVocabulary (groupId, name);

create unique index IX_181A4A1B on TasksProposal (classNameId, classPK);
create index IX_7FB27324 on TasksProposal (groupId);
create index IX_6EEC675E on TasksProposal (groupId, userId);

create index IX_4D0C7F8D on TasksReview (proposalId);
create index IX_70AFEA01 on TasksReview (proposalId, stage);
create index IX_1894B29A on TasksReview (proposalId, stage, completed);
create index IX_41AFC20C on TasksReview (proposalId, stage, completed, rejected);
create index IX_36F512E6 on TasksReview (userId);
create unique index IX_5C6BE4C7 on TasksReview (userId, proposalId);

create index IX_524FEFCE on UserGroup (companyId);
create unique index IX_23EAD0D on UserGroup (companyId, name);
create index IX_69771487 on UserGroup (companyId, parentUserGroupId);

create index IX_CCBE4063 on UserGroupGroupRole (groupId);
create index IX_CAB0CCC8 on UserGroupGroupRole (groupId, roleId);
create index IX_1CDF88C on UserGroupGroupRole (roleId);
create index IX_DCDED558 on UserGroupGroupRole (userGroupId);
create index IX_73C52252 on UserGroupGroupRole (userGroupId, groupId);

create index IX_1B988D7A on UserGroupRole (groupId);
create index IX_871412DF on UserGroupRole (groupId, roleId);
create index IX_887A2C95 on UserGroupRole (roleId);
create index IX_887BE56A on UserGroupRole (userId);
create index IX_4D040680 on UserGroupRole (userId, groupId);

create unique index IX_41A32E0D on UserIdMapper (type_, externalUserId);
create index IX_E60EA987 on UserIdMapper (userId);
create unique index IX_D1C44A6E on UserIdMapper (userId, type_);

create index IX_29BA1CF5 on UserTracker (companyId);
create index IX_46B0AE8E on UserTracker (sessionId);
create index IX_E4EFBA8D on UserTracker (userId);

create index IX_14D8BCC0 on UserTrackerPath (userTrackerId);

create index IX_3A1E834E on User_ (companyId);
create index IX_6EF03E4E on User_ (companyId, defaultUser);
create unique index IX_615E9F7A on User_ (companyId, emailAddress);
create unique index IX_C5806019 on User_ (companyId, screenName);
create unique index IX_9782AD88 on User_ (companyId, userId);
create unique index IX_5ADBE171 on User_ (contactId);
create index IX_762F63C6 on User_ (emailAddress);
create index IX_A9ED7DD3 on User_ (openId);
create index IX_A18034A4 on User_ (portraitId);
create index IX_E0422BDA on User_ (uuid_);

create index IX_C4F9E699 on Users_Groups (groupId);
create index IX_F10B6C6B on Users_Groups (userId);

create index IX_7EF4EC0E on Users_Orgs (organizationId);
create index IX_FB646CA6 on Users_Orgs (userId);

create index IX_8AE58A91 on Users_Permissions (permissionId);
create index IX_C26AA64D on Users_Permissions (userId);

create index IX_C19E5F31 on Users_Roles (roleId);
create index IX_C1A01806 on Users_Roles (userId);

create index IX_66FF2503 on Users_UserGroups (userGroupId);
create index IX_BE8102D6 on Users_UserGroups (userId);

create unique index IX_97DFA146 on WebDAVProps (classNameId, classPK);

create index IX_96F07007 on Website (companyId);
create index IX_4F0F0CA7 on Website (companyId, classNameId);
create index IX_F960131C on Website (companyId, classNameId, classPK);
create index IX_1AA07A6D on Website (companyId, classNameId, classPK, primary_);
create index IX_F75690BB on Website (userId);

create index IX_5D6FE3F0 on WikiNode (companyId);
create index IX_B480A672 on WikiNode (groupId);
create unique index IX_920CD8B1 on WikiNode (groupId, name);
create index IX_6C112D7C on WikiNode (uuid_);
create unique index IX_7609B2AE on WikiNode (uuid_, groupId);

create index IX_A2001730 on WikiPage (format);
create index IX_C8A9C476 on WikiPage (nodeId);
create index IX_E7F635CA on WikiPage (nodeId, head);
create index IX_65E84AF4 on WikiPage (nodeId, head, parentTitle);
create index IX_46EEF3C8 on WikiPage (nodeId, parentTitle);
create index IX_1ECC7656 on WikiPage (nodeId, redirectTitle);
create index IX_997EEDD2 on WikiPage (nodeId, title);
create index IX_E745EA26 on WikiPage (nodeId, title, head);
create unique index IX_3D4AF476 on WikiPage (nodeId, title, version);
create index IX_9C0E478F on WikiPage (uuid_);
create unique index IX_899D3DFB on WikiPage (uuid_, groupId);

create unique index IX_21277664 on WikiPageResource (nodeId, title);


