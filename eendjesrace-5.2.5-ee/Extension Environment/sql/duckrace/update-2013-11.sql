-- Add fields to Participant
alter table Participant
  add ( contestParticipant tinyint,
        mercedesDriver tinyint,
	      carBrand varchar(75) null,
	      mercedesOld tinyint,
        language varchar(10) null);

