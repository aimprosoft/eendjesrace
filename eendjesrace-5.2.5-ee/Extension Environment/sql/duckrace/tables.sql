-- Lot tables & indexes
create table Lot (
	code_ varchar(6) not null primary key,
	checksum varchar(3) not null,
	number_ integer,
	lotNumber integer,
	pressed tinyint,
	nbTries integer,
	participantId bigint,
	reservationId bigint,
	partnerId bigint,
	lockoutDate datetime null
);
create index IX_LOT_PART on Lot (participantId);
create index IX_LOT_RES on Lot (reservationId);
create index IX_LOT_CODE on Lot (code_);
create index IX_LOT_NUMBER on Lot (number_);
create index IX_LOT_PRESSED on Lot (pressed);
create index IX_LOT_PARTNER on Lot (partnerId);

-- Participant tables & indexes
create table Participant (
	participantId bigint not null primary key,
	emailAddress varchar(75) null,
	mobileNumber varchar(20) null,
	firstName varchar(75) null,
	lastName varchar(75) null,
	street varchar(75) null,
	place varchar(75) null,
	country varchar(20) null
);
create unique index IX_PART_MOB on Participant (mobileNumber);

-- Reservation tables & indexes
create table LotReservation (
	reservationId bigint not null primary key,
	participantId bigint,
	numberLots integer,
	approved tinyint,
	reservationDate datetime null
);
create index IX_RES_APPR on LotReservation (approved);

create table Partner (
	partnerId bigint not null primary key,
	category varchar(75) null,
	name varchar(75) null,
	returnedLotsCount integer
);