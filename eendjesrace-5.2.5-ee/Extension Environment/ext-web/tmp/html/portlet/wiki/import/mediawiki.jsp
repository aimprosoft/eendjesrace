<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/wiki/init.jsp" %>

<input name="<portlet:namespace />filesCount" type="hidden" value="3" />

<table class="lfr-table">
<tr>
	<td>
		<liferay-ui:message key="pages-file" />
	</td>
	<td>
		<input name="<portlet:namespace />file0" type="file" />

		<liferay-ui:icon-help message="import-wiki-pages-help" />
	</td>
</tr>
<tr>
	<td>
		<liferay-ui:message key="users-file" /> (<liferay-ui:message key="optional" />)
	</td>
	<td>
		<input name="<portlet:namespace />file1" type="file" />

		<liferay-ui:icon-help message="import-wiki-users-help" />
	</td>
</tr>
<tr>
	<td>
		<liferay-ui:message key="images-file" /> (<liferay-ui:message key="optional" />)
	</td>
	<td>
		<input name="<portlet:namespace />file2" type="file" />

		<liferay-ui:icon-help message="import-wiki-images-help" />
	</td>
</tr>
<tr>
	<td>
		<%= WikiPageImpl.FRONT_PAGE %> (<liferay-ui:message key="optional" />)
	</td>
	<td>
		<input name="<portlet:namespace /><%= WikiImporterKeys.OPTIONS_FRONT_PAGE %>" type="text" size="40" value="Main Page" />
	</td>
</tr>
</table>

<br />

<div>
	<input checked="checked" name="<portlet:namespace /><%= WikiImporterKeys.OPTIONS_IMPORT_LATEST_VERSION %>" type="checkbox" />

	<liferay-ui:message key="import-only-the-latest-version-and-not-the-full-history" />
</div>