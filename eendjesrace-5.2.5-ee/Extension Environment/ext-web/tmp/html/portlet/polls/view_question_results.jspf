<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%
NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
NumberFormat percentFormat = NumberFormat.getPercentInstance(locale);
%>

<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr class="portlet-section-header results-header" style="font-size: x-small; font-weight: bold;">
	<td>
		%
	</td>
	<td colspan="6">
		<liferay-ui:message key="votes" />
	</td>
</tr>

<%
int totalVotes = question.getVotesCount();

for (int i = 0; i < choices.size(); i++) {
	PollsChoice choice = (PollsChoice)choices.get(i);

	choice = choice.toEscapedModel();

	int choiceVotes = choice.getVotesCount();

	String className = "portlet-section-body results-row";
	String classHoverName = "portlet-section-body-hover results-row hover";

	if (MathUtil.isEven(i)) {
		className = "portlet-section-alternate results-row alt";
		classHoverName = "portlet-section-alternate-hover results-row alt hover";
	}

	double votesPercent = 0.0;

	if (totalVotes > 0) {
		votesPercent = (double)choiceVotes / totalVotes;
	}

	int votesPixelWidth = 35;

	if (windowState.equals(WindowState.MAXIMIZED)) {
		votesPixelWidth = 100;
	}

	int votesPercentWidth = (int)Math.floor(votesPercent * votesPixelWidth);
%>

<tr class="<%= className %>" style="font-size: x-small;" onMouseEnter="this.className = '<%= classHoverName %>';" onMouseLeave="this.className = '<%= className %>';">
	<td>
		<%= percentFormat.format(votesPercent) %>
	</td>
	<td colspan="<%= votesPercentWidth > 0 ? "1" : "3" %>">
		<%= numberFormat.format(choiceVotes) %>
	</td>

	<c:if test="<%= votesPercentWidth > 0 %>">
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="alpha"><img border="0" height="5" hspace="0" src="<%= themeDisplay.getPathThemeImages() %>/spacer.png" vspace="0" width="<%= votesPercentWidth %>" /></td>
				<td class="beta"><img border="0" height="5" hspace="0" src="<%= themeDisplay.getPathThemeImages() %>/spacer.png" vspace="0" width="<%= votesPixelWidth - votesPercentWidth %>" /></td>
			</tr>
			</table>
		</td>
	</c:if>

	<td></td>
	<td>
		<b><%= choice.getName() %>.</b>
	</td>
	<td width="99%">
		<%= choice.getDescription() %>
	</td>
</tr>

<%
}
%>

</table>

<br />

<div>
	<b><liferay-ui:message key="total-votes" />:</b> <%= numberFormat.format(totalVotes) %>
</div>

<c:if test="<%= portletName.equals(PortletKeys.POLLS) %>">
	<br />

	<div>
		<b><liferay-ui:message key="charts" />:</b>
		<a href="javascript:var viewChartWindow = window.open('<%= themeDisplay.getPathMain() %>/polls/view_chart?questionId=<%= question.getQuestionId() %>&chartType=area', 'viewChart', 'directories=no,height=430,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=420'); void(''); viewChartWindow.focus();"><liferay-ui:message key="area" /></a>,
		<a href="javascript:var viewChartWindow = window.open('<%= themeDisplay.getPathMain() %>/polls/view_chart?questionId=<%= question.getQuestionId() %>&chartType=horizontal_bar', 'viewChart', 'directories=no,height=430,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=420'); void(''); viewChartWindow.focus();"><liferay-ui:message key="horizontal-bar" /></a>,
		<a href="javascript:var viewChartWindow = window.open('<%= themeDisplay.getPathMain() %>/polls/view_chart?questionId=<%= question.getQuestionId() %>&chartType=line', 'viewChart', 'directories=no,height=430,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=420'); void(''); viewChartWindow.focus();"><liferay-ui:message key="line" /></a>,
		<a href="javascript:var viewChartWindow = window.open('<%= themeDisplay.getPathMain() %>/polls/view_chart?questionId=<%= question.getQuestionId() %>&chartType=pie', 'viewChart', 'directories=no,height=430,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=420'); void(''); viewChartWindow.focus();"><liferay-ui:message key="pie" /></a>,
		<a href="javascript:var viewChartWindow = window.open('<%= themeDisplay.getPathMain() %>/polls/view_chart?questionId=<%= question.getQuestionId() %>&chartType=vertical_bar', 'viewChart', 'directories=no,height=430,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=420'); void(''); viewChartWindow.focus();"><liferay-ui:message key="vertical-bar" /></a>
	</div>

	<c:if test="<%= PollsQuestionPermission.contains(permissionChecker, question, ActionKeys.UPDATE) %>">
		<br />

		<liferay-ui:tabs names="actual-votes" />

		<%
		PortletURL portletURL = renderResponse.createRenderURL();

		portletURL.setWindowState(WindowState.MAXIMIZED);

		portletURL.setParameter("struts_action", "/polls/view_question");
		portletURL.setParameter("redirect", redirect);
		portletURL.setParameter("questionId", String.valueOf(question.getQuestionId()));

		List<String> headerNames = new ArrayList<String>();

		headerNames.add("user");
		headerNames.add("choice");
		headerNames.add("vote-date");

		SearchContainer searchContainer = new SearchContainer(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, SearchContainer.DEFAULT_DELTA, portletURL, headerNames, null);

		int total = PollsVoteLocalServiceUtil.getQuestionVotesCount(question.getQuestionId());

		searchContainer.setTotal(total);

		List results = PollsVoteLocalServiceUtil.getQuestionVotes(question.getQuestionId(), searchContainer.getStart(), searchContainer.getEnd());

		searchContainer.setResults(results);

		List resultRows = searchContainer.getResultRows();

		for (int i = 0; i < results.size(); i++) {
			PollsVote vote = (PollsVote)results.get(i);

			ResultRow row = new ResultRow(vote, vote.getVoteId(), i);

			// User

			row.addText(PortalUtil.getUserName(vote.getUserId(), String.valueOf(vote.getUserId()), request));

			// Choice

			PollsChoice choice = vote.getChoice();

			row.addText(choice.getName() + ". " + choice.getDescription());

			// Vote date

			row.addText(dateFormatDateTime.format(vote.getVoteDate()));

			// Add result row

			resultRows.add(row);
		}
		%>

		<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />
	</c:if>
</c:if>

<c:if test="<%= question.isExpired() %>">
	<br />

	<div style="font-size: xx-small;">
	<%= LanguageUtil.format(pageContext, "voting-is-disabled-because-this-poll-expired-on-x", dateFormatDateTime.format(question.getExpirationDate())) %>.
	</div>
</c:if>