<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/quick_note/init.jsp" %>

<div id="<portlet:namespace />pad" style="background: <%= color %>;">
	<c:if test="<%= portletDisplay.isShowConfigurationIcon() %>">
		<div class="portlet-title-default">
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
			<tr>
				<td>
					<span class="note-color yellow"></span>
					<span class="note-color green"></span>
					<span class="note-color blue"></span>
					<span class="note-color red"></span>
				</td>

				<c:if test="<%= portletDisplay.isShowCloseIcon() %>">
					<td>
						<a border="0" class="close-note" href="<%= portletDisplay.getURLClose() %>"><img alt="<liferay-ui:message key="close" />" src="<%= themeDisplay.getPathThemeImages() %>/portlet/close.png" /></a>
					</td>
				</c:if>
			</tr>
			</table>
		</div>
	</c:if>

	<div id="<portlet:namespace />note"><%= data %></div>
</div>

<c:if test="<%= portletDisplay.isShowConfigurationIcon() %>">
	<script type="text/javascript">
		jQuery(
			function() {
				jQuery('#<portlet:namespace />pad .note-color').click(
					function() {
						var box = jQuery(this);

						var bgColor = box.css('background-color');

						jQuery('#<portlet:namespace />pad').css('background-color', bgColor);

						jQuery.ajax(
							{
								type: 'POST',
								url: '<%= themeDisplay.getPathMain() %>/quick_note/save',
								data: {
									p_l_id: '<%= plid %>',
									portletId: '<%= portletDisplay.getId() %>',
									color: bgColor
								}
							}
						);
					}
				);

				jQuery('#<portlet:namespace />note').editable(
					function(value, settings) {
						var newValue = value.replace(/\n/gi, '<br />');

						if (value != settings._LFR_.oldText) {
							jQuery.ajax(
								{
									url: '<%= themeDisplay.getPathMain() %>/quick_note/save',
									data: {
										p_l_id: '<%= plid %>',
										portletId: '<%= portletDisplay.getId() %>',
										data: newValue
									}
								}
							);
						}
						return newValue;
					},
					{
						data: function(value, settings) {
							var newValue = value.replace(/<br[\s\/]?>/gi, '\n');

							settings._LFR_ = {};
							settings._LFR_.oldText = newValue;

							return newValue;
						},
						onblur: 'submit',
						type: 'textarea',
						select: true,
						submit: ''
					}
				);
			}
		);
	</script>
</c:if>