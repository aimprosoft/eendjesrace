<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/init.jsp" %>

<%@ page import="com.liferay.portal.service.permission.OrganizationPermissionUtil" %>
<%@ page import="com.liferay.portal.service.permission.RolePermissionUtil" %>
<%@ page import="com.liferay.portal.service.permission.UserGroupPermissionUtil" %>
<%@ page import="com.liferay.portlet.announcements.EntryContentException" %>
<%@ page import="com.liferay.portlet.announcements.EntryDisplayDateException" %>
<%@ page import="com.liferay.portlet.announcements.EntryExpirationDateException" %>
<%@ page import="com.liferay.portlet.announcements.EntryTitleException" %>
<%@ page import="com.liferay.portlet.announcements.NoSuchEntryException" %>
<%@ page import="com.liferay.portlet.announcements.NoSuchFlagException" %>
<%@ page import="com.liferay.portlet.announcements.model.AnnouncementsEntry" %>
<%@ page import="com.liferay.portlet.announcements.model.impl.AnnouncementsEntryImpl" %>
<%@ page import="com.liferay.portlet.announcements.model.impl.AnnouncementsFlagImpl" %>
<%@ page import="com.liferay.portlet.announcements.service.AnnouncementsEntryLocalServiceUtil" %>
<%@ page import="com.liferay.portlet.announcements.service.AnnouncementsFlagLocalServiceUtil" %>
<%@ page import="com.liferay.portlet.announcements.service.permission.AnnouncementsEntryPermission" %>
<%@ page import="com.liferay.portlet.announcements.util.AnnouncementsUtil" %>

<%
int delta = 3;

DateFormat dateFormatDate = DateFormats.getDate(locale, timeZone);
%>

<%@ include file="/html/portlet/announcements/init-ext.jsp" %>