<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/css_init.jsp" %>

.portlet-journal .journal-edit-article-extra {
	border: 1px solid #CCC;
	width: 100%;
}

.portlet-journal .journal-edit-article-extra tr td {
	padding: 5px;
}

.portlet-journal .journal-article-status-approved {
	color: green;
}

.portlet-journal .journal-article-status-expired {
	color: red;
}

.portlet-journal .journal-article-status-not-approved {
	color: orange;
}

.journal-template-error .scroll-pane {
	border: 1px solid #BFBFBF;
	max-height: 200px;
	min-height: 50px;
	overflow: auto;
	width: 100%;
}

.journal-template-error .scroll-pane .inner-scroll-pane {
	min-width: 104%;
}

.journal-template-error .scroll-pane .error-line {
	background: #fdd;
}

.journal-template-error .scroll-pane pre {
	margin: 0px;
	white-space: pre;
}

.journal-template-error .scroll-pane pre span {
	background: #B5BFC4;
	border-right: 1px solid #BFBFBF;
	display: block;
	float: left;
	margin-right: 4px;
	padding-right: 4px;
	text-align: right;
	width: 40px;
}

.portlet-journal .lfr-panel-titlebar .lfr-panel-title {
	border-bottom: 1px solid #ccc;
	float: none;
	position: relative;
	top: -0.5em;
}

.portlet-journal .lfr-panel-titlebar .lfr-panel-title span {
	background: #fff;
	padding: 0 8px 0 4px;
	position: relative;
	top: 0.55em;
}

.portlet-journal .lfr-panel-content {
	background-color: #F0F5F7;
	padding: 10px;
}

.portlet-journal .journal-extras {
	border-width: 0;
}

.portlet-journal .journal-extras .lfr-panel {
	margin-bottom: 1em;
}