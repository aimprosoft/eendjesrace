<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/journal/init.jsp" %>

<form method="post" name="<portlet:namespace />fm">

<liferay-ui:tabs names="templates" />

<%
PortletURL portletURL = renderResponse.createRenderURL();

portletURL.setParameter("struts_action", "/journal/select_template");

TemplateSearch searchContainer = new TemplateSearch(renderRequest, portletURL);

searchContainer.setDelta(10);
%>

<liferay-ui:search-form
	page="/html/portlet/journal/template_search.jsp"
	searchContainer="<%= searchContainer %>"
/>

<%
TemplateSearchTerms searchTerms = (TemplateSearchTerms)searchContainer.getSearchTerms();

searchTerms.setStructureId(StringPool.BLANK);
searchTerms.setStructureIdComparator(StringPool.NOT_EQUAL);
%>

<%@ include file="/html/portlet/journal/template_search_results.jspf" %>

<div class="separator"><!-- --></div>

<%

List resultRows = searchContainer.getResultRows();

for (int i = 0; i < results.size(); i++) {
	JournalTemplate template = (JournalTemplate)results.get(i);

	ResultRow row = new ResultRow(template, template.getId(), i);

	StringBuilder sb = new StringBuilder();

	sb.append("javascript:opener.");
	sb.append(renderResponse.getNamespace());
	sb.append("selectTemplate('");
	sb.append(template.getStructureId());
	sb.append("', '");
	sb.append(template.getTemplateId());
	sb.append("'); window.close();");

	String rowHREF = sb.toString();

	row.setParameter("rowHREF", rowHREF);

	// Template id

	row.addText(template.getTemplateId(), rowHREF);

	// Name, description, and image

	row.addJSP("/html/portlet/journal/template_description.jsp");

	// Add result row

	resultRows.add(row);
}
%>

<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />

</form>

<script type="text/javascript">
	Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />searchTemplateId);
</script>