<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%
for (int j = 0; j < fileEntryColumns.length; j++) {
	String fileEntryColumn = fileEntryColumns[j];
%>

	<c:choose>
		<c:when test='<%= fileEntryColumn.equals("action") %>'>

			<%
			String align = SearchEntry.DEFAULT_ALIGN;

			if ((j + 1) == fileEntryColumns.length) {
				align = "right";
			}
			%>

			<liferay-ui:search-container-column-jsp
				align="<%= align %>"
				path="/html/portlet/document_library/file_entry_action.jsp"
			/>

		</c:when>
		<c:when test='<%= fileEntryColumn.equals("downloads") %>'>
			<liferay-ui:search-container-column-text
				href="<%= rowHREF %>"
				name="<%= fileEntryColumn %>"
				value="<%= String.valueOf(fileEntry.getReadCount()) %>"
			/>
		</c:when>
		<c:when test='<%= fileEntryColumn.equals("locked") %>'>

			<%
			boolean isLocked = LockServiceUtil.isLocked(DLFileEntry.class.getName(), DLUtil.getLockId(fileEntry.getFolderId(), HtmlUtil.unescape(fileEntry.getName())));
			%>

			<liferay-ui:search-container-column-text
				href="<%= rowHREF %>"
				name="<%= fileEntryColumn %>"
				value='<%= LanguageUtil.get(pageContext, isLocked ? "yes" : "no") %>'
			/>
		</c:when>
		<c:when test='<%= fileEntryColumn.equals("name") %>'>
			<liferay-ui:search-container-column-text
				buffer="buffer"
				href="<%= rowHREF %>"
				name="<%= fileEntryColumn %>"
			>

				<%
				buffer.append(_getFileEntryImage(fileEntry, themeDisplay));
				buffer.append(fileEntry.getTitleWithExtension());

				if (Validator.isNotNull(fileEntry.getDescription())) {
					buffer.append("<br />");
					buffer.append(fileEntry.getDescription());
				}
				%>

			</liferay-ui:search-container-column-text>
		</c:when>
		<c:when test='<%= fileEntryColumn.equals("size") %>'>
			<liferay-ui:search-container-column-text
				href="<%= rowHREF %>"
				name="<%= fileEntryColumn %>"
				value='<%= TextFormatter.formatKB(fileEntry.getSize(), locale) + "k" %>'
			/>
		</c:when>
		<c:when test='<%= fileEntryColumn.equals("tags") %>'>

			<%
			List<TagsEntry> tagsEntries = TagsEntryLocalServiceUtil.getEntries(DLFileEntry.class.getName(), fileEntry.getFileEntryId(), true);

			TagsUtil.addLayoutTagsEntries(request, tagsEntries);
			%>

			<liferay-ui:search-container-column-text
				href="<%= rowHREF %>"
				name="<%= fileEntryColumn %>"
				value='<%= ListUtil.toString(tagsEntries, "name", StringPool.COMMA_AND_SPACE) %>'
			/>
		</c:when>
	</c:choose>

<%
}
%>