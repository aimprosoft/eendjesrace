<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/portlet_configuration/init.jsp" %>

<%
String portletResource = ParamUtil.getString(request, "portletResource");

Portlet selPortlet = PortletLocalServiceUtil.getPortletById(company.getCompanyId(), portletResource);

String path = (String)request.getAttribute(WebKeys.CONFIGURATION_ACTION_PATH);
%>

<liferay-util:include page="/html/portlet/portlet_configuration/tabs1.jsp">
	<liferay-util:param name="tabs1" value="setup" />
</liferay-util:include>

<c:if test="<%= GroupPermissionUtil.contains(permissionChecker, layout.getGroupId(), ActionKeys.MANAGE_ARCHIVED_SETUPS) %>">
	<liferay-util:include page="/html/portlet/portlet_configuration/tabs2.jsp" />
</c:if>

<c:if test="<%= (selPortlet != null) && Validator.isNotNull(path) %>">

	<%
	PortletApp selPortletApp = selPortlet.getPortletApp();
	%>

	<c:choose>
		<c:when test="<%= selPortletApp.isWARFile() %>">

			<%
			PortletConfig selPortletConfig = PortletConfigFactory.create(selPortlet, application);
			PortletContextImpl selPortletCtx = (PortletContextImpl)selPortletConfig.getPortletContext();

			RequestDispatcher selRd = selPortletCtx.getServletContext().getRequestDispatcher(path);

			StringServletResponse stringResponse = new StringServletResponse(response);

			selRd.include(request, stringResponse);
			%>

			<%= stringResponse.getString() %>
		</c:when>
		<c:otherwise>
			<liferay-util:include page="<%= path %>" />
		</c:otherwise>
	</c:choose>
</c:if>