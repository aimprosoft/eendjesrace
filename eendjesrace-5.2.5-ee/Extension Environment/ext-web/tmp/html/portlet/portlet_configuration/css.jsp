<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/css_init.jsp" %>

.portlet-configuration {
}

.portlet-configuration .edit-permissions form:after {
	clear: both;
	content: ".";
	display: block;
	height: 0;
	visibility: hidden;
}

.ie .portlet-configuration .edit-permissions form {
	height: 1%;
}

.portlet-configuration .edit-permissions .assign-permissions {
	float: left;
}

.portlet-configuration .edit-permissions .assign-permissions .button-holder {
	position: relative;
}

.portlet-configuration .edit-permissions .assign-permissions .button-holder .finished {
	position: absolute;
	right: 0;
}

.portlet-configuration .facebook-api td {
	padding-bottom: 2px;
}

.portlet-configuration .facebook-api .api-input {
	padding-right: 12px;
}

.ie .portlet-configuration .facebook-api .api-input {
	padding-right: 17px;
}

.portlet-configuration .facebook-api .lfr-input-text {
	width: 100%;
}

.portlet-configuration .canvas-url .url-text {
	padding-right: 0;
	text-align: right;
}

.portlet-configuration .canvas-url .url-input {
	padding-left: 0
}