<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/sms/init.jsp" %>

<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">

		<%
		String to = ParamUtil.getString(request, "to");
		String subject = ParamUtil.getString(request, "subject");
		String message = ParamUtil.getString(request, "message");
		%>

		<form action="<portlet:actionURL />" method="post" name="<portlet:namespace />fm" onSubmit="submitForm(this); return false;">
		<input name="<portlet:namespace /><%= Constants.CMD %>" type="hidden" value="<%= Constants.SEND %>" />

		<liferay-ui:success key='<%= portletConfig.getPortletName() + ".send" %>' message="you-have-successfully-sent-a-sms-message" />

		<liferay-ui:error key="to" message="please-enter-a-valid-email-address" />
		<liferay-ui:error key="message" message="please-limit-your-message-to-500-characters" />

		<table class="lfr-table">
		<tr>
			<td>
				<liferay-ui:message key="to" />
			</td>
			<td>
				<input class="lfr-input-text" name="<portlet:namespace />to" type="text" value="<%= to %>" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="subject" />
			</td>
			<td>
				<input class="lfr-input-text" name="<portlet:namespace />subject" type="text" onChange="document.<portlet:namespace />fm.<portlet:namespace />length.value = document.<portlet:namespace />fm.<portlet:namespace />subject.value.length + document.<portlet:namespace />fm.<portlet:namespace />message.value.length;" onKeyUp="document.<portlet:namespace />fm.<portlet:namespace />length.value = document.<portlet:namespace />fm.<portlet:namespace />subject.value.length + document.<portlet:namespace />fm.<portlet:namespace />message.value.length;" value="<%= HtmlUtil.escape(subject) %>" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="message" />
			</td>
			<td>
				<textarea class="lfr-textarea" name="<portlet:namespace />message" wrap="soft" onChange="document.<portlet:namespace />fm.<portlet:namespace />length.value = document.<portlet:namespace />fm.<portlet:namespace />subject.value.length + document.<portlet:namespace />fm.<portlet:namespace />message.value.length;" onKeyUp="document.<portlet:namespace />fm.<portlet:namespace />length.value = document.<portlet:namespace />fm.<portlet:namespace />subject.value.length + document.<portlet:namespace />fm.<portlet:namespace />message.value.length;"><%= HtmlUtil.escape(message) %></textarea><br />
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input disabled maxlength="3" name="<portlet:namespace />length" size="3" type="text" value="<%= subject.length() + message.length() %>" />

				<span style="font-size: xx-small;">(500 <liferay-ui:message key="characters-maximum" />)</span>
			</td>
		</tr>
		</table>

		<br />

		<input type="submit" value="<liferay-ui:message key="send-text-message" />" />

		</form>

		<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
			<script type="text/javascript">
				Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />to);
			</script>
		</c:if>
	</c:when>
	<c:otherwise>
		<liferay-ui:message key="you-must-be-authenticated-to-use-this-portlet" />
	</c:otherwise>
</c:choose>