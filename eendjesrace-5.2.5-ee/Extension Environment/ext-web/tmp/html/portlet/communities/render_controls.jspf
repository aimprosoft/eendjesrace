<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%!
private String _renderControls(String namespace, ResourceBundle resourceBundle, PortletDataHandlerControl[] controls) {
	StringBuilder sb = new StringBuilder();

	for (int i = 0; i < controls.length; i++) {
		String controlName = controls[i].getControlName();

		try {
			controlName = resourceBundle.getString(controlName);
		}
		catch (MissingResourceException mre) {
		}

		sb.append("<li class=\"");
		sb.append(namespace);
		sb.append("handler-control\">");

		if (controls[i] instanceof PortletDataHandlerBoolean) {
			PortletDataHandlerBoolean control = (PortletDataHandlerBoolean)controls[i];
			PortletDataHandlerControl[] children = control.getChildren();

			sb.append("<label class=\"");
			sb.append(namespace);
			sb.append("handler-control-label\">");
			sb.append("<input ");
			sb.append(control.getDefaultState() ? "checked=\"checked\" " : "");

			if (controls[i].isDisabled()) {
				sb.append(" class=\"disabled\" disabled ");
			}

			sb.append("name=\"");
			sb.append(namespace);
			sb.append(control.getNamespacedControlName());
			sb.append("\" ");
			sb.append("id=\"");
			sb.append(namespace);
			sb.append(control.getNamespacedControlName());
			sb.append("\" ");

			if (children != null) {
				sb.append("onchange=\"");
				sb.append(namespace);
				sb.append("toggleChildren(this, '");
				sb.append(namespace);
				sb.append(control.getNamespacedControlName());
				sb.append("Controls");
				sb.append("');\" ");
			}

			sb.append("type=\"checkbox\" /> ");
			sb.append(controlName);
			sb.append("</label>");

			if (children != null) {
				sb.append("<ul id=\"");
				sb.append(namespace);
				sb.append(control.getNamespacedControlName());
				sb.append("Controls\">");
				sb.append(_renderControls(namespace, resourceBundle, children));
				sb.append("</ul>");
			}
		}
		else if (controls[i] instanceof PortletDataHandlerChoice) {
			PortletDataHandlerChoice control = (PortletDataHandlerChoice)controls[i];

			sb.append("<span class=\"");
			sb.append(namespace);
			sb.append("handler-control-label\">");
			sb.append("<strong>&#9632;</strong> ");
			sb.append(controlName);
			sb.append("</span>");

			String[] choices = control.getChoices();

			for (int j = 0; j < choices.length; j++) {
				String choice = choices[j];

				try {
					choice = resourceBundle.getString(choice);
				}
				catch (MissingResourceException mre) {
				}

				sb.append("<label class=\"");
				sb.append(namespace);
				sb.append("handler-control-label\">");
				sb.append("<input ");
				sb.append(control.getDefaultChoiceIndex() == j ? "checked=\"checked\" " : "");
				sb.append("name=\"");
				sb.append(namespace);
				sb.append(control.getNamespacedControlName());
				sb.append("\" type=\"radio\" value=\"");
				sb.append(choices[j]);
				sb.append("\" />");
				sb.append(choice);
				sb.append("</label>");
			}
		}

		sb.append("</li>");
	}

	return sb.toString();
}
%>