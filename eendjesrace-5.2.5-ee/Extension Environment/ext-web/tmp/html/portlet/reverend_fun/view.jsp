<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/reverend_fun/init.jsp" %>

<c:choose>
	<c:when test="<%= windowState.equals(WindowState.NORMAL) %>">
		<table class="lfr-table">
		<tr>
			<td>

				<%
				String currentDate = ReverendFunUtil.getCurrentDate();

				Calendar currentCal = CalendarFactoryUtil.getCalendar();

				currentCal.setTime(dateFormat.parse(currentDate));
				%>

				<a href="<portlet:renderURL windowState="<%= WindowState.MAXIMIZED.toString() %>" />"><img border="0" src="http://rev-fun.gospelcom.net/<%= currentCal.get(Calendar.YEAR) %>/<%= decimalFormat.format(currentCal.get(Calendar.MONTH) + 1) %>/<%= currentDate %>_sm.gif" width="72" /></a>
			</td>
			<td>
				Reverend Fun, daily humor for daily people!
			</td>
		</tr>
		</table>
	</c:when>
	<c:otherwise>

		<%
		String date = ParamUtil.getString(request, "date");

		if (!ReverendFunUtil.hasDate(date)) {
			date = ReverendFunUtil.getCurrentDate();
		}

		String previousDate = ReverendFunUtil.getPreviousDate(date);
		String nextDate = ReverendFunUtil.getNextDate(date);
		%>

		<table class="lfr-table">
		<tr>
			<td>
				<b><%= DateFormat.getDateInstance(DateFormat.LONG, locale).format(dateFormat.parse(date)) %></b>
			</td>
			<td align="right">
				<c:if test="<%= previousDate != null %>">
					<a href="<portlet:renderURL windowState="<%= WindowState.MAXIMIZED.toString() %>"><portlet:param name="date" value="<%= previousDate %>" /></portlet:renderURL>"><liferay-ui:message key="previous" /></a>
				</c:if>

				<c:if test="<%= previousDate != null && nextDate != null %>">
					-
				</c:if>

				<c:if test="<%= nextDate != null %>">
					<a href="<portlet:renderURL windowState="<%= WindowState.MAXIMIZED.toString() %>"><portlet:param name="date" value="<%= nextDate %>" /></portlet:renderURL>"><liferay-ui:message key="next" /></a>
				</c:if>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<br />

				<img alt="<liferay-ui:message key="cartoon" />" src="http://rev-fun.gospelcom.net/add_toon_info.php?date=<%= date %>" />
			</td>
		</tr>
		</table>
	</c:otherwise>
</c:choose>