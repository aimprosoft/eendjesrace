<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/recent_bloggers/init.jsp" %>

<%
List statsUsers = null;

if (selectionMethod.equals("users")) {
	if (organizationId > 0) {
		statsUsers = BlogsStatsUserLocalServiceUtil.getOrganizationStatsUsers(organizationId, 0, max, new StatsUserLastPostDateComparator());
	}
	else {
		statsUsers = BlogsStatsUserLocalServiceUtil.getCompanyStatsUsers(company.getCompanyId(), 0, max, new StatsUserLastPostDateComparator());
	}
}
else {
	statsUsers = BlogsStatsUserLocalServiceUtil.getGroupStatsUsers(themeDisplay.getScopeGroupId(), 0, max);
}
%>

<c:choose>
	<c:when test="<%= statsUsers.size() == 0 %>">
		<liferay-ui:message key="there-are-no-recent-bloggers" />
	</c:when>
	<c:otherwise>

		<%
		SearchContainer searchContainer = new SearchContainer();

		List<String> headerNames = new ArrayList<String>();

		headerNames.add("user");
		//headerNames.add("place");
		headerNames.add("posts");
		headerNames.add("date");

		if (displayStyle.equals("user-name")) {
			searchContainer.setHeaderNames(headerNames);
		}

		List resultRows = searchContainer.getResultRows();

		for (int i = 0; i < statsUsers.size(); i++) {
			BlogsStatsUser statsUser = (BlogsStatsUser)statsUsers.get(i);

			try {
				Group group = GroupLocalServiceUtil.getGroup(statsUser.getGroupId());
				User user2 = UserLocalServiceUtil.getUserById(statsUser.getUserId());

				String blogType = LanguageUtil.get(pageContext, "personal");

				if (group.isCommunity()) {
					blogType = group.getName();// + " " + LanguageUtil.get(pageContext, "community");
				}
				else if (group.isOrganization()) {
					Organization organization = OrganizationLocalServiceUtil.getOrganization(group.getClassPK());

					blogType = organization.getName();// + " " + LanguageUtil.get(pageContext, "organization");
				}

				int entryCount = BlogsEntryLocalServiceUtil.getGroupUserEntriesCount(group.getGroupId(), user2.getUserId(), false);

				List entries = BlogsEntryLocalServiceUtil.getGroupUserEntries(group.getGroupId(), user2.getUserId(), false, 0, 1);

				if (entries.size() == 1) {
					BlogsEntry entry = (BlogsEntry)entries.get(0);

					StringBuilder sb = new StringBuilder();

					sb.append(themeDisplay.getPathMain());
					sb.append("/blogs/find_entry?entryId=");
					sb.append(entry.getEntryId());
					sb.append("&showAllEntries=1");

					String rowHREF = sb.toString();

					ResultRow row = new ResultRow(new Object[] {statsUser, rowHREF}, statsUser.getStatsUserId(), i);

					if (displayStyle.equals("user-name")) {

						// User

						row.addText(user2.getFullName(), rowHREF);

						// Type

						//row.addText(blogType, rowHREF);

						// Number of posts

						row.addText(String.valueOf(entryCount), rowHREF);

						// Last post date

						row.addText(dateFormatDate.format(entry.getModifiedDate()), rowHREF);
					}
					else {

						// User display

						row.addJSP("/html/portlet/recent_bloggers/user_display.jsp");
					}

					// Add result row

					resultRows.add(row);
				}
			}
			catch (Exception e) {
			}
		}
		%>

		<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" paginate="<%= false %>" />
	</c:otherwise>
</c:choose>