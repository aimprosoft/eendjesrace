<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/image_gallery/init.jsp" %>

<%
ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

IGImage image = (IGImage)row.getObject();
%>

<table class="lfr-table">
<tr>
	<td>
		<a href="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= image.getLargeImageId() %>&t=<%= ImageServletTokenUtil.getToken(image.getLargeImageId()) %>" target="_blank">
		<img border="1" src="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= image.getSmallImageId() %>&t=<%= ImageServletTokenUtil.getToken(image.getSmallImageId()) %>" title="<%= image.getDescription() %>" /></a>
	</td>
</tr>

<c:if test="<%= (image.getCustom1ImageId() > 0) || (image.getCustom2ImageId() > 0) %>">
	<tr>
		<td>
			<a href="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= image.getLargeImageId() %>&t=<%= ImageServletTokenUtil.getToken(image.getLargeImageId()) %>" target="_blank">
			<liferay-ui:message key="original" /></a>

			<c:if test="<%= image.getCustom1ImageId() > 0 %>">
				|

				<a href="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= image.getCustom1ImageId() %>&t=<%= ImageServletTokenUtil.getToken(image.getCustom1ImageId()) %>" target="_blank">
				<liferay-ui:message key="size" /> 1</a>
			</c:if>

			<c:if test="<%= image.getCustom2ImageId() > 0 %>">
				|

				<a href="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= image.getCustom2ImageId() %>&t=<%= ImageServletTokenUtil.getToken(image.getCustom2ImageId()) %>" target="_blank">
				<liferay-ui:message key="size" /> 2</a>
			</c:if>
		</td>
	</tr>
</c:if>

</table>