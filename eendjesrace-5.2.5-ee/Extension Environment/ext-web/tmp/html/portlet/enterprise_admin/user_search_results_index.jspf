<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ page import="com.liferay.portal.kernel.search.Document" %>
<%@ page import="com.liferay.portal.kernel.search.Field" %>
<%@ page import="com.liferay.portal.kernel.search.Hits" %>
<%@ page import="com.liferay.portal.kernel.search.Sort" %>
<%@ page import="com.liferay.portlet.expando.model.ExpandoBridge" %>
<%@ page import="com.liferay.portlet.expando.model.ExpandoColumnConstants" %>
<%@ page import="com.liferay.portlet.expando.model.impl.ExpandoBridgeImpl" %>
<%@ page import="com.liferay.portlet.expando.util.ExpandoBridgeIndexer" %>

<%
ExpandoBridge expandoBridge = new ExpandoBridgeImpl(User.class.getName(), 0);

Enumeration<String> enu = expandoBridge.getAttributeNames();

while (enu.hasMoreElements()) {
	String attributeName = enu.nextElement();

	UnicodeProperties properties = expandoBridge.getAttributeProperties(attributeName);

	if (GetterUtil.getBoolean(properties.getProperty(ExpandoBridgeIndexer.INDEXABLE))) {
		int type = expandoBridge.getAttributeType(attributeName);

		if (type == ExpandoColumnConstants.STRING) {
			if (searchTerms.isAdvancedSearch()) {
				userParams.put(attributeName, ParamUtil.getString(request, attributeName));
			}
			else {
				userParams.put(attributeName, searchTerms.getKeywords());
			}
		}
	}
}

Sort sort = _getSort(searchContainer.getOrderByCol(), searchContainer.getOrderByType());

Hits hits = null;

if (searchTerms.isAdvancedSearch()) {
	hits = UserLocalServiceUtil.search(company.getCompanyId(), searchTerms.getFirstName(), searchTerms.getMiddleName(), searchTerms.getLastName(), searchTerms.getScreenName(), searchTerms.getEmailAddress(), searchTerms.getActive(), userParams, searchTerms.isAndOperator(), searchContainer.getStart(), searchContainer.getEnd(), sort);
}
else {
	hits = UserLocalServiceUtil.search(company.getCompanyId(), searchTerms.getKeywords(), searchTerms.getActive(), userParams, searchContainer.getStart(), searchContainer.getEnd(), sort);
}

results = _getResults(hits);
total = hits.getLength();

pageContext.setAttribute("results", results);
pageContext.setAttribute("total", total);
%>

<%!
private static List<User> _getResults(Hits hits) throws Exception {
	List<User> users = new ArrayList<User>();

	List<Document> hitsList = hits.toList();

	for (Document doc : hitsList) {
		long userId = GetterUtil.getLong(doc.get(Field.USER_ID));

		users.add(UserLocalServiceUtil.getUserById(userId));
	}

	return users;
}

private static Sort _getSort(String orderByCol, String orderByType) {
	String sortField = "firstName";

	if (Validator.isNotNull(orderByCol)) {
		if (orderByCol.equals("email-address")) {
			sortField = "emailAddress";
		}
		else if (orderByCol.equals("first-name")) {
			sortField = "firstName";
		}
		else if (orderByCol.equals("job-title")) {
			sortField = "jobTitle";
		}
		else if (orderByCol.equals("last-name")) {
			sortField = "lastName";
		}
		else if (orderByCol.equals("screen-name")) {
			sortField = "screenName";
		}
		else {
			sortField = orderByCol;
		}
	}

	if (Validator.isNull(orderByType)) {
		orderByType = "asc";
	}

	return new Sort(sortField, Sort.STRING_TYPE, !orderByType.equalsIgnoreCase("asc"));
}
%>