<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/enterprise_admin/init.jsp" %>

<%
User selUser = (User)request.getAttribute("user.selUser");

String languageId = BeanParamUtil.getString(selUser, request, "languageId", user.getLanguageId());
String timeZoneId = BeanParamUtil.getString(selUser, request, "timeZoneId", user.getTimeZoneId());
%>

<h3><liferay-ui:message key="display-settings" /></h3>

<fieldset class="block-labels">
	<div class="ctrl-holder">
		<label for="<portlet:namespace />languageId"><liferay-ui:message key="language" /></label>

		<select name="<portlet:namespace />languageId">

			<%
			Locale selLocale = LocaleUtil.fromLanguageId(languageId);

			Locale[] locales = LanguageUtil.getAvailableLocales();

			Locale languageLocale = locale;

			for (Locale curLocale : locales) {
				if (portletName.equals(PortletKeys.MY_ACCOUNT)) {
					languageLocale = curLocale;
				}
			%>

				<option <%= (selLocale.getLanguage().equals(curLocale.getLanguage()) && selLocale.getCountry().equals(curLocale.getCountry())) ? "selected" : "" %> value="<%= curLocale.getLanguage() + "_" + curLocale.getCountry() %>"><%= curLocale.getDisplayName(languageLocale) %></option>

			<%
			}
			%>

		</select>
	</div>

	<div class="ctrl-holder">
		<label for="<portlet:namespace />timeZoneId"><liferay-ui:message key="time-zone" /></label>

		<liferay-ui:input-time-zone name="timeZoneId" value="<%= timeZoneId %>" />
	</div>

	<div class="ctrl-holder">
		<label for="<portlet:namespace />greeting"><liferay-ui:message key="greeting" /></label>

		<liferay-ui:input-field  model="<%= User.class %>" bean="<%= selUser %>" field="greeting" />
	</div>
</fieldset>