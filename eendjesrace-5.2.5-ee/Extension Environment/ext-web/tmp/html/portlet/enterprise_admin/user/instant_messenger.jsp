<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/enterprise_admin/init.jsp" %>

<%
Contact selContact = (Contact)request.getAttribute("user.selContact");
%>

<h3><liferay-ui:message key="instant-messenger" /></h3>

<c:choose>
	<c:when test="<%= selContact != null %>">
		<fieldset class="block-labels">
			<div class="ctrl-holder">
				<label for="<portlet:namespace />aimSn"><liferay-ui:message key="aim" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="aimSn" />
			</div>

			<div class="ctrl-holder">
				<label for="<portlet:namespace />icqSn"><liferay-ui:message key="icq" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="icqSn" />

				<c:if test="<%= Validator.isNotNull(selContact.getIcqSn()) %>">
					<img class="instant-messenger-logo" src="http://web.icq.com/whitepages/online?icq=<%= selContact.getIcqSn() %>&img=5" />
				</c:if>
			</div>

			<div class="ctrl-holder">
				<label for="<portlet:namespace />jabberSn">	<liferay-ui:message key="jabber" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="jabberSn" />
			</div>

			<div class="ctrl-holder">
				<label for="<portlet:namespace />msnSn"><liferay-ui:message key="msn" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="msnSn" />
			</div>

			<div class="ctrl-holder">
				<label for="<portlet:namespace />skypeSn"><liferay-ui:message key="skype" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="skypeSn" />

				<c:if test="<%= Validator.isNotNull(selContact.getSkypeSn()) %>">
					<a href="callto://<%= selContact.getSkypeSn() %>"><img alt="<liferay-ui:message key="skype" />" class="instant-messenger-logo" src="http://mystatus.skype.com/smallicon/<%= selContact.getSkypeSn() %>" /></a>
				</c:if>
			</div>

			<div class="ctrl-holder">
				<label for="<portlet:namespace />ymSn"><liferay-ui:message key="ym" /></label>

				<liferay-ui:input-field model="<%= Contact.class %>" bean="<%= selContact %>" field="ymSn" />

				<c:if test="<%= Validator.isNotNull(selContact.getYmSn()) %>">
					<img class="instant-messenger-logo" src="http://opi.yahoo.com/online?u=<%= selContact.getYmSn() %>&m=g&t=0" />
				</c:if>
			</div>
		</fieldset>
	</c:when>
	<c:otherwise>
		<span class="portlet-msg-info"><liferay-ui:message key="this-section-will-be-editable-after-creating-the-user" /></span>
	</c:otherwise>
</c:choose>