<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portlet/enterprise_admin/init.jsp" %>

<%
User user2 = (User)request.getAttribute("edit_user.jsp-user2");

String sectionRedirectParams = (String)request.getAttribute("edit_user.jsp-sectionRedirectParams");

SearchContainer searchContainer = new SearchContainer();

List<String> headerNames = new ArrayList<String>();

headerNames.add("name");

if (user2.getUserId() != user.getUserId()) {
	headerNames.add(StringPool.BLANK);
}

searchContainer.setHeaderNames(headerNames);

if (user2.getUserId() == user.getUserId()) {
	searchContainer.setEmptyResultsMessage("you-do-not-have-any-regular-roles");
}
else {
	searchContainer.setEmptyResultsMessage("the-user-does-not-have-any-regular-roles");
}

List results = RoleLocalServiceUtil.getUserRoles(user2.getUserId());
List resultRows = searchContainer.getResultRows();

for (int i = 0; i < results.size(); i++) {
	Role role = (Role)results.get(i);

	ResultRow row = new ResultRow(new Object[] {user2, role, currentURL + sectionRedirectParams}, role.getRoleId(), i);

	// Name

	row.addText(role.getTitle(locale));

	// Action

	if (user2.getUserId() != user.getUserId()) {
		row.addJSP("right", SearchEntry.DEFAULT_VALIGN, "/html/portlet/enterprise_admin/user_role_action.jsp");
	}

	// CSS

	row.setClassName(EnterpriseAdminUtil.getCssClassName(role));
	row.setClassHoverName(EnterpriseAdminUtil.getCssClassName(role));

	// Add result row

	resultRows.add(row);
}
%>

<input onclick="javascript:location.href = '<portlet:renderURL windowState="<%= WindowState.MAXIMIZED.toString() %>"><portlet:param name="struts_action" value="/enterprise_admin/edit_user_regular_role_assignments" /><portlet:param name="redirect" value='<%= currentURL + "&" + renderResponse.getNamespace() + "tabs2=regular-roles" %>' /><portlet:param name="p_u_i_d" value="<%= String.valueOf(user2.getUserId()) %>" /></portlet:renderURL>';" type="button" value="<liferay-ui:message key="assign-regular-roles" />" />

<br /><br />

<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />

<br />