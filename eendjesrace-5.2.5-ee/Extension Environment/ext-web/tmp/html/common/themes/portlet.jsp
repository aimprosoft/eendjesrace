<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/common/init.jsp" %>

<portlet:defineObjects />

<tiles:useAttribute id="tilesPortletContent" name="portlet_content" classname="java.lang.String" ignore="true" />
<tiles:useAttribute id="tilesPortletDecorate" name="portlet_decorate" classname="java.lang.String" ignore="true" />
<tiles:useAttribute id="tilesPortletPadding" name="portlet_padding" classname="java.lang.String" ignore="true" />

<%
Portlet portlet = (Portlet)request.getAttribute(WebKeys.RENDER_PORTLET);

PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(layout, portletDisplay.getId());

RenderResponseImpl renderResponseImpl = (RenderResponseImpl)renderResponse;

// Portlet decorate

boolean tilesPortletDecorateBoolean = GetterUtil.getBoolean(tilesPortletDecorate, true);

boolean portletDecorateDefault = false;

if (tilesPortletDecorateBoolean) {
	portletDecorateDefault = GetterUtil.getBoolean(theme.getSetting("portlet-setup-show-borders-default"), PropsValues.THEME_PORTLET_DECORATE_DEFAULT);
}

boolean portletDecorate = GetterUtil.getBoolean(portletSetup.getValue("portlet-setup-show-borders", String.valueOf(portletDecorateDefault)));

Boolean portletDecorateObj = (Boolean)renderRequest.getAttribute(WebKeys.PORTLET_DECORATE);

if (portletDecorateObj != null) {
	portletDecorate = portletDecorateObj.booleanValue();

	request.removeAttribute(WebKeys.PORTLET_DECORATE);
}

// Portlet title

String portletTitle = PortletConfigurationUtil.getPortletTitle(portletSetup, themeDisplay.getLanguageId());

if (portletDisplay.isAccess() && portletDisplay.isActive()) {
	if (Validator.isNull(portletTitle)) {
		portletTitle = renderResponseImpl.getTitle();
	}
}

if (Validator.isNull(portletTitle)) {
	ResourceBundle resourceBundle = portletConfig.getResourceBundle(locale);

	portletTitle = resourceBundle.getString(JavaConstants.JAVAX_PORTLET_TITLE);
}

Group group = layout.getGroup();

portletDisplay.setTitle(portletTitle);

Boolean renderPortletResource = (Boolean)request.getAttribute(WebKeys.RENDER_PORTLET_RESOURCE);

boolean runtimePortlet = (renderPortletResource != null) && renderPortletResource.booleanValue();

boolean freeformPortlet = themeDisplay.isFreeformLayout() && !runtimePortlet && !layoutTypePortlet.hasStateMax();

String containerStyles = StringPool.BLANK;

StringBuilder sb = new StringBuilder();

sb.append("style=\"");

if (freeformPortlet) {
	Properties freeformStyleProps = PropertiesUtil.load(portletSetup.getValue("portlet-freeform-styles", StringPool.BLANK));

	sb.append("height: ");
	sb.append(GetterUtil.getString(freeformStyleProps.getProperty("height"), "300px"));
	sb.append(";");
	sb.append("overflow: auto;");
}

sb.append("\"");

containerStyles = sb.toString();
%>

<c:choose>
	<c:when test="<%= themeDisplay.isFacebook() %>">
		<%@ include file="/html/common/themes/portlet_facebook.jspf" %>
	</c:when>
	<c:when test="<%= themeDisplay.isStateExclusive() %>">
		<%@ include file="/html/common/themes/portlet_content_wrapper.jspf" %>
	</c:when>
	<c:when test="<%= themeDisplay.isStatePopUp() %>">
		<div>
			<c:if test="<%= Validator.isNotNull(tilesPortletContent) %>">
				<liferay-util:include page="<%= StrutsUtil.TEXT_HTML_DIR + tilesPortletContent %>" />
			</c:if>

			<c:if test="<%= Validator.isNull(tilesPortletContent) %>">

				<%
				pageContext.getOut().print(renderRequest.getAttribute(WebKeys.PORTLET_CONTENT));
				%>

			</c:if>
		</div>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="<%= portletDecorate %>">
				<liferay-theme:wrap-portlet page="portlet.jsp">
					<div class="portlet-content-container" <%= containerStyles %>>
						<%@ include file="/html/common/themes/portlet_content_wrapper.jspf" %>
					</div>
				</liferay-theme:wrap-portlet>

				<c:if test="<%= freeformPortlet && LayoutPermissionUtil.contains(permissionChecker, layout, ActionKeys.UPDATE) %>">
					<div class="ui-resizable-handle ui-resizable-se"></div>
				</c:if>
			</c:when>
			<c:otherwise>
				<div class="portlet-borderless-container" <%= containerStyles %>>
					<c:if test="<%= (tilesPortletDecorateBoolean && portletDisplay.isShowConfigurationIcon()) || portletDisplay.isShowBackIcon() %>">
						<div class="portlet-borderless-bar">
							<c:if test="<%= tilesPortletDecorateBoolean && portletDisplay.isShowConfigurationIcon() %>">
								<span class="portlet-title-default"><%= portletDisplay.getTitle() %></span>

								<span class="portlet-actions">
									<span class="portlet-css lfr-js-required">
										- <a href="<%= portletDisplay.getURLPortletCss() %>"><liferay-ui:message key="look-and-feel" /></a>
									</span>

									<span class="portlet-configuration">
										- <a href="<%= portletDisplay.getURLConfiguration() %>"><liferay-ui:message key="configuration" /></a>
									</span>

									<c:if test="<%= portletDisplay.isShowEditIcon() %>">
										<span class="portlet-edit">
											- <a href="<%= portletDisplay.getURLEdit() %>"><liferay-ui:message key="preferences" /></a>
										</span>
									</c:if>

									<c:if test="<%= portletDisplay.isShowCloseIcon() %>">
										<span class="portlet-close">
											- <a href="<%= portletDisplay.getURLClose() %>"><liferay-ui:message key="close" /></a>
										</span>
									</c:if>
								</span>
							</c:if>

							<c:if test="<%= portletDisplay.isShowBackIcon() %>">
								- <a href="<%= portletDisplay.getURLBack() %>"><liferay-ui:message key="back" /></a>
							</c:if>
						</div>
					</c:if>

					<%@ include file="/html/common/themes/portlet_content_wrapper.jspf" %>
				</div>

				<c:if test="<%= freeformPortlet %>">
					<div class="portlet-resize-container">
						<div class="portlet-resize-handle"></div>
					</div>
				</c:if>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>