<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/common/init.jsp" %>

<%-- Portal JavaScript --%>

<c:if test="<%= themeDisplay.isIncludeCalendarJs() %>">

	<%
	String[] calendarDays = new String[CalendarUtil.DAYS_ABBREVIATION.length];

	Calendar cal = CalendarFactoryUtil.getCalendar(timeZone, locale);

	for (int i = 0; i < CalendarUtil.DAYS_ABBREVIATION.length; i++) {
		calendarDays[i] = LanguageUtil.get(pageContext, CalendarUtil.DAYS_ABBREVIATION[i]);
	}
	%>

	<script type="text/javascript">
		// <![CDATA[
			jQuery.datepicker.setDefaults(
				{
					clearText: '<%= UnicodeLanguageUtil.get(pageContext, "clear") %>',
					clearStatus: '<%= UnicodeLanguageUtil.get(pageContext, "erase-the-current-date") %>',
					closeText: '<%= UnicodeLanguageUtil.get(pageContext, "close") %>',
					closeStatus: '<%= UnicodeLanguageUtil.get(pageContext, "cancel") %>',
					prevText: '&#x3c;<%= UnicodeLanguageUtil.get(pageContext, "previous") %>',
					prevStatus: '<%= UnicodeLanguageUtil.get(pageContext, "previous") %>',
					nextText: '<%= UnicodeLanguageUtil.get(pageContext, "next") %>&#x3e;',
					nextStatus: '<%= UnicodeLanguageUtil.get(pageContext, "next") %>',
					currentText: '<%= UnicodeLanguageUtil.get(pageContext, "today") %>',
					currentStatus: '<%= UnicodeLanguageUtil.get(pageContext, "current-month") %>',
					monthNames: <%= JS.toScript(CalendarUtil.getMonths(locale)) %>,
					monthNamesShort: <%= JS.toScript(CalendarUtil.getMonths(locale, "MMM")) %>,
					monthStatus: '<%= UnicodeLanguageUtil.get(pageContext, "show-a-different-month") %>',
					yearStatus: '<%= UnicodeLanguageUtil.get(pageContext, "show-a-different-year") %>',
					weekHeader: '<%= UnicodeLanguageUtil.get(pageContext, "week-abbreviation") %>',
					weekStatus: '<%= UnicodeLanguageUtil.get(pageContext, "wekk-of-the-year") %>',
					dayNames: <%= JS.toScript(CalendarUtil.getDays(locale)) %>,
					dayNamesShort: <%= JS.toScript(CalendarUtil.getDays(locale, "EEE")) %>,
					dayNamesMin: <%= JS.toScript(calendarDays) %>,
					dayStatus: '',
					dateStatus: '',
					dateFormat: 'mm/dd/yy',
					firstDay: <%= (cal.getFirstDayOfWeek() - 1) % 7 %>,
					initStatus: '<%= UnicodeLanguageUtil.get(pageContext, "select-date") %>',
					isRTL: ('<liferay-ui:message key="lang.dir" />' === 'rtl')
				}
			);
		// ]]>
	</script>
</c:if>

<c:if test="<%= themeDisplay.isIncludePortletCssJs() %>">

	<%
	long javaScriptLastModified = ServletContextUtil.getLastModified(application, "/html/js", true);
	%>

	<script src="<%= HtmlUtil.escape(PortalUtil.getStaticResourceURL(request, themeDisplay.getPathJavaScript() + "/liferay/portlet_css.js", javaScriptLastModified)) %>" type="text/javascript"></script>
</c:if>

<%-- Portlet CSS and JavaScript References --%>

<%
List<Portlet> portlets = (List<Portlet>)request.getAttribute(WebKeys.LAYOUT_PORTLETS);
%>

<c:if test="<%= portlets != null %>">

	<%
	Set<String> footerPortalCssSet = new LinkedHashSet<String>();

	for (Portlet portlet : portlets) {
		for (String footerPortalCss : portlet.getFooterPortalCss()) {
			if (!HttpUtil.hasProtocol(footerPortalCss)) {
				footerPortalCss = PortalUtil.getStaticResourceURL(request, request.getContextPath() + footerPortalCss, portlet.getTimestamp());
			}

			if (!footerPortalCssSet.contains(footerPortalCss)) {
				footerPortalCssSet.add(footerPortalCss);
	%>

				<link href="<%= HtmlUtil.escape(footerPortalCss) %>" rel="stylesheet" type="text/css" />

	<%
			}
		}
	}

	Set<String> footerPortletCssSet = new LinkedHashSet<String>();

	for (Portlet portlet : portlets) {
		for (String footerPortletCss : portlet.getFooterPortletCss()) {
			if (!HttpUtil.hasProtocol(footerPortletCss)) {
				footerPortletCss = PortalUtil.getStaticResourceURL(request, portlet.getContextPath() + footerPortletCss, portlet.getTimestamp());
			}

			if (!footerPortletCssSet.contains(footerPortletCss)) {
				footerPortletCssSet.add(footerPortletCss);
	%>

				<link href="<%= HtmlUtil.escape(footerPortletCss) %>" rel="stylesheet" type="text/css" />

	<%
			}
		}
	}

	Set<String> footerPortalJavaScriptSet = new LinkedHashSet<String>();

	for (Portlet portlet : portlets) {
		for (String footerPortalJavaScript : portlet.getFooterPortalJavaScript()) {
			if (!HttpUtil.hasProtocol(footerPortalJavaScript)) {
				footerPortalJavaScript = PortalUtil.getStaticResourceURL(request, request.getContextPath() + footerPortalJavaScript, portlet.getTimestamp());
			}

			if (!footerPortalJavaScriptSet.contains(footerPortalJavaScript) && !themeDisplay.isIncludedJs(footerPortalJavaScript)) {
				footerPortalJavaScriptSet.add(footerPortalJavaScript);
	%>

				<script src="<%= HtmlUtil.escape(footerPortalJavaScript) %>" type="text/javascript"></script>

	<%
			}
		}
	}

	Set<String> footerPortletJavaScriptSet = new LinkedHashSet<String>();

	for (Portlet portlet : portlets) {
		for (String footerPortletJavaScript : portlet.getFooterPortletJavaScript()) {
			if (!HttpUtil.hasProtocol(footerPortletJavaScript)) {
				footerPortletJavaScript = PortalUtil.getStaticResourceURL(request, portlet.getContextPath() + footerPortletJavaScript, portlet.getTimestamp());
			}

			if (!footerPortletJavaScriptSet.contains(footerPortletJavaScript)) {
				footerPortletJavaScriptSet.add(footerPortletJavaScript);
	%>

				<script src="<%= HtmlUtil.escape(footerPortletJavaScript) %>" type="text/javascript"></script>

	<%
			}
		}
	}
	%>

</c:if>

<%-- Raw Text --%>

<%
StringBuilder pageBottomSB = (StringBuilder)request.getAttribute(WebKeys.PAGE_BOTTOM);
%>

<c:if test="<%= pageBottomSB != null %>">
	<%= pageBottomSB.toString() %>
</c:if>

<%-- Theme JavaScript --%>

<script src="<%= HtmlUtil.escape(PortalUtil.getStaticResourceURL(request, themeDisplay.getPathThemeJavaScript() + "/javascript.js")) %>" type="text/javascript"></script>

<c:if test="<%= layout != null %>">

	<%-- User Inputted Layout JavaScript --%>

	<%
	UnicodeProperties typeSettings = layout.getTypeSettingsProperties();
	%>

	<script type="text/javascript">
		// <![CDATA[
			<%= typeSettings.getProperty("javascript-1") %>
			<%= typeSettings.getProperty("javascript-2") %>
			<%= typeSettings.getProperty("javascript-3") %>
		// ]]>
	</script>

	<%-- Google Analytics --%>

	<%
	UnicodeProperties groupTypeSettings = layout.getGroup().getTypeSettingsProperties();

	String googleAnalyticsId = groupTypeSettings.getProperty("googleAnalyticsId");

	if (Validator.isNotNull(googleAnalyticsId)) {
	%>

		<script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>

		<script type="text/javascript">
			var pageTracker = _gat._getTracker("<%= googleAnalyticsId %>");

			pageTracker._trackPageview();
		</script>

	<%
	}
	%>

</c:if>

<%@ include file="/html/common/themes/session_timeout.jspf" %>

<liferay-util:include page="/html/common/themes/bottom-ext.jsp" />

<c:if test='<%= permissionChecker.isOmniadmin() && (request.getAttribute("LICENSE_UPDATE") == null) && (application.getAttribute("LICENSE_VALID") != null) %>'>

	<%
	Properties licenseKeyProperties = (Properties)application.getAttribute("LICENSE_KEY_PROPERTIES");

	long now = System.currentTimeMillis();

	/*Calendar cal = new java.util.GregorianCalendar();

	cal.add(Calendar.DATE, 25);

	now = cal.getTime().getTime();*/

	String licenseKeyServerId = GetterUtil.getString(licenseKeyProperties.getProperty("serverId"));
	long startDate = GetterUtil.getLong(licenseKeyProperties.getProperty("startDate"));
	long lifetime = GetterUtil.getLong(licenseKeyProperties.getProperty("lifetime"));
	long lifetimeDays = lifetime / Time.DAY;
	long expirationDate = startDate + lifetime;
	long expirationDays = (expirationDate - now) / Time.DAY;
	%>

	<c:choose>
		<c:when test="<%= (startDate + lifetime) < now %>">
			<div class="popup-alert-warning">
				<a href="<%= themeDisplay.getPathMain() %>/portal/ee/update_license">Your license key has expired. Please update your license key to continue using Liferay Portal Enterprise Edition.</a>
			</div>
		</c:when>
		<c:when test="<%= ((lifetimeDays == 30) && (expirationDays < 7)) ||
						 ((lifetimeDays > 30) && (expirationDays < 30)) %>">

			<div class="popup-alert-notice">
				<a href="<%= themeDisplay.getPathMain() %>/portal/ee/update_license">Update your license key because it will expire in <%= expirationDays %> days.</a>
			</div>
		</c:when>
	</c:choose>
</c:if>