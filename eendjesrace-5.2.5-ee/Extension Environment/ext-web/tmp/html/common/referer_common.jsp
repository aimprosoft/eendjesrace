<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/common/init.jsp" %>

<%
String referer = null;

String refererParam = request.getParameter(WebKeys.REFERER);

if (Validator.isNotNull(refererParam) && HttpUtil.hasDomain(refererParam)) {
	try {
		String securityMode = PropsValues.REFERER_URL_SECURITY_MODE;

		String domain = StringUtil.split(HttpUtil.getDomain(refererParam), StringPool.COLON)[0];

		if (securityMode.equals("domain")) {
			String[] allowedDomains = PropsValues.REFERER_URL_DOMAINS_ALLOWED;

			if ((allowedDomains.length > 0) && !ArrayUtil.contains(allowedDomains, domain)) {
				refererParam = null;
			}
		}
		else if (securityMode.equals("ip")) {
			String[] allowedIps = PropsValues.REFERER_URL_IPS_ALLOWED;

			String serverIp = request.getServerName();

			InetAddress inetAddress = InetAddress.getByName(domain);

			if ((allowedIps.length > 0) && !ArrayUtil.contains(allowedIps, inetAddress.getHostAddress())) {
				if (!serverIp.equals(inetAddress.getHostAddress()) || !ArrayUtil.contains(allowedIps, "SERVER_IP")) {
					refererParam = null;
				}
			}
		}
	}
	catch (UnknownHostException uhe) {
		refererParam = null;
	}
}

String refererRequest = (String)request.getAttribute(WebKeys.REFERER);
String refererSession = (String)session.getAttribute(WebKeys.REFERER);

if ((refererParam != null) && (!refererParam.equals(StringPool.NULL)) && (!refererParam.equals(StringPool.BLANK))) {
	referer = refererParam;
}
else if ((refererRequest != null) && (!refererRequest.equals(StringPool.NULL)) && (!refererRequest.equals(StringPool.BLANK))) {
	referer = refererRequest;
}
else if ((refererSession != null) && (!refererSession.equals(StringPool.NULL)) && (!refererSession.equals(StringPool.BLANK))) {
	referer = refererSession;
}
else {
	if (referer == null) {
		referer = themeDisplay.getPathMain();
	}
}
%>