<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/taglib/init.jsp" %>

<%
String title = (String)request.getAttribute("liferay-ui:panel:title");
String id = (String)request.getAttribute("liferay-ui:panel:id");
boolean collapsible = GetterUtil.getBoolean((String)request.getAttribute("liferay-ui:panel:collapsible"));
String defaultState = (String)request.getAttribute("liferay-ui:panel:defaultState");
boolean persistState = GetterUtil.getBoolean((String)request.getAttribute("liferay-ui:panel:persistState"));
boolean extended = GetterUtil.getBoolean((String)request.getAttribute("liferay-ui:panel:extended"));
String cssClass = (String)request.getAttribute("liferay-ui:panel:cssClass");

IntegerWrapper panelCount = (IntegerWrapper)request.getAttribute("liferay-ui:panel-container:panel-count");

if (panelCount != null) {
	panelCount.increment();

	Boolean panelContainerExtended = (Boolean)request.getAttribute("liferay-ui:panel-container:extended");

	if (panelContainerExtended != null) {
		extended = panelContainerExtended.booleanValue();
	}
}

String panelState = GetterUtil.getString(SessionClicks.get(request, id, null), defaultState);

if (collapsible) {
	cssClass += " lfr-collapsible";
}

if (!panelState.equals("open")) {
	cssClass += " lfr-collapsed";
}

if (extended) {
	cssClass += " lfr-extended";
}
%>