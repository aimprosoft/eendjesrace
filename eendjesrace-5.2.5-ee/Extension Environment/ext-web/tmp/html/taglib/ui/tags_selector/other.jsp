<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/taglib/ui/tags_selector/init.jsp" %>

<%
themeDisplay.setIncludeServiceJs(true);

String randomNamespace = PwdGenerator.getPassword(PwdGenerator.KEY3, 4) + StringPool.UNDERLINE;

String className = (String)request.getAttribute("liferay-ui:tags_selector:className");
long classPK = GetterUtil.getLong((String)request.getAttribute("liferay-ui:tags_selector:classPK"));
String hiddenInput = (String)request.getAttribute("liferay-ui:tags_selector:hiddenInput");
String curTags = GetterUtil.getString((String)request.getAttribute("liferay-ui:tags_selector:curTags"));

if (Validator.isNotNull(className) && (classPK > 0)) {
	List<TagsEntry> entries = TagsEntryLocalServiceUtil.getEntries(className, classPK, TagsEntryConstants.FOLKSONOMY_CATEGORY);

	curTags = ListUtil.toString(entries, "name");
}

String curTagsParam = request.getParameter(hiddenInput);

if (curTagsParam != null) {
	curTags = curTagsParam;
}
%>

<input id="<%= namespace %><%= hiddenInput %>" type="hidden" />

<span class="ui-tags empty" id="<%= randomNamespace %>tagsCategoriesSummary"></span>

<input id="<%= randomNamespace %>selectTagsCategories" type="button" value="<liferay-ui:message key="select-categories" />" />

<script type="text/javascript">
	var <%= randomNamespace %> = null;

	jQuery(
		function() {
			<%= randomNamespace %> = new Liferay.TagsCategoriesSelector(
				{
					instanceVar: "<%= randomNamespace %>",
					hiddenInput: "<%= namespace + hiddenInput %>",
					summarySpan: "<%= randomNamespace %>tagsCategoriesSummary",
					curTagsCategories: "<%= HtmlUtil.escapeJS(curTags) %>"
				}
			);
		}
	);
</script>