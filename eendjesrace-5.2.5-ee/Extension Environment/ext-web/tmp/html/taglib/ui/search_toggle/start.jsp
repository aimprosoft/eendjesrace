<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/taglib/ui/search_toggle/init.jsp" %>

<script type="text/javascript">
	var <%= id %>curClickValue = "<%= clickValue %>";

	function <%= id %>toggleSearch() {
		jQuery("#<%= id %>basic").toggle();
		jQuery("#<%= id %>advanced").toggle();

		var advancedSearchObj = jQuery("#<%= id %><%= displayTerms.ADVANCED_SEARCH %>");

		if (<%= id %>curClickValue == "basic") {
			<%= id %>curClickValue = "advanced";

			advancedSearchObj.val(true);
		}
		else {
			<%= id %>curClickValue = "basic";

			advancedSearchObj.val(false);
		}

		jQuery.ajax(
			{
				url: '<%= themeDisplay.getPathMain() %>/portal/session_click',
				data: {
					'<%= id %>': <%= id %>curClickValue
				}
			}
		);
	}
</script>

<input id="<%= id %><%= displayTerms.ADVANCED_SEARCH %>" name="<%= namespace %><%= displayTerms.ADVANCED_SEARCH %>" type="hidden" value="<%= clickValue.equals("basic") ? false : true %>" />

<div id="<%= id %>basic" style="display: <%= clickValue.equals("basic") ? "block" : "none" %>;">
	<c:choose>
		<c:when test="<%= Validator.isNotNull(buttonLabel) %>">
			<input id="<%= id %><%= displayTerms.KEYWORDS %>" name="<%= namespace %><%= displayTerms.KEYWORDS %>" size="30" type="text" value="<%= HtmlUtil.escape(displayTerms.getKeywords()) %>" />

			<input type="submit" value="<liferay-ui:message key="<%= buttonLabel %>" />" /><br />

			<a href="javascript:<%= id %>toggleSearch();" tabindex="-1"><liferay-ui:message key="advanced" /> &raquo;</a>
		</c:when>
		<c:otherwise>
			<label for="<%= id %><%= displayTerms.KEYWORDS %>"><liferay-ui:message key="search" /></label>

			<input id="<%= id %><%= displayTerms.KEYWORDS %>" name="<%= namespace %><%= displayTerms.KEYWORDS %>" size="30" type="text" value="<%= HtmlUtil.escape(displayTerms.getKeywords()) %>" />

			&nbsp;<a href="javascript:<%= id %>toggleSearch();" tabindex="-1"><liferay-ui:message key="advanced" /> &raquo;</a>
		</c:otherwise>
	</c:choose>
</div>

<div id="<%= id %>advanced" style="display: <%= !clickValue.equals("basic") ? "block" : "none" %>;">

	<%
	StringBuilder sb = new StringBuilder();

	sb.append("<select name=\"");
	sb.append(namespace);
	sb.append(displayTerms.AND_OPERATOR);
	sb.append("\">");

	sb.append("<option ");

	if (displayTerms.isAndOperator()) {
		sb.append("selected ");
	}

	sb.append("value=\"1\">");
	sb.append(LanguageUtil.get(pageContext, "all"));
	sb.append("</option>");

	sb.append("<option ");

	if (!displayTerms.isAndOperator()) {
		sb.append("selected ");
	}

	sb.append("value=\"0\">");
	sb.append(LanguageUtil.get(pageContext, "any"));
	sb.append("</option>");

	sb.append("</select>");
	%>

	<%= LanguageUtil.format(pageContext, "match-x-of-the-following-fields", sb.toString()) %>

	<br /><br />