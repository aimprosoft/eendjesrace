<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/taglib/init.jsp" %>

<%
Portlet portlet = (Portlet)request.getAttribute("liferay-portlet:icon_portlet:portlet");

boolean showPortletIcon = true;
String message = null;
String src = null;

if (portlet != null) {
	message = PortalUtil.getPortletTitle(portlet, application, locale);
	src = portlet.getContextPath() + portlet.getIcon();
}
else {
	showPortletIcon = portletDisplay.isShowPortletIcon();
	message = portletDisplay.getTitle();
	src = portletDisplay.getURLPortlet();
}
%>

<c:if test="<%= showPortletIcon %>">
	<liferay-ui:icon message="<%= message %>" src="<%= src %>" />
</c:if>