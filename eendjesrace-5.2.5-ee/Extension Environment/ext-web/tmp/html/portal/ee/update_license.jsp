<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portal/init.jsp" %>

<style type="text/css">
	.version-info, .build-info {
		margin: 0;
	}

	.version-info {
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 2px;
	}

	.build-info {
		color: #555;
		font-size: 11px;
		margin-bottom: 15px;
	}

	tr.type owner {
		padding-top: 15px;
	}
</style>

<form action="<%= themeDisplay.getPathMain() %>/portal/ee/update_license" method="post">

<%
Properties licenseProperties = (Properties)application.getAttribute("LICENSE_PROPERTIES");

String serverId = licenseProperties.getProperty("serverId");
String licenseKey = GetterUtil.getString(licenseProperties.getProperty("licenseKey"));

Properties licenseKeyProperties = (Properties)application.getAttribute("LICENSE_KEY_PROPERTIES");

long now = System.currentTimeMillis();

/*Calendar cal = new java.util.GregorianCalendar();

cal.add(Calendar.DATE, 25);

now = cal.getTime().getTime();*/

String owner = StringPool.BLANK;
String description = StringPool.BLANK;
String type = StringPool.BLANK;
long startDate = 0;
long lifetime = 0;
long lifetimeDays = 0;
long expirationDate = 0;
long expirationDays = 0;
String licenseKeyServerId = StringPool.BLANK;

if (licenseKeyProperties != null) {
	owner = GetterUtil.getString(licenseKeyProperties.getProperty("owner"));
	description = GetterUtil.getString(licenseKeyProperties.getProperty("description"));
	type = GetterUtil.getString(licenseKeyProperties.getProperty("type"), "production");
	startDate = GetterUtil.getLong(licenseKeyProperties.getProperty("startDate"));
	lifetime = GetterUtil.getLong(licenseKeyProperties.getProperty("lifetime"));
	lifetimeDays = lifetime / Time.DAY;
	expirationDate = startDate + lifetime;
	expirationDays = (expirationDate - now) / Time.DAY;
	licenseKeyServerId = GetterUtil.getString(licenseKeyProperties.getProperty("serverId"));
}

boolean errorServerId = request.getAttribute("LICENSE_PROPERTIES_ERROR_SERVER_ID") != null;
boolean errorLifetime = request.getAttribute("LICENSE_PROPERTIES_ERROR_LIFETIME") != null;

String[] releaseInfoArray = StringUtil.split(ReleaseInfo.getReleaseInfo(), "(");

String versionInfo = releaseInfoArray[0];
String buildInfo = StringUtil.replace(releaseInfoArray[1], ")", "");

DateFormat dateFormatDateTime = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

dateFormatDateTime.setTimeZone(timeZone);
%>

<h2 class="version-info">
	<%= versionInfo %>
</h2>

<h3 class="build-info">
	<%= buildInfo %>
</h3>

<c:choose>
	<c:when test="<%= (startDate == 0) || (lifetime == 0) || errorServerId || errorLifetime %>">
		<div class="portlet-msg-info">
			<a href="http://www.liferay.com?serverId=<%= serverId %>" target="_blank">Get a license key to activate Liferay Portal Enterprise Edition.</a>
		</div>
	</c:when>
	<c:when test="<%= ((lifetimeDays == 30) && (expirationDays < 7)) ||
					   ((lifetimeDays > 30) && (expirationDays < 30)) %>">

		<div class="portlet-msg-error">
			<a href="http://www.liferay.com/get_license?serverId=<%= serverId %>" target="_blank">Update your license key because it will expire in <%= expirationDays %> days.</a>
		</div>
	</c:when>
</c:choose>

<c:if test='<%= request.getAttribute("LICENSE_PROPERTIES_SUCCESS") != null %>'>
	<div class="portlet-msg-success">
		Thank you for activating Liferay Portal Enterprise Edition.
	</div>
</c:if>

<c:if test="<%= errorServerId %>">
	<div class="portlet-msg-error">
		Your license key is registered to <%= licenseKeyServerId %> and is not valid for this server.
	</div>
</c:if>

<c:if test="<%= errorLifetime %>">
	<div class="portlet-msg-error">
		Your license key has expired.
	</div>
</c:if>

<table class="lfr-table">
<tr>
	<td>
		<liferay-ui:message key="server-id" />
	</td>
	<td>
		<%= serverId %>
	</td>
</tr>
<tr>
	<td>
		<liferay-ui:message key="license-key" />
	</td>
	<td>
		<textarea class="lfr-textarea" name="licenseKey"><%= licenseKey %></textarea>
	</td>
</tr>

<c:if test="<%= licenseKeyProperties != null %>">
	<tr class="owner">
		<td>
			<liferay-ui:message key="owner" />
		</td>
		<td>
			<liferay-ui:message key="<%= owner %>" />
		</td>
	</tr>
	<tr class="description">
		<td>
			<liferay-ui:message key="description" />
		</td>
		<td>
			<liferay-ui:message key="<%= description %>" />
		</td>
	</tr>
	<tr class="type">
		<td>
			<liferay-ui:message key="type" />
		</td>
		<td>
			<liferay-ui:message key="<%= type %>" />
		</td>
	</tr>
	<tr class="start-date">
		<td>
			<liferay-ui:message key="start-date" />
		</td>
		<td>
			<%= dateFormatDateTime.format(new Date(startDate)) %>
		</td>
	</tr>
	<tr class="end-date">
		<td>
			<liferay-ui:message key="expiration-date" />
		</td>
		<td>
			<%= dateFormatDateTime.format(new Date(expirationDate)) %>
		</td>
	</tr>
</c:if>

</table>

<br />

<input type="submit" value="<liferay-ui:message key="save" />" />

</form>