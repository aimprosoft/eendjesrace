<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portal/init.jsp" %>

<%
String uploadProgressId = ParamUtil.getString(request, "uploadProgressId");

String fileName = GetterUtil.getString((String)session.getAttribute(LiferayFileUpload.FILE_NAME + uploadProgressId));

if (fileName == null) {
	fileName = GetterUtil.getString((String)session.getAttribute(LiferayFileUpload.FILE_NAME));
}

Integer percent = (Integer)session.getAttribute(LiferayFileUpload.PERCENT + uploadProgressId);

if (percent == null) {
	percent = (Integer)session.getAttribute(LiferayFileUpload.PERCENT);
}
if (percent == null) {
	percent = new Integer(100);
}

if (percent.floatValue() >= 100) {
	session.removeAttribute(LiferayFileUpload.FILE_NAME);
	session.removeAttribute(LiferayFileUpload.FILE_NAME + uploadProgressId);
	session.removeAttribute(LiferayFileUpload.PERCENT);
	session.removeAttribute(LiferayFileUpload.PERCENT + uploadProgressId);
}
%>

<html>

<body>

<script type="text/javascript">
	parent.<%= HtmlUtil.escape(uploadProgressId) %>.updateBar(<%= percent.intValue() %>, "<%= fileName %>");

	<c:if test="<%= percent.intValue() < 100 %>">
		setTimeout("window.location.reload();", 1000);
	</c:if>
</script>

</body>

</html>