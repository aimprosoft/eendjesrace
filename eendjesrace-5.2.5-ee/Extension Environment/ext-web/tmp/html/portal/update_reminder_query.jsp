<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portal/init.jsp" %>

<form action="<%= themeDisplay.getPathMain() %>/portal/update_reminder_query" class="uni-form" method="post" name="fm" onSubmit="submitForm(document.fm); return false;">
<input name="doAsUserId" type="hidden" value="<%= HtmlUtil.escapeAttribute(themeDisplay.getDoAsUserId()) %>" />
<input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
<input name="<%= WebKeys.REFERER %>" type="hidden" value="<%= themeDisplay.getPathMain() %>?doAsUserId=<%= HtmlUtil.escapeAttribute(themeDisplay.getDoAsUserId()) %>" />

<span class="portlet-msg-info">
	<liferay-ui:message key="please-choose-a-reminder-query" />
</span>

<c:if test="<%= SessionErrors.contains(request, UserReminderQueryException.class.getName()) %>">
	<span class="portlet-msg-error">
		<liferay-ui:message key="reminder-query-and-answer-cannot-be-empty" />
	</span>
</c:if>

<fieldset class="block-labels">
	<legend><liferay-ui:message key="password-reminder" /></legend>

	<div class="ctrl-holder">
		<label for="reminderQueryQuestion"><liferay-ui:message key="question" /></label>

		<select id="reminderQueryQuestion" name="reminderQueryQuestion">

			<%
			for (String question : user.getReminderQueryQuestions()) {
			%>

				<option value="<%= question %>"><liferay-ui:message key="<%= question %>" /></option>

			<%
			}
			%>

			<c:if test="<%= PropsValues.USERS_REMINDER_QUERIES_CUSTOM_QUESTION_ENABLED %>">
				<option value="<%= EnterpriseAdminUtil.CUSTOM_QUESTION %>"><liferay-ui:message key="write-my-own-question" /></option>
			</c:if>
		</select>

		<c:if test="<%= PropsValues.USERS_REMINDER_QUERIES_CUSTOM_QUESTION_ENABLED %>">
			<div id="customQuestionDiv">
				<liferay-ui:input-field model="<%= User.class %>" bean="<%= user %>" field="reminderQueryQuestion"  fieldParam="reminderQueryCustomQuestion" />
			</div>
		</c:if>
	</div>

	<div class="ctrl-holder">
		<label for="reminderQueryAnswer"><liferay-ui:message key="answer" /></label>

		<input id="reminderQueryAnswer" name="reminderQueryAnswer" size="50" type="text" value="<%= HtmlUtil.escapeAttribute(user.getReminderQueryAnswer()) %>" />
	</div>
</fieldset>

<input type="submit" value="<liferay-ui:message key="save" />" />

</form>

<script type="text/javascript">
	jQuery(
		function() {
			var reminderQueryQuestion = jQuery('#reminderQueryQuestion');
			var customQuestionDiv = jQuery('#customQuestionDiv');

			if (reminderQueryQuestion.val() != '<%= EnterpriseAdminUtil.CUSTOM_QUESTION %>') {
				customQuestionDiv.hide();
			}

			reminderQueryQuestion.change(
				function(event) {
					if (this.value == '<%= EnterpriseAdminUtil.CUSTOM_QUESTION %>') {
						<c:if test="<%= PropsValues.USERS_REMINDER_QUERIES_CUSTOM_QUESTION_ENABLED %>">
							customQuestionDiv.show();

							Liferay.Util.focusFormField(jQuery('#reminderQueryCustomQuestion'));
						</c:if>
					}
					else {
						customQuestionDiv.hide();

						Liferay.Util.focusFormField(jQuery('#reminderQueryAnswer'));
					}
				}
			);

			Liferay.Util.focusFormField(reminderQueryQuestion);
		}
	);
</script>