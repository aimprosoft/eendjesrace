<%@ page import="com.liferay.portal.util.TreeView" %>
<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portal/layout/edit/init.jsp" %>

<table class="lfr-table">
<tr>
	<td>
		<liferay-ui:message key="description" />
	</td>
	<td>
		<textarea class="lfr-textarea" name="TypeSettingsProperties(description)" wrap="soft"><bean:write name="SEL_LAYOUT" property="typeSettingsProperties(description)" /></textarea>
	</td>
</tr>
</table>

<br />

<div class="portlet-msg-info">
	<liferay-ui:message key="select-the-applications-that-will-be-available-in-the-panel" />
</div>

<input name="TypeSettingsProperties(panelSelectedPortlets)" type="hidden" value="<bean:write name="SEL_LAYOUT" property="typeSettingsProperties(panelSelectedPortlets)" />" />

<%
String panelTreeKey = "panelSelectedPortletsPanelTree";
%>

<script src="<%= themeDisplay.getPathJavaScript() %>/liferay/tree.js" type="text/javascript"></script>

<div id="<portlet:namespace />panel-select-portlets-output" style="margin: 4px;"></div>

<script type="text/javascript">
	var <portlet:namespace />treeIcons = {
		checkbox: '<%= themeDisplay.getPathThemeImages() %>/trees/checkbox.png',
		checked: '<%= themeDisplay.getPathThemeImages() %>/trees/checked.png',
		childChecked: '<%= themeDisplay.getPathThemeImages() %>/trees/child_checked.png',
		minus: '<%= themeDisplay.getPathThemeImages() %>/trees/minus.png',
		page: '<%= themeDisplay.getPathThemeImages() %>/trees/page.png',
		plus: '<%= themeDisplay.getPathThemeImages() %>/trees/plus.png',
		root: '<%= themeDisplay.getPathThemeImages() %>/trees/root.png',
		spacer: '<%= themeDisplay.getPathThemeImages() %>/trees/spacer.png'
	};

	var <portlet:namespace />panelPortletsArray = [];

	<%
	PortletLister portletLister = new PortletLister();

	portletLister.setIncludeInstanceablePortlets(false);

	TreeView treeView = portletLister.getTreeView(layoutTypePortlet, LanguageUtil.get(pageContext, "application"), user, application);

	Iterator itr = treeView.getList().iterator();

	for (int i = 0; itr.hasNext(); i++) {
		TreeNodeView treeNodeView = (TreeNodeView)itr.next();
	%>

 		<portlet:namespace />panelPortletsArray[<%= i %>] = {
			depth: '<%= treeNodeView.getDepth() %>',
			id: '<%= treeNodeView.getId() %>',
			img: '<%= treeNodeView.getImg() %>',
			ls: '<%= treeNodeView.getLs() %>',
			href: '<%= treeNodeView.getHref() %>',
			parentId: '<%= treeNodeView.getParentId() %>',
			objId: '<%= treeNodeView.getObjId() %>',
			name: '<%= UnicodeFormatter.toString(treeNodeView.getName()) %>'
		};

	<%
	}
	%>

	jQuery(
		function() {
			new Liferay.Tree(
				{
					className: "lfr-tree",
					icons: <portlet:namespace />treeIcons,
					nodes: <portlet:namespace />panelPortletsArray,
					openNodes: '<bean:write name="SEL_LAYOUT" property="typeSettingsProperties(panelSelectedPortlets)" />',
					outputId: '#<portlet:namespace />panel-select-portlets-output',
					selectable: true,
					selectedNodes: '<bean:write name="SEL_LAYOUT" property="typeSettingsProperties(panelSelectedPortlets)" />',
					treeId: '<%= panelTreeKey %>',
					onSelect: function(params) {
						var portletId = params.branchId;
						var selectedNode = params.selectedNode;

						var panelSelectedPortletsEl = document.<portlet:namespace />fm['TypeSettingsProperties(panelSelectedPortlets)'];

						if (portletId) {
							if (selectedNode) {
								panelSelectedPortletsEl.value += portletId + ',';
							}
							else {
								panelSelectedPortletsEl.value = panelSelectedPortletsEl.value.replace(portletId + ',', '');
							}
						}
					}
				}
			);
		}
	);
</script>