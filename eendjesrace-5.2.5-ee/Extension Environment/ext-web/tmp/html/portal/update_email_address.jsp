<%
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
%>

<%@ include file="/html/portal/init.jsp" %>

<%
String emailAddress1 = ParamUtil.getString(request, "emailAddress1");
String emailAddress2 = ParamUtil.getString(request, "emailAddress2");
%>

<form action="<%= themeDisplay.getPathMain() %>/portal/update_email_address" class="uni-form" method="post" name="fm" onSubmit="submitForm(document.fm); return false;">
<input name="doAsUserId" type="hidden" value="<%= HtmlUtil.escapeAttribute(themeDisplay.getDoAsUserId()) %>" />
<input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
<input name="<%= WebKeys.REFERER %>" type="hidden" value="<%= themeDisplay.getPathMain() %>?doAsUserId=<%= HtmlUtil.escapeAttribute(themeDisplay.getDoAsUserId()) %>" />

<c:choose>
	<c:when test="<%= SessionErrors.contains(request, DuplicateUserEmailAddressException.class.getName()) %>">
		<span class="portlet-msg-error">
			<liferay-ui:message key="the-email-address-you-requested-is-already-taken" />
		</span>
	</c:when>
	<c:when test="<%= SessionErrors.contains(request, ReservedUserEmailAddressException.class.getName()) %>">
		<span class="portlet-msg-error">
			<liferay-ui:message key="the-email-address-you-requested-is-reserved" />
		</span>
	</c:when>
	<c:when test="<%= SessionErrors.contains(request, UserEmailAddressException.class.getName()) %>">
		<span class="portlet-msg-error">
			<liferay-ui:message key="please-enter-a-valid-email-address" />
		</span>
	</c:when>
	<c:otherwise>
		<span class="portlet-msg-info">
			<liferay-ui:message key="please-enter-a-valid-email-address" />
		</span>
	</c:otherwise>
</c:choose>

<fieldset class="block-labels">
	<legend><liferay-ui:message key="email-address" /></legend>

	<div class="ctrl-holder">
		<label for="emailAddress1"><liferay-ui:message key="email-address" /></label>

		<input class="lfr-input-text" name="emailAddress1" type="text" value="<%= HtmlUtil.escapeAttribute(emailAddress1) %>" />
	</div>

	<div class="ctrl-holder">
		<label for="emailAddress2"><liferay-ui:message key="enter-again" /></label>

		<input class="lfr-input-text" name="emailAddress2" type="text" value="<%= HtmlUtil.escapeAttribute(emailAddress2) %>" />
	</div>
</fieldset>

<input type="submit" value="<liferay-ui:message key="save" />" />

</form>

<script type="text/javascript">
	Liferay.Util.focusFormField(document.fm.emailAddress1);
</script>