package be.lions.duckrace.reservation;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllReservationTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All Reservation Tests");

		suite.addTestSuite(CreateReservationTest.class);
		suite.addTestSuite(ApproveReservationTest.class);
		suite.addTestSuite(GetNonApprovedReservationsTest.class);
		suite.addTestSuite(ApproveTwoReservationsOfSameParticipant.class);
		suite.addTestSuite(GetApprovedReservationsTest.class);

		return suite;
	}
	
}
