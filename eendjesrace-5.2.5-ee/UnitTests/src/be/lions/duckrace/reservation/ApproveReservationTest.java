package be.lions.duckrace.reservation;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

public class ApproveReservationTest extends DuckRaceTestCase {

	private LotReservation reservation;
	
	public void testApproveLotReservation() {
		try {
			reservation = LotReservationMother.createLotReservation();
			assertFalse(reservation.isApproved());
			reservation = LotReservationLocalServiceUtil.approveReservation(reservation.getReservationId());
			assertTrue(reservation.isApproved());
			assertSize(LotLocalServiceUtil.getLotNumbersForParticipant(reservation.getParticipantId()), reservation.getNumberLots());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(reservation);
	}
	
}
