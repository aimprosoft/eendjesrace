package be.lions.duckrace.reservation;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.mother.ParticipantMother;

public class CreateReservationTest extends DuckRaceTestCase {

	private LotReservation reservation;
	
	public void testCreateReservation() {
		try {
			reservation = LotReservationMother.createLotReservation();
			assertNotNull(reservation);
			assertEquals(LotReservationMother.NUMBER_LOTS, reservation.getNumberLots());
			Participant participant = reservation.getParticipant();
			assertNotNull(participant);
			assertEquals(LotReservationMother.MOBILE_NUMBER, participant.getMobileNumber());
			assertEquals(LotReservationMother.FIRST_NAME, participant.getFirstName());
			assertEquals(LotReservationMother.LAST_NAME, participant.getLastName());
			assertEquals(LotReservationMother.EMAIL_ADDRESS, participant.getEmailAddress());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
//	public void testCreateReservationWhenMobileNumberAlreadyExists() {
//		try {
//			String firstName = "Peter2";
//			String lastName = "Mesotten2";
//			String emailAddress = "p.mesotten.2@aca-it.be";
//			Participant p = ParticipantMother.createParticipant(LotReservationMother.MOBILE_NUMBER, firstName, lastName, emailAddress);
//			reservation = LotReservationMother.createLotReservation();
//			assertNotNull(reservation);
//			assertEquals(LotReservationMother.NUMBER_LOTS, reservation.getNumberLots());
//			Participant participant = reservation.getParticipant();
//			assertNotNull(participant);
//			assertEquals(LotReservationMother.MOBILE_NUMBER, participant.getMobileNumber());
//			assertEquals(LotReservationMother.FIRST_NAME, participant.getFirstName());
//			assertEquals(LotReservationMother.LAST_NAME, participant.getLastName());
//			assertEquals(LotReservationMother.EMAIL_ADDRESS, participant.getEmailAddress());
//			ParticipantMother.deleteParticipants(p);
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(reservation);
	}
	
}
