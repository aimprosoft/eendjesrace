package be.lions.duckrace.reservation;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

public class ApproveTwoReservationsOfSameParticipant extends DuckRaceTestCase {

	private LotReservation r1, r2;
	
	public void testTwoApprovedReservations() {
		try {
			r1 = LotReservationMother.createLotReservation();
			r1 = LotReservationLocalServiceUtil.approveReservation(r1.getReservationId());
			r2 = LotReservationMother.createLotReservation();
			r2 = LotReservationLocalServiceUtil.approveReservation(r2.getReservationId());
			
			assertSize(r1.getParticipant().getLots(), 2*LotReservationMother.NUMBER_LOTS);
			assertSize(r1.getLots(), LotReservationMother.NUMBER_LOTS);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(r1, r2);
	}
	
}
