package be.lions.duckrace.reservation;

import java.util.List;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

public class GetApprovedReservationsTest extends DuckRaceTestCase {

	private LotReservation unapproved, approved1, approved2;
	
	@Override
	public void setUp() throws Exception {
		unapproved = LotReservationMother.createLotReservation();
		approved1 = LotReservationMother.createLotReservation();
		LotReservationLocalServiceUtil.approveReservation(approved1.getReservationId());
		approved2 = LotReservationMother.createLotReservation();
		LotReservationLocalServiceUtil.approveReservation(approved2.getReservationId());
	}
	
	public void testApprovedReservationIsInList() {
		try {
			List<LotReservation> approvedReservations = LotReservationLocalServiceUtil.getApprovedReservations();
			assertNotContains(approvedReservations, unapproved);
			assertContains(approvedReservations, approved1, approved2);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(unapproved, approved1, approved2);
	}
	
}
