package be.lions.duckrace.reservation;

import java.util.List;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

public class GetNonApprovedReservationsTest extends DuckRaceTestCase {

	private LotReservation approved, notApproved;
	
	public void setUp() {
		try {
			approved = LotReservationMother.createLotReservation();
			LotReservationLocalServiceUtil.approveReservation(approved.getReservationId());
			notApproved = LotReservationMother.createLotReservation();
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testOnlyTheNotApprovedReservationIsReturned() {
		try {
			List<LotReservation> reservations = LotReservationLocalServiceUtil.getUnapprovedReservations();
			assertSize(reservations, 1);
			assertContains(reservations, notApproved);
			assertNotContains(reservations, approved);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(approved, notApproved);
	}
	
}
