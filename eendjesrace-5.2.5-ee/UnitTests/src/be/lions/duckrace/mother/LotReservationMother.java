package be.lions.duckrace.mother;

import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException;
import be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException;
import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

import com.liferay.portal.SystemException;

public class LotReservationMother {

	public static final int NUMBER_LOTS = 10;
	public static final String EMAIL_ADDRESS = "p.mesotten@aca-it.be";
	public static final String LAST_NAME = "Mesotten";
	public static final String FIRST_NAME = "Peter";
	public static final String MOBILE_NUMBER = "0472514225";
	
	public static LotReservation createLotReservation() throws InvalidMobileNumberException, NoSuchParticipantException, SystemException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		return LotReservationLocalServiceUtil.createReservation(MOBILE_NUMBER, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, NUMBER_LOTS);
	}
	
	public static void deleteReservations(LotReservation... reservations) {
		for (LotReservation reservation : reservations) {
			deleteReservation(reservation);
		}
	}
	
	private static void deleteReservation(LotReservation reservation) {
		if(reservation == null) {
			return;
		}
		try {
			ParticipantMother.deleteParticipants(reservation.getParticipant());
			LotReservationLocalServiceUtil.deleteLotReservation(reservation);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
