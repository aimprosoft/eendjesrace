package be.lions.duckrace.mother;

import java.util.Locale;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;

public class UserMother {

	public static User createUser(String emailAddress) throws PortalException, SystemException {
		long companyId = CompanyLocalServiceUtil.getCompanies().get(0).getCompanyId();
		long creatorId = UserLocalServiceUtil.getDefaultUserId(companyId);
		return UserLocalServiceUtil.addUser(creatorId, companyId, true, "", "", true, "", emailAddress, "", Locale.US, 
			"test", "", "test", 0, 0, true, 1, 1, 1900, "", new long[0], new long[0], new long[0], new long[0], 
			false, new ServiceContext());
	}
	
	public static void deleteUsers(User... users) throws SystemException {
		for (User user : users) {
			UserLocalServiceUtil.deleteUser(user);
		}
	}
	
}
