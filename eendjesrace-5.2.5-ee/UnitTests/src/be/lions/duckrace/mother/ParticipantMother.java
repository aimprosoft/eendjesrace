package be.lions.duckrace.mother;

import be.lions.duckrace.DuplicateMobileNumberException;
import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException;
import be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException;
import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

import com.liferay.portal.SystemException;

public class ParticipantMother {

	public static final String EMAIL = "test@participant.be";
	public static final String LAST_NAME = "Participant";
	public static final String FIRST_NAME = "Test";
	public static final String MOBILE_NO = "32456789780";

	public static Participant createParticipantByEmail(String email) throws InvalidMobileNumberException, NoSuchParticipantException, SystemException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		return ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, email);
	}
	
	public static Participant createParticipant(String mobileNumber) throws SystemException, DuplicateMobileNumberException, InvalidMobileNumberException, NoSuchParticipantException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		return ParticipantLocalServiceUtil.getOrCreateParticipant(mobileNumber, null, null, null);
	}
	
	public static Participant createParticipant(String mobileNumber, String firstName, String lastName, String emailAddress) throws InvalidMobileNumberException, NoSuchParticipantException, SystemException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		return ParticipantLocalServiceUtil.getOrCreateParticipant(mobileNumber, firstName, lastName, emailAddress);
	}
	
	public static void deleteParticipants(Participant... participants) {
		for (Participant participant : participants) {
			deleteParticipant(participant);
		}
	}
	
	private static void deleteParticipant(Participant participant) {
		if(participant == null) {
			return;
		}
		try {
			ParticipantLocalServiceUtil.deleteParticipant(participant);			
			for (Lot lot: participant.getLots()) {
				LotMother.resetLots(lot);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
