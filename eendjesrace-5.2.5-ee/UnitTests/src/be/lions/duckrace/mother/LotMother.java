package be.lions.duckrace.mother;

import com.liferay.portal.SystemException;

import be.lions.duckrace.model.Lot;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class LotMother {

	public static Lot createLot(String code, String checksum) throws SystemException {
		return LotLocalServiceUtil.createLot(code, checksum);
	}
	
	public static void resetLots(Lot... lots) {
		for(Lot lot: lots) {
			resetLot(lot);
		}
	}
	
	private static void resetLot(Lot lot) {
		if(lot == null) {
			return;
		}
		try {
			lot.setNbTries(0);
			lot.setNumber(0);
			lot.setLockoutDate(null);
			lot = LotLocalServiceUtil.updateLot(lot, false);
			if(lot.getParticipant() != null) {
				ParticipantLocalServiceUtil.deleteParticipant(lot.getParticipantId());
			}
			lot.setParticipantId(0);
			LotLocalServiceUtil.updateLot(lot, false);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
