package be.lions.duckrace.participant;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllParticipantTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All Participant Tests");

		suite.addTestSuite(CreateParticipantTest.class);
		suite.addTestSuite(GetParticipantByMobileNbTest.class);
		suite.addTestSuite(CreateTwoParticipantsWithDifferentDataTest.class);
		suite.addTestSuite(TransactionSafeEmailAddressTest.class);
		suite.addTestSuite(UpdateParticipantTest.class);
		suite.addTestSuite(SearchParticipantsTest.class);

		return suite;
	}
	
}
