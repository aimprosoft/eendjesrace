package be.lions.duckrace.participant;

import com.liferay.portal.SystemException;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class GetParticipantByMobileNbTest extends DuckRaceTestCase {

	private static final String MOBILE_NB = "32472514225";
	private Participant participant;
	
	@Override
	public void setUp() throws Exception {
		participant = ParticipantMother.createParticipant(MOBILE_NB);
	}
	
	public void testNoSuchParticipantReturnsException() {
		try {
			ParticipantLocalServiceUtil.getParticipantByMobileNumber("non-existent");
		} catch(NoSuchParticipantException e) {
			// expected
		} catch(SystemException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testExistingParticipantReturnsTheRightOne() {
		try {
			Participant dbParticipant = ParticipantLocalServiceUtil.getParticipantByMobileNumber(MOBILE_NB);
			assertEquals(participant, dbParticipant);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(participant);
	}
	
}
