package be.lions.duckrace.participant;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;

public class CreateParticipantTest extends DuckRaceTestCase {

	private Participant p1, p2;
	
	public void testCreateParticipantWithValidMobileNumber() {
		try {
			String mobileNo = "32472514225";
			p1 = ParticipantMother.createParticipant(mobileNo);
			assertEquals(mobileNo, p1.getMobileNumber());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testCreateParticipantWithInvalidMobileNumberThrowsException() {
		try {
			p1 = ParticipantMother.createParticipant("mmmlmsqdfnj");
			fail();
		} catch(InvalidMobileNumberException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(p1, p2);
	}
	
}
