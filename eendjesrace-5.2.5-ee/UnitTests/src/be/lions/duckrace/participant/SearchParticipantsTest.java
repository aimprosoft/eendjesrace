package be.lions.duckrace.participant;

import java.util.List;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class SearchParticipantsTest extends DuckRaceTestCase {

	private Participant p1, p2, p3;
	
	@Override
	public void setUp() throws Exception {
		p1 = ParticipantMother.createParticipant("0111111111", "Albert", "Arnouts", "a.arnouts@aa.be");
		p2 = ParticipantMother.createParticipant("0222222222", "Bart", "Bernardus", "b.bernardus@bb.be");
		p3 = ParticipantMother.createParticipant("0333333333", "Cyril", "Cornelis", "c.cornelis@cc.be");		
	}
	
	public void testSearchWithNoParameters() {
		try {
			List<Participant> result = ParticipantLocalServiceUtil.searchParticipants(null, "", null, "");
			assertNotEmpty(result);
			assertSize(result, 3);
			assertContains(result, p1, p2, p3);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testSearchWithFalseParameters() {
		try {
			List<Participant> result = ParticipantLocalServiceUtil.searchParticipants(null, "Peter", null, "");
			assertEmpty(result);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testSearchWithMultipleResults() {
		try {
			List<Participant> result = ParticipantLocalServiceUtil.searchParticipants(null, "rt", null, "");
			assertSize(result, 2);
			assertContains(result, p1, p2);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testSearchWithOneResult() {
		try {
			List<Participant> result = ParticipantLocalServiceUtil.searchParticipants("111", "Albert", "nout", "aa");
			assertSize(result, 1);
			assertContains(result, p1);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(p1, p2, p3);
	}
	
}
