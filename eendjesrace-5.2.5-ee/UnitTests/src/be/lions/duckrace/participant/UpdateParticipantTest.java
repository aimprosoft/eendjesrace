package be.lions.duckrace.participant;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.DuplicateMobileNumberException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class UpdateParticipantTest extends DuckRaceTestCase {

	private Participant old1, old2;
	
	@Override
	public void setUp() throws Exception {
		old1 = ParticipantMother.createParticipant("0472514225", "Peter", "Mesotten", "p.mesotten@aca-it.be");
	}
	
	public void testUpdateParticipantWithNonExistingMobileNumber() {
		String newMobileNumber = "0472514278";
		String newFirstName = "Bart";
		String newLastName = "Stroken";
		String newEmailAddress = "pmes@limburgie.be";
		try {
			Participant _new = ParticipantLocalServiceUtil.updateParticipant(
				old1.getParticipantId(), newMobileNumber, newEmailAddress, newFirstName, newLastName);
			assertEquals(newMobileNumber, _new.getMobileNumber());
			assertEquals(newFirstName, _new.getFirstName());
			assertEquals(newLastName, _new.getLastName());
			assertEquals(newEmailAddress, _new.getEmailAddress());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testUpdateParticipantToExistingMobileNumber() {
		try {
			old2 = ParticipantMother.createParticipant("0456768978", "Test", "Test", "test@example.com");
			ParticipantLocalServiceUtil.updateParticipant(
				old2.getParticipantId(), old1.getMobileNumber(), "Test", "Test", "test@example.com");
			fail();
		} catch(DuplicateMobileNumberException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(old1, old2);
	}
	
}
