package be.lions.duckrace.participant;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.MobileNumberAlreadyUsedInvalidDataException;
import be.lions.duckrace.MobileNumberAlreadyUsedPartlyValidDataException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class CreateTwoParticipantsWithDifferentDataTest extends DuckRaceTestCase {

	private static final String EMAIL_ADDRESS = "p.mesotten@aca-it.be";
	private static final String LAST_NAME = "Mesotten";
	private static final String FIRST_NAME = "Peter";
	private static final String MOBILE_NO = "32472514229";
	private Participant p1, p2;
	
//	public void testCreateTwoParticipantsWithDifferentDataCreateMobileOnlyFirst() {
//		try {
//			part = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, null, null, null);
//			part = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
//			Participant p = ParticipantLocalServiceUtil.getParticipant(part.getParticipantId());
//			
//			assertEquals(FIRST_NAME, p.getFirstName());
//			assertEquals(LAST_NAME, p.getLastName());
//			assertEquals(EMAIL_ADDRESS, p.getEmailAddress());
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	
//	public void testCreateTwoParticipantsWithDifferentDataCreateAllDataFirst() {
//		try {
//			part = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
//			part = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, null, null, null);
//			Participant p = ParticipantLocalServiceUtil.getParticipant(part.getParticipantId());
//			
//			assertEquals(FIRST_NAME, p.getFirstName());
//			assertEquals(LAST_NAME, p.getLastName());
//			assertEquals(EMAIL_ADDRESS, p.getEmailAddress());
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}

	public void testCreateTwoParticipantsWithExactDataGivesNoProblem() {
		try {
			p1 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
			p2 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testCreateTwoParticipantsWithOnlySameMobileNumberThrowsException() {
		try {
			p1 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
			p2 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, "a", "b", "c");
			fail();
		} catch(MobileNumberAlreadyUsedInvalidDataException e) {
			//expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testCreateTwoParticipantsWithSameMobileAndPartlySameDataThrowsException() {
		try {
			p1 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
			p2 = ParticipantLocalServiceUtil.getOrCreateParticipant(MOBILE_NO, "a", LAST_NAME, "c");
			fail();
		} catch(MobileNumberAlreadyUsedPartlyValidDataException e) {
			assertEquals(FIRST_NAME, e.getParticipant().getFirstName());
			assertEquals(LAST_NAME, e.getParticipant().getLastName());
			assertEquals(EMAIL_ADDRESS, e.getParticipant().getEmailAddress());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(p1, p2);
	}
	
}
