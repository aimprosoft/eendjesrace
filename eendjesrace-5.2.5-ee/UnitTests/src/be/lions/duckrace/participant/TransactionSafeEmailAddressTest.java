package be.lions.duckrace.participant;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.ParticipantMother;

public class TransactionSafeEmailAddressTest extends DuckRaceTestCase {

	private Participant participant;
	
	@Override
	public void setUp() throws Exception {
		participant = ParticipantMother.createParticipantByEmail("test@example.com");
	}
	
	public void testTransactionSafeEmailTest() {
		assertEquals("test_at_example_dot_com", participant.getTransactionSafeEmailAddress());
	}
	
	@Override
	protected void tearDown() throws Exception {
		ParticipantMother.deleteParticipants(participant);
	}
	
}
