package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class CodeExistsTest extends DuckRaceTestCase {

	private static final String CODE_TO_TEST = "abcdef";
	private Lot lot;
	
	public void setUp() throws Exception {
		lot = LotMother.createLot(CODE_TO_TEST, "123");
	}
	
	public void testCodeDoesNotExistIfNull() {
		testCodeExists(null, false);
	}
	
	public void testCodeDoesNotExistIfEmpty() {
		testCodeExists("", false);
	}
	
	public void testCodeDoesNotExistIfNotInDatabase() {
		testCodeExists("hijklm", false);
	}
	
	public void testCodeDoesExistIfExactTheSameCode() {
		testCodeExists(CODE_TO_TEST, true);
	}
	
	public void testCodeDoesExistIfCaseSensitiveSameCode() {
		testCodeExists("aBcdEF", true);
	}
	
	private void testCodeExists(String code, boolean exists) {
		try {
			assertEquals(exists, LotLocalServiceUtil.codeExists(code));
		} catch(Exception e) {
			fail();
		}
	}
	
	protected void tearDown() throws Exception {
		LotLocalServiceUtil.deleteLot(lot);
	}
	
}
