package be.lions.duckrace.lot;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllLotTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All Lot Tests");

		suite.addTestSuite(CodeExistsTest.class);
		suite.addTestSuite(GetAvailableLotTest.class);
//		suite.addTestSuite(RegisterLotWithMobileNoOnly.class);
		suite.addTestSuite(RegisterLotWithCodeAndChecksumTest.class);
		suite.addTestSuite(InvalidChecksumLotLockupTest.class);
		suite.addTestSuite(LotUnlockTest.class);
		suite.addTestSuite(RegisterWithExtraUserInfoTest.class);
		suite.addTestSuite(RegisterNotPressedLotTest.class);
		suite.addTestSuite(DeadlinesTest.class);
		suite.addTestSuite(LotCountsTest.class);

		return suite;
	}
	
}
