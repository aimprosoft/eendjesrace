package be.lions.duckrace.lot;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.InvalidChecksumException;
import be.lions.duckrace.LotLockoutException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;

public class LotUnlockTest extends DuckRaceTestCase {

	private Lot lot;
	
	public void testTodayIsBeforeUnlockDate() {
		Date today = new Date();
		Date unlockDate = DateUtils.addHours(today, 5);
		Date lockDate = DateUtils.addHours(unlockDate, -24);
		
		try {
			getAndLockAndRegisterLot(lockDate);
			fail();
		} catch(LotLockoutException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testTodayIsAfterUnlockDate() {
		Date today = new Date();
		Date unlockDate = DateUtils.addHours(today, -5);
		Date lockDate = DateUtils.addHours(unlockDate, -24);
		
		try {
			getAndLockAndRegisterLot(lockDate);
		} catch(InvalidChecksumException e) {
			//expected
			try {
				assertEquals(1, getLot().getNbTries());
				assertEquals(null, getLot().getLockoutDate());
			} catch(Exception e1) {
				e1.printStackTrace();
				fail();
			}
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	private void getAndLockAndRegisterLot(Date lockDate) throws NoSuchLotException, SystemException, PortalException {
		lot = LotLocalServiceUtil.getAvailableLot(true);
		lot.setLockoutDate(lockDate);
		lot.setNbTries(3);
		LotLocalServiceUtil.updateLot(lot, false);
		
		LotLocalServiceUtil.registerLot("32472514229", lot.getCode(), "000");
	}
	
	private Lot getLot() throws PortalException, SystemException {
		return LotLocalServiceUtil.getLot(lot.getCode());
	}
	
	@Override
	protected void tearDown() {
		LotMother.resetLots(lot);
	}
	
}
