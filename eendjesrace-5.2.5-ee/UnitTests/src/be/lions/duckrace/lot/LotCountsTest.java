package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.mother.LotReservationMother;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;

public class LotCountsTest extends DuckRaceTestCase {

	private Lot l0, l1, l2, l3, l4, l5;
	private LotReservation lr1, lr2;
	
	@Override
	public void setUp() throws Exception {
//		// Short SMS
//		l0 = LotLocalServiceUtil.registerLot("0472514225");
//		l1 = LotLocalServiceUtil.registerLot("0472514229");
//		// Long SMS
//		l2 = LotLocalServiceUtil.getAvailableLot(true);
//		l2 = LotLocalServiceUtil.registerLot("0472514225", l2.getCode(), l2.getChecksum());
//		l3 = LotLocalServiceUtil.getAvailableLot(true);
//		l3 = LotLocalServiceUtil.registerLot("0472514220", l3.getCode(), l3.getChecksum());
		// Website registration
		l4 = LotLocalServiceUtil.getAvailableLot(true);
		l4 = LotLocalServiceUtil.registerLot("0472514225", "Peter", "Mesotten", "p.mesotten@aca-it.be", l4.getCode(), l4.getChecksum());
		l5 = LotLocalServiceUtil.getAvailableLot(true);
		l5 = LotLocalServiceUtil.registerLot("0472514221", "Jan", "Janssen", "p.mesotten@aca-it.be", l5.getCode(), l5.getChecksum());
		// Website reservation
		lr1 = LotReservationLocalServiceUtil.createReservation("0472514226", "Peter", "Mesotten", "bla.test@be.be", 5);
		lr2 = LotReservationLocalServiceUtil.createReservation("0472514222", "Test", "Je", "bla.jaja@be.be", 10);
		LotReservationLocalServiceUtil.approveReservation(lr1.getReservationId());
		LotReservationLocalServiceUtil.approveReservation(lr2.getReservationId());
	}
	
//	public void testCountShortSmsRegistrations() {
//		try {
//			assertEquals(2, LotLocalServiceUtil.getShortSmsLotCount());
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
	
	public void testCountApprovedReservations() {
		try {
			assertEquals(15, LotLocalServiceUtil.getWebsiteReservationLotCount());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testCountWebsiteRegistrations() {
		try {
			assertEquals(2, LotLocalServiceUtil.getWebsiteRegistrationLotCount());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
//	public void testCountLongSmsRegistrations() {
//		try {
//			assertEquals(1, LotLocalServiceUtil.getLongSmsLotCount());
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
	
	public void testAllCountsSumToCurrentDuckNumber() {
		try {
			assertEquals(LotLocalServiceUtil.getCurrentNumber(),
				LotLocalServiceUtil.getWebsiteReservationLotCount() +
				LotLocalServiceUtil.getWebsiteRegistrationLotCount() +
				LotLocalServiceUtil.getShortSmsLotCount() +
				LotLocalServiceUtil.getLongSmsLotCount());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotReservationMother.deleteReservations(lr1, lr2);
		LotMother.resetLots(l0, l1, l2, l3, l4, l5);
	}
	
}
