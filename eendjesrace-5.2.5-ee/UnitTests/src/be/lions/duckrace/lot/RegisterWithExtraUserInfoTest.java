package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class RegisterWithExtraUserInfoTest extends DuckRaceTestCase {

	private Lot lot = null;
	
	public void testRegisterWithFirstLastNameEmailAddress() {
		String mobileNumber = "32472514225";
		String emailAddress = "p.mesotten@aca-it.be";
		String firstName = "Peter";
		String lastName = "Mesotten";
		
		try {
			Lot existingLot = LotLocalServiceUtil.getAvailableLot(true);
			lot = LotLocalServiceUtil.registerLot(mobileNumber, firstName, lastName, emailAddress, existingLot.getCode(), existingLot.getChecksum());
			
			Participant participant = lot.getParticipant();
			assertNotNull(participant);
			assertEquals(mobileNumber, participant.getMobileNumber());
			assertEquals(emailAddress, participant.getEmailAddress());
			assertEquals(firstName, participant.getFirstName());
			assertEquals(lastName, participant.getLastName());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotMother.resetLots(lot);
	}
	
}
