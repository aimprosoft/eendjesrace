package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class RegisterNotPressedLotTest extends DuckRaceTestCase {

	private Lot lot = null;
	
	public void testRegisterNotPressedLotThrowsException() {
		try {
			lot = LotLocalServiceUtil.getAvailableLot(false);
			LotLocalServiceUtil.registerLot("32472514225", lot.getCode(), lot.getChecksum());
			fail();
		} catch(NoSuchLotException e) {
			//expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() {
		LotMother.resetLots(lot);
	}
	
}
