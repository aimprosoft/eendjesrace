package be.lions.duckrace.lot;

import java.util.Date;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.mother.UserMother;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.types.DuckRaceRole;
import be.lions.duckrace.util.DateUtils;
import be.lions.duckrace.util.DateUtils.Month;

import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;

public class DeadlinesTest extends DuckRaceTestCase {

	private User user, admin;
	
	@Override
	public void setUp() throws Exception {
		user = UserMother.createUser("user@example.com");
		admin = UserMother.createUser("admin@example.com");
		RoleLocalServiceUtil.addUserRoles(admin.getUserId(), 
			new long[] {DuckRaceLocalServiceUtil.getRoleId(DuckRaceRole.LIONS_MEMBER.getName())});
	}
	
	
	public void testRegistrationBeforeUserDeadline() {
		// portal-ext.properties: user registration deadline = 2010-10-13 12:00:00
		Date input = DateUtils.createDate(13, Month.OCTOBER, 2010, 11);
		registrationDeadlineCheck(input, user, true);
		registrationDeadlineCheck(input, admin, true);
	}
	
	public void testRegistrationAfterAdminDeadline() {
		// portal-ext.properties: admin registration deadline = 2010-10-15 9:00:00
		Date input = DateUtils.createDate(15, Month.OCTOBER, 2010, 10);
		registrationDeadlineCheck(input, user, false);
		registrationDeadlineCheck(input, admin, false);
	}
	
	public void testRegistrationAfterUserDeadlineButBeforeAdminDeadline() {
		Date input = DateUtils.createDate(15, Month.OCTOBER, 2010, 8);
		registrationDeadlineCheck(input, user, false);
		registrationDeadlineCheck(input, admin, true);
	}
	
	
	public void testReservationBeforeUserDeadline() {
		// portal-ext.properties: user reservation deadline = 2010-10-14 16:00:00
		Date input = DateUtils.createDate(14, Month.OCTOBER, 2010, 15);
		reservationDeadlineCheck(input, user, true);
		reservationDeadlineCheck(input, admin, true);
	}
	
	public void testReservationAfterAdminDeadline() {
		// portal-ext.properties: admin reservation deadline = 2010-10-16 1:00:00
		Date input = DateUtils.createDate(16, Month.OCTOBER, 2010, 2);
		reservationDeadlineCheck(input, user, false);
		reservationDeadlineCheck(input, admin, false);
	}
	
	public void testReservationAfterUserDeadlineButBeforeAdminDeadline() {
		Date input = DateUtils.createDate(14, Month.OCTOBER, 2010, 17);
		reservationDeadlineCheck(input, user, false);
		reservationDeadlineCheck(input, admin, true);
	}
	
	private void reservationDeadlineCheck(Date input, User user, boolean isBefore) {
		try {
			assertEquals(isBefore, DuckRaceLocalServiceUtil.isReservationPossible(user.getUserId(), input));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	private void registrationDeadlineCheck(Date input, User user, boolean isBefore) {
		try {
			assertEquals(isBefore, DuckRaceLocalServiceUtil.isRegistrationPossible(user.getUserId(), input));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		UserMother.deleteUsers(user, admin);
	}
	
}
