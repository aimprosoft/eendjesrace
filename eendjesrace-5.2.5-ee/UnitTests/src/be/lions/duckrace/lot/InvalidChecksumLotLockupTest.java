package be.lions.duckrace.lot;

import java.util.Date;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.InvalidChecksumException;
import be.lions.duckrace.LotLockoutException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class InvalidChecksumLotLockupTest extends DuckRaceTestCase {

	private Lot lot;
	private static final String SOME_MOBILE_NO = "32472514220";

	@Override
	public void setUp() {
		try {
			lot = LotLocalServiceUtil.getAvailableLot(true);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testEachWrongChecksumEntryIsRegisteredAsTry() {
		try {
			LotLocalServiceUtil.registerLot(SOME_MOBILE_NO, lot.getCode(), "000");
			fail();
		} catch(InvalidChecksumException e) {
			try {
				assertEquals(1, LotLocalServiceUtil.getLot(lot.getCode()).getNbTries());
			} catch(Exception e1) {
				e.printStackTrace();
				fail();
			}
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testThreeTimesWrongBlocksTheLot() {
		Lot lot = null;
		try {
			lot = LotLocalServiceUtil.getAvailableLot(true);
			LotLocalServiceUtil.registerLot(SOME_MOBILE_NO, lot.getCode(), "000");
			fail();
		} catch(InvalidChecksumException e1) {
			try {
				LotLocalServiceUtil.registerLot(SOME_MOBILE_NO, lot.getCode(), "000");
				fail();
			} catch(InvalidChecksumException e2) {
				try {
					LotLocalServiceUtil.registerLot(SOME_MOBILE_NO, lot.getCode(), "000");
					fail();
				} catch(InvalidChecksumException e3) {
					assertNotNull(getLot().getLockoutDate());
					assertEquals(3, getLot().getNbTries());
				} catch(Exception e) {
					e.printStackTrace();
					fail();
				}
			} catch(Exception e) {
				e.printStackTrace();
				fail();
			}
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testTryingToRegisterLockedLotThrowsLotLockoutException() {
		Date lockoutDate = new Date();
		try {
			lot.setNbTries(3);
			lot.setLockoutDate(lockoutDate);
			LotLocalServiceUtil.updateLot(lot, false);
			
			LotLocalServiceUtil.registerLot(SOME_MOBILE_NO, lot.getCode(), "000");
			fail();
		} catch(LotLockoutException e) {
			//expected
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private Lot getLot() {
		try {
			return LotLocalServiceUtil.getLot(lot.getCode());
		} catch(Exception e) {
			return null;
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotMother.resetLots(lot);
	}
	
}
