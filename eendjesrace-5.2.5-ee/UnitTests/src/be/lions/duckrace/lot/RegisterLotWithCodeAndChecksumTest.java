package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.InvalidChecksumException;
import be.lions.duckrace.LotAlreadyRegisteredException;
import be.lions.duckrace.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;

public class RegisterLotWithCodeAndChecksumTest extends DuckRaceTestCase {

	private Lot lot;
	
	public void testRegisterLotWithCorrectCodeAndChecksum() {
		try {
			Lot aLot = LotLocalServiceUtil.getAvailableLot(true);
			String mobileNumber = "32472514229";
			lot = LotLocalServiceUtil.registerLot(mobileNumber, aLot.getCode(), aLot.getChecksum());
			assertNotNull(lot);
			Participant p = ParticipantLocalServiceUtil.getParticipantByMobileNumber(mobileNumber);
			assertEquals(lot.getParticipantId(), p.getParticipantId());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterLotWithIncorrectCode() {
		try {
			lot = LotLocalServiceUtil.registerLot("32472514228", "aqsdqa", "648");
			fail();
		} catch(NoSuchLotException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterLotWithEmptyCodeThrowsReadableException() {
		try {
			lot = LotLocalServiceUtil.registerLot("32472514228", null, "648");
			fail();
		} catch(NoSuchLotException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterLotWithInvalidChecksum() {
		try {
			Lot aLot = LotLocalServiceUtil.getAvailableLot(true);
			lot = LotLocalServiceUtil.registerLot("32472514227", aLot.getCode(), "000");
			fail();
		} catch(InvalidChecksumException e) {
			// expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterAlreadyRegisteredLotThrowsException() {
		try {
			Lot aLot = LotLocalServiceUtil.getAvailableLot(true);
			lot = LotLocalServiceUtil.registerLot("32472514226", aLot.getCode(), aLot.getChecksum());
			LotLocalServiceUtil.registerLot("32897161397", aLot.getCode(), aLot.getChecksum());
			fail();
		} catch(LotAlreadyRegisteredException e) {
			try {
			} catch (Exception e1) {
				e1.printStackTrace();
				fail();
			}
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotMother.resetLots(lot);
	}
	
}
