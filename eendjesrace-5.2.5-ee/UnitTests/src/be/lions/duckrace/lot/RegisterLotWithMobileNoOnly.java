package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.InvalidMobileNumberException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class RegisterLotWithMobileNoOnly extends DuckRaceTestCase {

	private Lot lot1, lot2;
	
	public void testRegisterLotWithValidMobileNumber() {
		try {
			lot1 = LotLocalServiceUtil.registerLot("32472514225");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterTwoLotsWithSameMobileNumberDoesntThrowException() {
		try {
			String mobileNo = "32472514225";
			lot1 = LotLocalServiceUtil.registerLot(mobileNo);
			lot2 = LotLocalServiceUtil.registerLot(mobileNo);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testRegisterLotWithInvalidMobileNumber() {
		try {
			lot1 = LotLocalServiceUtil.registerLot("klsqjd");
			fail();
		} catch(InvalidMobileNumberException e) {
			//expected
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotMother.resetLots(lot1, lot2);
	}
	
}
