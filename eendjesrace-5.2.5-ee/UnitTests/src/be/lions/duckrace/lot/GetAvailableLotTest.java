package be.lions.duckrace.lot;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.mother.LotMother;
import be.lions.duckrace.service.LotLocalServiceUtil;

public class GetAvailableLotTest extends DuckRaceTestCase {
	
	private Lot l1;
	
	@Override
	public void setUp() throws Exception {
		l1 = getLot();
	}
	
	public void testCannotGetUnavailableLot() {
		try {
			l1.setParticipantId(123);
			LotLocalServiceUtil.updateLot(l1, false);
			Lot l2 = getLot();
			assertNotSame(l2, l1);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testCannotGetLotThatAlreadyHasNumber() {
		try {
			l1.setNumber(342);
			LotLocalServiceUtil.updateLot(l1, false);
			Lot l2 = getLot();
			assertNotSame(l2, l1);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private Lot getLot() {
		Lot lot = null;
		try {
			lot = LotLocalServiceUtil.getAvailableLot(true);
			assertNotNull(lot);
			System.out.println("Retrieved lot with code "+lot.getCode());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return lot;
	}
	
	@Override
	protected void tearDown() throws Exception {
		LotMother.resetLots(l1);
	}
	
}
