package be.lions.duckrace;

import java.util.List;
import java.util.Map;

import base.BaseServiceTestCase;

public class DuckRaceTestCase extends BaseServiceTestCase {
	
	protected void assertEmpty(List<?> list) {
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}
	
	protected void assertNotEmpty(List<?> list) {
		assertNotNull(list);
		assertFalse(list.isEmpty());
	}
	
	protected void assertSize(List<?> list, int size) {
		assertEquals(size, list.size());
	}
	
	protected <T> void assertContains(List<T> list, T... t) {
		assertNotNull(list);
		for(T element: t) {
			assertTrue(list.contains(element));
		}
	}
	
	protected <T> void assertNotContains(List<T> list, T... t) {
		assertNotNull(list);
		for(T element: t) {
			assertFalse(list.contains(element));
		}
	}
	
	protected <K,V> void mapContains(Map<K,V> map, K key, V value) {
		assertTrue(map.containsKey(key));
		assertEquals(map.get(key), value);
	}
	
	protected void assertBetween(double test, double lower, double upper) {
		if(test < lower || test > upper) {
			fail("<"+test+">"+"does not lie between <"+lower+"> and <"+upper+">");
		}
	}
	
}
