package be.lions.duckrace.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DuckOrderUtilParameterTest {

	private static final String ERROR_INVALID_PARAMETERS_SIZE = "Invalid number of parameters provided";
	private static final String ERROR_REQUIRED_PARAMETERS_MISSING = "Some required parameters are missing";
	private static final String PARAM_NAME = "name";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_CELL_PHONE = "cellPhone";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_DUCK_COUNT = "duckCount";
	private static final String PARAM_ORDER_NUMBER = "orderNumber";
	private static final String PARAM_DUMMY = "dummy";
	private static final HashMap<String, String> INVALID_PARAMETER_MAP = new HashMap<String, String>();
	private final HashMap<String, String> VALID_PARAMETER_MAP = new HashMap<String, String>();

	@Mock private HttpServletResponse response;

	@Before
	public void setup() {
		VALID_PARAMETER_MAP.put(PARAM_NAME, null);
		VALID_PARAMETER_MAP.put(PARAM_FIRST_NAME, null);
		VALID_PARAMETER_MAP.put(PARAM_CELL_PHONE, null);
		VALID_PARAMETER_MAP.put(PARAM_EMAIL, null);
		VALID_PARAMETER_MAP.put(PARAM_DUCK_COUNT, null);
		VALID_PARAMETER_MAP.put(PARAM_ORDER_NUMBER, null);
	}

	@Test
	public void invalidNumberOfParametersReturnsFalseAndAddsErrorCode() throws IOException {
		boolean isValid = DuckOrderUtil.isValidParameterMap(INVALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_INVALID_PARAMETERS_SIZE);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void nameParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_NAME);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void firstNameParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_FIRST_NAME);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void cellPhoneParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_CELL_PHONE);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void emailParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_EMAIL);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void duckCountParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_DUCK_COUNT);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void orderNumberParameterMissingReturnsFalseAndAddsErrorCode() throws IOException {
		VALID_PARAMETER_MAP.remove(PARAM_ORDER_NUMBER);
		VALID_PARAMETER_MAP.put(PARAM_DUMMY, null);

		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertFalse(isValid);
		verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_REQUIRED_PARAMETERS_MISSING);
		verifyNoMoreInteractions(response);
	}

	@Test
	public void validParameterListReturnsTrueAndAddsNoErrorCode() throws IOException {
		boolean isValid = DuckOrderUtil.isValidParameterMap(VALID_PARAMETER_MAP, response);

		assertTrue(isValid);
		verifyZeroInteractions(response);
	}
}
