package be.lions.duckrace.utils.stringutils;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.util.StringUtils;

public class LowerCaseFirstCapitalTest extends DuckRaceTestCase {

	public void testNullReturnsNull() {
		check(null, null);
	}
	
	public void testEmptyStringReturnsEmptyString() {
		check("", "");
	}
	
	public void testSingleCharacterReturnsUppercaseChar() {
		check("r", "R");
	}
	
	public void testSomeLargerWord() {
		check("HELLO", "Hello");
	}
	
	public void testSomeRandomUpperAndLowerCaseChars() {
		check("lIfeRaY", "Liferay");
	}
	
	private void check(String input, String expectedOutput) {
		assertEquals(expectedOutput, StringUtils.lowerCaseFirstCapital(input));
	}
	
}
