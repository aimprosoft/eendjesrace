package be.lions.duckrace.utils.stringutils;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.util.StringUtils;

public class GetFormattedMobileNumberTest extends DuckRaceTestCase {

	public void testCheckNull() {
		check(null, null);
	}
	
	public void testStrippedNumber() {
		check("32472514225", "(+32)472/51.42.25");
	}
	
	public void testAlreadyFormattedNumber() {
		check("(+32)472/51.42.25", "(+32)472/51.42.25");
	}
	
	public void testUsualFormat() {
		check("0472/514225", "(+32)472/51.42.25");
	}
	
	public void testGermanNumber() {
		check("+494725142255", "(+49)472/51.42.255");
	}
	
	public void testInvalidNumber() {
		check("abc123423sdq", null);
	}
	
	private void check(String input, String output) {
		assertEquals(output, StringUtils.getFormattedMobileNumber(input));
	}
	
}
