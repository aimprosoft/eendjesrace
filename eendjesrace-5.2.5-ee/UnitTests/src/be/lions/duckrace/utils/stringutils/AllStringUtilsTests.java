package be.lions.duckrace.utils.stringutils;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllStringUtilsTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All StringUtils Tests");

		suite.addTestSuite(IsValidMobileNumberTest.class);
		suite.addTestSuite(LowerCaseFirstCapitalTest.class);
		suite.addTestSuite(GetFormattedMobileNumberTest.class);

		return suite;
	}
	
}