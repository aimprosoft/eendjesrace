package be.lions.duckrace.utils.stringutils;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.util.StringUtils;

public class IsValidMobileNumberTest extends DuckRaceTestCase {
	
	public void testNullIsInvalidMobileNumber() {
		checkIfValid(null, false);
	}
	
	public void testEmptyStringIsInvalidMobileNumber() {
		checkIfValid("", false);
	}
	
	public void testStringTooSmallStringSizeIsInvalidMobileNumber() {
		checkIfValid("1234567890", false);
	}
	
	public void testCorrectMobileNumber() {
		checkIfValid("32479123456", true);
	}
	
	public void testNoNumberIsInvalidMobileNumber() {
		checkIfValid("abcdefghij", false);
	}
	
	public void testUsualMobileNumber() {
		checkIfValid("0472/51.42.20", true);
	}

	public void testUsualMobileNumberWithWrongFormatStillSucceeds() {
		checkIfValid("0472/51.42.204", true);
	}
	
	private void checkIfValid(String number, boolean valid) {
		assertEquals(valid, StringUtils.isValidMobileNumber(number));
	}
	
}
