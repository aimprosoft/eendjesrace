package be.lions.duckrace.utils.listutils;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllListUtilsTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All ListUtils Tests");

		suite.addTestSuite(ReverseTest.class);

		return suite;
	}
	
}