package be.lions.duckrace.utils.listutils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.lions.duckrace.DuckRaceTestCase;
import be.lions.duckrace.util.ListUtils;

public class ReverseTest extends DuckRaceTestCase {
	
	public void testEmpty() {
		check(Collections.emptyList(), Collections.emptyList());
	}
	
	public void testFilled() {
		check(createList("one", "two", "three"), createList("three", "two", "one"));
	}
	
	private static void check(List<?> input, List<?> expected) {
		List<?> reverseInput = ListUtils.reverse(input);
		for (int i=0; i<reverseInput.size(); i++) {
			assertEquals(expected.get(i), reverseInput.get(i));
		}
	}
	
	private List<String> createList(String... elements) {
		List<String> result = new ArrayList<String>();
		for (String el : elements) {
			result.add(el);
		}
		return result;
	}

}
