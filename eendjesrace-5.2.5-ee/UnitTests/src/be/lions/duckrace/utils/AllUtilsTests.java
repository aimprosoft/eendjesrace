package be.lions.duckrace.utils;

import junit.framework.Test;
import junit.framework.TestSuite;
import be.lions.duckrace.utils.listutils.AllListUtilsTests;
import be.lions.duckrace.utils.stringutils.AllStringUtilsTests;

public class AllUtilsTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All Utils Tests");

		suite.addTest(AllStringUtilsTests.suite());
		suite.addTest(AllListUtilsTests.suite());

		return suite;
	}
	
}