package be.lions.duckrace;

import be.lions.duckrace.lot.AllLotTests;
import be.lions.duckrace.participant.AllParticipantTests;
import be.lions.duckrace.reservation.AllReservationTests;
import be.lions.duckrace.utils.AllUtilsTests;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("All Tests");

		suite.addTest(AllLotTests.suite());
		suite.addTest(AllParticipantTests.suite());
		suite.addTest(AllReservationTests.suite());
		suite.addTest(AllUtilsTests.suite());

		return suite;
	}
	
}
