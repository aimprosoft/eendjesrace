package be.lions.duckrace.servlet;

import be.lions.duckrace.util.PropKeys;
import com.liferay.portal.util.PropsUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PropsUtil.class)
public class DuckOrderServletTest {

	private static final String PARAM_NAME = "name";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_CELL_PHONE = "cellPhone";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_DUCK_COUNT = "duckCount";
	private static final String PARAM_ORDER_NUMBER = "orderNumber";
	private static final HashMap<String, String> INVALID_PARAMETER_MAP = new HashMap<String, String>();
	private final HashMap<String, String> VALID_PARAMETER_MAP = new HashMap<String, String>();
	private static final String[] TRUSTED_HOSTS = new String[] { "127.0.0.10" };
	private static final String UNTRUSTED_REMOTE_HOST = "127.0.0.1";
	private static final String TRUSTED_REMOTE_HOST = "127.0.0.10";
	private static final String DUCK_IDS = "123,456,789";

	@Mock private HttpServletRequest request;
	@Mock private HttpServletResponse response;
	@Mock private PrintWriter writer;

	@Before
	public void setup() throws IOException {
		VALID_PARAMETER_MAP.put(PARAM_NAME, null);
		VALID_PARAMETER_MAP.put(PARAM_FIRST_NAME, null);
		VALID_PARAMETER_MAP.put(PARAM_CELL_PHONE, null);
		VALID_PARAMETER_MAP.put(PARAM_EMAIL, null);
		VALID_PARAMETER_MAP.put(PARAM_DUCK_COUNT, null);
		VALID_PARAMETER_MAP.put(PARAM_ORDER_NUMBER, null);
		when(request.getParameterMap()).thenReturn(VALID_PARAMETER_MAP);

		mockStatic(PropsUtil.class);
		when(PropsUtil.getArray(PropKeys.BEHAPPY2_TRUSTED_HOSTS)).thenReturn(TRUSTED_HOSTS);

		when(request.getRemoteHost()).thenReturn(TRUSTED_REMOTE_HOST);
		when(request.isSecure()).thenReturn(true);

		when(response.getWriter()).thenReturn(writer);
	}

	@Test
	public void invalidParameterMapSendsBadRequestResponse() throws IOException {
		when(request.getParameterMap()).thenReturn(INVALID_PARAMETER_MAP);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
	}

	@Test
	public void untrustedHostRequestSendsUnauthorizedResponse() throws IOException {
		when(request.getRemoteHost()).thenReturn(UNTRUSTED_REMOTE_HOST);
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).sendError(eq(HttpServletResponse.SC_UNAUTHORIZED), anyString());
	}

	@Test
	public void validRequestSendsValidResponse() throws IOException {
		DuckOrderServlet servlet = new DuckOrderServlet();

		servlet.doGet(request, response);

		verify(response).setStatus(HttpServletResponse.SC_OK);
		verify(response).getWriter();
		verify(writer).println(DUCK_IDS);
	}
}
