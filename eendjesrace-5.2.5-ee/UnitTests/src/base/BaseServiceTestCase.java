package base;
/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 *
 *
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import java.net.URL;

import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalInstances;

/**
 * <a href="BaseServiceTestCase.java.html"><b><i>View Source</i></b></a>
 *
 * @author Michael Young
 *
 */
public class BaseServiceTestCase extends BaseTestCase {

	public void setUp() throws Exception {
		super.setUp();

		PortalInstances.addCompanyId(TestPropsValues.COMPANY_ID);

		PrincipalThreadLocal.setName(TestPropsValues.USER_ID);

		User user = UserLocalServiceUtil.getUserById(TestPropsValues.USER_ID);

		_permissionChecker = PermissionCheckerFactoryUtil.create(user, true);

		PermissionThreadLocal.setPermissionChecker(_permissionChecker);
	}

	protected URL getClassResource(String name) {
		return getClass().getClassLoader().getResource(name);
	}

	private PermissionChecker _permissionChecker = null;

}