-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.3


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema %DBNAME%
--

DROP DATABASE %DBNAME%;
CREATE DATABASE %DBNAME%;
USE %DBNAME%;

--
-- Definition of table `%DBNAME%`.`account_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`account_`;
CREATE TABLE  `%DBNAME%`.`account_` (
  `accountId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentAccountId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `legalName` varchar(75) DEFAULT NULL,
  `legalId` varchar(75) DEFAULT NULL,
  `legalType` varchar(75) DEFAULT NULL,
  `sicCode` varchar(75) DEFAULT NULL,
  `tickerSymbol` varchar(75) DEFAULT NULL,
  `industry` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `size_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`account_`
--

/*!40000 ALTER TABLE `account_` DISABLE KEYS */;
LOCK TABLES `account_` WRITE;
INSERT INTO `%DBNAME%`.`account_` VALUES  (10116,10114,0,'','2010-08-03 09:47:09','2010-08-03 09:47:09',0,'liferay.com','','','','','','','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `account_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`address`
--

DROP TABLE IF EXISTS `%DBNAME%`.`address`;
CREATE TABLE  `%DBNAME%`.`address` (
  `addressId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `street1` varchar(75) DEFAULT NULL,
  `street2` varchar(75) DEFAULT NULL,
  `street3` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `zip` varchar(75) DEFAULT NULL,
  `regionId` bigint(20) DEFAULT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `mailing` tinyint(4) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`addressId`),
  KEY `IX_93D5AD4E` (`companyId`),
  KEY `IX_ABD7DAC0` (`companyId`,`classNameId`),
  KEY `IX_71CB1123` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_923BD178` (`companyId`,`classNameId`,`classPK`,`mailing`),
  KEY `IX_9226DBB4` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_5BC8B0D4` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`address`
--

/*!40000 ALTER TABLE `address` DISABLE KEYS */;
LOCK TABLES `address` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`announcementsdelivery`
--

DROP TABLE IF EXISTS `%DBNAME%`.`announcementsdelivery`;
CREATE TABLE  `%DBNAME%`.`announcementsdelivery` (
  `deliveryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `email` tinyint(4) DEFAULT NULL,
  `sms` tinyint(4) DEFAULT NULL,
  `website` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`deliveryId`),
  UNIQUE KEY `IX_BA4413D5` (`userId`,`type_`),
  KEY `IX_6EDB9600` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`announcementsdelivery`
--

/*!40000 ALTER TABLE `announcementsdelivery` DISABLE KEYS */;
LOCK TABLES `announcementsdelivery` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `announcementsdelivery` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`announcementsentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`announcementsentry`;
CREATE TABLE  `%DBNAME%`.`announcementsentry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `title` varchar(75) DEFAULT NULL,
  `content` longtext,
  `url` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `displayDate` datetime DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `alert` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  KEY `IX_A6EF0B81` (`classNameId`,`classPK`),
  KEY `IX_14F06A6B` (`classNameId`,`classPK`,`alert`),
  KEY `IX_D49C2E66` (`userId`),
  KEY `IX_1AFBDE08` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`announcementsentry`
--

/*!40000 ALTER TABLE `announcementsentry` DISABLE KEYS */;
LOCK TABLES `announcementsentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `announcementsentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`announcementsflag`
--

DROP TABLE IF EXISTS `%DBNAME%`.`announcementsflag`;
CREATE TABLE  `%DBNAME%`.`announcementsflag` (
  `flagId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `entryId` bigint(20) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`flagId`),
  UNIQUE KEY `IX_4539A99C` (`userId`,`entryId`,`value`),
  KEY `IX_9C7EB9F` (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`announcementsflag`
--

/*!40000 ALTER TABLE `announcementsflag` DISABLE KEYS */;
LOCK TABLES `announcementsflag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `announcementsflag` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`blogsentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`blogsentry`;
CREATE TABLE  `%DBNAME%`.`blogsentry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `urlTitle` varchar(150) DEFAULT NULL,
  `content` longtext,
  `displayDate` datetime DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  `allowTrackbacks` tinyint(4) DEFAULT NULL,
  `trackbacks` longtext,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_DB780A20` (`groupId`,`urlTitle`),
  UNIQUE KEY `IX_1B1040FD` (`uuid_`,`groupId`),
  KEY `IX_72EF6041` (`companyId`),
  KEY `IX_E0D90212` (`companyId`,`displayDate`,`draft`),
  KEY `IX_81A50303` (`groupId`),
  KEY `IX_DA53AFD4` (`groupId`,`displayDate`,`draft`),
  KEY `IX_C07CA83D` (`groupId`,`userId`),
  KEY `IX_B88E740E` (`groupId`,`userId`,`displayDate`,`draft`),
  KEY `IX_69157A4D` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`blogsentry`
--

/*!40000 ALTER TABLE `blogsentry` DISABLE KEYS */;
LOCK TABLES `blogsentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `blogsentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`blogsstatsuser`
--

DROP TABLE IF EXISTS `%DBNAME%`.`blogsstatsuser`;
CREATE TABLE  `%DBNAME%`.`blogsstatsuser` (
  `statsUserId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `entryCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  `ratingsTotalEntries` int(11) DEFAULT NULL,
  `ratingsTotalScore` double DEFAULT NULL,
  `ratingsAverageScore` double DEFAULT NULL,
  PRIMARY KEY (`statsUserId`),
  UNIQUE KEY `IX_82254C25` (`groupId`,`userId`),
  KEY `IX_90CDA39A` (`companyId`,`entryCount`),
  KEY `IX_43840EEB` (`groupId`),
  KEY `IX_28C78D5C` (`groupId`,`entryCount`),
  KEY `IX_BB51F1D9` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`blogsstatsuser`
--

/*!40000 ALTER TABLE `blogsstatsuser` DISABLE KEYS */;
LOCK TABLES `blogsstatsuser` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `blogsstatsuser` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`bookmarksentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`bookmarksentry`;
CREATE TABLE  `%DBNAME%`.`bookmarksentry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext,
  `comments` longtext,
  `visits` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_EAA02A91` (`uuid_`,`groupId`),
  KEY `IX_443BDC38` (`folderId`),
  KEY `IX_E52FF7EF` (`groupId`),
  KEY `IX_E2E9F129` (`groupId`,`userId`),
  KEY `IX_B670BA39` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`bookmarksentry`
--

/*!40000 ALTER TABLE `bookmarksentry` DISABLE KEYS */;
LOCK TABLES `bookmarksentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bookmarksentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`bookmarksfolder`
--

DROP TABLE IF EXISTS `%DBNAME%`.`bookmarksfolder`;
CREATE TABLE  `%DBNAME%`.`bookmarksfolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_DC2F8927` (`uuid_`,`groupId`),
  KEY `IX_2ABA25D7` (`companyId`),
  KEY `IX_7F703619` (`groupId`),
  KEY `IX_967799C0` (`groupId`,`parentFolderId`),
  KEY `IX_451E7AE3` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`bookmarksfolder`
--

/*!40000 ALTER TABLE `bookmarksfolder` DISABLE KEYS */;
LOCK TABLES `bookmarksfolder` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bookmarksfolder` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`browsertracker`
--

DROP TABLE IF EXISTS `%DBNAME%`.`browsertracker`;
CREATE TABLE  `%DBNAME%`.`browsertracker` (
  `browserTrackerId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `browserKey` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`browserTrackerId`),
  UNIQUE KEY `IX_E7B95510` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`browsertracker`
--

/*!40000 ALTER TABLE `browsertracker` DISABLE KEYS */;
LOCK TABLES `browsertracker` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `browsertracker` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`calevent`
--

DROP TABLE IF EXISTS `%DBNAME%`.`calevent`;
CREATE TABLE  `%DBNAME%`.`calevent` (
  `uuid_` varchar(75) DEFAULT NULL,
  `eventId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` varchar(75) DEFAULT NULL,
  `description` longtext,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `durationHour` int(11) DEFAULT NULL,
  `durationMinute` int(11) DEFAULT NULL,
  `allDay` tinyint(4) DEFAULT NULL,
  `timeZoneSensitive` tinyint(4) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `repeating` tinyint(4) DEFAULT NULL,
  `recurrence` longtext,
  `remindBy` int(11) DEFAULT NULL,
  `firstReminder` int(11) DEFAULT NULL,
  `secondReminder` int(11) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  UNIQUE KEY `IX_5CCE79C8` (`uuid_`,`groupId`),
  KEY `IX_D6FD9496` (`companyId`),
  KEY `IX_12EE4898` (`groupId`),
  KEY `IX_4FDDD2BF` (`groupId`,`repeating`),
  KEY `IX_FCD7C63D` (`groupId`,`type_`),
  KEY `IX_F6006202` (`remindBy`),
  KEY `IX_C1AD2122` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`calevent`
--

/*!40000 ALTER TABLE `calevent` DISABLE KEYS */;
LOCK TABLES `calevent` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `calevent` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`classname_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`classname_`;
CREATE TABLE  `%DBNAME%`.`classname_` (
  `classNameId` bigint(20) NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`classNameId`),
  UNIQUE KEY `IX_B27A301F` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`classname_`
--

/*!40000 ALTER TABLE `classname_` DISABLE KEYS */;
LOCK TABLES `classname_` WRITE;
INSERT INTO `%DBNAME%`.`classname_` VALUES  (10201,'be.lions.duckrace.model.Lot'),
 (10202,'be.lions.duckrace.model.LotReservation'),
 (10203,'be.lions.duckrace.model.Participant'),
 (10001,'com.liferay.portal.model.Account'),
 (10002,'com.liferay.portal.model.Address'),
 (10003,'com.liferay.portal.model.BrowserTracker'),
 (10004,'com.liferay.portal.model.ClassName'),
 (10005,'com.liferay.portal.model.Company'),
 (10006,'com.liferay.portal.model.Contact'),
 (10007,'com.liferay.portal.model.Country'),
 (10008,'com.liferay.portal.model.EmailAddress'),
 (10009,'com.liferay.portal.model.Group'),
 (10010,'com.liferay.portal.model.Image'),
 (10011,'com.liferay.portal.model.Layout'),
 (10012,'com.liferay.portal.model.LayoutSet'),
 (10013,'com.liferay.portal.model.ListType'),
 (10014,'com.liferay.portal.model.Lock'),
 (10015,'com.liferay.portal.model.MembershipRequest'),
 (10019,'com.liferay.portal.model.Organization'),
 (10016,'com.liferay.portal.model.OrgGroupPermission'),
 (10017,'com.liferay.portal.model.OrgGroupRole'),
 (10018,'com.liferay.portal.model.OrgLabor'),
 (10020,'com.liferay.portal.model.PasswordPolicy'),
 (10021,'com.liferay.portal.model.PasswordPolicyRel'),
 (10022,'com.liferay.portal.model.PasswordTracker'),
 (10023,'com.liferay.portal.model.Permission'),
 (10024,'com.liferay.portal.model.Phone'),
 (10025,'com.liferay.portal.model.PluginSetting'),
 (10026,'com.liferay.portal.model.Portlet'),
 (10027,'com.liferay.portal.model.PortletItem'),
 (10028,'com.liferay.portal.model.PortletPreferences'),
 (10029,'com.liferay.portal.model.Region'),
 (10030,'com.liferay.portal.model.Release'),
 (10031,'com.liferay.portal.model.Resource'),
 (10032,'com.liferay.portal.model.ResourceAction'),
 (10033,'com.liferay.portal.model.ResourceCode'),
 (10034,'com.liferay.portal.model.ResourcePermission'),
 (10035,'com.liferay.portal.model.Role'),
 (10036,'com.liferay.portal.model.ServiceComponent'),
 (10037,'com.liferay.portal.model.Shard'),
 (10038,'com.liferay.portal.model.Subscription'),
 (10039,'com.liferay.portal.model.User'),
 (10040,'com.liferay.portal.model.UserGroup'),
 (10041,'com.liferay.portal.model.UserGroupGroupRole'),
 (10042,'com.liferay.portal.model.UserGroupRole'),
 (10043,'com.liferay.portal.model.UserIdMapper'),
 (10044,'com.liferay.portal.model.UserTracker'),
 (10045,'com.liferay.portal.model.UserTrackerPath'),
 (10046,'com.liferay.portal.model.WebDAVProps'),
 (10047,'com.liferay.portal.model.Website'),
 (10048,'com.liferay.portlet.announcements.model.AnnouncementsDelivery'),
 (10049,'com.liferay.portlet.announcements.model.AnnouncementsEntry'),
 (10050,'com.liferay.portlet.announcements.model.AnnouncementsFlag'),
 (10051,'com.liferay.portlet.blogs.model.BlogsEntry'),
 (10052,'com.liferay.portlet.blogs.model.BlogsStatsUser'),
 (10053,'com.liferay.portlet.bookmarks.model.BookmarksEntry'),
 (10054,'com.liferay.portlet.bookmarks.model.BookmarksFolder'),
 (10055,'com.liferay.portlet.calendar.model.CalEvent'),
 (10056,'com.liferay.portlet.documentlibrary.model.DLFileEntry'),
 (10057,'com.liferay.portlet.documentlibrary.model.DLFileRank'),
 (10058,'com.liferay.portlet.documentlibrary.model.DLFileShortcut'),
 (10059,'com.liferay.portlet.documentlibrary.model.DLFileVersion'),
 (10060,'com.liferay.portlet.documentlibrary.model.DLFolder'),
 (10061,'com.liferay.portlet.expando.model.ExpandoColumn'),
 (10062,'com.liferay.portlet.expando.model.ExpandoRow'),
 (10063,'com.liferay.portlet.expando.model.ExpandoTable'),
 (10064,'com.liferay.portlet.expando.model.ExpandoValue'),
 (10065,'com.liferay.portlet.imagegallery.model.IGFolder'),
 (10066,'com.liferay.portlet.imagegallery.model.IGImage'),
 (10067,'com.liferay.portlet.journal.model.JournalArticle'),
 (10068,'com.liferay.portlet.journal.model.JournalArticleImage'),
 (10069,'com.liferay.portlet.journal.model.JournalArticleResource'),
 (10070,'com.liferay.portlet.journal.model.JournalContentSearch'),
 (10071,'com.liferay.portlet.journal.model.JournalFeed'),
 (10072,'com.liferay.portlet.journal.model.JournalStructure'),
 (10073,'com.liferay.portlet.journal.model.JournalTemplate'),
 (10074,'com.liferay.portlet.messageboards.model.MBBan'),
 (10075,'com.liferay.portlet.messageboards.model.MBCategory'),
 (10076,'com.liferay.portlet.messageboards.model.MBDiscussion'),
 (10077,'com.liferay.portlet.messageboards.model.MBMailingList'),
 (10078,'com.liferay.portlet.messageboards.model.MBMessage'),
 (10079,'com.liferay.portlet.messageboards.model.MBMessageFlag'),
 (10080,'com.liferay.portlet.messageboards.model.MBStatsUser'),
 (10081,'com.liferay.portlet.messageboards.model.MBThread'),
 (10082,'com.liferay.portlet.polls.model.PollsChoice'),
 (10083,'com.liferay.portlet.polls.model.PollsQuestion'),
 (10084,'com.liferay.portlet.polls.model.PollsVote'),
 (10085,'com.liferay.portlet.ratings.model.RatingsEntry'),
 (10086,'com.liferay.portlet.ratings.model.RatingsStats'),
 (10087,'com.liferay.portlet.shopping.model.ShoppingCart'),
 (10088,'com.liferay.portlet.shopping.model.ShoppingCategory'),
 (10089,'com.liferay.portlet.shopping.model.ShoppingCoupon'),
 (10090,'com.liferay.portlet.shopping.model.ShoppingItem'),
 (10091,'com.liferay.portlet.shopping.model.ShoppingItemField'),
 (10092,'com.liferay.portlet.shopping.model.ShoppingItemPrice'),
 (10093,'com.liferay.portlet.shopping.model.ShoppingOrder'),
 (10094,'com.liferay.portlet.shopping.model.ShoppingOrderItem'),
 (10095,'com.liferay.portlet.social.model.SocialActivity'),
 (10096,'com.liferay.portlet.social.model.SocialRelation'),
 (10097,'com.liferay.portlet.social.model.SocialRequest'),
 (10098,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion'),
 (10099,'com.liferay.portlet.softwarecatalog.model.SCLicense'),
 (10100,'com.liferay.portlet.softwarecatalog.model.SCProductEntry'),
 (10101,'com.liferay.portlet.softwarecatalog.model.SCProductScreenshot'),
 (10102,'com.liferay.portlet.softwarecatalog.model.SCProductVersion'),
 (10103,'com.liferay.portlet.tags.model.TagsAsset'),
 (10104,'com.liferay.portlet.tags.model.TagsEntry'),
 (10105,'com.liferay.portlet.tags.model.TagsProperty'),
 (10106,'com.liferay.portlet.tags.model.TagsSource'),
 (10107,'com.liferay.portlet.tags.model.TagsVocabulary'),
 (10108,'com.liferay.portlet.tasks.model.TasksProposal'),
 (10109,'com.liferay.portlet.tasks.model.TasksReview'),
 (10110,'com.liferay.portlet.wiki.model.WikiNode'),
 (10111,'com.liferay.portlet.wiki.model.WikiPage'),
 (10112,'com.liferay.portlet.wiki.model.WikiPageResource');
UNLOCK TABLES;
/*!40000 ALTER TABLE `classname_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`company`
--

DROP TABLE IF EXISTS `%DBNAME%`.`company`;
CREATE TABLE  `%DBNAME%`.`company` (
  `companyId` bigint(20) NOT NULL,
  `accountId` bigint(20) DEFAULT NULL,
  `webId` varchar(75) DEFAULT NULL,
  `key_` longtext,
  `virtualHost` varchar(75) DEFAULT NULL,
  `mx` varchar(75) DEFAULT NULL,
  `homeURL` longtext,
  `logoId` bigint(20) DEFAULT NULL,
  `system` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`companyId`),
  UNIQUE KEY `IX_975996C0` (`virtualHost`),
  UNIQUE KEY `IX_EC00543C` (`webId`),
  KEY `IX_38EFE3FD` (`logoId`),
  KEY `IX_12566EC2` (`mx`),
  KEY `IX_35E3E7C6` (`system`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`company`
--

/*!40000 ALTER TABLE `company` DISABLE KEYS */;
LOCK TABLES `company` WRITE;
INSERT INTO `%DBNAME%`.`company` VALUES  (10114,10116,'liferay.com','rO0ABXNyABRqYXZhLnNlY3VyaXR5LktleVJlcL35T7OImqVDAgAETAAJYWxnb3JpdGhtdAASTGphdmEvbGFuZy9TdHJpbmc7WwAHZW5jb2RlZHQAAltCTAAGZm9ybWF0cQB+AAFMAAR0eXBldAAbTGphdmEvc2VjdXJpdHkvS2V5UmVwJFR5cGU7eHB0AANERVN1cgACW0Ks8xf4BghU4AIAAHhwAAAACGsWV4b0+9k4dAADUkFXfnIAGWphdmEuc2VjdXJpdHkuS2V5UmVwJFR5cGUAAAAAAAAAABIAAHhyAA5qYXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AAZTRUNSRVQ=','localhost','liferay.com','',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`contact_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`contact_`;
CREATE TABLE  `%DBNAME%`.`contact_` (
  `contactId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `accountId` bigint(20) DEFAULT NULL,
  `parentContactId` bigint(20) DEFAULT NULL,
  `firstName` varchar(75) DEFAULT NULL,
  `middleName` varchar(75) DEFAULT NULL,
  `lastName` varchar(75) DEFAULT NULL,
  `prefixId` int(11) DEFAULT NULL,
  `suffixId` int(11) DEFAULT NULL,
  `male` tinyint(4) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `smsSn` varchar(75) DEFAULT NULL,
  `aimSn` varchar(75) DEFAULT NULL,
  `facebookSn` varchar(75) DEFAULT NULL,
  `icqSn` varchar(75) DEFAULT NULL,
  `jabberSn` varchar(75) DEFAULT NULL,
  `msnSn` varchar(75) DEFAULT NULL,
  `mySpaceSn` varchar(75) DEFAULT NULL,
  `skypeSn` varchar(75) DEFAULT NULL,
  `twitterSn` varchar(75) DEFAULT NULL,
  `ymSn` varchar(75) DEFAULT NULL,
  `employeeStatusId` varchar(75) DEFAULT NULL,
  `employeeNumber` varchar(75) DEFAULT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `jobClass` varchar(75) DEFAULT NULL,
  `hoursOfOperation` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`contactId`),
  KEY `IX_66D496A3` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`contact_`
--

/*!40000 ALTER TABLE `contact_` DISABLE KEYS */;
LOCK TABLES `contact_` WRITE;
INSERT INTO `%DBNAME%`.`contact_` VALUES  (10118,10114,10117,'','2010-08-03 09:47:09','2010-08-03 09:47:09',10116,0,'','','',0,0,1,'2010-08-03 09:47:09','','','','','','','','','','','','','','',''),
 (10147,10114,10146,'','2010-08-03 09:47:11','2010-08-03 09:47:11',10116,0,'Test','','Test',0,0,1,'1970-01-01 00:00:00','','','','','','','','','','','','','','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `contact_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`counter`
--

DROP TABLE IF EXISTS `%DBNAME%`.`counter`;
CREATE TABLE  `%DBNAME%`.`counter` (
  `name` varchar(75) NOT NULL,
  `currentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`counter`
--

/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
LOCK TABLES `counter` WRITE;
INSERT INTO `%DBNAME%`.`counter` VALUES  ('com.liferay.counter.model.Counter',10800),
 ('com.liferay.portal.model.Permission',900),
 ('com.liferay.portal.model.Resource',400),
 ('com.liferay.portal.model.ResourceCode',700);
UNLOCK TABLES;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`country`
--

DROP TABLE IF EXISTS `%DBNAME%`.`country`;
CREATE TABLE  `%DBNAME%`.`country` (
  `countryId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `a2` varchar(75) DEFAULT NULL,
  `a3` varchar(75) DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `idd_` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`countryId`),
  UNIQUE KEY `IX_717B97E1` (`a2`),
  UNIQUE KEY `IX_717B9BA2` (`a3`),
  UNIQUE KEY `IX_19DA007B` (`name`),
  KEY `IX_25D734CD` (`active_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`country`
--

/*!40000 ALTER TABLE `country` DISABLE KEYS */;
LOCK TABLES `country` WRITE;
INSERT INTO `%DBNAME%`.`country` VALUES  (1,'Canada','CA','CAN','124','001',1),
 (2,'China','CN','CHN','156','086',1),
 (3,'France','FR','FRA','250','033',1),
 (4,'Germany','DE','DEU','276','049',1),
 (5,'Hong Kong','HK','HKG','344','852',1),
 (6,'Hungary','HU','HUN','348','036',1),
 (7,'Israel','IL','ISR','376','972',1),
 (8,'Italy','IT','ITA','380','039',1),
 (9,'Japan','JP','JPN','392','081',1),
 (10,'South Korea','KR','KOR','410','082',1),
 (11,'Netherlands','NL','NLD','528','031',1),
 (12,'Portugal','PT','PRT','620','351',1),
 (13,'Russia','RU','RUS','643','007',1),
 (14,'Singapore','SG','SGP','702','065',1),
 (15,'Spain','ES','ESP','724','034',1),
 (16,'Turkey','TR','TUR','792','090',1),
 (17,'Vietnam','VM','VNM','704','084',1),
 (18,'United Kingdom','GB','GBR','826','044',1),
 (19,'United States','US','USA','840','001',1),
 (20,'Afghanistan','AF','AFG','4','093',1),
 (21,'Albania','AL','ALB','8','355',1),
 (22,'Algeria','DZ','DZA','12','213',1),
 (23,'American Samoa','AS','ASM','16','684',1),
 (24,'Andorra','AD','AND','20','376',1),
 (25,'Angola','AO','AGO','24','244',1),
 (26,'Anguilla','AI','AIA','660','264',1),
 (27,'Antarctica','AQ','ATA','10','672',1),
 (28,'Antigua','AG','ATG','28','268',1),
 (29,'Argentina','AR','ARG','32','054',1),
 (30,'Armenia','AM','ARM','51','374',1),
 (31,'Aruba','AW','ABW','533','297',1),
 (32,'Australia','AU','AUS','36','061',1),
 (33,'Austria','AT','AUT','40','043',1),
 (34,'Azerbaijan','AZ','AZE','31','994',1),
 (35,'Bahamas','BS','BHS','44','242',1),
 (36,'Bahrain','BH','BHR','48','973',1),
 (37,'Bangladesh','BD','BGD','50','880',1),
 (38,'Barbados','BB','BRB','52','246',1),
 (39,'Belarus','BY','BLR','112','375',1),
 (40,'Belgium','BE','BEL','56','032',1),
 (41,'Belize','BZ','BLZ','84','501',1),
 (42,'Benin','BJ','BEN','204','229',1),
 (43,'Bermuda','BM','BMU','60','441',1),
 (44,'Bhutan','BT','BTN','64','975',1),
 (45,'Bolivia','BO','BOL','68','591',1),
 (46,'Bosnia-Herzegovina','BA','BIH','70','387',1),
 (47,'Botswana','BW','BWA','72','267',1),
 (48,'Brazil','BR','BRA','76','055',1),
 (49,'British Virgin Islands','VG','VGB','92','284',1),
 (50,'Brunei','BN','BRN','96','673',1),
 (51,'Bulgaria','BG','BGR','100','359',1),
 (52,'Burkina Faso','BF','BFA','854','226',1),
 (53,'Burma (Myanmar)','MM','MMR','104','095',1),
 (54,'Burundi','BI','BDI','108','257',1),
 (55,'Cambodia','KH','KHM','116','855',1),
 (56,'Cameroon','CM','CMR','120','237',1),
 (57,'Cape Verde Island','CV','CPV','132','238',1),
 (58,'Cayman Islands','KY','CYM','136','345',1),
 (59,'Central African Republic','CF','CAF','140','236',1),
 (60,'Chad','TD','TCD','148','235',1),
 (61,'Chile','CL','CHL','152','056',1),
 (62,'Christmas Island','CX','CXR','162','061',1),
 (63,'Cocos Islands','CC','CCK','166','061',1),
 (64,'Colombia','CO','COL','170','057',1),
 (65,'Comoros','KM','COM','174','269',1),
 (66,'Republic of Congo','CD','COD','180','242',1),
 (67,'Democratic Republic of Congo','CG','COG','178','243',1),
 (68,'Cook Islands','CK','COK','184','682',1),
 (69,'Costa Rica','CR','CRI','188','506',1),
 (70,'Croatia','HR','HRV','191','385',1),
 (71,'Cuba','CU','CUB','192','053',1),
 (72,'Cyprus','CY','CYP','196','357',1),
 (73,'Czech Republic','CZ','CZE','203','420',1),
 (74,'Denmark','DK','DNK','208','045',1),
 (75,'Djibouti','DJ','DJI','262','253',1),
 (76,'Dominica','DM','DMA','212','767',1),
 (77,'Dominican Republic','DO','DOM','214','809',1),
 (78,'Ecuador','EC','ECU','218','593',1),
 (79,'Egypt','EG','EGY','818','020',1),
 (80,'El Salvador','SV','SLV','222','503',1),
 (81,'Equatorial Guinea','GQ','GNQ','226','240',1),
 (82,'Eritrea','ER','ERI','232','291',1),
 (83,'Estonia','EE','EST','233','372',1),
 (84,'Ethiopia','ET','ETH','231','251',1),
 (85,'Faeroe Islands','FO','FRO','234','298',1),
 (86,'Falkland Islands','FK','FLK','238','500',1),
 (87,'Fiji Islands','FJ','FJI','242','679',1),
 (88,'Finland','FI','FIN','246','358',1),
 (89,'French Guiana','GF','GUF','254','594',1),
 (90,'French Polynesia','PF','PYF','258','689',1),
 (91,'Gabon','GA','GAB','266','241',1),
 (92,'Gambia','GM','GMB','270','220',1),
 (93,'Georgia','GE','GEO','268','995',1),
 (94,'Ghana','GH','GHA','288','233',1),
 (95,'Gibraltar','GI','GIB','292','350',1),
 (96,'Greece','GR','GRC','300','030',1),
 (97,'Greenland','GL','GRL','304','299',1),
 (98,'Grenada','GD','GRD','308','473',1),
 (99,'Guadeloupe','GP','GLP','312','590',1),
 (100,'Guam','GU','GUM','316','671',1),
 (101,'Guatemala','GT','GTM','320','502',1),
 (102,'Guinea','GN','GIN','324','224',1),
 (103,'Guinea-Bissau','GW','GNB','624','245',1),
 (104,'Guyana','GY','GUY','328','592',1),
 (105,'Haiti','HT','HTI','332','509',1),
 (106,'Honduras','HN','HND','340','504',1),
 (107,'Iceland','IS','ISL','352','354',1),
 (108,'India','IN','IND','356','091',1),
 (109,'Indonesia','ID','IDN','360','062',1),
 (110,'Iran','IR','IRN','364','098',1),
 (111,'Iraq','IQ','IRQ','368','964',1),
 (112,'Ireland','IE','IRL','372','353',1),
 (113,'Ivory Coast','CI','CIV','384','225',1),
 (114,'Jamaica','JM','JAM','388','876',1),
 (115,'Jordan','JO','JOR','400','962',1),
 (116,'Kazakhstan','KZ','KAZ','398','007',1),
 (117,'Kenya','KE','KEN','404','254',1),
 (118,'Kiribati','KI','KIR','408','686',1),
 (119,'Kuwait','KW','KWT','414','965',1),
 (120,'North Korea','KP','PRK','408','850',1),
 (121,'Kyrgyzstan','KG','KGZ','471','996',1),
 (122,'Laos','LA','LAO','418','856',1),
 (123,'Latvia','LV','LVA','428','371',1),
 (124,'Lebanon','LB','LBN','422','961',1),
 (125,'Lesotho','LS','LSO','426','266',1),
 (126,'Liberia','LR','LBR','430','231',1),
 (127,'Libya','LY','LBY','434','218',1),
 (128,'Liechtenstein','LI','LIE','438','423',1),
 (129,'Lithuania','LT','LTU','440','370',1),
 (130,'Luxembourg','LU','LUX','442','352',1),
 (131,'Macau','MO','MAC','446','853',1),
 (132,'Macedonia','MK','MKD','807','389',1),
 (133,'Madagascar','MG','MDG','450','261',1),
 (134,'Malawi','MW','MWI','454','265',1),
 (135,'Malaysia','MY','MYS','458','060',1),
 (136,'Maldives','MV','MDV','462','960',1),
 (137,'Mali','ML','MLI','466','223',1),
 (138,'Malta','MT','MLT','470','356',1),
 (139,'Marshall Islands','MH','MHL','584','692',1),
 (140,'Martinique','MQ','MTQ','474','596',1),
 (141,'Mauritania','MR','MRT','478','222',1),
 (142,'Mauritius','MU','MUS','480','230',1),
 (143,'Mayotte Island','YT','MYT','175','269',1),
 (144,'Mexico','MX','MEX','484','052',1),
 (145,'Micronesia','FM','FSM','583','691',1),
 (146,'Moldova','MD','MDA','498','373',1),
 (147,'Monaco','MC','MCO','492','377',1),
 (148,'Mongolia','MN','MNG','496','976',1),
 (149,'Montenegro','ME','MNE','499','382',1),
 (150,'Montserrat','MS','MSR','500','664',1),
 (151,'Morocco','MA','MAR','504','212',1),
 (152,'Mozambique','MZ','MOZ','508','258',1),
 (153,'Namibia','NA','NAM','516','264',1),
 (154,'Nauru','NR','NRU','520','674',1),
 (155,'Nepal','NP','NPL','524','977',1),
 (156,'Netherlands Antilles','AN','ANT','530','599',1),
 (157,'New Caledonia','NC','NCL','540','687',1),
 (158,'New Zealand','NZ','NZL','554','064',1),
 (159,'Nicaragua','NI','NIC','558','505',1),
 (160,'Niger','NE','NER','562','227',1),
 (161,'Nigeria','NG','NGA','566','234',1),
 (162,'Niue','NU','NIU','570','683',1),
 (163,'Norfolk Island','NF','NFK','574','672',1),
 (164,'Norway','NO','NOR','578','047',1),
 (165,'Oman','OM','OMN','512','968',1),
 (166,'Pakistan','PK','PAK','586','092',1),
 (167,'Palau','PW','PLW','585','680',1),
 (168,'Palestine','PS','PSE','275','970',1),
 (169,'Panama','PA','PAN','591','507',1),
 (170,'Papua New Guinea','PG','PNG','598','675',1),
 (171,'Paraguay','PY','PRY','600','595',1),
 (172,'Peru','PE','PER','604','051',1),
 (173,'Philippines','PH','PHL','608','063',1),
 (174,'Poland','PL','POL','616','048',1),
 (175,'Puerto Rico','PR','PRI','630','787',1),
 (176,'Qatar','QA','QAT','634','974',1),
 (177,'Reunion Island','RE','REU','638','262',1),
 (178,'Romania','RO','ROU','642','040',1),
 (179,'Rwanda','RW','RWA','646','250',1),
 (180,'St. Helena','SH','SHN','654','290',1),
 (181,'St. Kitts','KN','KNA','659','869',1),
 (182,'St. Lucia','LC','LCA','662','758',1),
 (183,'St. Pierre & Miquelon','PM','SPM','666','508',1),
 (184,'St. Vincent','VC','VCT','670','784',1),
 (185,'San Marino','SM','SMR','674','378',1),
 (186,'Sao Tome & Principe','ST','STP','678','239',1),
 (187,'Saudi Arabia','SA','SAU','682','966',1),
 (188,'Senegal','SN','SEN','686','221',1),
 (189,'Serbia','RS','SRB','688','381',1),
 (190,'Seychelles','SC','SYC','690','248',1),
 (191,'Sierra Leone','SL','SLE','694','249',1),
 (192,'Slovakia','SK','SVK','703','421',1),
 (193,'Slovenia','SI','SVN','705','386',1),
 (194,'Solomon Islands','SB','SLB','90','677',1),
 (195,'Somalia','SO','SOM','706','252',1),
 (196,'South Africa','ZA','ZAF','710','027',1),
 (197,'Sri Lanka','LK','LKA','144','094',1),
 (198,'Sudan','SD','SDN','736','095',1),
 (199,'Suriname','SR','SUR','740','597',1),
 (200,'Swaziland','SZ','SWZ','748','268',1),
 (201,'Sweden','SE','SWE','752','046',1),
 (202,'Switzerland','CH','CHE','756','041',1),
 (203,'Syria','SY','SYR','760','963',1),
 (204,'Taiwan','TW','TWN','158','886',1),
 (205,'Tajikistan','TJ','TJK','762','992',1),
 (206,'Tanzania','TZ','TZA','834','255',1),
 (207,'Thailand','TH','THA','764','066',1),
 (208,'Togo','TG','TGO','768','228',1),
 (209,'Tonga','TO','TON','776','676',1),
 (210,'Trinidad & Tobago','TT','TTO','780','868',1),
 (211,'Tunisia','TN','TUN','788','216',1),
 (212,'Turkmenistan','TM','TKM','795','993',1),
 (213,'Turks & Caicos','TC','TCA','796','649',1),
 (214,'Tuvalu','TV','TUV','798','688',1),
 (215,'Uganda','UG','UGA','800','256',1),
 (216,'Ukraine','UA','UKR','804','380',1),
 (217,'United Arab Emirates','AE','ARE','784','971',1),
 (218,'Uruguay','UY','URY','858','598',1),
 (219,'Uzbekistan','UZ','UZB','860','998',1),
 (220,'Vanuatu','VU','VUT','548','678',1),
 (221,'Vatican City','VA','VAT','336','039',1),
 (222,'Venezuela','VE','VEN','862','058',1),
 (223,'Wallis & Futuna','WF','WLF','876','681',1),
 (224,'Western Samoa','EH','ESH','732','685',1),
 (225,'Yemen','YE','YEM','887','967',1),
 (226,'Zambia','ZM','ZMB','894','260',1),
 (227,'Zimbabwe','ZW','ZWE','716','263',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`cyrususer`
--

DROP TABLE IF EXISTS `%DBNAME%`.`cyrususer`;
CREATE TABLE  `%DBNAME%`.`cyrususer` (
  `userId` varchar(75) NOT NULL,
  `password_` varchar(75) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`cyrususer`
--

/*!40000 ALTER TABLE `cyrususer` DISABLE KEYS */;
LOCK TABLES `cyrususer` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `cyrususer` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`cyrusvirtual`
--

DROP TABLE IF EXISTS `%DBNAME%`.`cyrusvirtual`;
CREATE TABLE  `%DBNAME%`.`cyrusvirtual` (
  `emailAddress` varchar(75) NOT NULL,
  `userId` varchar(75) NOT NULL,
  PRIMARY KEY (`emailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`cyrusvirtual`
--

/*!40000 ALTER TABLE `cyrusvirtual` DISABLE KEYS */;
LOCK TABLES `cyrusvirtual` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `cyrusvirtual` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`dlfileentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`dlfileentry`;
CREATE TABLE  `%DBNAME%`.`dlfileentry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `fileEntryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `versionUserId` bigint(20) DEFAULT NULL,
  `versionUserName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `version` double DEFAULT NULL,
  `size_` int(11) DEFAULT NULL,
  `readCount` int(11) DEFAULT NULL,
  `extraSettings` longtext,
  PRIMARY KEY (`fileEntryId`),
  UNIQUE KEY `IX_8F6C75D0` (`folderId`,`name`),
  UNIQUE KEY `IX_BC2E7E6A` (`uuid_`,`groupId`),
  KEY `IX_4CB1B2B4` (`companyId`),
  KEY `IX_24A846D1` (`folderId`),
  KEY `IX_A9951F17` (`folderId`,`title`),
  KEY `IX_F4AF5636` (`groupId`),
  KEY `IX_43261870` (`groupId`,`userId`),
  KEY `IX_64F0FE40` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`dlfileentry`
--

/*!40000 ALTER TABLE `dlfileentry` DISABLE KEYS */;
LOCK TABLES `dlfileentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dlfileentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`dlfilerank`
--

DROP TABLE IF EXISTS `%DBNAME%`.`dlfilerank`;
CREATE TABLE  `%DBNAME%`.`dlfilerank` (
  `fileRankId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileRankId`),
  UNIQUE KEY `IX_CE705D48` (`companyId`,`userId`,`folderId`,`name`),
  KEY `IX_40B56512` (`folderId`,`name`),
  KEY `IX_BAFB116E` (`groupId`,`userId`),
  KEY `IX_EED06670` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`dlfilerank`
--

/*!40000 ALTER TABLE `dlfilerank` DISABLE KEYS */;
LOCK TABLES `dlfilerank` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dlfilerank` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`dlfileshortcut`
--

DROP TABLE IF EXISTS `%DBNAME%`.`dlfileshortcut`;
CREATE TABLE  `%DBNAME%`.`dlfileshortcut` (
  `uuid_` varchar(75) DEFAULT NULL,
  `fileShortcutId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `toFolderId` bigint(20) DEFAULT NULL,
  `toName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileShortcutId`),
  UNIQUE KEY `IX_FDB4A946` (`uuid_`,`groupId`),
  KEY `IX_E56EC6AD` (`folderId`),
  KEY `IX_CA2708A2` (`toFolderId`,`toName`),
  KEY `IX_4831EBE4` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`dlfileshortcut`
--

/*!40000 ALTER TABLE `dlfileshortcut` DISABLE KEYS */;
LOCK TABLES `dlfileshortcut` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dlfileshortcut` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`dlfileversion`
--

DROP TABLE IF EXISTS `%DBNAME%`.`dlfileversion`;
CREATE TABLE  `%DBNAME%`.`dlfileversion` (
  `fileVersionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `size_` int(11) DEFAULT NULL,
  PRIMARY KEY (`fileVersionId`),
  UNIQUE KEY `IX_6C5E6512` (`folderId`,`name`,`version`),
  KEY `IX_9CD91DB6` (`folderId`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`dlfileversion`
--

/*!40000 ALTER TABLE `dlfileversion` DISABLE KEYS */;
LOCK TABLES `dlfileversion` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dlfileversion` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`dlfolder`
--

DROP TABLE IF EXISTS `%DBNAME%`.`dlfolder`;
CREATE TABLE  `%DBNAME%`.`dlfolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` longtext,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_902FD874` (`groupId`,`parentFolderId`,`name`),
  UNIQUE KEY `IX_3CC1DED2` (`uuid_`,`groupId`),
  KEY `IX_A74DB14C` (`companyId`),
  KEY `IX_F2EA1ACE` (`groupId`),
  KEY `IX_49C37475` (`groupId`,`parentFolderId`),
  KEY `IX_51556082` (`parentFolderId`,`name`),
  KEY `IX_CBC408D8` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`dlfolder`
--

/*!40000 ALTER TABLE `dlfolder` DISABLE KEYS */;
LOCK TABLES `dlfolder` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dlfolder` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`emailaddress`
--

DROP TABLE IF EXISTS `%DBNAME%`.`emailaddress`;
CREATE TABLE  `%DBNAME%`.`emailaddress` (
  `emailAddressId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `address` varchar(75) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`emailAddressId`),
  KEY `IX_1BB072CA` (`companyId`),
  KEY `IX_49D2DEC4` (`companyId`,`classNameId`),
  KEY `IX_551A519F` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_2A2CB130` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_7B43CD8` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`emailaddress`
--

/*!40000 ALTER TABLE `emailaddress` DISABLE KEYS */;
LOCK TABLES `emailaddress` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `emailaddress` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`expandocolumn`
--

DROP TABLE IF EXISTS `%DBNAME%`.`expandocolumn`;
CREATE TABLE  `%DBNAME%`.`expandocolumn` (
  `columnId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `defaultData` longtext,
  `typeSettings` longtext,
  PRIMARY KEY (`columnId`),
  UNIQUE KEY `IX_FEFC8DA7` (`tableId`,`name`),
  KEY `IX_A8C0CBE8` (`tableId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`expandocolumn`
--

/*!40000 ALTER TABLE `expandocolumn` DISABLE KEYS */;
LOCK TABLES `expandocolumn` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `expandocolumn` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`expandorow`
--

DROP TABLE IF EXISTS `%DBNAME%`.`expandorow`;
CREATE TABLE  `%DBNAME%`.`expandorow` (
  `rowId_` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`rowId_`),
  UNIQUE KEY `IX_81EFBFF5` (`tableId`,`classPK`),
  KEY `IX_D3F5D7AE` (`tableId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`expandorow`
--

/*!40000 ALTER TABLE `expandorow` DISABLE KEYS */;
LOCK TABLES `expandorow` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `expandorow` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`expandotable`
--

DROP TABLE IF EXISTS `%DBNAME%`.`expandotable`;
CREATE TABLE  `%DBNAME%`.`expandotable` (
  `tableId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`tableId`),
  UNIQUE KEY `IX_37562284` (`companyId`,`classNameId`,`name`),
  KEY `IX_B5AE8A85` (`companyId`,`classNameId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`expandotable`
--

/*!40000 ALTER TABLE `expandotable` DISABLE KEYS */;
LOCK TABLES `expandotable` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `expandotable` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`expandovalue`
--

DROP TABLE IF EXISTS `%DBNAME%`.`expandovalue`;
CREATE TABLE  `%DBNAME%`.`expandovalue` (
  `valueId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `columnId` bigint(20) DEFAULT NULL,
  `rowId_` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `data_` longtext,
  PRIMARY KEY (`valueId`),
  UNIQUE KEY `IX_9DDD21E5` (`columnId`,`rowId_`),
  UNIQUE KEY `IX_D27B03E7` (`tableId`,`columnId`,`classPK`),
  KEY `IX_B29FEF17` (`classNameId`,`classPK`),
  KEY `IX_F7DD0987` (`columnId`),
  KEY `IX_9112A7A0` (`rowId_`),
  KEY `IX_F0566A77` (`tableId`),
  KEY `IX_1BD3F4C` (`tableId`,`classPK`),
  KEY `IX_CA9AFB7C` (`tableId`,`columnId`),
  KEY `IX_B71E92D5` (`tableId`,`rowId_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`expandovalue`
--

/*!40000 ALTER TABLE `expandovalue` DISABLE KEYS */;
LOCK TABLES `expandovalue` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `expandovalue` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`group_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`group_`;
CREATE TABLE  `%DBNAME%`.`group_` (
  `groupId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `creatorUserId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `parentGroupId` bigint(20) DEFAULT NULL,
  `liveGroupId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `type_` int(11) DEFAULT NULL,
  `typeSettings` longtext,
  `friendlyURL` varchar(100) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `IX_D0D5E397` (`companyId`,`classNameId`,`classPK`),
  UNIQUE KEY `IX_5DE0BE11` (`companyId`,`classNameId`,`liveGroupId`,`name`),
  UNIQUE KEY `IX_5BDDB872` (`companyId`,`friendlyURL`),
  UNIQUE KEY `IX_BBCA55B` (`companyId`,`liveGroupId`,`name`),
  UNIQUE KEY `IX_5AA68501` (`companyId`,`name`),
  KEY `IX_ABA5CEC2` (`companyId`),
  KEY `IX_16218A38` (`liveGroupId`),
  KEY `IX_7B590A7A` (`type_`,`active_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`group_`
--

/*!40000 ALTER TABLE `group_` DISABLE KEYS */;
LOCK TABLES `group_` WRITE;
INSERT INTO `%DBNAME%`.`group_` VALUES  (10131,10114,10117,10009,10131,0,0,'Control Panel','',3,'','/control_panel',1),
 (10138,10114,10117,10009,10138,0,0,'Guest','',1,'','/guest',1),
 (10148,10114,10146,10039,10146,0,0,'10146','',0,'','/test',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `group_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`groups_orgs`
--

DROP TABLE IF EXISTS `%DBNAME%`.`groups_orgs`;
CREATE TABLE  `%DBNAME%`.`groups_orgs` (
  `groupId` bigint(20) NOT NULL,
  `organizationId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`organizationId`),
  KEY `IX_75267DCA` (`groupId`),
  KEY `IX_6BBB7682` (`organizationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`groups_orgs`
--

/*!40000 ALTER TABLE `groups_orgs` DISABLE KEYS */;
LOCK TABLES `groups_orgs` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `groups_orgs` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`groups_permissions`
--

DROP TABLE IF EXISTS `%DBNAME%`.`groups_permissions`;
CREATE TABLE  `%DBNAME%`.`groups_permissions` (
  `groupId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`permissionId`),
  KEY `IX_C48736B` (`groupId`),
  KEY `IX_EC97689D` (`permissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`groups_permissions`
--

/*!40000 ALTER TABLE `groups_permissions` DISABLE KEYS */;
LOCK TABLES `groups_permissions` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `groups_permissions` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`groups_roles`
--

DROP TABLE IF EXISTS `%DBNAME%`.`groups_roles`;
CREATE TABLE  `%DBNAME%`.`groups_roles` (
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`roleId`),
  KEY `IX_84471FD2` (`groupId`),
  KEY `IX_3103EF3D` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`groups_roles`
--

/*!40000 ALTER TABLE `groups_roles` DISABLE KEYS */;
LOCK TABLES `groups_roles` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `groups_roles` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`groups_usergroups`
--

DROP TABLE IF EXISTS `%DBNAME%`.`groups_usergroups`;
CREATE TABLE  `%DBNAME%`.`groups_usergroups` (
  `groupId` bigint(20) NOT NULL,
  `userGroupId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`userGroupId`),
  KEY `IX_31FB749A` (`groupId`),
  KEY `IX_3B69160F` (`userGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`groups_usergroups`
--

/*!40000 ALTER TABLE `groups_usergroups` DISABLE KEYS */;
LOCK TABLES `groups_usergroups` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `groups_usergroups` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`igfolder`
--

DROP TABLE IF EXISTS `%DBNAME%`.`igfolder`;
CREATE TABLE  `%DBNAME%`.`igfolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_9BBAFB1E` (`groupId`,`parentFolderId`,`name`),
  UNIQUE KEY `IX_B10EFD68` (`uuid_`,`groupId`),
  KEY `IX_60214CF6` (`companyId`),
  KEY `IX_206498F8` (`groupId`),
  KEY `IX_1A605E9F` (`groupId`,`parentFolderId`),
  KEY `IX_F73C0982` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`igfolder`
--

/*!40000 ALTER TABLE `igfolder` DISABLE KEYS */;
LOCK TABLES `igfolder` WRITE;
INSERT INTO `%DBNAME%`.`igfolder` VALUES  ('81b5c9c0-d33c-4ac3-8192-4d66b1003ad7',10205,10138,10114,10117,'2010-08-03 10:15:22','2010-08-03 10:18:35',0,'Album','Album');
UNLOCK TABLES;
/*!40000 ALTER TABLE `igfolder` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`igimage`
--

DROP TABLE IF EXISTS `%DBNAME%`.`igimage`;
CREATE TABLE  `%DBNAME%`.`igimage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `imageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `smallImageId` bigint(20) DEFAULT NULL,
  `largeImageId` bigint(20) DEFAULT NULL,
  `custom1ImageId` bigint(20) DEFAULT NULL,
  `custom2ImageId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`imageId`),
  UNIQUE KEY `IX_E97342D9` (`uuid_`,`groupId`),
  KEY `IX_E597322D` (`custom1ImageId`),
  KEY `IX_D9E0A34C` (`custom2ImageId`),
  KEY `IX_4438CA80` (`folderId`),
  KEY `IX_BCB13A3F` (`folderId`,`name`),
  KEY `IX_63820A7` (`groupId`),
  KEY `IX_BE79E1E1` (`groupId`,`userId`),
  KEY `IX_64F0B572` (`largeImageId`),
  KEY `IX_D3D32126` (`smallImageId`),
  KEY `IX_265BB0F1` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`igimage`
--

/*!40000 ALTER TABLE `igimage` DISABLE KEYS */;
LOCK TABLES `igimage` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `igimage` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`image`
--

DROP TABLE IF EXISTS `%DBNAME%`.`image`;
CREATE TABLE  `%DBNAME%`.`image` (
  `imageId` bigint(20) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `text_` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `size_` int(11) DEFAULT NULL,
  PRIMARY KEY (`imageId`),
  KEY `IX_6A925A4D` (`size_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`image`
--

/*!40000 ALTER TABLE `image` DISABLE KEYS */;
LOCK TABLES `image` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalarticle`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalarticle`;
CREATE TABLE  `%DBNAME%`.`journalarticle` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `resourcePrimKey` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `urlTitle` varchar(150) DEFAULT NULL,
  `description` longtext,
  `content` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `displayDate` datetime DEFAULT NULL,
  `approved` tinyint(4) DEFAULT NULL,
  `approvedByUserId` bigint(20) DEFAULT NULL,
  `approvedByUserName` varchar(75) DEFAULT NULL,
  `approvedDate` datetime DEFAULT NULL,
  `expired` tinyint(4) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `reviewDate` datetime DEFAULT NULL,
  `indexable` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_85C52EEC` (`groupId`,`articleId`,`version`),
  UNIQUE KEY `IX_3463D95B` (`uuid_`,`groupId`),
  KEY `IX_DFF98523` (`companyId`),
  KEY `IX_9356F865` (`groupId`),
  KEY `IX_68C0F69C` (`groupId`,`articleId`),
  KEY `IX_8DBF1387` (`groupId`,`articleId`,`approved`),
  KEY `IX_2E207659` (`groupId`,`structureId`),
  KEY `IX_8DEAE14E` (`groupId`,`templateId`),
  KEY `IX_22882D02` (`groupId`,`urlTitle`),
  KEY `IX_8B9A026D` (`groupId`,`urlTitle`,`approved`),
  KEY `IX_76186981` (`resourcePrimKey`,`approved`),
  KEY `IX_EF9B7028` (`smallImageId`),
  KEY `IX_F029602F` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalarticle`
--

/*!40000 ALTER TABLE `journalarticle` DISABLE KEYS */;
LOCK TABLES `journalarticle` WRITE;
INSERT INTO `%DBNAME%`.`journalarticle` VALUES  ('df33f27e-1de4-4baf-b215-8f23ec4f77d7',10388,10389,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','HOME',1,'HOME','home','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:48',0,NULL,NULL,1,0,10390,''),
 ('fbb11b85-2f07-4f67-b9fc-9457bcf6e848',10395,10396,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','UNDER_CONSTRUCTION',1,'UNDER_CONSTRUCTION','under_construction','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:48',0,NULL,NULL,1,0,10397,''),
 ('ef2788e7-6678-48d6-84d1-043f04434379',10402,10403,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:49','ORGANISATOREN',1,'ORGANISATOREN','organisatoren','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10404,''),
 ('15152880-6a04-4145-be8f-d5f176e2c62f',10409,10410,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49','ORG_LIONS_BREUGHEL',1,'ORG_LIONS_BREUGHEL','org_lions_breughel','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10411,''),
 ('beefbb33-4601-4b00-951a-3bdfedbacd08',10416,10417,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49','ORG_LIONS_INTERNATIONAL',1,'ORG_LIONS_INTERNATIONAL','org_lions_international','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10418,''),
 ('6c6504fc-d0e8-45f9-aa0a-c9f8d59e6e82',10423,10424,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49','ORG_HART_VOOR_LIMBURG',1,'ORG_HART_VOOR_LIMBURG','org_hart_voor_limburg','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10425,''),
 ('7fb7c66e-a844-48f5-9528-17e4294952b9',10430,10431,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49','ORG_HBVL',1,'ORG_HBVL','org_hbvl','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10432,''),
 ('c0bfcde6-1a17-4b37-8f8e-fc77fd58a4bc',10437,10438,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49','ORG_TVL',1,'ORG_TVL','org_tvl','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:49',0,NULL,NULL,1,0,10439,''),
 ('88737e72-0749-4047-b6a0-d8a5b5552497',10444,10445,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO',1,'INFO','info','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10446,''),
 ('ead32290-581f-4409-8924-fa33e64ec5c3',10451,10452,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO_EVENEMENT',1,'INFO_EVENEMENT','info_evenement','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10453,''),
 ('32d16270-8b17-43cc-bbfc-2fc3a68ab799',10458,10459,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO_REGLEMENT',1,'INFO_REGLEMENT','info_reglement','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10460,''),
 ('a066b8c4-ae27-4a9f-b014-3e3f92cd9c97',10465,10466,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO_RACEVERLOOP',1,'INFO_RACEVERLOOP','info_raceverloop','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10467,''),
 ('6a8b27e5-fb91-49d2-a49f-867e7b104043',10472,10473,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO_HOOFDPRIJS',1,'INFO_HOOFDPRIJS','info_hoofdprijs','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10474,''),
 ('da06dd07-35c9-4b4d-a6de-891b90d03de8',10479,10480,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','INFO_OPBRENGST',1,'INFO_OPBRENGST','info_opbrengst','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10481,''),
 ('70a6a88b-b58e-4654-88c3-a73078e4724a',10486,10487,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50','LOGOS',1,'LOGOS','logos','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"P13pwf1e\" name=\"dummy\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','LOGOS','LOGOS','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:50',0,NULL,NULL,1,0,10488,''),
 ('8568a6dc-f8a1-43fd-98c5-c61b8620980a',10493,10494,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','DUCKTYPE_MAIN_SPONSORS',1,'DUCKTYPE_MAIN_SPONSORS','ducktype_main_sponsors','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"DAdtAm3v\" name=\"subtitle\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"W9J8kdzj\" name=\"description\" type=\"text_box\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','DUCKTYPE','DUCKTYPE','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10495,''),
 ('6986ef18-4130-4bfe-9a79-3f23e5a607b1',10500,10501,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','DUCKTYPE_100_DUCKS',1,'DUCKTYPE_100_DUCKS','ducktype_100_ducks','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"DAdtAm3v\" name=\"subtitle\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"W9J8kdzj\" name=\"description\" type=\"text_box\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','DUCKTYPE','DUCKTYPE','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10502,''),
 ('80535e6e-0151-4917-bfbd-5472d6e76c4a',10507,10508,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','DUCKTYPE_50_DUCKS',1,'DUCKTYPE_50_DUCKS','ducktype_50_ducks','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"DAdtAm3v\" name=\"subtitle\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"W9J8kdzj\" name=\"description\" type=\"text_box\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','DUCKTYPE','DUCKTYPE','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10509,''),
 ('d3367589-20c7-4d70-a6a8-8bb756130794',10514,10515,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','DUCKTYPE_20_DUCKS',1,'DUCKTYPE_20_DUCKS','ducktype_20_ducks','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"DAdtAm3v\" name=\"subtitle\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"W9J8kdzj\" name=\"description\" type=\"text_box\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','DUCKTYPE','DUCKTYPE','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10516,''),
 ('706c2af4-0248-41a6-a3ae-8ab978ad0fe5',10521,10522,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','DUCKTYPE_10_DUCKS',1,'DUCKTYPE_10_DUCKS','ducktype_10_ducks','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"DAdtAm3v\" name=\"subtitle\" type=\"text\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"W9J8kdzj\" name=\"description\" type=\"text_box\">\n		<dynamic-content><![CDATA[]]></dynamic-content>\n	</dynamic-element>\n</root>','general','DUCKTYPE','DUCKTYPE','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10523,''),
 ('8aba87bf-148a-42b8-9c78-f4f983039895',10528,10529,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','FOTO_ALBUM',1,'FOTO_ALBUM','foto_album','','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<root>\n	<dynamic-element instance-id=\"0eJsbwYt\" name=\"number-of-columns\" type=\"text\">\n		<dynamic-content><![CDATA[15]]></dynamic-content>\n	</dynamic-element>\n	<dynamic-element instance-id=\"WBbPjMO6\" name=\"thumbnail-height\" type=\"text\">\n		<dynamic-content><![CDATA[50]]></dynamic-content>\n	</dynamic-element>\n</root>','general','GALLERY','GALLERY','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10530,''),
 ('1d648e58-66f7-450d-ab47-61b7b84f774e',10535,10536,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51','PERSCONFERENTIES',1,'PERSCONFERENTIES','persconferenties','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:51',0,NULL,NULL,1,0,10537,''),
 ('f4b4ccda-6c05-4e9e-ae2b-f1b9057dbfb7',10542,10543,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:52','REACTIES',1,'REACTIES','reacties','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10544,''),
 ('7919ec5a-185e-45e9-995f-bc33aaa76ac1',10549,10550,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52','EVENT',1,'EVENT','event','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10551,''),
 ('7eefe9a0-0561-4ac8-94ae-d8d4a18c5e57',10556,10557,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52','EVENT_PROGRAMMA',1,'EVENT_PROGRAMMA','event_programma','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10558,''),
 ('d66c202f-e4f5-4369-9a34-3287fadfdfb0',10563,10564,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52','EVENT_ACTS',1,'EVENT_ACTS','event_acts','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10565,''),
 ('36e61196-3456-4805-8e9c-0a4495ca0894',10570,10571,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52','EVENT_BEVERAGE',1,'EVENT_BEVERAGE','event_beverage','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10572,''),
 ('c7de7255-6e91-4247-95ae-1329e56b8af9',10577,10578,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52','EVENT_BIJSTAND',1,'EVENT_BIJSTAND','event_bijstand','','<?xml version=\"1.0\" encoding=\"UTF-8\"?><root available-locales=\"en_US\" default-locale=\"en_US\"><static-content language-id=\"en_US\"><![CDATA[]]></static-content></root>','general','','','1900-01-01 12:00:00',1,10146,'Test Test','2010-08-03 10:15:52',0,NULL,NULL,1,0,10579,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalarticle` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalarticleimage`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalarticleimage`;
CREATE TABLE  `%DBNAME%`.`journalarticleimage` (
  `articleImageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `elInstanceId` varchar(75) DEFAULT NULL,
  `elName` varchar(75) DEFAULT NULL,
  `languageId` varchar(75) DEFAULT NULL,
  `tempImage` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`articleImageId`),
  UNIQUE KEY `IX_103D6207` (`groupId`,`articleId`,`version`,`elInstanceId`,`elName`,`languageId`),
  KEY `IX_3B51BB68` (`groupId`),
  KEY `IX_158B526F` (`groupId`,`articleId`,`version`),
  KEY `IX_D4121315` (`tempImage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalarticleimage`
--

/*!40000 ALTER TABLE `journalarticleimage` DISABLE KEYS */;
LOCK TABLES `journalarticleimage` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalarticleimage` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalarticleresource`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalarticleresource`;
CREATE TABLE  `%DBNAME%`.`journalarticleresource` (
  `resourcePrimKey` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`resourcePrimKey`),
  UNIQUE KEY `IX_88DF994A` (`groupId`,`articleId`),
  KEY `IX_F8433677` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalarticleresource`
--

/*!40000 ALTER TABLE `journalarticleresource` DISABLE KEYS */;
LOCK TABLES `journalarticleresource` WRITE;
INSERT INTO `%DBNAME%`.`journalarticleresource` VALUES  (10501,10138,'DUCKTYPE_100_DUCKS'),
 (10522,10138,'DUCKTYPE_10_DUCKS'),
 (10515,10138,'DUCKTYPE_20_DUCKS'),
 (10508,10138,'DUCKTYPE_50_DUCKS'),
 (10494,10138,'DUCKTYPE_MAIN_SPONSORS'),
 (10550,10138,'EVENT'),
 (10564,10138,'EVENT_ACTS'),
 (10571,10138,'EVENT_BEVERAGE'),
 (10578,10138,'EVENT_BIJSTAND'),
 (10557,10138,'EVENT_PROGRAMMA'),
 (10529,10138,'FOTO_ALBUM'),
 (10389,10138,'HOME'),
 (10445,10138,'INFO'),
 (10452,10138,'INFO_EVENEMENT'),
 (10473,10138,'INFO_HOOFDPRIJS'),
 (10480,10138,'INFO_OPBRENGST'),
 (10466,10138,'INFO_RACEVERLOOP'),
 (10459,10138,'INFO_REGLEMENT'),
 (10487,10138,'LOGOS'),
 (10403,10138,'ORGANISATOREN'),
 (10424,10138,'ORG_HART_VOOR_LIMBURG'),
 (10431,10138,'ORG_HBVL'),
 (10410,10138,'ORG_LIONS_BREUGHEL'),
 (10417,10138,'ORG_LIONS_INTERNATIONAL'),
 (10438,10138,'ORG_TVL'),
 (10536,10138,'PERSCONFERENTIES'),
 (10543,10138,'REACTIES'),
 (10396,10138,'UNDER_CONSTRUCTION');
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalarticleresource` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalcontentsearch`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalcontentsearch`;
CREATE TABLE  `%DBNAME%`.`journalcontentsearch` (
  `contentSearchId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `layoutId` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`contentSearchId`),
  UNIQUE KEY `IX_C3AA93B8` (`groupId`,`privateLayout`,`layoutId`,`portletId`,`articleId`),
  KEY `IX_9207CB31` (`articleId`),
  KEY `IX_6838E427` (`groupId`,`articleId`),
  KEY `IX_20962903` (`groupId`,`privateLayout`),
  KEY `IX_7CC7D73E` (`groupId`,`privateLayout`,`articleId`),
  KEY `IX_B3B318DC` (`groupId`,`privateLayout`,`layoutId`),
  KEY `IX_7ACC74C9` (`groupId`,`privateLayout`,`layoutId`,`portletId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalcontentsearch`
--

/*!40000 ALTER TABLE `journalcontentsearch` DISABLE KEYS */;
LOCK TABLES `journalcontentsearch` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalcontentsearch` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalfeed`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalfeed`;
CREATE TABLE  `%DBNAME%`.`journalfeed` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `feedId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `rendererTemplateId` varchar(75) DEFAULT NULL,
  `delta` int(11) DEFAULT NULL,
  `orderByCol` varchar(75) DEFAULT NULL,
  `orderByType` varchar(75) DEFAULT NULL,
  `targetLayoutFriendlyUrl` varchar(75) DEFAULT NULL,
  `targetPortletId` varchar(75) DEFAULT NULL,
  `contentField` varchar(75) DEFAULT NULL,
  `feedType` varchar(75) DEFAULT NULL,
  `feedVersion` double DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_65576CBC` (`groupId`,`feedId`),
  UNIQUE KEY `IX_39031F51` (`uuid_`,`groupId`),
  KEY `IX_35A2DB2F` (`groupId`),
  KEY `IX_50C36D79` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalfeed`
--

/*!40000 ALTER TABLE `journalfeed` DISABLE KEYS */;
LOCK TABLES `journalfeed` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalfeed` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journalstructure`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journalstructure`;
CREATE TABLE  `%DBNAME%`.`journalstructure` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `parentStructureId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `xsd` longtext,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_AB6E9996` (`groupId`,`structureId`),
  UNIQUE KEY `IX_42E86E58` (`uuid_`,`groupId`),
  KEY `IX_B97F5608` (`groupId`),
  KEY `IX_CA0BD48C` (`groupId`,`parentStructureId`),
  KEY `IX_8831E4FC` (`structureId`),
  KEY `IX_6702CA92` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journalstructure`
--

/*!40000 ALTER TABLE `journalstructure` DISABLE KEYS */;
LOCK TABLES `journalstructure` WRITE;
INSERT INTO `%DBNAME%`.`journalstructure` VALUES  ('961d55cc-701d-4060-ba62-a638adc2a98d',10373,10138,10114,10146,'Test Test','2010-08-03 10:15:47','2010-08-03 10:15:47','MERCHANT','','MERCHANT','MERCHANT','<root>\n	<dynamic-element name=\"description\" type=\"text_box\" repeatable=\"false\"/>\n</root>'),
 ('b3856e9b-85a2-42f9-800a-21e41171e291',10374,10138,10114,10146,'Test Test','2010-08-03 10:15:47','2010-08-03 10:15:47','DUCKTYPE','','DUCKTYPE','DUCKTYPE','<root>\n	<dynamic-element name=\"subtitle\" type=\"text\" repeatable=\"false\"/>\n	<dynamic-element name=\"description\" type=\"text_box\" repeatable=\"false\"/>\n</root>'),
 ('5454f25c-2c09-4bea-b0ba-080ccf22912e',10375,10138,10114,10146,'Test Test','2010-08-03 10:15:47','2010-08-03 10:15:47','LOGOS','','LOGOS','LOGOS','<root>\n	<dynamic-element name=\"dummy\" type=\"text\" repeatable=\"false\"/>\n</root>'),
 ('73293ab8-b7e4-4416-8fae-a0308e20e8fe',10376,10138,10114,10146,'Test Test','2010-08-03 10:15:47','2010-08-03 10:15:47','GALLERY','','GALLERY','GALLERY','<root>\n	<dynamic-element name=\"number-of-columns\" type=\"text\"/>\n	<dynamic-element name=\"thumbnail-height\" type=\"text\"/>\n</root>'),
 ('6aa8a0a5-e939-420e-9b6a-4f0e2e212fe9',10377,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','SPONSOR','','SPONSOR','SPONSOR','<root>\n	<dynamic-element name=\"logo\" type=\"image_gallery\" repeatable=\"false\"/>\n	<dynamic-element name=\"address\" type=\"text\" repeatable=\"false\"/>\n	<dynamic-element name=\"website-url\" type=\"text\" repeatable=\"false\"/>\n</root>');
UNLOCK TABLES;
/*!40000 ALTER TABLE `journalstructure` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`journaltemplate`
--

DROP TABLE IF EXISTS `%DBNAME%`.`journaltemplate`;
CREATE TABLE  `%DBNAME%`.`journaltemplate` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `xsl` longtext,
  `langType` varchar(75) DEFAULT NULL,
  `cacheable` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_E802AA3C` (`groupId`,`templateId`),
  UNIQUE KEY `IX_62D1B3AD` (`uuid_`,`groupId`),
  KEY `IX_77923653` (`groupId`),
  KEY `IX_1701CB2B` (`groupId`,`structureId`),
  KEY `IX_25FFB6FA` (`smallImageId`),
  KEY `IX_1B12CA20` (`templateId`),
  KEY `IX_2857419D` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`journaltemplate`
--

/*!40000 ALTER TABLE `journaltemplate` DISABLE KEYS */;
LOCK TABLES `journaltemplate` WRITE;
INSERT INTO `%DBNAME%`.`journaltemplate` VALUES  ('0f717a1a-72fa-4c92-9c6c-861dd0006412',10378,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','MERCHANT','MERCHANT','MERCHANT','MERCHANT','<h3>$reserved-article-title.data</h3>\n<p>$description.data</p>','vm',0,0,10379,''),
 ('4b4ca383-475d-44c3-bec6-668af706fda9',10380,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','DUCKTYPE','DUCKTYPE','DUCKTYPE','DUCKTYPE','<div class=\"sponsor-intro\">\n	<img src=\"/eendjes-theme/images/ducks/${subtitle.data}-duck.png\" alt=\"$reserved-article-title.data\"/>\n\n	<div class=\"sponsor-text\">\n		<h2>$reserved-article-title.data<span>($subtitle.data)</span></h2>\n		<p>$description.data</p>\n	</div>\n</div>','vm',0,0,10381,''),
 ('e8c7a388-2280-4a68-a59b-c83562b61147',10382,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','LOGOS','LOGOS','LOGOS','LOGOS','#set ($imageService = $serviceLocator.findService(\"com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil\"))\n#set ($duckRaceUtil = $serviceLocator.findService(\"be.lions.duckrace.service.DuckRaceLocalServiceUtil\"))\n\n#set ($result = $saxReaderUtil.readURL(\"http://localhost/c/journal/get_articles?structureId=SPONSOR&delta=999\"))\n#set ($sponsors = $result.getRootElement().selectNodes(\"/result-set/result/root\", \"dynamic-element[@name=\'reserved-article-title\']/dynamic-content\"))\n\n<table class=\"list-logos\">\n	<tr>\n		#set ($count = 0)\n		#foreach ($sponsor in $sponsors)\n			#set ($logoUrl = $sponsor.selectSingleNode(\"dynamic-element[@name=\'logo\']/dynamic-content\").text)\n			#if ($logoUrl != \"\")\n				#set ($name = $sponsor.selectSingleNode(\"dynamic-element[@name=\'reserved-article-title\']/dynamic-content\").text)\n				#set ($imgUuid = $httpUtil.getParameter($logoUrl, \"uuid\", false))\n				#set ($imgGroupId = $getterUtil.getLong($httpUtil.getParameter($logoUrl, \"groupId\", false)))\n				#set ($image = $imageService.getImageByUuidAndGroupId($imgUuid, $imgGroupId))\n				#set ($id = $sponsor.selectSingleNode(\"dynamic-element[@name=\'reserved-article-id\']/dynamic-content\").text)\n				#set ($url = $sponsor.selectSingleNode(\"dynamic-element[@name=\'website-url\']/dynamic-content\").text)\n				#if (!$stringUtil.startsWith($url, \"http://\"))\n					#set ($url = \"http://$url\")\n				#end\n				#set ($stars = $duckRaceUtil.getNbStars($id))\n				#if ($stars == 0)\n					<td>\n						<a href=\"$url\" target=\"_blank\">\n							<img src=\"/image/image_gallery?img_id=${image.smallImageId}\" alt=\"$name\" style=\"border:none\"/>\n						</a>\n					</td>\n					#set ($count = $count + 1)\n				#end\n				#if ($count % 5 == 0)\n					</tr><tr>\n					#set ($count = 0)\n				#end\n			#end\n		#end\n	</tr>\n</table>','vm',0,0,10383,''),
 ('f485b5f1-c473-4154-8a1f-05fb44a55c49',10384,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','SPONSOR','SPONSOR','SPONSOR','SPONSOR','#set ($imageService = $serviceLocator.findService(\"com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil\"))\n#set ($duckService = $serviceLocator.findService(\"be.lions.duckrace.service.DuckRaceLocalServiceUtil\"))\n\n#set ($stars = $duckService.getNbStars($reserved-article-id.data))\n#set ($sponsorName = $reserved-article-title.data)\n#set ($logoUrl = $logo.data)\n\n#set ($imgUuid = $httpUtil.getParameter($logoUrl, \"uuid\", false))\n#set ($imgGroupId = $getterUtil.getLong($httpUtil.getParameter($logoUrl, \"groupId\", false)))\n#set ($image = $imageService.getImageByUuidAndGroupId($imgUuid, $imgGroupId))\n#set ($sponsorAddress = $address.data)\n#set ($url = $website-url.data)\n#if(!$stringUtil.startsWith($url, \"http://\"))\n	#set ($url = \"http://$url\")\n#end\n\n\n<a href=\"$url\" class=\"left\">\n	<img class=\"logo\" src=\"/image/image_gallery?img_id=${image.custom1ImageId}\" alt=\"$sponsorName\"/>\n</a>\n\n<div class=\"sponsor-desc\">\n	<h2>$sponsorName</h2>\n	\n	<p>\n		<b>Adres:</b> $sponsorAddress<br/>\n	</p>\n</div>','vm',0,0,10385,''),
 ('fa6ed552-58d8-42f7-b10a-88091d984133',10386,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48','GALLERY','GALLERY','GALLERY','GALLERY','#set ($duckUtil = $serviceLocator.findService(\"be.lions.duckrace.service.DuckRaceLocalServiceUtil\"))\n#set ($folderUtil = $serviceLocator.findService(\"com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil\"))\n#set ($imageUtil = $serviceLocator.findService(\"com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil\"))\n\n#set ($group_id = $getterUtil.getLong($groupId))\n#set ($folders = $duckUtil.getGalleryFolders())\n\n#set ($themeImgPath = \"/eendjes-theme/images\")\n#set ($imgGallery= \"/image/image_gallery\")\n\n<style type=\"text/css\">\n.image_gallery td {\n  text-align: center;\n  padding: 6px;\n}\n.image_gallery img {\n  border: 1px solid #ccc;\n  height: ${thumbnail-height.data}px;\n}\n.image_gallery img:hover {\n  border-color: #fff;\n}\n</style>\n\n<script type=\"text/javascript\">\n  jQuery(document).ready(function() {\n    jQuery(\".image_gallery a\").lightBox({\n      imageLoading: \'${themeImgPath}/lightbox/loading.gif\',\n      imageBtnClose: \'${themeImgPath}/lightbox/close.gif\',\n      imageBtnPrev: \'${themeImgPath}/lightbox/prev.gif\',\n      imageBtnNext: \'${themeImgPath}/lightbox/next.gif\',\n      imageBlank: \'${themeImgPath}/lightbox/blank.gif\',\n      containerResizeSpeed: 550\n    });\n  });\n</script>\n\n#foreach($folder in $folders)\n<h2>$folder.name</h2>\n<p>$folder.description</p>\n<table cellspacing=\"0\" cellpadding=\"0\" class=\"image_gallery\">\n<tr>\n#foreach ($image in  $duckUtil.getGalleryImages($folder))\n    #set ($largeId = $image.custom2ImageId)\n    #set ($smallId = $image.smallImageId)\n    #set ($name = $image.name)\n    <td>\n      <a title=\"$name\" href=\"${imgGallery}?img_id=$largeId\">\n        <img alt=\"$name\" src=\"${imgGallery}?img_id=$smallId\"/>\n      </a>\n    </td>\n    #if ($velocityCount % $getterUtil.getInteger($number-of-columns.data) == 0)\n      </tr><tr>\n    #end\n#end\n</tr>\n</table>\n<br/><br/>\n#end','vm',0,0,10387,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `journaltemplate` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`layout`
--

DROP TABLE IF EXISTS `%DBNAME%`.`layout`;
CREATE TABLE  `%DBNAME%`.`layout` (
  `plid` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `layoutId` bigint(20) DEFAULT NULL,
  `parentLayoutId` bigint(20) DEFAULT NULL,
  `name` longtext,
  `title` longtext,
  `description` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `typeSettings` longtext,
  `hidden_` tinyint(4) DEFAULT NULL,
  `friendlyURL` varchar(100) DEFAULT NULL,
  `iconImage` tinyint(4) DEFAULT NULL,
  `iconImageId` bigint(20) DEFAULT NULL,
  `themeId` varchar(75) DEFAULT NULL,
  `colorSchemeId` varchar(75) DEFAULT NULL,
  `wapThemeId` varchar(75) DEFAULT NULL,
  `wapColorSchemeId` varchar(75) DEFAULT NULL,
  `css` longtext,
  `priority` int(11) DEFAULT NULL,
  `dlFolderId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`plid`),
  UNIQUE KEY `IX_BC2C4231` (`groupId`,`privateLayout`,`friendlyURL`),
  UNIQUE KEY `IX_7162C27C` (`groupId`,`privateLayout`,`layoutId`),
  KEY `IX_C7FBC998` (`companyId`),
  KEY `IX_FAD05595` (`dlFolderId`),
  KEY `IX_C099D61A` (`groupId`),
  KEY `IX_705F5AA3` (`groupId`,`privateLayout`),
  KEY `IX_6DE88B06` (`groupId`,`privateLayout`,`parentLayoutId`),
  KEY `IX_1A1B61D2` (`groupId`,`privateLayout`,`type_`),
  KEY `IX_23922F7D` (`iconImageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`layout`
--

/*!40000 ALTER TABLE `layout` DISABLE KEYS */;
LOCK TABLES `layout` WRITE;
INSERT INTO `%DBNAME%`.`layout` VALUES  (10134,10131,10114,1,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US\" default-locale=\"en_US\"><name language-id=\"en_US\">Control Panel</name></root>','','','control_panel','',0,'/manage',0,0,'','','','','',0,0),
 (10608,10138,10114,0,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Home</name><name language-id=\"en_US\">Home</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_aIl8,',0,'/home',0,0,'','','','','',0,0),
 (10613,10138,10114,0,2,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Organisatoren</name><name language-id=\"en_US\">Organisatoren</name></root>','','','link_to_layout','linkToLayoutId=3',0,'/organisatoren',0,0,'','','','','',1,0),
 (10618,10138,10114,0,3,2,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Lions Brueghel</name><name language-id=\"en_US\">Lions Brueghel</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_bG17,',0,'/organisatoren/lions-brueghel',0,0,'','','','','',0,0),
 (10623,10138,10114,0,4,2,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Een Hart voor Limburg</name><name language-id=\"en_US\">Een Hart voor Limburg</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_0bJR,',0,'/organisatoren/een-hart-voor-limburg',0,0,'','','','','',1,0),
 (10628,10138,10114,0,5,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Info</name><name language-id=\"en_US\">Info</name></root>','','','link_to_layout','linkToLayoutId=6',0,'/info',0,0,'','','','','',2,0),
 (10633,10138,10114,0,6,5,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Evenement</name><name language-id=\"en_US\">Evenement</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_g8H2,',0,'/info/evenement',0,0,'','','','','',0,0),
 (10638,10138,10114,0,7,5,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Reglement</name><name language-id=\"en_US\">Reglement</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_iY7a,',0,'/info/reglement',0,0,'','','','','',1,0),
 (10643,10138,10114,0,8,5,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Race verloop</name><name language-id=\"en_US\">Race verloop</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_2w1I,',0,'/info/race-verloop',0,0,'','','','','',2,0),
 (10648,10138,10114,0,9,5,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Hoofdprijs</name><name language-id=\"en_US\">Hoofdprijs</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_qG8b,',0,'/info/hoofdprijs',0,0,'','','','','',3,0),
 (10653,10138,10114,0,10,5,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Opbrengst</name><name language-id=\"en_US\">Opbrengst</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_uKR0,',0,'/info/opbrengst',0,0,'','','','','',4,0),
 (10658,10138,10114,0,11,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Sponsors</name><name language-id=\"en_US\">Sponsors</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_D1gQ,',0,'/sponsors',0,0,'','','','','',3,0),
 (10663,10138,10114,0,12,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Hoofdsponsors (diamant)</name><name language-id=\"en_US\">Hoofdsponsors (diamant)</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_Eg7m,101_INSTANCE_JlA0,',0,'/sponsors/hoofdsponsors',0,0,'','','','','',0,0),
 (10669,10138,10114,0,13,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Sponsors (goud)</name><name language-id=\"en_US\">Sponsors (goud)</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_DF3g,101_INSTANCE_f3Z6,',0,'/sponsors/goud',0,0,'','','','','',1,0),
 (10675,10138,10114,0,14,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Sponsors (zilver)</name><name language-id=\"en_US\">Sponsors (zilver)</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_f1hA,101_INSTANCE_HA7g,',0,'/sponsors/zilver',0,0,'','','','','',2,0),
 (10681,10138,10114,0,15,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Sponsors (brons)</name><name language-id=\"en_US\">Sponsors (brons)</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_85Wa,101_INSTANCE_si8G,',0,'/sponsors/brons',0,0,'','','','','',3,0),
 (10687,10138,10114,0,16,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Sponsors (uit sympathie)</name><name language-id=\"en_US\">Sponsors (uit sympathie)</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_G4z8,101_INSTANCE_6NBd,',0,'/sponsors/uit-sympathie',0,0,'','','','','',4,0),
 (10693,10138,10114,0,17,11,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Deelnemende handelaars</name><name language-id=\"en_US\">Deelnemende handelaars</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=101_INSTANCE_V6yb,',0,'/sponsors/handelaars',0,0,'','','','','',5,0),
 (10698,10138,10114,0,18,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Nieuws</name><name language-id=\"en_US\">Nieuws</name></root>','','','link_to_layout','linkToLayoutId=19',0,'/nieuws',0,0,'','','','','',4,0),
 (10702,10138,10114,0,19,18,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Blogs</name><name language-id=\"en_US\">Blogs</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=101_INSTANCE_FOr4,',0,'/nieuws/blogs',0,0,'','','','','',0,0),
 (10707,10138,10114,0,20,18,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Foto-album</name><name language-id=\"en_US\">Foto-album</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_5Wiu,',0,'/nieuws/foto-album',0,0,'','','','','',1,0),
 (10712,10138,10114,0,21,18,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Persconferenties</name><name language-id=\"en_US\">Persconferenties</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_DR8q,',0,'/nieuws/persconferenties',0,0,'','','','','',2,0),
 (10717,10138,10114,0,22,18,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Reacties</name><name language-id=\"en_US\">Reacties</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_8nJn,',0,'/nieuws/reacties',0,0,'','','','','',3,0),
 (10722,10138,10114,0,23,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">4 Juli 2010</name><name language-id=\"en_US\">4 Juli 2010</name></root>','','','link_to_layout','linkToLayoutId=24',0,'/4juli2010',0,0,'','','','','',5,0),
 (10727,10138,10114,0,24,23,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Programma</name><name language-id=\"en_US\">Programma</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_a7M9,',0,'/4juli2010/programma',0,0,'','','','','',0,0),
 (10732,10138,10114,0,25,23,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Activiteiten</name><name language-id=\"en_US\">Activiteiten</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_PyB4,',0,'/4juli2010/activiteiten',0,0,'','','','','',1,0),
 (10737,10138,10114,0,26,23,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Eten &amp; Drinken</name><name language-id=\"en_US\">Eten &amp; Drinken</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_Gb2i,',0,'/4juli2010/eten-en-drinken',0,0,'','','','','',2,0),
 (10742,10138,10114,0,27,23,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Medische bijstand</name><name language-id=\"en_US\">Medische bijstand</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=56_INSTANCE_6PcY,',0,'/4juli2010/medische-bijstand',0,0,'','','','','',3,0),
 (10747,10138,10114,0,28,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Registreren</name><name language-id=\"en_US\">Registreren</name></root>','','','link_to_layout','linkToLayoutId=29',0,'/registreren',0,0,'','','','','',6,0),
 (10751,10138,10114,0,29,28,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Registratie / Bestelling</name><name language-id=\"en_US\">Registratie / Bestelling</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=RegistrationPortlet_WAR_registrationportlet,',0,'/mijn-eendjes/registreren',0,0,'','','','','',0,0),
 (10756,10138,10114,0,30,28,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Mijn eendjesnummers</name><name language-id=\"en_US\">Mijn eendjesnummers</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=QueryRegistratedDucksPortlet_WAR_registrationportlet,',0,'/mijn-eendjes/eendjesnummers',0,0,'','','','','',1,0),
 (10761,10138,10114,0,31,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US,nl_NL,\" default-locale=\"en_US\"><name language-id=\"nl_NL\">Admin</name><name language-id=\"en_US\">Admin</name></root>','','','portlet','layout-template-id=1_column\ncolumn-1=58,',1,'/admin',0,0,'','','','','',7,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `layout` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`layoutset`
--

DROP TABLE IF EXISTS `%DBNAME%`.`layoutset`;
CREATE TABLE  `%DBNAME%`.`layoutset` (
  `layoutSetId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `logo` tinyint(4) DEFAULT NULL,
  `logoId` bigint(20) DEFAULT NULL,
  `themeId` varchar(75) DEFAULT NULL,
  `colorSchemeId` varchar(75) DEFAULT NULL,
  `wapThemeId` varchar(75) DEFAULT NULL,
  `wapColorSchemeId` varchar(75) DEFAULT NULL,
  `css` longtext,
  `pageCount` int(11) DEFAULT NULL,
  `virtualHost` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`layoutSetId`),
  UNIQUE KEY `IX_48550691` (`groupId`,`privateLayout`),
  KEY `IX_A40B8BEC` (`groupId`),
  KEY `IX_5ABC2905` (`virtualHost`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`layoutset`
--

/*!40000 ALTER TABLE `layoutset` DISABLE KEYS */;
LOCK TABLES `layoutset` WRITE;
INSERT INTO `%DBNAME%`.`layoutset` VALUES  (10132,10131,10114,1,0,0,'classic','01','mobile','01','',1,''),
 (10133,10131,10114,0,0,0,'classic','01','mobile','01','',0,''),
 (10139,10138,10114,1,0,0,'eendjes_WAR_eendjestheme','01','mobile','01','',0,''),
 (10140,10138,10114,0,0,0,'eendjes_WAR_eendjestheme','01','mobile','01','',31,''),
 (10149,10148,10114,1,0,0,'classic','01','mobile','01','',0,''),
 (10150,10148,10114,0,0,0,'classic','01','mobile','01','',0,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `layoutset` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`listtype`
--

DROP TABLE IF EXISTS `%DBNAME%`.`listtype`;
CREATE TABLE  `%DBNAME%`.`listtype` (
  `listTypeId` int(11) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`listTypeId`),
  KEY `IX_2932DD37` (`type_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`listtype`
--

/*!40000 ALTER TABLE `listtype` DISABLE KEYS */;
LOCK TABLES `listtype` WRITE;
INSERT INTO `%DBNAME%`.`listtype` VALUES  (10000,'Billing','com.liferay.portal.model.Account.address'),
 (10001,'Other','com.liferay.portal.model.Account.address'),
 (10002,'P.O. Box','com.liferay.portal.model.Account.address'),
 (10003,'Shipping','com.liferay.portal.model.Account.address'),
 (10004,'E-mail','com.liferay.portal.model.Account.emailAddress'),
 (10005,'E-mail 2','com.liferay.portal.model.Account.emailAddress'),
 (10006,'E-mail 3','com.liferay.portal.model.Account.emailAddress'),
 (10007,'Fax','com.liferay.portal.model.Account.phone'),
 (10008,'Local','com.liferay.portal.model.Account.phone'),
 (10009,'Other','com.liferay.portal.model.Account.phone'),
 (10010,'Toll-Free','com.liferay.portal.model.Account.phone'),
 (10011,'TTY','com.liferay.portal.model.Account.phone'),
 (10012,'Intranet','com.liferay.portal.model.Account.website'),
 (10013,'Public','com.liferay.portal.model.Account.website'),
 (11000,'Business','com.liferay.portal.model.Contact.address'),
 (11001,'Other','com.liferay.portal.model.Contact.address'),
 (11002,'Personal','com.liferay.portal.model.Contact.address'),
 (11003,'E-mail','com.liferay.portal.model.Contact.emailAddress'),
 (11004,'E-mail 2','com.liferay.portal.model.Contact.emailAddress'),
 (11005,'E-mail 3','com.liferay.portal.model.Contact.emailAddress'),
 (11006,'Business','com.liferay.portal.model.Contact.phone'),
 (11007,'Business Fax','com.liferay.portal.model.Contact.phone'),
 (11008,'Mobile','com.liferay.portal.model.Contact.phone'),
 (11009,'Other','com.liferay.portal.model.Contact.phone'),
 (11010,'Pager','com.liferay.portal.model.Contact.phone'),
 (11011,'Personal','com.liferay.portal.model.Contact.phone'),
 (11012,'Personal Fax','com.liferay.portal.model.Contact.phone'),
 (11013,'TTY','com.liferay.portal.model.Contact.phone'),
 (11014,'Dr.','com.liferay.portal.model.Contact.prefix'),
 (11015,'Mr.','com.liferay.portal.model.Contact.prefix'),
 (11016,'Mrs.','com.liferay.portal.model.Contact.prefix'),
 (11017,'Ms.','com.liferay.portal.model.Contact.prefix'),
 (11020,'II','com.liferay.portal.model.Contact.suffix'),
 (11021,'III','com.liferay.portal.model.Contact.suffix'),
 (11022,'IV','com.liferay.portal.model.Contact.suffix'),
 (11023,'Jr.','com.liferay.portal.model.Contact.suffix'),
 (11024,'PhD.','com.liferay.portal.model.Contact.suffix'),
 (11025,'Sr.','com.liferay.portal.model.Contact.suffix'),
 (11026,'Blog','com.liferay.portal.model.Contact.website'),
 (11027,'Business','com.liferay.portal.model.Contact.website'),
 (11028,'Other','com.liferay.portal.model.Contact.website'),
 (11029,'Personal','com.liferay.portal.model.Contact.website'),
 (12000,'Billing','com.liferay.portal.model.Organization.address'),
 (12001,'Other','com.liferay.portal.model.Organization.address'),
 (12002,'P.O. Box','com.liferay.portal.model.Organization.address'),
 (12003,'Shipping','com.liferay.portal.model.Organization.address'),
 (12004,'E-mail','com.liferay.portal.model.Organization.emailAddress'),
 (12005,'E-mail 2','com.liferay.portal.model.Organization.emailAddress'),
 (12006,'E-mail 3','com.liferay.portal.model.Organization.emailAddress'),
 (12007,'Fax','com.liferay.portal.model.Organization.phone'),
 (12008,'Local','com.liferay.portal.model.Organization.phone'),
 (12009,'Other','com.liferay.portal.model.Organization.phone'),
 (12010,'Toll-Free','com.liferay.portal.model.Organization.phone'),
 (12011,'TTY','com.liferay.portal.model.Organization.phone'),
 (12012,'Administrative','com.liferay.portal.model.Organization.service'),
 (12013,'Contracts','com.liferay.portal.model.Organization.service'),
 (12014,'Donation','com.liferay.portal.model.Organization.service'),
 (12015,'Retail','com.liferay.portal.model.Organization.service'),
 (12016,'Training','com.liferay.portal.model.Organization.service'),
 (12017,'Full Member','com.liferay.portal.model.Organization.status'),
 (12018,'Provisional Member','com.liferay.portal.model.Organization.status'),
 (12019,'Intranet','com.liferay.portal.model.Organization.website'),
 (12020,'Public','com.liferay.portal.model.Organization.website');
UNLOCK TABLES;
/*!40000 ALTER TABLE `listtype` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`lock_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`lock_`;
CREATE TABLE  `%DBNAME%`.`lock_` (
  `uuid_` varchar(75) DEFAULT NULL,
  `lockId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `className` varchar(75) DEFAULT NULL,
  `key_` varchar(200) DEFAULT NULL,
  `owner` varchar(75) DEFAULT NULL,
  `inheritable` tinyint(4) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`lockId`),
  KEY `IX_228562AD` (`className`,`key_`),
  KEY `IX_E3F1286B` (`expirationDate`),
  KEY `IX_13C5CD3A` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`lock_`
--

/*!40000 ALTER TABLE `lock_` DISABLE KEYS */;
LOCK TABLES `lock_` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `lock_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbban`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbban`;
CREATE TABLE  `%DBNAME%`.`mbban` (
  `banId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `banUserId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`banId`),
  UNIQUE KEY `IX_8ABC4E3B` (`groupId`,`banUserId`),
  KEY `IX_69951A25` (`banUserId`),
  KEY `IX_5C3FF12A` (`groupId`),
  KEY `IX_48814BBA` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbban`
--

/*!40000 ALTER TABLE `mbban` DISABLE KEYS */;
LOCK TABLES `mbban` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbban` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbcategory`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbcategory`;
CREATE TABLE  `%DBNAME%`.`mbcategory` (
  `uuid_` varchar(75) DEFAULT NULL,
  `categoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentCategoryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `threadCount` int(11) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `IX_F7D28C2F` (`uuid_`,`groupId`),
  KEY `IX_BC735DCF` (`companyId`),
  KEY `IX_BB870C11` (`groupId`),
  KEY `IX_ED292508` (`groupId`,`parentCategoryId`),
  KEY `IX_C2626EDB` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbcategory`
--

/*!40000 ALTER TABLE `mbcategory` DISABLE KEYS */;
LOCK TABLES `mbcategory` WRITE;
INSERT INTO `%DBNAME%`.`mbcategory` VALUES  ('2db4da9d-f77f-4286-94bb-3e3076131378',0,0,0,0,'',NULL,NULL,0,'','',58,58,'2010-08-03 10:18:49');
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbcategory` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbdiscussion`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbdiscussion`;
CREATE TABLE  `%DBNAME%`.`mbdiscussion` (
  `discussionId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`discussionId`),
  UNIQUE KEY `IX_33A4DE38` (`classNameId`,`classPK`),
  UNIQUE KEY `IX_B5CA2DC` (`threadId`),
  KEY `IX_79D0120B` (`classNameId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbdiscussion`
--

/*!40000 ALTER TABLE `mbdiscussion` DISABLE KEYS */;
LOCK TABLES `mbdiscussion` WRITE;
INSERT INTO `%DBNAME%`.`mbdiscussion` VALUES  (10137,10011,10134,10136),
 (10393,10067,10389,10392),
 (10400,10067,10396,10399),
 (10407,10067,10403,10406),
 (10414,10067,10410,10413),
 (10421,10067,10417,10420),
 (10428,10067,10424,10427),
 (10435,10067,10431,10434),
 (10442,10067,10438,10441),
 (10449,10067,10445,10448),
 (10456,10067,10452,10455),
 (10463,10067,10459,10462),
 (10470,10067,10466,10469),
 (10477,10067,10473,10476),
 (10484,10067,10480,10483),
 (10491,10067,10487,10490),
 (10498,10067,10494,10497),
 (10505,10067,10501,10504),
 (10512,10067,10508,10511),
 (10519,10067,10515,10518),
 (10526,10067,10522,10525),
 (10533,10067,10529,10532),
 (10540,10067,10536,10539),
 (10547,10067,10543,10546),
 (10554,10067,10550,10553),
 (10561,10067,10557,10560),
 (10568,10067,10564,10567),
 (10575,10067,10571,10574),
 (10582,10067,10578,10581),
 (10611,10011,10608,10610),
 (10616,10011,10613,10615),
 (10621,10011,10618,10620),
 (10626,10011,10623,10625),
 (10631,10011,10628,10630),
 (10636,10011,10633,10635),
 (10641,10011,10638,10640),
 (10646,10011,10643,10645),
 (10651,10011,10648,10650),
 (10656,10011,10653,10655),
 (10661,10011,10658,10660),
 (10666,10011,10663,10665),
 (10672,10011,10669,10671),
 (10678,10011,10675,10677),
 (10684,10011,10681,10683),
 (10690,10011,10687,10689),
 (10696,10011,10693,10695),
 (10701,10011,10698,10700),
 (10705,10011,10702,10704),
 (10710,10011,10707,10709),
 (10715,10011,10712,10714),
 (10720,10011,10717,10719),
 (10725,10011,10722,10724),
 (10730,10011,10727,10729),
 (10735,10011,10732,10734),
 (10740,10011,10737,10739),
 (10745,10011,10742,10744),
 (10750,10011,10747,10749),
 (10754,10011,10751,10753),
 (10759,10011,10756,10758),
 (10764,10011,10761,10763);
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbdiscussion` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbmailinglist`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbmailinglist`;
CREATE TABLE  `%DBNAME%`.`mbmailinglist` (
  `uuid_` varchar(75) DEFAULT NULL,
  `mailingListId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `emailAddress` varchar(75) DEFAULT NULL,
  `inProtocol` varchar(75) DEFAULT NULL,
  `inServerName` varchar(75) DEFAULT NULL,
  `inServerPort` int(11) DEFAULT NULL,
  `inUseSSL` tinyint(4) DEFAULT NULL,
  `inUserName` varchar(75) DEFAULT NULL,
  `inPassword` varchar(75) DEFAULT NULL,
  `inReadInterval` int(11) DEFAULT NULL,
  `outEmailAddress` varchar(75) DEFAULT NULL,
  `outCustom` tinyint(4) DEFAULT NULL,
  `outServerName` varchar(75) DEFAULT NULL,
  `outServerPort` int(11) DEFAULT NULL,
  `outUseSSL` tinyint(4) DEFAULT NULL,
  `outUserName` varchar(75) DEFAULT NULL,
  `outPassword` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`mailingListId`),
  UNIQUE KEY `IX_ADA16FE7` (`categoryId`),
  UNIQUE KEY `IX_E858F170` (`uuid_`,`groupId`),
  KEY `IX_BFEB984F` (`active_`),
  KEY `IX_4115EC7A` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbmailinglist`
--

/*!40000 ALTER TABLE `mbmailinglist` DISABLE KEYS */;
LOCK TABLES `mbmailinglist` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbmailinglist` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbmessage`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbmessage`;
CREATE TABLE  `%DBNAME%`.`mbmessage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `messageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  `parentMessageId` bigint(20) DEFAULT NULL,
  `subject` varchar(75) DEFAULT NULL,
  `body` longtext,
  `attachments` tinyint(4) DEFAULT NULL,
  `anonymous` tinyint(4) DEFAULT NULL,
  `priority` double DEFAULT NULL,
  PRIMARY KEY (`messageId`),
  UNIQUE KEY `IX_8D12316E` (`uuid_`,`groupId`),
  KEY `IX_3C865EE5` (`categoryId`),
  KEY `IX_138C7F1E` (`categoryId`,`threadId`),
  KEY `IX_51A8D44D` (`classNameId`,`classPK`),
  KEY `IX_B1432D30` (`companyId`),
  KEY `IX_5B153FB2` (`groupId`),
  KEY `IX_8EB8C5EC` (`groupId`,`userId`),
  KEY `IX_75B95071` (`threadId`),
  KEY `IX_A7038CD7` (`threadId`,`parentMessageId`),
  KEY `IX_C57B16BC` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbmessage`
--

/*!40000 ALTER TABLE `mbmessage` DISABLE KEYS */;
LOCK TABLES `mbmessage` WRITE;
INSERT INTO `%DBNAME%`.`mbmessage` VALUES  ('f206432f-13af-41d3-997f-2c62d16c6a14',10135,0,10114,10117,'','2010-08-03 09:47:10','2010-08-03 09:47:10',10011,10134,0,10136,0,'10134','10134',0,1,0),
 ('8be3bc75-b848-487b-8995-ba629f8f2a72',10391,0,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10389,0,10392,0,'10389','10389',0,0,0),
 ('0f92a8db-05b7-4b60-8e47-5f422fbd3f05',10398,0,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10396,0,10399,0,'10396','10396',0,0,0),
 ('1068d24b-bb3a-470a-91c5-dce010d5de56',10405,0,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10403,0,10406,0,'10403','10403',0,0,0),
 ('b9846875-02fe-49c1-844c-9059f34a5caa',10412,0,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10410,0,10413,0,'10410','10410',0,0,0),
 ('540ec756-e0bc-46a2-b7fc-83bd80f837f6',10419,0,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10417,0,10420,0,'10417','10417',0,0,0),
 ('fc49f535-85e2-4e7f-ae6a-4e0b0a9b59a4',10426,0,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10424,0,10427,0,'10424','10424',0,0,0),
 ('6408b872-ecb0-4782-819a-59f202aa9453',10433,0,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10431,0,10434,0,'10431','10431',0,0,0),
 ('003e3f09-aa95-40f6-bdbe-941495e7c900',10440,0,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10438,0,10441,0,'10438','10438',0,0,0),
 ('3aa24103-6341-4c5b-a7c7-801d835351f2',10447,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10445,0,10448,0,'10445','10445',0,0,0),
 ('e6c2d61d-1044-4a26-ba27-15ef02c11bda',10454,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10452,0,10455,0,'10452','10452',0,0,0),
 ('1cc47215-db1b-47f0-a730-e0103e0f13e8',10461,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10459,0,10462,0,'10459','10459',0,0,0),
 ('02f30484-5893-4dbc-a991-2a11980f9894',10468,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10466,0,10469,0,'10466','10466',0,0,0),
 ('42173ea0-2cb0-4809-adad-9eaab82203a8',10475,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10473,0,10476,0,'10473','10473',0,0,0),
 ('f3096901-2fd5-47e7-82da-b0ee82d6d357',10482,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10480,0,10483,0,'10480','10480',0,0,0),
 ('2bd7b9a6-0dcc-4ed9-832f-ed4003aeeff9',10489,0,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10487,0,10490,0,'10487','10487',0,0,0),
 ('fbe15266-40c1-4792-b799-7dd73fd9d926',10496,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10494,0,10497,0,'10494','10494',0,0,0),
 ('5928227f-7916-40ad-913e-ccf1b52ea9fe',10503,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10501,0,10504,0,'10501','10501',0,0,0),
 ('1a0251c9-805c-44ee-a1b5-789a094e5ba9',10510,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10508,0,10511,0,'10508','10508',0,0,0),
 ('3442ae01-b1c8-4ae5-8fd0-2a1c88efa77e',10517,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10515,0,10518,0,'10515','10515',0,0,0),
 ('9c7f9f46-5742-4d9c-ac32-fdf6232c9666',10524,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10522,0,10525,0,'10522','10522',0,0,0),
 ('5e853967-c703-4737-8e63-282a5a7448ce',10531,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10529,0,10532,0,'10529','10529',0,0,0),
 ('6b968823-2fee-43e5-b9f6-161e246b5904',10538,0,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10536,0,10539,0,'10536','10536',0,0,0),
 ('0c098de8-2de0-49f1-8829-1bb6487311b0',10545,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10543,0,10546,0,'10543','10543',0,0,0),
 ('bdad8e01-ed84-4bbb-a3e9-afdc9c0bac0b',10552,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10550,0,10553,0,'10550','10550',0,0,0),
 ('e254ae62-0a2b-45d3-9692-fe215e5b62e8',10559,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10557,0,10560,0,'10557','10557',0,0,0),
 ('b6e3c641-54ba-4264-b621-948c23139b96',10566,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10564,0,10567,0,'10564','10564',0,0,0),
 ('15992ed2-490a-4c17-b1eb-52b5094f326b',10573,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10571,0,10574,0,'10571','10571',0,0,0),
 ('e6dcb7e7-c1fc-4082-9db1-79fe126070ee',10580,0,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10578,0,10581,0,'10578','10578',0,0,0),
 ('b5ebb561-63af-4a71-866c-b4d88433af0f',10609,0,10114,10117,'','2010-08-03 10:18:38','2010-08-03 10:18:38',10011,10608,0,10610,0,'10608','10608',0,1,0),
 ('9a419988-469a-49d8-be0c-cdd5dfbbf32b',10614,0,10114,10117,'','2010-08-03 10:18:39','2010-08-03 10:18:39',10011,10613,0,10615,0,'10613','10613',0,1,0),
 ('91ff6374-ca0f-416d-b3d2-1ebd58134c66',10619,0,10114,10117,'','2010-08-03 10:18:39','2010-08-03 10:18:39',10011,10618,0,10620,0,'10618','10618',0,1,0),
 ('77a9b95c-a1bc-42e5-9da6-00638585b422',10624,0,10114,10117,'','2010-08-03 10:18:39','2010-08-03 10:18:39',10011,10623,0,10625,0,'10623','10623',0,1,0),
 ('4f5946ea-11ef-4cf5-8912-2b4e522d4bfa',10629,0,10114,10117,'','2010-08-03 10:18:40','2010-08-03 10:18:40',10011,10628,0,10630,0,'10628','10628',0,1,0),
 ('f308fbae-831d-48ad-8ef4-784714d71fec',10634,0,10114,10117,'','2010-08-03 10:18:40','2010-08-03 10:18:40',10011,10633,0,10635,0,'10633','10633',0,1,0),
 ('edc31991-6dea-4519-b3a1-0d9f65a0925c',10639,0,10114,10117,'','2010-08-03 10:18:40','2010-08-03 10:18:40',10011,10638,0,10640,0,'10638','10638',0,1,0),
 ('a34190ce-9279-499a-9837-bbfc189b9a53',10644,0,10114,10117,'','2010-08-03 10:18:41','2010-08-03 10:18:41',10011,10643,0,10645,0,'10643','10643',0,1,0),
 ('15b2366e-cfc7-4355-acf1-892b31a5586a',10649,0,10114,10117,'','2010-08-03 10:18:41','2010-08-03 10:18:41',10011,10648,0,10650,0,'10648','10648',0,1,0),
 ('03ea8ca4-fdac-45dc-83ed-b1676c4f1302',10654,0,10114,10117,'','2010-08-03 10:18:41','2010-08-03 10:18:41',10011,10653,0,10655,0,'10653','10653',0,1,0),
 ('a295eff5-1aa9-4bf8-938b-abd76983b1f2',10659,0,10114,10117,'','2010-08-03 10:18:41','2010-08-03 10:18:41',10011,10658,0,10660,0,'10658','10658',0,1,0),
 ('2a666036-60ad-45fe-924c-f28af5f65935',10664,0,10114,10117,'','2010-08-03 10:18:42','2010-08-03 10:18:42',10011,10663,0,10665,0,'10663','10663',0,1,0),
 ('040a12b4-a96c-4712-a083-6f06ff061313',10670,0,10114,10117,'','2010-08-03 10:18:42','2010-08-03 10:18:42',10011,10669,0,10671,0,'10669','10669',0,1,0),
 ('77ccfb1a-47bd-425e-ae97-6e2c21939b75',10676,0,10114,10117,'','2010-08-03 10:18:43','2010-08-03 10:18:43',10011,10675,0,10677,0,'10675','10675',0,1,0),
 ('ddba2254-a105-498e-bbca-608ef905a69c',10682,0,10114,10117,'','2010-08-03 10:18:43','2010-08-03 10:18:43',10011,10681,0,10683,0,'10681','10681',0,1,0),
 ('eb4c6a35-b6c2-454c-ab10-93d523d02b4c',10688,0,10114,10117,'','2010-08-03 10:18:44','2010-08-03 10:18:44',10011,10687,0,10689,0,'10687','10687',0,1,0),
 ('7009dcc5-60ba-460c-bb76-efaa1446e88f',10694,0,10114,10117,'','2010-08-03 10:18:45','2010-08-03 10:18:45',10011,10693,0,10695,0,'10693','10693',0,1,0),
 ('11fa4406-2f15-49ad-aaf5-d91de3d3edba',10699,0,10114,10117,'','2010-08-03 10:18:45','2010-08-03 10:18:45',10011,10698,0,10700,0,'10698','10698',0,1,0),
 ('baca9024-8f34-4dc8-bedc-d7aa4507b278',10703,0,10114,10117,'','2010-08-03 10:18:45','2010-08-03 10:18:45',10011,10702,0,10704,0,'10702','10702',0,1,0),
 ('f52149d3-3f25-457f-be0a-6a2d0f24969e',10708,0,10114,10117,'','2010-08-03 10:18:45','2010-08-03 10:18:45',10011,10707,0,10709,0,'10707','10707',0,1,0),
 ('4e1fd5f8-d56d-4091-a1af-90dcb8a9cbf8',10713,0,10114,10117,'','2010-08-03 10:18:46','2010-08-03 10:18:46',10011,10712,0,10714,0,'10712','10712',0,1,0),
 ('21cf215b-22e8-4491-8a35-d54835ebcb0d',10718,0,10114,10117,'','2010-08-03 10:18:46','2010-08-03 10:18:46',10011,10717,0,10719,0,'10717','10717',0,1,0),
 ('b5c245b7-b2fb-47d4-9439-5f1d7a5dce1f',10723,0,10114,10117,'','2010-08-03 10:18:46','2010-08-03 10:18:46',10011,10722,0,10724,0,'10722','10722',0,1,0),
 ('502238a5-e27d-4fe9-9168-227f8608ebf4',10728,0,10114,10117,'','2010-08-03 10:18:47','2010-08-03 10:18:47',10011,10727,0,10729,0,'10727','10727',0,1,0),
 ('4832e3b3-9f46-43af-b0ed-0ae83fb14fc0',10733,0,10114,10117,'','2010-08-03 10:18:47','2010-08-03 10:18:47',10011,10732,0,10734,0,'10732','10732',0,1,0),
 ('d5afdec5-4729-433a-b4f2-655e7c12b43e',10738,0,10114,10117,'','2010-08-03 10:18:47','2010-08-03 10:18:47',10011,10737,0,10739,0,'10737','10737',0,1,0),
 ('11593685-3aa5-4b83-84ac-f3e83c92f0a5',10743,0,10114,10117,'','2010-08-03 10:18:47','2010-08-03 10:18:47',10011,10742,0,10744,0,'10742','10742',0,1,0),
 ('bfb3feda-2e70-4c52-a6d1-02e884ef8e20',10748,0,10114,10117,'','2010-08-03 10:18:48','2010-08-03 10:18:48',10011,10747,0,10749,0,'10747','10747',0,1,0),
 ('aade4d55-6d5c-4c36-b171-a77c15611a67',10752,0,10114,10117,'','2010-08-03 10:18:48','2010-08-03 10:18:48',10011,10751,0,10753,0,'10751','10751',0,1,0),
 ('0615ae6f-4d68-4e83-82f7-b9b6b41538dc',10757,0,10114,10117,'','2010-08-03 10:18:48','2010-08-03 10:18:48',10011,10756,0,10758,0,'10756','10756',0,1,0),
 ('e7415d74-8934-4ec9-9946-5917e1f3f5ed',10762,0,10114,10117,'','2010-08-03 10:18:49','2010-08-03 10:18:49',10011,10761,0,10763,0,'10761','10761',0,1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbmessage` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbmessageflag`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbmessageflag`;
CREATE TABLE  `%DBNAME%`.`mbmessageflag` (
  `messageFlagId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  `messageId` bigint(20) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`messageFlagId`),
  UNIQUE KEY `IX_E9EB6194` (`userId`,`messageId`,`flag`),
  KEY `IX_D180D4AE` (`messageId`),
  KEY `IX_A6973A8E` (`messageId`,`flag`),
  KEY `IX_C1C9A8FD` (`threadId`),
  KEY `IX_3CFD579D` (`threadId`,`flag`),
  KEY `IX_7B2917BE` (`userId`),
  KEY `IX_2EA537D7` (`userId`,`threadId`,`flag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbmessageflag`
--

/*!40000 ALTER TABLE `mbmessageflag` DISABLE KEYS */;
LOCK TABLES `mbmessageflag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbmessageflag` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbstatsuser`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbstatsuser`;
CREATE TABLE  `%DBNAME%`.`mbstatsuser` (
  `statsUserId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`statsUserId`),
  UNIQUE KEY `IX_9168E2C9` (`groupId`,`userId`),
  KEY `IX_A00A898F` (`groupId`),
  KEY `IX_FAB5A88B` (`groupId`,`messageCount`),
  KEY `IX_847F92B5` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbstatsuser`
--

/*!40000 ALTER TABLE `mbstatsuser` DISABLE KEYS */;
LOCK TABLES `mbstatsuser` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbstatsuser` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`mbthread`
--

DROP TABLE IF EXISTS `%DBNAME%`.`mbthread`;
CREATE TABLE  `%DBNAME%`.`mbthread` (
  `threadId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `rootMessageId` bigint(20) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `viewCount` int(11) DEFAULT NULL,
  `lastPostByUserId` bigint(20) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  `priority` double DEFAULT NULL,
  PRIMARY KEY (`threadId`),
  KEY `IX_CB854772` (`categoryId`),
  KEY `IX_19D8B60A` (`categoryId`,`lastPostDate`),
  KEY `IX_95C0EA45` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`mbthread`
--

/*!40000 ALTER TABLE `mbthread` DISABLE KEYS */;
LOCK TABLES `mbthread` WRITE;
INSERT INTO `%DBNAME%`.`mbthread` VALUES  (10136,0,0,10135,1,0,0,'2010-08-03 09:47:10',0),
 (10392,0,0,10391,1,0,10146,'2010-08-03 10:15:48',0),
 (10399,0,0,10398,1,0,10146,'2010-08-03 10:15:48',0),
 (10406,0,0,10405,1,0,10146,'2010-08-03 10:15:48',0),
 (10413,0,0,10412,1,0,10146,'2010-08-03 10:15:49',0),
 (10420,0,0,10419,1,0,10146,'2010-08-03 10:15:49',0),
 (10427,0,0,10426,1,0,10146,'2010-08-03 10:15:49',0),
 (10434,0,0,10433,1,0,10146,'2010-08-03 10:15:49',0),
 (10441,0,0,10440,1,0,10146,'2010-08-03 10:15:49',0),
 (10448,0,0,10447,1,0,10146,'2010-08-03 10:15:50',0),
 (10455,0,0,10454,1,0,10146,'2010-08-03 10:15:50',0),
 (10462,0,0,10461,1,0,10146,'2010-08-03 10:15:50',0),
 (10469,0,0,10468,1,0,10146,'2010-08-03 10:15:50',0),
 (10476,0,0,10475,1,0,10146,'2010-08-03 10:15:50',0),
 (10483,0,0,10482,1,0,10146,'2010-08-03 10:15:50',0),
 (10490,0,0,10489,1,0,10146,'2010-08-03 10:15:50',0),
 (10497,0,0,10496,1,0,10146,'2010-08-03 10:15:51',0),
 (10504,0,0,10503,1,0,10146,'2010-08-03 10:15:51',0),
 (10511,0,0,10510,1,0,10146,'2010-08-03 10:15:51',0),
 (10518,0,0,10517,1,0,10146,'2010-08-03 10:15:51',0),
 (10525,0,0,10524,1,0,10146,'2010-08-03 10:15:51',0),
 (10532,0,0,10531,1,0,10146,'2010-08-03 10:15:51',0),
 (10539,0,0,10538,1,0,10146,'2010-08-03 10:15:51',0),
 (10546,0,0,10545,1,0,10146,'2010-08-03 10:15:52',0),
 (10553,0,0,10552,1,0,10146,'2010-08-03 10:15:52',0),
 (10560,0,0,10559,1,0,10146,'2010-08-03 10:15:52',0),
 (10567,0,0,10566,1,0,10146,'2010-08-03 10:15:52',0),
 (10574,0,0,10573,1,0,10146,'2010-08-03 10:15:52',0),
 (10581,0,0,10580,1,0,10146,'2010-08-03 10:15:52',0),
 (10610,0,0,10609,1,0,0,'2010-08-03 10:18:38',0),
 (10615,0,0,10614,1,0,0,'2010-08-03 10:18:39',0),
 (10620,0,0,10619,1,0,0,'2010-08-03 10:18:39',0),
 (10625,0,0,10624,1,0,0,'2010-08-03 10:18:39',0),
 (10630,0,0,10629,1,0,0,'2010-08-03 10:18:40',0),
 (10635,0,0,10634,1,0,0,'2010-08-03 10:18:40',0),
 (10640,0,0,10639,1,0,0,'2010-08-03 10:18:40',0),
 (10645,0,0,10644,1,0,0,'2010-08-03 10:18:41',0),
 (10650,0,0,10649,1,0,0,'2010-08-03 10:18:41',0),
 (10655,0,0,10654,1,0,0,'2010-08-03 10:18:41',0),
 (10660,0,0,10659,1,0,0,'2010-08-03 10:18:41',0),
 (10665,0,0,10664,1,0,0,'2010-08-03 10:18:42',0),
 (10671,0,0,10670,1,0,0,'2010-08-03 10:18:42',0),
 (10677,0,0,10676,1,0,0,'2010-08-03 10:18:43',0),
 (10683,0,0,10682,1,0,0,'2010-08-03 10:18:43',0),
 (10689,0,0,10688,1,0,0,'2010-08-03 10:18:44',0),
 (10695,0,0,10694,1,0,0,'2010-08-03 10:18:45',0),
 (10700,0,0,10699,1,0,0,'2010-08-03 10:18:45',0),
 (10704,0,0,10703,1,0,0,'2010-08-03 10:18:45',0),
 (10709,0,0,10708,1,0,0,'2010-08-03 10:18:45',0),
 (10714,0,0,10713,1,0,0,'2010-08-03 10:18:46',0),
 (10719,0,0,10718,1,0,0,'2010-08-03 10:18:46',0),
 (10724,0,0,10723,1,0,0,'2010-08-03 10:18:46',0),
 (10729,0,0,10728,1,0,0,'2010-08-03 10:18:47',0),
 (10734,0,0,10733,1,0,0,'2010-08-03 10:18:47',0),
 (10739,0,0,10738,1,0,0,'2010-08-03 10:18:47',0),
 (10744,0,0,10743,1,0,0,'2010-08-03 10:18:47',0),
 (10749,0,0,10748,1,0,0,'2010-08-03 10:18:48',0),
 (10753,0,0,10752,1,0,0,'2010-08-03 10:18:48',0),
 (10758,0,0,10757,1,0,0,'2010-08-03 10:18:48',0),
 (10763,0,0,10762,1,0,0,'2010-08-03 10:18:49',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `mbthread` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`membershiprequest`
--

DROP TABLE IF EXISTS `%DBNAME%`.`membershiprequest`;
CREATE TABLE  `%DBNAME%`.`membershiprequest` (
  `membershipRequestId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `comments` longtext,
  `replyComments` longtext,
  `replyDate` datetime DEFAULT NULL,
  `replierUserId` bigint(20) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  PRIMARY KEY (`membershipRequestId`),
  KEY `IX_8A1CC4B` (`groupId`),
  KEY `IX_C28C72EC` (`groupId`,`statusId`),
  KEY `IX_66D70879` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`membershiprequest`
--

/*!40000 ALTER TABLE `membershiprequest` DISABLE KEYS */;
LOCK TABLES `membershiprequest` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `membershiprequest` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`organization_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`organization_`;
CREATE TABLE  `%DBNAME%`.`organization_` (
  `organizationId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `parentOrganizationId` bigint(20) DEFAULT NULL,
  `leftOrganizationId` bigint(20) DEFAULT NULL,
  `rightOrganizationId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `recursable` tinyint(4) DEFAULT NULL,
  `regionId` bigint(20) DEFAULT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  `comments` longtext,
  PRIMARY KEY (`organizationId`),
  UNIQUE KEY `IX_E301BDF5` (`companyId`,`name`),
  KEY `IX_834BCEB6` (`companyId`),
  KEY `IX_418E4522` (`companyId`,`parentOrganizationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`organization_`
--

/*!40000 ALTER TABLE `organization_` DISABLE KEYS */;
LOCK TABLES `organization_` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `organization_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`orggrouppermission`
--

DROP TABLE IF EXISTS `%DBNAME%`.`orggrouppermission`;
CREATE TABLE  `%DBNAME%`.`orggrouppermission` (
  `organizationId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`organizationId`,`groupId`,`permissionId`),
  KEY `IX_A425F71A` (`groupId`),
  KEY `IX_6C53DA4E` (`permissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`orggrouppermission`
--

/*!40000 ALTER TABLE `orggrouppermission` DISABLE KEYS */;
LOCK TABLES `orggrouppermission` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `orggrouppermission` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`orggrouprole`
--

DROP TABLE IF EXISTS `%DBNAME%`.`orggrouprole`;
CREATE TABLE  `%DBNAME%`.`orggrouprole` (
  `organizationId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`organizationId`,`groupId`,`roleId`),
  KEY `IX_4A527DD3` (`groupId`),
  KEY `IX_AB044D1C` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`orggrouprole`
--

/*!40000 ALTER TABLE `orggrouprole` DISABLE KEYS */;
LOCK TABLES `orggrouprole` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `orggrouprole` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`orglabor`
--

DROP TABLE IF EXISTS `%DBNAME%`.`orglabor`;
CREATE TABLE  `%DBNAME%`.`orglabor` (
  `orgLaborId` bigint(20) NOT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sunOpen` int(11) DEFAULT NULL,
  `sunClose` int(11) DEFAULT NULL,
  `monOpen` int(11) DEFAULT NULL,
  `monClose` int(11) DEFAULT NULL,
  `tueOpen` int(11) DEFAULT NULL,
  `tueClose` int(11) DEFAULT NULL,
  `wedOpen` int(11) DEFAULT NULL,
  `wedClose` int(11) DEFAULT NULL,
  `thuOpen` int(11) DEFAULT NULL,
  `thuClose` int(11) DEFAULT NULL,
  `friOpen` int(11) DEFAULT NULL,
  `friClose` int(11) DEFAULT NULL,
  `satOpen` int(11) DEFAULT NULL,
  `satClose` int(11) DEFAULT NULL,
  PRIMARY KEY (`orgLaborId`),
  KEY `IX_6AF0D434` (`organizationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`orglabor`
--

/*!40000 ALTER TABLE `orglabor` DISABLE KEYS */;
LOCK TABLES `orglabor` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `orglabor` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`passwordpolicy`
--

DROP TABLE IF EXISTS `%DBNAME%`.`passwordpolicy`;
CREATE TABLE  `%DBNAME%`.`passwordpolicy` (
  `passwordPolicyId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `defaultPolicy` tinyint(4) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `changeable` tinyint(4) DEFAULT NULL,
  `changeRequired` tinyint(4) DEFAULT NULL,
  `minAge` bigint(20) DEFAULT NULL,
  `checkSyntax` tinyint(4) DEFAULT NULL,
  `allowDictionaryWords` tinyint(4) DEFAULT NULL,
  `minLength` int(11) DEFAULT NULL,
  `history` tinyint(4) DEFAULT NULL,
  `historyCount` int(11) DEFAULT NULL,
  `expireable` tinyint(4) DEFAULT NULL,
  `maxAge` bigint(20) DEFAULT NULL,
  `warningTime` bigint(20) DEFAULT NULL,
  `graceLimit` int(11) DEFAULT NULL,
  `lockout` tinyint(4) DEFAULT NULL,
  `maxFailure` int(11) DEFAULT NULL,
  `lockoutDuration` bigint(20) DEFAULT NULL,
  `requireUnlock` tinyint(4) DEFAULT NULL,
  `resetFailureCount` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`passwordPolicyId`),
  UNIQUE KEY `IX_3FBFA9F4` (`companyId`,`name`),
  KEY `IX_2C1142E` (`companyId`,`defaultPolicy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`passwordpolicy`
--

/*!40000 ALTER TABLE `passwordpolicy` DISABLE KEYS */;
LOCK TABLES `passwordpolicy` WRITE;
INSERT INTO `%DBNAME%`.`passwordpolicy` VALUES  (10145,10114,10117,'','2010-08-03 09:47:11','2010-08-03 09:47:11',1,'Default Password Policy','Default Password Policy',1,0,0,0,1,6,0,6,0,8640000,86400,0,0,3,0,1,600);
UNLOCK TABLES;
/*!40000 ALTER TABLE `passwordpolicy` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`passwordpolicyrel`
--

DROP TABLE IF EXISTS `%DBNAME%`.`passwordpolicyrel`;
CREATE TABLE  `%DBNAME%`.`passwordpolicyrel` (
  `passwordPolicyRelId` bigint(20) NOT NULL,
  `passwordPolicyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`passwordPolicyRelId`),
  KEY `IX_C3A17327` (`classNameId`,`classPK`),
  KEY `IX_CD25266E` (`passwordPolicyId`),
  KEY `IX_ED7CF243` (`passwordPolicyId`,`classNameId`,`classPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`passwordpolicyrel`
--

/*!40000 ALTER TABLE `passwordpolicyrel` DISABLE KEYS */;
LOCK TABLES `passwordpolicyrel` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `passwordpolicyrel` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`passwordtracker`
--

DROP TABLE IF EXISTS `%DBNAME%`.`passwordtracker`;
CREATE TABLE  `%DBNAME%`.`passwordtracker` (
  `passwordTrackerId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `password_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`passwordTrackerId`),
  KEY `IX_326F75BD` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`passwordtracker`
--

/*!40000 ALTER TABLE `passwordtracker` DISABLE KEYS */;
LOCK TABLES `passwordtracker` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `passwordtracker` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`permission_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`permission_`;
CREATE TABLE  `%DBNAME%`.`permission_` (
  `permissionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `resourceId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`permissionId`),
  UNIQUE KEY `IX_4D19C2B8` (`actionId`,`resourceId`),
  KEY `IX_F090C113` (`resourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`permission_`
--

/*!40000 ALTER TABLE `permission_` DISABLE KEYS */;
LOCK TABLES `permission_` WRITE;
INSERT INTO `%DBNAME%`.`permission_` VALUES  (2,10114,'ADD_DISCUSSION',4),
 (3,10114,'DELETE_DISCUSSION',4),
 (4,10114,'UPDATE',4),
 (5,10114,'UPDATE_DISCUSSION',4),
 (6,10114,'VIEW',4),
 (12,10114,'DELETE',8),
 (13,10114,'IMPERSONATE',8),
 (14,10114,'PERMISSIONS',8),
 (15,10114,'UPDATE',8),
 (16,10114,'VIEW',8),
 (101,10114,'ADD_IMAGE',103),
 (102,10114,'ADD_SUBFOLDER',103),
 (103,10114,'DELETE',103),
 (104,10114,'PERMISSIONS',103),
 (105,10114,'UPDATE',103),
 (106,10114,'VIEW',103),
 (107,10114,'DELETE',106),
 (108,10114,'PERMISSIONS',106),
 (109,10114,'UPDATE',106),
 (110,10114,'VIEW',106),
 (111,10114,'ADD_ENTRY',109),
 (112,10114,'DELETE',109),
 (113,10114,'PERMISSIONS',109),
 (114,10114,'UPDATE',109),
 (115,10114,'VIEW',109),
 (116,10114,'ADD_ENTRY',110),
 (117,10114,'DELETE',110),
 (118,10114,'PERMISSIONS',110),
 (119,10114,'UPDATE',110),
 (120,10114,'VIEW',110),
 (121,10114,'ADD_ENTRY',111),
 (122,10114,'DELETE',111),
 (123,10114,'PERMISSIONS',111),
 (124,10114,'UPDATE',111),
 (125,10114,'VIEW',111),
 (126,10114,'ADD_ENTRY',112),
 (127,10114,'DELETE',112),
 (128,10114,'PERMISSIONS',112),
 (129,10114,'UPDATE',112),
 (130,10114,'VIEW',112),
 (131,10114,'ADD_ENTRY',113),
 (132,10114,'DELETE',113),
 (133,10114,'PERMISSIONS',113),
 (134,10114,'UPDATE',113),
 (135,10114,'VIEW',113),
 (136,10114,'ADD_ENTRY',114),
 (137,10114,'DELETE',114),
 (138,10114,'PERMISSIONS',114),
 (139,10114,'UPDATE',114),
 (140,10114,'VIEW',114),
 (296,10114,'VIEW',146),
 (297,10114,'ADD_DISCUSSION',146),
 (298,10114,'VIEW',147),
 (299,10114,'ADD_DISCUSSION',147),
 (300,10114,'ASSIGN_MEMBERS',149),
 (301,10114,'DEFINE_PERMISSIONS',149),
 (302,10114,'DELETE',149),
 (303,10114,'MANAGE_ANNOUNCEMENTS',149),
 (304,10114,'PERMISSIONS',149),
 (305,10114,'UPDATE',149),
 (306,10114,'VIEW',149),
 (307,10114,'ASSIGN_MEMBERS',150),
 (308,10114,'DEFINE_PERMISSIONS',150),
 (309,10114,'DELETE',150),
 (310,10114,'MANAGE_ANNOUNCEMENTS',150),
 (311,10114,'PERMISSIONS',150),
 (312,10114,'UPDATE',150),
 (313,10114,'VIEW',150),
 (314,10114,'ACCESS_IN_CONTROL_PANEL',151),
 (315,10114,'ACCESS_IN_CONTROL_PANEL',152),
 (316,10114,'ACCESS_IN_CONTROL_PANEL',153),
 (317,10114,'ACCESS_IN_CONTROL_PANEL',154),
 (318,10114,'ADD_ARTICLE',155),
 (319,10114,'APPROVE_ARTICLE',155),
 (320,10114,'DELETE',146),
 (321,10114,'EXPIRE',146),
 (322,10114,'UPDATE',146),
 (323,10114,'DELETE_DISCUSSION',146),
 (324,10114,'UPDATE_DISCUSSION',146),
 (325,10114,'ADD_FOLDER',156),
 (326,10114,'UPDATE',101),
 (327,10114,'DELETE',101),
 (328,10114,'ADD_SUBFOLDER',101),
 (329,10114,'ADD_IMAGE',101),
 (330,10114,'VIEW',101),
 (331,10114,'UPDATE',157),
 (332,10114,'DELETE',157),
 (333,10114,'VIEW',157),
 (334,10114,'ADD_FOLDER',158),
 (335,10114,'UPDATE',159),
 (336,10114,'DELETE',159),
 (337,10114,'ADD_SUBFOLDER',159),
 (338,10114,'ADD_DOCUMENT',159),
 (339,10114,'VIEW',159),
 (340,10114,'UPDATE',160),
 (341,10114,'DELETE',160),
 (342,10114,'VIEW',160),
 (343,10114,'ADD_ENTRY',161),
 (344,10114,'DELETE',147),
 (345,10114,'DELETE_DISCUSSION',147),
 (346,10114,'UPDATE',147),
 (347,10114,'UPDATE_DISCUSSION',147),
 (348,10114,'VIEW',107),
 (349,10114,'VIEW',104),
 (350,10114,'DELETE',164),
 (351,10114,'PERMISSIONS',164),
 (352,10114,'UPDATE',164),
 (353,10114,'VIEW',164),
 (354,10114,'DELETE',165),
 (355,10114,'PERMISSIONS',165),
 (356,10114,'UPDATE',165),
 (357,10114,'VIEW',165),
 (358,10114,'DELETE',166),
 (359,10114,'PERMISSIONS',166),
 (360,10114,'UPDATE',166),
 (361,10114,'VIEW',166),
 (362,10114,'DELETE',167),
 (363,10114,'PERMISSIONS',167),
 (364,10114,'UPDATE',167),
 (365,10114,'VIEW',167),
 (366,10114,'DELETE',168),
 (367,10114,'PERMISSIONS',168),
 (368,10114,'UPDATE',168),
 (369,10114,'VIEW',168),
 (370,10114,'DELETE',171),
 (371,10114,'PERMISSIONS',171),
 (372,10114,'UPDATE',171),
 (373,10114,'VIEW',171),
 (374,10114,'DELETE',172),
 (375,10114,'PERMISSIONS',172),
 (376,10114,'UPDATE',172),
 (377,10114,'VIEW',172),
 (378,10114,'DELETE',173),
 (379,10114,'PERMISSIONS',173),
 (380,10114,'UPDATE',173),
 (381,10114,'VIEW',173),
 (382,10114,'DELETE',174),
 (383,10114,'PERMISSIONS',174),
 (384,10114,'UPDATE',174),
 (385,10114,'VIEW',174),
 (386,10114,'DELETE',175),
 (387,10114,'PERMISSIONS',175),
 (388,10114,'UPDATE',175),
 (389,10114,'VIEW',175),
 (390,10114,'ADD_DISCUSSION',177),
 (391,10114,'DELETE',177),
 (392,10114,'DELETE_DISCUSSION',177),
 (393,10114,'EXPIRE',177),
 (394,10114,'PERMISSIONS',177),
 (395,10114,'UPDATE',177),
 (396,10114,'UPDATE_DISCUSSION',177),
 (397,10114,'VIEW',177),
 (398,10114,'ADD_DISCUSSION',178),
 (399,10114,'DELETE',178),
 (400,10114,'DELETE_DISCUSSION',178),
 (401,10114,'EXPIRE',178),
 (402,10114,'PERMISSIONS',178),
 (403,10114,'UPDATE',178),
 (404,10114,'UPDATE_DISCUSSION',178),
 (405,10114,'VIEW',178),
 (406,10114,'ADD_DISCUSSION',179),
 (407,10114,'DELETE',179),
 (408,10114,'DELETE_DISCUSSION',179),
 (409,10114,'EXPIRE',179),
 (410,10114,'PERMISSIONS',179),
 (411,10114,'UPDATE',179),
 (412,10114,'UPDATE_DISCUSSION',179),
 (413,10114,'VIEW',179),
 (414,10114,'ADD_DISCUSSION',180),
 (415,10114,'DELETE',180),
 (416,10114,'DELETE_DISCUSSION',180),
 (417,10114,'EXPIRE',180),
 (418,10114,'PERMISSIONS',180),
 (419,10114,'UPDATE',180),
 (420,10114,'UPDATE_DISCUSSION',180),
 (421,10114,'VIEW',180),
 (422,10114,'ADD_DISCUSSION',181),
 (423,10114,'DELETE',181),
 (424,10114,'DELETE_DISCUSSION',181),
 (425,10114,'EXPIRE',181),
 (426,10114,'PERMISSIONS',181),
 (427,10114,'UPDATE',181),
 (428,10114,'UPDATE_DISCUSSION',181),
 (429,10114,'VIEW',181),
 (430,10114,'ADD_DISCUSSION',182),
 (431,10114,'DELETE',182),
 (432,10114,'DELETE_DISCUSSION',182),
 (433,10114,'EXPIRE',182),
 (434,10114,'PERMISSIONS',182),
 (435,10114,'UPDATE',182),
 (436,10114,'UPDATE_DISCUSSION',182),
 (437,10114,'VIEW',182),
 (438,10114,'ADD_DISCUSSION',183),
 (439,10114,'DELETE',183),
 (440,10114,'DELETE_DISCUSSION',183),
 (441,10114,'EXPIRE',183),
 (442,10114,'PERMISSIONS',183),
 (443,10114,'UPDATE',183),
 (444,10114,'UPDATE_DISCUSSION',183),
 (445,10114,'VIEW',183),
 (446,10114,'ADD_DISCUSSION',184),
 (447,10114,'DELETE',184),
 (448,10114,'DELETE_DISCUSSION',184),
 (449,10114,'EXPIRE',184),
 (450,10114,'PERMISSIONS',184),
 (451,10114,'UPDATE',184),
 (452,10114,'UPDATE_DISCUSSION',184),
 (453,10114,'VIEW',184),
 (454,10114,'ADD_DISCUSSION',185),
 (455,10114,'DELETE',185),
 (456,10114,'DELETE_DISCUSSION',185),
 (457,10114,'EXPIRE',185),
 (458,10114,'PERMISSIONS',185),
 (459,10114,'UPDATE',185),
 (460,10114,'UPDATE_DISCUSSION',185),
 (461,10114,'VIEW',185),
 (462,10114,'ADD_DISCUSSION',186),
 (463,10114,'DELETE',186),
 (464,10114,'DELETE_DISCUSSION',186),
 (465,10114,'EXPIRE',186),
 (466,10114,'PERMISSIONS',186),
 (467,10114,'UPDATE',186),
 (468,10114,'UPDATE_DISCUSSION',186),
 (469,10114,'VIEW',186),
 (470,10114,'ADD_DISCUSSION',187),
 (471,10114,'DELETE',187),
 (472,10114,'DELETE_DISCUSSION',187),
 (473,10114,'EXPIRE',187),
 (474,10114,'PERMISSIONS',187),
 (475,10114,'UPDATE',187),
 (476,10114,'UPDATE_DISCUSSION',187),
 (477,10114,'VIEW',187),
 (478,10114,'ADD_DISCUSSION',188),
 (479,10114,'DELETE',188),
 (480,10114,'DELETE_DISCUSSION',188),
 (481,10114,'EXPIRE',188),
 (482,10114,'PERMISSIONS',188),
 (483,10114,'UPDATE',188),
 (484,10114,'UPDATE_DISCUSSION',188),
 (485,10114,'VIEW',188),
 (486,10114,'ADD_DISCUSSION',189),
 (487,10114,'DELETE',189),
 (488,10114,'DELETE_DISCUSSION',189),
 (489,10114,'EXPIRE',189),
 (490,10114,'PERMISSIONS',189),
 (491,10114,'UPDATE',189),
 (492,10114,'UPDATE_DISCUSSION',189),
 (493,10114,'VIEW',189),
 (494,10114,'ADD_DISCUSSION',190),
 (495,10114,'DELETE',190),
 (496,10114,'DELETE_DISCUSSION',190),
 (497,10114,'EXPIRE',190),
 (498,10114,'PERMISSIONS',190),
 (499,10114,'UPDATE',190),
 (500,10114,'UPDATE_DISCUSSION',190),
 (501,10114,'VIEW',190),
 (502,10114,'ADD_DISCUSSION',191),
 (503,10114,'DELETE',191),
 (504,10114,'DELETE_DISCUSSION',191),
 (505,10114,'EXPIRE',191),
 (506,10114,'PERMISSIONS',191),
 (507,10114,'UPDATE',191),
 (508,10114,'UPDATE_DISCUSSION',191),
 (509,10114,'VIEW',191),
 (510,10114,'ADD_DISCUSSION',192),
 (511,10114,'DELETE',192),
 (512,10114,'DELETE_DISCUSSION',192),
 (513,10114,'EXPIRE',192),
 (514,10114,'PERMISSIONS',192),
 (515,10114,'UPDATE',192),
 (516,10114,'UPDATE_DISCUSSION',192),
 (517,10114,'VIEW',192),
 (518,10114,'ADD_DISCUSSION',193),
 (519,10114,'DELETE',193),
 (520,10114,'DELETE_DISCUSSION',193),
 (521,10114,'EXPIRE',193),
 (522,10114,'PERMISSIONS',193),
 (523,10114,'UPDATE',193),
 (524,10114,'UPDATE_DISCUSSION',193),
 (525,10114,'VIEW',193),
 (526,10114,'ADD_DISCUSSION',194),
 (527,10114,'DELETE',194),
 (528,10114,'DELETE_DISCUSSION',194),
 (529,10114,'EXPIRE',194),
 (530,10114,'PERMISSIONS',194),
 (531,10114,'UPDATE',194),
 (532,10114,'UPDATE_DISCUSSION',194),
 (533,10114,'VIEW',194),
 (534,10114,'ADD_DISCUSSION',195),
 (535,10114,'DELETE',195),
 (536,10114,'DELETE_DISCUSSION',195),
 (537,10114,'EXPIRE',195),
 (538,10114,'PERMISSIONS',195),
 (539,10114,'UPDATE',195),
 (540,10114,'UPDATE_DISCUSSION',195),
 (541,10114,'VIEW',195),
 (542,10114,'ADD_DISCUSSION',196),
 (543,10114,'DELETE',196),
 (544,10114,'DELETE_DISCUSSION',196),
 (545,10114,'EXPIRE',196),
 (546,10114,'PERMISSIONS',196),
 (547,10114,'UPDATE',196),
 (548,10114,'UPDATE_DISCUSSION',196),
 (549,10114,'VIEW',196),
 (550,10114,'ADD_DISCUSSION',197),
 (551,10114,'DELETE',197),
 (552,10114,'DELETE_DISCUSSION',197),
 (553,10114,'EXPIRE',197),
 (554,10114,'PERMISSIONS',197),
 (555,10114,'UPDATE',197),
 (556,10114,'UPDATE_DISCUSSION',197),
 (557,10114,'VIEW',197),
 (558,10114,'ADD_DISCUSSION',198),
 (559,10114,'DELETE',198),
 (560,10114,'DELETE_DISCUSSION',198),
 (561,10114,'EXPIRE',198),
 (562,10114,'PERMISSIONS',198),
 (563,10114,'UPDATE',198),
 (564,10114,'UPDATE_DISCUSSION',198),
 (565,10114,'VIEW',198),
 (566,10114,'ADD_DISCUSSION',199),
 (567,10114,'DELETE',199),
 (568,10114,'DELETE_DISCUSSION',199),
 (569,10114,'EXPIRE',199),
 (570,10114,'PERMISSIONS',199),
 (571,10114,'UPDATE',199),
 (572,10114,'UPDATE_DISCUSSION',199),
 (573,10114,'VIEW',199),
 (574,10114,'ADD_DISCUSSION',200),
 (575,10114,'DELETE',200),
 (576,10114,'DELETE_DISCUSSION',200),
 (577,10114,'EXPIRE',200),
 (578,10114,'PERMISSIONS',200),
 (579,10114,'UPDATE',200),
 (580,10114,'UPDATE_DISCUSSION',200),
 (581,10114,'VIEW',200),
 (582,10114,'ADD_DISCUSSION',201),
 (583,10114,'DELETE',201),
 (584,10114,'DELETE_DISCUSSION',201),
 (585,10114,'EXPIRE',201),
 (586,10114,'PERMISSIONS',201),
 (587,10114,'UPDATE',201),
 (588,10114,'UPDATE_DISCUSSION',201),
 (589,10114,'VIEW',201),
 (590,10114,'ADD_DISCUSSION',202),
 (591,10114,'DELETE',202),
 (592,10114,'DELETE_DISCUSSION',202),
 (593,10114,'EXPIRE',202),
 (594,10114,'PERMISSIONS',202),
 (595,10114,'UPDATE',202),
 (596,10114,'UPDATE_DISCUSSION',202),
 (597,10114,'VIEW',202),
 (598,10114,'ADD_DISCUSSION',203),
 (599,10114,'DELETE',203),
 (600,10114,'DELETE_DISCUSSION',203),
 (601,10114,'EXPIRE',203),
 (602,10114,'PERMISSIONS',203),
 (603,10114,'UPDATE',203),
 (604,10114,'UPDATE_DISCUSSION',203),
 (605,10114,'VIEW',203),
 (606,10114,'ADD_DISCUSSION',204),
 (607,10114,'DELETE',204),
 (608,10114,'DELETE_DISCUSSION',204),
 (609,10114,'EXPIRE',204),
 (610,10114,'PERMISSIONS',204),
 (611,10114,'UPDATE',204),
 (612,10114,'UPDATE_DISCUSSION',204),
 (613,10114,'VIEW',204),
 (701,10114,'ADD_DISCUSSION',301),
 (702,10114,'DELETE_DISCUSSION',301),
 (703,10114,'UPDATE',301),
 (704,10114,'UPDATE_DISCUSSION',301),
 (705,10114,'VIEW',301),
 (706,10114,'ADD_DISCUSSION',302),
 (707,10114,'DELETE_DISCUSSION',302),
 (708,10114,'UPDATE',302),
 (709,10114,'UPDATE_DISCUSSION',302),
 (710,10114,'VIEW',302),
 (711,10114,'ADD_DISCUSSION',303),
 (712,10114,'DELETE_DISCUSSION',303),
 (713,10114,'UPDATE',303),
 (714,10114,'UPDATE_DISCUSSION',303),
 (715,10114,'VIEW',303),
 (716,10114,'ADD_DISCUSSION',304),
 (717,10114,'DELETE_DISCUSSION',304),
 (718,10114,'UPDATE',304),
 (719,10114,'UPDATE_DISCUSSION',304),
 (720,10114,'VIEW',304),
 (721,10114,'ADD_DISCUSSION',305),
 (722,10114,'DELETE_DISCUSSION',305),
 (723,10114,'UPDATE',305),
 (724,10114,'UPDATE_DISCUSSION',305),
 (725,10114,'VIEW',305),
 (726,10114,'ADD_DISCUSSION',306),
 (727,10114,'DELETE_DISCUSSION',306),
 (728,10114,'UPDATE',306),
 (729,10114,'UPDATE_DISCUSSION',306),
 (730,10114,'VIEW',306),
 (731,10114,'ADD_DISCUSSION',307),
 (732,10114,'DELETE_DISCUSSION',307),
 (733,10114,'UPDATE',307),
 (734,10114,'UPDATE_DISCUSSION',307),
 (735,10114,'VIEW',307),
 (736,10114,'ADD_DISCUSSION',308),
 (737,10114,'DELETE_DISCUSSION',308),
 (738,10114,'UPDATE',308),
 (739,10114,'UPDATE_DISCUSSION',308),
 (740,10114,'VIEW',308),
 (741,10114,'ADD_DISCUSSION',309),
 (742,10114,'DELETE_DISCUSSION',309),
 (743,10114,'UPDATE',309),
 (744,10114,'UPDATE_DISCUSSION',309),
 (745,10114,'VIEW',309),
 (746,10114,'ADD_DISCUSSION',310),
 (747,10114,'DELETE_DISCUSSION',310),
 (748,10114,'UPDATE',310),
 (749,10114,'UPDATE_DISCUSSION',310),
 (750,10114,'VIEW',310),
 (751,10114,'ADD_DISCUSSION',311),
 (752,10114,'DELETE_DISCUSSION',311),
 (753,10114,'UPDATE',311),
 (754,10114,'UPDATE_DISCUSSION',311),
 (755,10114,'VIEW',311),
 (756,10114,'ADD_DISCUSSION',312),
 (757,10114,'DELETE_DISCUSSION',312),
 (758,10114,'UPDATE',312),
 (759,10114,'UPDATE_DISCUSSION',312),
 (760,10114,'VIEW',312),
 (761,10114,'ADD_DISCUSSION',313),
 (762,10114,'DELETE_DISCUSSION',313),
 (763,10114,'UPDATE',313),
 (764,10114,'UPDATE_DISCUSSION',313),
 (765,10114,'VIEW',313),
 (766,10114,'ADD_DISCUSSION',314),
 (767,10114,'DELETE_DISCUSSION',314),
 (768,10114,'UPDATE',314),
 (769,10114,'UPDATE_DISCUSSION',314),
 (770,10114,'VIEW',314),
 (771,10114,'ADD_DISCUSSION',315),
 (772,10114,'DELETE_DISCUSSION',315),
 (773,10114,'UPDATE',315),
 (774,10114,'UPDATE_DISCUSSION',315),
 (775,10114,'VIEW',315),
 (776,10114,'ADD_DISCUSSION',316),
 (777,10114,'DELETE_DISCUSSION',316),
 (778,10114,'UPDATE',316),
 (779,10114,'UPDATE_DISCUSSION',316),
 (780,10114,'VIEW',316),
 (781,10114,'ADD_DISCUSSION',317),
 (782,10114,'DELETE_DISCUSSION',317),
 (783,10114,'UPDATE',317),
 (784,10114,'UPDATE_DISCUSSION',317),
 (785,10114,'VIEW',317),
 (786,10114,'ADD_DISCUSSION',318),
 (787,10114,'DELETE_DISCUSSION',318),
 (788,10114,'UPDATE',318),
 (789,10114,'UPDATE_DISCUSSION',318),
 (790,10114,'VIEW',318),
 (791,10114,'ADD_DISCUSSION',319),
 (792,10114,'DELETE_DISCUSSION',319),
 (793,10114,'UPDATE',319),
 (794,10114,'UPDATE_DISCUSSION',319),
 (795,10114,'VIEW',319),
 (796,10114,'ADD_DISCUSSION',320),
 (797,10114,'DELETE_DISCUSSION',320),
 (798,10114,'UPDATE',320),
 (799,10114,'UPDATE_DISCUSSION',320),
 (800,10114,'VIEW',320),
 (801,10114,'ADD_DISCUSSION',321),
 (802,10114,'DELETE_DISCUSSION',321),
 (803,10114,'UPDATE',321),
 (804,10114,'UPDATE_DISCUSSION',321),
 (805,10114,'VIEW',321),
 (806,10114,'ADD_DISCUSSION',322),
 (807,10114,'DELETE_DISCUSSION',322),
 (808,10114,'UPDATE',322),
 (809,10114,'UPDATE_DISCUSSION',322),
 (810,10114,'VIEW',322),
 (811,10114,'ADD_DISCUSSION',323),
 (812,10114,'DELETE_DISCUSSION',323),
 (813,10114,'UPDATE',323),
 (814,10114,'UPDATE_DISCUSSION',323),
 (815,10114,'VIEW',323),
 (816,10114,'ADD_DISCUSSION',324),
 (817,10114,'DELETE_DISCUSSION',324),
 (818,10114,'UPDATE',324),
 (819,10114,'UPDATE_DISCUSSION',324),
 (820,10114,'VIEW',324),
 (821,10114,'ADD_DISCUSSION',325),
 (822,10114,'DELETE_DISCUSSION',325),
 (823,10114,'UPDATE',325),
 (824,10114,'UPDATE_DISCUSSION',325),
 (825,10114,'VIEW',325),
 (826,10114,'ADD_DISCUSSION',326),
 (827,10114,'DELETE_DISCUSSION',326),
 (828,10114,'UPDATE',326),
 (829,10114,'UPDATE_DISCUSSION',326),
 (830,10114,'VIEW',326),
 (831,10114,'ADD_DISCUSSION',327),
 (832,10114,'DELETE_DISCUSSION',327),
 (833,10114,'UPDATE',327),
 (834,10114,'UPDATE_DISCUSSION',327),
 (835,10114,'VIEW',327),
 (836,10114,'ADD_DISCUSSION',328),
 (837,10114,'DELETE_DISCUSSION',328),
 (838,10114,'UPDATE',328),
 (839,10114,'UPDATE_DISCUSSION',328),
 (840,10114,'VIEW',328),
 (841,10114,'ADD_DISCUSSION',329),
 (842,10114,'DELETE_DISCUSSION',329),
 (843,10114,'UPDATE',329),
 (844,10114,'UPDATE_DISCUSSION',329),
 (845,10114,'VIEW',329),
 (846,10114,'ADD_DISCUSSION',330),
 (847,10114,'DELETE_DISCUSSION',330),
 (848,10114,'UPDATE',330),
 (849,10114,'UPDATE_DISCUSSION',330),
 (850,10114,'VIEW',330),
 (851,10114,'ADD_DISCUSSION',331),
 (852,10114,'DELETE_DISCUSSION',331),
 (853,10114,'UPDATE',331),
 (854,10114,'UPDATE_DISCUSSION',331),
 (855,10114,'VIEW',331),
 (856,10114,'VIEW',332),
 (857,10114,'CONFIGURATION',332),
 (858,10114,'CONFIGURATION',333),
 (859,10114,'VIEW',333);
UNLOCK TABLES;
/*!40000 ALTER TABLE `permission_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`phone`
--

DROP TABLE IF EXISTS `%DBNAME%`.`phone`;
CREATE TABLE  `%DBNAME%`.`phone` (
  `phoneId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `extension` varchar(75) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`phoneId`),
  KEY `IX_9F704A14` (`companyId`),
  KEY `IX_A2E4AFBA` (`companyId`,`classNameId`),
  KEY `IX_9A53569` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_812CE07A` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_F202B9CE` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`phone`
--

/*!40000 ALTER TABLE `phone` DISABLE KEYS */;
LOCK TABLES `phone` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `phone` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`pluginsetting`
--

DROP TABLE IF EXISTS `%DBNAME%`.`pluginsetting`;
CREATE TABLE  `%DBNAME%`.`pluginsetting` (
  `pluginSettingId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `pluginId` varchar(75) DEFAULT NULL,
  `pluginType` varchar(75) DEFAULT NULL,
  `roles` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pluginSettingId`),
  UNIQUE KEY `IX_7171B2E8` (`companyId`,`pluginId`,`pluginType`),
  KEY `IX_B9746445` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`pluginsetting`
--

/*!40000 ALTER TABLE `pluginsetting` DISABLE KEYS */;
LOCK TABLES `pluginsetting` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `pluginsetting` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`pollschoice`
--

DROP TABLE IF EXISTS `%DBNAME%`.`pollschoice`;
CREATE TABLE  `%DBNAME%`.`pollschoice` (
  `uuid_` varchar(75) DEFAULT NULL,
  `choiceId` bigint(20) NOT NULL,
  `questionId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`choiceId`),
  UNIQUE KEY `IX_D76DD2CF` (`questionId`,`name`),
  KEY `IX_EC370F10` (`questionId`),
  KEY `IX_6660B399` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`pollschoice`
--

/*!40000 ALTER TABLE `pollschoice` DISABLE KEYS */;
LOCK TABLES `pollschoice` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `pollschoice` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`pollsquestion`
--

DROP TABLE IF EXISTS `%DBNAME%`.`pollsquestion`;
CREATE TABLE  `%DBNAME%`.`pollsquestion` (
  `uuid_` varchar(75) DEFAULT NULL,
  `questionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `description` longtext,
  `expirationDate` datetime DEFAULT NULL,
  `lastVoteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`questionId`),
  UNIQUE KEY `IX_F3C9F36` (`uuid_`,`groupId`),
  KEY `IX_9FF342EA` (`groupId`),
  KEY `IX_51F087F4` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`pollsquestion`
--

/*!40000 ALTER TABLE `pollsquestion` DISABLE KEYS */;
LOCK TABLES `pollsquestion` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `pollsquestion` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`pollsvote`
--

DROP TABLE IF EXISTS `%DBNAME%`.`pollsvote`;
CREATE TABLE  `%DBNAME%`.`pollsvote` (
  `voteId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `questionId` bigint(20) DEFAULT NULL,
  `choiceId` bigint(20) DEFAULT NULL,
  `voteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`voteId`),
  UNIQUE KEY `IX_1BBFD4D3` (`questionId`,`userId`),
  KEY `IX_D5DF7B54` (`choiceId`),
  KEY `IX_12112599` (`questionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`pollsvote`
--

/*!40000 ALTER TABLE `pollsvote` DISABLE KEYS */;
LOCK TABLES `pollsvote` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `pollsvote` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`portlet`
--

DROP TABLE IF EXISTS `%DBNAME%`.`portlet`;
CREATE TABLE  `%DBNAME%`.`portlet` (
  `id_` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `roles` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_12B5E51D` (`companyId`,`portletId`),
  KEY `IX_80CC9508` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`portlet`
--

/*!40000 ALTER TABLE `portlet` DISABLE KEYS */;
LOCK TABLES `portlet` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `portlet` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`portletitem`
--

DROP TABLE IF EXISTS `%DBNAME%`.`portletitem`;
CREATE TABLE  `%DBNAME%`.`portletitem` (
  `portletItemId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `portletId` varchar(75) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`portletItemId`),
  KEY `IX_96BDD537` (`groupId`,`classNameId`),
  KEY `IX_D699243F` (`groupId`,`name`,`portletId`,`classNameId`),
  KEY `IX_2C61314E` (`groupId`,`portletId`),
  KEY `IX_E922D6C0` (`groupId`,`portletId`,`classNameId`),
  KEY `IX_8E71167F` (`groupId`,`portletId`,`classNameId`,`name`),
  KEY `IX_33B8CE8D` (`groupId`,`portletId`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`portletitem`
--

/*!40000 ALTER TABLE `portletitem` DISABLE KEYS */;
LOCK TABLES `portletitem` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `portletitem` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`portletpreferences`
--

DROP TABLE IF EXISTS `%DBNAME%`.`portletpreferences`;
CREATE TABLE  `%DBNAME%`.`portletpreferences` (
  `portletPreferencesId` bigint(20) NOT NULL,
  `ownerId` bigint(20) DEFAULT NULL,
  `ownerType` int(11) DEFAULT NULL,
  `plid` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `preferences` longtext,
  PRIMARY KEY (`portletPreferencesId`),
  UNIQUE KEY `IX_C7057FF7` (`ownerId`,`ownerType`,`plid`,`portletId`),
  KEY `IX_E4F13E6E` (`ownerId`,`ownerType`,`plid`),
  KEY `IX_F15C1C4F` (`plid`),
  KEY `IX_D340DB76` (`plid`,`portletId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`portletpreferences`
--

/*!40000 ALTER TABLE `portletpreferences` DISABLE KEYS */;
LOCK TABLES `portletpreferences` WRITE;
INSERT INTO `%DBNAME%`.`portletpreferences` VALUES  (10113,0,1,0,'LIFERAY_PORTAL','<portlet-preferences />'),
 (10119,10114,1,0,'LIFERAY_PORTAL','<portlet-preferences />'),
 (10155,10117,4,0,'LIFERAY_PORTAL','<portlet-preferences />'),
 (10612,0,3,10608,'56_INSTANCE_aIl8','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>HOME</value></preference></portlet-preferences>'),
 (10617,0,3,10613,'56_INSTANCE_0zXJ','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>ORGANISATOREN</value></preference></portlet-preferences>'),
 (10622,0,3,10618,'56_INSTANCE_bG17','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>ORG_LIONS_BREUGHEL</value></preference></portlet-preferences>'),
 (10627,0,3,10623,'56_INSTANCE_0bJR','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>ORG_HART_VOOR_LIMBURG</value></preference></portlet-preferences>'),
 (10632,0,3,10628,'56_INSTANCE_2eQQ','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO</value></preference></portlet-preferences>'),
 (10637,0,3,10633,'56_INSTANCE_g8H2','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO_EVENEMENT</value></preference></portlet-preferences>'),
 (10642,0,3,10638,'56_INSTANCE_iY7a','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO_REGLEMENT</value></preference></portlet-preferences>'),
 (10647,0,3,10643,'56_INSTANCE_2w1I','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO_RACEVERLOOP</value></preference></portlet-preferences>'),
 (10652,0,3,10648,'56_INSTANCE_qG8b','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO_HOOFDPRIJS</value></preference></portlet-preferences>'),
 (10657,0,3,10653,'56_INSTANCE_uKR0','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>INFO_OPBRENGST</value></preference></portlet-preferences>'),
 (10662,0,3,10658,'56_INSTANCE_D1gQ','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>LOGOS</value></preference></portlet-preferences>'),
 (10667,0,3,10663,'56_INSTANCE_Eg7m','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>DUCKTYPE_MAIN_SPONSORS</value></preference></portlet-preferences>'),
 (10668,0,3,10663,'101_INSTANCE_JlA0','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>main-sponsor</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10673,0,3,10669,'56_INSTANCE_DF3g','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>DUCKTYPE_100_DUCKS</value></preference></portlet-preferences>'),
 (10674,0,3,10669,'101_INSTANCE_f3Z6','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>sponsor-4-stars</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10679,0,3,10675,'56_INSTANCE_f1hA','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>DUCKTYPE_50_DUCKS</value></preference></portlet-preferences>'),
 (10680,0,3,10675,'101_INSTANCE_HA7g','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>sponsor-3-stars</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10685,0,3,10681,'56_INSTANCE_85Wa','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>DUCKTYPE_20_DUCKS</value></preference></portlet-preferences>'),
 (10686,0,3,10681,'101_INSTANCE_si8G','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>sponsor-2-stars</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10691,0,3,10687,'56_INSTANCE_G4z8','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>DUCKTYPE_10_DUCKS</value></preference></portlet-preferences>'),
 (10692,0,3,10687,'101_INSTANCE_6NBd','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>sponsor-1-star</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10697,0,3,10693,'101_INSTANCE_V6yb','<portlet-preferences><preference><name>order-by-type-1</name><value>ASC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>100</value></preference><preference><name>order-by-column-1</name><value>title</value></preference><preference><name>entries</name><value>merchant</value></preference><preference><name>class-name-id</name><value>10067</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>false</value></preference></portlet-preferences>'),
 (10706,0,3,10702,'101_INSTANCE_FOr4','<portlet-preferences><preference><name>order-by-type-1</name><value>DESC</value></preference><preference><name>show-context-link</name><value>false</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>pagination-type</name><value>simple</value></preference><preference><name>delta</name><value>5</value></preference><preference><name>order-by-column-1</name><value>publishDate</value></preference><preference><name>entries</name><value></value></preference><preference><name>class-name-id</name><value>10051</value></preference><preference><name>show-asset-title</name><value>false</value></preference><preference><name>display-style</name><value>full-content</value></preference><preference><name>merge-url-tags</name><value>true</value></preference></portlet-preferences>'),
 (10711,0,3,10707,'56_INSTANCE_5Wiu','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>FOTO_ALBUM</value></preference></portlet-preferences>'),
 (10716,0,3,10712,'56_INSTANCE_DR8q','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>PERSCONFERENTIES</value></preference></portlet-preferences>'),
 (10721,0,3,10717,'56_INSTANCE_8nJn','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>true</value></preference><preference><name>article-id</name><value>REACTIES</value></preference></portlet-preferences>'),
 (10726,0,3,10722,'56_INSTANCE_qQ3C','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>EVENT</value></preference></portlet-preferences>'),
 (10731,0,3,10727,'56_INSTANCE_a7M9','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>EVENT_PROGRAMMA</value></preference></portlet-preferences>'),
 (10736,0,3,10732,'56_INSTANCE_PyB4','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>EVENT_ACTS</value></preference></portlet-preferences>'),
 (10741,0,3,10737,'56_INSTANCE_Gb2i','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>EVENT_BEVERAGE</value></preference></portlet-preferences>'),
 (10746,0,3,10742,'56_INSTANCE_6PcY','<portlet-preferences><preference><name>group-id</name><value>10138</value></preference><preference><name>enable-comments</name><value>false</value></preference><preference><name>article-id</name><value>EVENT_BIJSTAND</value></preference></portlet-preferences>'),
 (10755,0,3,10751,'RegistrationPortlet_WAR_registrationportlet','<portlet-preferences/>'),
 (10760,0,3,10756,'QueryRegistratedDucksPortlet_WAR_registrationportlet','<portlet-preferences/>'),
 (10765,0,3,10761,'58','<portlet-preferences/>'),
 (10766,0,3,10608,'103','<portlet-preferences />');
UNLOCK TABLES;
/*!40000 ALTER TABLE `portletpreferences` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_blob_triggers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_blob_triggers`;
CREATE TABLE  `%DBNAME%`.`quartz_blob_triggers` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_blob_triggers`
--

/*!40000 ALTER TABLE `quartz_blob_triggers` DISABLE KEYS */;
LOCK TABLES `quartz_blob_triggers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_blob_triggers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_calendars`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_calendars`;
CREATE TABLE  `%DBNAME%`.`quartz_calendars` (
  `CALENDAR_NAME` varchar(80) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_calendars`
--

/*!40000 ALTER TABLE `quartz_calendars` DISABLE KEYS */;
LOCK TABLES `quartz_calendars` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_calendars` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_cron_triggers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_cron_triggers`;
CREATE TABLE  `%DBNAME%`.`quartz_cron_triggers` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `CRON_EXPRESSION` varchar(80) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_cron_triggers`
--

/*!40000 ALTER TABLE `quartz_cron_triggers` DISABLE KEYS */;
LOCK TABLES `quartz_cron_triggers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_cron_triggers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_fired_triggers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_fired_triggers`;
CREATE TABLE  `%DBNAME%`.`quartz_fired_triggers` (
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `INSTANCE_NAME` varchar(80) NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(80) DEFAULT NULL,
  `JOB_GROUP` varchar(80) DEFAULT NULL,
  `IS_STATEFUL` tinyint(4) DEFAULT NULL,
  `REQUESTS_RECOVERY` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`),
  KEY `IX_804154AF` (`INSTANCE_NAME`),
  KEY `IX_BAB9A1F7` (`JOB_GROUP`),
  KEY `IX_ADEE6A17` (`JOB_NAME`),
  KEY `IX_64B194F2` (`TRIGGER_GROUP`),
  KEY `IX_5FEABBC` (`TRIGGER_NAME`),
  KEY `IX_20D8706C` (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_fired_triggers`
--

/*!40000 ALTER TABLE `quartz_fired_triggers` DISABLE KEYS */;
LOCK TABLES `quartz_fired_triggers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_fired_triggers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_job_details`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_job_details`;
CREATE TABLE  `%DBNAME%`.`quartz_job_details` (
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(128) NOT NULL,
  `IS_DURABLE` tinyint(4) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `IS_STATEFUL` tinyint(4) NOT NULL,
  `REQUESTS_RECOVERY` tinyint(4) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_job_details`
--

/*!40000 ALTER TABLE `quartz_job_details` DISABLE KEYS */;
LOCK TABLES `quartz_job_details` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_job_details` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_job_listeners`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_job_listeners`;
CREATE TABLE  `%DBNAME%`.`quartz_job_listeners` (
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `JOB_LISTENER` varchar(80) NOT NULL,
  PRIMARY KEY (`JOB_NAME`,`JOB_GROUP`,`JOB_LISTENER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_job_listeners`
--

/*!40000 ALTER TABLE `quartz_job_listeners` DISABLE KEYS */;
LOCK TABLES `quartz_job_listeners` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_job_listeners` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_locks`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_locks`;
CREATE TABLE  `%DBNAME%`.`quartz_locks` (
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_locks`
--

/*!40000 ALTER TABLE `quartz_locks` DISABLE KEYS */;
LOCK TABLES `quartz_locks` WRITE;
INSERT INTO `%DBNAME%`.`quartz_locks` VALUES  ('CALENDAR_ACCESS'),
 ('JOB_ACCESS'),
 ('MISFIRE_ACCESS'),
 ('STATE_ACCESS'),
 ('TRIGGER_ACCESS');
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_locks` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_paused_trigger_grps`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_paused_trigger_grps`;
CREATE TABLE  `%DBNAME%`.`quartz_paused_trigger_grps` (
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  PRIMARY KEY (`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_paused_trigger_grps`
--

/*!40000 ALTER TABLE `quartz_paused_trigger_grps` DISABLE KEYS */;
LOCK TABLES `quartz_paused_trigger_grps` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_paused_trigger_grps` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_scheduler_state`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_scheduler_state`;
CREATE TABLE  `%DBNAME%`.`quartz_scheduler_state` (
  `INSTANCE_NAME` varchar(80) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_scheduler_state`
--

/*!40000 ALTER TABLE `quartz_scheduler_state` DISABLE KEYS */;
LOCK TABLES `quartz_scheduler_state` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_scheduler_state` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_simple_triggers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_simple_triggers`;
CREATE TABLE  `%DBNAME%`.`quartz_simple_triggers` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_simple_triggers`
--

/*!40000 ALTER TABLE `quartz_simple_triggers` DISABLE KEYS */;
LOCK TABLES `quartz_simple_triggers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_simple_triggers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_trigger_listeners`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_trigger_listeners`;
CREATE TABLE  `%DBNAME%`.`quartz_trigger_listeners` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `TRIGGER_LISTENER` varchar(80) NOT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_LISTENER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_trigger_listeners`
--

/*!40000 ALTER TABLE `quartz_trigger_listeners` DISABLE KEYS */;
LOCK TABLES `quartz_trigger_listeners` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_trigger_listeners` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`quartz_triggers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`quartz_triggers`;
CREATE TABLE  `%DBNAME%`.`quartz_triggers` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) DEFAULT NULL,
  `CALENDAR_NAME` varchar(80) DEFAULT NULL,
  `MISFIRE_INSTR` int(11) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IX_F7655CC3` (`NEXT_FIRE_TIME`),
  KEY `IX_9955EFB5` (`TRIGGER_STATE`),
  KEY `IX_8040C593` (`TRIGGER_STATE`,`NEXT_FIRE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`quartz_triggers`
--

/*!40000 ALTER TABLE `quartz_triggers` DISABLE KEYS */;
LOCK TABLES `quartz_triggers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `quartz_triggers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`ratingsentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`ratingsentry`;
CREATE TABLE  `%DBNAME%`.`ratingsentry` (
  `entryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `score` double DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_B47E3C11` (`userId`,`classNameId`,`classPK`),
  KEY `IX_16184D57` (`classNameId`,`classPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`ratingsentry`
--

/*!40000 ALTER TABLE `ratingsentry` DISABLE KEYS */;
LOCK TABLES `ratingsentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `ratingsentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`ratingsstats`
--

DROP TABLE IF EXISTS `%DBNAME%`.`ratingsstats`;
CREATE TABLE  `%DBNAME%`.`ratingsstats` (
  `statsId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `totalEntries` int(11) DEFAULT NULL,
  `totalScore` double DEFAULT NULL,
  `averageScore` double DEFAULT NULL,
  PRIMARY KEY (`statsId`),
  UNIQUE KEY `IX_A6E99284` (`classNameId`,`classPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`ratingsstats`
--

/*!40000 ALTER TABLE `ratingsstats` DISABLE KEYS */;
LOCK TABLES `ratingsstats` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `ratingsstats` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`region`
--

DROP TABLE IF EXISTS `%DBNAME%`.`region`;
CREATE TABLE  `%DBNAME%`.`region` (
  `regionId` bigint(20) NOT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `regionCode` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`regionId`),
  KEY `IX_2D9A426F` (`active_`),
  KEY `IX_16D87CA7` (`countryId`),
  KEY `IX_11FB3E42` (`countryId`,`active_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`region`
--

/*!40000 ALTER TABLE `region` DISABLE KEYS */;
LOCK TABLES `region` WRITE;
INSERT INTO `%DBNAME%`.`region` VALUES  (1001,1,'AB','Alberta',1),
 (1002,1,'BC','British Columbia',1),
 (1003,1,'MB','Manitoba',1),
 (1004,1,'NB','New Brunswick',1),
 (1005,1,'NL','Newfoundland and Labrador',1),
 (1006,1,'NT','Northwest Territories',1),
 (1007,1,'NS','Nova Scotia',1),
 (1008,1,'NU','Nunavut',1),
 (1009,1,'ON','Ontario',1),
 (1010,1,'PE','Prince Edward Island',1),
 (1011,1,'QC','Quebec',1),
 (1012,1,'SK','Saskatchewan',1),
 (1013,1,'YT','Yukon',1),
 (3001,3,'A','Alsace',1),
 (3002,3,'B','Aquitaine',1),
 (3003,3,'C','Auvergne',1),
 (3004,3,'P','Basse-Normandie',1),
 (3005,3,'D','Bourgogne',1),
 (3006,3,'E','Bretagne',1),
 (3007,3,'F','Centre',1),
 (3008,3,'G','Champagne-Ardenne',1),
 (3009,3,'H','Corse',1),
 (3010,3,'GF','Guyane',1),
 (3011,3,'I','Franche Comté',1),
 (3012,3,'GP','Guadeloupe',1),
 (3013,3,'Q','Haute-Normandie',1),
 (3014,3,'J','Île-de-France',1),
 (3015,3,'K','Languedoc-Roussillon',1),
 (3016,3,'L','Limousin',1),
 (3017,3,'M','Lorraine',1),
 (3018,3,'MQ','Martinique',1),
 (3019,3,'N','Midi-Pyrénées',1),
 (3020,3,'O','Nord Pas de Calais',1),
 (3021,3,'R','Pays de la Loire',1),
 (3022,3,'S','Picardie',1),
 (3023,3,'T','Poitou-Charentes',1),
 (3024,3,'U','Provence-Alpes-Côte-d\'Azur',1),
 (3025,3,'RE','Réunion',1),
 (3026,3,'V','Rhône-Alpes',1),
 (4001,4,'BW','Baden-Württemberg',1),
 (4002,4,'BY','Bayern',1),
 (4003,4,'BE','Berlin',1),
 (4004,4,'BR','Brandenburg',1),
 (4005,4,'HB','Bremen',1),
 (4006,4,'HH','Hamburg',1),
 (4007,4,'HE','Hessen',1),
 (4008,4,'MV','Mecklenburg-Vorpommern',1),
 (4009,4,'NI','Niedersachsen',1),
 (4010,4,'NW','Nordrhein-Westfalen',1),
 (4011,4,'RP','Rheinland-Pfalz',1),
 (4012,4,'SL','Saarland',1),
 (4013,4,'SN','Sachsen',1),
 (4014,4,'ST','Sachsen-Anhalt',1),
 (4015,4,'SH','Schleswig-Holstein',1),
 (4016,4,'TH','Thüringen',1),
 (8001,8,'AG','Agrigento',1),
 (8002,8,'AL','Alessandria',1),
 (8003,8,'AN','Ancona',1),
 (8004,8,'AO','Aosta',1),
 (8005,8,'AR','Arezzo',1),
 (8006,8,'AP','Ascoli Piceno',1),
 (8007,8,'AT','Asti',1),
 (8008,8,'AV','Avellino',1),
 (8009,8,'BA','Bari',1),
 (8010,8,'BT','Barletta-Andria-Trani',1),
 (8011,8,'BL','Belluno',1),
 (8012,8,'BN','Benevento',1),
 (8013,8,'BG','Bergamo',1),
 (8014,8,'BI','Biella',1),
 (8015,8,'BO','Bologna',1),
 (8016,8,'BZ','Bolzano',1),
 (8017,8,'BS','Brescia',1),
 (8018,8,'BR','Brindisi',1),
 (8019,8,'CA','Cagliari',1),
 (8020,8,'CL','Caltanissetta',1),
 (8021,8,'CB','Campobasso',1),
 (8022,8,'CI','Carbonia-Iglesias',1),
 (8023,8,'CE','Caserta',1),
 (8024,8,'CT','Catania',1),
 (8025,8,'CZ','Catanzaro',1),
 (8026,8,'CH','Chieti',1),
 (8027,8,'CO','Como',1),
 (8028,8,'CS','Cosenza',1),
 (8029,8,'CR','Cremona',1),
 (8030,8,'KR','Crotone',1),
 (8031,8,'CN','Cuneo',1),
 (8032,8,'EN','Enna',1),
 (8033,8,'FM','Fermo',1),
 (8034,8,'FE','Ferrara',1),
 (8035,8,'FI','Firenze',1),
 (8036,8,'FG','Foggia',1),
 (8037,8,'FC','Forli-Cesena',1),
 (8038,8,'FR','Frosinone',1),
 (8039,8,'GE','Genova',1),
 (8040,8,'GO','Gorizia',1),
 (8041,8,'GR','Grosseto',1),
 (8042,8,'IM','Imperia',1),
 (8043,8,'IS','Isernia',1),
 (8044,8,'AQ','L\'Aquila',1),
 (8045,8,'SP','La Spezia',1),
 (8046,8,'LT','Latina',1),
 (8047,8,'LE','Lecce',1),
 (8048,8,'LC','Lecco',1),
 (8049,8,'LI','Livorno',1),
 (8050,8,'LO','Lodi',1),
 (8051,8,'LU','Lucca',1),
 (8052,8,'MC','Macerata',1),
 (8053,8,'MN','Mantova',1),
 (8054,8,'MS','Massa-Carrara',1),
 (8055,8,'MT','Matera',1),
 (8056,8,'MA','Medio Campidano',1),
 (8057,8,'ME','Messina',1),
 (8058,8,'MI','Milano',1),
 (8059,8,'MO','Modena',1),
 (8060,8,'MZ','Monza',1),
 (8061,8,'NA','Napoli',1),
 (8062,8,'NO','Novara',1),
 (8063,8,'NU','Nuoro',1),
 (8064,8,'OG','Ogliastra',1),
 (8065,8,'OT','Olbia-Tempio',1),
 (8066,8,'OR','Oristano',1),
 (8067,8,'PD','Padova',1),
 (8068,8,'PA','Palermo',1),
 (8069,8,'PR','Parma',1),
 (8070,8,'PV','Pavia',1),
 (8071,8,'PG','Perugia',1),
 (8072,8,'PU','Pesaro e Urbino',1),
 (8073,8,'PE','Pescara',1),
 (8074,8,'PC','Piacenza',1),
 (8075,8,'PI','Pisa',1),
 (8076,8,'PT','Pistoia',1),
 (8077,8,'PN','Pordenone',1),
 (8078,8,'PZ','Potenza',1),
 (8079,8,'PO','Prato',1),
 (8080,8,'RG','Ragusa',1),
 (8081,8,'RA','Ravenna',1),
 (8082,8,'RC','Reggio Calabria',1),
 (8083,8,'RE','Reggio Emilia',1),
 (8084,8,'RI','Rieti',1),
 (8085,8,'RN','Rimini',1),
 (8086,8,'RM','Roma',1),
 (8087,8,'RO','Rovigo',1),
 (8088,8,'SA','Salerno',1),
 (8089,8,'SS','Sassari',1),
 (8090,8,'SV','Savona',1),
 (8091,8,'SI','Siena',1),
 (8092,8,'SR','Siracusa',1),
 (8093,8,'SO','Sondrio',1),
 (8094,8,'TA','Taranto',1),
 (8095,8,'TE','Teramo',1),
 (8096,8,'TR','Terni',1),
 (8097,8,'TO','Torino',1),
 (8098,8,'TP','Trapani',1),
 (8099,8,'TN','Trento',1),
 (8100,8,'TV','Treviso',1),
 (8101,8,'TS','Trieste',1),
 (8102,8,'UD','Udine',1),
 (8103,8,'VA','Varese',1),
 (8104,8,'VE','Venezia',1),
 (8105,8,'VB','Verbano-Cusio-Ossola',1),
 (8106,8,'VC','Vercelli',1),
 (8107,8,'VR','Verona',1),
 (8108,8,'VV','Vibo Valentia',1),
 (8109,8,'VI','Vicenza',1),
 (8110,8,'VT','Viterbo',1),
 (15001,15,'AN','Andalusia',1),
 (15002,15,'AR','Aragon',1),
 (15003,15,'AS','Asturias',1),
 (15004,15,'IB','Balearic Islands',1),
 (15005,15,'PV','Basque Country',1),
 (15006,15,'CN','Canary Islands',1),
 (15007,15,'CB','Cantabria',1),
 (15008,15,'CL','Castile and Leon',1),
 (15009,15,'CM','Castile-La Mancha',1),
 (15010,15,'CT','Catalonia',1),
 (15011,15,'CE','Ceuta',1),
 (15012,15,'EX','Extremadura',1),
 (15013,15,'GA','Galicia',1),
 (15014,15,'LO','La Rioja',1),
 (15015,15,'M','Madrid',1),
 (15016,15,'ML','Melilla',1),
 (15017,15,'MU','Murcia',1),
 (15018,15,'NA','Navarra',1),
 (15019,15,'VC','Valencia',1),
 (19001,19,'AL','Alabama',1),
 (19002,19,'AK','Alaska',1),
 (19003,19,'AZ','Arizona',1),
 (19004,19,'AR','Arkansas',1),
 (19005,19,'CA','California',1),
 (19006,19,'CO','Colorado',1),
 (19007,19,'CT','Connecticut',1),
 (19008,19,'DC','District of Columbia',1),
 (19009,19,'DE','Delaware',1),
 (19010,19,'FL','Florida',1),
 (19011,19,'GA','Georgia',1),
 (19012,19,'HI','Hawaii',1),
 (19013,19,'ID','Idaho',1),
 (19014,19,'IL','Illinois',1),
 (19015,19,'IN','Indiana',1),
 (19016,19,'IA','Iowa',1),
 (19017,19,'KS','Kansas',1),
 (19018,19,'KY','Kentucky ',1),
 (19019,19,'LA','Louisiana ',1),
 (19020,19,'ME','Maine',1),
 (19021,19,'MD','Maryland',1),
 (19022,19,'MA','Massachusetts',1),
 (19023,19,'MI','Michigan',1),
 (19024,19,'MN','Minnesota',1),
 (19025,19,'MS','Mississippi',1),
 (19026,19,'MO','Missouri',1),
 (19027,19,'MT','Montana',1),
 (19028,19,'NE','Nebraska',1),
 (19029,19,'NV','Nevada',1),
 (19030,19,'NH','New Hampshire',1),
 (19031,19,'NJ','New Jersey',1),
 (19032,19,'NM','New Mexico',1),
 (19033,19,'NY','New York',1),
 (19034,19,'NC','North Carolina',1),
 (19035,19,'ND','North Dakota',1),
 (19036,19,'OH','Ohio',1),
 (19037,19,'OK','Oklahoma ',1),
 (19038,19,'OR','Oregon',1),
 (19039,19,'PA','Pennsylvania',1),
 (19040,19,'PR','Puerto Rico',1),
 (19041,19,'RI','Rhode Island',1),
 (19042,19,'SC','South Carolina',1),
 (19043,19,'SD','South Dakota',1),
 (19044,19,'TN','Tennessee',1),
 (19045,19,'TX','Texas',1),
 (19046,19,'UT','Utah',1),
 (19047,19,'VT','Vermont',1),
 (19048,19,'VA','Virginia',1),
 (19049,19,'WA','Washington',1),
 (19050,19,'WV','West Virginia',1),
 (19051,19,'WI','Wisconsin',1),
 (19052,19,'WY','Wyoming',1),
 (32001,32,'ACT','Australian Capital Territory',1),
 (32002,32,'NSW','New South Wales',1),
 (32003,32,'NT','Northern Territory',1),
 (32004,32,'QLD','Queensland',1),
 (32005,32,'SA','South Australia',1),
 (32006,32,'TAS','Tasmania',1),
 (32007,32,'VIC','Victoria',1),
 (32008,32,'WA','Western Australia',1),
 (202001,202,'AG','Aargau',1),
 (202002,202,'AR','Appenzell Ausserrhoden',1),
 (202003,202,'AI','Appenzell Innerrhoden',1),
 (202004,202,'BL','Basel-Landschaft',1),
 (202005,202,'BS','Basel-Stadt',1),
 (202006,202,'BE','Bern',1),
 (202007,202,'FR','Fribourg',1),
 (202008,202,'GE','Geneva',1),
 (202009,202,'GL','Glarus',1),
 (202010,202,'GR','Graubünden',1),
 (202011,202,'JU','Jura',1),
 (202012,202,'LU','Lucerne',1),
 (202013,202,'NE','Neuchâtel',1),
 (202014,202,'NW','Nidwalden',1),
 (202015,202,'OW','Obwalden',1),
 (202016,202,'SH','Schaffhausen',1),
 (202017,202,'SZ','Schwyz',1),
 (202018,202,'SO','Solothurn',1),
 (202019,202,'SG','St. Gallen',1),
 (202020,202,'TG','Thurgau',1),
 (202021,202,'TI','Ticino',1),
 (202022,202,'UR','Uri',1),
 (202023,202,'VS','Valais',1),
 (202024,202,'VD','Vaud',1),
 (202025,202,'ZG','Zug',1),
 (202026,202,'ZH','Zürich',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`release_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`release_`;
CREATE TABLE  `%DBNAME%`.`release_` (
  `releaseId` bigint(20) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `servletContextName` varchar(75) DEFAULT NULL,
  `buildNumber` int(11) DEFAULT NULL,
  `buildDate` datetime DEFAULT NULL,
  `verified` tinyint(4) DEFAULT NULL,
  `testString` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`releaseId`),
  KEY `IX_8BD6BCA7` (`servletContextName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`release_`
--

/*!40000 ALTER TABLE `release_` DISABLE KEYS */;
LOCK TABLES `release_` WRITE;
INSERT INTO `%DBNAME%`.`release_` VALUES  (1,'2010-08-03 11:44:57','2010-08-03 10:18:18','portal',5205,'2009-08-06 00:00:00',1,'You take the blue pill, the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill, you stay in Wonderland, and I show you how deep the rabbit hole goes.');
UNLOCK TABLES;
/*!40000 ALTER TABLE `release_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`resource_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`resource_`;
CREATE TABLE  `%DBNAME%`.`resource_` (
  `resourceId` bigint(20) NOT NULL,
  `codeId` bigint(20) DEFAULT NULL,
  `primKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`resourceId`),
  UNIQUE KEY `IX_67DE7856` (`codeId`,`primKey`),
  KEY `IX_2578FBD3` (`codeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`resource_`
--

/*!40000 ALTER TABLE `resource_` DISABLE KEYS */;
LOCK TABLES `resource_` WRITE;
INSERT INTO `%DBNAME%`.`resource_` VALUES  (2,1,'10114'),
 (3,2,'10131'),
 (5,2,'10138'),
 (4,3,'10134'),
 (301,3,'10608'),
 (302,3,'10613'),
 (303,3,'10618'),
 (304,3,'10623'),
 (305,3,'10628'),
 (306,3,'10633'),
 (307,3,'10638'),
 (308,3,'10643'),
 (309,3,'10648'),
 (310,3,'10653'),
 (311,3,'10658'),
 (312,3,'10663'),
 (313,3,'10669'),
 (314,3,'10675'),
 (315,3,'10681'),
 (316,3,'10687'),
 (317,3,'10693'),
 (318,3,'10698'),
 (319,3,'10702'),
 (320,3,'10707'),
 (321,3,'10712'),
 (322,3,'10717'),
 (323,3,'10722'),
 (324,3,'10727'),
 (325,3,'10732'),
 (326,3,'10737'),
 (327,3,'10742'),
 (328,3,'10747'),
 (329,3,'10751'),
 (330,3,'10756'),
 (331,3,'10761'),
 (7,4,'10114'),
 (8,5,'10146'),
 (9,6,'10138'),
 (332,7,'10608_LAYOUT_103'),
 (10,8,'10114'),
 (11,9,'10138'),
 (13,11,'10114'),
 (14,12,'10138'),
 (16,14,'10114'),
 (17,15,'10138'),
 (148,284,'10114'),
 (149,287,'10371'),
 (150,287,'10372'),
 (151,364,'10114'),
 (206,365,'10138'),
 (333,367,'10608_LAYOUT_56_INSTANCE_aIl8'),
 (153,384,'10114'),
 (159,388,'10114'),
 (158,392,'10114'),
 (160,396,'10114'),
 (104,416,'10114'),
 (105,417,'10138'),
 (106,419,'10206'),
 (107,424,'10114'),
 (108,425,'10138'),
 (109,427,'10207'),
 (110,427,'10208'),
 (111,427,'10209'),
 (112,427,'10210'),
 (113,427,'10211'),
 (114,427,'10212'),
 (146,476,'10114'),
 (176,477,'10138'),
 (177,479,'10389'),
 (178,479,'10396'),
 (179,479,'10403'),
 (180,479,'10410'),
 (181,479,'10417'),
 (182,479,'10424'),
 (183,479,'10431'),
 (184,479,'10438'),
 (185,479,'10445'),
 (186,479,'10452'),
 (187,479,'10459'),
 (188,479,'10466'),
 (189,479,'10473'),
 (190,479,'10480'),
 (191,479,'10487'),
 (192,479,'10494'),
 (193,479,'10501'),
 (194,479,'10508'),
 (195,479,'10515'),
 (196,479,'10522'),
 (197,479,'10529'),
 (198,479,'10536'),
 (199,479,'10543'),
 (200,479,'10550'),
 (201,479,'10557'),
 (202,479,'10564'),
 (203,479,'10571'),
 (204,479,'10578'),
 (155,480,'10114'),
 (162,484,'10114'),
 (163,485,'10138'),
 (164,487,'10373'),
 (165,487,'10374'),
 (166,487,'10375'),
 (167,487,'10376'),
 (168,487,'10377'),
 (169,488,'10114'),
 (170,489,'10138'),
 (171,491,'10378'),
 (172,491,'10380'),
 (173,491,'10382'),
 (174,491,'10384'),
 (175,491,'10386'),
 (152,538,'10114'),
 (157,542,'10114'),
 (101,546,'10114'),
 (102,547,'10138'),
 (103,549,'10205'),
 (156,550,'10114'),
 (154,586,'10114'),
 (161,590,'10114'),
 (147,594,'10114');
UNLOCK TABLES;
/*!40000 ALTER TABLE `resource_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`resourceaction`
--

DROP TABLE IF EXISTS `%DBNAME%`.`resourceaction`;
CREATE TABLE  `%DBNAME%`.`resourceaction` (
  `resourceActionId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `bitwiseValue` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`resourceActionId`),
  UNIQUE KEY `IX_EDB9986E` (`name`,`actionId`),
  KEY `IX_81F2DB09` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`resourceaction`
--

/*!40000 ALTER TABLE `resourceaction` DISABLE KEYS */;
LOCK TABLES `resourceaction` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `resourceaction` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`resourcecode`
--

DROP TABLE IF EXISTS `%DBNAME%`.`resourcecode`;
CREATE TABLE  `%DBNAME%`.`resourcecode` (
  `codeId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  PRIMARY KEY (`codeId`),
  UNIQUE KEY `IX_A32C097E` (`companyId`,`name`,`scope`),
  KEY `IX_717FDD47` (`companyId`),
  KEY `IX_AACAFF40` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`resourcecode`
--

/*!40000 ALTER TABLE `resourcecode` DISABLE KEYS */;
LOCK TABLES `resourcecode` WRITE;
INSERT INTO `%DBNAME%`.`resourcecode` VALUES  (177,10114,'100',1),
 (178,10114,'100',2),
 (179,10114,'100',3),
 (180,10114,'100',4),
 (332,10114,'101',1),
 (333,10114,'101',2),
 (334,10114,'101',3),
 (335,10114,'101',4),
 (638,10114,'102',1),
 (639,10114,'102',2),
 (640,10114,'102',3),
 (641,10114,'102',4),
 (8,10114,'103',1),
 (9,10114,'103',2),
 (501,10114,'103',3),
 (7,10114,'103',4),
 (173,10114,'104',1),
 (174,10114,'104',2),
 (175,10114,'104',3),
 (176,10114,'104',4),
 (276,10114,'107',1),
 (277,10114,'107',2),
 (278,10114,'107',3),
 (279,10114,'107',4),
 (312,10114,'108',1),
 (313,10114,'108',2),
 (314,10114,'108',3),
 (315,10114,'108',4),
 (229,10114,'11',1),
 (230,10114,'11',2),
 (231,10114,'11',3),
 (232,10114,'11',4),
 (622,10114,'110',1),
 (623,10114,'110',2),
 (624,10114,'110',3),
 (625,10114,'110',4),
 (372,10114,'111',1),
 (373,10114,'111',2),
 (374,10114,'111',3),
 (375,10114,'111',4),
 (582,10114,'113',1),
 (583,10114,'113',2),
 (584,10114,'113',3),
 (585,10114,'113',4),
 (614,10114,'114',1),
 (615,10114,'114',2),
 (616,10114,'114',3),
 (617,10114,'114',4),
 (356,10114,'115',1),
 (357,10114,'115',2),
 (358,10114,'115',3),
 (359,10114,'115',4),
 (464,10114,'116',1),
 (465,10114,'116',2),
 (466,10114,'116',3),
 (467,10114,'116',4),
 (272,10114,'118',1),
 (273,10114,'118',2),
 (274,10114,'118',3),
 (275,10114,'118',4),
 (606,10114,'119',1),
 (607,10114,'119',2),
 (608,10114,'119',3),
 (609,10114,'119',4),
 (209,10114,'120',1),
 (210,10114,'120',2),
 (211,10114,'120',3),
 (212,10114,'120',4),
 (336,10114,'121',1),
 (337,10114,'121',2),
 (338,10114,'121',3),
 (339,10114,'121',4),
 (145,10114,'122',1),
 (146,10114,'122',2),
 (147,10114,'122',3),
 (148,10114,'122',4),
 (225,10114,'124',1),
 (226,10114,'124',2),
 (227,10114,'124',3),
 (228,10114,'124',4),
 (300,10114,'125',1),
 (301,10114,'125',2),
 (302,10114,'125',3),
 (303,10114,'125',4),
 (610,10114,'126',1),
 (611,10114,'126',2),
 (612,10114,'126',3),
 (613,10114,'126',4),
 (558,10114,'127',1),
 (559,10114,'127',2),
 (560,10114,'127',3),
 (561,10114,'127',4),
 (201,10114,'128',1),
 (202,10114,'128',2),
 (203,10114,'128',3),
 (204,10114,'128',4),
 (165,10114,'129',1),
 (166,10114,'129',2),
 (167,10114,'129',3),
 (168,10114,'129',4),
 (141,10114,'130',1),
 (142,10114,'130',2),
 (143,10114,'130',3),
 (144,10114,'130',4),
 (634,10114,'131',1),
 (635,10114,'131',2),
 (636,10114,'131',3),
 (637,10114,'131',4),
 (506,10114,'132',1),
 (507,10114,'132',2),
 (508,10114,'132',3),
 (509,10114,'132',4),
 (460,10114,'133',1),
 (461,10114,'133',2),
 (462,10114,'133',3),
 (463,10114,'133',4),
 (137,10114,'134',1),
 (138,10114,'134',2),
 (139,10114,'134',3),
 (140,10114,'134',4),
 (626,10114,'135',1),
 (627,10114,'135',2),
 (628,10114,'135',3),
 (629,10114,'135',4),
 (554,10114,'136',1),
 (555,10114,'136',2),
 (556,10114,'136',3),
 (557,10114,'136',4),
 (440,10114,'137',1),
 (441,10114,'137',2),
 (442,10114,'137',3),
 (443,10114,'137',4),
 (316,10114,'139',1),
 (317,10114,'139',2),
 (318,10114,'139',3),
 (319,10114,'139',4),
 (502,10114,'140',1),
 (503,10114,'140',2),
 (504,10114,'140',3),
 (505,10114,'140',4),
 (432,10114,'141',1),
 (433,10114,'141',2),
 (434,10114,'141',3),
 (435,10114,'141',4),
 (368,10114,'142',1),
 (369,10114,'142',2),
 (370,10114,'142',3),
 (371,10114,'142',4),
 (344,10114,'143',1),
 (345,10114,'143',2),
 (346,10114,'143',3),
 (347,10114,'143',4),
 (304,10114,'144',1),
 (305,10114,'144',2),
 (306,10114,'144',3),
 (307,10114,'144',4),
 (468,10114,'15',1),
 (469,10114,'15',2),
 (470,10114,'15',3),
 (471,10114,'15',4),
 (376,10114,'16',1),
 (377,10114,'16',2),
 (378,10114,'16',3),
 (379,10114,'16',4),
 (185,10114,'19',1),
 (186,10114,'19',2),
 (187,10114,'19',3),
 (188,10114,'19',4),
 (602,10114,'2',1),
 (603,10114,'2',2),
 (604,10114,'2',3),
 (605,10114,'2',4),
 (384,10114,'20',1),
 (385,10114,'20',2),
 (386,10114,'20',3),
 (387,10114,'20',4),
 (404,10114,'23',1),
 (405,10114,'23',2),
 (406,10114,'23',3),
 (407,10114,'23',4),
 (598,10114,'24',1),
 (599,10114,'24',2),
 (600,10114,'24',3),
 (601,10114,'24',4),
 (566,10114,'25',1),
 (567,10114,'25',2),
 (568,10114,'25',3),
 (569,10114,'25',4),
 (169,10114,'26',1),
 (170,10114,'26',2),
 (171,10114,'26',3),
 (172,10114,'26',4),
 (121,10114,'27',1),
 (122,10114,'27',2),
 (123,10114,'27',3),
 (124,10114,'27',4),
 (444,10114,'28',1),
 (445,10114,'28',2),
 (446,10114,'28',3),
 (447,10114,'28',4),
 (213,10114,'29',1),
 (214,10114,'29',2),
 (215,10114,'29',3),
 (216,10114,'29',4),
 (380,10114,'3',1),
 (381,10114,'3',2),
 (382,10114,'3',3),
 (383,10114,'3',4),
 (292,10114,'30',1),
 (293,10114,'30',2),
 (294,10114,'30',3),
 (295,10114,'30',4),
 (538,10114,'31',1),
 (539,10114,'31',2),
 (540,10114,'31',3),
 (541,10114,'31',4),
 (586,10114,'33',1),
 (587,10114,'33',2),
 (588,10114,'33',3),
 (589,10114,'33',4),
 (510,10114,'34',1),
 (511,10114,'34',2),
 (512,10114,'34',3),
 (513,10114,'34',4),
 (149,10114,'36',1),
 (150,10114,'36',2),
 (151,10114,'36',3),
 (152,10114,'36',4),
 (348,10114,'37',1),
 (349,10114,'37',2),
 (350,10114,'37',3),
 (351,10114,'37',4),
 (256,10114,'39',1),
 (257,10114,'39',2),
 (258,10114,'39',3),
 (259,10114,'39',4),
 (11,10114,'47',1),
 (12,10114,'47',2),
 (492,10114,'47',3),
 (10,10114,'47',4),
 (296,10114,'48',1),
 (297,10114,'48',2),
 (298,10114,'48',3),
 (299,10114,'48',4),
 (340,10114,'49',1),
 (341,10114,'49',2),
 (342,10114,'49',3),
 (343,10114,'49',4),
 (562,10114,'50',1),
 (563,10114,'50',2),
 (564,10114,'50',3),
 (565,10114,'50',4),
 (493,10114,'54',1),
 (494,10114,'54',2),
 (495,10114,'54',3),
 (496,10114,'54',4),
 (364,10114,'56',1),
 (365,10114,'56',2),
 (366,10114,'56',3),
 (367,10114,'56',4),
 (14,10114,'58',1),
 (15,10114,'58',2),
 (255,10114,'58',3),
 (13,10114,'58',4),
 (630,10114,'59',1),
 (631,10114,'59',2),
 (632,10114,'59',3),
 (633,10114,'59',4),
 (360,10114,'6',1),
 (361,10114,'6',2),
 (362,10114,'6',3),
 (363,10114,'6',4),
 (530,10114,'61',1),
 (531,10114,'61',2),
 (532,10114,'61',3),
 (533,10114,'61',4),
 (308,10114,'62',1),
 (309,10114,'62',2),
 (310,10114,'62',3),
 (311,10114,'62',4),
 (181,10114,'64',1),
 (182,10114,'64',2),
 (183,10114,'64',3),
 (184,10114,'64',4),
 (125,10114,'66',1),
 (126,10114,'66',2),
 (127,10114,'66',3),
 (128,10114,'66',4),
 (618,10114,'67',1),
 (619,10114,'67',2),
 (620,10114,'67',3),
 (621,10114,'67',4),
 (428,10114,'70',1),
 (429,10114,'70',2),
 (430,10114,'70',3),
 (431,10114,'70',4),
 (264,10114,'71',1),
 (265,10114,'71',2),
 (266,10114,'71',3),
 (267,10114,'71',4),
 (534,10114,'73',1),
 (535,10114,'73',2),
 (536,10114,'73',3),
 (537,10114,'73',4),
 (352,10114,'77',1),
 (353,10114,'77',2),
 (354,10114,'77',3),
 (355,10114,'77',4),
 (280,10114,'79',1),
 (281,10114,'79',2),
 (282,10114,'79',3),
 (283,10114,'79',4),
 (243,10114,'8',1),
 (244,10114,'8',2),
 (245,10114,'8',3),
 (246,10114,'8',4),
 (497,10114,'82',1),
 (498,10114,'82',2),
 (499,10114,'82',3),
 (500,10114,'82',4),
 (408,10114,'83',1),
 (409,10114,'83',2),
 (410,10114,'83',3),
 (411,10114,'83',4),
 (324,10114,'84',1),
 (325,10114,'84',2),
 (326,10114,'84',3),
 (327,10114,'84',4),
 (268,10114,'85',1),
 (269,10114,'85',2),
 (270,10114,'85',3),
 (271,10114,'85',4),
 (205,10114,'86',1),
 (206,10114,'86',2),
 (207,10114,'86',3),
 (208,10114,'86',4),
 (133,10114,'87',1),
 (134,10114,'87',2),
 (135,10114,'87',3),
 (136,10114,'87',4),
 (129,10114,'88',1),
 (130,10114,'88',2),
 (131,10114,'88',3),
 (132,10114,'88',4),
 (436,10114,'9',1),
 (437,10114,'9',2),
 (438,10114,'9',3),
 (439,10114,'9',4),
 (578,10114,'90',1),
 (579,10114,'90',2),
 (580,10114,'90',3),
 (581,10114,'90',4),
 (260,10114,'97',1),
 (261,10114,'97',2),
 (262,10114,'97',3),
 (263,10114,'97',4),
 (101,10114,'98',1),
 (102,10114,'98',2),
 (103,10114,'98',3),
 (104,10114,'98',4),
 (412,10114,'99',1),
 (413,10114,'99',2),
 (414,10114,'99',3),
 (415,10114,'99',4),
 (218,10114,'com.liferay.portal.model.Group',1),
 (219,10114,'com.liferay.portal.model.Group',2),
 (220,10114,'com.liferay.portal.model.Group',3),
 (6,10114,'com.liferay.portal.model.Group',4),
 (1,10114,'com.liferay.portal.model.Layout',1),
 (2,10114,'com.liferay.portal.model.Layout',2),
 (217,10114,'com.liferay.portal.model.Layout',3),
 (3,10114,'com.liferay.portal.model.Layout',4),
 (239,10114,'com.liferay.portal.model.Organization',1),
 (240,10114,'com.liferay.portal.model.Organization',2),
 (241,10114,'com.liferay.portal.model.Organization',3),
 (242,10114,'com.liferay.portal.model.Organization',4),
 (288,10114,'com.liferay.portal.model.PasswordPolicy',1),
 (289,10114,'com.liferay.portal.model.PasswordPolicy',2),
 (290,10114,'com.liferay.portal.model.PasswordPolicy',3),
 (291,10114,'com.liferay.portal.model.PasswordPolicy',4),
 (284,10114,'com.liferay.portal.model.Role',1),
 (285,10114,'com.liferay.portal.model.Role',2),
 (286,10114,'com.liferay.portal.model.Role',3),
 (287,10114,'com.liferay.portal.model.Role',4),
 (4,10114,'com.liferay.portal.model.User',1),
 (233,10114,'com.liferay.portal.model.User',2),
 (234,10114,'com.liferay.portal.model.User',3),
 (5,10114,'com.liferay.portal.model.User',4),
 (235,10114,'com.liferay.portal.model.UserGroup',1),
 (236,10114,'com.liferay.portal.model.UserGroup',2),
 (237,10114,'com.liferay.portal.model.UserGroup',3),
 (238,10114,'com.liferay.portal.model.UserGroup',4),
 (328,10114,'com.liferay.portlet.announcements.model.AnnouncementsEntry',1),
 (329,10114,'com.liferay.portlet.announcements.model.AnnouncementsEntry',2),
 (330,10114,'com.liferay.portlet.announcements.model.AnnouncementsEntry',3),
 (331,10114,'com.liferay.portlet.announcements.model.AnnouncementsEntry',4),
 (590,10114,'com.liferay.portlet.blogs',1),
 (591,10114,'com.liferay.portlet.blogs',2),
 (592,10114,'com.liferay.portlet.blogs',3),
 (593,10114,'com.liferay.portlet.blogs',4),
 (594,10114,'com.liferay.portlet.blogs.model.BlogsEntry',1),
 (595,10114,'com.liferay.portlet.blogs.model.BlogsEntry',2),
 (596,10114,'com.liferay.portlet.blogs.model.BlogsEntry',3),
 (597,10114,'com.liferay.portlet.blogs.model.BlogsEntry',4),
 (452,10114,'com.liferay.portlet.bookmarks',1),
 (453,10114,'com.liferay.portlet.bookmarks',2),
 (454,10114,'com.liferay.portlet.bookmarks',3),
 (455,10114,'com.liferay.portlet.bookmarks',4),
 (456,10114,'com.liferay.portlet.bookmarks.model.BookmarksEntry',1),
 (457,10114,'com.liferay.portlet.bookmarks.model.BookmarksEntry',2),
 (458,10114,'com.liferay.portlet.bookmarks.model.BookmarksEntry',3),
 (459,10114,'com.liferay.portlet.bookmarks.model.BookmarksEntry',4),
 (448,10114,'com.liferay.portlet.bookmarks.model.BookmarksFolder',1),
 (449,10114,'com.liferay.portlet.bookmarks.model.BookmarksFolder',2),
 (450,10114,'com.liferay.portlet.bookmarks.model.BookmarksFolder',3),
 (451,10114,'com.liferay.portlet.bookmarks.model.BookmarksFolder',4),
 (247,10114,'com.liferay.portlet.calendar',1),
 (248,10114,'com.liferay.portlet.calendar',2),
 (249,10114,'com.liferay.portlet.calendar',3),
 (250,10114,'com.liferay.portlet.calendar',4),
 (251,10114,'com.liferay.portlet.calendar.model.CalEvent',1),
 (252,10114,'com.liferay.portlet.calendar.model.CalEvent',2),
 (253,10114,'com.liferay.portlet.calendar.model.CalEvent',3),
 (254,10114,'com.liferay.portlet.calendar.model.CalEvent',4),
 (392,10114,'com.liferay.portlet.documentlibrary',1),
 (393,10114,'com.liferay.portlet.documentlibrary',2),
 (394,10114,'com.liferay.portlet.documentlibrary',3),
 (395,10114,'com.liferay.portlet.documentlibrary',4),
 (396,10114,'com.liferay.portlet.documentlibrary.model.DLFileEntry',1),
 (397,10114,'com.liferay.portlet.documentlibrary.model.DLFileEntry',2),
 (398,10114,'com.liferay.portlet.documentlibrary.model.DLFileEntry',3),
 (399,10114,'com.liferay.portlet.documentlibrary.model.DLFileEntry',4),
 (400,10114,'com.liferay.portlet.documentlibrary.model.DLFileShortcut',1),
 (401,10114,'com.liferay.portlet.documentlibrary.model.DLFileShortcut',2),
 (402,10114,'com.liferay.portlet.documentlibrary.model.DLFileShortcut',3),
 (403,10114,'com.liferay.portlet.documentlibrary.model.DLFileShortcut',4),
 (388,10114,'com.liferay.portlet.documentlibrary.model.DLFolder',1),
 (389,10114,'com.liferay.portlet.documentlibrary.model.DLFolder',2),
 (390,10114,'com.liferay.portlet.documentlibrary.model.DLFolder',3),
 (391,10114,'com.liferay.portlet.documentlibrary.model.DLFolder',4),
 (320,10114,'com.liferay.portlet.expando.model.ExpandoColumn',1),
 (321,10114,'com.liferay.portlet.expando.model.ExpandoColumn',2),
 (322,10114,'com.liferay.portlet.expando.model.ExpandoColumn',3),
 (323,10114,'com.liferay.portlet.expando.model.ExpandoColumn',4),
 (550,10114,'com.liferay.portlet.imagegallery',1),
 (551,10114,'com.liferay.portlet.imagegallery',2),
 (552,10114,'com.liferay.portlet.imagegallery',3),
 (553,10114,'com.liferay.portlet.imagegallery',4),
 (546,10114,'com.liferay.portlet.imagegallery.model.IGFolder',1),
 (547,10114,'com.liferay.portlet.imagegallery.model.IGFolder',2),
 (548,10114,'com.liferay.portlet.imagegallery.model.IGFolder',3),
 (549,10114,'com.liferay.portlet.imagegallery.model.IGFolder',4),
 (542,10114,'com.liferay.portlet.imagegallery.model.IGImage',1),
 (543,10114,'com.liferay.portlet.imagegallery.model.IGImage',2),
 (544,10114,'com.liferay.portlet.imagegallery.model.IGImage',3),
 (545,10114,'com.liferay.portlet.imagegallery.model.IGImage',4),
 (480,10114,'com.liferay.portlet.journal',1),
 (481,10114,'com.liferay.portlet.journal',2),
 (482,10114,'com.liferay.portlet.journal',3),
 (483,10114,'com.liferay.portlet.journal',4),
 (476,10114,'com.liferay.portlet.journal.model.JournalArticle',1),
 (477,10114,'com.liferay.portlet.journal.model.JournalArticle',2),
 (478,10114,'com.liferay.portlet.journal.model.JournalArticle',3),
 (479,10114,'com.liferay.portlet.journal.model.JournalArticle',4),
 (472,10114,'com.liferay.portlet.journal.model.JournalFeed',1),
 (473,10114,'com.liferay.portlet.journal.model.JournalFeed',2),
 (474,10114,'com.liferay.portlet.journal.model.JournalFeed',3),
 (475,10114,'com.liferay.portlet.journal.model.JournalFeed',4),
 (484,10114,'com.liferay.portlet.journal.model.JournalStructure',1),
 (485,10114,'com.liferay.portlet.journal.model.JournalStructure',2),
 (486,10114,'com.liferay.portlet.journal.model.JournalStructure',3),
 (487,10114,'com.liferay.portlet.journal.model.JournalStructure',4),
 (488,10114,'com.liferay.portlet.journal.model.JournalTemplate',1),
 (489,10114,'com.liferay.portlet.journal.model.JournalTemplate',2),
 (490,10114,'com.liferay.portlet.journal.model.JournalTemplate',3),
 (491,10114,'com.liferay.portlet.journal.model.JournalTemplate',4),
 (193,10114,'com.liferay.portlet.messageboards',1),
 (194,10114,'com.liferay.portlet.messageboards',2),
 (195,10114,'com.liferay.portlet.messageboards',3),
 (196,10114,'com.liferay.portlet.messageboards',4),
 (189,10114,'com.liferay.portlet.messageboards.model.MBCategory',1),
 (190,10114,'com.liferay.portlet.messageboards.model.MBCategory',2),
 (191,10114,'com.liferay.portlet.messageboards.model.MBCategory',3),
 (192,10114,'com.liferay.portlet.messageboards.model.MBCategory',4),
 (197,10114,'com.liferay.portlet.messageboards.model.MBMessage',1),
 (198,10114,'com.liferay.portlet.messageboards.model.MBMessage',2),
 (199,10114,'com.liferay.portlet.messageboards.model.MBMessage',3),
 (200,10114,'com.liferay.portlet.messageboards.model.MBMessage',4),
 (570,10114,'com.liferay.portlet.polls',1),
 (571,10114,'com.liferay.portlet.polls',2),
 (572,10114,'com.liferay.portlet.polls',3),
 (573,10114,'com.liferay.portlet.polls',4),
 (574,10114,'com.liferay.portlet.polls.model.PollsQuestion',1),
 (575,10114,'com.liferay.portlet.polls.model.PollsQuestion',2),
 (576,10114,'com.liferay.portlet.polls.model.PollsQuestion',3),
 (577,10114,'com.liferay.portlet.polls.model.PollsQuestion',4),
 (514,10114,'com.liferay.portlet.shopping',1),
 (515,10114,'com.liferay.portlet.shopping',2),
 (516,10114,'com.liferay.portlet.shopping',3),
 (517,10114,'com.liferay.portlet.shopping',4),
 (518,10114,'com.liferay.portlet.shopping.model.ShoppingCategory',1),
 (519,10114,'com.liferay.portlet.shopping.model.ShoppingCategory',2),
 (520,10114,'com.liferay.portlet.shopping.model.ShoppingCategory',3),
 (521,10114,'com.liferay.portlet.shopping.model.ShoppingCategory',4),
 (526,10114,'com.liferay.portlet.shopping.model.ShoppingItem',1),
 (527,10114,'com.liferay.portlet.shopping.model.ShoppingItem',2),
 (528,10114,'com.liferay.portlet.shopping.model.ShoppingItem',3),
 (529,10114,'com.liferay.portlet.shopping.model.ShoppingItem',4),
 (522,10114,'com.liferay.portlet.shopping.model.ShoppingOrder',1),
 (523,10114,'com.liferay.portlet.shopping.model.ShoppingOrder',2),
 (524,10114,'com.liferay.portlet.shopping.model.ShoppingOrder',3),
 (525,10114,'com.liferay.portlet.shopping.model.ShoppingOrder',4),
 (109,10114,'com.liferay.portlet.softwarecatalog',1),
 (110,10114,'com.liferay.portlet.softwarecatalog',2),
 (111,10114,'com.liferay.portlet.softwarecatalog',3),
 (112,10114,'com.liferay.portlet.softwarecatalog',4),
 (105,10114,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion',1),
 (106,10114,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion',2),
 (107,10114,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion',3),
 (108,10114,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion',4),
 (113,10114,'com.liferay.portlet.softwarecatalog.model.SCLicense',1),
 (114,10114,'com.liferay.portlet.softwarecatalog.model.SCLicense',2),
 (115,10114,'com.liferay.portlet.softwarecatalog.model.SCLicense',3),
 (116,10114,'com.liferay.portlet.softwarecatalog.model.SCLicense',4),
 (117,10114,'com.liferay.portlet.softwarecatalog.model.SCProductEntry',1),
 (118,10114,'com.liferay.portlet.softwarecatalog.model.SCProductEntry',2),
 (119,10114,'com.liferay.portlet.softwarecatalog.model.SCProductEntry',3),
 (120,10114,'com.liferay.portlet.softwarecatalog.model.SCProductEntry',4),
 (420,10114,'com.liferay.portlet.tags',1),
 (421,10114,'com.liferay.portlet.tags',2),
 (422,10114,'com.liferay.portlet.tags',3),
 (423,10114,'com.liferay.portlet.tags',4),
 (424,10114,'com.liferay.portlet.tags.model.TagsEntry',1),
 (425,10114,'com.liferay.portlet.tags.model.TagsEntry',2),
 (426,10114,'com.liferay.portlet.tags.model.TagsEntry',3),
 (427,10114,'com.liferay.portlet.tags.model.TagsEntry',4),
 (416,10114,'com.liferay.portlet.tags.model.TagsVocabulary',1),
 (417,10114,'com.liferay.portlet.tags.model.TagsVocabulary',2),
 (418,10114,'com.liferay.portlet.tags.model.TagsVocabulary',3),
 (419,10114,'com.liferay.portlet.tags.model.TagsVocabulary',4),
 (221,10114,'com.liferay.portlet.tasks.model.TasksProposal',1),
 (222,10114,'com.liferay.portlet.tasks.model.TasksProposal',2),
 (223,10114,'com.liferay.portlet.tasks.model.TasksProposal',3),
 (224,10114,'com.liferay.portlet.tasks.model.TasksProposal',4),
 (161,10114,'com.liferay.portlet.wiki',1),
 (162,10114,'com.liferay.portlet.wiki',2),
 (163,10114,'com.liferay.portlet.wiki',3),
 (164,10114,'com.liferay.portlet.wiki',4),
 (157,10114,'com.liferay.portlet.wiki.model.WikiNode',1),
 (158,10114,'com.liferay.portlet.wiki.model.WikiNode',2),
 (159,10114,'com.liferay.portlet.wiki.model.WikiNode',3),
 (160,10114,'com.liferay.portlet.wiki.model.WikiNode',4),
 (153,10114,'com.liferay.portlet.wiki.model.WikiPage',1),
 (154,10114,'com.liferay.portlet.wiki.model.WikiPage',2),
 (155,10114,'com.liferay.portlet.wiki.model.WikiPage',3),
 (156,10114,'com.liferay.portlet.wiki.model.WikiPage',4),
 (28,10114,'QueryParticipantByNumberPortlet_WAR_registrationportlet',1),
 (29,10114,'QueryParticipantByNumberPortlet_WAR_registrationportlet',2),
 (30,10114,'QueryParticipantByNumberPortlet_WAR_registrationportlet',3),
 (31,10114,'QueryParticipantByNumberPortlet_WAR_registrationportlet',4),
 (24,10114,'QueryRegistratedDucksPortlet_WAR_registrationportlet',1),
 (25,10114,'QueryRegistratedDucksPortlet_WAR_registrationportlet',2),
 (26,10114,'QueryRegistratedDucksPortlet_WAR_registrationportlet',3),
 (27,10114,'QueryRegistratedDucksPortlet_WAR_registrationportlet',4),
 (20,10114,'RegistrationPortlet_WAR_registrationportlet',1),
 (21,10114,'RegistrationPortlet_WAR_registrationportlet',2),
 (22,10114,'RegistrationPortlet_WAR_registrationportlet',3),
 (23,10114,'RegistrationPortlet_WAR_registrationportlet',4),
 (16,10114,'ReservationApprovalPortlet_WAR_registrationportlet',1),
 (17,10114,'ReservationApprovalPortlet_WAR_registrationportlet',2),
 (18,10114,'ReservationApprovalPortlet_WAR_registrationportlet',3),
 (19,10114,'ReservationApprovalPortlet_WAR_registrationportlet',4);
UNLOCK TABLES;
/*!40000 ALTER TABLE `resourcecode` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`resourcepermission`
--

DROP TABLE IF EXISTS `%DBNAME%`.`resourcepermission`;
CREATE TABLE  `%DBNAME%`.`resourcepermission` (
  `resourcePermissionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `primKey` varchar(255) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  `actionIds` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`resourcePermissionId`),
  UNIQUE KEY `IX_8D83D0CE` (`companyId`,`name`,`scope`,`primKey`,`roleId`),
  KEY `IX_60B99860` (`companyId`,`name`,`scope`),
  KEY `IX_2200AA69` (`companyId`,`name`,`scope`,`primKey`),
  KEY `IX_A37A0588` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`resourcepermission`
--

/*!40000 ALTER TABLE `resourcepermission` DISABLE KEYS */;
LOCK TABLES `resourcepermission` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `resourcepermission` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`role_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`role_`;
CREATE TABLE  `%DBNAME%`.`role_` (
  `roleId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `title` longtext,
  `description` longtext,
  `type_` int(11) DEFAULT NULL,
  `subtype` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `IX_A88E424E` (`companyId`,`classNameId`,`classPK`),
  UNIQUE KEY `IX_EBC931B8` (`companyId`,`name`),
  KEY `IX_449A10B9` (`companyId`),
  KEY `IX_5EB4E2FB` (`subtype`),
  KEY `IX_CBE204` (`type_`,`subtype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`role_`
--

/*!40000 ALTER TABLE `role_` DISABLE KEYS */;
LOCK TABLES `role_` WRITE;
INSERT INTO `%DBNAME%`.`role_` VALUES  (10120,10114,10035,10120,'Administrator','','Administrators are super users who can do anything.',1,''),
 (10121,10114,10035,10121,'Guest','','Unauthenticated users always have this role.',1,''),
 (10122,10114,10035,10122,'Owner','','This is an implied role with respect to the objects users create.',1,''),
 (10123,10114,10035,10123,'Power User','','Power Users have their own public and private pages.',1,''),
 (10124,10114,10035,10124,'User','','Authenticated users should be assigned this role.',1,''),
 (10125,10114,10035,10125,'Community Administrator','','Community Administrators are super users of their community but cannot make other users into Community Administrators.',2,''),
 (10126,10114,10035,10126,'Community Member','','All users who belong to a community have this role within that community.',2,''),
 (10127,10114,10035,10127,'Community Owner','','Community Owners are super users of their community and can assign community roles to users.',2,''),
 (10128,10114,10035,10128,'Organization Administrator','','Organization Administrators are super users of their organization but cannot make other users into Organization Administrators.',3,''),
 (10129,10114,10035,10129,'Organization Member','','All users who belong to a organization have this role within that organization.',3,''),
 (10130,10114,10035,10130,'Organization Owner','','Organization Owners are super users of their organization and can assign organization roles to users.',3,''),
 (10371,10114,10035,10371,'Lions Lid','','Lions Lid',1,''),
 (10372,10114,10035,10372,'Webmaster','','Webmaster',1,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `role_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`roles_permissions`
--

DROP TABLE IF EXISTS `%DBNAME%`.`roles_permissions`;
CREATE TABLE  `%DBNAME%`.`roles_permissions` (
  `roleId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `IX_7A3619C6` (`permissionId`),
  KEY `IX_E04E486D` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`roles_permissions`
--

/*!40000 ALTER TABLE `roles_permissions` DISABLE KEYS */;
LOCK TABLES `roles_permissions` WRITE;
INSERT INTO `%DBNAME%`.`roles_permissions` VALUES  (10122,2),
 (10126,2),
 (10122,3),
 (10122,4),
 (10122,5),
 (10121,6),
 (10122,6),
 (10126,6),
 (10122,12),
 (10122,13),
 (10122,14),
 (10122,15),
 (10122,16),
 (10122,101),
 (10122,102),
 (10122,103),
 (10122,104),
 (10122,105),
 (10122,106),
 (10122,107),
 (10122,108),
 (10122,109),
 (10122,110),
 (10122,111),
 (10122,112),
 (10122,113),
 (10122,114),
 (10122,115),
 (10122,116),
 (10122,117),
 (10122,118),
 (10122,119),
 (10122,120),
 (10122,121),
 (10122,122),
 (10122,123),
 (10122,124),
 (10122,125),
 (10122,126),
 (10122,127),
 (10122,128),
 (10122,129),
 (10122,130),
 (10122,131),
 (10122,132),
 (10122,133),
 (10122,134),
 (10122,135),
 (10122,136),
 (10122,137),
 (10122,138),
 (10122,139),
 (10122,140),
 (10121,296),
 (10372,296),
 (10121,297),
 (10121,298),
 (10121,299),
 (10122,300),
 (10122,301),
 (10122,302),
 (10122,303),
 (10122,304),
 (10122,305),
 (10122,306),
 (10122,307),
 (10122,308),
 (10122,309),
 (10122,310),
 (10122,311),
 (10122,312),
 (10122,313),
 (10372,314),
 (10372,315),
 (10372,316),
 (10372,317),
 (10372,318),
 (10372,319),
 (10372,320),
 (10372,321),
 (10372,322),
 (10372,323),
 (10372,324),
 (10372,325),
 (10372,326),
 (10372,327),
 (10372,328),
 (10372,329),
 (10372,330),
 (10372,331),
 (10372,332),
 (10372,333),
 (10372,334),
 (10372,335),
 (10372,336),
 (10372,337),
 (10372,338),
 (10372,339),
 (10372,340),
 (10372,341),
 (10372,342),
 (10372,343),
 (10372,344),
 (10372,345),
 (10372,346),
 (10372,347),
 (10372,348),
 (10372,349),
 (10122,350),
 (10122,351),
 (10122,352),
 (10121,353),
 (10122,353),
 (10126,353),
 (10122,354),
 (10122,355),
 (10122,356),
 (10121,357),
 (10122,357),
 (10126,357),
 (10122,358),
 (10122,359),
 (10122,360),
 (10121,361),
 (10122,361),
 (10126,361),
 (10122,362),
 (10122,363),
 (10122,364),
 (10121,365),
 (10122,365),
 (10126,365),
 (10122,366),
 (10122,367),
 (10122,368),
 (10121,369),
 (10122,369),
 (10126,369),
 (10122,370),
 (10122,371),
 (10122,372),
 (10121,373),
 (10122,373),
 (10126,373),
 (10122,374),
 (10122,375),
 (10122,376),
 (10121,377),
 (10122,377),
 (10126,377),
 (10122,378),
 (10122,379),
 (10122,380),
 (10121,381),
 (10122,381),
 (10126,381),
 (10122,382),
 (10122,383),
 (10122,384),
 (10121,385),
 (10122,385),
 (10126,385),
 (10122,386),
 (10122,387),
 (10122,388),
 (10121,389),
 (10122,389),
 (10126,389),
 (10122,390),
 (10122,391),
 (10122,392),
 (10122,393),
 (10122,394),
 (10122,395),
 (10122,396),
 (10122,397),
 (10122,398),
 (10122,399),
 (10122,400),
 (10122,401),
 (10122,402),
 (10122,403),
 (10122,404),
 (10122,405),
 (10122,406),
 (10122,407),
 (10122,408),
 (10122,409),
 (10122,410),
 (10122,411),
 (10122,412),
 (10122,413),
 (10122,414),
 (10122,415),
 (10122,416),
 (10122,417),
 (10122,418),
 (10122,419),
 (10122,420),
 (10122,421),
 (10122,422),
 (10122,423),
 (10122,424),
 (10122,425),
 (10122,426),
 (10122,427),
 (10122,428),
 (10122,429),
 (10122,430),
 (10122,431),
 (10122,432),
 (10122,433),
 (10122,434),
 (10122,435),
 (10122,436),
 (10122,437),
 (10122,438),
 (10122,439),
 (10122,440),
 (10122,441),
 (10122,442),
 (10122,443),
 (10122,444),
 (10122,445),
 (10122,446),
 (10122,447),
 (10122,448),
 (10122,449),
 (10122,450),
 (10122,451),
 (10122,452),
 (10122,453),
 (10122,454),
 (10122,455),
 (10122,456),
 (10122,457),
 (10122,458),
 (10122,459),
 (10122,460),
 (10122,461),
 (10122,462),
 (10122,463),
 (10122,464),
 (10122,465),
 (10122,466),
 (10122,467),
 (10122,468),
 (10122,469),
 (10122,470),
 (10122,471),
 (10122,472),
 (10122,473),
 (10122,474),
 (10122,475),
 (10122,476),
 (10122,477),
 (10122,478),
 (10122,479),
 (10122,480),
 (10122,481),
 (10122,482),
 (10122,483),
 (10122,484),
 (10122,485),
 (10122,486),
 (10122,487),
 (10122,488),
 (10122,489),
 (10122,490),
 (10122,491),
 (10122,492),
 (10122,493),
 (10122,494),
 (10122,495),
 (10122,496),
 (10122,497),
 (10122,498),
 (10122,499),
 (10122,500),
 (10122,501),
 (10122,502),
 (10122,503),
 (10122,504),
 (10122,505),
 (10122,506),
 (10122,507),
 (10122,508),
 (10122,509),
 (10122,510),
 (10122,511),
 (10122,512),
 (10122,513),
 (10122,514),
 (10122,515),
 (10122,516),
 (10122,517),
 (10122,518),
 (10122,519),
 (10122,520),
 (10122,521),
 (10122,522),
 (10122,523),
 (10122,524),
 (10122,525),
 (10122,526),
 (10122,527),
 (10122,528),
 (10122,529),
 (10122,530),
 (10122,531),
 (10122,532),
 (10122,533),
 (10122,534),
 (10122,535),
 (10122,536),
 (10122,537),
 (10122,538),
 (10122,539),
 (10122,540),
 (10122,541),
 (10122,542),
 (10122,543),
 (10122,544),
 (10122,545),
 (10122,546),
 (10122,547),
 (10122,548),
 (10122,549),
 (10122,550),
 (10122,551),
 (10122,552),
 (10122,553),
 (10122,554),
 (10122,555),
 (10122,556),
 (10122,557),
 (10122,558),
 (10122,559),
 (10122,560),
 (10122,561),
 (10122,562),
 (10122,563),
 (10122,564),
 (10122,565),
 (10122,566),
 (10122,567),
 (10122,568),
 (10122,569),
 (10122,570),
 (10122,571),
 (10122,572),
 (10122,573),
 (10122,574),
 (10122,575),
 (10122,576),
 (10122,577),
 (10122,578),
 (10122,579),
 (10122,580),
 (10122,581),
 (10122,582),
 (10122,583),
 (10122,584),
 (10122,585),
 (10122,586),
 (10122,587),
 (10122,588),
 (10122,589),
 (10122,590),
 (10122,591),
 (10122,592),
 (10122,593),
 (10122,594),
 (10122,595),
 (10122,596),
 (10122,597),
 (10122,598),
 (10122,599),
 (10122,600),
 (10122,601),
 (10122,602),
 (10122,603),
 (10122,604),
 (10122,605),
 (10122,606),
 (10122,607),
 (10122,608),
 (10122,609),
 (10122,610),
 (10122,611),
 (10122,612),
 (10122,613),
 (10122,701),
 (10126,701),
 (10122,702),
 (10122,703),
 (10122,704),
 (10121,705),
 (10122,705),
 (10126,705),
 (10122,706),
 (10126,706),
 (10122,707),
 (10122,708),
 (10122,709),
 (10121,710),
 (10122,710),
 (10126,710),
 (10122,711),
 (10126,711),
 (10122,712),
 (10122,713),
 (10122,714),
 (10121,715),
 (10122,715),
 (10126,715),
 (10122,716),
 (10126,716),
 (10122,717),
 (10122,718),
 (10122,719),
 (10121,720),
 (10122,720),
 (10126,720),
 (10122,721),
 (10126,721),
 (10122,722),
 (10122,723),
 (10122,724),
 (10121,725),
 (10122,725),
 (10126,725),
 (10122,726),
 (10126,726),
 (10122,727),
 (10122,728),
 (10122,729),
 (10121,730),
 (10122,730),
 (10126,730),
 (10122,731),
 (10126,731),
 (10122,732),
 (10122,733),
 (10122,734),
 (10121,735),
 (10122,735),
 (10126,735),
 (10122,736),
 (10126,736),
 (10122,737),
 (10122,738),
 (10122,739),
 (10121,740),
 (10122,740),
 (10126,740),
 (10122,741),
 (10126,741),
 (10122,742),
 (10122,743),
 (10122,744),
 (10121,745),
 (10122,745),
 (10126,745),
 (10122,746),
 (10126,746),
 (10122,747),
 (10122,748),
 (10122,749),
 (10121,750),
 (10122,750),
 (10126,750),
 (10122,751),
 (10126,751),
 (10122,752),
 (10122,753),
 (10122,754),
 (10121,755),
 (10122,755),
 (10126,755),
 (10122,756),
 (10126,756),
 (10122,757),
 (10122,758),
 (10122,759),
 (10121,760),
 (10122,760),
 (10126,760),
 (10122,761),
 (10126,761),
 (10122,762),
 (10122,763),
 (10122,764),
 (10121,765),
 (10122,765),
 (10126,765),
 (10122,766),
 (10126,766),
 (10122,767),
 (10122,768),
 (10122,769),
 (10121,770),
 (10122,770),
 (10126,770),
 (10122,771),
 (10126,771),
 (10122,772),
 (10122,773),
 (10122,774),
 (10121,775),
 (10122,775),
 (10126,775),
 (10122,776),
 (10126,776),
 (10122,777),
 (10122,778),
 (10122,779),
 (10121,780),
 (10122,780),
 (10126,780),
 (10122,781),
 (10126,781),
 (10122,782),
 (10122,783),
 (10122,784),
 (10121,785),
 (10122,785),
 (10126,785),
 (10122,786),
 (10126,786),
 (10122,787),
 (10122,788),
 (10122,789),
 (10121,790),
 (10122,790),
 (10126,790),
 (10122,791),
 (10126,791),
 (10122,792),
 (10122,793),
 (10122,794),
 (10121,795),
 (10122,795),
 (10126,795),
 (10122,796),
 (10126,796),
 (10122,797),
 (10122,798),
 (10122,799),
 (10121,800),
 (10122,800),
 (10126,800),
 (10122,801),
 (10126,801),
 (10122,802),
 (10122,803),
 (10122,804),
 (10121,805),
 (10122,805),
 (10126,805),
 (10122,806),
 (10126,806),
 (10122,807),
 (10122,808),
 (10122,809),
 (10121,810),
 (10122,810),
 (10126,810),
 (10122,811),
 (10126,811),
 (10122,812),
 (10122,813),
 (10122,814),
 (10121,815),
 (10122,815),
 (10126,815),
 (10122,816),
 (10126,816),
 (10122,817),
 (10122,818),
 (10122,819),
 (10121,820),
 (10122,820),
 (10126,820),
 (10122,821),
 (10126,821),
 (10122,822),
 (10122,823),
 (10122,824),
 (10121,825),
 (10122,825),
 (10126,825),
 (10122,826),
 (10126,826),
 (10122,827),
 (10122,828),
 (10122,829),
 (10121,830),
 (10122,830),
 (10126,830),
 (10122,831),
 (10126,831),
 (10122,832),
 (10122,833),
 (10122,834),
 (10121,835),
 (10122,835),
 (10126,835),
 (10122,836),
 (10126,836),
 (10122,837),
 (10122,838),
 (10122,839),
 (10121,840),
 (10122,840),
 (10126,840),
 (10122,841),
 (10126,841),
 (10122,842),
 (10122,843),
 (10122,844),
 (10121,845),
 (10122,845),
 (10126,845),
 (10122,846),
 (10126,846),
 (10122,847),
 (10122,848),
 (10122,849),
 (10121,850),
 (10122,850),
 (10126,850),
 (10122,851),
 (10126,851),
 (10122,852),
 (10122,853),
 (10122,854),
 (10122,855),
 (10124,855),
 (10126,855),
 (10121,856),
 (10122,856),
 (10126,856),
 (10122,857),
 (10122,858),
 (10121,859),
 (10122,859),
 (10126,859);
UNLOCK TABLES;
/*!40000 ALTER TABLE `roles_permissions` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`scframeworkversi_scproductvers`
--

DROP TABLE IF EXISTS `%DBNAME%`.`scframeworkversi_scproductvers`;
CREATE TABLE  `%DBNAME%`.`scframeworkversi_scproductvers` (
  `frameworkVersionId` bigint(20) NOT NULL,
  `productVersionId` bigint(20) NOT NULL,
  PRIMARY KEY (`frameworkVersionId`,`productVersionId`),
  KEY `IX_3BB93ECA` (`frameworkVersionId`),
  KEY `IX_E8D33FF9` (`productVersionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`scframeworkversi_scproductvers`
--

/*!40000 ALTER TABLE `scframeworkversi_scproductvers` DISABLE KEYS */;
LOCK TABLES `scframeworkversi_scproductvers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `scframeworkversi_scproductvers` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`scframeworkversion`
--

DROP TABLE IF EXISTS `%DBNAME%`.`scframeworkversion`;
CREATE TABLE  `%DBNAME%`.`scframeworkversion` (
  `frameworkVersionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `url` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`frameworkVersionId`),
  KEY `IX_C98C0D78` (`companyId`),
  KEY `IX_272991FA` (`groupId`),
  KEY `IX_6E1764F` (`groupId`,`active_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`scframeworkversion`
--

/*!40000 ALTER TABLE `scframeworkversion` DISABLE KEYS */;
LOCK TABLES `scframeworkversion` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `scframeworkversion` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`sclicense`
--

DROP TABLE IF EXISTS `%DBNAME%`.`sclicense`;
CREATE TABLE  `%DBNAME%`.`sclicense` (
  `licenseId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `url` longtext,
  `openSource` tinyint(4) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  `recommended` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`licenseId`),
  KEY `IX_1C841592` (`active_`),
  KEY `IX_5327BB79` (`active_`,`recommended`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`sclicense`
--

/*!40000 ALTER TABLE `sclicense` DISABLE KEYS */;
LOCK TABLES `sclicense` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sclicense` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`sclicenses_scproductentries`
--

DROP TABLE IF EXISTS `%DBNAME%`.`sclicenses_scproductentries`;
CREATE TABLE  `%DBNAME%`.`sclicenses_scproductentries` (
  `licenseId` bigint(20) NOT NULL,
  `productEntryId` bigint(20) NOT NULL,
  PRIMARY KEY (`licenseId`,`productEntryId`),
  KEY `IX_27006638` (`licenseId`),
  KEY `IX_D7710A66` (`productEntryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`sclicenses_scproductentries`
--

/*!40000 ALTER TABLE `sclicenses_scproductentries` DISABLE KEYS */;
LOCK TABLES `sclicenses_scproductentries` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sclicenses_scproductentries` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`scproductentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`scproductentry`;
CREATE TABLE  `%DBNAME%`.`scproductentry` (
  `productEntryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `shortDescription` longtext,
  `longDescription` longtext,
  `pageURL` longtext,
  `author` varchar(75) DEFAULT NULL,
  `repoGroupId` varchar(75) DEFAULT NULL,
  `repoArtifactId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`productEntryId`),
  KEY `IX_5D25244F` (`companyId`),
  KEY `IX_72F87291` (`groupId`),
  KEY `IX_98E6A9CB` (`groupId`,`userId`),
  KEY `IX_7311E812` (`repoGroupId`,`repoArtifactId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`scproductentry`
--

/*!40000 ALTER TABLE `scproductentry` DISABLE KEYS */;
LOCK TABLES `scproductentry` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `scproductentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`scproductscreenshot`
--

DROP TABLE IF EXISTS `%DBNAME%`.`scproductscreenshot`;
CREATE TABLE  `%DBNAME%`.`scproductscreenshot` (
  `productScreenshotId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `productEntryId` bigint(20) DEFAULT NULL,
  `thumbnailId` bigint(20) DEFAULT NULL,
  `fullImageId` bigint(20) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`productScreenshotId`),
  KEY `IX_AE8224CC` (`fullImageId`),
  KEY `IX_467956FD` (`productEntryId`),
  KEY `IX_DA913A55` (`productEntryId`,`priority`),
  KEY `IX_6C572DAC` (`thumbnailId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`scproductscreenshot`
--

/*!40000 ALTER TABLE `scproductscreenshot` DISABLE KEYS */;
LOCK TABLES `scproductscreenshot` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `scproductscreenshot` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`scproductversion`
--

DROP TABLE IF EXISTS `%DBNAME%`.`scproductversion`;
CREATE TABLE  `%DBNAME%`.`scproductversion` (
  `productVersionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `productEntryId` bigint(20) DEFAULT NULL,
  `version` varchar(75) DEFAULT NULL,
  `changeLog` longtext,
  `downloadPageURL` longtext,
  `directDownloadURL` varchar(2000) DEFAULT NULL,
  `repoStoreArtifact` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`productVersionId`),
  KEY `IX_7020130F` (`directDownloadURL`(767)),
  KEY `IX_8377A211` (`productEntryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`scproductversion`
--

/*!40000 ALTER TABLE `scproductversion` DISABLE KEYS */;
LOCK TABLES `scproductversion` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `scproductversion` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`servicecomponent`
--

DROP TABLE IF EXISTS `%DBNAME%`.`servicecomponent`;
CREATE TABLE  `%DBNAME%`.`servicecomponent` (
  `serviceComponentId` bigint(20) NOT NULL,
  `buildNamespace` varchar(75) DEFAULT NULL,
  `buildNumber` bigint(20) DEFAULT NULL,
  `buildDate` bigint(20) DEFAULT NULL,
  `data_` longtext,
  PRIMARY KEY (`serviceComponentId`),
  UNIQUE KEY `IX_4F0315B8` (`buildNamespace`,`buildNumber`),
  KEY `IX_7338606F` (`buildNamespace`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`servicecomponent`
--

/*!40000 ALTER TABLE `servicecomponent` DISABLE KEYS */;
LOCK TABLES `servicecomponent` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `servicecomponent` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shard`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shard`;
CREATE TABLE  `%DBNAME%`.`shard` (
  `shardId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`shardId`),
  KEY `IX_DA5F4359` (`classNameId`,`classPK`),
  KEY `IX_941BA8C3` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shard`
--

/*!40000 ALTER TABLE `shard` DISABLE KEYS */;
LOCK TABLES `shard` WRITE;
INSERT INTO `%DBNAME%`.`shard` VALUES  (10115,10005,10114,'default');
UNLOCK TABLES;
/*!40000 ALTER TABLE `shard` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingcart`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingcart`;
CREATE TABLE  `%DBNAME%`.`shoppingcart` (
  `cartId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `itemIds` longtext,
  `couponCodes` varchar(75) DEFAULT NULL,
  `altShipping` int(11) DEFAULT NULL,
  `insure` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`cartId`),
  UNIQUE KEY `IX_FC46FE16` (`groupId`,`userId`),
  KEY `IX_C28B41DC` (`groupId`),
  KEY `IX_54101CC8` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingcart`
--

/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
LOCK TABLES `shoppingcart` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingcategory`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingcategory`;
CREATE TABLE  `%DBNAME%`.`shoppingcategory` (
  `categoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentCategoryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`categoryId`),
  KEY `IX_5F615D3E` (`groupId`),
  KEY `IX_1E6464F5` (`groupId`,`parentCategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingcategory`
--

/*!40000 ALTER TABLE `shoppingcategory` DISABLE KEYS */;
LOCK TABLES `shoppingcategory` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingcategory` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingcoupon`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingcoupon`;
CREATE TABLE  `%DBNAME%`.`shoppingcoupon` (
  `couponId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `code_` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  `limitCategories` longtext,
  `limitSkus` longtext,
  `minOrder` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `discountType` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`couponId`),
  UNIQUE KEY `IX_DC60CFAE` (`code_`),
  KEY `IX_3251AF16` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingcoupon`
--

/*!40000 ALTER TABLE `shoppingcoupon` DISABLE KEYS */;
LOCK TABLES `shoppingcoupon` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingcoupon` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingitem`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingitem`;
CREATE TABLE  `%DBNAME%`.`shoppingitem` (
  `itemId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `sku` varchar(75) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` longtext,
  `properties` longtext,
  `fields_` tinyint(4) DEFAULT NULL,
  `fieldsQuantities` longtext,
  `minQuantity` int(11) DEFAULT NULL,
  `maxQuantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `taxable` tinyint(4) DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `useShippingFormula` tinyint(4) DEFAULT NULL,
  `requiresShipping` tinyint(4) DEFAULT NULL,
  `stockQuantity` int(11) DEFAULT NULL,
  `featured_` tinyint(4) DEFAULT NULL,
  `sale_` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` varchar(75) DEFAULT NULL,
  `mediumImage` tinyint(4) DEFAULT NULL,
  `mediumImageId` bigint(20) DEFAULT NULL,
  `mediumImageURL` varchar(75) DEFAULT NULL,
  `largeImage` tinyint(4) DEFAULT NULL,
  `largeImageId` bigint(20) DEFAULT NULL,
  `largeImageURL` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  UNIQUE KEY `IX_1C717CA6` (`companyId`,`sku`),
  KEY `IX_C8EACF2E` (`categoryId`),
  KEY `IX_903DC750` (`largeImageId`),
  KEY `IX_D217AB30` (`mediumImageId`),
  KEY `IX_FF203304` (`smallImageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingitem`
--

/*!40000 ALTER TABLE `shoppingitem` DISABLE KEYS */;
LOCK TABLES `shoppingitem` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingitem` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingitemfield`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingitemfield`;
CREATE TABLE  `%DBNAME%`.`shoppingitemfield` (
  `itemFieldId` bigint(20) NOT NULL,
  `itemId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `values_` longtext,
  `description` longtext,
  PRIMARY KEY (`itemFieldId`),
  KEY `IX_6D5F9B87` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingitemfield`
--

/*!40000 ALTER TABLE `shoppingitemfield` DISABLE KEYS */;
LOCK TABLES `shoppingitemfield` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingitemfield` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingitemprice`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingitemprice`;
CREATE TABLE  `%DBNAME%`.`shoppingitemprice` (
  `itemPriceId` bigint(20) NOT NULL,
  `itemId` bigint(20) DEFAULT NULL,
  `minQuantity` int(11) DEFAULT NULL,
  `maxQuantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `taxable` tinyint(4) DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `useShippingFormula` tinyint(4) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemPriceId`),
  KEY `IX_EA6FD516` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingitemprice`
--

/*!40000 ALTER TABLE `shoppingitemprice` DISABLE KEYS */;
LOCK TABLES `shoppingitemprice` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingitemprice` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingorder`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingorder`;
CREATE TABLE  `%DBNAME%`.`shoppingorder` (
  `orderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `altShipping` varchar(75) DEFAULT NULL,
  `requiresShipping` tinyint(4) DEFAULT NULL,
  `insure` tinyint(4) DEFAULT NULL,
  `insurance` double DEFAULT NULL,
  `couponCodes` varchar(75) DEFAULT NULL,
  `couponDiscount` double DEFAULT NULL,
  `billingFirstName` varchar(75) DEFAULT NULL,
  `billingLastName` varchar(75) DEFAULT NULL,
  `billingEmailAddress` varchar(75) DEFAULT NULL,
  `billingCompany` varchar(75) DEFAULT NULL,
  `billingStreet` varchar(75) DEFAULT NULL,
  `billingCity` varchar(75) DEFAULT NULL,
  `billingState` varchar(75) DEFAULT NULL,
  `billingZip` varchar(75) DEFAULT NULL,
  `billingCountry` varchar(75) DEFAULT NULL,
  `billingPhone` varchar(75) DEFAULT NULL,
  `shipToBilling` tinyint(4) DEFAULT NULL,
  `shippingFirstName` varchar(75) DEFAULT NULL,
  `shippingLastName` varchar(75) DEFAULT NULL,
  `shippingEmailAddress` varchar(75) DEFAULT NULL,
  `shippingCompany` varchar(75) DEFAULT NULL,
  `shippingStreet` varchar(75) DEFAULT NULL,
  `shippingCity` varchar(75) DEFAULT NULL,
  `shippingState` varchar(75) DEFAULT NULL,
  `shippingZip` varchar(75) DEFAULT NULL,
  `shippingCountry` varchar(75) DEFAULT NULL,
  `shippingPhone` varchar(75) DEFAULT NULL,
  `ccName` varchar(75) DEFAULT NULL,
  `ccType` varchar(75) DEFAULT NULL,
  `ccNumber` varchar(75) DEFAULT NULL,
  `ccExpMonth` int(11) DEFAULT NULL,
  `ccExpYear` int(11) DEFAULT NULL,
  `ccVerNumber` varchar(75) DEFAULT NULL,
  `comments` longtext,
  `ppTxnId` varchar(75) DEFAULT NULL,
  `ppPaymentStatus` varchar(75) DEFAULT NULL,
  `ppPaymentGross` double DEFAULT NULL,
  `ppReceiverEmail` varchar(75) DEFAULT NULL,
  `ppPayerEmail` varchar(75) DEFAULT NULL,
  `sendOrderEmail` tinyint(4) DEFAULT NULL,
  `sendShippingEmail` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `IX_D7D6E87A` (`number_`),
  KEY `IX_1D15553E` (`groupId`),
  KEY `IX_119B5630` (`groupId`,`userId`,`ppPaymentStatus`),
  KEY `IX_F474FD89` (`ppTxnId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingorder`
--

/*!40000 ALTER TABLE `shoppingorder` DISABLE KEYS */;
LOCK TABLES `shoppingorder` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingorder` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`shoppingorderitem`
--

DROP TABLE IF EXISTS `%DBNAME%`.`shoppingorderitem`;
CREATE TABLE  `%DBNAME%`.`shoppingorderitem` (
  `orderItemId` bigint(20) NOT NULL,
  `orderId` bigint(20) DEFAULT NULL,
  `itemId` varchar(75) DEFAULT NULL,
  `sku` varchar(75) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` longtext,
  `properties` longtext,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `shippedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`orderItemId`),
  KEY `IX_B5F82C7A` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`shoppingorderitem`
--

/*!40000 ALTER TABLE `shoppingorderitem` DISABLE KEYS */;
LOCK TABLES `shoppingorderitem` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shoppingorderitem` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`socialactivity`
--

DROP TABLE IF EXISTS `%DBNAME%`.`socialactivity`;
CREATE TABLE  `%DBNAME%`.`socialactivity` (
  `activityId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `mirrorActivityId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `extraData` longtext,
  `receiverUserId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`activityId`),
  UNIQUE KEY `IX_8F32DEC9` (`groupId`,`userId`,`createDate`,`classNameId`,`classPK`,`type_`,`receiverUserId`),
  KEY `IX_82E39A0C` (`classNameId`),
  KEY `IX_A853C757` (`classNameId`,`classPK`),
  KEY `IX_64B1BC66` (`companyId`),
  KEY `IX_2A2468` (`groupId`),
  KEY `IX_1271F25F` (`mirrorActivityId`),
  KEY `IX_1F00C374` (`mirrorActivityId`,`classNameId`,`classPK`),
  KEY `IX_121CA3CB` (`receiverUserId`),
  KEY `IX_3504B8BC` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`socialactivity`
--

/*!40000 ALTER TABLE `socialactivity` DISABLE KEYS */;
LOCK TABLES `socialactivity` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `socialactivity` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`socialrelation`
--

DROP TABLE IF EXISTS `%DBNAME%`.`socialrelation`;
CREATE TABLE  `%DBNAME%`.`socialrelation` (
  `uuid_` varchar(75) DEFAULT NULL,
  `relationId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `userId1` bigint(20) DEFAULT NULL,
  `userId2` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  PRIMARY KEY (`relationId`),
  UNIQUE KEY `IX_12A92145` (`userId1`,`userId2`,`type_`),
  KEY `IX_61171E99` (`companyId`),
  KEY `IX_95135D1C` (`companyId`,`type_`),
  KEY `IX_C31A64C6` (`type_`),
  KEY `IX_5A40CDCC` (`userId1`),
  KEY `IX_4B52BE89` (`userId1`,`type_`),
  KEY `IX_5A40D18D` (`userId2`),
  KEY `IX_3F9C2FA8` (`userId2`,`type_`),
  KEY `IX_F0CA24A5` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`socialrelation`
--

/*!40000 ALTER TABLE `socialrelation` DISABLE KEYS */;
LOCK TABLES `socialrelation` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `socialrelation` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`socialrequest`
--

DROP TABLE IF EXISTS `%DBNAME%`.`socialrequest`;
CREATE TABLE  `%DBNAME%`.`socialrequest` (
  `uuid_` varchar(75) DEFAULT NULL,
  `requestId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `modifiedDate` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `extraData` longtext,
  `receiverUserId` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`requestId`),
  UNIQUE KEY `IX_36A90CA7` (`userId`,`classNameId`,`classPK`,`type_`,`receiverUserId`),
  UNIQUE KEY `IX_4F973EFE` (`uuid_`,`groupId`),
  KEY `IX_D3425487` (`classNameId`,`classPK`,`type_`,`receiverUserId`,`status`),
  KEY `IX_A90FE5A0` (`companyId`),
  KEY `IX_32292ED1` (`receiverUserId`),
  KEY `IX_D9380CB7` (`receiverUserId`,`status`),
  KEY `IX_80F7A9C2` (`userId`),
  KEY `IX_CC86A444` (`userId`,`classNameId`,`classPK`,`type_`,`status`),
  KEY `IX_AB5906A8` (`userId`,`status`),
  KEY `IX_49D5872C` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`socialrequest`
--

/*!40000 ALTER TABLE `socialrequest` DISABLE KEYS */;
LOCK TABLES `socialrequest` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `socialrequest` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`subscription`
--

DROP TABLE IF EXISTS `%DBNAME%`.`subscription`;
CREATE TABLE  `%DBNAME%`.`subscription` (
  `subscriptionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `frequency` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`subscriptionId`),
  UNIQUE KEY `IX_2E1A92D4` (`companyId`,`userId`,`classNameId`,`classPK`),
  KEY `IX_786D171A` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_54243AFD` (`userId`),
  KEY `IX_E8F34171` (`userId`,`classNameId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`subscription`
--

/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
LOCK TABLES `subscription` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagsasset`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagsasset`;
CREATE TABLE  `%DBNAME%`.`tagsasset` (
  `assetId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `visible` tinyint(4) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `publishDate` datetime DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `mimeType` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `summary` longtext,
  `url` longtext,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `priority` double DEFAULT NULL,
  `viewCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`assetId`),
  UNIQUE KEY `IX_1AB6D6D2` (`classNameId`,`classPK`),
  KEY `IX_AB3D8BCB` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagsasset`
--

/*!40000 ALTER TABLE `tagsasset` DISABLE KEYS */;
LOCK TABLES `tagsasset` WRITE;
INSERT INTO `%DBNAME%`.`tagsasset` VALUES  (10151,0,10114,10146,'Test Test','2010-08-03 09:47:11','2010-08-03 09:47:11',10039,10146,1,NULL,NULL,NULL,NULL,'','Test Test','','','',0,0,0,0),
 (10394,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10389,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','HOME','','','',0,0,0,4),
 (10401,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10396,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','UNDER_CONSTRUCTION','','','',0,0,0,0),
 (10408,10138,10114,10146,'Test Test','2010-08-03 10:15:48','2010-08-03 10:15:48',10067,10403,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORGANISATOREN','','','',0,0,0,0),
 (10415,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10410,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORG_LIONS_BREUGHEL','','','',0,0,0,0),
 (10422,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10417,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORG_LIONS_INTERNATIONAL','','','',0,0,0,0),
 (10429,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10424,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORG_HART_VOOR_LIMBURG','','','',0,0,0,0),
 (10436,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10431,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORG_HBVL','','','',0,0,0,0),
 (10443,10138,10114,10146,'Test Test','2010-08-03 10:15:49','2010-08-03 10:15:49',10067,10438,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','ORG_TVL','','','',0,0,0,0),
 (10450,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10445,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO','','','',0,0,0,0),
 (10457,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10452,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO_EVENEMENT','','','',0,0,0,0),
 (10464,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10459,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO_REGLEMENT','','','',0,0,0,0),
 (10471,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10466,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO_RACEVERLOOP','','','',0,0,0,0),
 (10478,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10473,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO_HOOFDPRIJS','','','',0,0,0,0),
 (10485,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10480,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','INFO_OPBRENGST','','','',0,0,0,0),
 (10492,10138,10114,10146,'Test Test','2010-08-03 10:15:50','2010-08-03 10:15:50',10067,10487,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','LOGOS','','','',0,0,0,0),
 (10499,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10494,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','DUCKTYPE_MAIN_SPONSORS','','','',0,0,0,0),
 (10506,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10501,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','DUCKTYPE_100_DUCKS','','','',0,0,0,0),
 (10513,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10508,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','DUCKTYPE_50_DUCKS','','','',0,0,0,0),
 (10520,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10515,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','DUCKTYPE_20_DUCKS','','','',0,0,0,0),
 (10527,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10522,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','DUCKTYPE_10_DUCKS','','','',0,0,0,0),
 (10534,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10529,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','FOTO_ALBUM','','','',0,0,0,0),
 (10541,10138,10114,10146,'Test Test','2010-08-03 10:15:51','2010-08-03 10:15:51',10067,10536,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','PERSCONFERENTIES','','','',0,0,0,0),
 (10548,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10543,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','REACTIES','','','',0,0,0,0),
 (10555,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10550,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','EVENT','','','',0,0,0,0),
 (10562,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10557,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','EVENT_PROGRAMMA','','','',0,0,0,0),
 (10569,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10564,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','EVENT_ACTS','','','',0,0,0,0),
 (10576,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10571,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','EVENT_BEVERAGE','','','',0,0,0,0),
 (10583,10138,10114,10146,'Test Test','2010-08-03 10:15:52','2010-08-03 10:15:52',10067,10578,1,NULL,NULL,'1900-01-01 12:00:00',NULL,'text/html','EVENT_BIJSTAND','','','',0,0,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagsasset` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagsassets_tagsentries`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagsassets_tagsentries`;
CREATE TABLE  `%DBNAME%`.`tagsassets_tagsentries` (
  `assetId` bigint(20) NOT NULL,
  `entryId` bigint(20) NOT NULL,
  PRIMARY KEY (`assetId`,`entryId`),
  KEY `IX_B22F3A1` (`assetId`),
  KEY `IX_A02A8023` (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagsassets_tagsentries`
--

/*!40000 ALTER TABLE `tagsassets_tagsentries` DISABLE KEYS */;
LOCK TABLES `tagsassets_tagsentries` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagsassets_tagsentries` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagsentry`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagsentry`;
CREATE TABLE  `%DBNAME%`.`tagsentry` (
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentEntryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `vocabularyId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  KEY `IX_EE55ED49` (`parentEntryId`,`vocabularyId`),
  KEY `IX_28E8954` (`vocabularyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagsentry`
--

/*!40000 ALTER TABLE `tagsentry` DISABLE KEYS */;
LOCK TABLES `tagsentry` WRITE;
INSERT INTO `%DBNAME%`.`tagsentry` VALUES  (10207,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'main-sponsor',10206),
 (10208,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'sponsor-4-stars',10206),
 (10209,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'sponsor-3-stars',10206),
 (10210,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'sponsor-2-stars',10206),
 (10211,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'sponsor-1-star',10206),
 (10212,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23',0,'merchant',10206);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagsentry` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagsproperty`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagsproperty`;
CREATE TABLE  `%DBNAME%`.`tagsproperty` (
  `propertyId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `entryId` bigint(20) DEFAULT NULL,
  `key_` varchar(75) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`propertyId`),
  UNIQUE KEY `IX_F505253D` (`entryId`,`key_`),
  KEY `IX_C134234` (`companyId`),
  KEY `IX_EB974D08` (`companyId`,`key_`),
  KEY `IX_5200A629` (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagsproperty`
--

/*!40000 ALTER TABLE `tagsproperty` DISABLE KEYS */;
LOCK TABLES `tagsproperty` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagsproperty` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagssource`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagssource`;
CREATE TABLE  `%DBNAME%`.`tagssource` (
  `sourceId` bigint(20) NOT NULL,
  `parentSourceId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `acronym` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`sourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagssource`
--

/*!40000 ALTER TABLE `tagssource` DISABLE KEYS */;
LOCK TABLES `tagssource` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagssource` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tagsvocabulary`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tagsvocabulary`;
CREATE TABLE  `%DBNAME%`.`tagsvocabulary` (
  `vocabularyId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` varchar(75) DEFAULT NULL,
  `folksonomy` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`vocabularyId`),
  UNIQUE KEY `IX_F9E51044` (`groupId`,`name`),
  KEY `IX_E0D51848` (`companyId`,`folksonomy`),
  KEY `IX_9F26308A` (`groupId`,`folksonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tagsvocabulary`
--

/*!40000 ALTER TABLE `tagsvocabulary` DISABLE KEYS */;
LOCK TABLES `tagsvocabulary` WRITE;
INSERT INTO `%DBNAME%`.`tagsvocabulary` VALUES  (10206,10138,10114,10117,'','2010-08-03 10:15:23','2010-08-03 10:15:23','sponsor','',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tagsvocabulary` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tasksproposal`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tasksproposal`;
CREATE TABLE  `%DBNAME%`.`tasksproposal` (
  `proposalId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `publishDate` datetime DEFAULT NULL,
  `dueDate` datetime DEFAULT NULL,
  PRIMARY KEY (`proposalId`),
  UNIQUE KEY `IX_181A4A1B` (`classNameId`,`classPK`),
  KEY `IX_7FB27324` (`groupId`),
  KEY `IX_6EEC675E` (`groupId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tasksproposal`
--

/*!40000 ALTER TABLE `tasksproposal` DISABLE KEYS */;
LOCK TABLES `tasksproposal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tasksproposal` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`tasksreview`
--

DROP TABLE IF EXISTS `%DBNAME%`.`tasksreview`;
CREATE TABLE  `%DBNAME%`.`tasksreview` (
  `reviewId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `proposalId` bigint(20) DEFAULT NULL,
  `assignedByUserId` bigint(20) DEFAULT NULL,
  `assignedByUserName` varchar(75) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  `completed` tinyint(4) DEFAULT NULL,
  `rejected` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`reviewId`),
  UNIQUE KEY `IX_5C6BE4C7` (`userId`,`proposalId`),
  KEY `IX_4D0C7F8D` (`proposalId`),
  KEY `IX_70AFEA01` (`proposalId`,`stage`),
  KEY `IX_1894B29A` (`proposalId`,`stage`,`completed`),
  KEY `IX_41AFC20C` (`proposalId`,`stage`,`completed`,`rejected`),
  KEY `IX_36F512E6` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`tasksreview`
--

/*!40000 ALTER TABLE `tasksreview` DISABLE KEYS */;
LOCK TABLES `tasksreview` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tasksreview` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`user_`
--

DROP TABLE IF EXISTS `%DBNAME%`.`user_`;
CREATE TABLE  `%DBNAME%`.`user_` (
  `uuid_` varchar(75) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `defaultUser` tinyint(4) DEFAULT NULL,
  `contactId` bigint(20) DEFAULT NULL,
  `password_` varchar(75) DEFAULT NULL,
  `passwordEncrypted` tinyint(4) DEFAULT NULL,
  `passwordReset` tinyint(4) DEFAULT NULL,
  `passwordModifiedDate` datetime DEFAULT NULL,
  `reminderQueryQuestion` varchar(75) DEFAULT NULL,
  `reminderQueryAnswer` varchar(75) DEFAULT NULL,
  `graceLoginCount` int(11) DEFAULT NULL,
  `screenName` varchar(75) DEFAULT NULL,
  `emailAddress` varchar(75) DEFAULT NULL,
  `openId` varchar(1024) DEFAULT NULL,
  `portraitId` bigint(20) DEFAULT NULL,
  `languageId` varchar(75) DEFAULT NULL,
  `timeZoneId` varchar(75) DEFAULT NULL,
  `greeting` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `firstName` varchar(75) DEFAULT NULL,
  `middleName` varchar(75) DEFAULT NULL,
  `lastName` varchar(75) DEFAULT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `loginDate` datetime DEFAULT NULL,
  `loginIP` varchar(75) DEFAULT NULL,
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginIP` varchar(75) DEFAULT NULL,
  `lastFailedLoginDate` datetime DEFAULT NULL,
  `failedLoginAttempts` int(11) DEFAULT NULL,
  `lockout` tinyint(4) DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `agreedToTermsOfUse` tinyint(4) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `IX_615E9F7A` (`companyId`,`emailAddress`),
  UNIQUE KEY `IX_C5806019` (`companyId`,`screenName`),
  UNIQUE KEY `IX_9782AD88` (`companyId`,`userId`),
  UNIQUE KEY `IX_5ADBE171` (`contactId`),
  KEY `IX_3A1E834E` (`companyId`),
  KEY `IX_6EF03E4E` (`companyId`,`defaultUser`),
  KEY `IX_762F63C6` (`emailAddress`),
  KEY `IX_A9ED7DD3` (`openId`(767)),
  KEY `IX_A18034A4` (`portraitId`),
  KEY `IX_E0422BDA` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`user_`
--

/*!40000 ALTER TABLE `user_` DISABLE KEYS */;
LOCK TABLES `user_` WRITE;
INSERT INTO `%DBNAME%`.`user_` VALUES  ('fdba278b-c304-4a7d-bd2d-d56f7d95deb6',10117,10114,'2010-08-03 09:47:09','2010-08-03 09:47:09',1,10118,'password',0,0,NULL,'','',0,'10117','default@liferay.com','',0,'en_US','UTC','Welcome!','','','','','','2010-08-03 09:47:09','',NULL,'',NULL,0,0,NULL,1,1),
 ('8326edc4-fce4-4e12-aa54-90c7f1e94286',10146,10114,'2010-08-03 09:47:11','2010-08-03 09:47:11',0,10147,'qUqP5cyxm6YcTAhz05Hph5gvu9M=',1,0,NULL,'','',0,'test','test@liferay.com','',0,'en_US','UTC','Welcome Test Test!','','Test','','Test','',NULL,'',NULL,'',NULL,0,0,NULL,0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `user_` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`usergroup`
--

DROP TABLE IF EXISTS `%DBNAME%`.`usergroup`;
CREATE TABLE  `%DBNAME%`.`usergroup` (
  `userGroupId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `parentUserGroupId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`userGroupId`),
  UNIQUE KEY `IX_23EAD0D` (`companyId`,`name`),
  KEY `IX_524FEFCE` (`companyId`),
  KEY `IX_69771487` (`companyId`,`parentUserGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`usergroup`
--

/*!40000 ALTER TABLE `usergroup` DISABLE KEYS */;
LOCK TABLES `usergroup` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `usergroup` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`usergroupgrouprole`
--

DROP TABLE IF EXISTS `%DBNAME%`.`usergroupgrouprole`;
CREATE TABLE  `%DBNAME%`.`usergroupgrouprole` (
  `userGroupId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userGroupId`,`groupId`,`roleId`),
  KEY `IX_CCBE4063` (`groupId`),
  KEY `IX_CAB0CCC8` (`groupId`,`roleId`),
  KEY `IX_1CDF88C` (`roleId`),
  KEY `IX_DCDED558` (`userGroupId`),
  KEY `IX_73C52252` (`userGroupId`,`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`usergroupgrouprole`
--

/*!40000 ALTER TABLE `usergroupgrouprole` DISABLE KEYS */;
LOCK TABLES `usergroupgrouprole` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `usergroupgrouprole` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`usergrouprole`
--

DROP TABLE IF EXISTS `%DBNAME%`.`usergrouprole`;
CREATE TABLE  `%DBNAME%`.`usergrouprole` (
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`groupId`,`roleId`),
  KEY `IX_1B988D7A` (`groupId`),
  KEY `IX_871412DF` (`groupId`,`roleId`),
  KEY `IX_887A2C95` (`roleId`),
  KEY `IX_887BE56A` (`userId`),
  KEY `IX_4D040680` (`userId`,`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`usergrouprole`
--

/*!40000 ALTER TABLE `usergrouprole` DISABLE KEYS */;
LOCK TABLES `usergrouprole` WRITE;
INSERT INTO `%DBNAME%`.`usergrouprole` VALUES  (10146,10138,10126);
UNLOCK TABLES;
/*!40000 ALTER TABLE `usergrouprole` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`useridmapper`
--

DROP TABLE IF EXISTS `%DBNAME%`.`useridmapper`;
CREATE TABLE  `%DBNAME%`.`useridmapper` (
  `userIdMapperId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `description` varchar(75) DEFAULT NULL,
  `externalUserId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`userIdMapperId`),
  UNIQUE KEY `IX_41A32E0D` (`type_`,`externalUserId`),
  UNIQUE KEY `IX_D1C44A6E` (`userId`,`type_`),
  KEY `IX_E60EA987` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`useridmapper`
--

/*!40000 ALTER TABLE `useridmapper` DISABLE KEYS */;
LOCK TABLES `useridmapper` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `useridmapper` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`users_groups`
--

DROP TABLE IF EXISTS `%DBNAME%`.`users_groups`;
CREATE TABLE  `%DBNAME%`.`users_groups` (
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`groupId`),
  KEY `IX_C4F9E699` (`groupId`),
  KEY `IX_F10B6C6B` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`users_groups`
--

/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
LOCK TABLES `users_groups` WRITE;
INSERT INTO `%DBNAME%`.`users_groups` VALUES  (10146,10138);
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`users_orgs`
--

DROP TABLE IF EXISTS `%DBNAME%`.`users_orgs`;
CREATE TABLE  `%DBNAME%`.`users_orgs` (
  `userId` bigint(20) NOT NULL,
  `organizationId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`organizationId`),
  KEY `IX_7EF4EC0E` (`organizationId`),
  KEY `IX_FB646CA6` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`users_orgs`
--

/*!40000 ALTER TABLE `users_orgs` DISABLE KEYS */;
LOCK TABLES `users_orgs` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_orgs` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`users_permissions`
--

DROP TABLE IF EXISTS `%DBNAME%`.`users_permissions`;
CREATE TABLE  `%DBNAME%`.`users_permissions` (
  `userId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`permissionId`),
  KEY `IX_8AE58A91` (`permissionId`),
  KEY `IX_C26AA64D` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`users_permissions`
--

/*!40000 ALTER TABLE `users_permissions` DISABLE KEYS */;
LOCK TABLES `users_permissions` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_permissions` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`users_roles`
--

DROP TABLE IF EXISTS `%DBNAME%`.`users_roles`;
CREATE TABLE  `%DBNAME%`.`users_roles` (
  `userId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `IX_C19E5F31` (`roleId`),
  KEY `IX_C1A01806` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`users_roles`
--

/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
LOCK TABLES `users_roles` WRITE;
INSERT INTO `%DBNAME%`.`users_roles` VALUES  (10146,10120),
 (10117,10121),
 (10146,10123),
 (10146,10124);
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`users_usergroups`
--

DROP TABLE IF EXISTS `%DBNAME%`.`users_usergroups`;
CREATE TABLE  `%DBNAME%`.`users_usergroups` (
  `userGroupId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`userGroupId`,`userId`),
  KEY `IX_66FF2503` (`userGroupId`),
  KEY `IX_BE8102D6` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`users_usergroups`
--

/*!40000 ALTER TABLE `users_usergroups` DISABLE KEYS */;
LOCK TABLES `users_usergroups` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_usergroups` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`usertracker`
--

DROP TABLE IF EXISTS `%DBNAME%`.`usertracker`;
CREATE TABLE  `%DBNAME%`.`usertracker` (
  `userTrackerId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `sessionId` varchar(200) DEFAULT NULL,
  `remoteAddr` varchar(75) DEFAULT NULL,
  `remoteHost` varchar(75) DEFAULT NULL,
  `userAgent` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`userTrackerId`),
  KEY `IX_29BA1CF5` (`companyId`),
  KEY `IX_46B0AE8E` (`sessionId`),
  KEY `IX_E4EFBA8D` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`usertracker`
--

/*!40000 ALTER TABLE `usertracker` DISABLE KEYS */;
LOCK TABLES `usertracker` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `usertracker` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`usertrackerpath`
--

DROP TABLE IF EXISTS `%DBNAME%`.`usertrackerpath`;
CREATE TABLE  `%DBNAME%`.`usertrackerpath` (
  `userTrackerPathId` bigint(20) NOT NULL,
  `userTrackerId` bigint(20) DEFAULT NULL,
  `path_` longtext,
  `pathDate` datetime DEFAULT NULL,
  PRIMARY KEY (`userTrackerPathId`),
  KEY `IX_14D8BCC0` (`userTrackerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`usertrackerpath`
--

/*!40000 ALTER TABLE `usertrackerpath` DISABLE KEYS */;
LOCK TABLES `usertrackerpath` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `usertrackerpath` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`vocabulary`
--

DROP TABLE IF EXISTS `%DBNAME%`.`vocabulary`;
CREATE TABLE  `%DBNAME%`.`vocabulary` (
  `vocabularyId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` varchar(75) DEFAULT NULL,
  `folksonomy` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`vocabularyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`vocabulary`
--

/*!40000 ALTER TABLE `vocabulary` DISABLE KEYS */;
LOCK TABLES `vocabulary` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `vocabulary` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`webdavprops`
--

DROP TABLE IF EXISTS `%DBNAME%`.`webdavprops`;
CREATE TABLE  `%DBNAME%`.`webdavprops` (
  `webDavPropsId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `props` longtext,
  PRIMARY KEY (`webDavPropsId`),
  UNIQUE KEY `IX_97DFA146` (`classNameId`,`classPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`webdavprops`
--

/*!40000 ALTER TABLE `webdavprops` DISABLE KEYS */;
LOCK TABLES `webdavprops` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `webdavprops` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`website`
--

DROP TABLE IF EXISTS `%DBNAME%`.`website`;
CREATE TABLE  `%DBNAME%`.`website` (
  `websiteId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `url` longtext,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`websiteId`),
  KEY `IX_96F07007` (`companyId`),
  KEY `IX_4F0F0CA7` (`companyId`,`classNameId`),
  KEY `IX_F960131C` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_1AA07A6D` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_F75690BB` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`website`
--

/*!40000 ALTER TABLE `website` DISABLE KEYS */;
LOCK TABLES `website` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `website` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`wikinode`
--

DROP TABLE IF EXISTS `%DBNAME%`.`wikinode`;
CREATE TABLE  `%DBNAME%`.`wikinode` (
  `uuid_` varchar(75) DEFAULT NULL,
  `nodeId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`nodeId`),
  UNIQUE KEY `IX_920CD8B1` (`groupId`,`name`),
  UNIQUE KEY `IX_7609B2AE` (`uuid_`,`groupId`),
  KEY `IX_5D6FE3F0` (`companyId`),
  KEY `IX_B480A672` (`groupId`),
  KEY `IX_6C112D7C` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`wikinode`
--

/*!40000 ALTER TABLE `wikinode` DISABLE KEYS */;
LOCK TABLES `wikinode` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wikinode` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`wikipage`
--

DROP TABLE IF EXISTS `%DBNAME%`.`wikipage`;
CREATE TABLE  `%DBNAME%`.`wikipage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `pageId` bigint(20) NOT NULL,
  `resourcePrimKey` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `nodeId` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `minorEdit` tinyint(4) DEFAULT NULL,
  `content` longtext,
  `summary` longtext,
  `format` varchar(75) DEFAULT NULL,
  `head` tinyint(4) DEFAULT NULL,
  `parentTitle` varchar(255) DEFAULT NULL,
  `redirectTitle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pageId`),
  UNIQUE KEY `IX_3D4AF476` (`nodeId`,`title`,`version`),
  UNIQUE KEY `IX_899D3DFB` (`uuid_`,`groupId`),
  KEY `IX_A2001730` (`format`),
  KEY `IX_C8A9C476` (`nodeId`),
  KEY `IX_E7F635CA` (`nodeId`,`head`),
  KEY `IX_65E84AF4` (`nodeId`,`head`,`parentTitle`),
  KEY `IX_46EEF3C8` (`nodeId`,`parentTitle`),
  KEY `IX_1ECC7656` (`nodeId`,`redirectTitle`),
  KEY `IX_997EEDD2` (`nodeId`,`title`),
  KEY `IX_E745EA26` (`nodeId`,`title`,`head`),
  KEY `IX_9C0E478F` (`uuid_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`wikipage`
--

/*!40000 ALTER TABLE `wikipage` DISABLE KEYS */;
LOCK TABLES `wikipage` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wikipage` ENABLE KEYS */;


--
-- Definition of table `%DBNAME%`.`wikipageresource`
--

DROP TABLE IF EXISTS `%DBNAME%`.`wikipageresource`;
CREATE TABLE  `%DBNAME%`.`wikipageresource` (
  `resourcePrimKey` bigint(20) NOT NULL,
  `nodeId` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`resourcePrimKey`),
  UNIQUE KEY `IX_21277664` (`nodeId`,`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `%DBNAME%`.`wikipageresource`
--

/*!40000 ALTER TABLE `wikipageresource` DISABLE KEYS */;
LOCK TABLES `wikipageresource` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wikipageresource` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
