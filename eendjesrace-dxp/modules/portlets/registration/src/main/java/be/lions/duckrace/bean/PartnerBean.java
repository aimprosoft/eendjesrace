package be.lions.duckrace.bean;

import be.lions.duckrace.exception.LotsAlreadyAssignedToOtherPartnersException;
import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service_portlet.InternalRangeOverlapException;
import be.lions.duckrace.service_portlet.InvalidRangeException;
import be.lions.duckrace.service_portlet.LotRangeService;
import be.lions.duckrace.service.*;
import be.lions.duckrace.util_portlet.MessageUtil;
import be.lions.duckrace.vo.PartnerVO;
import be.lions.duckrace.vo.PartnerVOConverter;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import org.apache.commons.lang3.StringUtils;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class PartnerBean {

	private LotRangeService lotRangeService = new LotRangeService();
	private PartnerVOConverter converter = new PartnerVOConverter();
	
	private List<PartnerVO> partners;
	private List<SelectItem> categories;
	private PartnerVO partner;
	private String category;
	private PartnerComparator partnerComparator;
	
	public List<PartnerVO> getPartners() {
		if(partners == null) {
			search();
		}
		Collections.sort(partners, partnerComparator);
		return partners;
	}

	public void search(ValueChangeEvent event) {
		category = (String) event.getNewValue();
		search();
	}
	
	public void search() {
		try {
			List<Partner> dbPartners = null;
			if(StringUtils.isBlank(category)) {
				dbPartners = PartnerLocalServiceUtil.getPartners();
			} else {
				dbPartners = PartnerLocalServiceUtil.getPartners(category);
			}
			partners = converter.fromPartners(dbPartners);
		} catch(SystemException e) {
			MessageUtil.unexpectedError(e);
		}
	}
	
	public String newPartner() {
		partner = new PartnerVO();
		return "edit-partner";
	}
	
	public String editPartner(PartnerVO partner) {
		try {
			this.partner = converter.extendWithRangeInformation(partner);
			return "edit-partner";
		} catch (SystemException e) {
			MessageUtil.unexpectedError(e);
		} catch (PortalException e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String addRange() {
		partner.getAssignedRanges().add(new LotRange());
		return null;
	}
	
	public String deleteRange(LotRange lotRange) {
		partner.getAssignedRanges().remove(lotRange);
		return null;
	}
	
	public String save() {
		try {
			if(!partner.isAssignedValid()) {
				MessageUtil.error("Aantal teruggegeven lotjes moet groter of gelijk zijn aan aantal toegewezen lotjes.");
				return null;
			}
			int max = LotLocalServiceUtil.getPressedLotsCount();
			List<LotRange> lotRanges = partner.getAssignedRanges();
			lotRangeService.validate(lotRanges, max);
			lotRanges = lotRangeService.normalize(lotRanges);
			PartnerLocalServiceUtil.save(converter.fromPartnerVO(partner), lotRanges);
			MessageUtil.info("Partner werd succesvol opgeslagen!");
			return goBack();
		} catch (LotsAlreadyAssignedToOtherPartnersException e) {
			MessageUtil.error(e.getMessage());
		} catch (NoSuchLotException e) {
			MessageUtil.unexpectedError(e);
		} catch (InvalidRangeException e) {
			MessageUtil.error(e.getMessage());
		} catch (InternalRangeOverlapException e) {
			MessageUtil.error(e.getMessage());
		} catch (SystemException e) {
			MessageUtil.unexpectedError(e);
		} catch (PortalException e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
	public String goBack() {
		clearAll();
		return "partners";
	}

	public PartnerVO getPartner() {
		return partner;
	}

	public void setPartner(PartnerVO partner) {
		this.partner = partner;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	private void clearAll() {
		partner = null;
		partners = null;
		categories = null;
	}
	
	public List<SelectItem> getCategories() {
		if(categories == null) {
			categories = new ArrayList<SelectItem>();
			try {
				List<String> categoryNames = PartnerLocalServiceUtil.getCategories();
				for(String categoryName: categoryNames) {
					categories.add(new SelectItem(categoryName));
				}
			} catch (SystemException e) {
			}
		}
		return categories;
	}

	public PartnerComparator getPartnerComparator() {
		return partnerComparator;
	}

	public void setPartnerComparator(PartnerComparator partnerComparator) {
		this.partnerComparator = partnerComparator;
	}

}
