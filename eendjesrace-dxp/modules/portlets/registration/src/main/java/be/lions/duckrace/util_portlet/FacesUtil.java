package be.lions.duckrace.util_portlet;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.portlet.PortletSession;
import java.io.IOException;

public class FacesUtil {

	public static void putOnSession(String key, Object obj) {
		getPortletSession().setAttribute(key, obj, PortletSession.APPLICATION_SCOPE);
	}
	
	public static Object getFromSession(String key) {
		return getPortletSession().getAttribute(key, PortletSession.APPLICATION_SCOPE);
	}
	
	public static void removeFromSession(String key) {
		getPortletSession().removeAttribute(key, PortletSession.APPLICATION_SCOPE);
	}
	
	public static User getUser() {
		return getThemeDisplay().getUser();
	}
	
	public static long getUserId() {
		return getThemeDisplay().getUserId();
	}
	
	public static void redirectTo(String friendlyUrl) {
		try {
			ec().redirect(getRelativePageLink(friendlyUrl));
		} catch (IOException e) {
			MessageUtil.unexpectedError(e);
		}
	}

	private static PortletSession getPortletSession() {
		return (PortletSession) ec().getSession(false);
	}

	private static String getRelativePageLink(String friendlyUrl) {
		if (getThemeDisplay().getURLCurrent().startsWith("/web")) {
			return "/web/guest" + friendlyUrl;
		}
		return friendlyUrl;
	}
	
	private static ThemeDisplay getThemeDisplay() {
		return (ThemeDisplay) ec().getRequestMap().get(WebKeys.THEME_DISPLAY);
	}

	private static ExternalContext ec() {
		return fc().getExternalContext();
	}

	private static FacesContext fc() {
		return FacesContext.getCurrentInstance();
	}

}
