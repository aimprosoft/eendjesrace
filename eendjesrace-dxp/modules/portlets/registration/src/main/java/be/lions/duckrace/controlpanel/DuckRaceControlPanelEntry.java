package be.lions.duckrace.controlpanel;

import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.BaseControlPanelEntry;
import com.liferay.portal.kernel.security.permission.PermissionChecker;

public class DuckRaceControlPanelEntry extends BaseControlPanelEntry {

    @Override
    public boolean hasAccessPermission(PermissionChecker pc, Group group, Portlet portlet) throws Exception {
        return pc.isCompanyAdmin() || pc.isOmniadmin() || DuckRaceLocalServiceUtil.isLionsMember(pc.getUserId());
    }

}
