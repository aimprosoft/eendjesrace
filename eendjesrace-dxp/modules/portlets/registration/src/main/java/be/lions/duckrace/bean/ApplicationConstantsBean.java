package be.lions.duckrace.bean;

import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.util_portlet.MessageUtil;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ApplicationConstantsBean {

	private List<SelectItem> duckAmounts;
	
	public List<SelectItem> getDuckAmounts() {
		if(duckAmounts == null) {
			duckAmounts = new ArrayList<SelectItem>();
			duckAmounts.add(new SelectItem(1, "1"));
			duckAmounts.add(new SelectItem(2, "2"));
			duckAmounts.add(new SelectItem(3, "3"));
			duckAmounts.add(new SelectItem(4, "4"));
			duckAmounts.add(new SelectItem(5, "5"));
			duckAmounts.add(new SelectItem(6, "6"));
			duckAmounts.add(new SelectItem(7, "7"));
			duckAmounts.add(new SelectItem(8, "8"));
			duckAmounts.add(new SelectItem(9, "9"));
			duckAmounts.add(new SelectItem(10, "10"));
			duckAmounts.add(new SelectItem(20, "20"));
			duckAmounts.add(new SelectItem(30, "30"));
			duckAmounts.add(new SelectItem(50, "50"));
			duckAmounts.add(new SelectItem(100, "100 - "+MessageUtil.getMessage("partner_package")+" "+MessageUtil.getMessage("bronze")));
			duckAmounts.add(new SelectItem(300, "300 - "+MessageUtil.getMessage("partner_package")+" "+MessageUtil.getMessage("silver")));
			duckAmounts.add(new SelectItem(500, "500 - "+MessageUtil.getMessage("partner_package")+" "+MessageUtil.getMessage("gold")));
			duckAmounts.add(new SelectItem(1000, "1.000 - "+MessageUtil.getMessage("partner_package")+" "+MessageUtil.getMessage("diamond")));
			duckAmounts.add(new SelectItem(2000, "2.000 - "+MessageUtil.getMessage("partner_package")+" "+MessageUtil.getMessage("platinum")));
		}
		return duckAmounts;
	}
	
	public Locale getLocale() {
		return MessageUtil.getLocale();
	}
	
	public String getAccountNoBelgium() {
		return DuckRaceLocalServiceUtil.getAccountNoBelgium();
	}
	
	public String getAccountNoIban() {
		return DuckRaceLocalServiceUtil.getAccountNoIban();
	}
	
	public String getAccountNoSwift() {
		return DuckRaceLocalServiceUtil.getAccountNoSwift();
	}
	
}
