package be.lions.duckrace.bean;

import be.lions.duckrace.vo.PartnerVO;

import java.util.Comparator;

public class PartnerComparator implements Comparator<PartnerVO> {

	private static final String COLUMN_CATEGORY = "category";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_ASSIGNED = "assigned";
	private static final String COLUMN_REGISTERED = "registered";
	private static final String COLUMN_RETURNED = "returned";
	private static final String COLUMN_SOLD = "sold";
	private static final String COLUMN_RANGES = "ranges";
	
	private String sortColumn;
	private boolean sortAsc = true;

	public int compare(PartnerVO p1, PartnerVO p2) {
		int result = 0;
		if(COLUMN_CATEGORY.equals(sortColumn)) {
			result = p1.getCategory().compareTo(p2.getCategory());
		} else if(COLUMN_NAME.equals(sortColumn)) {
			result = p1.getName().compareTo(p2.getName());
		} else if(COLUMN_ASSIGNED.equals(sortColumn)) {
			result = p2.getNbAssigned() - p1.getNbAssigned();
		} else if(COLUMN_REGISTERED.equals(sortColumn)) {
			result = p2.getNbRegistered() - p1.getNbRegistered();
		} else if(COLUMN_RETURNED.equals(sortColumn)) {
			result = p2.getNbReturned() - p1.getNbReturned();
		} else if(COLUMN_SOLD.equals(sortColumn)) {
			result = p2.getNbSold() - p1.getNbSold();
		} else if(COLUMN_RANGES.equals(sortColumn)) {
			result = p2.getFirstAssignedLot() - p1.getFirstAssignedLot();
		}
		if(!sortAsc) {
			result = -result;
		}
		return result;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

}
