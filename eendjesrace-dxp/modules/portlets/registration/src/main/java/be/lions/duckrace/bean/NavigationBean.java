package be.lions.duckrace.bean;

import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.util_portlet.FacesUtil;
import be.lions.duckrace.util_portlet.MessageUtil;
import be.lions.duckrace.util_service_api.PropKeys;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PropsUtil;

import java.util.Date;

public class NavigationBean {

	private static final String HAS_LOT_ACTION = "has-lot";
	private static final String HAS_NO_LOT_ACTION = "has-no-lot";
	private static final String ACTION_PARAM_NAME = "duckrace_action";
	private static final String LOCALE_REPLACEMENT = "%LOCALE%";

	public String navigate() {
		Object action = FacesUtil.getFromSession(ACTION_PARAM_NAME);
		if (action == null) {
			return "choose-action";
		}
		FacesUtil.removeFromSession(ACTION_PARAM_NAME);
		String actionStr = (String) action;
		if (HAS_LOT_ACTION.equals(actionStr)) {
			return toHasLot();
		} else if (HAS_NO_LOT_ACTION.equals(actionStr)) {
			return toHasNoLot();
		}
		return (String) action;
	}

	public String toHasLot() {
		try {
			if (!DuckRaceLocalServiceUtil.isRegistrationPossible(FacesUtil.getUserId(), new Date())) {
				MessageUtil.error(MessageUtil.getMessage("registrations_for_x_are_closed",
						DuckRaceLocalServiceUtil.getSiteTitle()));
				return null;
			}
			return HAS_LOT_ACTION;
		} catch (Exception e) {
			MessageUtil.unexpectedError(e);
			return null;
		}
	}

	public String toHasNoLot() {
		try {
			if (!DuckRaceLocalServiceUtil.isReservationPossible(FacesUtil.getUserId(), new Date())) {
				MessageUtil.error(MessageUtil.getMessage("reservations_are_closed"));
				return null;
			}
		} catch (Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return HAS_NO_LOT_ACTION;
	}

	public int getTotalCount() {
		try {
			return LotLocalServiceUtil.getCurrentNumber();
		} catch (SystemException e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public int getWebsiteRegistrationCount() {
		try {
			return LotLocalServiceUtil.getWebsiteRegistrationLotCount();
		} catch (SystemException e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public int getWebsiteReservationCount() {
		try {
			return LotLocalServiceUtil.getWebsiteReservationLotCount();
		} catch (SystemException e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public int getShortSmsCount() {
		try {
			return LotLocalServiceUtil.getShortSmsLotCount();
		} catch (SystemException e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public int getLongSmsCount() {
		try {
			return LotLocalServiceUtil.getLongSmsLotCount();
		} catch (SystemException e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public String toHasLotRedirect() {
		FacesUtil.putOnSession("resetRegistrationInfo", "true");
		return toRegistration(HAS_LOT_ACTION);
	}

	public String toHasNoLotRedirect() {
		FacesUtil.putOnSession("resetRegistrationInfo", "true");
		return toRegistration(HAS_NO_LOT_ACTION);
	}

	public String getPaymentModuleURL() {
		String urlTpl = PropsUtil.get(PropKeys.BEHAPPY2_ORDER_URL);
		String language = MessageUtil.getLocale().getLanguage();
		if (language.equals("en")) {
			language = "nl";
		}

		return urlTpl.replace(LOCALE_REPLACEMENT, language);
	}

	private String toRegistration(String action) {
		FacesUtil.putOnSession(ACTION_PARAM_NAME, action);
		FacesUtil.redirectTo("/registratie");
		return null;
	}

	private static final Log LOGGER = LogFactoryUtil.getLog(NavigationBean.class);

}
