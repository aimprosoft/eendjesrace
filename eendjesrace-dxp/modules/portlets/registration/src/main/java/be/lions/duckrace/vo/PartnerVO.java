package be.lions.duckrace.vo;

import be.lions.duckrace.model.LotRange;
import com.liferay.portal.kernel.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class PartnerVO implements Comparable<PartnerVO> {

	private long id;
	private String category;
	private String name;
	private int nbAssigned;
	private int nbRegistered;
	private int nbReturned;
	private List<LotRange> assignedRanges = new ArrayList<LotRange>();

	public String getAssignedRangesStr() {
		return StringUtil.merge(assignedRanges, "<br/>");
	}
	
	public int getFirstAssignedLot() {
		if(assignedRanges.isEmpty()) {
			return -1;
		}
		return assignedRanges.get(0).getFrom();
	}
	
	public int getNbSold() {
		int result = nbAssigned - nbReturned;
		return result < 0 ? 0 : result;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNbAssigned() {
		return nbAssigned;
	}

	public void setNbAssigned(int nbAssigned) {
		this.nbAssigned = nbAssigned;
	}

	public int getNbRegistered() {
		return nbRegistered;
	}

	public void setNbRegistered(int nbRegistered) {
		this.nbRegistered = nbRegistered;
	}

	public int getNbReturned() {
		return nbReturned;
	}

	public void setNbReturned(int nbReturned) {
		this.nbReturned = nbReturned;
	}

	public List<LotRange> getAssignedRanges() {
		return assignedRanges;
	}

	public void setAssignedRanges(List<LotRange> assignedRanges) {
		this.assignedRanges = assignedRanges;
	}
	
	public boolean isAssignedValid() {
		return nbReturned >= 0 && nbReturned <= nbAssigned;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartnerVO other = (PartnerVO) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public int compareTo(PartnerVO other) {
		int result = category.compareTo(other.getCategory());
		if(result == 0) {
			result = name.compareTo(other.getName());
		}
		return result;
	}

}
