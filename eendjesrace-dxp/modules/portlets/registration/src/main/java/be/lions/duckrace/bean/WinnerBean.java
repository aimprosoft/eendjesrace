package be.lions.duckrace.bean;

import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.util_portlet.MessageUtil;

public class WinnerBean {

	private int number;
	private Participant participant;

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Participant getParticipant() {
		return participant;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public String search() {
		if(number == 0) {
			MessageUtil.error("Gelieve het gewonnen eendjesnummer in te voeren");
		}
		try {
			participant = ParticipantLocalServiceUtil.getParticipantByLotNumber(number);
			if(participant == null) {
				MessageUtil.error("Er bestaat geen deelnemer gelinkt aan het opgegeven eendjesnummer");
			}
		} catch(NoSuchLotException e) {
			MessageUtil.error("Er bestaat geen lotje met het opgegeven eendjesnummer");
		} catch(Exception e) {
			MessageUtil.unexpectedError(e);
		}
		return null;
	}
	
}
