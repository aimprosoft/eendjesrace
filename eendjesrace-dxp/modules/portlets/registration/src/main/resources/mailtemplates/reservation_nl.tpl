<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
#set ($participant = $reservation.participant)
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Beste $participant.fullName,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Bedankt voor uw registratie voor de $title.<br/>
Registratiedetails:
</p>
<ul style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Naam: <b>$participant.fullName</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">E-mailadres: <b>$participant.emailAddress</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Gsm-nummer: <b>$participant.formattedMobileNumber</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Aantal eendjes: <b>$reservation.numberLots</b></li>
	<li style="margin-bottom:0px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Kostprijs: <b>&euro; $reservation.totalCost</b></li>
</ul>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Gelieve het bedrag van <b>&euro; $reservation.totalCost</b> over te maken op het
rekeningnummer <b>$accountNoBelgium</b> met de volgende vermelding: <b>EEND $reservation.reservationId</b>.</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
(IBAN-code: $accountNoIban - Swiftcode: $accountNoSwift)
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Wij controleren regelmatig de status van de betalingen. Wanneer je betaling correct ontvangen is wordt je registratie
goedgekeurd en worden de lotgegevens naar dit e-mailadres doorgestuurd.<br/>
Opgelet! Als we binnen 2 weken uw betaling niet ontvangen heeft, dan vervalt de reservatie.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Met vriendelijke groeten,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>