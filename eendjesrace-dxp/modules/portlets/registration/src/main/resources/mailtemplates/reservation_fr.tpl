<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
#set ($participant = $reservation.participant)
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Cher, Chère $participant.fullName,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Merci pour votre participation à $title.<br/>
Détails d’enregistrement:
</p>
<ul style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Nom: <b>$participant.fullName</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Adresse e-mail: <b>$participant.emailAddress</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Numéro de GSM: <b>$participant.formattedMobileNumber</b></li>
	<li style="margin-bottom:10px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Nombre de canards: <b>$reservation.numberLots</b></li>
	<li style="margin-bottom:0px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Prix: <b>&euro; $reservation.totalCost</b></li>
</ul>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Merci de verser le montant de <b>&euro; $reservation.totalCost</b> sur le numéro de compte
<b>$accountNoBelgium</b> avec la mention: <b>CANARD $reservation.reservationId</b>.</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
(code IBAN: $accountNoIban - code Swift: $accountNoSwift)
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Nous contrôlons régulièrement l’état des paiements.
Lorsque nous avons bien reçu votre paiement, votre enregistrement est
accordé et les données du billet sont envoyées à cette adresse e-mail.<br/>
Attention! Si nous n’avons pas reçu votre paiement endéans les 2 semaines, votre réservation est annulée.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Sincères salutations,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>