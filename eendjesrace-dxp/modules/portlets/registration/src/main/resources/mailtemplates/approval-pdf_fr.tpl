<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="http://$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Beste $reservation.participant.fullName,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Uw reservatie van <b>$reservation.numberLots eendjes</b> voor de $title
werd zojuist goedgekeurd. Dit betekent dat wij uw betaling van <b>&euro; $reservation.totalCost</b>
correct ontvangen hebben.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
In bijlage vindt u een PDF-bestand met de eendjesnummers die aan u werden toegekend. U kan deze 
bijlage gebruiken om de lotjes te personaliseren en desgewenst uit te delen aan uw werknemers.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Met vriendelijke groeten,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>