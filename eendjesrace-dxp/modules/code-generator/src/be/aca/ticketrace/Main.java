package be.aca.ticketrace;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Main {

	private static final int CHECKSUM_LENGTH = 3;
	private static final int CODE_LENGTH = 6;
	
	private static final int NB_PRESSED_CODES = 1000;
	private static final int NB_ONLINE_CODES = 1000;
	
	public static void main(String[] args) throws IOException {
		new Main().generate();
	}
	
	private void generate() throws IOException {
		StringBuffer sqlContent = new StringBuffer("INSERT INTO DuckRace_Lot (code_, checksum, pressed, lotNumber) VALUES\n");
		StringBuffer txtContent = new StringBuffer();
		
		RandomCodeGenerator gen = new RandomCodeGenerator();
		for(int i=1; i<=NB_PRESSED_CODES; i++) {
			generateLot(i, gen, sqlContent, txtContent, true);
		}
		for(int i=0; i<NB_ONLINE_CODES; i++) {
			generateLot(0, gen, sqlContent, txtContent, false);
		}
		
		//Remove trailing comma
		sqlContent.delete(sqlContent.lastIndexOf(","), sqlContent.length()).append(";");
		
		writeContent("items.sql", sqlContent.toString());
		writeContent("items.txt", txtContent.toString());
	}

	private void generateLot(int lotNumber, RandomCodeGenerator gen, StringBuffer sqlContent, StringBuffer txtContent, boolean pressed) {
		String code = gen.nextString(CODE_LENGTH);
		String checksum = gen.nextNumber(CHECKSUM_LENGTH);
		
		String pressedString = pressed ? "1" : "0";
		String lotNumberString = lotNumber == 0 ? "null" : "'" +lotNumber + "'";
		sqlContent.append("  ('"+code+"', '"+checksum+"', '"+pressedString+"', "+lotNumberString+"),\n");
		if(pressed) {
			txtContent.append(code+" "+checksum+"\n");
		}
	}

	private void writeContent(String filename, String content) throws IOException {
		Writer out = new BufferedWriter(new FileWriter(filename));
		out.write(content);
		out.close();
	}
	
}