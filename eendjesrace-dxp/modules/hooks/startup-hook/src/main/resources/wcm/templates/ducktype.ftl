<#assign theme_images_path = request['theme-display']['path-theme-images']>

<div class="sponsor-intro">
    <img src="${theme_images_path}/ducks/${.vars['subtitle'].data}-duck.png" alt="${.vars['reserved-article-title'].data}"/>
    <div class="sponsor-info">
        <h2>${.vars['reserved-article-title'].data}<span>(${.vars['subtitle'].data})</span></h2>
        <p>${.vars['description'].data}</p>
    </div>
</div>