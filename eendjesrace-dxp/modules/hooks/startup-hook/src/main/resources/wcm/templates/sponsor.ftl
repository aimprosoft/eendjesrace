<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()>
<#assign themeDisplay = serviceContext.getThemeDisplay() />

<#assign logoUrl = logo.getData()>
<#assign sponsorAddress = address.getData()>

<#if (websiteUrl.getData() != "")>
    <#assign url = websiteUrl.getData()>
<#else>
    <#assign url = "#">
</#if>

<#--<#if (!stringUtil.startsWith($url, "https://"))>-->
    <#--<#assign url = "https://${url}">-->
<#--</#if>-->

<#if (logoUrl != "")>
    <#assign logoUrl = themeDisplay.getPortalURL() + logoUrl>
<a href="${url}" class="left">
    <img class="logo" src="${logoUrl}" alt="${.vars['reserved-article-title'].data}"/>
</a>
</#if>

<div class="sponsor-desc">
    <h2>${.vars['reserved-article-title'].data}</h2>
<#if (sponsorAddress != "")>
    <p>
        <b>Adres:</b> ${sponsorAddress}<br/>
    </p>
</#if>
</div>