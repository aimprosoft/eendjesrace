<#assign JournalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()>
<#assign themeDisplay = serviceContext.getThemeDisplay() />
<#assign duckRaceUtil = serviceLocator.findService("be.lions.duckrace.service.DuckRaceLocalService")>
<#assign structure = ["SPONSOR"]>
<#assign sponsors = JournalArticleLocalService.getIndexableArticlesByDDMStructureKey(structure)>

<table class="list-logos">
<tr>
<#assign count = 0>

<#list sponsors as sponsor>
    <#assign document = saxReaderUtil.read(sponsor.getContentByLocale(locale.toString())) />
    <#assign logoUrl = document.selectSingleNode("/root/dynamic-element[@name='logo']/dynamic-content").getText() />
    <#if (logoUrl != "")>
        <#assign logoUrl = themeDisplay.getPortalURL() + logoUrl>

        <#assign id = sponsor.getArticleId()>
        <#assign url = document.selectSingleNode("/root/dynamic-element[@name='websiteUrl']/dynamic-content").getText() />
        <#--<#if (!stringUtil.startsWith(url, "https://"))>-->
            <#--<#assign url = "https://${url}">-->
        <#--</#if>-->

        <#assign stars = duckRaceUtil.getNbStars(id)>

        <#if (stars == 0)>
            <td>
                <a href="${url}" target="_blank">
                    <img src="${logoUrl}" alt="${.vars['reserved-article-title'].data}" style="border:none"/>
                </a>
            </td>
            <#assign count = count + 1>
        </#if>

        <#if (count % 5 == 0)>
            </tr><tr>
            <#assign count = 0>
        </#if>
    </#if>
</#list>

</tr>
</table>