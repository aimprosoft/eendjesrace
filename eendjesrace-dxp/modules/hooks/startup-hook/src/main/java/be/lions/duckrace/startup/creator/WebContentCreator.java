package be.lions.duckrace.startup.creator;

import static be.lions.duckrace.startup.type.LiferayStructureField.FieldType.IMAGE_FROM_GALLERY;
import static be.lions.duckrace.startup.type.LiferayStructureField.FieldType.TEXTAREA;
import static be.lions.duckrace.startup.type.LiferayStructureField.FieldType.TEXTFIELD;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import be.lions.duckrace.startup.type.LiferayStructureFieldList;
import be.lions.duckrace.startup.type.LiferayCommunity;
import be.lions.duckrace.startup.type.LiferayStructure;
import be.lions.duckrace.startup.type.LiferayTemplate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;

/**
 * Wordt nog niet gebruikt...
 */
public class WebContentCreator {

	private static final String VM_BASE_DIR = "/wcm/templates";
	private LiferayCommunity guest;
	
	public WebContentCreator(LiferayCommunity guest) {
		this.guest = guest;
	}

	public void insertContent() throws SystemException, PortalException, IOException {
		createDuckTypes();
		createGallery();
		createLogos();
		createMerchants();
		createSponsors();
	}
	
	private void createDuckTypes() throws PortalException, SystemException, IOException {
		LiferayStructureFieldList fields = new LiferayStructureFieldList();
		fields.addField("subtitle", TEXTFIELD);
		fields.addField("description", TEXTAREA);
		LiferayStructure structure = guest.addStructure("DUCKTYPE", fields);
		structure.addTemplate(getContent("ducktype.vm"), false);
	}

	private void createGallery() throws SystemException, PortalException, IOException {
		LiferayStructureFieldList fields = new LiferayStructureFieldList();
		fields.addField("number-of-columns", TEXTFIELD);
		fields.addField("thumbnail-height", TEXTFIELD);
		LiferayStructure structure = guest.addStructure("GALLERY", fields);
		LiferayTemplate template = structure.addTemplate(getContent("gallery.vm"), true);
		
		Map<String, String> values = new HashMap<String, String>();
		values.put("number-of-columns", "5");
		values.put("thumbnail-height", "150");
		template.addArticle("JAJA", "bla", "bla", values, true);
		
		values.put("number-of-columns", "7");
		values.put("thumnail-height", "150");
		template.addArticle("NOGIETS", "test", "ja", values, false);
	}

	private void createLogos() throws SystemException, PortalException, IOException {
		LiferayStructure structure = guest.addStructure("LOGOS", null);
		structure.addTemplate(getContent("logos.ftl"), true);
	}

	private void createMerchants() throws SystemException, PortalException, IOException {
		LiferayStructureFieldList fields = new LiferayStructureFieldList();
		fields.addField("description", TEXTAREA);
		LiferayStructure structure = guest.addStructure("MERCHANT", fields);
		structure.addTemplate(getContent("merchant.vm"), false);
	}

	private void createSponsors() throws SystemException, PortalException, IOException {
		LiferayStructureFieldList fields = new LiferayStructureFieldList();
		fields.addField("logo", IMAGE_FROM_GALLERY);
		fields.addField("address", TEXTFIELD);
		fields.addField("website-url", TEXTFIELD);
		LiferayStructure structure = guest.addStructure("SPONSOR", fields);
		structure.addTemplate(getContent("sponsor.vm"), false);
	}
	
	private String getContent(String fileName) throws IOException {
		return new String(FileUtil.getBytes(getClass().getClassLoader()
			.getResourceAsStream(VM_BASE_DIR + "/" + fileName)));
	}
	
}
