package be.lions.duckrace.startup.creator;

import be.lions.duckrace.startup.util.StartupUtil;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.util.DDMUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;

import java.util.*;


public class StructureCreator extends ResourceCreator {

	private static final String STRUCTURE_PATH = "/wcm/structures/";

	@Override
	void doInsert(String filename, String fileContent) throws PortalException, SystemException {
		String parentStructureId = null;
		String structureKey = filename;
		String storageType = "json";
		int type = DDMStructureConstants.TYPE_DEFAULT;
		Map<Locale,String> nameMap = new HashMap<>();
		Map<Locale,String> descriptionMap = new HashMap<>();
		nameMap.put(Locale.getDefault(), filename);
		descriptionMap.put(Locale.getDefault(), filename);
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		long classNameId = PortalUtil.getPortal().getClassNameId(JournalArticle.class);
		DDMForm ddmForm = null;
		DDMFormLayout ddmFormLayout;
		try {
			ddmForm = DDMUtil.getDDMForm(fileContent);
		} catch (PortalException e) {
			log.error("Exception when parsing structure JSON", e);
		}
		ddmFormLayout= DDMUtil.getDefaultDDMFormLayout(ddmForm);

		try {
			DDMStructureLocalServiceUtil.addStructure(StartupUtil.getAdminId(), StartupUtil.getGroupId(),parentStructureId,
					classNameId, structureKey, nameMap, descriptionMap, ddmForm, ddmFormLayout, storageType, type, serviceContext);
			log.info("Structure '" + structureKey + "' created");
		} catch (PortalException e) {
			log.warn("Structure '" + structureKey + "' already existed, updating JSON...");
			DDMStructureLocalServiceUtil.updateStructure(StartupUtil.getAdminId(), StartupUtil.getGroupId(), 0, classNameId,
					 structureKey, nameMap, descriptionMap, ddmForm, ddmFormLayout, serviceContext);
			log.info("Structure '" + structureKey + "' JSON updated");
		}
	}

	@Override
	String getResourceFolder() {
		return STRUCTURE_PATH;
	}

	private static Log log = LogFactoryUtil.getLog(StructureCreator.class);

}