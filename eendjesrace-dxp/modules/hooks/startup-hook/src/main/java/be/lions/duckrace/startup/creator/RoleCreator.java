package be.lions.duckrace.startup.creator;

import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayRole;
import be.lions.duckrace.startup.type.LiferayRole.RoleScope;
import be.lions.duckrace.types.DuckRaceRole;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.blogs.kernel.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import static com.liferay.journal.constants.JournalPortletKeys.JOURNAL;
import static com.liferay.portal.kernel.security.permission.ActionKeys.*;
import static com.liferay.portal.kernel.util.PortletKeys.*;

public class RoleCreator {

    private LiferayCompany company;

    public RoleCreator(LiferayCompany company) {
        this.company = company;
    }

    public void insertRoles() throws PortalException, SystemException {
        editGuestRole();
        createLionsMemberRole();
        createWebmasterRole();
    }

    private void editGuestRole() throws PortalException, SystemException {
        LiferayRole guest = company.getGuestRole();
        // Allow comments
        guest.setGlobalPermissions(JournalArticle.class, VIEW, ADD_DISCUSSION);
        guest.setGlobalPermissions(BlogsEntry.class, VIEW, ADD_DISCUSSION);

        guest.setGlobalPermissions(DLFolder.class, VIEW);
        guest.setGlobalPermissions(DLFileEntry.class, VIEW);
    }

    private void createWebmasterRole() throws PortalException, SystemException {
        LiferayRole webmaster = company.addRole(DuckRaceRole.WEBMASTER.getName(), RoleScope.COMPANY);
        // Control panel
        webmaster.addControlPanelAccess(JOURNAL, MEDIA_GALLERY_DISPLAY, DOCUMENT_LIBRARY, BLOGS);
        // Web content
        webmaster.setGlobalPermissions("com.liferay.journal", ADD_ARTICLE);
        webmaster.setGlobalPermissions(JournalArticle.class, VIEW, DELETE, EXPIRE, UPDATE, DELETE_DISCUSSION, UPDATE_DISCUSSION);
        // Document library
        webmaster.setGlobalPermissions("com.liferay.document.library", ADD_FOLDER);
        webmaster.setGlobalPermissions(DLFolder.class, UPDATE, DELETE, ADD_SUBFOLDER, ADD_DOCUMENT, VIEW);
        webmaster.setGlobalPermissions(DLFileEntry.class, UPDATE, DELETE, VIEW);
        // Blogs
        webmaster.setGlobalPermissions("com.liferay.blogs", ADD_ENTRY);
        webmaster.setGlobalPermissions(BlogsEntry.class, DELETE, DELETE_DISCUSSION, UPDATE, UPDATE_DISCUSSION);
        // Tags and categories
        webmaster.setGlobalPermissions(AssetTag.class, VIEW);
        webmaster.setGlobalPermissions(AssetVocabulary.class, VIEW);
    }

    private void createLionsMemberRole() throws PortalException, SystemException {
        company.addRole(DuckRaceRole.LIONS_MEMBER.getName(), RoleScope.COMPANY);
    }

}
