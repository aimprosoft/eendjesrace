package be.lions.duckrace.startup.type.portlet;


import be.lions.duckrace.startup.type.LiferayPortlet;

import java.util.HashMap;
import java.util.Map;

public class MediaGalleryPortlet extends LiferayPortlet {

        public MediaGalleryPortlet(long groupId, String articleId) {
            super("com_liferay_document_library_web_portlet_IGDisplayPortlet_INSTANCE_ZAD60LoCCtqL");
            this.groupId = groupId;
            this.articleId = articleId;
        }

        private long groupId;
        private String articleId;
        private boolean comments;

        public long getGroupId() {
            return groupId;
        }

        public void setGroupId(long groupId) {
            this.groupId = groupId;
        }

        public String getArticleId() {
            return articleId;
        }

        public void setArticleId(String articleId) {
            this.articleId = articleId;
        }

        public boolean isComments() {
            return comments;
        }

        public void setComments(boolean comments) {
            this.comments = comments;
        }

        @Override
        protected Map<String, String> getPreferences() {
            Map<String, String> result = new HashMap<String, String>();
            result.put("group-id", ""+groupId);
            result.put("article-id", articleId);
            result.put("enable-comments", String.valueOf(comments));
            return result;
        }
}
