package be.lions.duckrace.startup;

import be.lions.duckrace.startup.creator.RoleCreator;
import be.lions.duckrace.startup.creator.StructureCreator;
import be.lions.duckrace.startup.creator.TemplateCreator;
import be.lions.duckrace.startup.type.LiferayCommunity;
import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayTagSet;
import be.lions.duckrace.types.DuckRaceFolder;
import be.lions.duckrace.types.DuckRaceTag;
import be.lions.duckrace.types.DuckRaceTagset;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class StartupAction extends SimpleAction {

	private static final String THEME_ID = "eendjes_WAR_eendjestheme";

	@Override
	public void run(String[] companyIds) throws ActionException {
		long companyId = Long.valueOf(companyIds[0]);
		try {
			startup(companyId);
		} catch(Exception e) {
			log.error(e);
		}
	}
	
	private void startup(long companyId) throws Exception {
		LiferayCompany company = LiferayCompany.get(companyId);

		LiferayCommunity guest = company.getGuestCommunity();
		guest.setThemeId(THEME_ID);

//		 Create folders
		guest.addImageFolder(DuckRaceFolder.ALBUM.getName());

//		 Create tags
		LiferayTagSet tagset = guest.addTagSet(DuckRaceTagset.SPONSOR.getName());
		for (DuckRaceTag tag: DuckRaceTag.values()) {
			tagset.addEntry(tag.getName());
		}

//		new PageCreator(guest).insertPages();
		new RoleCreator(company).insertRoles();

		new StructureCreator().insert();
		new TemplateCreator().insert();
//		new ArticleCreator().insertArticles();


	}

	private static Log log = LogFactoryUtil.getLog(StartupAction.class);
	
}
