package be.lions.duckrace.startup.type;

import java.util.ArrayList;
import java.util.List;

import be.lions.duckrace.startup.type.LiferayStructureField.FieldType;

public class LiferayStructureFieldList {

	private List<LiferayStructureField> fields = new ArrayList<LiferayStructureField>();
	
	public void addField(LiferayStructureField field) {
		fields.add(field);
	}
	
	public void addField(String name, FieldType type, boolean repeatable) {
		addField(new LiferayStructureField(name, type, repeatable));
	}
	
	public void addField(String name, FieldType type) {
		addField(name, type, false);
	}
	
	public List<LiferayStructureField> getFields() {
		return fields;
	}
	
	public String getXml() {
		StringBuilder sb = new StringBuilder();
		sb.append("<root>\n");
		for (LiferayStructureField field: fields) {
			sb.append("\t<dynamic-element "+
				"name=\""+field.getName()+"\" "+
				"type=\""+field.getTypeId()+"\" "+
				"repeatable=\""+String.valueOf(field.isRepeatable())+"\""+
			"/>\n");
		}
		sb.append("</root>");
		return sb.toString();
	}

	public boolean isEmpty() {
		return fields == null || fields.isEmpty();
	}
	
}
