package be.lions.duckrace.startup.type;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class LiferayTagSet extends LiferayVocabulary {

	public LiferayTagSet(LiferayGroup group, String name) throws PortalException, SystemException {
		super(group, name);
	}

	@Override
	protected String getName() {
		return "TagSet";
	}

	@Override
	protected boolean isFolksonomy() {
		return true;
	}
	
}
