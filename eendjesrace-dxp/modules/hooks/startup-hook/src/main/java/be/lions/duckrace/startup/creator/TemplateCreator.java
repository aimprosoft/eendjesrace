package be.lions.duckrace.startup.creator;

import be.lions.duckrace.startup.util.StartupUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TemplateCreator extends ResourceCreator {

    private static final String TEMPLATE_PATH = "/wcm/templates/";

    @Override
    void doInsert(String filename, String fileContent) throws PortalException, SystemException {
        String templateKey = filename;
        String xsl = fileContent;
        String langType = "ftl";
        boolean cacheable = false;
        boolean smallImage = false;
        String smallImageURL = null;
        File smallImageFile = null;
        long classNameId = PortalUtil.getPortal().getClassNameId(DDMStructure.class);
        long resourceClassNameId = PortalUtil.getPortal().getClassNameId(JournalArticle.class);
        com.liferay.dynamic.data.mapping.kernel.DDMStructure ddmStructure = DDMStructureManagerUtil.getStructure(StartupUtil.getGroupId(), resourceClassNameId, filename);


        long classPK = ddmStructure.getStructureId();
        Map<Locale, String> nameMap = new HashMap<>();
        Map<Locale, String> descriptionMap = new HashMap<>();
        nameMap.put(Locale.getDefault(), filename);
        descriptionMap.put(Locale.getDefault(), filename);
        String type = DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY;
        String mode = "";

        ServiceContext serviceContext = new ServiceContext();
        serviceContext.setAddGroupPermissions(true);
        serviceContext.setAddGuestPermissions(true);

        try {
            DDMTemplateLocalServiceUtil.addTemplate(StartupUtil.getAdminId(), StartupUtil.getGroupId(), classNameId, classPK,
                    resourceClassNameId, templateKey, nameMap, descriptionMap, type, mode, langType, xsl, cacheable,
                    smallImage, smallImageURL, smallImageFile, serviceContext);
            log.info("Template '" + templateKey + "' inserted.");
        } catch (PortalException e) {
            log.warn("Template '" + templateKey + "' already existed, start update.");
            DDMTemplate template = DDMTemplateLocalServiceUtil.getTemplate(StartupUtil.getGroupId(), classNameId, templateKey);
            template.setScript(xsl);
            DDMTemplateLocalServiceUtil.updateDDMTemplate(template);
            log.info("Template '" + templateKey + "' updated.");
        }
    }

    @Override
    String getResourceFolder() {
        return TEMPLATE_PATH;
    }

    private static Log log = LogFactoryUtil.getLog(TemplateCreator.class);

}