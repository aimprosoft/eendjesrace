package be.lions.duckrace.startup.type;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import be.lions.duckrace.startup.type.LiferayTemplate.Language;
import be.lions.duckrace.startup.type.LiferayStructureField.FieldType;

import com.liferay.dynamic.data.mapping.kernel.DDMForm;
import com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMTemplateManagerUtil;

//import com.liferay.dynamic.data.mapping.kernel.*;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portletdisplaytemplate.BasePortletDisplayTemplateHandler;
import com.liferay.portal.kernel.service.ServiceContext;

import com.liferay.portal.kernel.template.*;
//import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLocalServiceUtil;
//import com.liferay.portlet.dynamicdatamapping.service.DDMTemplateLocalServiceUtil;
//import com.liferay.portlet.journal.DuplicateStructureIdException;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;


/**
 * TODO:
 * - Nested fields
 * - Structure inheritance
 */
public class LiferayStructure {

	private static final Language DEFAULT_SCRIPT_LANGUAGE = Language.VELOCITY;
	
	private LiferayGroup group;
	private String id;
	private LiferayStructureFieldList fieldList;
	
	LiferayStructure(LiferayGroup group, String id, LiferayStructureFieldList fieldList) throws SystemException, PortalException {
		this.group = group;
		this.id = id;
		if(fieldList == null || fieldList.isEmpty()) {
			this.fieldList = new LiferayStructureFieldList();
			this.fieldList.addField("dummy", FieldType.TEXTFIELD);
		} else {
			this.fieldList = fieldList;
		}
		persist();
	}
	
	public LiferayTemplate addTemplate(Language lang, String script, boolean cached) throws PortalException, SystemException {
		int nbTemplates = DDMTemplateLocalServiceUtil.getTemplatesByStructureClassNameIdCount(group.getGroupId(), 0, WorkflowConstants.STATUS_ANY);
		String templateId = nbTemplates == 0 ? id : id+"_"+nbTemplates;
		return new LiferayTemplate(this, templateId, lang, script, cached);
	}
	
	public LiferayTemplate addTemplate(String script, boolean cached) throws PortalException, SystemException {
		return addTemplate(DEFAULT_SCRIPT_LANGUAGE, script, cached);
	}
	
	LiferayGroup getGroup() {
		return group;
	}
	
	String getId() {
		return id;
	}
	
	public List<LiferayStructureField> getFields() {
		return fieldList.getFields();
	}

	private void persist() throws SystemException, PortalException {
		long userId = group.getDefaultUserId();
		long groupId = group.getGroupId();

		boolean autoId = false;
		String parent = null;
		String xml = fieldList.getXml();
		ServiceContext sc = new ServiceContext();
		sc.setAddGroupPermissions(true);
		sc.setAddGuestPermissions(true);

		String parentStructureId = null;

		long classNameId = JournalArticleConstants.CLASSNAME_ID_DEFAULT;
		String structureKey = id;
		String storageType = null;
		int type = 0;
		Map<Locale,String> nameMap = new HashMap<>();
		Map<Locale,String> descriptionMap = new HashMap<>();
		nameMap.put(Locale.getDefault(), id);
		descriptionMap.put(Locale.getDefault(), id);
		DDMForm ddmForm = new DDMForm();

		try {
			DDMStructureManagerUtil.addStructure(userId, groupId, parentStructureId, classNameId, structureKey, nameMap, descriptionMap, ddmForm, storageType, type, sc);
			log.info("Structure '" + id + "' created.");
		} catch (PortalException e) {
			log.warn("Structure '" + id + "' already existed, updating...");
			DDMStructureManagerUtil.updateStructure(userId, 0, 0, nameMap, descriptionMap, ddmForm, sc);
			log.info("Structure '" + id + "' updated.");
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayStructure.class);
	
}
