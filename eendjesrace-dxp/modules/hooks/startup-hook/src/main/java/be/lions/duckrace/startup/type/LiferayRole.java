package be.lions.duckrace.startup.type;

import com.liferay.blogs.kernel.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.DuplicateRoleException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;

import java.util.*;

import static com.liferay.journal.constants.JournalContentPortletKeys.JOURNAL_CONTENT;
import static com.liferay.portal.kernel.security.permission.ActionKeys.*;
import static com.liferay.portal.kernel.util.PortletKeys.BLOGS;
import static com.liferay.portal.kernel.util.PortletKeys.DOCUMENT_LIBRARY;

public class LiferayRole {

    private LiferayCompany company;
    private String roleName;
    private RoleScope scope;

    protected LiferayRole(LiferayCompany company, String roleName, RoleScope scope) throws PortalException, SystemException {
        this.company = company;
        this.roleName = roleName;
        this.scope = scope;
        persist();
    }

    public void addControlPanelAccess(String... portletIds) throws SystemException, PortalException {
        for (String portletId : portletIds) {
            setGlobalPermissions(portletId, ActionKeys.ACCESS_IN_CONTROL_PANEL);
        }
    }

    public void setGlobalPermissions(String name, String... actionIds) throws SystemException, PortalException {
        int scope = ResourceConstants.SCOPE_COMPANY;
        String primKey = String.valueOf(getCompanyId());
        setPermissions(scope, primKey, name, actionIds);
    }

    public void setGlobalPermissions(Class<?> clazz, String... actionIds) throws SystemException, PortalException {
        setGlobalPermissions(clazz.getName(), actionIds);
    }

    private void setPermissions(int scope, String primKey, String name, String[] actionIds) throws PortalException, SystemException {
        getOrCreateResource(Arrays.asList(actionIds), name);
        ResourcePermissionLocalServiceUtil.setResourcePermissions(getCompanyId(), name, scope, primKey, getRole().getRoleId(), actionIds);
    }

    private void getOrCreateResource(List<String> actionId, String name) throws PortalException, SystemException {
        ResourceActionLocalServiceUtil.checkResourceActions(name, actionId);
    }

    private long getCompanyId() {
        return company.getCompanyId();
    }

    private void persist() throws PortalException, SystemException {
        long creatorId = company.getDefaultUserId();
        Map<Locale, String> titleMap = new HashMap<>();
        Map<Locale, String> descriptionMap = new HashMap<>();
        titleMap.put(Locale.getDefault(), roleName);
        descriptionMap.put(Locale.getDefault(), roleName);
        try {
            RoleLocalServiceUtil.addRole(creatorId, "", 0, roleName, titleMap, descriptionMap, scope.getType(), "", null);
            log.info("Role " + roleName + " was created.");
        } catch (DuplicateRoleException e) {
            log.warn("Role " + roleName + " already existed.");
        }
    }

    public Role getRole() throws PortalException, SystemException {
        return RoleLocalServiceUtil.getRole(getCompanyId(), roleName);
    }

    public enum RoleScope {
        COMPANY(RoleConstants.TYPE_REGULAR),
        COMMUNITY(RoleConstants.TYPE_SITE),
        ORGANIZATION(RoleConstants.TYPE_ORGANIZATION);
        private int type;

        private RoleScope(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    public enum Permission {
        // Web content
        CONTENT_CONTROL_PANEL(JOURNAL_CONTENT, ACCESS_IN_CONTROL_PANEL),
        CREATE_ARTICLE("com.liferay.journal.model.JournalArticle", ADD_ARTICLE),
//		CREATE_ARTICLE("com.liferay.portlet.journal", ADD_ARTICLE),

        //		APPROVE_ARTICLE("com.liferay.portlet.journal", ActionKeys.APPROVE_ARTICLE),
        VIEW_ARTICLE(JournalArticle.class, VIEW),
        DELETE_ARTICLE(JournalArticle.class, DELETE),
        EXPIRE_ARTICLE(JournalArticle.class, EXPIRE),
        UPDATE_ARTICLE(JournalArticle.class, UPDATE),
        DELETE_ARTICLE_COMMENT(JournalArticle.class, DELETE_DISCUSSION),
        UPDATE_ARTICLE_COMMENT(JournalArticle.class, UPDATE_DISCUSSION),

        // Image Gallery
//		IMAGES_CONTROL_PANEL(IMAGE_GALLERY, ACCESS_IN_CONTROL_PANEL),
//		ADD_IMG_FOLDER("com.liferay.portlet.imagegallery", ADD_FOLDER),
//		UPDATE_IMG_FOLDER(IGFolder.class, UPDATE),
//		DELETE_IMG_FOLDER(IGFolder.class, DELETE),
//		VIEW_IMG_FOLDER(IGFolder.class, VIEW),
//		ADD_IMG_SUBFOLDER(IGFolder.class, ADD_SUBFOLDER),
//		ADD_IMAGE(IGFolder.class, ActionKeys.ADD_IMAGE),
//		UPDATE_IMAGE(IGImage.class, UPDATE),
//		DELETE_IMAGE(IGImage.class, DELETE),
//		VIEW_IMAGE(IGImage.class, VIEW),

        // Document library
        DOCUMENTS_CONTROL_PANEL(DOCUMENT_LIBRARY, ACCESS_IN_CONTROL_PANEL),
        ADD_DOC_FOLDER("com.liferay.portlet.documentlibrary", ADD_FOLDER),
        UPDATE_DOC_FOLDER(DLFolder.class, UPDATE),
        DELETE_DOC_FOLDER(DLFolder.class, DELETE),
        ADD_DOC_SUBFOLDER(DLFolder.class, ADD_SUBFOLDER),
        VIEW_DOC_FOLDER(DLFolder.class, VIEW),
        ADD_DOCUMENT(DLFolder.class, ActionKeys.ADD_DOCUMENT),
        UPDATE_DOCUMENT(DLFileEntry.class, UPDATE),
        DELETE_DOCUMENT(DLFileEntry.class, DELETE),
        VIEW_DOCUMENT(DLFileEntry.class, VIEW),

        // Blogs
        BLOGS_CONTROL_PANEL(BLOGS, ACCESS_IN_CONTROL_PANEL),
        ADD_BLOG("com.liferay.portlet.blogs", ADD_ENTRY),
        DELETE_BLOG(BlogsEntry.class, DELETE),
        DELETE_BLOG_COMMENT(BlogsEntry.class, DELETE_DISCUSSION),
        UPDATE_BLOG(BlogsEntry.class, UPDATE),
        UPDATE_BLOG_COMMENT(BlogsEntry.class, UPDATE_DISCUSSION);

        // Tags and categories
//		TAGS_CONTROL_PANEL(TAGS_ADMIN, ACCESS_IN_CONTROL_PANEL),
//		VIEW_TAGS(TagsEntry.class, VIEW),
//		VIEW_TAGSETS(TagsVocabulary.class, VIEW);

        private String subject;
        private String action;

        private Permission(String subject, String action) {
            this.subject = subject;
            this.action = action;
        }

        private Permission(Class<?> clazz, String action) {
            this(clazz.getName(), action);
        }

        public String getSubject() {
            return subject;
        }

        public String getAction() {
            return action;
        }
    }

    private static Log log = LogFactoryUtil.getLog(LiferayRole.class);

}
