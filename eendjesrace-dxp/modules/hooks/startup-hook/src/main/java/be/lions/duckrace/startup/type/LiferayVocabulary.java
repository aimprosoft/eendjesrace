package be.lions.duckrace.startup.type;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetTagLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;

public abstract class LiferayVocabulary {

	private String setName;
	private LiferayGroup group;

	protected LiferayVocabulary(LiferayGroup group, String name) throws PortalException, SystemException {
		this.group = group;
		this.setName = name;
		persist();
	}
	
	private void persist() throws PortalException, SystemException {
		try {
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(group.getGroupId());
			AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.addVocabulary(group.getDefaultUserId(), group.getGroupId(), setName, new ServiceContext());
			AssetCategoryLocalServiceUtil.addCategory(group.getDefaultUserId(), group.getGroupId(), setName, assetVocabulary.getVocabularyId(), new ServiceContext());
			log.info(getName()+" '"+setName+"' was created.");
		} catch(PortalException e) {
			log.warn(getName()+" '"+setName+"' already existed.");
		}
	}
	
	public void addEntry(String entryName) throws PortalException, SystemException {
		try {
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(group.getGroupId());
			AssetTagLocalServiceUtil.addTag(group.getDefaultUserId(), group.getGroupId(), entryName, sc);
			log.info("Entry '"+entryName+"' was created inside vocabulary '"+setName+"'.");
		} catch(PortalException e) {
			log.warn("Entry '"+entryName+"' already existed in vocabulary '"+setName+"'.");
		}
	}
	
	protected abstract boolean isFolksonomy();
	protected abstract String getName();
	
	private static Log log = LogFactoryUtil.getLog(LiferayVocabulary.class);
	
}
