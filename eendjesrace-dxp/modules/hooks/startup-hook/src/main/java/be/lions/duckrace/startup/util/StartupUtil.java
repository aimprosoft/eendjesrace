package be.lions.duckrace.startup.util;

import be.lions.duckrace.service.DuckRaceLocalServiceUtil;

import com.liferay.portal. kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class StartupUtil {

	public static long getCompanyId() throws SystemException {
		return DuckRaceLocalServiceUtil.getCompanyId();
	}
	
	public static long getAdminId() throws PortalException, SystemException {
		return DuckRaceLocalServiceUtil.getAdminId();
	}
	
	public static long getGroupId() throws PortalException, SystemException {
		return DuckRaceLocalServiceUtil.getGroupId();
	}
	
}
