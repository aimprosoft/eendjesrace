package be.lions.duckrace.startup.type.role;

import be.lions.duckrace.startup.type.LiferayCompany;
import be.lions.duckrace.startup.type.LiferayRole;
import be.lions.duckrace.types.DuckRaceRole;

import com.liferay.blogs.kernel.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortletKeys;
//import com.liferay.portal.util.PortletKeys;
//import com.liferay.portlet.blogs.model.BlogsEntry;
//import com.liferay.portlet.documentlibrary.model.DLFileEntry;
//import com.liferay.portlet.documentlibrary.model.DLFolder;
//import com.liferay.portlet.journal.model.JournalArticle;
import static com.liferay.portal.kernel.security.permission.ActionKeys.*;

public class WebmasterRole extends LiferayRole {

	public WebmasterRole() throws PortalException, SystemException {
		super(LiferayCompany.getFirst(), DuckRaceRole.WEBMASTER.getName(), RoleScope.COMPANY);
		addWebContentPermissions();
//		addImageGalleryPermissions();
		addDocumentLibraryPermissions();
//		addTagPermissions();
		addBlogsPermissions();
	}
	
	private void addWebContentPermissions() throws SystemException, PortalException {
		setGlobalPermissions("com.liferay.portlet.journal", ADD_ARTICLE);
		setGlobalPermissions(JournalArticle.class.getName(), VIEW, DELETE, EXPIRE, UPDATE);
		setGlobalPermissions(JournalArticle.class.getName(), DELETE_DISCUSSION, UPDATE_DISCUSSION);
	}
	
//	private void addImageGalleryPermissions() throws PortalException, SystemException {
//		addControlPanelAccess(PortletKeys.IMAGE_GALLERY);
//		setGlobalPermissions("com.liferay.portlet.imagegallery", ADD_FOLDER);
//		setGlobalPermissions(IGFolder.class.getName(), UPDATE, DELETE, ADD_SUBFOLDER, ADD_IMAGE, VIEW);
//		setGlobalPermissions(IGImage.class.getName(), UPDATE, DELETE, VIEW);
//	}
	
	private void addDocumentLibraryPermissions() throws PortalException, SystemException {
		addControlPanelAccess(PortletKeys.DOCUMENT_LIBRARY);
		setGlobalPermissions("com.liferay.portlet.documentlibrary", ADD_FOLDER);
		setGlobalPermissions(DLFolder.class.getName(), UPDATE, DELETE, ADD_SUBFOLDER, ADD_DOCUMENT, VIEW);
		setGlobalPermissions(DLFileEntry.class.getName(), UPDATE, DELETE, VIEW);
	}
	
	private void addBlogsPermissions() throws SystemException, PortalException {
		addControlPanelAccess(PortletKeys.BLOGS);
		setGlobalPermissions("com.liferay.portlet.blogs", ADD_ENTRY);
		setGlobalPermissions(BlogsEntry.class.getName(), DELETE, DELETE_DISCUSSION, UPDATE, UPDATE_DISCUSSION);
	}
	
//	private void addTagPermissions() throws PortalException, SystemException {
//		setGlobalPermissions(TagsEntry.class.getName(), VIEW);
//		setGlobalPermissions(TagsVocabulary.class.getName(), VIEW);
//	}
	
}
