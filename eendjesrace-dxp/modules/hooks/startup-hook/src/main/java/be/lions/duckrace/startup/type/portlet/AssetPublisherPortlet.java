package be.lions.duckrace.startup.type.portlet;

import be.lions.duckrace.startup.type.LiferayPortlet;
import be.lions.duckrace.util_service_api.StringUtils;
import com.liferay.blogs.kernel.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.journal.model.JournalArticle;
import com.liferay.message.boards.kernel.model.MBMessage;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;

import java.util.HashMap;
import java.util.Map;

public class AssetPublisherPortlet extends LiferayPortlet {

	private static final int DEFAULT_NO_ITEMS_PER_PAGE = 25;

	public AssetPublisherPortlet() {
		super("com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_hUvc3pz8wsDD");
	}
	
	private String entries = "";
	private DisplayStyle displayStyle = DisplayStyle.FULL_CONTENT;
	private AssetType assetType = AssetType.ALL;
	private boolean mergeWithUrlTags;
	private boolean showContextLink;
	private boolean showAssetTitle;
	private int nbItemsPerPage = DEFAULT_NO_ITEMS_PER_PAGE;
	private OrderByField orderByField = OrderByField.PUBLISH_DATE;
	private OrderByType orderByType = OrderByType.ASC;
	private boolean commentsEnabled;
	private Pagination pagination = Pagination.SIMPLE;

	public AssetType getAssetType() {
		return assetType;
	}

	public void setAssetType(AssetType assetType) {
		this.assetType = assetType;
	}
	
	public boolean isCommentsEnabled() {
		return commentsEnabled;
	}

	public void setCommentsEnabled(boolean commentsEnabled) {
		this.commentsEnabled = commentsEnabled;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public OrderByField getOrderByField() {
		return orderByField;
	}

	public void setOrderByField(OrderByField orderByField) {
		this.orderByField = orderByField;
	}

	public OrderByType getOrderByType() {
		return orderByType;
	}

	public void setOrderByType(OrderByType orderByType) {
		this.orderByType = orderByType;
	}

	public int getNbItemsPerPage() {
		return nbItemsPerPage;
	}

	public void setNbItemsPerPage(int nbItemsPerPage) {
		this.nbItemsPerPage = nbItemsPerPage;
	}

	public String getEntries() {
		return entries;
	}

	public void setEntries(String entries) {
		this.entries = entries;
	}

	public DisplayStyle getDisplayStyle() {
		return displayStyle;
	}

	public void setDisplayStyle(DisplayStyle displayStyle) {
		this.displayStyle = displayStyle;
	}

	public boolean isMergeWithUrlTags() {
		return mergeWithUrlTags;
	}

	public void setMergeWithUrlTags(boolean mergeWithUrlTags) {
		this.mergeWithUrlTags = mergeWithUrlTags;
	}

	public boolean isShowContextLink() {
		return showContextLink;
	}

	public void setShowContextLink(boolean showContextLink) {
		this.showContextLink = showContextLink;
	}

	public boolean isShowAssetTitle() {
		return showAssetTitle;
	}

	public void setShowAssetTitle(boolean showAssetTitle) {
		this.showAssetTitle = showAssetTitle;
	}

	@Override
	protected Map<String, String> getPreferences() {
		Map<String, String> result = new HashMap<String, String>();
		result.put("entries", getEntries());
		result.put("display-style", getDisplayStyle().getPrefKey());
		result.put("class-name-id", getAssetType().getValue());
		result.put("merge-url-tags",  String.valueOf(isMergeWithUrlTags()));
		result.put("show-context-link", String.valueOf(isShowContextLink()));
		result.put("show-asset-title", String.valueOf(isShowAssetTitle()));
		result.put("delta", String.valueOf(getNbItemsPerPage()));
		result.put("order-by-type-1", getOrderByType().name());
		result.put("order-by-column-1", getOrderByField().getName());
		result.put("enable-comments", String.valueOf(isCommentsEnabled()));
		result.put("pagination-type", getPagination().getName());
		return result;
	}
	
	public enum DisplayStyle {
		TABLE, TITLE_LIST, ABSTRACTS, FULL_CONTENT;
		
		public String getPrefKey() {
			return StringUtils.fromEnumToName(name());
		}
	}
	
	public enum OrderByField {
		TITLE("title"),
		CREATION_DATE("createDate"),
		MODIFIED_DATE("modifiedDate"),
		PUBLISH_DATE("publishDate"),
		EXPIRATION_DATE("expirationDate"),
		PRIORITY("priority"),
		VIEW_COUNT("viewCount");
		
		private String name;
		
		private OrderByField(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
	
	public enum OrderByType {
		ASC, DESC;
	}
	
	public enum Pagination {
		NONE("none"),
		SIMPLE("simple"),
		REGULAR("regular");
		
		private String type;
		
		private Pagination(String type) {
			this.type = type;
		}
		
		public String getName() {
			return type;
		}
	}
	
	public enum AssetType {
		ALL,
		BLOGS(BlogsEntry.class),
//		BOOKMARKS(BookmarksEntry.class),
		DOCUMENTS(DLFileEntry.class),
// 		IMAGES(IGImage.class),
		WEB_CONTENT(JournalArticle.class),
		FORUMS(MBMessage.class);
//		WIKI(WikiPage.class);
		
		private String value;
		
		private AssetType() {
			value = "";
		}
		
		private AssetType(Class<?> clazz) {
			value = String.valueOf(ClassNameLocalServiceUtil.getClassNameId(clazz));
		}
		
		public String getValue() {
			return value;
		}
	}

}
