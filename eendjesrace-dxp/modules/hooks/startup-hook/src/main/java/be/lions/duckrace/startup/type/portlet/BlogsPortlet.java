package be.lions.duckrace.startup.type.portlet;

import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.util.PortletKeys;

import be.lions.duckrace.startup.type.LiferayPortlet;

public class BlogsPortlet extends LiferayPortlet {

	private static final int DEFAULT_NO_BLOGS = 10;

	public BlogsPortlet() {
		super(PortletKeys.BLOGS);
	}
	
	private boolean comments;
	private boolean flags;
	private boolean commentRatings;
	private boolean ratings;
	private int numberOfItems = DEFAULT_NO_BLOGS;
	
	public boolean isComments() {
		return comments;
	}

	public void setComments(boolean comments) {
		this.comments = comments;
	}

	public boolean isFlags() {
		return flags;
	}

	public void setFlags(boolean flags) {
		this.flags = flags;
	}

	public boolean isCommentRatings() {
		return commentRatings;
	}

	public void setCommentRatings(boolean commentRatings) {
		this.commentRatings = commentRatings;
	}

	public boolean isRatings() {
		return ratings;
	}

	public void setRatings(boolean ratings) {
		this.ratings = ratings;
	}

	public int getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	@Override
	protected Map<String, String> getPreferences() {
		Map<String, String> result = new HashMap<String, String>();
		result.put("enable-comments", String.valueOf(isComments()));
		result.put("enable-flags", String.valueOf(isFlags()));
		result.put("enable-comment-ratings", String.valueOf(isCommentRatings()));
		result.put("enable-ratings", String.valueOf(isRatings()));
		result.put("page-delta", String.valueOf(getNumberOfItems()));
		return result;
	}

}
