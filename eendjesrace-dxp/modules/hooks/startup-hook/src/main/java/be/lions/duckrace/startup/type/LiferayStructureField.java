package be.lions.duckrace.startup.type;



//TODO: nested structures
public class LiferayStructureField {

	private String name;
	private FieldType type;
	private boolean repeatable;
	
	LiferayStructureField(String name, FieldType type, boolean repeatable) {
		this.type = type;
		this.repeatable = repeatable;
	}
	
	String getTypeId() {
		return type.getId();
	}
	
	boolean isRepeatable() {
		return repeatable;
	}
	
	String getName() {
		return name;
	}
	
	public enum FieldType {
		TEXTFIELD("text"),
		TEXTAREA("textarea"),
		HTML_AREA("textarea"),

		IMAGE("image"),
		IMAGE_FROM_GALLERY("image_gallery"),
		DOCUMENT_FROM_LIBRARY("document_library"),
		BOOLEAN("boolean"),
		DROPDOWN_LIST("list"),
		MULTISELECT_LIST("multi-list"),
		LINK_TO_PAGE("link_to_layout");
		
		private String id;
		private FieldType(String id) {
			this.id = id;
		}
		public String getId() {
			return id;
		}
	}
	
}
