package be.lions.duckrace.startup.type.portlet;

import java.util.HashMap;
import java.util.Map;

import be.lions.duckrace.startup.type.LiferayPortlet;
import com.liferay.portal.kernel.util.PortletKeys;


public class WebContentPortlet extends LiferayPortlet {
	
	public WebContentPortlet(long groupId, String articleId) {
		super("com_liferay_journal_content_web_portlet_JournalContentPortlet_INSTANCE_FBdftNczmh7z");
		this.groupId = groupId;
		this.articleId = articleId;
	}

	private long groupId;
	private String articleId;
	private boolean comments;
	
	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public boolean isComments() {
		return comments;
	}

	public void setComments(boolean comments) {
		this.comments = comments;
	}

	@Override
	protected Map<String, String> getPreferences() {
		Map<String, String> result = new HashMap<String, String>();
		result.put("group-id", ""+groupId);
		result.put("article-id", articleId);
		result.put("enable-comments", String.valueOf(comments));
		return result;
	}
	
}
