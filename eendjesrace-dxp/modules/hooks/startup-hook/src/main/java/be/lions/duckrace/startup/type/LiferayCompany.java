package be.lions.duckrace.startup.type;

import be.lions.duckrace.startup.type.LiferayRole.RoleScope;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

public class LiferayCompany {

	private long companyId;
	
	public static LiferayCompany getFirst() throws SystemException {
		return new LiferayCompany(CompanyLocalServiceUtil.getCompanies().get(0).getCompanyId());
	}
	
	public static LiferayCompany get(long companyId) throws PortalException, SystemException {
		return new LiferayCompany(CompanyLocalServiceUtil.getCompanyById(companyId).getCompanyId());
	}
	
	private LiferayCompany(long companyId) {
		this.companyId = companyId;
	}
	
	long getCompanyId() {
		return companyId;
	}
	
	long getDefaultUserId() throws PortalException, SystemException {
		return UserLocalServiceUtil.getDefaultUserId(companyId);
	}
	
	/*
	 * Roles
	 */
	
	public LiferayRole addRole(String name, RoleScope scope) throws PortalException, SystemException {
		return new LiferayRole(this, name, scope);
	}
	
	public LiferayCommunity addCommunity(String name) throws PortalException, SystemException {
		return new LiferayCommunity(this, name);
	}
	
	public LiferayOrganization addOrganization(String name) throws PortalException, SystemException {
		return new LiferayOrganization(this, name);
	}
	
	public LiferayRole getGuestRole() throws PortalException, SystemException {
		return new LiferayRole(this, RoleConstants.GUEST, RoleScope.COMPANY);
	}
	
	
	public LiferayCommunity getGuestCommunity() throws PortalException, SystemException {
		return new LiferayCommunity(this, GroupConstants.GUEST);
	}
	
//	public LiferayUser getCompanyAdmin() throws SystemException, PortalException {
//		long adminRoleId = RoleLocalServiceUtil.getRole(companyId, RoleConstants.ADMINISTRATOR).getRoleId();
//		List<User> admins = UserLocalServiceUtil.getRoleUsers(adminRoleId);
//		if(admins.isEmpty()) {
//			throw new NoSuchUserException();
//		}
//		return new LiferayUser(admins.get(0).getUserId());
//	}
//	
//	public LiferayUser addUser(String email, String password, String firstName, String lastName) throws PortalException, SystemException {
//		return new LiferayUser(this, email, password, firstName, lastName);
//	}
	
}
