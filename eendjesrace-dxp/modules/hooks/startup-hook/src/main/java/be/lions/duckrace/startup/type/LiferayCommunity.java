package be.lions.duckrace.startup.type;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LiferayCommunity extends LiferayGroup {

	LiferayCommunity(LiferayCompany company, String name) throws PortalException, SystemException {
		super(company, name);
	}

	protected void persist() throws PortalException, SystemException {
		long userId = UserLocalServiceUtil.getDefaultUserId(getCompanyId());
		long parentGroupId = 0;
		String className = Group.class.getName();
		long classPK = 0;
		long liveGroupId = 0;
		Map<Locale, String> nameMap = new HashMap<>();
		Map<Locale, String> descriptionMap = new HashMap<>();
		nameMap.put(Locale.getDefault(), getName());
		descriptionMap.put(Locale.getDefault(), getName());
		int type = GroupConstants.TYPE_SITE_OPEN;
		boolean manualMembership = true;
		int membershipRestriction = GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION;
		String friendlyURL = "";
		boolean site = true;
		boolean inheritContent = false;
		boolean active = true;
		ServiceContext serviceContext = new ServiceContext();

		try {
			GroupLocalServiceUtil.addGroup(userId, parentGroupId, className, classPK, liveGroupId, nameMap, descriptionMap,
					type, manualMembership, membershipRestriction, friendlyURL, site, inheritContent, active, serviceContext);
			log.info("Community '"+getName()+"' created.");
		} catch(PortalException e) {
			log.warn("Community '"+getName()+"' already existed.");
		}
	}

	public long getGroupId() throws PortalException, SystemException {
		return GroupLocalServiceUtil.getGroup(getCompanyId(), getName()).getGroupId();
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayCommunity.class);

}
