package be.lions.duckrace.startup.type;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;

public class LiferayImageFolder {

	private LiferayImageFolder parent;
	private LiferayGroup group;
	private String name;
	private String description;
	
	private LiferayImageFolder(LiferayGroup group, LiferayImageFolder parent, String name, String description) throws PortalException, SystemException {
		this.group = group;
		this.name = name;
		this.description = description;
		this.parent = parent;
		persist();
	}
	
	LiferayImageFolder(LiferayGroup group, String name, String description) throws PortalException, SystemException {
		this(group, null, name, description);
	}
	
	public LiferayImageFolder addChildFolder(String name, String description) throws PortalException, SystemException {
		return new LiferayImageFolder(group, this, name, description);
	}
	
	private long getParentFolderId() throws PortalException, SystemException {
		if(parent == null) {
			return DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		}
		return parent.getFolderId();
	}
	
	private long getFolderId() throws PortalException, SystemException {
		return DLAppLocalServiceUtil.getFolder(group.getGroupId(), getParentFolderId(), name).getFolderId();
	}

	private void persist() throws PortalException, SystemException {
		ServiceContext sc = new ServiceContext();
		sc.setScopeGroupId(group.getGroupId());
		sc.setUserId(group.getDefaultUserId());
		try {
			DLAppLocalServiceUtil.addFolder(group.getDefaultUserId(), group.getGroupId(), getParentFolderId(), name, description, sc);
			log.info("Image folder '"+name+"' created.");
		} catch(PortalException | SystemException e) {
			log.warn("Image folder '"+name+"' already exists, updating...");
			DLAppLocalServiceUtil.updateFolder(getFolderId(), getParentFolderId(), name, description, sc);
			log.info("Image folder '"+name+"' updated.");
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayImageFolder.class);
	
}
