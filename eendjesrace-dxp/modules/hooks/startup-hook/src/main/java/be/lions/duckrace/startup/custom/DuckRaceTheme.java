package be.lions.duckrace.startup.custom;

public enum DuckRaceTheme {

	THEME("eendjes_WAR_eendjestheme"),
	
	NORMAL_SCHEME("normal"),
	BANNERLESS_SCHEME("bannerless");
	
	
	private String id;
	
	private DuckRaceTheme(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
}
