package be.lions.duckrace.startup.type;

import com.liferay.portal.kernel.exception.DuplicateOrganizationException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.OrganizationConstants;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

public class LiferayOrganization extends LiferayGroup {

	private static final int STATUS_FULL_MEMBER = 12017;
	
	protected LiferayOrganization(LiferayCompany company, String name) throws PortalException, SystemException {
		super(company, name);
	}

	@Override
	public long getGroupId() throws PortalException, SystemException {
		return OrganizationLocalServiceUtil.getOrganization(getCompanyId(), getName()).getGroup().getGroupId();
	}

	@Override
	protected void persist() throws PortalException, SystemException {
		long creatorId = UserLocalServiceUtil.getDefaultUserId(getCompanyId());
		long parentOrgId = 0;
		String type = OrganizationConstants.TYPE_REGULAR_ORGANIZATION;
		boolean recursable = true;
		long regionId = 0;
		long countryId = 0;
		int statusId = STATUS_FULL_MEMBER;
		String comments = null;
		ServiceContext serviceContext = null;

		try {
			OrganizationLocalServiceUtil.addOrganization(creatorId, parentOrgId, getName(), type, regionId, countryId, statusId, comments, recursable, serviceContext);
			log.info("Organization '" + getName() + "' created");
		} catch (DuplicateOrganizationException e) {
			OrganizationLocalServiceUtil.getOrganization(getCompanyId(), getName());
			log.warn("Organization '" + getName() + "' already existed");
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayOrganization.class);

}
