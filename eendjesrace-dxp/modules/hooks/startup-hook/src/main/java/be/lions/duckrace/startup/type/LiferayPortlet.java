package be.lions.duckrace.startup.type;

import java.util.HashMap;
import java.util.Map;

public abstract class LiferayPortlet {

	private String portletId;
	
	public LiferayPortlet(String portletId) {
		this.portletId = portletId;
	}
	
	public String getPortletId() {
		return portletId;
	}
	
	protected Map<String, String> getPreferences() {
		return new HashMap<String, String>();
	}
	
}
