package be.lions.duckrace.startup.type.portlet;

import be.lions.duckrace.startup.type.LiferayPortlet;

import com.liferay.portal.kernel.util.PortletKeys;

public class LoginPortlet extends LiferayPortlet {

	public LoginPortlet() {
		super(PortletKeys.LOGIN);
	}

}
