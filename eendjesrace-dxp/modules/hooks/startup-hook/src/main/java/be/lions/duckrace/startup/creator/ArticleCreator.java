package be.lions.duckrace.startup.creator;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import be.lions.duckrace.startup.custom.LiferayArticle;
import be.lions.duckrace.startup.util.StartupUtil;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.journal.exception.DuplicateArticleIdException;


public class ArticleCreator {

	private static Log log = LogFactoryUtil.getLog(ArticleCreator.class);

	public void insertArticles() throws PortalException, SystemException, IOException {
		for (LiferayArticle article: LiferayArticle.values()) {
			insertArticle(article);
		}
	}
	
	private void insertArticle(LiferayArticle article) throws IOException, PortalException, SystemException {
		String articleId = article.name();
		boolean autoArticleId = false;
		String content = getContent(article);
		long userId = StartupUtil.getAdminId();
		long groupId = StartupUtil.getGroupId();
		int displayDateMonth = Calendar.JANUARY;
		int displayDateDay = 1;
		int displayDateYear = 1900;
		int displayDateHour = 12;
		int displayDateMinute = 0;
		int expirationDateMonth = Calendar.JANUARY;
		int expirationDateDay = 1;
		int expirationDateYear = 2200;
		int expirationDateHour = 12;
		int expirationDateMinute = 0;
		boolean neverExpire = true;
		int reviewDateMonth = Calendar.JANUARY;
		int reviewDateDay = 1;
		int reviewDateYear = 2200;
		int reviewDateHour = 12;
		int reviewDateMinute = 0;
		boolean neverReview = true;
		boolean indexable = true;
		boolean smallImage = false;
		String smallImageURL = "";
		File smallFile = null;
		Map<String, byte[]> images = null;
		String articleURL = null;
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(StartupUtil.getGroupId());


		long classNameId = JournalArticleConstants.CLASSNAME_ID_DEFAULT;
		long classPK = 0;
		long folderId = 0;
		double version = 0;
		Map<Locale,String> titleMap = new HashMap<>();
		Map<Locale,String> descriptionMap = new HashMap<>();
		titleMap.put(Locale.getDefault(), articleId);
		descriptionMap.put(Locale.getDefault(), articleId);


		String ddmStructureKey = article.getStructureId();
		String ddmTemplateKey = article.getStructureId();
		String layoutUuid = null;
		java.io.File smallImageFile = null;

		try {
			JournalArticle article1 = JournalArticleLocalServiceUtil.addArticle(userId, groupId, folderId, classNameId , classPK, articleId, autoArticleId, version, titleMap, descriptionMap,
					content, ddmStructureKey, ddmTemplateKey, layoutUuid, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth,
					expirationDateDay, expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour,
					reviewDateMinute, neverReview, indexable, smallImage, smallImageURL, smallImageFile,images, articleURL, serviceContext);
			log.info("Article '" + articleId + "' successfully created.");
		} catch (DuplicateArticleIdException e) {
			log.info("Article '" + articleId + "' already existed.");
		}
	}

	private String getContent(LiferayArticle article) throws IOException, PortalException, SystemException {
		if(!article.hasStructure()) {
			String fileContent = null;
			try {
				fileContent = StringUtil.read(getClass().getClassLoader(), "wcm/articles/"+article.name()+".html");
			} catch(IOException e) {
				fileContent = "";
			}
			fileContent = StringUtil.replace(fileContent, "[$GROUP_ID$]", ""+StartupUtil.getGroupId());
			StringBuffer articleContentXml = new StringBuffer();
			articleContentXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			articleContentXml.append("<root available-locales=\"en_US\" default-locale=\"en_US\">");
			articleContentXml.append("<static-content language-id=\"en_US\"><![CDATA[" + fileContent + "]]></static-content>");
			articleContentXml.append("</root>");
			return articleContentXml.toString();
		}
		return StringUtil.read(getClass().getClassLoader(), "wcm/articles/"+article.getStructureId().toLowerCase()+"_dummy.html");
	}

}