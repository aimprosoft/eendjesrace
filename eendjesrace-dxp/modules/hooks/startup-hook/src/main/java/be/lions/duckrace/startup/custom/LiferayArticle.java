package be.lions.duckrace.startup.custom;


public enum LiferayArticle {

	HOME,

	UNDER_CONSTRUCTION,

	ORG_LIONS,
	ORG_HART_VOOR_LIMBURG,

	INFO_EVENEMENT,
	INFO_REGLEMENT,
	INFO_RACEVERLOOP,
	INFO_HOOFDPRIJS,
	INFO_OPBRENGST,
	
	LOGOS(LiferayStructure.LOGOS),
	DUCKTYPE_MAIN_SPONSORS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_100_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_50_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_20_DUCKS(LiferayStructure.DUCKTYPE),
	DUCKTYPE_10_DUCKS(LiferayStructure.DUCKTYPE),
	
	FOTO_ALBUM,
	PERSCONFERENTIES,
	REACTIES,
	
	EVENT_PROGRAMMA,
	EVENT_ACTS,
	EVENT_BEVERAGE,
	EVENT_BIJSTAND;
	
	private LiferayStructure structure;
	private String BASIC_WEB_CONTENT = "BASIC-WEB-CONTENT";
	
	private LiferayArticle() {
		this(null);
	}
	
	private LiferayArticle(LiferayStructure structure) {
		this.structure = structure;
	}
	
	public String getStructureId() {
		return structure == null ? BASIC_WEB_CONTENT : structure.name();
	}
	
	public boolean hasStructure() {
		if (BASIC_WEB_CONTENT.equals(getStructureId())) {
			return false;
		}
		return true;
	}
	
	public String getId() {
		return name();
	}
	
}
