package be.lions.duckrace.startup.type;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.DDMTemplateServiceUtil;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
//import com.liferay.portlet.dynamicdatamapping.model.DDMTemplate;
//import com.liferay.portlet.dynamicdatamapping.model.DDMTemplateConstants;
//import com.liferay.portlet.dynamicdatamapping.service.DDMTemplateLocalServiceUtil;

public class LiferayTemplate {

	private String id;
	private LiferayStructure structure;
	
	LiferayTemplate(LiferayStructure structure, String id, Language scriptingLanguage, String script, boolean cached) throws PortalException, SystemException {
		this.id = id;
		this.structure = structure;
		persist(scriptingLanguage, script, cached);
	}
	
	public LiferayArticle addArticle(String id, String title, String description, Map<String, String> values, boolean override) throws PortalException, SystemException {
		return new LiferayArticle(this, id, title, description, values, override);
	}
	
	private void persist(Language scriptingLanguage, String script, boolean cached) throws PortalException, SystemException {
		LiferayGroup group = structure.getGroup();
		long userId = group.getDefaultUserId();
		long groupId = group.getGroupId();
		boolean autoTemplateId = false;
		boolean formatXsl = true;
		String langType = scriptingLanguage.getId();
		boolean smallImage = false;
		String smallImageURL = null;
		File smallFile = null;

		String templateKey = id;
		long classNameId = JournalArticleConstants.CLASSNAME_ID_DEFAULT;
		long classPK = 0;
		Map<Locale, String> nameMap = new HashMap<>();
		Map<Locale, String> descriptionMap = new HashMap<>();
		nameMap.put(Locale.getDefault(), id);
		descriptionMap.put(Locale.getDefault(), id);

		String type = DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY;
		String mode = DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY;

		ServiceContext sc = new ServiceContext();
		sc.setAddGroupPermissions(true);
		sc.setAddGuestPermissions(true);

		try {
			DDMTemplateLocalServiceUtil.addTemplate(userId, groupId, classNameId, classPK, 0, templateKey, nameMap, descriptionMap, type,
					mode, langType, script, cached, smallImage, smallImageURL, smallFile, sc);
			log.info("Template '" + id + "' created.");
		} catch (PortalException | SystemException e) {
			log.warn("Template '" + id + "' already existed, updating...");

			DDMTemplate ddmTemplate = DDMTemplateLocalServiceUtil.getTemplate(groupId, classNameId, id);
			long templateId = ddmTemplate.getTemplateId();
			DDMTemplateLocalServiceUtil.updateTemplate(userId, templateId, classPK, nameMap, descriptionMap, type, mode, langType,
					script, cached, smallImage, smallImageURL, smallFile, sc);
			log.info("Template '" + id + "' updated.");
		}
	}
	
	String getId() {
		return id;
	}
	
	LiferayStructure getStructure() {
		return structure;
	}

	public enum Language {
		VELOCITY("vm"),
		XSL("xsl"),
		CSS("css");
		
		private String id;
		private Language(String id) {
			this.id = id;
		}
		public String getId() {
			return id;
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayTemplate.class);
	
}
