package be.lions.duckrace.startup.type;

import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;

import java.util.ArrayList;

public class LiferayDocumentFolder {

	private LiferayDocumentFolder parent;
	private LiferayGroup group;
	private String name;
	private String description;
	
	private LiferayDocumentFolder(LiferayGroup group, LiferayDocumentFolder parent, String name, String description) throws PortalException, SystemException {
		this.group = group;
		this.name = name;
		this.description = description;
		this.parent = parent;
		persist();
	}
	
	LiferayDocumentFolder(LiferayGroup group, String name, String description) throws PortalException, SystemException {
		this(group, null, name, description);
	}
	
	public LiferayDocumentFolder addChildFolder(String name, String description) throws PortalException, SystemException {
		return new LiferayDocumentFolder(group, this, name, description);
	}
	
	private long getParentFolderId() throws PortalException, SystemException {
		if(parent == null) {
			return DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		}
		return parent.getFolderId();
	}
	
	private long getFolderId() throws PortalException, SystemException {
		return DLFolderLocalServiceUtil.getFolder(group.getGroupId(), getParentFolderId(), name).getFolderId();
	}

	private void persist() throws PortalException, SystemException {
		ServiceContext sc = new ServiceContext();
		sc.setScopeGroupId(group.getGroupId());
		try {
			DLFolderLocalServiceUtil.addFolder(group.getDefaultUserId(), group.getGroupId(), 0, false, getParentFolderId(), name, description, false, sc);
			log.info("Image folder '"+name+"' created.");
		} catch(PortalException | SystemException e) {
			log.warn("Image folder '"+name+"' already exists, updating...");
			DLFolderLocalServiceUtil.updateFolder(getFolderId(), getParentFolderId(), name, description,  0, new ArrayList<>(), false, sc);
			log.info("Image folder '"+name+"' updated.");
		}
	}
	
	private static Log log = LogFactoryUtil.getLog(LiferayImageFolder.class);
	
}