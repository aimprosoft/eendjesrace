/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LotSoap implements Serializable {
	public static LotSoap toSoapModel(Lot model) {
		LotSoap soapModel = new LotSoap();

		soapModel.setCode(model.getCode());
		soapModel.setChecksum(model.getChecksum());
		soapModel.setLotNumber(model.getLotNumber());
		soapModel.setNumber(model.getNumber());
		soapModel.setPressed(model.getPressed());
		soapModel.setNbTries(model.getNbTries());
		soapModel.setLockoutDate(model.getLockoutDate());
		soapModel.setParticipantId(model.getParticipantId());
		soapModel.setReservationId(model.getReservationId());
		soapModel.setPartnerId(model.getPartnerId());
		soapModel.setOrderNumber(model.getOrderNumber());

		return soapModel;
	}

	public static LotSoap[] toSoapModels(Lot[] models) {
		LotSoap[] soapModels = new LotSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LotSoap[][] toSoapModels(Lot[][] models) {
		LotSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LotSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LotSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LotSoap[] toSoapModels(List<Lot> models) {
		List<LotSoap> soapModels = new ArrayList<LotSoap>(models.size());

		for (Lot model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LotSoap[soapModels.size()]);
	}

	public LotSoap() {
	}

	public String getPrimaryKey() {
		return _code;
	}

	public void setPrimaryKey(String pk) {
		setCode(pk);
	}

	public String getCode() {
		return _code;
	}

	public void setCode(String code) {
		_code = code;
	}

	public String getChecksum() {
		return _checksum;
	}

	public void setChecksum(String checksum) {
		_checksum = checksum;
	}

	public int getLotNumber() {
		return _lotNumber;
	}

	public void setLotNumber(int lotNumber) {
		_lotNumber = lotNumber;
	}

	public int getNumber() {
		return _number;
	}

	public void setNumber(int number) {
		_number = number;
	}

	public boolean getPressed() {
		return _pressed;
	}

	public boolean isPressed() {
		return _pressed;
	}

	public void setPressed(boolean pressed) {
		_pressed = pressed;
	}

	public int getNbTries() {
		return _nbTries;
	}

	public void setNbTries(int nbTries) {
		_nbTries = nbTries;
	}

	public Date getLockoutDate() {
		return _lockoutDate;
	}

	public void setLockoutDate(Date lockoutDate) {
		_lockoutDate = lockoutDate;
	}

	public long getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(long participantId) {
		_participantId = participantId;
	}

	public long getReservationId() {
		return _reservationId;
	}

	public void setReservationId(long reservationId) {
		_reservationId = reservationId;
	}

	public long getPartnerId() {
		return _partnerId;
	}

	public void setPartnerId(long partnerId) {
		_partnerId = partnerId;
	}

	public String getOrderNumber() {
		return _orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		_orderNumber = orderNumber;
	}

	private String _code;
	private String _checksum;
	private int _lotNumber;
	private int _number;
	private boolean _pressed;
	private int _nbTries;
	private Date _lockoutDate;
	private long _participantId;
	private long _reservationId;
	private long _partnerId;
	private String _orderNumber;
}