/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Lot;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the lot service. This utility wraps {@link be.lions.duckrace.service.persistence.impl.LotPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotPersistence
 * @see be.lions.duckrace.service.persistence.impl.LotPersistenceImpl
 * @generated
 */
@ProviderType
public class LotUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Lot lot) {
		getPersistence().clearCache(lot);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Lot> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Lot> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Lot> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Lot update(Lot lot) {
		return getPersistence().update(lot);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Lot update(Lot lot, ServiceContext serviceContext) {
		return getPersistence().update(lot, serviceContext);
	}

	/**
	* Returns all the lots where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @return the matching lots
	*/
	public static List<Lot> findByParticipant(long participantId) {
		return getPersistence().findByParticipant(participantId);
	}

	/**
	* Returns a range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public static List<Lot> findByParticipant(long participantId, int start,
		int end) {
		return getPersistence().findByParticipant(participantId, start, end);
	}

	/**
	* Returns an ordered range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByParticipant(long participantId, int start,
		int end, OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .findByParticipant(participantId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByParticipant(long participantId, int start,
		int end, OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByParticipant(participantId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByParticipant_First(long participantId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByParticipant_First(participantId, orderByComparator);
	}

	/**
	* Returns the first lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByParticipant_First(long participantId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .fetchByParticipant_First(participantId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByParticipant_Last(long participantId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByParticipant_Last(participantId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByParticipant_Last(long participantId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .fetchByParticipant_Last(participantId, orderByComparator);
	}

	/**
	* Returns the lots before and after the current lot in the ordered set where participantId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public static Lot[] findByParticipant_PrevAndNext(java.lang.String code,
		long participantId, OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByParticipant_PrevAndNext(code, participantId,
			orderByComparator);
	}

	/**
	* Removes all the lots where participantId = &#63; from the database.
	*
	* @param participantId the participant ID
	*/
	public static void removeByParticipant(long participantId) {
		getPersistence().removeByParticipant(participantId);
	}

	/**
	* Returns the number of lots where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @return the number of matching lots
	*/
	public static int countByParticipant(long participantId) {
		return getPersistence().countByParticipant(participantId);
	}

	/**
	* Returns all the lots where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @return the matching lots
	*/
	public static List<Lot> findByReservation(long reservationId) {
		return getPersistence().findByReservation(reservationId);
	}

	/**
	* Returns a range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public static List<Lot> findByReservation(long reservationId, int start,
		int end) {
		return getPersistence().findByReservation(reservationId, start, end);
	}

	/**
	* Returns an ordered range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByReservation(long reservationId, int start,
		int end, OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .findByReservation(reservationId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByReservation(long reservationId, int start,
		int end, OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByReservation(reservationId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByReservation_First(long reservationId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByReservation_First(reservationId, orderByComparator);
	}

	/**
	* Returns the first lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByReservation_First(long reservationId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .fetchByReservation_First(reservationId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByReservation_Last(long reservationId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByReservation_Last(reservationId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByReservation_Last(long reservationId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .fetchByReservation_Last(reservationId, orderByComparator);
	}

	/**
	* Returns the lots before and after the current lot in the ordered set where reservationId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public static Lot[] findByReservation_PrevAndNext(java.lang.String code,
		long reservationId, OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByReservation_PrevAndNext(code, reservationId,
			orderByComparator);
	}

	/**
	* Removes all the lots where reservationId = &#63; from the database.
	*
	* @param reservationId the reservation ID
	*/
	public static void removeByReservation(long reservationId) {
		getPersistence().removeByReservation(reservationId);
	}

	/**
	* Returns the number of lots where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @return the number of matching lots
	*/
	public static int countByReservation(long reservationId) {
		return getPersistence().countByReservation(reservationId);
	}

	/**
	* Returns the lot where lotNumber = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param lotNumber the lot number
	* @return the matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByLotNumber(int lotNumber)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().findByLotNumber(lotNumber);
	}

	/**
	* Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param lotNumber the lot number
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByLotNumber(int lotNumber) {
		return getPersistence().fetchByLotNumber(lotNumber);
	}

	/**
	* Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param lotNumber the lot number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByLotNumber(int lotNumber, boolean retrieveFromCache) {
		return getPersistence().fetchByLotNumber(lotNumber, retrieveFromCache);
	}

	/**
	* Removes the lot where lotNumber = &#63; from the database.
	*
	* @param lotNumber the lot number
	* @return the lot that was removed
	*/
	public static Lot removeByLotNumber(int lotNumber)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().removeByLotNumber(lotNumber);
	}

	/**
	* Returns the number of lots where lotNumber = &#63;.
	*
	* @param lotNumber the lot number
	* @return the number of matching lots
	*/
	public static int countByLotNumber(int lotNumber) {
		return getPersistence().countByLotNumber(lotNumber);
	}

	/**
	* Returns the lot where number = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param number the number
	* @return the matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByNumber(int number)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().findByNumber(number);
	}

	/**
	* Returns the lot where number = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param number the number
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByNumber(int number) {
		return getPersistence().fetchByNumber(number);
	}

	/**
	* Returns the lot where number = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param number the number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByNumber(int number, boolean retrieveFromCache) {
		return getPersistence().fetchByNumber(number, retrieveFromCache);
	}

	/**
	* Removes the lot where number = &#63; from the database.
	*
	* @param number the number
	* @return the lot that was removed
	*/
	public static Lot removeByNumber(int number)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().removeByNumber(number);
	}

	/**
	* Returns the number of lots where number = &#63;.
	*
	* @param number the number
	* @return the number of matching lots
	*/
	public static int countByNumber(int number) {
		return getPersistence().countByNumber(number);
	}

	/**
	* Returns all the lots where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @return the matching lots
	*/
	public static List<Lot> findByPartner(long partnerId) {
		return getPersistence().findByPartner(partnerId);
	}

	/**
	* Returns a range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public static List<Lot> findByPartner(long partnerId, int start, int end) {
		return getPersistence().findByPartner(partnerId, start, end);
	}

	/**
	* Returns an ordered range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByPartner(long partnerId, int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .findByPartner(partnerId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public static List<Lot> findByPartner(long partnerId, int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByPartner(partnerId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByPartner_First(long partnerId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().findByPartner_First(partnerId, orderByComparator);
	}

	/**
	* Returns the first lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByPartner_First(long partnerId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence()
				   .fetchByPartner_First(partnerId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public static Lot findByPartner_Last(long partnerId,
		OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().findByPartner_Last(partnerId, orderByComparator);
	}

	/**
	* Returns the last lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public static Lot fetchByPartner_Last(long partnerId,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence().fetchByPartner_Last(partnerId, orderByComparator);
	}

	/**
	* Returns the lots before and after the current lot in the ordered set where partnerId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public static Lot[] findByPartner_PrevAndNext(java.lang.String code,
		long partnerId, OrderByComparator<Lot> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence()
				   .findByPartner_PrevAndNext(code, partnerId, orderByComparator);
	}

	/**
	* Removes all the lots where partnerId = &#63; from the database.
	*
	* @param partnerId the partner ID
	*/
	public static void removeByPartner(long partnerId) {
		getPersistence().removeByPartner(partnerId);
	}

	/**
	* Returns the number of lots where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @return the number of matching lots
	*/
	public static int countByPartner(long partnerId) {
		return getPersistence().countByPartner(partnerId);
	}

	/**
	* Caches the lot in the entity cache if it is enabled.
	*
	* @param lot the lot
	*/
	public static void cacheResult(Lot lot) {
		getPersistence().cacheResult(lot);
	}

	/**
	* Caches the lots in the entity cache if it is enabled.
	*
	* @param lots the lots
	*/
	public static void cacheResult(List<Lot> lots) {
		getPersistence().cacheResult(lots);
	}

	/**
	* Creates a new lot with the primary key. Does not add the lot to the database.
	*
	* @param code the primary key for the new lot
	* @return the new lot
	*/
	public static Lot create(java.lang.String code) {
		return getPersistence().create(code);
	}

	/**
	* Removes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param code the primary key of the lot
	* @return the lot that was removed
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public static Lot remove(java.lang.String code)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().remove(code);
	}

	public static Lot updateImpl(Lot lot) {
		return getPersistence().updateImpl(lot);
	}

	/**
	* Returns the lot with the primary key or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param code the primary key of the lot
	* @return the lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public static Lot findByPrimaryKey(java.lang.String code)
		throws be.lions.duckrace.exception.NoSuchLotException {
		return getPersistence().findByPrimaryKey(code);
	}

	/**
	* Returns the lot with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param code the primary key of the lot
	* @return the lot, or <code>null</code> if a lot with the primary key could not be found
	*/
	public static Lot fetchByPrimaryKey(java.lang.String code) {
		return getPersistence().fetchByPrimaryKey(code);
	}

	public static java.util.Map<java.io.Serializable, Lot> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the lots.
	*
	* @return the lots
	*/
	public static List<Lot> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of lots
	*/
	public static List<Lot> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lots
	*/
	public static List<Lot> findAll(int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of lots
	*/
	public static List<Lot> findAll(int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the lots from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of lots.
	*
	* @return the number of lots
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static LotPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LotPersistence, LotPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LotPersistence.class);
}