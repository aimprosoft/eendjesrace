/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Partner}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Partner
 * @generated
 */
@ProviderType
public class PartnerWrapper implements Partner, ModelWrapper<Partner> {
	public PartnerWrapper(Partner partner) {
		_partner = partner;
	}

	@Override
	public Class<?> getModelClass() {
		return Partner.class;
	}

	@Override
	public String getModelClassName() {
		return Partner.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("partnerId", getPartnerId());
		attributes.put("category", getCategory());
		attributes.put("name", getName());
		attributes.put("returnedLotsCount", getReturnedLotsCount());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long partnerId = (Long)attributes.get("partnerId");

		if (partnerId != null) {
			setPartnerId(partnerId);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer returnedLotsCount = (Integer)attributes.get("returnedLotsCount");

		if (returnedLotsCount != null) {
			setReturnedLotsCount(returnedLotsCount);
		}
	}

	@Override
	public be.lions.duckrace.model.Partner toEscapedModel() {
		return new PartnerWrapper(_partner.toEscapedModel());
	}

	@Override
	public be.lions.duckrace.model.Partner toUnescapedModel() {
		return new PartnerWrapper(_partner.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _partner.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _partner.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _partner.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _partner.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<be.lions.duckrace.model.Partner> toCacheModel() {
		return _partner.toCacheModel();
	}

	@Override
	public int compareTo(be.lions.duckrace.model.Partner partner) {
		return _partner.compareTo(partner);
	}

	@Override
	public int getAssignedLotsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partner.getAssignedLotsCount();
	}

	@Override
	public int getRegisteredLotsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partner.getRegisteredLotsCount();
	}

	/**
	* Returns the returned lots count of this partner.
	*
	* @return the returned lots count of this partner
	*/
	@Override
	public int getReturnedLotsCount() {
		return _partner.getReturnedLotsCount();
	}

	@Override
	public int hashCode() {
		return _partner.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _partner.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PartnerWrapper((Partner)_partner.clone());
	}

	/**
	* Returns the category of this partner.
	*
	* @return the category of this partner
	*/
	@Override
	public java.lang.String getCategory() {
		return _partner.getCategory();
	}

	/**
	* Returns the name of this partner.
	*
	* @return the name of this partner
	*/
	@Override
	public java.lang.String getName() {
		return _partner.getName();
	}

	@Override
	public java.lang.String toString() {
		return _partner.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _partner.toXmlString();
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Lot> getAssignedLots()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partner.getAssignedLots();
	}

	/**
	* Returns the partner ID of this partner.
	*
	* @return the partner ID of this partner
	*/
	@Override
	public long getPartnerId() {
		return _partner.getPartnerId();
	}

	/**
	* Returns the primary key of this partner.
	*
	* @return the primary key of this partner
	*/
	@Override
	public long getPrimaryKey() {
		return _partner.getPrimaryKey();
	}

	@Override
	public void persist() {
		_partner.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_partner.setCachedModel(cachedModel);
	}

	/**
	* Sets the category of this partner.
	*
	* @param category the category of this partner
	*/
	@Override
	public void setCategory(java.lang.String category) {
		_partner.setCategory(category);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_partner.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_partner.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_partner.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the name of this partner.
	*
	* @param name the name of this partner
	*/
	@Override
	public void setName(java.lang.String name) {
		_partner.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_partner.setNew(n);
	}

	/**
	* Sets the partner ID of this partner.
	*
	* @param partnerId the partner ID of this partner
	*/
	@Override
	public void setPartnerId(long partnerId) {
		_partner.setPartnerId(partnerId);
	}

	/**
	* Sets the primary key of this partner.
	*
	* @param primaryKey the primary key of this partner
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_partner.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_partner.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the returned lots count of this partner.
	*
	* @param returnedLotsCount the returned lots count of this partner
	*/
	@Override
	public void setReturnedLotsCount(int returnedLotsCount) {
		_partner.setReturnedLotsCount(returnedLotsCount);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PartnerWrapper)) {
			return false;
		}

		PartnerWrapper partnerWrapper = (PartnerWrapper)obj;

		if (Objects.equals(_partner, partnerWrapper._partner)) {
			return true;
		}

		return false;
	}

	@Override
	public Partner getWrappedModel() {
		return _partner;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _partner.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _partner.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_partner.resetOriginalValues();
	}

	private final Partner _partner;
}