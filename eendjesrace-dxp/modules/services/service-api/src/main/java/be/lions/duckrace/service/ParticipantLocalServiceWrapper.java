/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ParticipantLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ParticipantLocalService
 * @generated
 */
@ProviderType
public class ParticipantLocalServiceWrapper implements ParticipantLocalService,
	ServiceWrapper<ParticipantLocalService> {
	public ParticipantLocalServiceWrapper(
		ParticipantLocalService participantLocalService) {
		_participantLocalService = participantLocalService;
	}

	/**
	* Adds the participant to the database. Also notifies the appropriate model listeners.
	*
	* @param participant the participant
	* @return the participant that was added
	*/
	@Override
	public be.lions.duckrace.model.Participant addParticipant(
		be.lions.duckrace.model.Participant participant) {
		return _participantLocalService.addParticipant(participant);
	}

	/**
	* Creates a new participant with the primary key. Does not add the participant to the database.
	*
	* @param participantId the primary key for the new participant
	* @return the new participant
	*/
	@Override
	public be.lions.duckrace.model.Participant createParticipant(
		long participantId) {
		return _participantLocalService.createParticipant(participantId);
	}

	/**
	* Deletes the participant from the database. Also notifies the appropriate model listeners.
	*
	* @param participant the participant
	* @return the participant that was removed
	*/
	@Override
	public be.lions.duckrace.model.Participant deleteParticipant(
		be.lions.duckrace.model.Participant participant) {
		return _participantLocalService.deleteParticipant(participant);
	}

	/**
	* Deletes the participant with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param participantId the primary key of the participant
	* @return the participant that was removed
	* @throws PortalException if a participant with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.Participant deleteParticipant(
		long participantId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _participantLocalService.deleteParticipant(participantId);
	}

	@Override
	public be.lions.duckrace.model.Participant fetchParticipant(
		long participantId) {
		return _participantLocalService.fetchParticipant(participantId);
	}

	@Override
	public be.lions.duckrace.model.Participant getMatchingParticipant(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress)
		throws MobileNumberAlreadyUsedInvalidDataException,
			MobileNumberAlreadyUsedPartlyValidDataException,
			com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.getMatchingParticipant(mobileNumber,
			firstName, lastName, emailAddress);
	}

	@Override
	public be.lions.duckrace.model.Participant getOrCreateParticipant(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress,
		boolean contestParticipant, boolean mercedesDriver,
		java.lang.String carBrand, boolean mercedesOld,
		java.lang.String language)
		throws InvalidMobileNumberException,
			MobileNumberAlreadyUsedInvalidDataException,
			MobileNumberAlreadyUsedPartlyValidDataException,
			NoSuchParticipantException,
			com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.getOrCreateParticipant(mobileNumber,
			firstName, lastName, emailAddress, contestParticipant,
			mercedesDriver, carBrand, mercedesOld, language);
	}

	/**
	* Returns the participant with the primary key.
	*
	* @param participantId the primary key of the participant
	* @return the participant
	* @throws PortalException if a participant with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.Participant getParticipant(
		long participantId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _participantLocalService.getParticipant(participantId);
	}

	@Override
	public be.lions.duckrace.model.Participant getParticipantByLotNumber(
		int lotNumber)
		throws NoSuchLotException,
			com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.getParticipantByLotNumber(lotNumber);
	}

	@Override
	public be.lions.duckrace.model.Participant getParticipantByMobileNumber(
		java.lang.String mobileNumber)
		throws NoSuchParticipantException,
			com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.getParticipantByMobileNumber(mobileNumber);
	}

	/**
	* Updates the participant in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param participant the participant
	* @return the participant that was updated
	*/
	@Override
	public be.lions.duckrace.model.Participant updateParticipant(
		be.lions.duckrace.model.Participant participant) {
		return _participantLocalService.updateParticipant(participant);
	}

	@Override
	public be.lions.duckrace.model.Participant updateParticipant(long id,
		java.lang.String mobileNumber, java.lang.String emailAddress,
		java.lang.String firstName, java.lang.String lastName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.updateParticipant(id, mobileNumber,
			emailAddress, firstName, lastName);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _participantLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _participantLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _participantLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _participantLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _participantLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of participants.
	*
	* @return the number of participants
	*/
	@Override
	public int getParticipantsCount() {
		return _participantLocalService.getParticipantsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _participantLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _participantLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _participantLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _participantLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @return the range of participants
	*/
	@Override
	public java.util.List<be.lions.duckrace.model.Participant> getParticipants(
		int start, int end) {
		return _participantLocalService.getParticipants(start, end);
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Participant> searchParticipants(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _participantLocalService.searchParticipants(mobileNumber,
			firstName, lastName, emailAddress);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _participantLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _participantLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ParticipantLocalService getWrappedService() {
		return _participantLocalService;
	}

	@Override
	public void setWrappedService(
		ParticipantLocalService participantLocalService) {
		_participantLocalService = participantLocalService;
	}

	private ParticipantLocalService _participantLocalService;
}