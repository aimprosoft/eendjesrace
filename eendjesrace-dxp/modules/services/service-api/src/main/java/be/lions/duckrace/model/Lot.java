/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Lot service. Represents a row in the &quot;DuckRace_Lot&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see LotModel
 * @see be.lions.duckrace.model.impl.LotImpl
 * @see be.lions.duckrace.model.impl.LotModelImpl
 * @generated
 */
@ImplementationClassName("be.lions.duckrace.model.impl.LotImpl")
@ProviderType
public interface Lot extends LotModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link be.lions.duckrace.model.impl.LotImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Lot, String> CODE_ACCESSOR = new Accessor<Lot, String>() {
			@Override
			public String get(Lot lot) {
				return lot.getCode();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<Lot> getTypeClass() {
				return Lot.class;
			}
		};

	public boolean isLocked();

	public java.util.Date getUnlockDate();

	public boolean isLockOverdue();

	public be.lions.duckrace.model.Participant getParticipant()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	public be.lions.duckrace.model.Partner getPartner()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;
}