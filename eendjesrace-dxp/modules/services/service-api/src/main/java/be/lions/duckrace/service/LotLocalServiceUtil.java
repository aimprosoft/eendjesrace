/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Partner;
import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Lot. This utility wraps
 * {@link be.lions.duckrace.service.impl.LotLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LotLocalService
 * @see be.lions.duckrace.service.base.LotLocalServiceBaseImpl
 * @see be.lions.duckrace.service.impl.LotLocalServiceImpl
 * @generated
 */
@ProviderType
public class LotLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link be.lions.duckrace.service.impl.LotLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static be.lions.duckrace.model.Lot createLot(java.lang.String code,
		java.lang.String checksum)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().createLot(code, checksum);
	}

	public static be.lions.duckrace.model.Lot getAvailableLot(boolean pressed)
		throws NoSuchLotException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getAvailableLot(pressed);
	}

	public static be.lions.duckrace.model.Lot getLotByNumber(int number)
		throws NoSuchLotException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotByNumber(number);
	}

	public static be.lions.duckrace.model.Lot registerLot(
		java.lang.String mobileNumber)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().registerLot(mobileNumber);
	}

	public static be.lions.duckrace.model.Lot registerLot(
		java.lang.String mobileNumber, java.lang.String code,
		java.lang.String checksum)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().registerLot(mobileNumber, code, checksum);
	}

	public static be.lions.duckrace.model.Lot registerLot(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress,
		boolean contestParticipant, boolean mercedesDriver,
		java.lang.String carBrand, boolean mercedesOld, java.lang.String code,
		java.lang.String checksum, java.lang.String language)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .registerLot(mobileNumber, firstName, lastName,
			emailAddress, contestParticipant, mercedesDriver, carBrand,
			mercedesOld, code, checksum, language);
	}

	public static be.lions.duckrace.model.Lot registerLot(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress,
		java.lang.String orderNumber, java.lang.String language)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .registerLot(mobileNumber, firstName, lastName,
			emailAddress, orderNumber, language);
	}

	/**
	* Adds the lot to the database. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was added
	*/
	public static be.lions.duckrace.model.Lot addLot(
		be.lions.duckrace.model.Lot lot) {
		return getService().addLot(lot);
	}

	/**
	* Creates a new lot with the primary key. Does not add the lot to the database.
	*
	* @param code the primary key for the new lot
	* @return the new lot
	*/
	public static be.lions.duckrace.model.Lot createLot(java.lang.String code) {
		return getService().createLot(code);
	}

	/**
	* Deletes the lot from the database. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was removed
	*/
	public static be.lions.duckrace.model.Lot deleteLot(
		be.lions.duckrace.model.Lot lot) {
		return getService().deleteLot(lot);
	}

	/**
	* Deletes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param code the primary key of the lot
	* @return the lot that was removed
	* @throws PortalException if a lot with the primary key could not be found
	*/
	public static be.lions.duckrace.model.Lot deleteLot(java.lang.String code)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteLot(code);
	}

	public static be.lions.duckrace.model.Lot fetchLot(java.lang.String code) {
		return getService().fetchLot(code);
	}

	/**
	* Returns the lot with the primary key.
	*
	* @param code the primary key of the lot
	* @return the lot
	* @throws PortalException if a lot with the primary key could not be found
	*/
	public static be.lions.duckrace.model.Lot getLot(java.lang.String code)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getLot(code);
	}

	/**
	* Updates the lot in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was updated
	*/
	public static be.lions.duckrace.model.Lot updateLot(
		be.lions.duckrace.model.Lot lot) {
		return getService().updateLot(lot);
	}

	public static boolean codeExists(java.lang.String code)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().codeExists(code);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int getCurrentNumber()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCurrentNumber();
	}

	public static int getLongSmsLotCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLongSmsLotCount();
	}

	/**
	* Returns the number of lots.
	*
	* @return the number of lots
	*/
	public static int getLotsCount() {
		return getService().getLotsCount();
	}

	public static int getLotsForPartnerCount(long partnerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotsForPartnerCount(partnerId);
	}

	public static int getPressedLotsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPressedLotsCount();
	}

	public static int getRegisteredLotsForPartnerCount(long partnerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getRegisteredLotsForPartnerCount(partnerId);
	}

	public static int getShortSmsLotCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getShortSmsLotCount();
	}

	public static int getWebsiteRegistrationLotCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWebsiteRegistrationLotCount();
	}

	public static int getWebsiteReservationLotCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWebsiteReservationLotCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<java.lang.Integer> getLotNumbersForParticipant(
		long participantId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotNumbersForParticipant(participantId);
	}

	/**
	* Returns a range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of lots
	*/
	public static java.util.List<be.lions.duckrace.model.Lot> getLots(
		int start, int end) {
		return getService().getLots(start, end);
	}

	public static java.util.List<be.lions.duckrace.model.Lot> getLotsForParticipant(
		long participantId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotsForParticipant(participantId);
	}

	public static java.util.List<be.lions.duckrace.model.Lot> getLotsForPartner(
		long partnerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotsForPartner(partnerId);
	}

	public static java.util.List<be.lions.duckrace.model.Lot> getLotsForReservation(
		long reservationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLotsForReservation(reservationId);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static long getRegisteredLotCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getRegisteredLotCount();
	}

	public static void assignFreeLotToParticipant(LotReservation reservation)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().assignFreeLotToParticipant(reservation);
	}

	public static void assignLotsToPartner(Partner partner,
		java.util.List<LotRange> lotRanges)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().assignLotsToPartner(partner, lotRanges);
	}

	public static LotLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LotLocalService, LotLocalService> _serviceTracker =
		ServiceTrackerFactory.open(LotLocalService.class);
}