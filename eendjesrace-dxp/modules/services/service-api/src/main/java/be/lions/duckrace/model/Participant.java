/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Participant service. Represents a row in the &quot;DuckRace_Participant&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ParticipantModel
 * @see be.lions.duckrace.model.impl.ParticipantImpl
 * @see be.lions.duckrace.model.impl.ParticipantModelImpl
 * @generated
 */
@ImplementationClassName("be.lions.duckrace.model.impl.ParticipantImpl")
@ProviderType
public interface Participant extends ParticipantModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link be.lions.duckrace.model.impl.ParticipantImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Participant, Long> PARTICIPANT_ID_ACCESSOR = new Accessor<Participant, Long>() {
			@Override
			public Long get(Participant participant) {
				return participant.getParticipantId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Participant> getTypeClass() {
				return Participant.class;
			}
		};

	public java.lang.String getFullName();

	public java.util.List<be.lions.duckrace.model.Lot> getLots()
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.lang.String getFormattedMobileNumber();

	public java.lang.String getTransactionSafeEmailAddress();

	public boolean matchesOneOf(java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress);

	public boolean matchesAll(java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress);

	public void validate();
}