/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the LotReservation service. Represents a row in the &quot;DuckRace_LotReservation&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link be.lions.duckrace.model.impl.LotReservationModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link be.lions.duckrace.model.impl.LotReservationImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotReservation
 * @see be.lions.duckrace.model.impl.LotReservationImpl
 * @see be.lions.duckrace.model.impl.LotReservationModelImpl
 * @generated
 */
@ProviderType
public interface LotReservationModel extends BaseModel<LotReservation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a lot reservation model instance should use the {@link LotReservation} interface instead.
	 */

	/**
	 * Returns the primary key of this lot reservation.
	 *
	 * @return the primary key of this lot reservation
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this lot reservation.
	 *
	 * @param primaryKey the primary key of this lot reservation
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the reservation ID of this lot reservation.
	 *
	 * @return the reservation ID of this lot reservation
	 */
	public long getReservationId();

	/**
	 * Sets the reservation ID of this lot reservation.
	 *
	 * @param reservationId the reservation ID of this lot reservation
	 */
	public void setReservationId(long reservationId);

	/**
	 * Returns the participant ID of this lot reservation.
	 *
	 * @return the participant ID of this lot reservation
	 */
	public long getParticipantId();

	/**
	 * Sets the participant ID of this lot reservation.
	 *
	 * @param participantId the participant ID of this lot reservation
	 */
	public void setParticipantId(long participantId);

	/**
	 * Returns the number lots of this lot reservation.
	 *
	 * @return the number lots of this lot reservation
	 */
	public int getNumberLots();

	/**
	 * Sets the number lots of this lot reservation.
	 *
	 * @param numberLots the number lots of this lot reservation
	 */
	public void setNumberLots(int numberLots);

	/**
	 * Returns the approved of this lot reservation.
	 *
	 * @return the approved of this lot reservation
	 */
	public boolean getApproved();

	/**
	 * Returns <code>true</code> if this lot reservation is approved.
	 *
	 * @return <code>true</code> if this lot reservation is approved; <code>false</code> otherwise
	 */
	public boolean isApproved();

	/**
	 * Sets whether this lot reservation is approved.
	 *
	 * @param approved the approved of this lot reservation
	 */
	public void setApproved(boolean approved);

	/**
	 * Returns the reservation date of this lot reservation.
	 *
	 * @return the reservation date of this lot reservation
	 */
	public Date getReservationDate();

	/**
	 * Sets the reservation date of this lot reservation.
	 *
	 * @param reservationDate the reservation date of this lot reservation
	 */
	public void setReservationDate(Date reservationDate);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(be.lions.duckrace.model.LotReservation lotReservation);

	@Override
	public int hashCode();

	@Override
	public CacheModel<be.lions.duckrace.model.LotReservation> toCacheModel();

	@Override
	public be.lions.duckrace.model.LotReservation toEscapedModel();

	@Override
	public be.lions.duckrace.model.LotReservation toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}