/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.LotReservation;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the lot reservation service. This utility wraps {@link be.lions.duckrace.service.persistence.impl.LotReservationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationPersistence
 * @see be.lions.duckrace.service.persistence.impl.LotReservationPersistenceImpl
 * @generated
 */
@ProviderType
public class LotReservationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(LotReservation lotReservation) {
		getPersistence().clearCache(lotReservation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LotReservation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LotReservation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LotReservation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<LotReservation> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static LotReservation update(LotReservation lotReservation) {
		return getPersistence().update(lotReservation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static LotReservation update(LotReservation lotReservation,
		ServiceContext serviceContext) {
		return getPersistence().update(lotReservation, serviceContext);
	}

	/**
	* Returns all the lot reservations where approved = &#63;.
	*
	* @param approved the approved
	* @return the matching lot reservations
	*/
	public static List<LotReservation> findByApproved(boolean approved) {
		return getPersistence().findByApproved(approved);
	}

	/**
	* Returns a range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of matching lot reservations
	*/
	public static List<LotReservation> findByApproved(boolean approved,
		int start, int end) {
		return getPersistence().findByApproved(approved, start, end);
	}

	/**
	* Returns an ordered range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lot reservations
	*/
	public static List<LotReservation> findByApproved(boolean approved,
		int start, int end, OrderByComparator<LotReservation> orderByComparator) {
		return getPersistence()
				   .findByApproved(approved, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lot reservations
	*/
	public static List<LotReservation> findByApproved(boolean approved,
		int start, int end,
		OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByApproved(approved, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot reservation
	* @throws NoSuchLotReservationException if a matching lot reservation could not be found
	*/
	public static LotReservation findByApproved_First(boolean approved,
		OrderByComparator<LotReservation> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotReservationException {
		return getPersistence().findByApproved_First(approved, orderByComparator);
	}

	/**
	* Returns the first lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	*/
	public static LotReservation fetchByApproved_First(boolean approved,
		OrderByComparator<LotReservation> orderByComparator) {
		return getPersistence()
				   .fetchByApproved_First(approved, orderByComparator);
	}

	/**
	* Returns the last lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot reservation
	* @throws NoSuchLotReservationException if a matching lot reservation could not be found
	*/
	public static LotReservation findByApproved_Last(boolean approved,
		OrderByComparator<LotReservation> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotReservationException {
		return getPersistence().findByApproved_Last(approved, orderByComparator);
	}

	/**
	* Returns the last lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	*/
	public static LotReservation fetchByApproved_Last(boolean approved,
		OrderByComparator<LotReservation> orderByComparator) {
		return getPersistence().fetchByApproved_Last(approved, orderByComparator);
	}

	/**
	* Returns the lot reservations before and after the current lot reservation in the ordered set where approved = &#63;.
	*
	* @param reservationId the primary key of the current lot reservation
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot reservation
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public static LotReservation[] findByApproved_PrevAndNext(
		long reservationId, boolean approved,
		OrderByComparator<LotReservation> orderByComparator)
		throws be.lions.duckrace.exception.NoSuchLotReservationException {
		return getPersistence()
				   .findByApproved_PrevAndNext(reservationId, approved,
			orderByComparator);
	}

	/**
	* Removes all the lot reservations where approved = &#63; from the database.
	*
	* @param approved the approved
	*/
	public static void removeByApproved(boolean approved) {
		getPersistence().removeByApproved(approved);
	}

	/**
	* Returns the number of lot reservations where approved = &#63;.
	*
	* @param approved the approved
	* @return the number of matching lot reservations
	*/
	public static int countByApproved(boolean approved) {
		return getPersistence().countByApproved(approved);
	}

	/**
	* Caches the lot reservation in the entity cache if it is enabled.
	*
	* @param lotReservation the lot reservation
	*/
	public static void cacheResult(LotReservation lotReservation) {
		getPersistence().cacheResult(lotReservation);
	}

	/**
	* Caches the lot reservations in the entity cache if it is enabled.
	*
	* @param lotReservations the lot reservations
	*/
	public static void cacheResult(List<LotReservation> lotReservations) {
		getPersistence().cacheResult(lotReservations);
	}

	/**
	* Creates a new lot reservation with the primary key. Does not add the lot reservation to the database.
	*
	* @param reservationId the primary key for the new lot reservation
	* @return the new lot reservation
	*/
	public static LotReservation create(long reservationId) {
		return getPersistence().create(reservationId);
	}

	/**
	* Removes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation that was removed
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public static LotReservation remove(long reservationId)
		throws be.lions.duckrace.exception.NoSuchLotReservationException {
		return getPersistence().remove(reservationId);
	}

	public static LotReservation updateImpl(LotReservation lotReservation) {
		return getPersistence().updateImpl(lotReservation);
	}

	/**
	* Returns the lot reservation with the primary key or throws a {@link NoSuchLotReservationException} if it could not be found.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public static LotReservation findByPrimaryKey(long reservationId)
		throws be.lions.duckrace.exception.NoSuchLotReservationException {
		return getPersistence().findByPrimaryKey(reservationId);
	}

	/**
	* Returns the lot reservation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation, or <code>null</code> if a lot reservation with the primary key could not be found
	*/
	public static LotReservation fetchByPrimaryKey(long reservationId) {
		return getPersistence().fetchByPrimaryKey(reservationId);
	}

	public static java.util.Map<java.io.Serializable, LotReservation> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the lot reservations.
	*
	* @return the lot reservations
	*/
	public static List<LotReservation> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of lot reservations
	*/
	public static List<LotReservation> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lot reservations
	*/
	public static List<LotReservation> findAll(int start, int end,
		OrderByComparator<LotReservation> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of lot reservations
	*/
	public static List<LotReservation> findAll(int start, int end,
		OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the lot reservations from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of lot reservations.
	*
	* @return the number of lot reservations
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static LotReservationPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LotReservationPersistence, LotReservationPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LotReservationPersistence.class);
}