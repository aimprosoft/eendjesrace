/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import be.lions.duckrace.model.*;
import be.lions.duckrace.model.Lot;

import com.liferay.portal.kernel.dao.orm.*;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Lot. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LotLocalServiceUtil
 * @see be.lions.duckrace.service.base.LotLocalServiceBaseImpl
 * @see be.lions.duckrace.service.impl.LotLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface LotLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LotLocalServiceUtil} to access the lot local service. Add custom service methods to {@link be.lions.duckrace.service.impl.LotLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public Lot createLot(java.lang.String code, java.lang.String checksum)
		throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Lot getAvailableLot(boolean pressed)
		throws NoSuchLotException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Lot getLotByNumber(int number)
		throws NoSuchLotException, SystemException;

	public Lot registerLot(java.lang.String mobileNumber)
		throws PortalException, SystemException;

	public Lot registerLot(java.lang.String mobileNumber,
		java.lang.String code, java.lang.String checksum)
		throws PortalException, SystemException;

	public Lot registerLot(java.lang.String mobileNumber,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, boolean contestParticipant,
		boolean mercedesDriver, java.lang.String carBrand, boolean mercedesOld,
		java.lang.String code, java.lang.String checksum,
		java.lang.String language) throws PortalException, SystemException;

	public Lot registerLot(java.lang.String mobileNumber,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, java.lang.String orderNumber,
		java.lang.String language) throws PortalException, SystemException;

	/**
	* Adds the lot to the database. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Lot addLot(Lot lot);

	/**
	* Creates a new lot with the primary key. Does not add the lot to the database.
	*
	* @param code the primary key for the new lot
	* @return the new lot
	*/
	public Lot createLot(java.lang.String code);

	/**
	* Deletes the lot from the database. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Lot deleteLot(Lot lot);

	/**
	* Deletes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param code the primary key of the lot
	* @return the lot that was removed
	* @throws PortalException if a lot with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Lot deleteLot(java.lang.String code) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Lot fetchLot(java.lang.String code);

	/**
	* Returns the lot with the primary key.
	*
	* @param code the primary key of the lot
	* @return the lot
	* @throws PortalException if a lot with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Lot getLot(java.lang.String code) throws PortalException;

	/**
	* Updates the lot in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lot the lot
	* @return the lot that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Lot updateLot(Lot lot);

	public boolean codeExists(java.lang.String code) throws SystemException;

	public DynamicQuery dynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getCurrentNumber() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLongSmsLotCount() throws SystemException;

	/**
	* Returns the number of lots.
	*
	* @return the number of lots
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLotsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLotsForPartnerCount(long partnerId) throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getPressedLotsCount() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getRegisteredLotsForPartnerCount(long partnerId)
		throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getShortSmsLotCount() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getWebsiteRegistrationLotCount() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getWebsiteReservationLotCount() throws SystemException;

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Integer> getLotNumbersForParticipant(
		long participantId) throws SystemException;

	/**
	* Returns a range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of lots
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Lot> getLots(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Lot> getLotsForParticipant(long participantId)
		throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Lot> getLotsForPartner(long partnerId)
		throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Lot> getLotsForReservation(long reservationId)
		throws SystemException;

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getRegisteredLotCount() throws SystemException;

	public void assignFreeLotToParticipant(LotReservation reservation)
		throws PortalException, SystemException;

	public void assignLotsToPartner(Partner partner, List<LotRange> lotRanges)
		throws PortalException, SystemException;
}