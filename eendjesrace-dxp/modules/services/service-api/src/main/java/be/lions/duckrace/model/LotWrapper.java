/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Lot}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Lot
 * @generated
 */
@ProviderType
public class LotWrapper implements Lot, ModelWrapper<Lot> {
	public LotWrapper(Lot lot) {
		_lot = lot;
	}

	@Override
	public Class<?> getModelClass() {
		return Lot.class;
	}

	@Override
	public String getModelClassName() {
		return Lot.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("code", getCode());
		attributes.put("checksum", getChecksum());
		attributes.put("lotNumber", getLotNumber());
		attributes.put("number", getNumber());
		attributes.put("pressed", getPressed());
		attributes.put("nbTries", getNbTries());
		attributes.put("lockoutDate", getLockoutDate());
		attributes.put("participantId", getParticipantId());
		attributes.put("reservationId", getReservationId());
		attributes.put("partnerId", getPartnerId());
		attributes.put("orderNumber", getOrderNumber());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String code = (String)attributes.get("code");

		if (code != null) {
			setCode(code);
		}

		String checksum = (String)attributes.get("checksum");

		if (checksum != null) {
			setChecksum(checksum);
		}

		Integer lotNumber = (Integer)attributes.get("lotNumber");

		if (lotNumber != null) {
			setLotNumber(lotNumber);
		}

		Integer number = (Integer)attributes.get("number");

		if (number != null) {
			setNumber(number);
		}

		Boolean pressed = (Boolean)attributes.get("pressed");

		if (pressed != null) {
			setPressed(pressed);
		}

		Integer nbTries = (Integer)attributes.get("nbTries");

		if (nbTries != null) {
			setNbTries(nbTries);
		}

		Date lockoutDate = (Date)attributes.get("lockoutDate");

		if (lockoutDate != null) {
			setLockoutDate(lockoutDate);
		}

		Long participantId = (Long)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}

		Long reservationId = (Long)attributes.get("reservationId");

		if (reservationId != null) {
			setReservationId(reservationId);
		}

		Long partnerId = (Long)attributes.get("partnerId");

		if (partnerId != null) {
			setPartnerId(partnerId);
		}

		String orderNumber = (String)attributes.get("orderNumber");

		if (orderNumber != null) {
			setOrderNumber(orderNumber);
		}
	}

	@Override
	public be.lions.duckrace.model.Lot toEscapedModel() {
		return new LotWrapper(_lot.toEscapedModel());
	}

	@Override
	public be.lions.duckrace.model.Lot toUnescapedModel() {
		return new LotWrapper(_lot.toUnescapedModel());
	}

	@Override
	public be.lions.duckrace.model.Participant getParticipant()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lot.getParticipant();
	}

	@Override
	public be.lions.duckrace.model.Partner getPartner()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lot.getPartner();
	}

	/**
	* Returns the pressed of this lot.
	*
	* @return the pressed of this lot
	*/
	@Override
	public boolean getPressed() {
		return _lot.getPressed();
	}

	@Override
	public boolean isCachedModel() {
		return _lot.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _lot.isEscapedModel();
	}

	@Override
	public boolean isLockOverdue() {
		return _lot.isLockOverdue();
	}

	@Override
	public boolean isLocked() {
		return _lot.isLocked();
	}

	@Override
	public boolean isNew() {
		return _lot.isNew();
	}

	/**
	* Returns <code>true</code> if this lot is pressed.
	*
	* @return <code>true</code> if this lot is pressed; <code>false</code> otherwise
	*/
	@Override
	public boolean isPressed() {
		return _lot.isPressed();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _lot.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<be.lions.duckrace.model.Lot> toCacheModel() {
		return _lot.toCacheModel();
	}

	@Override
	public int compareTo(be.lions.duckrace.model.Lot lot) {
		return _lot.compareTo(lot);
	}

	/**
	* Returns the lot number of this lot.
	*
	* @return the lot number of this lot
	*/
	@Override
	public int getLotNumber() {
		return _lot.getLotNumber();
	}

	/**
	* Returns the nb tries of this lot.
	*
	* @return the nb tries of this lot
	*/
	@Override
	public int getNbTries() {
		return _lot.getNbTries();
	}

	/**
	* Returns the number of this lot.
	*
	* @return the number of this lot
	*/
	@Override
	public int getNumber() {
		return _lot.getNumber();
	}

	@Override
	public int hashCode() {
		return _lot.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _lot.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LotWrapper((Lot)_lot.clone());
	}

	/**
	* Returns the checksum of this lot.
	*
	* @return the checksum of this lot
	*/
	@Override
	public java.lang.String getChecksum() {
		return _lot.getChecksum();
	}

	/**
	* Returns the code of this lot.
	*
	* @return the code of this lot
	*/
	@Override
	public java.lang.String getCode() {
		return _lot.getCode();
	}

	/**
	* Returns the order number of this lot.
	*
	* @return the order number of this lot
	*/
	@Override
	public java.lang.String getOrderNumber() {
		return _lot.getOrderNumber();
	}

	/**
	* Returns the primary key of this lot.
	*
	* @return the primary key of this lot
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _lot.getPrimaryKey();
	}

	@Override
	public java.lang.String toString() {
		return _lot.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _lot.toXmlString();
	}

	/**
	* Returns the lockout date of this lot.
	*
	* @return the lockout date of this lot
	*/
	@Override
	public Date getLockoutDate() {
		return _lot.getLockoutDate();
	}

	@Override
	public Date getUnlockDate() {
		return _lot.getUnlockDate();
	}

	/**
	* Returns the participant ID of this lot.
	*
	* @return the participant ID of this lot
	*/
	@Override
	public long getParticipantId() {
		return _lot.getParticipantId();
	}

	/**
	* Returns the partner ID of this lot.
	*
	* @return the partner ID of this lot
	*/
	@Override
	public long getPartnerId() {
		return _lot.getPartnerId();
	}

	/**
	* Returns the reservation ID of this lot.
	*
	* @return the reservation ID of this lot
	*/
	@Override
	public long getReservationId() {
		return _lot.getReservationId();
	}

	@Override
	public void persist() {
		_lot.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_lot.setCachedModel(cachedModel);
	}

	/**
	* Sets the checksum of this lot.
	*
	* @param checksum the checksum of this lot
	*/
	@Override
	public void setChecksum(java.lang.String checksum) {
		_lot.setChecksum(checksum);
	}

	/**
	* Sets the code of this lot.
	*
	* @param code the code of this lot
	*/
	@Override
	public void setCode(java.lang.String code) {
		_lot.setCode(code);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_lot.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_lot.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_lot.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the lockout date of this lot.
	*
	* @param lockoutDate the lockout date of this lot
	*/
	@Override
	public void setLockoutDate(Date lockoutDate) {
		_lot.setLockoutDate(lockoutDate);
	}

	/**
	* Sets the lot number of this lot.
	*
	* @param lotNumber the lot number of this lot
	*/
	@Override
	public void setLotNumber(int lotNumber) {
		_lot.setLotNumber(lotNumber);
	}

	/**
	* Sets the nb tries of this lot.
	*
	* @param nbTries the nb tries of this lot
	*/
	@Override
	public void setNbTries(int nbTries) {
		_lot.setNbTries(nbTries);
	}

	@Override
	public void setNew(boolean n) {
		_lot.setNew(n);
	}

	/**
	* Sets the number of this lot.
	*
	* @param number the number of this lot
	*/
	@Override
	public void setNumber(int number) {
		_lot.setNumber(number);
	}

	/**
	* Sets the order number of this lot.
	*
	* @param orderNumber the order number of this lot
	*/
	@Override
	public void setOrderNumber(java.lang.String orderNumber) {
		_lot.setOrderNumber(orderNumber);
	}

	/**
	* Sets the participant ID of this lot.
	*
	* @param participantId the participant ID of this lot
	*/
	@Override
	public void setParticipantId(long participantId) {
		_lot.setParticipantId(participantId);
	}

	/**
	* Sets the partner ID of this lot.
	*
	* @param partnerId the partner ID of this lot
	*/
	@Override
	public void setPartnerId(long partnerId) {
		_lot.setPartnerId(partnerId);
	}

	/**
	* Sets whether this lot is pressed.
	*
	* @param pressed the pressed of this lot
	*/
	@Override
	public void setPressed(boolean pressed) {
		_lot.setPressed(pressed);
	}

	/**
	* Sets the primary key of this lot.
	*
	* @param primaryKey the primary key of this lot
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_lot.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_lot.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the reservation ID of this lot.
	*
	* @param reservationId the reservation ID of this lot
	*/
	@Override
	public void setReservationId(long reservationId) {
		_lot.setReservationId(reservationId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LotWrapper)) {
			return false;
		}

		LotWrapper lotWrapper = (LotWrapper)obj;

		if (Objects.equals(_lot, lotWrapper._lot)) {
			return true;
		}

		return false;
	}

	@Override
	public Lot getWrappedModel() {
		return _lot;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _lot.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _lot.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_lot.resetOriginalValues();
	}

	private final Lot _lot;
}