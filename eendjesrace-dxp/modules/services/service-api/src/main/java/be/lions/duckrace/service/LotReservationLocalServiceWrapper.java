/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LotReservationLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationLocalService
 * @generated
 */
@ProviderType
public class LotReservationLocalServiceWrapper
	implements LotReservationLocalService,
		ServiceWrapper<LotReservationLocalService> {
	public LotReservationLocalServiceWrapper(
		LotReservationLocalService lotReservationLocalService) {
		_lotReservationLocalService = lotReservationLocalService;
	}

	/**
	* Adds the lot reservation to the database. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was added
	*/
	@Override
	public be.lions.duckrace.model.LotReservation addLotReservation(
		be.lions.duckrace.model.LotReservation lotReservation) {
		return _lotReservationLocalService.addLotReservation(lotReservation);
	}

	@Override
	public be.lions.duckrace.model.LotReservation approveReservation(
		long reservationId)
		throws InvalidMobileNumberException, NoSuchLotException,
			NoSuchParticipantException,
			com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lotReservationLocalService.approveReservation(reservationId);
	}

	/**
	* Creates a new lot reservation with the primary key. Does not add the lot reservation to the database.
	*
	* @param reservationId the primary key for the new lot reservation
	* @return the new lot reservation
	*/
	@Override
	public be.lions.duckrace.model.LotReservation createLotReservation(
		long reservationId) {
		return _lotReservationLocalService.createLotReservation(reservationId);
	}

	@Override
	public be.lions.duckrace.model.LotReservation createReservation(
		java.lang.String mobileNumber, java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress,
		boolean contestParticipant, boolean mercedesDriver,
		java.lang.String carBrand, boolean mercedesOld, int numberLots,
		java.lang.String language)
		throws InvalidMobileNumberException,
			MobileNumberAlreadyUsedInvalidDataException,
			MobileNumberAlreadyUsedPartlyValidDataException,
			NoSuchParticipantException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lotReservationLocalService.createReservation(mobileNumber,
			firstName, lastName, emailAddress, contestParticipant,
			mercedesDriver, carBrand, mercedesOld, numberLots, language);
	}

	/**
	* Deletes the lot reservation from the database. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was removed
	*/
	@Override
	public be.lions.duckrace.model.LotReservation deleteLotReservation(
		be.lions.duckrace.model.LotReservation lotReservation) {
		return _lotReservationLocalService.deleteLotReservation(lotReservation);
	}

	/**
	* Deletes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation that was removed
	* @throws PortalException if a lot reservation with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.LotReservation deleteLotReservation(
		long reservationId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _lotReservationLocalService.deleteLotReservation(reservationId);
	}

	@Override
	public be.lions.duckrace.model.LotReservation fetchLotReservation(
		long reservationId) {
		return _lotReservationLocalService.fetchLotReservation(reservationId);
	}

	/**
	* Returns the lot reservation with the primary key.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation
	* @throws PortalException if a lot reservation with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.LotReservation getLotReservation(
		long reservationId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _lotReservationLocalService.getLotReservation(reservationId);
	}

	/**
	* Updates the lot reservation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was updated
	*/
	@Override
	public be.lions.duckrace.model.LotReservation updateLotReservation(
		be.lions.duckrace.model.LotReservation lotReservation) {
		return _lotReservationLocalService.updateLotReservation(lotReservation);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _lotReservationLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _lotReservationLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _lotReservationLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _lotReservationLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _lotReservationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of lot reservations.
	*
	* @return the number of lot reservations
	*/
	@Override
	public int getLotReservationsCount() {
		return _lotReservationLocalService.getLotReservationsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _lotReservationLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _lotReservationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _lotReservationLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _lotReservationLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<be.lions.duckrace.model.LotReservation> getApprovedReservations()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lotReservationLocalService.getApprovedReservations();
	}

	/**
	* Returns a range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of lot reservations
	*/
	@Override
	public java.util.List<be.lions.duckrace.model.LotReservation> getLotReservations(
		int start, int end) {
		return _lotReservationLocalService.getLotReservations(start, end);
	}

	@Override
	public java.util.List<be.lions.duckrace.model.LotReservation> getUnapprovedReservations()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lotReservationLocalService.getUnapprovedReservations();
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _lotReservationLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _lotReservationLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public LotReservationLocalService getWrappedService() {
		return _lotReservationLocalService;
	}

	@Override
	public void setWrappedService(
		LotReservationLocalService lotReservationLocalService) {
		_lotReservationLocalService = lotReservationLocalService;
	}

	private LotReservationLocalService _lotReservationLocalService;
}