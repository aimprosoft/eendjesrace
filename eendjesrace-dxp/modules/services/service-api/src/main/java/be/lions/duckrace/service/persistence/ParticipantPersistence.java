/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the participant service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see be.lions.duckrace.service.persistence.impl.ParticipantPersistenceImpl
 * @see ParticipantUtil
 * @generated
 */
@ProviderType
public interface ParticipantPersistence extends BasePersistence<Participant> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ParticipantUtil} to access the participant persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the participant where mobileNumber = &#63; or throws a {@link NoSuchParticipantException} if it could not be found.
	*
	* @param mobileNumber the mobile number
	* @return the matching participant
	* @throws NoSuchParticipantException if a matching participant could not be found
	*/
	public Participant findByMobileNumber(java.lang.String mobileNumber)
		throws NoSuchParticipantException;

	/**
	* Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param mobileNumber the mobile number
	* @return the matching participant, or <code>null</code> if a matching participant could not be found
	*/
	public Participant fetchByMobileNumber(java.lang.String mobileNumber);

	/**
	* Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param mobileNumber the mobile number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching participant, or <code>null</code> if a matching participant could not be found
	*/
	public Participant fetchByMobileNumber(java.lang.String mobileNumber,
		boolean retrieveFromCache);

	/**
	* Removes the participant where mobileNumber = &#63; from the database.
	*
	* @param mobileNumber the mobile number
	* @return the participant that was removed
	*/
	public Participant removeByMobileNumber(java.lang.String mobileNumber)
		throws NoSuchParticipantException;

	/**
	* Returns the number of participants where mobileNumber = &#63;.
	*
	* @param mobileNumber the mobile number
	* @return the number of matching participants
	*/
	public int countByMobileNumber(java.lang.String mobileNumber);

	/**
	* Caches the participant in the entity cache if it is enabled.
	*
	* @param participant the participant
	*/
	public void cacheResult(Participant participant);

	/**
	* Caches the participants in the entity cache if it is enabled.
	*
	* @param participants the participants
	*/
	public void cacheResult(java.util.List<Participant> participants);

	/**
	* Creates a new participant with the primary key. Does not add the participant to the database.
	*
	* @param participantId the primary key for the new participant
	* @return the new participant
	*/
	public Participant create(long participantId);

	/**
	* Removes the participant with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param participantId the primary key of the participant
	* @return the participant that was removed
	* @throws NoSuchParticipantException if a participant with the primary key could not be found
	*/
	public Participant remove(long participantId)
		throws NoSuchParticipantException;

	public Participant updateImpl(Participant participant);

	/**
	* Returns the participant with the primary key or throws a {@link NoSuchParticipantException} if it could not be found.
	*
	* @param participantId the primary key of the participant
	* @return the participant
	* @throws NoSuchParticipantException if a participant with the primary key could not be found
	*/
	public Participant findByPrimaryKey(long participantId)
		throws NoSuchParticipantException;

	/**
	* Returns the participant with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param participantId the primary key of the participant
	* @return the participant, or <code>null</code> if a participant with the primary key could not be found
	*/
	public Participant fetchByPrimaryKey(long participantId);

	@Override
	public java.util.Map<java.io.Serializable, Participant> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the participants.
	*
	* @return the participants
	*/
	public java.util.List<Participant> findAll();

	/**
	* Returns a range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @return the range of participants
	*/
	public java.util.List<Participant> findAll(int start, int end);

	/**
	* Returns an ordered range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of participants
	*/
	public java.util.List<Participant> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Participant> orderByComparator);

	/**
	* Returns an ordered range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of participants
	*/
	public java.util.List<Participant> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Participant> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the participants from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of participants.
	*
	* @return the number of participants
	*/
	public int countAll();
}