/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Lot service. Represents a row in the &quot;DuckRace_Lot&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link be.lions.duckrace.model.impl.LotModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link be.lions.duckrace.model.impl.LotImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Lot
 * @see be.lions.duckrace.model.impl.LotImpl
 * @see be.lions.duckrace.model.impl.LotModelImpl
 * @generated
 */
@ProviderType
public interface LotModel extends BaseModel<Lot> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a lot model instance should use the {@link Lot} interface instead.
	 */

	/**
	 * Returns the primary key of this lot.
	 *
	 * @return the primary key of this lot
	 */
	public String getPrimaryKey();

	/**
	 * Sets the primary key of this lot.
	 *
	 * @param primaryKey the primary key of this lot
	 */
	public void setPrimaryKey(String primaryKey);

	/**
	 * Returns the code of this lot.
	 *
	 * @return the code of this lot
	 */
	@AutoEscape
	public String getCode();

	/**
	 * Sets the code of this lot.
	 *
	 * @param code the code of this lot
	 */
	public void setCode(String code);

	/**
	 * Returns the checksum of this lot.
	 *
	 * @return the checksum of this lot
	 */
	@AutoEscape
	public String getChecksum();

	/**
	 * Sets the checksum of this lot.
	 *
	 * @param checksum the checksum of this lot
	 */
	public void setChecksum(String checksum);

	/**
	 * Returns the lot number of this lot.
	 *
	 * @return the lot number of this lot
	 */
	public int getLotNumber();

	/**
	 * Sets the lot number of this lot.
	 *
	 * @param lotNumber the lot number of this lot
	 */
	public void setLotNumber(int lotNumber);

	/**
	 * Returns the number of this lot.
	 *
	 * @return the number of this lot
	 */
	public int getNumber();

	/**
	 * Sets the number of this lot.
	 *
	 * @param number the number of this lot
	 */
	public void setNumber(int number);

	/**
	 * Returns the pressed of this lot.
	 *
	 * @return the pressed of this lot
	 */
	public boolean getPressed();

	/**
	 * Returns <code>true</code> if this lot is pressed.
	 *
	 * @return <code>true</code> if this lot is pressed; <code>false</code> otherwise
	 */
	public boolean isPressed();

	/**
	 * Sets whether this lot is pressed.
	 *
	 * @param pressed the pressed of this lot
	 */
	public void setPressed(boolean pressed);

	/**
	 * Returns the nb tries of this lot.
	 *
	 * @return the nb tries of this lot
	 */
	public int getNbTries();

	/**
	 * Sets the nb tries of this lot.
	 *
	 * @param nbTries the nb tries of this lot
	 */
	public void setNbTries(int nbTries);

	/**
	 * Returns the lockout date of this lot.
	 *
	 * @return the lockout date of this lot
	 */
	public Date getLockoutDate();

	/**
	 * Sets the lockout date of this lot.
	 *
	 * @param lockoutDate the lockout date of this lot
	 */
	public void setLockoutDate(Date lockoutDate);

	/**
	 * Returns the participant ID of this lot.
	 *
	 * @return the participant ID of this lot
	 */
	public long getParticipantId();

	/**
	 * Sets the participant ID of this lot.
	 *
	 * @param participantId the participant ID of this lot
	 */
	public void setParticipantId(long participantId);

	/**
	 * Returns the reservation ID of this lot.
	 *
	 * @return the reservation ID of this lot
	 */
	public long getReservationId();

	/**
	 * Sets the reservation ID of this lot.
	 *
	 * @param reservationId the reservation ID of this lot
	 */
	public void setReservationId(long reservationId);

	/**
	 * Returns the partner ID of this lot.
	 *
	 * @return the partner ID of this lot
	 */
	public long getPartnerId();

	/**
	 * Sets the partner ID of this lot.
	 *
	 * @param partnerId the partner ID of this lot
	 */
	public void setPartnerId(long partnerId);

	/**
	 * Returns the order number of this lot.
	 *
	 * @return the order number of this lot
	 */
	@AutoEscape
	public String getOrderNumber();

	/**
	 * Sets the order number of this lot.
	 *
	 * @param orderNumber the order number of this lot
	 */
	public void setOrderNumber(String orderNumber);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(be.lions.duckrace.model.Lot lot);

	@Override
	public int hashCode();

	@Override
	public CacheModel<be.lions.duckrace.model.Lot> toCacheModel();

	@Override
	public be.lions.duckrace.model.Lot toEscapedModel();

	@Override
	public be.lions.duckrace.model.Lot toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}