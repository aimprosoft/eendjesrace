/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import be.lions.duckrace.model.LotReservation;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for LotReservation. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationLocalServiceUtil
 * @see be.lions.duckrace.service.base.LotReservationLocalServiceBaseImpl
 * @see be.lions.duckrace.service.impl.LotReservationLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface LotReservationLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LotReservationLocalServiceUtil} to access the lot reservation local service. Add custom service methods to {@link be.lions.duckrace.service.impl.LotReservationLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the lot reservation to the database. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LotReservation addLotReservation(LotReservation lotReservation);

	public LotReservation approveReservation(long reservationId)
		throws InvalidMobileNumberException, NoSuchLotException,
			NoSuchParticipantException, PortalException, SystemException;

	/**
	* Creates a new lot reservation with the primary key. Does not add the lot reservation to the database.
	*
	* @param reservationId the primary key for the new lot reservation
	* @return the new lot reservation
	*/
	public LotReservation createLotReservation(long reservationId);

	public LotReservation createReservation(java.lang.String mobileNumber,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, boolean contestParticipant,
		boolean mercedesDriver, java.lang.String carBrand, boolean mercedesOld,
		int numberLots, java.lang.String language)
		throws InvalidMobileNumberException,
			MobileNumberAlreadyUsedInvalidDataException,
			MobileNumberAlreadyUsedPartlyValidDataException,
			NoSuchParticipantException, SystemException;

	/**
	* Deletes the lot reservation from the database. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public LotReservation deleteLotReservation(LotReservation lotReservation);

	/**
	* Deletes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation that was removed
	* @throws PortalException if a lot reservation with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public LotReservation deleteLotReservation(long reservationId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LotReservation fetchLotReservation(long reservationId);

	/**
	* Returns the lot reservation with the primary key.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation
	* @throws PortalException if a lot reservation with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LotReservation getLotReservation(long reservationId)
		throws PortalException;

	/**
	* Updates the lot reservation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lotReservation the lot reservation
	* @return the lot reservation that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LotReservation updateLotReservation(LotReservation lotReservation);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Returns the number of lot reservations.
	*
	* @return the number of lot reservations
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLotReservationsCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LotReservation> getApprovedReservations()
		throws SystemException;

	/**
	* Returns a range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of lot reservations
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LotReservation> getLotReservations(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LotReservation> getUnapprovedReservations()
		throws SystemException;

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}