/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class PartnerSoap implements Serializable {
	public static PartnerSoap toSoapModel(Partner model) {
		PartnerSoap soapModel = new PartnerSoap();

		soapModel.setPartnerId(model.getPartnerId());
		soapModel.setCategory(model.getCategory());
		soapModel.setName(model.getName());
		soapModel.setReturnedLotsCount(model.getReturnedLotsCount());

		return soapModel;
	}

	public static PartnerSoap[] toSoapModels(Partner[] models) {
		PartnerSoap[] soapModels = new PartnerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PartnerSoap[][] toSoapModels(Partner[][] models) {
		PartnerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PartnerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PartnerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PartnerSoap[] toSoapModels(List<Partner> models) {
		List<PartnerSoap> soapModels = new ArrayList<PartnerSoap>(models.size());

		for (Partner model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PartnerSoap[soapModels.size()]);
	}

	public PartnerSoap() {
	}

	public long getPrimaryKey() {
		return _partnerId;
	}

	public void setPrimaryKey(long pk) {
		setPartnerId(pk);
	}

	public long getPartnerId() {
		return _partnerId;
	}

	public void setPartnerId(long partnerId) {
		_partnerId = partnerId;
	}

	public String getCategory() {
		return _category;
	}

	public void setCategory(String category) {
		_category = category;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getReturnedLotsCount() {
		return _returnedLotsCount;
	}

	public void setReturnedLotsCount(int returnedLotsCount) {
		_returnedLotsCount = returnedLotsCount;
	}

	private long _partnerId;
	private String _category;
	private String _name;
	private int _returnedLotsCount;
}