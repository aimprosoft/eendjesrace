/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Participant;

import com.liferay.document.library.kernel.model.DLFileEntryModel;
import com.liferay.document.library.kernel.model.DLFolder;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.*;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;

import java.io.File;

import java.text.ParseException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Provides the local service interface for DuckRace. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see DuckRaceLocalServiceUtil
 * @see be.lions.duckrace.service.base.DuckRaceLocalServiceBaseImpl
 * @see be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface DuckRaceLocalService extends BaseLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DuckRaceLocalServiceUtil} to access the duck race local service. Add custom service methods to {@link be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isAdministrator(long userId)
		throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isLionsMember(long userId)
		throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isRegistrationPossible(long userId, Date date)
		throws PortalException, SystemException, ParseException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isReservationPossible(long userId, Date date)
		throws PortalException, SystemException, ParseException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isWebmaster(long userId)
		throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Company getCompany() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public float getLotPrice();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getNbStars(java.lang.String articleId)
		throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getAccountNoBelgium();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getAccountNoIban();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getAccountNoSwift();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getHostName()
		throws PortalException, SystemException;

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getProperty(java.lang.String key);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getSiteTitle() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String[] getProperties(java.lang.String key);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Date getAdminRegistrationDeadline() throws ParseException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Date getAdminReservationDeadline() throws ParseException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<DLFolder> getGalleryFolders()
		throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<DLFileEntryModel> getGalleryImages(DLFolder folder)
		throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.String> getTagsForArticle(java.lang.String articleId)
		throws PortalException, SystemException;

	public List reverseList(List input);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getAdminId() throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getCompanyId() throws SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getGroupId() throws PortalException, SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getRoleId(java.lang.String roleName)
		throws PortalException, SystemException;

	public void sendMail(User registerer, java.lang.String content,
		Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, Participant to) throws java.lang.Exception;

	public void sendMail(User registerer, java.lang.String content,
		Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, Participant to, List<File> attachments)
		throws java.lang.Exception;

	public void sendMail(java.lang.String content,
		Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, Participant to) throws java.lang.Exception;
}