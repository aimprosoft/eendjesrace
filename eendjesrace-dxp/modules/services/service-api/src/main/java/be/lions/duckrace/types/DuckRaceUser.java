package be.lions.duckrace.types;

public enum DuckRaceUser {

	PARATEL("paratel@duckrace.be");
	
	
	private String email;
	
	private DuckRaceUser(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
}
