package be.lions.duckrace.util_service_api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListUtils {

	public static <P> List<P> reverse(List<P> input) {
		List<P> result = new ArrayList<P>(input);
		Collections.reverse(result);
		return result;
	}
	
}
