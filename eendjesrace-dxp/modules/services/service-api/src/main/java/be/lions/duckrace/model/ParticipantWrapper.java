/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Participant}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Participant
 * @generated
 */
@ProviderType
public class ParticipantWrapper implements Participant,
	ModelWrapper<Participant> {
	public ParticipantWrapper(Participant participant) {
		_participant = participant;
	}

	@Override
	public Class<?> getModelClass() {
		return Participant.class;
	}

	@Override
	public String getModelClassName() {
		return Participant.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("participantId", getParticipantId());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("mobileNumber", getMobileNumber());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("contestParticipant", getContestParticipant());
		attributes.put("mercedesDriver", getMercedesDriver());
		attributes.put("carBrand", getCarBrand());
		attributes.put("mercedesOld", getMercedesOld());
		attributes.put("language", getLanguage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long participantId = (Long)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String mobileNumber = (String)attributes.get("mobileNumber");

		if (mobileNumber != null) {
			setMobileNumber(mobileNumber);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Boolean contestParticipant = (Boolean)attributes.get(
				"contestParticipant");

		if (contestParticipant != null) {
			setContestParticipant(contestParticipant);
		}

		Boolean mercedesDriver = (Boolean)attributes.get("mercedesDriver");

		if (mercedesDriver != null) {
			setMercedesDriver(mercedesDriver);
		}

		String carBrand = (String)attributes.get("carBrand");

		if (carBrand != null) {
			setCarBrand(carBrand);
		}

		Boolean mercedesOld = (Boolean)attributes.get("mercedesOld");

		if (mercedesOld != null) {
			setMercedesOld(mercedesOld);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}
	}

	@Override
	public be.lions.duckrace.model.Participant toEscapedModel() {
		return new ParticipantWrapper(_participant.toEscapedModel());
	}

	@Override
	public be.lions.duckrace.model.Participant toUnescapedModel() {
		return new ParticipantWrapper(_participant.toUnescapedModel());
	}

	/**
	* Returns the contest participant of this participant.
	*
	* @return the contest participant of this participant
	*/
	@Override
	public boolean getContestParticipant() {
		return _participant.getContestParticipant();
	}

	/**
	* Returns the mercedes driver of this participant.
	*
	* @return the mercedes driver of this participant
	*/
	@Override
	public boolean getMercedesDriver() {
		return _participant.getMercedesDriver();
	}

	/**
	* Returns the mercedes old of this participant.
	*
	* @return the mercedes old of this participant
	*/
	@Override
	public boolean getMercedesOld() {
		return _participant.getMercedesOld();
	}

	@Override
	public boolean isCachedModel() {
		return _participant.isCachedModel();
	}

	/**
	* Returns <code>true</code> if this participant is contest participant.
	*
	* @return <code>true</code> if this participant is contest participant; <code>false</code> otherwise
	*/
	@Override
	public boolean isContestParticipant() {
		return _participant.isContestParticipant();
	}

	@Override
	public boolean isEscapedModel() {
		return _participant.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this participant is mercedes driver.
	*
	* @return <code>true</code> if this participant is mercedes driver; <code>false</code> otherwise
	*/
	@Override
	public boolean isMercedesDriver() {
		return _participant.isMercedesDriver();
	}

	/**
	* Returns <code>true</code> if this participant is mercedes old.
	*
	* @return <code>true</code> if this participant is mercedes old; <code>false</code> otherwise
	*/
	@Override
	public boolean isMercedesOld() {
		return _participant.isMercedesOld();
	}

	@Override
	public boolean isNew() {
		return _participant.isNew();
	}

	@Override
	public boolean matchesAll(java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress) {
		return _participant.matchesAll(firstName, lastName, emailAddress);
	}

	@Override
	public boolean matchesOneOf(java.lang.String firstName,
		java.lang.String lastName, java.lang.String emailAddress) {
		return _participant.matchesOneOf(firstName, lastName, emailAddress);
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _participant.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<be.lions.duckrace.model.Participant> toCacheModel() {
		return _participant.toCacheModel();
	}

	@Override
	public int compareTo(be.lions.duckrace.model.Participant participant) {
		return _participant.compareTo(participant);
	}

	@Override
	public int hashCode() {
		return _participant.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _participant.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ParticipantWrapper((Participant)_participant.clone());
	}

	/**
	* Returns the car brand of this participant.
	*
	* @return the car brand of this participant
	*/
	@Override
	public java.lang.String getCarBrand() {
		return _participant.getCarBrand();
	}

	/**
	* Returns the email address of this participant.
	*
	* @return the email address of this participant
	*/
	@Override
	public java.lang.String getEmailAddress() {
		return _participant.getEmailAddress();
	}

	/**
	* Returns the first name of this participant.
	*
	* @return the first name of this participant
	*/
	@Override
	public java.lang.String getFirstName() {
		return _participant.getFirstName();
	}

	@Override
	public java.lang.String getFormattedMobileNumber() {
		return _participant.getFormattedMobileNumber();
	}

	@Override
	public java.lang.String getFullName() {
		return _participant.getFullName();
	}

	/**
	* Returns the language of this participant.
	*
	* @return the language of this participant
	*/
	@Override
	public java.lang.String getLanguage() {
		return _participant.getLanguage();
	}

	/**
	* Returns the last name of this participant.
	*
	* @return the last name of this participant
	*/
	@Override
	public java.lang.String getLastName() {
		return _participant.getLastName();
	}

	/**
	* Returns the mobile number of this participant.
	*
	* @return the mobile number of this participant
	*/
	@Override
	public java.lang.String getMobileNumber() {
		return _participant.getMobileNumber();
	}

	@Override
	public java.lang.String getTransactionSafeEmailAddress() {
		return _participant.getTransactionSafeEmailAddress();
	}

	@Override
	public java.lang.String toString() {
		return _participant.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _participant.toXmlString();
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Lot> getLots()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _participant.getLots();
	}

	/**
	* Returns the participant ID of this participant.
	*
	* @return the participant ID of this participant
	*/
	@Override
	public long getParticipantId() {
		return _participant.getParticipantId();
	}

	/**
	* Returns the primary key of this participant.
	*
	* @return the primary key of this participant
	*/
	@Override
	public long getPrimaryKey() {
		return _participant.getPrimaryKey();
	}

	@Override
	public void persist() {
		_participant.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_participant.setCachedModel(cachedModel);
	}

	/**
	* Sets the car brand of this participant.
	*
	* @param carBrand the car brand of this participant
	*/
	@Override
	public void setCarBrand(java.lang.String carBrand) {
		_participant.setCarBrand(carBrand);
	}

	/**
	* Sets whether this participant is contest participant.
	*
	* @param contestParticipant the contest participant of this participant
	*/
	@Override
	public void setContestParticipant(boolean contestParticipant) {
		_participant.setContestParticipant(contestParticipant);
	}

	/**
	* Sets the email address of this participant.
	*
	* @param emailAddress the email address of this participant
	*/
	@Override
	public void setEmailAddress(java.lang.String emailAddress) {
		_participant.setEmailAddress(emailAddress);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_participant.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_participant.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_participant.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this participant.
	*
	* @param firstName the first name of this participant
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_participant.setFirstName(firstName);
	}

	/**
	* Sets the language of this participant.
	*
	* @param language the language of this participant
	*/
	@Override
	public void setLanguage(java.lang.String language) {
		_participant.setLanguage(language);
	}

	/**
	* Sets the last name of this participant.
	*
	* @param lastName the last name of this participant
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_participant.setLastName(lastName);
	}

	/**
	* Sets whether this participant is mercedes driver.
	*
	* @param mercedesDriver the mercedes driver of this participant
	*/
	@Override
	public void setMercedesDriver(boolean mercedesDriver) {
		_participant.setMercedesDriver(mercedesDriver);
	}

	/**
	* Sets whether this participant is mercedes old.
	*
	* @param mercedesOld the mercedes old of this participant
	*/
	@Override
	public void setMercedesOld(boolean mercedesOld) {
		_participant.setMercedesOld(mercedesOld);
	}

	/**
	* Sets the mobile number of this participant.
	*
	* @param mobileNumber the mobile number of this participant
	*/
	@Override
	public void setMobileNumber(java.lang.String mobileNumber) {
		_participant.setMobileNumber(mobileNumber);
	}

	@Override
	public void setNew(boolean n) {
		_participant.setNew(n);
	}

	/**
	* Sets the participant ID of this participant.
	*
	* @param participantId the participant ID of this participant
	*/
	@Override
	public void setParticipantId(long participantId) {
		_participant.setParticipantId(participantId);
	}

	/**
	* Sets the primary key of this participant.
	*
	* @param primaryKey the primary key of this participant
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_participant.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_participant.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public void validate() {
		_participant.validate();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParticipantWrapper)) {
			return false;
		}

		ParticipantWrapper participantWrapper = (ParticipantWrapper)obj;

		if (Objects.equals(_participant, participantWrapper._participant)) {
			return true;
		}

		return false;
	}

	@Override
	public Participant getWrappedModel() {
		return _participant;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _participant.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _participant.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_participant.resetOriginalValues();
	}

	private final Participant _participant;
}