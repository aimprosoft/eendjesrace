/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the Partner service. Represents a row in the &quot;DuckRace_Partner&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link be.lions.duckrace.model.impl.PartnerModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link be.lions.duckrace.model.impl.PartnerImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Partner
 * @see be.lions.duckrace.model.impl.PartnerImpl
 * @see be.lions.duckrace.model.impl.PartnerModelImpl
 * @generated
 */
@ProviderType
public interface PartnerModel extends BaseModel<Partner> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a partner model instance should use the {@link Partner} interface instead.
	 */

	/**
	 * Returns the primary key of this partner.
	 *
	 * @return the primary key of this partner
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this partner.
	 *
	 * @param primaryKey the primary key of this partner
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the partner ID of this partner.
	 *
	 * @return the partner ID of this partner
	 */
	public long getPartnerId();

	/**
	 * Sets the partner ID of this partner.
	 *
	 * @param partnerId the partner ID of this partner
	 */
	public void setPartnerId(long partnerId);

	/**
	 * Returns the category of this partner.
	 *
	 * @return the category of this partner
	 */
	@AutoEscape
	public String getCategory();

	/**
	 * Sets the category of this partner.
	 *
	 * @param category the category of this partner
	 */
	public void setCategory(String category);

	/**
	 * Returns the name of this partner.
	 *
	 * @return the name of this partner
	 */
	@AutoEscape
	public String getName();

	/**
	 * Sets the name of this partner.
	 *
	 * @param name the name of this partner
	 */
	public void setName(String name);

	/**
	 * Returns the returned lots count of this partner.
	 *
	 * @return the returned lots count of this partner
	 */
	public int getReturnedLotsCount();

	/**
	 * Sets the returned lots count of this partner.
	 *
	 * @param returnedLotsCount the returned lots count of this partner
	 */
	public void setReturnedLotsCount(int returnedLotsCount);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(be.lions.duckrace.model.Partner partner);

	@Override
	public int hashCode();

	@Override
	public CacheModel<be.lions.duckrace.model.Partner> toCacheModel();

	@Override
	public be.lions.duckrace.model.Partner toEscapedModel();

	@Override
	public be.lions.duckrace.model.Partner toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}