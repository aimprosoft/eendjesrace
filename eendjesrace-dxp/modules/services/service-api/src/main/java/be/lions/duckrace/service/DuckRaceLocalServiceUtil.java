/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for DuckRace. This utility wraps
 * {@link be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DuckRaceLocalService
 * @see be.lions.duckrace.service.base.DuckRaceLocalServiceBaseImpl
 * @see be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl
 * @generated
 */
@ProviderType
public class DuckRaceLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link be.lions.duckrace.service.impl.DuckRaceLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean isAdministrator(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().isAdministrator(userId);
	}

	public static boolean isLionsMember(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().isLionsMember(userId);
	}

	public static boolean isRegistrationPossible(long userId,
		java.util.Date date)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.text.ParseException {
		return getService().isRegistrationPossible(userId, date);
	}

	public static boolean isReservationPossible(long userId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.text.ParseException {
		return getService().isReservationPossible(userId, date);
	}

	public static boolean isWebmaster(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().isWebmaster(userId);
	}

	public static com.liferay.portal.kernel.model.Company getCompany()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCompany();
	}

	public static float getLotPrice() {
		return getService().getLotPrice();
	}

	public static int getNbStars(java.lang.String articleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getNbStars(articleId);
	}

	public static java.lang.String getAccountNoBelgium() {
		return getService().getAccountNoBelgium();
	}

	public static java.lang.String getAccountNoIban() {
		return getService().getAccountNoIban();
	}

	public static java.lang.String getAccountNoSwift() {
		return getService().getAccountNoSwift();
	}

	public static java.lang.String getHostName()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getHostName();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String getProperty(java.lang.String key) {
		return getService().getProperty(key);
	}

	public static java.lang.String getSiteTitle()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getSiteTitle();
	}

	public static java.lang.String[] getProperties(java.lang.String key) {
		return getService().getProperties(key);
	}

	public static java.util.Date getAdminRegistrationDeadline()
		throws java.text.ParseException {
		return getService().getAdminRegistrationDeadline();
	}

	public static java.util.Date getAdminReservationDeadline()
		throws java.text.ParseException {
		return getService().getAdminReservationDeadline();
	}

	public static java.util.List<com.liferay.document.library.kernel.model.DLFolder> getGalleryFolders()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getGalleryFolders();
	}

	public static java.util.List<com.liferay.document.library.kernel.model.DLFileEntryModel> getGalleryImages(
		com.liferay.document.library.kernel.model.DLFolder folder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getGalleryImages(folder);
	}

	public static java.util.List<java.lang.String> getTagsForArticle(
		java.lang.String articleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getTagsForArticle(articleId);
	}

	public static java.util.List reverseList(java.util.List input) {
		return getService().reverseList(input);
	}

	public static long getAdminId()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getAdminId();
	}

	public static long getCompanyId()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCompanyId();
	}

	public static long getGroupId()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getGroupId();
	}

	public static long getRoleId(java.lang.String roleName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getRoleId(roleName);
	}

	public static void sendMail(
		com.liferay.portal.kernel.model.User registerer,
		java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to)
		throws java.lang.Exception {
		getService().sendMail(registerer, content, params, subject, to);
	}

	public static void sendMail(
		com.liferay.portal.kernel.model.User registerer,
		java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to,
		java.util.List<java.io.File> attachments) throws java.lang.Exception {
		getService()
			.sendMail(registerer, content, params, subject, to, attachments);
	}

	public static void sendMail(java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to)
		throws java.lang.Exception {
		getService().sendMail(content, params, subject, to);
	}

	public static DuckRaceLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DuckRaceLocalService, DuckRaceLocalService> _serviceTracker =
		ServiceTrackerFactory.open(DuckRaceLocalService.class);
}