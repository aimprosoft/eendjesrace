/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PartnerLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PartnerLocalService
 * @generated
 */
@ProviderType
public class PartnerLocalServiceWrapper implements PartnerLocalService,
	ServiceWrapper<PartnerLocalService> {
	public PartnerLocalServiceWrapper(PartnerLocalService partnerLocalService) {
		_partnerLocalService = partnerLocalService;
	}

	/**
	* Adds the partner to the database. Also notifies the appropriate model listeners.
	*
	* @param partner the partner
	* @return the partner that was added
	*/
	@Override
	public be.lions.duckrace.model.Partner addPartner(
		be.lions.duckrace.model.Partner partner) {
		return _partnerLocalService.addPartner(partner);
	}

	@Override
	public be.lions.duckrace.model.Partner createPartner()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partnerLocalService.createPartner();
	}

	/**
	* Creates a new partner with the primary key. Does not add the partner to the database.
	*
	* @param partnerId the primary key for the new partner
	* @return the new partner
	*/
	@Override
	public be.lions.duckrace.model.Partner createPartner(long partnerId) {
		return _partnerLocalService.createPartner(partnerId);
	}

	/**
	* Deletes the partner from the database. Also notifies the appropriate model listeners.
	*
	* @param partner the partner
	* @return the partner that was removed
	*/
	@Override
	public be.lions.duckrace.model.Partner deletePartner(
		be.lions.duckrace.model.Partner partner) {
		return _partnerLocalService.deletePartner(partner);
	}

	/**
	* Deletes the partner with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param partnerId the primary key of the partner
	* @return the partner that was removed
	* @throws PortalException if a partner with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.Partner deletePartner(long partnerId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _partnerLocalService.deletePartner(partnerId);
	}

	@Override
	public be.lions.duckrace.model.Partner fetchPartner(long partnerId) {
		return _partnerLocalService.fetchPartner(partnerId);
	}

	/**
	* Returns the partner with the primary key.
	*
	* @param partnerId the primary key of the partner
	* @return the partner
	* @throws PortalException if a partner with the primary key could not be found
	*/
	@Override
	public be.lions.duckrace.model.Partner getPartner(long partnerId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _partnerLocalService.getPartner(partnerId);
	}

	@Override
	public be.lions.duckrace.model.Partner save(
		be.lions.duckrace.model.Partner partner,
		java.util.List<be.lions.duckrace.model.LotRange> lotRanges)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _partnerLocalService.save(partner, lotRanges);
	}

	/**
	* Updates the partner in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param partner the partner
	* @return the partner that was updated
	*/
	@Override
	public be.lions.duckrace.model.Partner updatePartner(
		be.lions.duckrace.model.Partner partner) {
		return _partnerLocalService.updatePartner(partner);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _partnerLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _partnerLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _partnerLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _partnerLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _partnerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of partners.
	*
	* @return the number of partners
	*/
	@Override
	public int getPartnersCount() {
		return _partnerLocalService.getPartnersCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _partnerLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _partnerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.PartnerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _partnerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.PartnerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _partnerLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<java.lang.String> getCategories()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partnerLocalService.getCategories();
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Partner> getPartners()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partnerLocalService.getPartners();
	}

	/**
	* Returns a range of all the partners.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link be.lions.duckrace.model.impl.PartnerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of partners
	* @param end the upper bound of the range of partners (not inclusive)
	* @return the range of partners
	*/
	@Override
	public java.util.List<be.lions.duckrace.model.Partner> getPartners(
		int start, int end) {
		return _partnerLocalService.getPartners(start, end);
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Partner> getPartners(
		java.lang.String category)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _partnerLocalService.getPartners(category);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _partnerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _partnerLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public PartnerLocalService getWrappedService() {
		return _partnerLocalService;
	}

	@Override
	public void setWrappedService(PartnerLocalService partnerLocalService) {
		_partnerLocalService = partnerLocalService;
	}

	private PartnerLocalService _partnerLocalService;
}