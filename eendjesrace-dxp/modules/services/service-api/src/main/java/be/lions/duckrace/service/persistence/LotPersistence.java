/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.Lot;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the lot service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see be.lions.duckrace.service.persistence.impl.LotPersistenceImpl
 * @see LotUtil
 * @generated
 */
@ProviderType
public interface LotPersistence extends BasePersistence<Lot> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LotUtil} to access the lot persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the lots where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @return the matching lots
	*/
	public java.util.List<Lot> findByParticipant(long participantId);

	/**
	* Returns a range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public java.util.List<Lot> findByParticipant(long participantId, int start,
		int end);

	/**
	* Returns an ordered range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByParticipant(long participantId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns an ordered range of all the lots where participantId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param participantId the participant ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByParticipant(long participantId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByParticipant_First(long participantId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the first lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByParticipant_First(long participantId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the last lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByParticipant_Last(long participantId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the last lot in the ordered set where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByParticipant_Last(long participantId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the lots before and after the current lot in the ordered set where participantId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param participantId the participant ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public Lot[] findByParticipant_PrevAndNext(java.lang.String code,
		long participantId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Removes all the lots where participantId = &#63; from the database.
	*
	* @param participantId the participant ID
	*/
	public void removeByParticipant(long participantId);

	/**
	* Returns the number of lots where participantId = &#63;.
	*
	* @param participantId the participant ID
	* @return the number of matching lots
	*/
	public int countByParticipant(long participantId);

	/**
	* Returns all the lots where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @return the matching lots
	*/
	public java.util.List<Lot> findByReservation(long reservationId);

	/**
	* Returns a range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public java.util.List<Lot> findByReservation(long reservationId, int start,
		int end);

	/**
	* Returns an ordered range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByReservation(long reservationId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns an ordered range of all the lots where reservationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reservationId the reservation ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByReservation(long reservationId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByReservation_First(long reservationId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the first lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByReservation_First(long reservationId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the last lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByReservation_Last(long reservationId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the last lot in the ordered set where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByReservation_Last(long reservationId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the lots before and after the current lot in the ordered set where reservationId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param reservationId the reservation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public Lot[] findByReservation_PrevAndNext(java.lang.String code,
		long reservationId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Removes all the lots where reservationId = &#63; from the database.
	*
	* @param reservationId the reservation ID
	*/
	public void removeByReservation(long reservationId);

	/**
	* Returns the number of lots where reservationId = &#63;.
	*
	* @param reservationId the reservation ID
	* @return the number of matching lots
	*/
	public int countByReservation(long reservationId);

	/**
	* Returns the lot where lotNumber = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param lotNumber the lot number
	* @return the matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByLotNumber(int lotNumber) throws NoSuchLotException;

	/**
	* Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param lotNumber the lot number
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByLotNumber(int lotNumber);

	/**
	* Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param lotNumber the lot number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByLotNumber(int lotNumber, boolean retrieveFromCache);

	/**
	* Removes the lot where lotNumber = &#63; from the database.
	*
	* @param lotNumber the lot number
	* @return the lot that was removed
	*/
	public Lot removeByLotNumber(int lotNumber) throws NoSuchLotException;

	/**
	* Returns the number of lots where lotNumber = &#63;.
	*
	* @param lotNumber the lot number
	* @return the number of matching lots
	*/
	public int countByLotNumber(int lotNumber);

	/**
	* Returns the lot where number = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param number the number
	* @return the matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByNumber(int number) throws NoSuchLotException;

	/**
	* Returns the lot where number = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param number the number
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByNumber(int number);

	/**
	* Returns the lot where number = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param number the number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByNumber(int number, boolean retrieveFromCache);

	/**
	* Removes the lot where number = &#63; from the database.
	*
	* @param number the number
	* @return the lot that was removed
	*/
	public Lot removeByNumber(int number) throws NoSuchLotException;

	/**
	* Returns the number of lots where number = &#63;.
	*
	* @param number the number
	* @return the number of matching lots
	*/
	public int countByNumber(int number);

	/**
	* Returns all the lots where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @return the matching lots
	*/
	public java.util.List<Lot> findByPartner(long partnerId);

	/**
	* Returns a range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of matching lots
	*/
	public java.util.List<Lot> findByPartner(long partnerId, int start, int end);

	/**
	* Returns an ordered range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByPartner(long partnerId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns an ordered range of all the lots where partnerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param partnerId the partner ID
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lots
	*/
	public java.util.List<Lot> findByPartner(long partnerId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByPartner_First(long partnerId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the first lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByPartner_First(long partnerId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the last lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot
	* @throws NoSuchLotException if a matching lot could not be found
	*/
	public Lot findByPartner_Last(long partnerId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Returns the last lot in the ordered set where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot, or <code>null</code> if a matching lot could not be found
	*/
	public Lot fetchByPartner_Last(long partnerId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns the lots before and after the current lot in the ordered set where partnerId = &#63;.
	*
	* @param code the primary key of the current lot
	* @param partnerId the partner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public Lot[] findByPartner_PrevAndNext(java.lang.String code,
		long partnerId,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator)
		throws NoSuchLotException;

	/**
	* Removes all the lots where partnerId = &#63; from the database.
	*
	* @param partnerId the partner ID
	*/
	public void removeByPartner(long partnerId);

	/**
	* Returns the number of lots where partnerId = &#63;.
	*
	* @param partnerId the partner ID
	* @return the number of matching lots
	*/
	public int countByPartner(long partnerId);

	/**
	* Caches the lot in the entity cache if it is enabled.
	*
	* @param lot the lot
	*/
	public void cacheResult(Lot lot);

	/**
	* Caches the lots in the entity cache if it is enabled.
	*
	* @param lots the lots
	*/
	public void cacheResult(java.util.List<Lot> lots);

	/**
	* Creates a new lot with the primary key. Does not add the lot to the database.
	*
	* @param code the primary key for the new lot
	* @return the new lot
	*/
	public Lot create(java.lang.String code);

	/**
	* Removes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param code the primary key of the lot
	* @return the lot that was removed
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public Lot remove(java.lang.String code) throws NoSuchLotException;

	public Lot updateImpl(Lot lot);

	/**
	* Returns the lot with the primary key or throws a {@link NoSuchLotException} if it could not be found.
	*
	* @param code the primary key of the lot
	* @return the lot
	* @throws NoSuchLotException if a lot with the primary key could not be found
	*/
	public Lot findByPrimaryKey(java.lang.String code)
		throws NoSuchLotException;

	/**
	* Returns the lot with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param code the primary key of the lot
	* @return the lot, or <code>null</code> if a lot with the primary key could not be found
	*/
	public Lot fetchByPrimaryKey(java.lang.String code);

	@Override
	public java.util.Map<java.io.Serializable, Lot> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the lots.
	*
	* @return the lots
	*/
	public java.util.List<Lot> findAll();

	/**
	* Returns a range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @return the range of lots
	*/
	public java.util.List<Lot> findAll(int start, int end);

	/**
	* Returns an ordered range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lots
	*/
	public java.util.List<Lot> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator);

	/**
	* Returns an ordered range of all the lots.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lots
	* @param end the upper bound of the range of lots (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of lots
	*/
	public java.util.List<Lot> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Lot> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the lots from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of lots.
	*
	* @return the number of lots
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}