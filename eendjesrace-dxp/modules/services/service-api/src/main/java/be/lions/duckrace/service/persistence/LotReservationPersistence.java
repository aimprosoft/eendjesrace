/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchLotReservationException;
import be.lions.duckrace.model.LotReservation;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the lot reservation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see be.lions.duckrace.service.persistence.impl.LotReservationPersistenceImpl
 * @see LotReservationUtil
 * @generated
 */
@ProviderType
public interface LotReservationPersistence extends BasePersistence<LotReservation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LotReservationUtil} to access the lot reservation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the lot reservations where approved = &#63;.
	*
	* @param approved the approved
	* @return the matching lot reservations
	*/
	public java.util.List<LotReservation> findByApproved(boolean approved);

	/**
	* Returns a range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of matching lot reservations
	*/
	public java.util.List<LotReservation> findByApproved(boolean approved,
		int start, int end);

	/**
	* Returns an ordered range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lot reservations
	*/
	public java.util.List<LotReservation> findByApproved(boolean approved,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator);

	/**
	* Returns an ordered range of all the lot reservations where approved = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param approved the approved
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching lot reservations
	*/
	public java.util.List<LotReservation> findByApproved(boolean approved,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot reservation
	* @throws NoSuchLotReservationException if a matching lot reservation could not be found
	*/
	public LotReservation findByApproved_First(boolean approved,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException;

	/**
	* Returns the first lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	*/
	public LotReservation fetchByApproved_First(boolean approved,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator);

	/**
	* Returns the last lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot reservation
	* @throws NoSuchLotReservationException if a matching lot reservation could not be found
	*/
	public LotReservation findByApproved_Last(boolean approved,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException;

	/**
	* Returns the last lot reservation in the ordered set where approved = &#63;.
	*
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	*/
	public LotReservation fetchByApproved_Last(boolean approved,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator);

	/**
	* Returns the lot reservations before and after the current lot reservation in the ordered set where approved = &#63;.
	*
	* @param reservationId the primary key of the current lot reservation
	* @param approved the approved
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lot reservation
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public LotReservation[] findByApproved_PrevAndNext(long reservationId,
		boolean approved,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException;

	/**
	* Removes all the lot reservations where approved = &#63; from the database.
	*
	* @param approved the approved
	*/
	public void removeByApproved(boolean approved);

	/**
	* Returns the number of lot reservations where approved = &#63;.
	*
	* @param approved the approved
	* @return the number of matching lot reservations
	*/
	public int countByApproved(boolean approved);

	/**
	* Caches the lot reservation in the entity cache if it is enabled.
	*
	* @param lotReservation the lot reservation
	*/
	public void cacheResult(LotReservation lotReservation);

	/**
	* Caches the lot reservations in the entity cache if it is enabled.
	*
	* @param lotReservations the lot reservations
	*/
	public void cacheResult(java.util.List<LotReservation> lotReservations);

	/**
	* Creates a new lot reservation with the primary key. Does not add the lot reservation to the database.
	*
	* @param reservationId the primary key for the new lot reservation
	* @return the new lot reservation
	*/
	public LotReservation create(long reservationId);

	/**
	* Removes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation that was removed
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public LotReservation remove(long reservationId)
		throws NoSuchLotReservationException;

	public LotReservation updateImpl(LotReservation lotReservation);

	/**
	* Returns the lot reservation with the primary key or throws a {@link NoSuchLotReservationException} if it could not be found.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation
	* @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	*/
	public LotReservation findByPrimaryKey(long reservationId)
		throws NoSuchLotReservationException;

	/**
	* Returns the lot reservation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reservationId the primary key of the lot reservation
	* @return the lot reservation, or <code>null</code> if a lot reservation with the primary key could not be found
	*/
	public LotReservation fetchByPrimaryKey(long reservationId);

	@Override
	public java.util.Map<java.io.Serializable, LotReservation> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the lot reservations.
	*
	* @return the lot reservations
	*/
	public java.util.List<LotReservation> findAll();

	/**
	* Returns a range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @return the range of lot reservations
	*/
	public java.util.List<LotReservation> findAll(int start, int end);

	/**
	* Returns an ordered range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lot reservations
	*/
	public java.util.List<LotReservation> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator);

	/**
	* Returns an ordered range of all the lot reservations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lot reservations
	* @param end the upper bound of the range of lot reservations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of lot reservations
	*/
	public java.util.List<LotReservation> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the lot reservations from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of lot reservations.
	*
	* @return the number of lot reservations
	*/
	public int countAll();
}