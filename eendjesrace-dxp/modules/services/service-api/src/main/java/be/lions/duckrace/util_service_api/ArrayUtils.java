package be.lions.duckrace.util_service_api;

import com.liferay.portal.kernel.util.ArrayUtil;

public class ArrayUtils {

	public static <P> boolean isEmpty(P[] list) {
		return list == null || list.length == 0;
	}
	
	public static <P> boolean contains(P[] list, P element) {
		return ArrayUtil.contains(list, element);
	}
	
}
