/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.itextpdf.text.DocumentException;
import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the LotReservation service. Represents a row in the &quot;DuckRace_LotReservation&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationModel
 * @see be.lions.duckrace.model.impl.LotReservationImpl
 * @see be.lions.duckrace.model.impl.LotReservationModelImpl
 * @generated
 */
@ImplementationClassName("be.lions.duckrace.model.impl.LotReservationImpl")
@ProviderType
public interface LotReservation extends LotReservationModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link be.lions.duckrace.model.impl.LotReservationImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<LotReservation, Long> RESERVATION_ID_ACCESSOR = new Accessor<LotReservation, Long>() {
			@Override
			public Long get(LotReservation lotReservation) {
				return lotReservation.getReservationId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<LotReservation> getTypeClass() {
				return LotReservation.class;
			}
		};

	public int getTotalCost();

	public be.lions.duckrace.model.Participant getParticipant()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	public java.util.List<be.lions.duckrace.model.Lot> getLots()
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.io.File generatePdf()
		throws DocumentException,
			com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.io.IOException;
}