package be.lions.duckrace.types;

import be.lions.duckrace.util_service_api.StringUtils;

public enum DuckRaceTagset {

	SPONSOR;
	
	public String getName() {
		return StringUtils.fromEnumToName(name());
	}
	
}
