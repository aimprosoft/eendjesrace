/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Participant;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the participant service. This utility wraps {@link be.lions.duckrace.service.persistence.impl.ParticipantPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParticipantPersistence
 * @see be.lions.duckrace.service.persistence.impl.ParticipantPersistenceImpl
 * @generated
 */
@ProviderType
public class ParticipantUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Participant participant) {
		getPersistence().clearCache(participant);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Participant> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Participant> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Participant> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Participant> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Participant update(Participant participant) {
		return getPersistence().update(participant);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Participant update(Participant participant,
		ServiceContext serviceContext) {
		return getPersistence().update(participant, serviceContext);
	}

	/**
	* Returns the participant where mobileNumber = &#63; or throws a {@link NoSuchParticipantException} if it could not be found.
	*
	* @param mobileNumber the mobile number
	* @return the matching participant
	* @throws NoSuchParticipantException if a matching participant could not be found
	*/
	public static Participant findByMobileNumber(java.lang.String mobileNumber)
		throws be.lions.duckrace.exception.NoSuchParticipantException {
		return getPersistence().findByMobileNumber(mobileNumber);
	}

	/**
	* Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param mobileNumber the mobile number
	* @return the matching participant, or <code>null</code> if a matching participant could not be found
	*/
	public static Participant fetchByMobileNumber(java.lang.String mobileNumber) {
		return getPersistence().fetchByMobileNumber(mobileNumber);
	}

	/**
	* Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param mobileNumber the mobile number
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching participant, or <code>null</code> if a matching participant could not be found
	*/
	public static Participant fetchByMobileNumber(
		java.lang.String mobileNumber, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByMobileNumber(mobileNumber, retrieveFromCache);
	}

	/**
	* Removes the participant where mobileNumber = &#63; from the database.
	*
	* @param mobileNumber the mobile number
	* @return the participant that was removed
	*/
	public static Participant removeByMobileNumber(
		java.lang.String mobileNumber)
		throws be.lions.duckrace.exception.NoSuchParticipantException {
		return getPersistence().removeByMobileNumber(mobileNumber);
	}

	/**
	* Returns the number of participants where mobileNumber = &#63;.
	*
	* @param mobileNumber the mobile number
	* @return the number of matching participants
	*/
	public static int countByMobileNumber(java.lang.String mobileNumber) {
		return getPersistence().countByMobileNumber(mobileNumber);
	}

	/**
	* Caches the participant in the entity cache if it is enabled.
	*
	* @param participant the participant
	*/
	public static void cacheResult(Participant participant) {
		getPersistence().cacheResult(participant);
	}

	/**
	* Caches the participants in the entity cache if it is enabled.
	*
	* @param participants the participants
	*/
	public static void cacheResult(List<Participant> participants) {
		getPersistence().cacheResult(participants);
	}

	/**
	* Creates a new participant with the primary key. Does not add the participant to the database.
	*
	* @param participantId the primary key for the new participant
	* @return the new participant
	*/
	public static Participant create(long participantId) {
		return getPersistence().create(participantId);
	}

	/**
	* Removes the participant with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param participantId the primary key of the participant
	* @return the participant that was removed
	* @throws NoSuchParticipantException if a participant with the primary key could not be found
	*/
	public static Participant remove(long participantId)
		throws be.lions.duckrace.exception.NoSuchParticipantException {
		return getPersistence().remove(participantId);
	}

	public static Participant updateImpl(Participant participant) {
		return getPersistence().updateImpl(participant);
	}

	/**
	* Returns the participant with the primary key or throws a {@link NoSuchParticipantException} if it could not be found.
	*
	* @param participantId the primary key of the participant
	* @return the participant
	* @throws NoSuchParticipantException if a participant with the primary key could not be found
	*/
	public static Participant findByPrimaryKey(long participantId)
		throws be.lions.duckrace.exception.NoSuchParticipantException {
		return getPersistence().findByPrimaryKey(participantId);
	}

	/**
	* Returns the participant with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param participantId the primary key of the participant
	* @return the participant, or <code>null</code> if a participant with the primary key could not be found
	*/
	public static Participant fetchByPrimaryKey(long participantId) {
		return getPersistence().fetchByPrimaryKey(participantId);
	}

	public static java.util.Map<java.io.Serializable, Participant> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the participants.
	*
	* @return the participants
	*/
	public static List<Participant> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @return the range of participants
	*/
	public static List<Participant> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of participants
	*/
	public static List<Participant> findAll(int start, int end,
		OrderByComparator<Participant> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the participants.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of participants
	* @param end the upper bound of the range of participants (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of participants
	*/
	public static List<Participant> findAll(int start, int end,
		OrderByComparator<Participant> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the participants from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of participants.
	*
	* @return the number of participants
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ParticipantPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ParticipantPersistence, ParticipantPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ParticipantPersistence.class);
}