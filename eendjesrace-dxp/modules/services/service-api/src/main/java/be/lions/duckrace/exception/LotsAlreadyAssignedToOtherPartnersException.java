package be.lions.duckrace.exception;

import java.util.List;

import be.lions.duckrace.model.LotRangeAssignment;

import com.liferay.portal.kernel.exception.PortalException;


public class LotsAlreadyAssignedToOtherPartnersException extends PortalException {

    public LotsAlreadyAssignedToOtherPartnersException(List<LotRangeAssignment> assignments) {
        super(createMessage(assignments));
    }

    private static String createMessage(List<LotRangeAssignment> assignments) {
        StringBuilder sb = new StringBuilder();
        for(LotRangeAssignment assignment: assignments) {
            int from = assignment.getLotRange().getFrom();
            int to = assignment.getLotRange().getTo();
            String partnerName = assignment.getPartnerName();
            sb.append("Lotjes met lotnummers "+from + " tot "+ to+ " zijn reeds toegewezen aan partner "+partnerName+".<br/>");
        }
        return sb.toString();
    }

}