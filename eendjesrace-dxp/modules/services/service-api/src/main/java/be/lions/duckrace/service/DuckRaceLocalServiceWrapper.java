/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DuckRaceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DuckRaceLocalService
 * @generated
 */
@ProviderType
public class DuckRaceLocalServiceWrapper implements DuckRaceLocalService,
	ServiceWrapper<DuckRaceLocalService> {
	public DuckRaceLocalServiceWrapper(
		DuckRaceLocalService duckRaceLocalService) {
		_duckRaceLocalService = duckRaceLocalService;
	}

	@Override
	public boolean isAdministrator(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.isAdministrator(userId);
	}

	@Override
	public boolean isLionsMember(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.isLionsMember(userId);
	}

	@Override
	public boolean isRegistrationPossible(long userId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.text.ParseException {
		return _duckRaceLocalService.isRegistrationPossible(userId, date);
	}

	@Override
	public boolean isReservationPossible(long userId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.text.ParseException {
		return _duckRaceLocalService.isReservationPossible(userId, date);
	}

	@Override
	public boolean isWebmaster(long userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.isWebmaster(userId);
	}

	@Override
	public com.liferay.portal.kernel.model.Company getCompany()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getCompany();
	}

	@Override
	public float getLotPrice() {
		return _duckRaceLocalService.getLotPrice();
	}

	@Override
	public int getNbStars(java.lang.String articleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getNbStars(articleId);
	}

	@Override
	public java.lang.String getAccountNoBelgium() {
		return _duckRaceLocalService.getAccountNoBelgium();
	}

	@Override
	public java.lang.String getAccountNoIban() {
		return _duckRaceLocalService.getAccountNoIban();
	}

	@Override
	public java.lang.String getAccountNoSwift() {
		return _duckRaceLocalService.getAccountNoSwift();
	}

	@Override
	public java.lang.String getHostName()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getHostName();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _duckRaceLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getProperty(java.lang.String key) {
		return _duckRaceLocalService.getProperty(key);
	}

	@Override
	public java.lang.String getSiteTitle()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getSiteTitle();
	}

	@Override
	public java.lang.String[] getProperties(java.lang.String key) {
		return _duckRaceLocalService.getProperties(key);
	}

	@Override
	public java.util.Date getAdminRegistrationDeadline()
		throws java.text.ParseException {
		return _duckRaceLocalService.getAdminRegistrationDeadline();
	}

	@Override
	public java.util.Date getAdminReservationDeadline()
		throws java.text.ParseException {
		return _duckRaceLocalService.getAdminReservationDeadline();
	}

	@Override
	public java.util.List<com.liferay.document.library.kernel.model.DLFolder> getGalleryFolders()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getGalleryFolders();
	}

	@Override
	public java.util.List<com.liferay.document.library.kernel.model.DLFileEntryModel> getGalleryImages(
		com.liferay.document.library.kernel.model.DLFolder folder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getGalleryImages(folder);
	}

	@Override
	public java.util.List<java.lang.String> getTagsForArticle(
		java.lang.String articleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getTagsForArticle(articleId);
	}

	@Override
	public java.util.List reverseList(java.util.List input) {
		return _duckRaceLocalService.reverseList(input);
	}

	@Override
	public long getAdminId()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getAdminId();
	}

	@Override
	public long getCompanyId()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getCompanyId();
	}

	@Override
	public long getGroupId()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getGroupId();
	}

	@Override
	public long getRoleId(java.lang.String roleName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _duckRaceLocalService.getRoleId(roleName);
	}

	@Override
	public void sendMail(com.liferay.portal.kernel.model.User registerer,
		java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to)
		throws java.lang.Exception {
		_duckRaceLocalService.sendMail(registerer, content, params, subject, to);
	}

	@Override
	public void sendMail(com.liferay.portal.kernel.model.User registerer,
		java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to,
		java.util.List<java.io.File> attachments) throws java.lang.Exception {
		_duckRaceLocalService.sendMail(registerer, content, params, subject,
			to, attachments);
	}

	@Override
	public void sendMail(java.lang.String content,
		java.util.Map<java.lang.String, java.lang.Object> params,
		java.lang.String subject, be.lions.duckrace.model.Participant to)
		throws java.lang.Exception {
		_duckRaceLocalService.sendMail(content, params, subject, to);
	}

	@Override
	public DuckRaceLocalService getWrappedService() {
		return _duckRaceLocalService;
	}

	@Override
	public void setWrappedService(DuckRaceLocalService duckRaceLocalService) {
		_duckRaceLocalService = duckRaceLocalService;
	}

	private DuckRaceLocalService _duckRaceLocalService;
}