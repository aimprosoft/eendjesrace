/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import com.itextpdf.text.DocumentException;
import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LotReservation}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotReservation
 * @generated
 */
@ProviderType
public class LotReservationWrapper implements LotReservation,
	ModelWrapper<LotReservation> {
	public LotReservationWrapper(LotReservation lotReservation) {
		_lotReservation = lotReservation;
	}

	@Override
	public Class<?> getModelClass() {
		return LotReservation.class;
	}

	@Override
	public String getModelClassName() {
		return LotReservation.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("reservationId", getReservationId());
		attributes.put("participantId", getParticipantId());
		attributes.put("numberLots", getNumberLots());
		attributes.put("approved", getApproved());
		attributes.put("reservationDate", getReservationDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long reservationId = (Long)attributes.get("reservationId");

		if (reservationId != null) {
			setReservationId(reservationId);
		}

		Long participantId = (Long)attributes.get("participantId");

		if (participantId != null) {
			setParticipantId(participantId);
		}

		Integer numberLots = (Integer)attributes.get("numberLots");

		if (numberLots != null) {
			setNumberLots(numberLots);
		}

		Boolean approved = (Boolean)attributes.get("approved");

		if (approved != null) {
			setApproved(approved);
		}

		Date reservationDate = (Date)attributes.get("reservationDate");

		if (reservationDate != null) {
			setReservationDate(reservationDate);
		}
	}

	@Override
	public be.lions.duckrace.model.LotReservation toEscapedModel() {
		return new LotReservationWrapper(_lotReservation.toEscapedModel());
	}

	@Override
	public be.lions.duckrace.model.LotReservation toUnescapedModel() {
		return new LotReservationWrapper(_lotReservation.toUnescapedModel());
	}

	@Override
	public be.lions.duckrace.model.Participant getParticipant()
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lotReservation.getParticipant();
	}

	/**
	* Returns the approved of this lot reservation.
	*
	* @return the approved of this lot reservation
	*/
	@Override
	public boolean getApproved() {
		return _lotReservation.getApproved();
	}

	/**
	* Returns <code>true</code> if this lot reservation is approved.
	*
	* @return <code>true</code> if this lot reservation is approved; <code>false</code> otherwise
	*/
	@Override
	public boolean isApproved() {
		return _lotReservation.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _lotReservation.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _lotReservation.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _lotReservation.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _lotReservation.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<be.lions.duckrace.model.LotReservation> toCacheModel() {
		return _lotReservation.toCacheModel();
	}

	@Override
	public int compareTo(be.lions.duckrace.model.LotReservation lotReservation) {
		return _lotReservation.compareTo(lotReservation);
	}

	/**
	* Returns the number lots of this lot reservation.
	*
	* @return the number lots of this lot reservation
	*/
	@Override
	public int getNumberLots() {
		return _lotReservation.getNumberLots();
	}

	@Override
	public int getTotalCost() {
		return _lotReservation.getTotalCost();
	}

	@Override
	public int hashCode() {
		return _lotReservation.hashCode();
	}

	@Override
	public java.io.File generatePdf()
		throws DocumentException,
			com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.io.IOException {
		return _lotReservation.generatePdf();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _lotReservation.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LotReservationWrapper((LotReservation)_lotReservation.clone());
	}

	@Override
	public java.lang.String toString() {
		return _lotReservation.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _lotReservation.toXmlString();
	}

	/**
	* Returns the reservation date of this lot reservation.
	*
	* @return the reservation date of this lot reservation
	*/
	@Override
	public Date getReservationDate() {
		return _lotReservation.getReservationDate();
	}

	@Override
	public java.util.List<be.lions.duckrace.model.Lot> getLots()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lotReservation.getLots();
	}

	/**
	* Returns the participant ID of this lot reservation.
	*
	* @return the participant ID of this lot reservation
	*/
	@Override
	public long getParticipantId() {
		return _lotReservation.getParticipantId();
	}

	/**
	* Returns the primary key of this lot reservation.
	*
	* @return the primary key of this lot reservation
	*/
	@Override
	public long getPrimaryKey() {
		return _lotReservation.getPrimaryKey();
	}

	/**
	* Returns the reservation ID of this lot reservation.
	*
	* @return the reservation ID of this lot reservation
	*/
	@Override
	public long getReservationId() {
		return _lotReservation.getReservationId();
	}

	@Override
	public void persist() {
		_lotReservation.persist();
	}

	/**
	* Sets whether this lot reservation is approved.
	*
	* @param approved the approved of this lot reservation
	*/
	@Override
	public void setApproved(boolean approved) {
		_lotReservation.setApproved(approved);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_lotReservation.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_lotReservation.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_lotReservation.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_lotReservation.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_lotReservation.setNew(n);
	}

	/**
	* Sets the number lots of this lot reservation.
	*
	* @param numberLots the number lots of this lot reservation
	*/
	@Override
	public void setNumberLots(int numberLots) {
		_lotReservation.setNumberLots(numberLots);
	}

	/**
	* Sets the participant ID of this lot reservation.
	*
	* @param participantId the participant ID of this lot reservation
	*/
	@Override
	public void setParticipantId(long participantId) {
		_lotReservation.setParticipantId(participantId);
	}

	/**
	* Sets the primary key of this lot reservation.
	*
	* @param primaryKey the primary key of this lot reservation
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_lotReservation.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_lotReservation.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the reservation date of this lot reservation.
	*
	* @param reservationDate the reservation date of this lot reservation
	*/
	@Override
	public void setReservationDate(Date reservationDate) {
		_lotReservation.setReservationDate(reservationDate);
	}

	/**
	* Sets the reservation ID of this lot reservation.
	*
	* @param reservationId the reservation ID of this lot reservation
	*/
	@Override
	public void setReservationId(long reservationId) {
		_lotReservation.setReservationId(reservationId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LotReservationWrapper)) {
			return false;
		}

		LotReservationWrapper lotReservationWrapper = (LotReservationWrapper)obj;

		if (Objects.equals(_lotReservation,
					lotReservationWrapper._lotReservation)) {
			return true;
		}

		return false;
	}

	@Override
	public LotReservation getWrappedModel() {
		return _lotReservation;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _lotReservation.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _lotReservation.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_lotReservation.resetOriginalValues();
	}

	private final LotReservation _lotReservation;
}