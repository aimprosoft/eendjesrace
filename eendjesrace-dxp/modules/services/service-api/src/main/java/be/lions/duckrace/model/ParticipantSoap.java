/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ParticipantSoap implements Serializable {
	public static ParticipantSoap toSoapModel(Participant model) {
		ParticipantSoap soapModel = new ParticipantSoap();

		soapModel.setParticipantId(model.getParticipantId());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setMobileNumber(model.getMobileNumber());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setContestParticipant(model.getContestParticipant());
		soapModel.setMercedesDriver(model.getMercedesDriver());
		soapModel.setCarBrand(model.getCarBrand());
		soapModel.setMercedesOld(model.getMercedesOld());
		soapModel.setLanguage(model.getLanguage());

		return soapModel;
	}

	public static ParticipantSoap[] toSoapModels(Participant[] models) {
		ParticipantSoap[] soapModels = new ParticipantSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ParticipantSoap[][] toSoapModels(Participant[][] models) {
		ParticipantSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ParticipantSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ParticipantSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ParticipantSoap[] toSoapModels(List<Participant> models) {
		List<ParticipantSoap> soapModels = new ArrayList<ParticipantSoap>(models.size());

		for (Participant model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ParticipantSoap[soapModels.size()]);
	}

	public ParticipantSoap() {
	}

	public long getPrimaryKey() {
		return _participantId;
	}

	public void setPrimaryKey(long pk) {
		setParticipantId(pk);
	}

	public long getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(long participantId) {
		_participantId = participantId;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public String getMobileNumber() {
		return _mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		_mobileNumber = mobileNumber;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public boolean getContestParticipant() {
		return _contestParticipant;
	}

	public boolean isContestParticipant() {
		return _contestParticipant;
	}

	public void setContestParticipant(boolean contestParticipant) {
		_contestParticipant = contestParticipant;
	}

	public boolean getMercedesDriver() {
		return _mercedesDriver;
	}

	public boolean isMercedesDriver() {
		return _mercedesDriver;
	}

	public void setMercedesDriver(boolean mercedesDriver) {
		_mercedesDriver = mercedesDriver;
	}

	public String getCarBrand() {
		return _carBrand;
	}

	public void setCarBrand(String carBrand) {
		_carBrand = carBrand;
	}

	public boolean getMercedesOld() {
		return _mercedesOld;
	}

	public boolean isMercedesOld() {
		return _mercedesOld;
	}

	public void setMercedesOld(boolean mercedesOld) {
		_mercedesOld = mercedesOld;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	private long _participantId;
	private String _emailAddress;
	private String _mobileNumber;
	private String _firstName;
	private String _lastName;
	private boolean _contestParticipant;
	private boolean _mercedesDriver;
	private String _carBrand;
	private boolean _mercedesOld;
	private String _language;
}