/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LotReservationSoap implements Serializable {
	public static LotReservationSoap toSoapModel(LotReservation model) {
		LotReservationSoap soapModel = new LotReservationSoap();

		soapModel.setReservationId(model.getReservationId());
		soapModel.setParticipantId(model.getParticipantId());
		soapModel.setNumberLots(model.getNumberLots());
		soapModel.setApproved(model.getApproved());
		soapModel.setReservationDate(model.getReservationDate());

		return soapModel;
	}

	public static LotReservationSoap[] toSoapModels(LotReservation[] models) {
		LotReservationSoap[] soapModels = new LotReservationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LotReservationSoap[][] toSoapModels(LotReservation[][] models) {
		LotReservationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LotReservationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LotReservationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LotReservationSoap[] toSoapModels(List<LotReservation> models) {
		List<LotReservationSoap> soapModels = new ArrayList<LotReservationSoap>(models.size());

		for (LotReservation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LotReservationSoap[soapModels.size()]);
	}

	public LotReservationSoap() {
	}

	public long getPrimaryKey() {
		return _reservationId;
	}

	public void setPrimaryKey(long pk) {
		setReservationId(pk);
	}

	public long getReservationId() {
		return _reservationId;
	}

	public void setReservationId(long reservationId) {
		_reservationId = reservationId;
	}

	public long getParticipantId() {
		return _participantId;
	}

	public void setParticipantId(long participantId) {
		_participantId = participantId;
	}

	public int getNumberLots() {
		return _numberLots;
	}

	public void setNumberLots(int numberLots) {
		_numberLots = numberLots;
	}

	public boolean getApproved() {
		return _approved;
	}

	public boolean isApproved() {
		return _approved;
	}

	public void setApproved(boolean approved) {
		_approved = approved;
	}

	public Date getReservationDate() {
		return _reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		_reservationDate = reservationDate;
	}

	private long _reservationId;
	private long _participantId;
	private int _numberLots;
	private boolean _approved;
	private Date _reservationDate;
}