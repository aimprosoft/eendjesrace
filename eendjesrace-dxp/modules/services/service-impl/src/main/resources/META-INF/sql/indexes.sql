create index IX_D03C9468 on DuckRace_Lot (lotNumber);
create unique index IX_E428D8A4 on DuckRace_Lot (number_);
create index IX_C6E0C71C on DuckRace_Lot (participantId);
create index IX_8B912031 on DuckRace_Lot (partnerId);
create index IX_809A2735 on DuckRace_Lot (reservationId);

create index IX_A8606DA1 on DuckRace_LotReservation (approved);

create unique index IX_20806CA3 on DuckRace_Participant (mobileNumber[$COLUMN_LENGTH:75$]);

create index IX_41D328EB on DuckRace_Partner (category[$COLUMN_LENGTH:75$]);