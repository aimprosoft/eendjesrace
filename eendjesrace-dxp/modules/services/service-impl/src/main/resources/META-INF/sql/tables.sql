create table DuckRace_Lot (
	code_ VARCHAR(75) not null primary key,
	checksum VARCHAR(75) null,
	lotNumber INTEGER,
	number_ INTEGER,
	pressed BOOLEAN,
	nbTries INTEGER,
	lockoutDate DATE null,
	participantId LONG,
	reservationId LONG,
	partnerId LONG,
	orderNumber VARCHAR(75) null
);

create table DuckRace_LotReservation (
	reservationId LONG not null primary key,
	participantId LONG,
	numberLots INTEGER,
	approved BOOLEAN,
	reservationDate DATE null
);

create table DuckRace_Participant (
	participantId LONG not null primary key,
	emailAddress VARCHAR(75) null,
	mobileNumber VARCHAR(75) null,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	contestParticipant BOOLEAN,
	mercedesDriver BOOLEAN,
	carBrand VARCHAR(75) null,
	mercedesOld BOOLEAN,
	language VARCHAR(75) null
);

create table DuckRace_Partner (
	partnerId LONG not null primary key,
	category VARCHAR(75) null,
	name VARCHAR(75) null,
	returnedLotsCount INTEGER
);