/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.exception.NoSuchParticipantException;
import be.lions.duckrace.exception.NoSuchPartnerException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import be.lions.duckrace.service.PartnerLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import org.apache.commons.lang3.time.DateUtils;


import java.util.Date;

/**
 * The extended model implementation for the Lot service. Represents a row in the &quot;DuckRace_Lot&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.model.Lot} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class LotImpl extends LotBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a lot model instance should use the {@link be.lions.duckrace.model.Lot} interface instead.
	 */
	public LotImpl() {
	}

	public static final int LOCK_DURATION_HOURS = 24;

	public boolean isLocked() {
		return getLockoutDate() != null;
	}

	public Date getUnlockDate() {
		if(getLockoutDate() == null) {
			return null;
		}
		return DateUtils.addHours(getLockoutDate(), LOCK_DURATION_HOURS);
	}

	public boolean isLockOverdue() {
		return new Date().compareTo(getUnlockDate()) > 0;
	}

	public Participant getParticipant() throws PortalException, SystemException {
		try {
			return ParticipantLocalServiceUtil.getParticipant(getParticipantId());
		} catch(NoSuchParticipantException e) {
			return null;
		}
	}

	public Partner getPartner() throws PortalException, SystemException {
		try {
			return PartnerLocalServiceUtil.getPartner(getPartnerId());
		} catch (NoSuchPartnerException e) {
			return null;
		}
	}
}