/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Partner;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Partner in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Partner
 * @generated
 */
@ProviderType
public class PartnerCacheModel implements CacheModel<Partner>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PartnerCacheModel)) {
			return false;
		}

		PartnerCacheModel partnerCacheModel = (PartnerCacheModel)obj;

		if (partnerId == partnerCacheModel.partnerId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, partnerId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{partnerId=");
		sb.append(partnerId);
		sb.append(", category=");
		sb.append(category);
		sb.append(", name=");
		sb.append(name);
		sb.append(", returnedLotsCount=");
		sb.append(returnedLotsCount);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Partner toEntityModel() {
		PartnerImpl partnerImpl = new PartnerImpl();

		partnerImpl.setPartnerId(partnerId);

		if (category == null) {
			partnerImpl.setCategory(StringPool.BLANK);
		}
		else {
			partnerImpl.setCategory(category);
		}

		if (name == null) {
			partnerImpl.setName(StringPool.BLANK);
		}
		else {
			partnerImpl.setName(name);
		}

		partnerImpl.setReturnedLotsCount(returnedLotsCount);

		partnerImpl.resetOriginalValues();

		return partnerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		partnerId = objectInput.readLong();
		category = objectInput.readUTF();
		name = objectInput.readUTF();

		returnedLotsCount = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(partnerId);

		if (category == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(category);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeInt(returnedLotsCount);
	}

	public long partnerId;
	public String category;
	public String name;
	public int returnedLotsCount;
}