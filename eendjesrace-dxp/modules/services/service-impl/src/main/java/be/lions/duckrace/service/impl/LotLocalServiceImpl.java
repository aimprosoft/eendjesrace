/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import be.lions.duckrace.model.*;
import be.lions.duckrace.model.impl.LotImpl;
import be.lions.duckrace.model.impl.LotReservationImpl;
import be.lions.duckrace.model.impl.ParticipantImpl;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.base.LotLocalServiceBaseImpl;
import be.lions.duckrace.util_service_impl.NumberSequenceUtil;
import be.lions.duckrace.util_service_api.StringUtils;
import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.dao.orm.*;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the lot local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.service.LotLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotLocalServiceBaseImpl
 * @see be.lions.duckrace.service.LotLocalServiceUtil
 */
@ProviderType
public class LotLocalServiceImpl extends LotLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link be.lions.duckrace.service.LotLocalServiceUtil} to access the lot local service.
	 */

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<Lot> getLotsForPartner(long partnerId) throws SystemException {
        DynamicQuery dq = createPartnerDQ(partnerId);
        dq.addOrder(OrderFactoryUtil.asc("lotNumber"));
        return (List) dynamicQuery(dq);
    }

    public Lot registerLot(String mobileNumber, String firstName, String lastName, String emailAddress,
                           String orderNumber, String language) throws SystemException, PortalException {
        validate(mobileNumber);
        Participant p = participantLocalService.getOrCreateParticipant(mobileNumber, firstName, lastName, emailAddress,
                false, false, null, false, language);

        Lot lot = getAvailableLot(false);
        lot.setOrderNumber(orderNumber);

        return assignLotToParticipant(lot, p);
    }

    public int getLotsForPartnerCount(long partnerId) throws SystemException {
        return getCount(createPartnerDQ(partnerId));
    }

    public int getRegisteredLotsForPartnerCount(long partnerId) throws SystemException {
        DynamicQuery dq = createPartnerDQ(partnerId);
        dq.add(PropertyFactoryUtil.forName("number").isNotNull());
        dq.add(PropertyFactoryUtil.forName("number").ne(0));
        return getCount(dq);
    }

    private DynamicQuery createPartnerDQ(long partnerId) {
        DynamicQuery dq = DynamicQueryFactoryUtil.forClass(LotImpl.class, LotImpl.TABLE_NAME,
                PortalClassLoaderUtil.getClassLoader());
        dq.add(PropertyFactoryUtil.forName("partnerId").eq(partnerId));
        return dq;
    }

    @SuppressWarnings("rawtypes")
    private int getCount(DynamicQuery dq) throws SystemException {
        dq.setProjection(ProjectionFactoryUtil.rowCount());
        return new BigDecimal((Long) ((List) dynamicQuery(dq)).get(0) ).intValueExact();
    }

    public void assignLotsToPartner(Partner partner, List<LotRange> lotRanges) throws SystemException, PortalException {
        // First check if there are overlapping lots

        List<LotRangeAssignment> assignments = new ArrayList<LotRangeAssignment>();
        for (LotRange range : lotRanges) {
            assignments
                    .addAll(executeQuery(String
                            .format("SELECT partnerId, MIN(lotNumber) AS from_, MAX(lotNumber) AS to_ FROM DuckRace_Lot WHERE lotNumber BETWEEN '%d' AND '%d' and partnerId != '%d' GROUP BY partnerId",
                                    range.getFrom(), range.getTo(), partner.getPartnerId())));
        }
        if (!assignments.isEmpty()) {
            throw new LotsAlreadyAssignedToOtherPartnersException(assignments);
        }
        // Unassign all lots
        executeUpdate(String.format("UPDATE DuckRace_Lot set partnerId = NULL WHERE partnerId = '%d';", partner.getPartnerId()));
        // Assign new lots to partner
        for (LotRange range : lotRanges) {
            executeUpdate(String.format("UPDATE DuckRace_Lot set partnerId = '%d' WHERE lotNumber BETWEEN '%d' AND '%d';",
                    partner.getPartnerId(), range.getFrom(), range.getTo()));
        }
    }

    private void executeUpdate(String sql) throws SystemException {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DataAccess.getConnection();
            ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SystemException(e);
        } finally {
            DataAccess.cleanUp(connection, ps, rs);
        }
    }

    private List<LotRangeAssignment> executeQuery(String sql) throws SystemException {
        List<LotRangeAssignment> result = new ArrayList<LotRangeAssignment>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DataAccess.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                long partnerId = rs.getLong("partnerId");
                if (partnerId == 0) {
                    continue;
                }
                LotRange range = new LotRange();
                range.setFrom(rs.getInt("from_"));
                range.setTo(rs.getInt("to_"));
                LotRangeAssignment assignment = new LotRangeAssignment();
                assignment.setLotRange(range);
                assignment.setPartnerId(partnerId);
                result.add(assignment);
            }
        } catch (SQLException e) {
            throw new SystemException(e);
        } finally {
            DataAccess.cleanUp(connection, ps, rs);
        }
        return result;
    }

    public int getPressedLotsCount() throws SystemException {
        DynamicQuery dq = DynamicQueryFactoryUtil.forClass(LotImpl.class, LotImpl.TABLE_NAME,
                PortalClassLoaderUtil.getClassLoader());
        dq.add(PropertyFactoryUtil.forName("pressed").eq(true));
        return getCount(dq);
    }

    public Lot createLot(String code, String checksum) throws SystemException {
        Lot lot = lotPersistence.create(code);
        lot.setCode(code);
        lot.setChecksum(checksum);
        return lotPersistence.update(lot);
    }

    public boolean codeExists(String code) throws SystemException {
        if (StringUtils.isEmpty(code)) {
            return false;
        }
        try {
            lotPersistence.findByPrimaryKey(code);
            return true;
        } catch (NoSuchLotException e) {
            return false;
        }
    }

    public Lot registerLot(String mobileNumber) throws SystemException, PortalException {
        validate(mobileNumber);
        Participant p = participantLocalService.getOrCreateParticipant(mobileNumber, null, null, null, false, false,
                null, false, "nl");
        return assignLotToParticipant(getAvailableLot(false), p);
    }

    private Lot assignLotToParticipant(Lot lot, Participant p) throws InvalidMobileNumberException, SystemException,
            NoSuchParticipantException {
        lot.setParticipantId(p.getParticipantId());
        lot.setNumber(getNextNumber());
        return lotPersistence.update(lot);
    }

    private int getNextNumber() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotImpl.class, LotImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .setProjection(ProjectionFactoryUtil.property("number"))
                .add(PropertyFactoryUtil.forName("number").isNotNull())
                .add(PropertyFactoryUtil.forName("participantId").isNotNull()).addOrder(OrderFactoryUtil.asc("number"));
        List<?> sequence = dynamicQuery(q);

        return NumberSequenceUtil.getFirstGapNumber((List<Integer>) sequence);
    }

    public int getCurrentNumber() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotImpl.class, LotImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("number").isNotNull()).setProjection(ProjectionFactoryUtil.rowCount());
        return (Integer) dynamicQuery(q).get(0);
    }

    public Lot registerLot(String mobileNumber, String firstName, String lastName, String emailAddress,
                           boolean contestParticipant, boolean mercedesDriver, String carBrand, boolean mercedesOld, String code,
                           String checksum, String language) throws SystemException, PortalException {
        validate(mobileNumber, code, checksum);
        Lot lot = getPressedLotByCode(code);
        if (lot.getParticipantId() != 0) {
            throw new LotAlreadyRegisteredException();
        }
        lot = checkLockedStatus(lot);
        if (!checksum.equals(lot.getChecksum())) {
            lot = increaseNbTries(lot);
            throw new InvalidChecksumException();
        }
        Participant p = participantLocalService.getOrCreateParticipant(mobileNumber, firstName, lastName, emailAddress,
                contestParticipant, mercedesDriver, carBrand, mercedesOld, language);
        return assignLotToParticipant(lot, p);
    }

    private Lot getPressedLotByCode(String code) throws NoSuchLotException, SystemException {
        Lot lot = lotPersistence.findByPrimaryKey(code);
        if (!lot.isPressed()) {
            throw new NoSuchLotException();
        }
        return lot;
    }

    public Lot registerLot(String mobileNumber, String code, String checksum) throws SystemException, PortalException {
        return registerLot(mobileNumber, null, null, null, false, false, null, false, code, checksum, "nl");
    }

    private Lot checkLockedStatus(Lot lot) throws LotLockoutException, SystemException {
        if (!lot.isLocked()) {
            return lot;
        }
        if (!lot.isLockOverdue()) {
            throw new LotLockoutException(String.valueOf(lot.getLockoutDate()));
        }
        return resetLock(lot);
    }

    private Lot resetLock(Lot lot) throws SystemException {
        lot.setLockoutDate(null);
        lot.setNbTries(0);
        return LotLocalServiceUtil.updateLot(lot);
    }

    private Lot increaseNbTries(Lot lot) throws SystemException {
        int currentNbTries = lot.getNbTries();
        lot.setNbTries(currentNbTries + 1);
        if (lot.getNbTries() == 3) {
            lot.setLockoutDate(new Date());
        }
        return LotLocalServiceUtil.updateLot(lot);
    }

    private void validate(String mobileNumber) throws InvalidMobileNumberException {
        if (!StringUtils.isValidMobileNumber(mobileNumber)) {
            throw new InvalidMobileNumberException();
        }
    }

    private void validate(String mobileNumber, String code, String checksum) throws NoSuchLotException,
            InvalidMobileNumberException, InvalidChecksumException {
        validate(mobileNumber);
        if (StringUtils.isEmpty(code)) {
            throw new NoSuchLotException();
        }
        if (StringUtils.isEmpty(checksum)) {
            throw new InvalidChecksumException();
        }
    }

    public Lot getAvailableLot(boolean pressed) throws SystemException, NoSuchLotException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotImpl.class, PortalClassLoaderUtil.getClassLoader())
                .add(RestrictionsFactoryUtil.or(PropertyFactoryUtil.forName("participantId").isNull(),
                        PropertyFactoryUtil.forName("participantId").eq(new Long(0))))
                .add(RestrictionsFactoryUtil.or(PropertyFactoryUtil.forName("number").isNull(), PropertyFactoryUtil
                        .forName("number").eq(new Integer(0)))).add(PropertyFactoryUtil.forName("pressed").eq(pressed));
        List<?> resultList = dynamicQuery(q, 0, 1);
        if (resultList.isEmpty()) {
            throw new NoSuchLotException();
        }
        return (Lot) resultList.get(0);
    }

    public List<Integer> getLotNumbersForParticipant(long participantId) throws SystemException {
        List<Integer> result = new ArrayList<Integer>();
        for (Lot lot : getLotsForParticipant(participantId)) {
            result.add(lot.getNumber());
        }
        return result;
    }

    public List<Lot> getLotsForParticipant(long participantId) throws SystemException {
        return lotPersistence.findByParticipant(participantId);
    }

    public List<Lot> getLotsForReservation(long reservationId) throws SystemException {
        return lotPersistence.findByReservation(reservationId);
    }

    public void assignFreeLotToParticipant(LotReservation reservation) throws SystemException, PortalException {
        Lot lot = assignLotToParticipant(getAvailableLot(false), reservation.getParticipant());
        lot.setReservationId(reservation.getReservationId());
        LotLocalServiceUtil.updateLot(lot);
    }

    public Lot getLotByNumber(int number) throws NoSuchLotException, SystemException {
        return lotPersistence.findByNumber(number);
    }

    public int getShortSmsLotCount() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil.forClass(LotImpl.class, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("pressed").eq(false))
                .add(PropertyFactoryUtil.forName("participantId").isNotNull())
                .add(PropertyFactoryUtil.forName("participantId").ne(new Long(0)))
                .setProjection(ProjectionFactoryUtil.rowCount());
        int pressedAndRegistered = (Integer) dynamicQuery(q).get(0);
        return pressedAndRegistered - getWebsiteReservationLotCount();
    }

    public int getLongSmsLotCount() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil.forClass(LotImpl.class, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("pressed").eq(true))
                .add(PropertyFactoryUtil.forName("participantId").in(getParticipantIds(true)))
                .setProjection(ProjectionFactoryUtil.rowCount());
        return (Integer) dynamicQuery(q).get(0);
    }

    public int getWebsiteRegistrationLotCount() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotImpl.class, LotImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("pressed").eq(true))
                .add(PropertyFactoryUtil.forName("participantId").in(getParticipantIds(false)))
                .setProjection(ProjectionFactoryUtil.rowCount());
        return (Integer) dynamicQuery(q).get(0);
    }

    public long getRegisteredLotCount() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotImpl.class, LotImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("participantId").isNotNull())
                .add(PropertyFactoryUtil.forName("participantId").ne(0L))
                .setProjection(ProjectionFactoryUtil.rowCount());
        return (Long) dynamicQuery(q).get(0);
    }

    public int getWebsiteReservationLotCount() throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(LotReservationImpl.class, LotReservationImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("approved").eq(true))
                .setProjection(ProjectionFactoryUtil.sum("numberLots"));
        return (Integer) dynamicQuery(q).get(0);
    }

    private DynamicQuery getParticipantIds(boolean nullData) throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil.forClass(ParticipantImpl.class, ParticipantImpl.TABLE_NAME,
                PortalClassLoaderUtil.getClassLoader());
        if (nullData) {
            q.add(RestrictionsFactoryUtil.or(PropertyFactoryUtil.forName("firstName").isNull(), PropertyFactoryUtil
                    .forName("firstName").eq("")));
        } else {
            q.add(RestrictionsFactoryUtil.and(PropertyFactoryUtil.forName("firstName").isNotNull(), PropertyFactoryUtil
                    .forName("firstName").ne("")));
        }
        return q.setProjection(ProjectionFactoryUtil.property("participantId"));
    }

}