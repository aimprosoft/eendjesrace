/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.impl.LotImpl;
import be.lions.duckrace.model.impl.LotModelImpl;
import be.lions.duckrace.service.persistence.LotPersistence;
import com.liferay.portal.kernel.dao.orm.*;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;
import java.util.*;

/**
 * The persistence implementation for the lot service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotPersistence
 * @see be.lions.duckrace.service.persistence.LotUtil
 * @generated
 */
@ProviderType
public class LotPersistenceImpl extends BasePersistenceImpl<Lot>
	implements LotPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LotUtil} to access the lot persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LotImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PARTICIPANT =
		new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByParticipant",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTICIPANT =
		new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByParticipant",
			new String[] { Long.class.getName() },
			LotModelImpl.PARTICIPANTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PARTICIPANT = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByParticipant",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the lots where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @return the matching lots
	 */
	@Override
	public List<Lot> findByParticipant(long participantId) {
		return findByParticipant(participantId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lots where participantId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param participantId the participant ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @return the range of matching lots
	 */
	@Override
	public List<Lot> findByParticipant(long participantId, int start, int end) {
		return findByParticipant(participantId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lots where participantId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param participantId the participant ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByParticipant(long participantId, int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return findByParticipant(participantId, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the lots where participantId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param participantId the participant ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByParticipant(long participantId, int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTICIPANT;
			finderArgs = new Object[] { participantId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PARTICIPANT;
			finderArgs = new Object[] {
					participantId,
					
					start, end, orderByComparator
				};
		}

		List<Lot> list = null;

		if (retrieveFromCache) {
			list = (List<Lot>)finderCache.getResult(finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Lot lot : list) {
					if ((participantId != lot.getParticipantId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LOT_WHERE);

			query.append(_FINDER_COLUMN_PARTICIPANT_PARTICIPANTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LotModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(participantId);

				if (!pagination) {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lot in the ordered set where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByParticipant_First(long participantId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByParticipant_First(participantId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("participantId=");
		msg.append(participantId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the first lot in the ordered set where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByParticipant_First(long participantId,
		OrderByComparator<Lot> orderByComparator) {
		List<Lot> list = findByParticipant(participantId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lot in the ordered set where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByParticipant_Last(long participantId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByParticipant_Last(participantId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("participantId=");
		msg.append(participantId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the last lot in the ordered set where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByParticipant_Last(long participantId,
		OrderByComparator<Lot> orderByComparator) {
		int count = countByParticipant(participantId);

		if (count == 0) {
			return null;
		}

		List<Lot> list = findByParticipant(participantId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lots before and after the current lot in the ordered set where participantId = &#63;.
	 *
	 * @param code the primary key of the current lot
	 * @param participantId the participant ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lot
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot[] findByParticipant_PrevAndNext(String code, long participantId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = findByPrimaryKey(code);

		Session session = null;

		try {
			session = openSession();

			Lot[] array = new LotImpl[3];

			array[0] = getByParticipant_PrevAndNext(session, lot,
					participantId, orderByComparator, true);

			array[1] = lot;

			array[2] = getByParticipant_PrevAndNext(session, lot,
					participantId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Lot getByParticipant_PrevAndNext(Session session, Lot lot,
		long participantId, OrderByComparator<Lot> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LOT_WHERE);

		query.append(_FINDER_COLUMN_PARTICIPANT_PARTICIPANTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LotModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(participantId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lot);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Lot> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lots where participantId = &#63; from the database.
	 *
	 * @param participantId the participant ID
	 */
	@Override
	public void removeByParticipant(long participantId) {
		for (Lot lot : findByParticipant(participantId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(lot);
		}
	}

	/**
	 * Returns the number of lots where participantId = &#63;.
	 *
	 * @param participantId the participant ID
	 * @return the number of matching lots
	 */
	@Override
	public int countByParticipant(long participantId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PARTICIPANT;

		Object[] finderArgs = new Object[] { participantId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOT_WHERE);

			query.append(_FINDER_COLUMN_PARTICIPANT_PARTICIPANTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(participantId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PARTICIPANT_PARTICIPANTID_2 = "lot.participantId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RESERVATION =
		new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByReservation",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESERVATION =
		new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByReservation",
			new String[] { Long.class.getName() },
			LotModelImpl.RESERVATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RESERVATION = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByReservation",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the lots where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @return the matching lots
	 */
	@Override
	public List<Lot> findByReservation(long reservationId) {
		return findByReservation(reservationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lots where reservationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reservationId the reservation ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @return the range of matching lots
	 */
	@Override
	public List<Lot> findByReservation(long reservationId, int start, int end) {
		return findByReservation(reservationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lots where reservationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reservationId the reservation ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByReservation(long reservationId, int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return findByReservation(reservationId, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the lots where reservationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reservationId the reservation ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByReservation(long reservationId, int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESERVATION;
			finderArgs = new Object[] { reservationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RESERVATION;
			finderArgs = new Object[] {
					reservationId,
					
					start, end, orderByComparator
				};
		}

		List<Lot> list = null;

		if (retrieveFromCache) {
			list = (List<Lot>)finderCache.getResult(finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Lot lot : list) {
					if ((reservationId != lot.getReservationId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LOT_WHERE);

			query.append(_FINDER_COLUMN_RESERVATION_RESERVATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LotModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reservationId);

				if (!pagination) {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lot in the ordered set where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByReservation_First(long reservationId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByReservation_First(reservationId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reservationId=");
		msg.append(reservationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the first lot in the ordered set where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByReservation_First(long reservationId,
		OrderByComparator<Lot> orderByComparator) {
		List<Lot> list = findByReservation(reservationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lot in the ordered set where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByReservation_Last(long reservationId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByReservation_Last(reservationId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reservationId=");
		msg.append(reservationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the last lot in the ordered set where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByReservation_Last(long reservationId,
		OrderByComparator<Lot> orderByComparator) {
		int count = countByReservation(reservationId);

		if (count == 0) {
			return null;
		}

		List<Lot> list = findByReservation(reservationId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lots before and after the current lot in the ordered set where reservationId = &#63;.
	 *
	 * @param code the primary key of the current lot
	 * @param reservationId the reservation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lot
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot[] findByReservation_PrevAndNext(String code, long reservationId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = findByPrimaryKey(code);

		Session session = null;

		try {
			session = openSession();

			Lot[] array = new LotImpl[3];

			array[0] = getByReservation_PrevAndNext(session, lot,
					reservationId, orderByComparator, true);

			array[1] = lot;

			array[2] = getByReservation_PrevAndNext(session, lot,
					reservationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Lot getByReservation_PrevAndNext(Session session, Lot lot,
		long reservationId, OrderByComparator<Lot> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LOT_WHERE);

		query.append(_FINDER_COLUMN_RESERVATION_RESERVATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LotModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(reservationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lot);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Lot> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lots where reservationId = &#63; from the database.
	 *
	 * @param reservationId the reservation ID
	 */
	@Override
	public void removeByReservation(long reservationId) {
		for (Lot lot : findByReservation(reservationId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(lot);
		}
	}

	/**
	 * Returns the number of lots where reservationId = &#63;.
	 *
	 * @param reservationId the reservation ID
	 * @return the number of matching lots
	 */
	@Override
	public int countByReservation(long reservationId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RESERVATION;

		Object[] finderArgs = new Object[] { reservationId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOT_WHERE);

			query.append(_FINDER_COLUMN_RESERVATION_RESERVATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reservationId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RESERVATION_RESERVATIONID_2 = "lot.reservationId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_LOTNUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByLotNumber",
			new String[] { Integer.class.getName() },
			LotModelImpl.LOTNUMBER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LOTNUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLotNumber",
			new String[] { Integer.class.getName() });

	/**
	 * Returns the lot where lotNumber = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	 *
	 * @param lotNumber the lot number
	 * @return the matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByLotNumber(int lotNumber) throws NoSuchLotException {
		Lot lot = fetchByLotNumber(lotNumber);

		if (lot == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("lotNumber=");
			msg.append(lotNumber);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchLotException(msg.toString());
		}

		return lot;
	}

	/**
	 * Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param lotNumber the lot number
	 * @return the matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByLotNumber(int lotNumber) {
		return fetchByLotNumber(lotNumber, true);
	}

	/**
	 * Returns the lot where lotNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param lotNumber the lot number
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByLotNumber(int lotNumber, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { lotNumber };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
					finderArgs, this);
		}

		if (result instanceof Lot) {
			Lot lot = (Lot)result;

			if ((lotNumber != lot.getLotNumber())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_LOT_WHERE);

			query.append(_FINDER_COLUMN_LOTNUMBER_LOTNUMBER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lotNumber);

				List<Lot> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"LotPersistenceImpl.fetchByLotNumber(int, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Lot lot = list.get(0);

					result = lot;

					cacheResult(lot);

					if ((lot.getLotNumber() != lotNumber)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
							finderArgs, lot);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Lot)result;
		}
	}

	/**
	 * Removes the lot where lotNumber = &#63; from the database.
	 *
	 * @param lotNumber the lot number
	 * @return the lot that was removed
	 */
	@Override
	public Lot removeByLotNumber(int lotNumber) throws NoSuchLotException {
		Lot lot = findByLotNumber(lotNumber);

		return remove(lot);
	}

	/**
	 * Returns the number of lots where lotNumber = &#63;.
	 *
	 * @param lotNumber the lot number
	 * @return the number of matching lots
	 */
	@Override
	public int countByLotNumber(int lotNumber) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LOTNUMBER;

		Object[] finderArgs = new Object[] { lotNumber };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOT_WHERE);

			query.append(_FINDER_COLUMN_LOTNUMBER_LOTNUMBER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lotNumber);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LOTNUMBER_LOTNUMBER_2 = "lot.lotNumber = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_NUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByNumber",
			new String[] { Integer.class.getName() },
			LotModelImpl.NUMBER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NUMBER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNumber",
			new String[] { Integer.class.getName() });

	/**
	 * Returns the lot where number = &#63; or throws a {@link NoSuchLotException} if it could not be found.
	 *
	 * @param number the number
	 * @return the matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByNumber(int number) throws NoSuchLotException {
		Lot lot = fetchByNumber(number);

		if (lot == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("number=");
			msg.append(number);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchLotException(msg.toString());
		}

		return lot;
	}

	/**
	 * Returns the lot where number = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param number the number
	 * @return the matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByNumber(int number) {
		return fetchByNumber(number, true);
	}

	/**
	 * Returns the lot where number = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param number the number
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByNumber(int number, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { number };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_NUMBER,
					finderArgs, this);
		}

		if (result instanceof Lot) {
			Lot lot = (Lot)result;

			if ((number != lot.getNumber())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_LOT_WHERE);

			query.append(_FINDER_COLUMN_NUMBER_NUMBER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(number);

				List<Lot> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_NUMBER,
						finderArgs, list);
				}
				else {
					Lot lot = list.get(0);

					result = lot;

					cacheResult(lot);

					if ((lot.getNumber() != number)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_NUMBER,
							finderArgs, lot);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_NUMBER, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Lot)result;
		}
	}

	/**
	 * Removes the lot where number = &#63; from the database.
	 *
	 * @param number the number
	 * @return the lot that was removed
	 */
	@Override
	public Lot removeByNumber(int number) throws NoSuchLotException {
		Lot lot = findByNumber(number);

		return remove(lot);
	}

	/**
	 * Returns the number of lots where number = &#63;.
	 *
	 * @param number the number
	 * @return the number of matching lots
	 */
	@Override
	public int countByNumber(int number) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NUMBER;

		Object[] finderArgs = new Object[] { number };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOT_WHERE);

			query.append(_FINDER_COLUMN_NUMBER_NUMBER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(number);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NUMBER_NUMBER_2 = "lot.number = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PARTNER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPartner",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTNER =
		new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, LotImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPartner",
			new String[] { Long.class.getName() },
			LotModelImpl.PARTNERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PARTNER = new FinderPath(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPartner",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the lots where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @return the matching lots
	 */
	@Override
	public List<Lot> findByPartner(long partnerId) {
		return findByPartner(partnerId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the lots where partnerId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param partnerId the partner ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @return the range of matching lots
	 */
	@Override
	public List<Lot> findByPartner(long partnerId, int start, int end) {
		return findByPartner(partnerId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lots where partnerId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param partnerId the partner ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByPartner(long partnerId, int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return findByPartner(partnerId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the lots where partnerId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param partnerId the partner ID
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching lots
	 */
	@Override
	public List<Lot> findByPartner(long partnerId, int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTNER;
			finderArgs = new Object[] { partnerId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PARTNER;
			finderArgs = new Object[] { partnerId, start, end, orderByComparator };
		}

		List<Lot> list = null;

		if (retrieveFromCache) {
			list = (List<Lot>)finderCache.getResult(finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Lot lot : list) {
					if ((partnerId != lot.getPartnerId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LOT_WHERE);

			query.append(_FINDER_COLUMN_PARTNER_PARTNERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LotModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(partnerId);

				if (!pagination) {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lot in the ordered set where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByPartner_First(long partnerId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByPartner_First(partnerId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("partnerId=");
		msg.append(partnerId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the first lot in the ordered set where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByPartner_First(long partnerId,
		OrderByComparator<Lot> orderByComparator) {
		List<Lot> list = findByPartner(partnerId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lot in the ordered set where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot
	 * @throws NoSuchLotException if a matching lot could not be found
	 */
	@Override
	public Lot findByPartner_Last(long partnerId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = fetchByPartner_Last(partnerId, orderByComparator);

		if (lot != null) {
			return lot;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("partnerId=");
		msg.append(partnerId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotException(msg.toString());
	}

	/**
	 * Returns the last lot in the ordered set where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot, or <code>null</code> if a matching lot could not be found
	 */
	@Override
	public Lot fetchByPartner_Last(long partnerId,
		OrderByComparator<Lot> orderByComparator) {
		int count = countByPartner(partnerId);

		if (count == 0) {
			return null;
		}

		List<Lot> list = findByPartner(partnerId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lots before and after the current lot in the ordered set where partnerId = &#63;.
	 *
	 * @param code the primary key of the current lot
	 * @param partnerId the partner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lot
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot[] findByPartner_PrevAndNext(String code, long partnerId,
		OrderByComparator<Lot> orderByComparator) throws NoSuchLotException {
		Lot lot = findByPrimaryKey(code);

		Session session = null;

		try {
			session = openSession();

			Lot[] array = new LotImpl[3];

			array[0] = getByPartner_PrevAndNext(session, lot, partnerId,
					orderByComparator, true);

			array[1] = lot;

			array[2] = getByPartner_PrevAndNext(session, lot, partnerId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Lot getByPartner_PrevAndNext(Session session, Lot lot,
		long partnerId, OrderByComparator<Lot> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LOT_WHERE);

		query.append(_FINDER_COLUMN_PARTNER_PARTNERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LotModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(partnerId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lot);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Lot> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lots where partnerId = &#63; from the database.
	 *
	 * @param partnerId the partner ID
	 */
	@Override
	public void removeByPartner(long partnerId) {
		for (Lot lot : findByPartner(partnerId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(lot);
		}
	}

	/**
	 * Returns the number of lots where partnerId = &#63;.
	 *
	 * @param partnerId the partner ID
	 * @return the number of matching lots
	 */
	@Override
	public int countByPartner(long partnerId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PARTNER;

		Object[] finderArgs = new Object[] { partnerId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOT_WHERE);

			query.append(_FINDER_COLUMN_PARTNER_PARTNERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(partnerId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PARTNER_PARTNERID_2 = "lot.partnerId = ?";

	public LotPersistenceImpl() {
		setModelClass(Lot.class);
	}

	/**
	 * Caches the lot in the entity cache if it is enabled.
	 *
	 * @param lot the lot
	 */
	@Override
	public void cacheResult(Lot lot) {
		entityCache.putResult(LotModelImpl.ENTITY_CACHE_ENABLED, LotImpl.class,
			lot.getPrimaryKey(), lot);

		finderCache.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER,
			new Object[] { lot.getLotNumber() }, lot);

		finderCache.putResult(FINDER_PATH_FETCH_BY_NUMBER,
			new Object[] { lot.getNumber() }, lot);

		lot.resetOriginalValues();
	}

	/**
	 * Caches the lots in the entity cache if it is enabled.
	 *
	 * @param lots the lots
	 */
	@Override
	public void cacheResult(List<Lot> lots) {
		for (Lot lot : lots) {
			if (entityCache.getResult(LotModelImpl.ENTITY_CACHE_ENABLED,
						LotImpl.class, lot.getPrimaryKey()) == null) {
				cacheResult(lot);
			}
			else {
				lot.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lots.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LotImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lot.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Lot lot) {
		entityCache.removeResult(LotModelImpl.ENTITY_CACHE_ENABLED,
			LotImpl.class, lot.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((LotModelImpl)lot, true);
	}

	@Override
	public void clearCache(List<Lot> lots) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Lot lot : lots) {
			entityCache.removeResult(LotModelImpl.ENTITY_CACHE_ENABLED,
				LotImpl.class, lot.getPrimaryKey());

			clearUniqueFindersCache((LotModelImpl)lot, true);
		}
	}

	protected void cacheUniqueFindersCache(LotModelImpl lotModelImpl) {
		Object[] args = new Object[] { lotModelImpl.getLotNumber() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_LOTNUMBER, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_LOTNUMBER, args,
			lotModelImpl, false);

		args = new Object[] { lotModelImpl.getNumber() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_NUMBER, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_NUMBER, args, lotModelImpl,
			false);
	}

	protected void clearUniqueFindersCache(LotModelImpl lotModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { lotModelImpl.getLotNumber() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_LOTNUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_LOTNUMBER, args);
		}

		if ((lotModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_LOTNUMBER.getColumnBitmask()) != 0) {
			Object[] args = new Object[] { lotModelImpl.getOriginalLotNumber() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_LOTNUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_LOTNUMBER, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] { lotModelImpl.getNumber() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_NUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_NUMBER, args);
		}

		if ((lotModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_NUMBER.getColumnBitmask()) != 0) {
			Object[] args = new Object[] { lotModelImpl.getOriginalNumber() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_NUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_NUMBER, args);
		}
	}

	/**
	 * Creates a new lot with the primary key. Does not add the lot to the database.
	 *
	 * @param code the primary key for the new lot
	 * @return the new lot
	 */
	@Override
	public Lot create(String code) {
		Lot lot = new LotImpl();

		lot.setNew(true);
		lot.setPrimaryKey(code);

		return lot;
	}

	/**
	 * Removes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param code the primary key of the lot
	 * @return the lot that was removed
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot remove(String code) throws NoSuchLotException {
		return remove((Serializable)code);
	}

	/**
	 * Removes the lot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lot
	 * @return the lot that was removed
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot remove(Serializable primaryKey) throws NoSuchLotException {
		Session session = null;

		try {
			session = openSession();

			Lot lot = (Lot)session.get(LotImpl.class, primaryKey);

			if (lot == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLotException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(lot);
		}
		catch (NoSuchLotException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Lot removeImpl(Lot lot) {
		lot = toUnwrappedModel(lot);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(lot)) {
				lot = (Lot)session.get(LotImpl.class, lot.getPrimaryKeyObj());
			}

			if (lot != null) {
				session.delete(lot);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (lot != null) {
			clearCache(lot);
		}

		return lot;
	}

	@Override
	public Lot updateImpl(Lot lot) {
		lot = toUnwrappedModel(lot);

		boolean isNew = lot.isNew();

		LotModelImpl lotModelImpl = (LotModelImpl)lot;

		Session session = null;

		try {
			session = openSession();

			if (lot.isNew()) {
				session.save(lot);

				lot.setNew(false);
			}
			else {
				lot = (Lot)session.merge(lot);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LotModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((lotModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTICIPANT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						lotModelImpl.getOriginalParticipantId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PARTICIPANT, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTICIPANT,
					args);

				args = new Object[] { lotModelImpl.getParticipantId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PARTICIPANT, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTICIPANT,
					args);
			}

			if ((lotModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESERVATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						lotModelImpl.getOriginalReservationId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_RESERVATION, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESERVATION,
					args);

				args = new Object[] { lotModelImpl.getReservationId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_RESERVATION, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESERVATION,
					args);
			}

			if ((lotModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTNER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { lotModelImpl.getOriginalPartnerId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PARTNER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTNER,
					args);

				args = new Object[] { lotModelImpl.getPartnerId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PARTNER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PARTNER,
					args);
			}
		}

		entityCache.putResult(LotModelImpl.ENTITY_CACHE_ENABLED, LotImpl.class,
			lot.getPrimaryKey(), lot, false);

		clearUniqueFindersCache(lotModelImpl, false);
		cacheUniqueFindersCache(lotModelImpl);

		lot.resetOriginalValues();

		return lot;
	}

	protected Lot toUnwrappedModel(Lot lot) {
		if (lot instanceof LotImpl) {
			return lot;
		}

		LotImpl lotImpl = new LotImpl();

		lotImpl.setNew(lot.isNew());
		lotImpl.setPrimaryKey(lot.getPrimaryKey());

		lotImpl.setCode(lot.getCode());
		lotImpl.setChecksum(lot.getChecksum());
		lotImpl.setLotNumber(lot.getLotNumber());
		lotImpl.setNumber(lot.getNumber());
		lotImpl.setPressed(lot.isPressed());
		lotImpl.setNbTries(lot.getNbTries());
		lotImpl.setLockoutDate(lot.getLockoutDate());
		lotImpl.setParticipantId(lot.getParticipantId());
		lotImpl.setReservationId(lot.getReservationId());
		lotImpl.setPartnerId(lot.getPartnerId());
		lotImpl.setOrderNumber(lot.getOrderNumber());

		return lotImpl;
	}

	/**
	 * Returns the lot with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lot
	 * @return the lot
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLotException {
		Lot lot = fetchByPrimaryKey(primaryKey);

		if (lot == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLotException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return lot;
	}

	/**
	 * Returns the lot with the primary key or throws a {@link NoSuchLotException} if it could not be found.
	 *
	 * @param code the primary key of the lot
	 * @return the lot
	 * @throws NoSuchLotException if a lot with the primary key could not be found
	 */
	@Override
	public Lot findByPrimaryKey(String code) throws NoSuchLotException {
		return findByPrimaryKey((Serializable)code);
	}

	/**
	 * Returns the lot with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lot
	 * @return the lot, or <code>null</code> if a lot with the primary key could not be found
	 */
	@Override
	public Lot fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LotModelImpl.ENTITY_CACHE_ENABLED,
				LotImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Lot lot = (Lot)serializable;

		if (lot == null) {
			Session session = null;

			try {
				session = openSession();

				lot = (Lot)session.get(LotImpl.class, primaryKey);

				if (lot != null) {
					cacheResult(lot);
				}
				else {
					entityCache.putResult(LotModelImpl.ENTITY_CACHE_ENABLED,
						LotImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LotModelImpl.ENTITY_CACHE_ENABLED,
					LotImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return lot;
	}

	/**
	 * Returns the lot with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param code the primary key of the lot
	 * @return the lot, or <code>null</code> if a lot with the primary key could not be found
	 */
	@Override
	public Lot fetchByPrimaryKey(String code) {
		return fetchByPrimaryKey((Serializable)code);
	}

	@Override
	public Map<Serializable, Lot> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Lot> map = new HashMap<Serializable, Lot>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Lot lot = fetchByPrimaryKey(primaryKey);

			if (lot != null) {
				map.put(primaryKey, lot);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LotModelImpl.ENTITY_CACHE_ENABLED,
					LotImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Lot)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_LOT_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Lot lot : (List<Lot>)q.list()) {
				map.put(lot.getPrimaryKeyObj(), lot);

				cacheResult(lot);

				uncachedPrimaryKeys.remove(lot.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LotModelImpl.ENTITY_CACHE_ENABLED,
					LotImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the lots.
	 *
	 * @return the lots
	 */
	@Override
	public List<Lot> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @return the range of lots
	 */
	@Override
	public List<Lot> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lots
	 */
	@Override
	public List<Lot> findAll(int start, int end,
		OrderByComparator<Lot> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the lots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lots
	 * @param end the upper bound of the range of lots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of lots
	 */
	@Override
	public List<Lot> findAll(int start, int end,
		OrderByComparator<Lot> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Lot> list = null;

		if (retrieveFromCache) {
			list = (List<Lot>)finderCache.getResult(finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LOT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LOT;

				if (pagination) {
					sql = sql.concat(LotModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Lot>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lots from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Lot lot : findAll()) {
			remove(lot);
		}
	}

	/**
	 * Returns the number of lots.
	 *
	 * @return the number of lots
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LOT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LotModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the lot persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LotImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LOT = "SELECT lot FROM Lot lot";
	private static final String _SQL_SELECT_LOT_WHERE_PKS_IN = "SELECT lot FROM Lot lot WHERE code_ IN (";
	private static final String _SQL_SELECT_LOT_WHERE = "SELECT lot FROM Lot lot WHERE ";
	private static final String _SQL_COUNT_LOT = "SELECT COUNT(lot) FROM Lot lot";
	private static final String _SQL_COUNT_LOT_WHERE = "SELECT COUNT(lot) FROM Lot lot WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "lot.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Lot exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Lot exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(LotPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"code", "number"
			});
}