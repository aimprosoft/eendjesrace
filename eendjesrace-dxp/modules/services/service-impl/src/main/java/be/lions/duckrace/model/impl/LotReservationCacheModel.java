/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.LotReservation;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing LotReservation in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see LotReservation
 * @generated
 */
@ProviderType
public class LotReservationCacheModel implements CacheModel<LotReservation>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LotReservationCacheModel)) {
			return false;
		}

		LotReservationCacheModel lotReservationCacheModel = (LotReservationCacheModel)obj;

		if (reservationId == lotReservationCacheModel.reservationId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, reservationId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{reservationId=");
		sb.append(reservationId);
		sb.append(", participantId=");
		sb.append(participantId);
		sb.append(", numberLots=");
		sb.append(numberLots);
		sb.append(", approved=");
		sb.append(approved);
		sb.append(", reservationDate=");
		sb.append(reservationDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LotReservation toEntityModel() {
		LotReservationImpl lotReservationImpl = new LotReservationImpl();

		lotReservationImpl.setReservationId(reservationId);
		lotReservationImpl.setParticipantId(participantId);
		lotReservationImpl.setNumberLots(numberLots);
		lotReservationImpl.setApproved(approved);

		if (reservationDate == Long.MIN_VALUE) {
			lotReservationImpl.setReservationDate(null);
		}
		else {
			lotReservationImpl.setReservationDate(new Date(reservationDate));
		}

		lotReservationImpl.resetOriginalValues();

		return lotReservationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		reservationId = objectInput.readLong();

		participantId = objectInput.readLong();

		numberLots = objectInput.readInt();

		approved = objectInput.readBoolean();
		reservationDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(reservationId);

		objectOutput.writeLong(participantId);

		objectOutput.writeInt(numberLots);

		objectOutput.writeBoolean(approved);
		objectOutput.writeLong(reservationDate);
	}

	public long reservationId;
	public long participantId;
	public int numberLots;
	public boolean approved;
	public long reservationDate;
}