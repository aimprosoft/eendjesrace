/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.base.DuckRaceLocalServiceBaseImpl;
import be.lions.duckrace.types.DuckRaceFolder;
import be.lions.duckrace.types.DuckRaceRole;
import be.lions.duckrace.types.DuckRaceTag;
import be.lions.duckrace.util_service_api.DateUtils;
import be.lions.duckrace.util_service_api.ListUtils;
import be.lions.duckrace.util_service_api.PropKeys;
import com.liferay.asset.kernel.service.AssetTagLocalServiceUtil;

import com.liferay.document.library.kernel.model.DLFileEntryModel;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.*;
import com.liferay.portal.kernel.util.*;
import com.liferay.portlet.documentlibrary.model.impl.DLFileEntryModelImpl;
import com.liferay.portlet.documentlibrary.model.impl.DLFolderModelImpl;


import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The implementation of the duck race local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.service.DuckRaceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DuckRaceLocalServiceBaseImpl
 * @see be.lions.duckrace.service.DuckRaceLocalServiceUtil
 */
@ProviderType
public class DuckRaceLocalServiceImpl extends DuckRaceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link be.lions.duckrace.service.DuckRaceLocalServiceUtil} to access the duck race local service.
	 */

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public long getCompanyId() throws SystemException {
        return getCompany().getCompanyId();
    }

    public Company getCompany() throws SystemException {
        return CompanyLocalServiceUtil.getCompanies().get(0);
    }

    public String getHostName() throws PortalException, SystemException {
        return LayoutSetLocalServiceUtil.getLayoutSet(getGroupId(), false).getVirtualHostname();
    }

    public String getSiteTitle() throws SystemException {
        return getProperty(PropKeys.SITE_TITLE);
    }

    public long getAdminId() throws PortalException, SystemException {
        return UserLocalServiceUtil.getUserIdByEmailAddress(getCompanyId(), "test@liferay.com");
    }

    public long getGroupId() throws PortalException, SystemException {
        return GroupLocalServiceUtil.getGroup(getCompanyId(), GroupConstants.GUEST).getGroupId();
    }

    public List<String> getTagsForArticle(String articleId) throws PortalException, SystemException {
        long classPK = JournalArticleLocalServiceUtil.getArticle(getGroupId(), articleId).getResourcePrimKey();
        return ListUtil.toList(AssetTagLocalServiceUtil.getTagNames(JournalArticle.class.getName(), classPK));
    }

    public int getNbStars(String articleId) throws PortalException, SystemException {
        List<String> tags = getTagsForArticle(articleId);
        if (tags.isEmpty()) {
            return 0;
        }
        return DuckRaceTag.getStarsForTag(tags.get(0));
    }


    public void sendMail(String content, Map<String, Object> params, String subject, Participant to) throws Exception {
        MailMessage email = new MailMessage();
        addReceiversPart(email, null, to);
        addFromPart(email);
        addReplyToPart(email);
        addBccMailPart(email);
        addSubjectAndBody(email, subject, content, params);
        MailServiceUtil.sendEmail(email);
    }

    public void sendMail(User registerer, String content, Map<String, Object> params, String subject, Participant to)
            throws Exception {
        sendMail(registerer, content, params, subject, to, new ArrayList<File>());
    }

    public void sendMail(User registerer, String content, Map<String, Object> params, String subject, Participant to,
                         List<File> attachments) throws Exception {
        MailMessage email = new MailMessage();
        addReceiversPart(email, registerer, to);
        addFromPart(email);
        addReplyToPart(email);
        addBccMailPart(email);
        addSubjectAndBody(email, subject, content, params);
        addAttachments(email, attachments);
        MailServiceUtil.sendEmail(email);
    }

    private void addFromPart(MailMessage email) throws UnsupportedEncodingException {
        email.setFrom(new InternetAddress(getProperty(PropKeys.MAIL_FROM_ADDRESS), getProperty(PropKeys.SITE_TITLE)));
    }

    private void addReceiversPart(MailMessage email, User registerer, Participant to)
            throws UnsupportedEncodingException {
        if (registerer == null || registerer.isDefaultUser()) {
            email.setTo(new InternetAddress(to.getEmailAddress(), to.getFullName()));
        } else {
            email.setTo(new InternetAddress(registerer.getEmailAddress(), registerer.getFullName()));
            email.setCC(new InternetAddress(to.getEmailAddress(), to.getFullName()));
        }
    }

    private void addReplyToPart(MailMessage email) throws UnsupportedEncodingException {
        email.setReplyTo(new InternetAddress[] { new InternetAddress(getProperty(PropKeys.MAIL_REPLY_TO_ADDRESS),
                getProperty(PropKeys.SITE_TITLE)) });
    }

    private void addBccMailPart(MailMessage email) throws AddressException {
        String[] bccAddressesStr = getProperties(PropKeys.MAIL_BCC_ADDRESSES);
        int nbBccAddresses = bccAddressesStr.length;
        InternetAddress[] bccAddresses = new InternetAddress[nbBccAddresses];
        for (int i = 0; i < nbBccAddresses; i++) {
            bccAddresses[i] = new InternetAddress(bccAddressesStr[i]);
        }
        email.setBCC(bccAddresses);
    }

    private void addSubjectAndBody(MailMessage email, String subject, String content, Map<String, Object> params)
            throws Exception {
        //TODO VelocityUtil
//        String body = VelocityUtil.evaluate(content, params);
        email.setSubject(subject);
//        email.setBody(body);
        email.setHTMLFormat(true);
    }

    private void addAttachments(MailMessage email, List<File> attachments) {
        for (File file : attachments) {
            email.addFileAttachment(file);
        }
    }

    public String getProperty(String key) {
        try {
            return PropsUtil.get(key);
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public String[] getProperties(String key) {
        try {
            return PropsUtil.getArray(key);
        } catch (Exception e) {
            return new String[0];
        }
    }

    public String getAccountNoIban() {
        return getProperty(PropKeys.ACCOUNT_NO_IBAN);
    }

    public String getAccountNoBelgium() {
        return getProperty(PropKeys.ACCOUNT_NO_BE);
    }

    public String getAccountNoSwift() {
        return getProperty(PropKeys.ACCOUNT_NO_SWIFT);
    }

    public float getLotPrice() {
        return GetterUtil.getFloat(getProperty(PropKeys.LOT_PRICE));
    }

    @SuppressWarnings("unchecked")
 public List<DLFolder> getGalleryFolders() throws PortalException, SystemException {
        DLFolder rootFolder = DLFolderLocalServiceUtil.getFolder(getGroupId(), 0, DuckRaceFolder.ALBUM.getName());
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(DLFolder.class, DLFolderModelImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("parentFolderId").eq(rootFolder.getFolderId()))
                .addOrder(OrderFactoryUtil.desc("createDate"));
        return (List<DLFolder>) ((List<?>) LotLocalServiceUtil.dynamicQuery(q));
    }

    @SuppressWarnings("unchecked")
    public List<DLFileEntryModel> getGalleryImages(DLFolder folder) throws SystemException {
        DynamicQuery q = DynamicQueryFactoryUtil
                .forClass(DLFileEntryModel.class, DLFileEntryModelImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader())
                .add(PropertyFactoryUtil.forName("folderId").eq(folder.getFolderId()))
                .addOrder(OrderFactoryUtil.desc("createDate"));
        return (List<DLFileEntryModel>) ((List<?>) LotLocalServiceUtil.dynamicQuery(q));
    }


    public boolean isLionsMember(long userId) throws SystemException, PortalException {
        return RoleLocalServiceUtil.hasUserRole(userId, getRoleId(DuckRaceRole.LIONS_MEMBER.getName()));
    }

    public boolean isAdministrator(long userId) throws SystemException, PortalException {
        return RoleLocalServiceUtil.hasUserRole(userId, getRoleId(RoleConstants.ADMINISTRATOR));
    }

    public boolean isWebmaster(long userId) throws SystemException, PortalException {
        return RoleLocalServiceUtil.hasUserRole(userId, getRoleId(DuckRaceRole.WEBMASTER.getName()));
    }

    public long getRoleId(String roleName) throws PortalException, SystemException {
        return RoleLocalServiceUtil.getRole(getCompanyId(), roleName).getRoleId();
    }

    private Date getRegistrationDeadline(long userId) throws ParseException, SystemException, PortalException {
        return isLionsMember(userId) ? getDate(PropKeys.ADMIN_REGISTRATION_DEADLINE)
                : getDate(PropKeys.REGISTRATION_DEADLINE);
    }

    private Date getReservationDeadline(long userId) throws ParseException, SystemException, PortalException {
        return isLionsMember(userId) ? getDate(PropKeys.ADMIN_RESERVATION_DEADLINE)
                : getDate(PropKeys.RESERVATION_DEADLINE);
    }

    public Date getAdminRegistrationDeadline() throws ParseException {
        return getDate(PropKeys.ADMIN_REGISTRATION_DEADLINE);
    }

    public Date getAdminReservationDeadline() throws ParseException {
        return getDate(PropKeys.ADMIN_RESERVATION_DEADLINE);
    }

    private Date getDate(String propertyKey) throws ParseException {
        return DATE_FORMAT.parse(getProperty(propertyKey));
    }

    public boolean isRegistrationPossible(long userId, Date date) throws ParseException, SystemException,
            PortalException {
        return DateUtils.before(date, getRegistrationDeadline(userId));
    }

    public boolean isReservationPossible(long userId, Date date) throws ParseException, SystemException,
            PortalException {
        return DateUtils.before(date, getReservationDeadline(userId));
    }

    @SuppressWarnings("unchecked")
    public List reverseList(List input) {
        return ListUtils.reverse(input);
    }

}