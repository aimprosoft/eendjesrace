/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.model.LotRange;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.model.impl.PartnerImpl;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.PartnerLocalServiceUtil;
import be.lions.duckrace.service.base.PartnerLocalServiceBaseImpl;
import be.lions.duckrace.service.persistence.PartnerUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import java.util.List;

/**
 * The implementation of the partner local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.service.PartnerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PartnerLocalServiceBaseImpl
 * @see be.lions.duckrace.service.PartnerLocalServiceUtil
 */
@ProviderType
public class PartnerLocalServiceImpl extends PartnerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link be.lions.duckrace.service.PartnerLocalServiceUtil} to access the partner local service.
	 */

    public Partner createPartner() throws SystemException {
        long id = CounterLocalServiceUtil.increment();
        return createPartner(id);
    }

    public List<Partner> getPartners(String category) throws SystemException {
        return PartnerUtil.findByCategory(category);
    }

    public Partner save(Partner partner, List<LotRange> lotRanges) throws SystemException, PortalException {
        partner = updatePartner(partner);
        LotLocalServiceUtil.assignLotsToPartner(partner, lotRanges);
        return partner;
    }

    public List<Partner> getPartners() throws SystemException {
        return PartnerUtil.findAll();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<String> getCategories() throws SystemException {
        DynamicQuery dq = DynamicQueryFactoryUtil.forClass(PartnerImpl.class, PartnerImpl.TABLE_NAME, PortalClassLoaderUtil.getClassLoader());
        dq.addOrder(OrderFactoryUtil.asc("category"));
        dq.setProjection(ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("category")));
        return (List) PartnerLocalServiceUtil.dynamicQuery(dq);
    }

}