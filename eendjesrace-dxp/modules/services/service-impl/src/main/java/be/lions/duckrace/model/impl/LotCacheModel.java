/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Lot;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Lot in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Lot
 * @generated
 */
@ProviderType
public class LotCacheModel implements CacheModel<Lot>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LotCacheModel)) {
			return false;
		}

		LotCacheModel lotCacheModel = (LotCacheModel)obj;

		if (code.equals(lotCacheModel.code)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, code);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{code=");
		sb.append(code);
		sb.append(", checksum=");
		sb.append(checksum);
		sb.append(", lotNumber=");
		sb.append(lotNumber);
		sb.append(", number=");
		sb.append(number);
		sb.append(", pressed=");
		sb.append(pressed);
		sb.append(", nbTries=");
		sb.append(nbTries);
		sb.append(", lockoutDate=");
		sb.append(lockoutDate);
		sb.append(", participantId=");
		sb.append(participantId);
		sb.append(", reservationId=");
		sb.append(reservationId);
		sb.append(", partnerId=");
		sb.append(partnerId);
		sb.append(", orderNumber=");
		sb.append(orderNumber);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Lot toEntityModel() {
		LotImpl lotImpl = new LotImpl();

		if (code == null) {
			lotImpl.setCode(StringPool.BLANK);
		}
		else {
			lotImpl.setCode(code);
		}

		if (checksum == null) {
			lotImpl.setChecksum(StringPool.BLANK);
		}
		else {
			lotImpl.setChecksum(checksum);
		}

		lotImpl.setLotNumber(lotNumber);
		lotImpl.setNumber(number);
		lotImpl.setPressed(pressed);
		lotImpl.setNbTries(nbTries);

		if (lockoutDate == Long.MIN_VALUE) {
			lotImpl.setLockoutDate(null);
		}
		else {
			lotImpl.setLockoutDate(new Date(lockoutDate));
		}

		lotImpl.setParticipantId(participantId);
		lotImpl.setReservationId(reservationId);
		lotImpl.setPartnerId(partnerId);

		if (orderNumber == null) {
			lotImpl.setOrderNumber(StringPool.BLANK);
		}
		else {
			lotImpl.setOrderNumber(orderNumber);
		}

		lotImpl.resetOriginalValues();

		return lotImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		code = objectInput.readUTF();
		checksum = objectInput.readUTF();

		lotNumber = objectInput.readInt();

		number = objectInput.readInt();

		pressed = objectInput.readBoolean();

		nbTries = objectInput.readInt();
		lockoutDate = objectInput.readLong();

		participantId = objectInput.readLong();

		reservationId = objectInput.readLong();

		partnerId = objectInput.readLong();
		orderNumber = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (code == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(code);
		}

		if (checksum == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(checksum);
		}

		objectOutput.writeInt(lotNumber);

		objectOutput.writeInt(number);

		objectOutput.writeBoolean(pressed);

		objectOutput.writeInt(nbTries);
		objectOutput.writeLong(lockoutDate);

		objectOutput.writeLong(participantId);

		objectOutput.writeLong(reservationId);

		objectOutput.writeLong(partnerId);

		if (orderNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(orderNumber);
		}
	}

	public String code;
	public String checksum;
	public int lotNumber;
	public int number;
	public boolean pressed;
	public int nbTries;
	public long lockoutDate;
	public long participantId;
	public long reservationId;
	public long partnerId;
	public String orderNumber;
}