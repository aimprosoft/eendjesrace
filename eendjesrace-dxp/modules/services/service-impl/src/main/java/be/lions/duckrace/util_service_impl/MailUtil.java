package be.lions.duckrace.util_service_impl;

import com.liferay.portal.kernel.util.StringUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MailUtil {

	public static String getTplContent(String fileName, String language) throws IOException {
		return StringUtil.read(MailUtil.class.getClassLoader(), "be/lions/duckrace/mailtemplates/"+fileName+"_"+language+".tpl");
	}
	
	public static Map<String, Object> createParamMap(String key, Object value) {
		return createParamMap(new String[] {key}, new Object[] {value});
	}
	
	public static Map<String, Object> createParamMap(String[] keys, Object[] values) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (int i=0; i<keys.length; i++) {
			result.put(keys[i], values[i]);
		}
		return result;
	}
	
}
