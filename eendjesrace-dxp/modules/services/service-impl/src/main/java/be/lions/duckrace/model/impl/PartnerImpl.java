/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.service.LotLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

import java.util.List;

/**
 * The extended model implementation for the Partner service. Represents a row in the &quot;DuckRace_Partner&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.model.Partner} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class PartnerImpl extends PartnerBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a partner model instance should use the {@link be.lions.duckrace.model.Partner} interface instead.
	 */
	public PartnerImpl() {
	}

	public int getAssignedLotsCount() throws SystemException {
		return LotLocalServiceUtil.getLotsForPartnerCount(getPartnerId());
	}

	public int getRegisteredLotsCount() throws SystemException {
		return LotLocalServiceUtil.getRegisteredLotsForPartnerCount(getPartnerId());
	}

	public List<Lot> getAssignedLots() throws SystemException {
		return LotLocalServiceUtil.getLotsForPartner(getPartnerId());
	}
}