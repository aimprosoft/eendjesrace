/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.model.Participant;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Participant in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Participant
 * @generated
 */
@ProviderType
public class ParticipantCacheModel implements CacheModel<Participant>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParticipantCacheModel)) {
			return false;
		}

		ParticipantCacheModel participantCacheModel = (ParticipantCacheModel)obj;

		if (participantId == participantCacheModel.participantId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, participantId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{participantId=");
		sb.append(participantId);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", mobileNumber=");
		sb.append(mobileNumber);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", contestParticipant=");
		sb.append(contestParticipant);
		sb.append(", mercedesDriver=");
		sb.append(mercedesDriver);
		sb.append(", carBrand=");
		sb.append(carBrand);
		sb.append(", mercedesOld=");
		sb.append(mercedesOld);
		sb.append(", language=");
		sb.append(language);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Participant toEntityModel() {
		ParticipantImpl participantImpl = new ParticipantImpl();

		participantImpl.setParticipantId(participantId);

		if (emailAddress == null) {
			participantImpl.setEmailAddress(StringPool.BLANK);
		}
		else {
			participantImpl.setEmailAddress(emailAddress);
		}

		if (mobileNumber == null) {
			participantImpl.setMobileNumber(StringPool.BLANK);
		}
		else {
			participantImpl.setMobileNumber(mobileNumber);
		}

		if (firstName == null) {
			participantImpl.setFirstName(StringPool.BLANK);
		}
		else {
			participantImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			participantImpl.setLastName(StringPool.BLANK);
		}
		else {
			participantImpl.setLastName(lastName);
		}

		participantImpl.setContestParticipant(contestParticipant);
		participantImpl.setMercedesDriver(mercedesDriver);

		if (carBrand == null) {
			participantImpl.setCarBrand(StringPool.BLANK);
		}
		else {
			participantImpl.setCarBrand(carBrand);
		}

		participantImpl.setMercedesOld(mercedesOld);

		if (language == null) {
			participantImpl.setLanguage(StringPool.BLANK);
		}
		else {
			participantImpl.setLanguage(language);
		}

		participantImpl.resetOriginalValues();

		return participantImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		participantId = objectInput.readLong();
		emailAddress = objectInput.readUTF();
		mobileNumber = objectInput.readUTF();
		firstName = objectInput.readUTF();
		lastName = objectInput.readUTF();

		contestParticipant = objectInput.readBoolean();

		mercedesDriver = objectInput.readBoolean();
		carBrand = objectInput.readUTF();

		mercedesOld = objectInput.readBoolean();
		language = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(participantId);

		if (emailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		if (mobileNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mobileNumber);
		}

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		objectOutput.writeBoolean(contestParticipant);

		objectOutput.writeBoolean(mercedesDriver);

		if (carBrand == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(carBrand);
		}

		objectOutput.writeBoolean(mercedesOld);

		if (language == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(language);
		}
	}

	public long participantId;
	public String emailAddress;
	public String mobileNumber;
	public String firstName;
	public String lastName;
	public boolean contestParticipant;
	public boolean mercedesDriver;
	public String carBrand;
	public boolean mercedesOld;
	public String language;
}