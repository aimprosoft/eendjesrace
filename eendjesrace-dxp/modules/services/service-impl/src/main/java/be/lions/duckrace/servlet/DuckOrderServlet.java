package be.lions.duckrace.servlet;

public class DuckOrderServlet /*extends HttpServlet*/ {
//
//	private static final String PARAM_NAME = "name";
//	private static final String PARAM_FIRST_NAME = "firstName";
//	private static final String PARAM_CELL_PHONE = "cellPhone";
//	private static final String PARAM_EMAIL = "email";
//	private static final String PARAM_DUCK_COUNT = "duckCount";
//	private static final String PARAM_ORDER_NUMBER = "orderNumber";
//	private static final String PARAM_LANGUAGE = "language";
//	private static final String PARAM_LOTS = "lots";
//	private static final String PARAM_TITLE = "title";
//	private static final String PARAM_FULL_NAME = "fullName";
//	private static final String PARAM_URL = "url";
//	private static final String DEFAULT_LANGUAGE = "nl";
//	private static final String KEY_PURCHASE = "purchase";
//	private static final String TEMPLATE_NAME = "behappy2";
//
//	@Override
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		try {
//			if (!DuckOrderUtil.isValidParameterMap(request.getParameterMap(), response)) {
//				return;
//			}
//
//			String[] trustedHosts = PropsUtil.getArray(PropKeys.BEHAPPY2_TRUSTED_HOSTS);
//			if (!DuckOrderUtil.isTrustedHost(trustedHosts, request, response)) {
//				return;
//			}
//
//			String orderCodes = doRegistration(request);
//
//			response.getWriter().println(orderCodes);
//			response.setStatus(HttpServletResponse.SC_OK);
//		} catch (Exception e) {
//			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
//		}
//	}
//
//	private String doRegistration(HttpServletRequest request) throws Exception {
//		String cellPhone = ParamUtil.getString(request, PARAM_CELL_PHONE);
//		String firstName = ParamUtil.getString(request, PARAM_FIRST_NAME);
//		String lastName = ParamUtil.getString(request, PARAM_NAME);
//		String email = ParamUtil.getString(request, PARAM_EMAIL);
//		String orderNumber = ParamUtil.getString(request, PARAM_ORDER_NUMBER);
//		String language = ParamUtil.getString(request, PARAM_LANGUAGE, DEFAULT_LANGUAGE);
//		int duckCount = ParamUtil.getInteger(request, PARAM_DUCK_COUNT);
//		String urlPrefix = request.getScheme() + "://" + request.getServerName() + ':' + Integer.toString(request.getServerPort());
//
//		List<Lot> lots = new ArrayList<Lot>();
//		for (int i = 0; i < duckCount; ++i) {
//			Lot lot = LotLocalServiceUtil.registerLot(cellPhone, firstName, lastName, email, orderNumber, language);
//			lots.add(lot);
//		}
//
//		sendRegistrationEmail(lots, urlPrefix);
//
//		return generateResponseMessage(lots);
//	}
//
//	private void sendRegistrationEmail(List<Lot> lots, String urlPrefix) throws Exception {
//		if (lots.isEmpty()) {
//			return;
//		}
//
//		Participant to = lots.get(0).getParticipant();
//		String subject = MessageUtil.getMessage(to.getLanguage(), KEY_PURCHASE) + " " + DuckRaceLocalServiceUtil.getSiteTitle();
//		String content = MailUtil.getTplContent(TEMPLATE_NAME, to.getLanguage());
//
//		Map<String, Object> params = MailUtil.createParamMap(
//				new String[] { PARAM_LOTS, PARAM_TITLE, PARAM_FULL_NAME, PARAM_URL },
//				new Object[] { lots, DuckRaceLocalServiceUtil.getSiteTitle(), to.getFullName(), urlPrefix });
//
//		DuckRaceLocalServiceUtil.sendMail(content, params, subject, to);
//	}
//
//	private String generateResponseMessage(List<Lot> lots) {
//		StringBuilder builder = new StringBuilder();
//
//		for (int i = 0; i < lots.size(); ++i) {
//			if (i != 0) {
//				builder.append(',');
//			}
//			builder.append(lots.get(i).getNumber());
//		}
//
//		return builder.toString();
//	}
}
