/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.*;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.base.LotReservationLocalServiceBaseImpl;
import com.liferay.counter.kernel.service.persistence.CounterFinderUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the lot reservation local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.service.LotReservationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationLocalServiceBaseImpl
 * @see be.lions.duckrace.service.LotReservationLocalServiceUtil
 */
@ProviderType
public class LotReservationLocalServiceImpl
	extends LotReservationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link be.lions.duckrace.service.LotReservationLocalServiceUtil} to access the lot reservation local service.
	 */

	public LotReservation createReservation(String mobileNumber, String firstName, String lastName, String emailAddress, boolean contestParticipant, boolean mercedesDriver, String carBrand, boolean mercedesOld, int numberLots, String language) throws SystemException, InvalidMobileNumberException, NoSuchParticipantException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
		Participant participant = participantLocalService.getOrCreateParticipant(mobileNumber, firstName, lastName, emailAddress, contestParticipant, mercedesDriver, carBrand, mercedesOld, language);

		long id = CounterFinderUtil.increment();
		LotReservation reservation = lotReservationPersistence.create(id);
		reservation.setParticipantId(participant.getParticipantId());
		reservation.setNumberLots(numberLots);
		reservation.setApproved(false);
		reservation.setReservationDate(new Date());
		return lotReservationPersistence.update(reservation);
	}

	public LotReservation approveReservation(long reservationId) throws SystemException, InvalidMobileNumberException, NoSuchLotException, NoSuchParticipantException, PortalException {
		LotReservation reservation = lotReservationPersistence.findByPrimaryKey(reservationId);
		if(reservation.isApproved()) {
			return reservation;
		}
		for (int i=0; i<reservation.getNumberLots(); i++) {
			lotLocalService.assignFreeLotToParticipant(reservation);
		}
		reservation.setApproved(true);
		return lotReservationPersistence.update(reservation);
	}

	public List<LotReservation> getUnapprovedReservations() throws SystemException {
		return lotReservationPersistence.findByApproved(false);
	}

	public List<LotReservation> getApprovedReservations() throws SystemException {
		return lotReservationPersistence.findByApproved(true);
	}


}