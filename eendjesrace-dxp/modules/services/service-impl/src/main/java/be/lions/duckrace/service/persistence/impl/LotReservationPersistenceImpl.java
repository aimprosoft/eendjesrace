/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchLotReservationException;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.model.impl.LotReservationImpl;
import be.lions.duckrace.model.impl.LotReservationModelImpl;
import be.lions.duckrace.service.persistence.LotReservationPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the lot reservation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LotReservationPersistence
 * @see be.lions.duckrace.service.persistence.LotReservationUtil
 * @generated
 */
@ProviderType
public class LotReservationPersistenceImpl extends BasePersistenceImpl<LotReservation>
	implements LotReservationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LotReservationUtil} to access the lot reservation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LotReservationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED,
			LotReservationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED,
			LotReservationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_APPROVED = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED,
			LotReservationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByApproved",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPROVED =
		new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED,
			LotReservationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByApproved",
			new String[] { Boolean.class.getName() },
			LotReservationModelImpl.APPROVED_COLUMN_BITMASK |
			LotReservationModelImpl.RESERVATIONDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_APPROVED = new FinderPath(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByApproved",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the lot reservations where approved = &#63;.
	 *
	 * @param approved the approved
	 * @return the matching lot reservations
	 */
	@Override
	public List<LotReservation> findByApproved(boolean approved) {
		return findByApproved(approved, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the lot reservations where approved = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param approved the approved
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @return the range of matching lot reservations
	 */
	@Override
	public List<LotReservation> findByApproved(boolean approved, int start,
		int end) {
		return findByApproved(approved, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lot reservations where approved = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param approved the approved
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lot reservations
	 */
	@Override
	public List<LotReservation> findByApproved(boolean approved, int start,
		int end, OrderByComparator<LotReservation> orderByComparator) {
		return findByApproved(approved, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the lot reservations where approved = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param approved the approved
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching lot reservations
	 */
	@Override
	public List<LotReservation> findByApproved(boolean approved, int start,
		int end, OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPROVED;
			finderArgs = new Object[] { approved };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_APPROVED;
			finderArgs = new Object[] { approved, start, end, orderByComparator };
		}

		List<LotReservation> list = null;

		if (retrieveFromCache) {
			list = (List<LotReservation>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LotReservation lotReservation : list) {
					if ((approved != lotReservation.getApproved())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LOTRESERVATION_WHERE);

			query.append(_FINDER_COLUMN_APPROVED_APPROVED_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LotReservationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(approved);

				if (!pagination) {
					list = (List<LotReservation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LotReservation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lot reservation in the ordered set where approved = &#63;.
	 *
	 * @param approved the approved
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot reservation
	 * @throws NoSuchLotReservationException if a matching lot reservation could not be found
	 */
	@Override
	public LotReservation findByApproved_First(boolean approved,
		OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException {
		LotReservation lotReservation = fetchByApproved_First(approved,
				orderByComparator);

		if (lotReservation != null) {
			return lotReservation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("approved=");
		msg.append(approved);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotReservationException(msg.toString());
	}

	/**
	 * Returns the first lot reservation in the ordered set where approved = &#63;.
	 *
	 * @param approved the approved
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	 */
	@Override
	public LotReservation fetchByApproved_First(boolean approved,
		OrderByComparator<LotReservation> orderByComparator) {
		List<LotReservation> list = findByApproved(approved, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lot reservation in the ordered set where approved = &#63;.
	 *
	 * @param approved the approved
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot reservation
	 * @throws NoSuchLotReservationException if a matching lot reservation could not be found
	 */
	@Override
	public LotReservation findByApproved_Last(boolean approved,
		OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException {
		LotReservation lotReservation = fetchByApproved_Last(approved,
				orderByComparator);

		if (lotReservation != null) {
			return lotReservation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("approved=");
		msg.append(approved);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLotReservationException(msg.toString());
	}

	/**
	 * Returns the last lot reservation in the ordered set where approved = &#63;.
	 *
	 * @param approved the approved
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lot reservation, or <code>null</code> if a matching lot reservation could not be found
	 */
	@Override
	public LotReservation fetchByApproved_Last(boolean approved,
		OrderByComparator<LotReservation> orderByComparator) {
		int count = countByApproved(approved);

		if (count == 0) {
			return null;
		}

		List<LotReservation> list = findByApproved(approved, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lot reservations before and after the current lot reservation in the ordered set where approved = &#63;.
	 *
	 * @param reservationId the primary key of the current lot reservation
	 * @param approved the approved
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lot reservation
	 * @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation[] findByApproved_PrevAndNext(long reservationId,
		boolean approved, OrderByComparator<LotReservation> orderByComparator)
		throws NoSuchLotReservationException {
		LotReservation lotReservation = findByPrimaryKey(reservationId);

		Session session = null;

		try {
			session = openSession();

			LotReservation[] array = new LotReservationImpl[3];

			array[0] = getByApproved_PrevAndNext(session, lotReservation,
					approved, orderByComparator, true);

			array[1] = lotReservation;

			array[2] = getByApproved_PrevAndNext(session, lotReservation,
					approved, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LotReservation getByApproved_PrevAndNext(Session session,
		LotReservation lotReservation, boolean approved,
		OrderByComparator<LotReservation> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LOTRESERVATION_WHERE);

		query.append(_FINDER_COLUMN_APPROVED_APPROVED_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LotReservationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(approved);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lotReservation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LotReservation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lot reservations where approved = &#63; from the database.
	 *
	 * @param approved the approved
	 */
	@Override
	public void removeByApproved(boolean approved) {
		for (LotReservation lotReservation : findByApproved(approved,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(lotReservation);
		}
	}

	/**
	 * Returns the number of lot reservations where approved = &#63;.
	 *
	 * @param approved the approved
	 * @return the number of matching lot reservations
	 */
	@Override
	public int countByApproved(boolean approved) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_APPROVED;

		Object[] finderArgs = new Object[] { approved };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LOTRESERVATION_WHERE);

			query.append(_FINDER_COLUMN_APPROVED_APPROVED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(approved);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPROVED_APPROVED_2 = "lotReservation.approved = ?";

	public LotReservationPersistenceImpl() {
		setModelClass(LotReservation.class);
	}

	/**
	 * Caches the lot reservation in the entity cache if it is enabled.
	 *
	 * @param lotReservation the lot reservation
	 */
	@Override
	public void cacheResult(LotReservation lotReservation) {
		entityCache.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationImpl.class, lotReservation.getPrimaryKey(),
			lotReservation);

		lotReservation.resetOriginalValues();
	}

	/**
	 * Caches the lot reservations in the entity cache if it is enabled.
	 *
	 * @param lotReservations the lot reservations
	 */
	@Override
	public void cacheResult(List<LotReservation> lotReservations) {
		for (LotReservation lotReservation : lotReservations) {
			if (entityCache.getResult(
						LotReservationModelImpl.ENTITY_CACHE_ENABLED,
						LotReservationImpl.class, lotReservation.getPrimaryKey()) == null) {
				cacheResult(lotReservation);
			}
			else {
				lotReservation.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lot reservations.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LotReservationImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lot reservation.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LotReservation lotReservation) {
		entityCache.removeResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationImpl.class, lotReservation.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LotReservation> lotReservations) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LotReservation lotReservation : lotReservations) {
			entityCache.removeResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
				LotReservationImpl.class, lotReservation.getPrimaryKey());
		}
	}

	/**
	 * Creates a new lot reservation with the primary key. Does not add the lot reservation to the database.
	 *
	 * @param reservationId the primary key for the new lot reservation
	 * @return the new lot reservation
	 */
	@Override
	public LotReservation create(long reservationId) {
		LotReservation lotReservation = new LotReservationImpl();

		lotReservation.setNew(true);
		lotReservation.setPrimaryKey(reservationId);

		return lotReservation;
	}

	/**
	 * Removes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param reservationId the primary key of the lot reservation
	 * @return the lot reservation that was removed
	 * @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation remove(long reservationId)
		throws NoSuchLotReservationException {
		return remove((Serializable)reservationId);
	}

	/**
	 * Removes the lot reservation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lot reservation
	 * @return the lot reservation that was removed
	 * @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation remove(Serializable primaryKey)
		throws NoSuchLotReservationException {
		Session session = null;

		try {
			session = openSession();

			LotReservation lotReservation = (LotReservation)session.get(LotReservationImpl.class,
					primaryKey);

			if (lotReservation == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLotReservationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(lotReservation);
		}
		catch (NoSuchLotReservationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LotReservation removeImpl(LotReservation lotReservation) {
		lotReservation = toUnwrappedModel(lotReservation);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(lotReservation)) {
				lotReservation = (LotReservation)session.get(LotReservationImpl.class,
						lotReservation.getPrimaryKeyObj());
			}

			if (lotReservation != null) {
				session.delete(lotReservation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (lotReservation != null) {
			clearCache(lotReservation);
		}

		return lotReservation;
	}

	@Override
	public LotReservation updateImpl(LotReservation lotReservation) {
		lotReservation = toUnwrappedModel(lotReservation);

		boolean isNew = lotReservation.isNew();

		LotReservationModelImpl lotReservationModelImpl = (LotReservationModelImpl)lotReservation;

		Session session = null;

		try {
			session = openSession();

			if (lotReservation.isNew()) {
				session.save(lotReservation);

				lotReservation.setNew(false);
			}
			else {
				lotReservation = (LotReservation)session.merge(lotReservation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LotReservationModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((lotReservationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPROVED.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						lotReservationModelImpl.getOriginalApproved()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_APPROVED, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPROVED,
					args);

				args = new Object[] { lotReservationModelImpl.getApproved() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_APPROVED, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPROVED,
					args);
			}
		}

		entityCache.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
			LotReservationImpl.class, lotReservation.getPrimaryKey(),
			lotReservation, false);

		lotReservation.resetOriginalValues();

		return lotReservation;
	}

	protected LotReservation toUnwrappedModel(LotReservation lotReservation) {
		if (lotReservation instanceof LotReservationImpl) {
			return lotReservation;
		}

		LotReservationImpl lotReservationImpl = new LotReservationImpl();

		lotReservationImpl.setNew(lotReservation.isNew());
		lotReservationImpl.setPrimaryKey(lotReservation.getPrimaryKey());

		lotReservationImpl.setReservationId(lotReservation.getReservationId());
		lotReservationImpl.setParticipantId(lotReservation.getParticipantId());
		lotReservationImpl.setNumberLots(lotReservation.getNumberLots());
		lotReservationImpl.setApproved(lotReservation.isApproved());
		lotReservationImpl.setReservationDate(lotReservation.getReservationDate());

		return lotReservationImpl;
	}

	/**
	 * Returns the lot reservation with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lot reservation
	 * @return the lot reservation
	 * @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLotReservationException {
		LotReservation lotReservation = fetchByPrimaryKey(primaryKey);

		if (lotReservation == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLotReservationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return lotReservation;
	}

	/**
	 * Returns the lot reservation with the primary key or throws a {@link NoSuchLotReservationException} if it could not be found.
	 *
	 * @param reservationId the primary key of the lot reservation
	 * @return the lot reservation
	 * @throws NoSuchLotReservationException if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation findByPrimaryKey(long reservationId)
		throws NoSuchLotReservationException {
		return findByPrimaryKey((Serializable)reservationId);
	}

	/**
	 * Returns the lot reservation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lot reservation
	 * @return the lot reservation, or <code>null</code> if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
				LotReservationImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LotReservation lotReservation = (LotReservation)serializable;

		if (lotReservation == null) {
			Session session = null;

			try {
				session = openSession();

				lotReservation = (LotReservation)session.get(LotReservationImpl.class,
						primaryKey);

				if (lotReservation != null) {
					cacheResult(lotReservation);
				}
				else {
					entityCache.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
						LotReservationImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
					LotReservationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return lotReservation;
	}

	/**
	 * Returns the lot reservation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param reservationId the primary key of the lot reservation
	 * @return the lot reservation, or <code>null</code> if a lot reservation with the primary key could not be found
	 */
	@Override
	public LotReservation fetchByPrimaryKey(long reservationId) {
		return fetchByPrimaryKey((Serializable)reservationId);
	}

	@Override
	public Map<Serializable, LotReservation> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LotReservation> map = new HashMap<Serializable, LotReservation>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LotReservation lotReservation = fetchByPrimaryKey(primaryKey);

			if (lotReservation != null) {
				map.put(primaryKey, lotReservation);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
					LotReservationImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LotReservation)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_LOTRESERVATION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LotReservation lotReservation : (List<LotReservation>)q.list()) {
				map.put(lotReservation.getPrimaryKeyObj(), lotReservation);

				cacheResult(lotReservation);

				uncachedPrimaryKeys.remove(lotReservation.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LotReservationModelImpl.ENTITY_CACHE_ENABLED,
					LotReservationImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the lot reservations.
	 *
	 * @return the lot reservations
	 */
	@Override
	public List<LotReservation> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lot reservations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @return the range of lot reservations
	 */
	@Override
	public List<LotReservation> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lot reservations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lot reservations
	 */
	@Override
	public List<LotReservation> findAll(int start, int end,
		OrderByComparator<LotReservation> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the lot reservations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LotReservationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lot reservations
	 * @param end the upper bound of the range of lot reservations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of lot reservations
	 */
	@Override
	public List<LotReservation> findAll(int start, int end,
		OrderByComparator<LotReservation> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LotReservation> list = null;

		if (retrieveFromCache) {
			list = (List<LotReservation>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LOTRESERVATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LOTRESERVATION;

				if (pagination) {
					sql = sql.concat(LotReservationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LotReservation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LotReservation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lot reservations from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LotReservation lotReservation : findAll()) {
			remove(lotReservation);
		}
	}

	/**
	 * Returns the number of lot reservations.
	 *
	 * @return the number of lot reservations
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LOTRESERVATION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LotReservationModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the lot reservation persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LotReservationImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LOTRESERVATION = "SELECT lotReservation FROM LotReservation lotReservation";
	private static final String _SQL_SELECT_LOTRESERVATION_WHERE_PKS_IN = "SELECT lotReservation FROM LotReservation lotReservation WHERE reservationId IN (";
	private static final String _SQL_SELECT_LOTRESERVATION_WHERE = "SELECT lotReservation FROM LotReservation lotReservation WHERE ";
	private static final String _SQL_COUNT_LOTRESERVATION = "SELECT COUNT(lotReservation) FROM LotReservation lotReservation";
	private static final String _SQL_COUNT_LOTRESERVATION_WHERE = "SELECT COUNT(lotReservation) FROM LotReservation lotReservation WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "lotReservation.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LotReservation exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LotReservation exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(LotReservationPersistenceImpl.class);
}