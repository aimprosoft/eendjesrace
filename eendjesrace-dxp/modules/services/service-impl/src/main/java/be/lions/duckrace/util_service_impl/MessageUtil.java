package be.lions.duckrace.util_service_impl;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MessageUtil {

	private static final String RESOURCE_BUNDLE_NAME = "Language";

	public static String getMessage(String language, String key, Object... params) {
		String result = "???" + key + "???";
		Locale locale = getLocale(language);
		ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, locale);
		if (rb == null) {
			return result;
		}
		try {
			result = rb.getString(key);
		} catch (MissingResourceException e) {
			return result;
		}
		if (params != null) {
			result = new MessageFormat(result, locale).format(params);
		}

		return result;
	}

	private static Locale getLocale(String language) {
		return new Locale(language);
	}
}
