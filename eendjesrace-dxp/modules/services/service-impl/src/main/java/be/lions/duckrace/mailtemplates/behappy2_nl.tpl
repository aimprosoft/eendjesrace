<html>
<head>
<title>$title</title>
</head>
<body style="padding:0;margin:0;font-family:Verdana;font-size:12px">
<center>
<table cellpadding="0" cellspacing="0" border="0" width="560" style="border-collapse:collapse;">
<tr>
<td valign="top"><img src="$url/eendjes-theme/images/mail/mail-head-2014.png" width="560" height="185" alt="" style="margin-bottom:43px;border:0;"/></td>
</tr>
<tr>
<td valign="top"><h1 style="margin-top:0px;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:30px;font-weight:normal;">Beste $fullName,</h1></td>
</tr>
<tr>
<td valign="top">
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Uw aankoop van <b>${lots.size()} eendjes</b> voor de $title
werd correct ontvangen.
</p>
<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Wat volgt zijn de eendjesnummers die aan u werden toegekend. Houd deze mail zorgvuldig bij.
</p>
<table cellspacing="0" cellpadding="3" style="border-collapse:collapse;width:300px;margin-right:50px;margin-bottom:30px;margin-left:35px;">
	<tr>
		<th style="text-align:center;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Eendjesnummer</th>
		<th style="text-align:center;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">Code</th>
	</tr>
#foreach($lot in $lots)
	<tr>
		<td valign="top" style="text-align:center;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;"><p>$lot.number</p></td>
		<td valign="top" style="text-align:center;margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;"><p>${lot.code}${lot.checksum}</p></td>
	</tr>
#end
</table>

<p style="margin-right:50px;margin-bottom:30px;margin-left:35px;font-family:Arial,Verdana;color:#076d97;font-size:14px;">
Met vriendelijke groeten,<br/>
Een Hart voor Limburg
</p>
</td>
</tr>
</table>
</center>
</body>
</html>