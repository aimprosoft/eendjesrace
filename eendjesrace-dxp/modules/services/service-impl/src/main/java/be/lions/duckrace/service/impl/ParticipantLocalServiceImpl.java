/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.exception.*;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.impl.ParticipantImpl;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.base.ParticipantLocalServiceBaseImpl;
import be.lions.duckrace.util_service_api.StringUtils;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import java.util.List;

/**
 * The implementation of the participant local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.service.ParticipantLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParticipantLocalServiceBaseImpl
 * @see be.lions.duckrace.service.ParticipantLocalServiceUtil
 */
@ProviderType
public class ParticipantLocalServiceImpl extends ParticipantLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link be.lions.duckrace.service.ParticipantLocalServiceUtil} to access the participant local service.
	 */

    private Participant createParticipant(String mobileNumber) throws SystemException, InvalidMobileNumberException, DuplicateMobileNumberException {
        validate(mobileNumber);
        if(isMobileNumberAlreadyTaken(mobileNumber)) {
            throw new DuplicateMobileNumberException();
        }
        long participantId = CounterLocalServiceUtil.increment();
        Participant participant = participantPersistence.create(participantId);
        participant.setMobileNumber(StringUtils.getStrippedMobileNumber(mobileNumber));
        return participantPersistence.update(participant);
    }

    private void validate(String mobileNumber) throws InvalidMobileNumberException {
        if(!StringUtils.isValidMobileNumber(mobileNumber)) {
            throw new InvalidMobileNumberException();
        }
    }

    public Participant getOrCreateParticipant(String mobileNumber, String firstName, String lastName, String emailAddress, boolean contestParticipant, boolean mercedesDriver, String carBrand, boolean mercedesOld, String language) throws InvalidMobileNumberException, SystemException, NoSuchParticipantException, MobileNumberAlreadyUsedInvalidDataException, MobileNumberAlreadyUsedPartlyValidDataException {
        Participant p = getMatchingParticipant(mobileNumber, firstName, lastName, emailAddress);
        if(p == null) {
            try {
                Participant participant = createParticipant(mobileNumber);
                participant.setFirstName(firstName);
                participant.setLastName(lastName);
                participant.setEmailAddress(emailAddress);
                participant.setContestParticipant(contestParticipant);
                participant.setMercedesDriver(mercedesDriver);
                participant.setCarBrand(carBrand);
                participant.setMercedesOld(mercedesOld);
                participant.setLanguage(language);
                return participantPersistence.update(participant);
            } catch(DuplicateMobileNumberException e) {
                //cannot occur
            }
        }
        return p;
    }

    public Participant getMatchingParticipant(String mobileNumber, String firstName, String lastName, String emailAddress) throws MobileNumberAlreadyUsedPartlyValidDataException, SystemException, MobileNumberAlreadyUsedInvalidDataException {
        try {
            Participant p = getParticipantByMobileNumber(mobileNumber);
            if(p.matchesAll(firstName, lastName, emailAddress)) {
                return p;
            }
            if(p.matchesOneOf(firstName, lastName, emailAddress)) {
                throw new MobileNumberAlreadyUsedPartlyValidDataException(p);
            }
            throw new MobileNumberAlreadyUsedInvalidDataException();
        } catch(NoSuchParticipantException e) {
            return null;
        }
    }

    public Participant getParticipantByMobileNumber(String mobileNumber) throws NoSuchParticipantException, SystemException {
        return participantPersistence.findByMobileNumber(StringUtils.getStrippedMobileNumber(mobileNumber));
    }

    private boolean isMobileNumberAlreadyTaken(String mobileNumber) throws SystemException {
        try {
            getParticipantByMobileNumber(mobileNumber);
            return true;
        } catch(NoSuchParticipantException e) {
            return false;
        }
    }

    public Participant getParticipantByLotNumber(int lotNumber) throws NoSuchLotException, PortalException, SystemException {
        return LotLocalServiceUtil.getLotByNumber(lotNumber).getParticipant();
    }

    public Participant updateParticipant(long id, String mobileNumber, String emailAddress, String firstName, String lastName) throws SystemException, PortalException {
        String normalizedMobileNumber = StringUtils.getStrippedMobileNumber(mobileNumber);
        Participant result = getParticipant(id);
        if(!result.getMobileNumber().equals(normalizedMobileNumber) && isMobileNumberAlreadyTaken(normalizedMobileNumber)) {
            throw new DuplicateMobileNumberException();
        }
        result.setMobileNumber(normalizedMobileNumber);
        result.setEmailAddress(emailAddress);
        result.setFirstName(firstName);
        result.setLastName(lastName);
        return updateParticipant(result);
    }

    @SuppressWarnings("unchecked")
    public List<Participant> searchParticipants(String mobileNumber, String firstName, String lastName, String emailAddress) throws SystemException {
        DynamicQuery query = DynamicQueryFactoryUtil.forClass(ParticipantImpl.class, PortalClassLoaderUtil.getClassLoader());
        if(!StringUtils.isEmpty(mobileNumber)) {
            query.add(PropertyFactoryUtil.forName("mobileNumber").like("%"+StringUtils.getStrippedMobileNumber(mobileNumber)+"%"));
        }
        if(!StringUtils.isEmpty(emailAddress)) {
            query.add(PropertyFactoryUtil.forName("emailAddress").like("%"+emailAddress+"%"));
        }
        if(!StringUtils.isEmpty(firstName)) {
            query.add(PropertyFactoryUtil.forName("firstName").like("%"+firstName+"%"));
        }
        if(!StringUtils.isEmpty(lastName)) {
            query.add(PropertyFactoryUtil.forName("lastName").like("%"+lastName+"%"));
        }
        return (List<Participant>)((List)dynamicQuery(query));
    }

}