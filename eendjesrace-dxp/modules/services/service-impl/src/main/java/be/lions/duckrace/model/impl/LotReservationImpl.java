/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.exception.NoSuchParticipantException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.service.DuckRaceLocalServiceUtil;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.service.ParticipantLocalServiceUtil;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.SystemProperties;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * The extended model implementation for the LotReservation service. Represents a row in the &quot;DuckRace_LotReservation&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.model.LotReservation} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class LotReservationImpl extends LotReservationBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a lot reservation model instance should use the {@link be.lions.duckrace.model.LotReservation} interface instead.
	 */
	public LotReservationImpl() {
	}

	public int getTotalCost() {
		return (int) (getNumberLots() * DuckRaceLocalServiceUtil.getLotPrice());
	}

	public Participant getParticipant() throws SystemException, PortalException {
		try {
			return ParticipantLocalServiceUtil.getParticipant(getParticipantId());
		} catch(NoSuchParticipantException e) {
			return null;
		}
	}

	public List<Lot> getLots() throws SystemException {
		return LotLocalServiceUtil.getLotsForReservation(getReservationId());
	}

	public File generatePdf() throws DocumentException, SystemException, PortalException, IOException {
		File result = new File(getTempFilePath());
		OutputStream fos = new FileOutputStream(result);
		Document doc = new Document(PageSize.A4);
		PdfWriter.getInstance(doc, fos);
		doc.open();
		doc.add(createLotTable());
		doc.close();
		fos.close();
		return result;
	}

	private Element createLotTable() throws SystemException, PortalException {
		PdfPTable result = new PdfPTable(5);
		result.setWidthPercentage(100);
		result.getDefaultCell().setPadding(10);
		result.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		for (Lot lot: getLots()) {
			PdfPCell cell = new PdfPCell();
			cell.addElement(getParagraph("Nummer", ""+lot.getNumber()));
			cell.addElement(getParagraph("Code", lot.getCode()+lot.getChecksum()));
			cell.addElement(getDots());
			cell.addElement(getDots());
			result.addCell(cell);
		}
		return result;
	}

	private String getTempFilePath() throws SystemException, PortalException {
		StringBuilder path = new StringBuilder();
		path.append(SystemProperties.get(SystemProperties.TMP_DIR));
		path.append(StringPool.SLASH);
		path.append(getReservationId()+"-eendjes");
		path.append(StringPool.PERIOD);
		path.append("pdf");
		return path.toString();
	}

	private Element getParagraph(String label, String boldValue) {
		Paragraph result = new Paragraph();
		result.add(new Phrase(label+": ", getFont(false)));
		result.add(new Phrase(boldValue, getFont(true)));
		return result;
	}

	private Element getDots() {
		return new Paragraph("................................", getFont(false));
	}

	private Font getFont(boolean bold) {
		int fontType = bold ? Font.BOLD : Font.NORMAL;
		return new Font(Font.FontFamily.HELVETICA, 10, fontType);
	}
}