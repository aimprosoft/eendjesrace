/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import be.lions.duckrace.exception.NoSuchParticipantException;
import be.lions.duckrace.model.Participant;
import be.lions.duckrace.model.impl.ParticipantImpl;
import be.lions.duckrace.model.impl.ParticipantModelImpl;
import be.lions.duckrace.service.persistence.ParticipantPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the participant service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParticipantPersistence
 * @see be.lions.duckrace.service.persistence.ParticipantUtil
 * @generated
 */
@ProviderType
public class ParticipantPersistenceImpl extends BasePersistenceImpl<Participant>
	implements ParticipantPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ParticipantUtil} to access the participant persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ParticipantImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantModelImpl.FINDER_CACHE_ENABLED, ParticipantImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantModelImpl.FINDER_CACHE_ENABLED, ParticipantImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_MOBILENUMBER = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantModelImpl.FINDER_CACHE_ENABLED, ParticipantImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByMobileNumber",
			new String[] { String.class.getName() },
			ParticipantModelImpl.MOBILENUMBER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MOBILENUMBER = new FinderPath(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMobileNumber",
			new String[] { String.class.getName() });

	/**
	 * Returns the participant where mobileNumber = &#63; or throws a {@link NoSuchParticipantException} if it could not be found.
	 *
	 * @param mobileNumber the mobile number
	 * @return the matching participant
	 * @throws NoSuchParticipantException if a matching participant could not be found
	 */
	@Override
	public Participant findByMobileNumber(String mobileNumber)
		throws NoSuchParticipantException {
		Participant participant = fetchByMobileNumber(mobileNumber);

		if (participant == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("mobileNumber=");
			msg.append(mobileNumber);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchParticipantException(msg.toString());
		}

		return participant;
	}

	/**
	 * Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param mobileNumber the mobile number
	 * @return the matching participant, or <code>null</code> if a matching participant could not be found
	 */
	@Override
	public Participant fetchByMobileNumber(String mobileNumber) {
		return fetchByMobileNumber(mobileNumber, true);
	}

	/**
	 * Returns the participant where mobileNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param mobileNumber the mobile number
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching participant, or <code>null</code> if a matching participant could not be found
	 */
	@Override
	public Participant fetchByMobileNumber(String mobileNumber,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { mobileNumber };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
					finderArgs, this);
		}

		if (result instanceof Participant) {
			Participant participant = (Participant)result;

			if (!Objects.equals(mobileNumber, participant.getMobileNumber())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PARTICIPANT_WHERE);

			boolean bindMobileNumber = false;

			if (mobileNumber == null) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1);
			}
			else if (mobileNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3);
			}
			else {
				bindMobileNumber = true;

				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMobileNumber) {
					qPos.add(mobileNumber);
				}

				List<Participant> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
						finderArgs, list);
				}
				else {
					Participant participant = list.get(0);

					result = participant;

					cacheResult(participant);

					if ((participant.getMobileNumber() == null) ||
							!participant.getMobileNumber().equals(mobileNumber)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
							finderArgs, participant);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Participant)result;
		}
	}

	/**
	 * Removes the participant where mobileNumber = &#63; from the database.
	 *
	 * @param mobileNumber the mobile number
	 * @return the participant that was removed
	 */
	@Override
	public Participant removeByMobileNumber(String mobileNumber)
		throws NoSuchParticipantException {
		Participant participant = findByMobileNumber(mobileNumber);

		return remove(participant);
	}

	/**
	 * Returns the number of participants where mobileNumber = &#63;.
	 *
	 * @param mobileNumber the mobile number
	 * @return the number of matching participants
	 */
	@Override
	public int countByMobileNumber(String mobileNumber) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MOBILENUMBER;

		Object[] finderArgs = new Object[] { mobileNumber };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PARTICIPANT_WHERE);

			boolean bindMobileNumber = false;

			if (mobileNumber == null) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1);
			}
			else if (mobileNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3);
			}
			else {
				bindMobileNumber = true;

				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMobileNumber) {
					qPos.add(mobileNumber);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1 = "participant.mobileNumber IS NULL";
	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2 = "participant.mobileNumber = ?";
	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3 = "(participant.mobileNumber IS NULL OR participant.mobileNumber = '')";

	public ParticipantPersistenceImpl() {
		setModelClass(Participant.class);
	}

	/**
	 * Caches the participant in the entity cache if it is enabled.
	 *
	 * @param participant the participant
	 */
	@Override
	public void cacheResult(Participant participant) {
		entityCache.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantImpl.class, participant.getPrimaryKey(), participant);

		finderCache.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER,
			new Object[] { participant.getMobileNumber() }, participant);

		participant.resetOriginalValues();
	}

	/**
	 * Caches the participants in the entity cache if it is enabled.
	 *
	 * @param participants the participants
	 */
	@Override
	public void cacheResult(List<Participant> participants) {
		for (Participant participant : participants) {
			if (entityCache.getResult(
						ParticipantModelImpl.ENTITY_CACHE_ENABLED,
						ParticipantImpl.class, participant.getPrimaryKey()) == null) {
				cacheResult(participant);
			}
			else {
				participant.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all participants.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ParticipantImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the participant.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Participant participant) {
		entityCache.removeResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantImpl.class, participant.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ParticipantModelImpl)participant, true);
	}

	@Override
	public void clearCache(List<Participant> participants) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Participant participant : participants) {
			entityCache.removeResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
				ParticipantImpl.class, participant.getPrimaryKey());

			clearUniqueFindersCache((ParticipantModelImpl)participant, true);
		}
	}

	protected void cacheUniqueFindersCache(
		ParticipantModelImpl participantModelImpl) {
		Object[] args = new Object[] { participantModelImpl.getMobileNumber() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_MOBILENUMBER, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_MOBILENUMBER, args,
			participantModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		ParticipantModelImpl participantModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { participantModelImpl.getMobileNumber() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_MOBILENUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_MOBILENUMBER, args);
		}

		if ((participantModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_MOBILENUMBER.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					participantModelImpl.getOriginalMobileNumber()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_MOBILENUMBER, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_MOBILENUMBER, args);
		}
	}

	/**
	 * Creates a new participant with the primary key. Does not add the participant to the database.
	 *
	 * @param participantId the primary key for the new participant
	 * @return the new participant
	 */
	@Override
	public Participant create(long participantId) {
		Participant participant = new ParticipantImpl();

		participant.setNew(true);
		participant.setPrimaryKey(participantId);

		return participant;
	}

	/**
	 * Removes the participant with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param participantId the primary key of the participant
	 * @return the participant that was removed
	 * @throws NoSuchParticipantException if a participant with the primary key could not be found
	 */
	@Override
	public Participant remove(long participantId)
		throws NoSuchParticipantException {
		return remove((Serializable)participantId);
	}

	/**
	 * Removes the participant with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the participant
	 * @return the participant that was removed
	 * @throws NoSuchParticipantException if a participant with the primary key could not be found
	 */
	@Override
	public Participant remove(Serializable primaryKey)
		throws NoSuchParticipantException {
		Session session = null;

		try {
			session = openSession();

			Participant participant = (Participant)session.get(ParticipantImpl.class,
					primaryKey);

			if (participant == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchParticipantException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(participant);
		}
		catch (NoSuchParticipantException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Participant removeImpl(Participant participant) {
		participant = toUnwrappedModel(participant);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(participant)) {
				participant = (Participant)session.get(ParticipantImpl.class,
						participant.getPrimaryKeyObj());
			}

			if (participant != null) {
				session.delete(participant);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (participant != null) {
			clearCache(participant);
		}

		return participant;
	}

	@Override
	public Participant updateImpl(Participant participant) {
		participant = toUnwrappedModel(participant);

		boolean isNew = participant.isNew();

		ParticipantModelImpl participantModelImpl = (ParticipantModelImpl)participant;

		Session session = null;

		try {
			session = openSession();

			if (participant.isNew()) {
				session.save(participant);

				participant.setNew(false);
			}
			else {
				participant = (Participant)session.merge(participant);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ParticipantModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		entityCache.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
			ParticipantImpl.class, participant.getPrimaryKey(), participant,
			false);

		clearUniqueFindersCache(participantModelImpl, false);
		cacheUniqueFindersCache(participantModelImpl);

		participant.resetOriginalValues();

		return participant;
	}

	protected Participant toUnwrappedModel(Participant participant) {
		if (participant instanceof ParticipantImpl) {
			return participant;
		}

		ParticipantImpl participantImpl = new ParticipantImpl();

		participantImpl.setNew(participant.isNew());
		participantImpl.setPrimaryKey(participant.getPrimaryKey());

		participantImpl.setParticipantId(participant.getParticipantId());
		participantImpl.setEmailAddress(participant.getEmailAddress());
		participantImpl.setMobileNumber(participant.getMobileNumber());
		participantImpl.setFirstName(participant.getFirstName());
		participantImpl.setLastName(participant.getLastName());
		participantImpl.setContestParticipant(participant.isContestParticipant());
		participantImpl.setMercedesDriver(participant.isMercedesDriver());
		participantImpl.setCarBrand(participant.getCarBrand());
		participantImpl.setMercedesOld(participant.isMercedesOld());
		participantImpl.setLanguage(participant.getLanguage());

		return participantImpl;
	}

	/**
	 * Returns the participant with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the participant
	 * @return the participant
	 * @throws NoSuchParticipantException if a participant with the primary key could not be found
	 */
	@Override
	public Participant findByPrimaryKey(Serializable primaryKey)
		throws NoSuchParticipantException {
		Participant participant = fetchByPrimaryKey(primaryKey);

		if (participant == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchParticipantException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return participant;
	}

	/**
	 * Returns the participant with the primary key or throws a {@link NoSuchParticipantException} if it could not be found.
	 *
	 * @param participantId the primary key of the participant
	 * @return the participant
	 * @throws NoSuchParticipantException if a participant with the primary key could not be found
	 */
	@Override
	public Participant findByPrimaryKey(long participantId)
		throws NoSuchParticipantException {
		return findByPrimaryKey((Serializable)participantId);
	}

	/**
	 * Returns the participant with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the participant
	 * @return the participant, or <code>null</code> if a participant with the primary key could not be found
	 */
	@Override
	public Participant fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
				ParticipantImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Participant participant = (Participant)serializable;

		if (participant == null) {
			Session session = null;

			try {
				session = openSession();

				participant = (Participant)session.get(ParticipantImpl.class,
						primaryKey);

				if (participant != null) {
					cacheResult(participant);
				}
				else {
					entityCache.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
						ParticipantImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
					ParticipantImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return participant;
	}

	/**
	 * Returns the participant with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param participantId the primary key of the participant
	 * @return the participant, or <code>null</code> if a participant with the primary key could not be found
	 */
	@Override
	public Participant fetchByPrimaryKey(long participantId) {
		return fetchByPrimaryKey((Serializable)participantId);
	}

	@Override
	public Map<Serializable, Participant> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Participant> map = new HashMap<Serializable, Participant>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Participant participant = fetchByPrimaryKey(primaryKey);

			if (participant != null) {
				map.put(primaryKey, participant);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
					ParticipantImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Participant)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PARTICIPANT_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Participant participant : (List<Participant>)q.list()) {
				map.put(participant.getPrimaryKeyObj(), participant);

				cacheResult(participant);

				uncachedPrimaryKeys.remove(participant.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ParticipantModelImpl.ENTITY_CACHE_ENABLED,
					ParticipantImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the participants.
	 *
	 * @return the participants
	 */
	@Override
	public List<Participant> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the participants.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of participants
	 * @param end the upper bound of the range of participants (not inclusive)
	 * @return the range of participants
	 */
	@Override
	public List<Participant> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the participants.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of participants
	 * @param end the upper bound of the range of participants (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of participants
	 */
	@Override
	public List<Participant> findAll(int start, int end,
		OrderByComparator<Participant> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the participants.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ParticipantModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of participants
	 * @param end the upper bound of the range of participants (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of participants
	 */
	@Override
	public List<Participant> findAll(int start, int end,
		OrderByComparator<Participant> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Participant> list = null;

		if (retrieveFromCache) {
			list = (List<Participant>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PARTICIPANT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PARTICIPANT;

				if (pagination) {
					sql = sql.concat(ParticipantModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Participant>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Participant>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the participants from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Participant participant : findAll()) {
			remove(participant);
		}
	}

	/**
	 * Returns the number of participants.
	 *
	 * @return the number of participants
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PARTICIPANT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ParticipantModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the participant persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ParticipantImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PARTICIPANT = "SELECT participant FROM Participant participant";
	private static final String _SQL_SELECT_PARTICIPANT_WHERE_PKS_IN = "SELECT participant FROM Participant participant WHERE participantId IN (";
	private static final String _SQL_SELECT_PARTICIPANT_WHERE = "SELECT participant FROM Participant participant WHERE ";
	private static final String _SQL_COUNT_PARTICIPANT = "SELECT COUNT(participant) FROM Participant participant";
	private static final String _SQL_COUNT_PARTICIPANT_WHERE = "SELECT COUNT(participant) FROM Participant participant WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "participant.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Participant exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Participant exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ParticipantPersistenceImpl.class);
}