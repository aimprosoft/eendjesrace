/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.model.impl;

import aQute.bnd.annotation.ProviderType;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.service.LotLocalServiceUtil;
import be.lions.duckrace.util_service_api.StringUtils;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

import java.util.List;

/**
 * The extended model implementation for the Participant service. Represents a row in the &quot;DuckRace_Participant&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link be.lions.duckrace.model.Participant} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class ParticipantImpl extends ParticipantBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a participant model instance should use the {@link be.lions.duckrace.model.Participant} interface instead.
	 */
	public ParticipantImpl() {
	}

	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	public List<Lot> getLots() throws SystemException {
		return LotLocalServiceUtil.getLotsForParticipant(getParticipantId());
	}

	public String getFormattedMobileNumber() {
		return StringUtils.getFormattedMobileNumber(getMobileNumber());
	}

	public String getTransactionSafeEmailAddress() {
		return StringUtil.replace(getEmailAddress(),
				new String[] {StringPool.AT, StringPool.PERIOD},
				new String[] {"_at_", "_dot_"});
	}

	public boolean matchesOneOf(String firstName, String lastName, String emailAddress) {
		return (firstName != null && firstName.equals(getFirstName())) ||
				(lastName != null && lastName.equals(getLastName())) ||
				(emailAddress != null && emailAddress.equals(getEmailAddress()));
	}

	public boolean matchesAll(String firstName, String lastName, String emailAddress) {
		return (firstName != null && firstName.equals(getFirstName())) &&
				(lastName != null && lastName.equals(getLastName())) &&
				(emailAddress != null && emailAddress.equals(getEmailAddress()));
	}

	public void validate() {

	}

}