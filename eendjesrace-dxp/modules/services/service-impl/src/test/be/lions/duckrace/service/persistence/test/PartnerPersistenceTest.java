/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.test;

import be.lions.duckrace.exception.NoSuchPartnerException;
import be.lions.duckrace.model.Partner;
import be.lions.duckrace.service.PartnerLocalServiceUtil;
import be.lions.duckrace.service.persistence.PartnerPersistence;
import be.lions.duckrace.service.persistence.PartnerUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class PartnerPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED,
				"be.lions.duckrace.service"));

	@Before
	public void setUp() {
		_persistence = PartnerUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Partner> iterator = _partners.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Partner partner = _persistence.create(pk);

		Assert.assertNotNull(partner);

		Assert.assertEquals(partner.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Partner newPartner = addPartner();

		_persistence.remove(newPartner);

		Partner existingPartner = _persistence.fetchByPrimaryKey(newPartner.getPrimaryKey());

		Assert.assertNull(existingPartner);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addPartner();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Partner newPartner = _persistence.create(pk);

		newPartner.setCategory(RandomTestUtil.randomString());

		newPartner.setName(RandomTestUtil.randomString());

		newPartner.setReturnedLotsCount(RandomTestUtil.nextInt());

		_partners.add(_persistence.update(newPartner));

		Partner existingPartner = _persistence.findByPrimaryKey(newPartner.getPrimaryKey());

		Assert.assertEquals(existingPartner.getPartnerId(),
			newPartner.getPartnerId());
		Assert.assertEquals(existingPartner.getCategory(),
			newPartner.getCategory());
		Assert.assertEquals(existingPartner.getName(), newPartner.getName());
		Assert.assertEquals(existingPartner.getReturnedLotsCount(),
			newPartner.getReturnedLotsCount());
	}

	@Test
	public void testCountByCategory() throws Exception {
		_persistence.countByCategory(StringPool.BLANK);

		_persistence.countByCategory(StringPool.NULL);

		_persistence.countByCategory((String)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Partner newPartner = addPartner();

		Partner existingPartner = _persistence.findByPrimaryKey(newPartner.getPrimaryKey());

		Assert.assertEquals(existingPartner, newPartner);
	}

	@Test(expected = NoSuchPartnerException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Partner> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("DuckRace_Partner",
			"partnerId", true, "category", true, "name", true,
			"returnedLotsCount", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Partner newPartner = addPartner();

		Partner existingPartner = _persistence.fetchByPrimaryKey(newPartner.getPrimaryKey());

		Assert.assertEquals(existingPartner, newPartner);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Partner missingPartner = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingPartner);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Partner newPartner1 = addPartner();
		Partner newPartner2 = addPartner();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newPartner1.getPrimaryKey());
		primaryKeys.add(newPartner2.getPrimaryKey());

		Map<Serializable, Partner> partners = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, partners.size());
		Assert.assertEquals(newPartner1,
			partners.get(newPartner1.getPrimaryKey()));
		Assert.assertEquals(newPartner2,
			partners.get(newPartner2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Partner> partners = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(partners.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Partner newPartner = addPartner();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newPartner.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Partner> partners = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, partners.size());
		Assert.assertEquals(newPartner, partners.get(newPartner.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Partner> partners = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(partners.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Partner newPartner = addPartner();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newPartner.getPrimaryKey());

		Map<Serializable, Partner> partners = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, partners.size());
		Assert.assertEquals(newPartner, partners.get(newPartner.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = PartnerLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Partner>() {
				@Override
				public void performAction(Partner partner) {
					Assert.assertNotNull(partner);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Partner newPartner = addPartner();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Partner.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("partnerId",
				newPartner.getPartnerId()));

		List<Partner> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Partner existingPartner = result.get(0);

		Assert.assertEquals(existingPartner, newPartner);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Partner.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("partnerId",
				RandomTestUtil.nextLong()));

		List<Partner> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Partner newPartner = addPartner();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Partner.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("partnerId"));

		Object newPartnerId = newPartner.getPartnerId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("partnerId",
				new Object[] { newPartnerId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingPartnerId = result.get(0);

		Assert.assertEquals(existingPartnerId, newPartnerId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Partner.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("partnerId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("partnerId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Partner addPartner() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Partner partner = _persistence.create(pk);

		partner.setCategory(RandomTestUtil.randomString());

		partner.setName(RandomTestUtil.randomString());

		partner.setReturnedLotsCount(RandomTestUtil.nextInt());

		_partners.add(_persistence.update(partner));

		return partner;
	}

	private List<Partner> _partners = new ArrayList<Partner>();
	private PartnerPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}