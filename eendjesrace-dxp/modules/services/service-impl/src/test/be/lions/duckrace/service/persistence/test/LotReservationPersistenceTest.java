/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.test;

import be.lions.duckrace.exception.NoSuchLotReservationException;
import be.lions.duckrace.model.LotReservation;
import be.lions.duckrace.service.LotReservationLocalServiceUtil;
import be.lions.duckrace.service.persistence.LotReservationPersistence;
import be.lions.duckrace.service.persistence.LotReservationUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class LotReservationPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED,
				"be.lions.duckrace.service"));

	@Before
	public void setUp() {
		_persistence = LotReservationUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<LotReservation> iterator = _lotReservations.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		LotReservation lotReservation = _persistence.create(pk);

		Assert.assertNotNull(lotReservation);

		Assert.assertEquals(lotReservation.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		LotReservation newLotReservation = addLotReservation();

		_persistence.remove(newLotReservation);

		LotReservation existingLotReservation = _persistence.fetchByPrimaryKey(newLotReservation.getPrimaryKey());

		Assert.assertNull(existingLotReservation);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addLotReservation();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		LotReservation newLotReservation = _persistence.create(pk);

		newLotReservation.setParticipantId(RandomTestUtil.nextLong());

		newLotReservation.setNumberLots(RandomTestUtil.nextInt());

		newLotReservation.setApproved(RandomTestUtil.randomBoolean());

		newLotReservation.setReservationDate(RandomTestUtil.nextDate());

		_lotReservations.add(_persistence.update(newLotReservation));

		LotReservation existingLotReservation = _persistence.findByPrimaryKey(newLotReservation.getPrimaryKey());

		Assert.assertEquals(existingLotReservation.getReservationId(),
			newLotReservation.getReservationId());
		Assert.assertEquals(existingLotReservation.getParticipantId(),
			newLotReservation.getParticipantId());
		Assert.assertEquals(existingLotReservation.getNumberLots(),
			newLotReservation.getNumberLots());
		Assert.assertEquals(existingLotReservation.getApproved(),
			newLotReservation.getApproved());
		Assert.assertEquals(Time.getShortTimestamp(
				existingLotReservation.getReservationDate()),
			Time.getShortTimestamp(newLotReservation.getReservationDate()));
	}

	@Test
	public void testCountByApproved() throws Exception {
		_persistence.countByApproved(RandomTestUtil.randomBoolean());

		_persistence.countByApproved(RandomTestUtil.randomBoolean());
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		LotReservation newLotReservation = addLotReservation();

		LotReservation existingLotReservation = _persistence.findByPrimaryKey(newLotReservation.getPrimaryKey());

		Assert.assertEquals(existingLotReservation, newLotReservation);
	}

	@Test(expected = NoSuchLotReservationException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<LotReservation> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("DuckRace_LotReservation",
			"reservationId", true, "participantId", true, "numberLots", true,
			"approved", true, "reservationDate", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		LotReservation newLotReservation = addLotReservation();

		LotReservation existingLotReservation = _persistence.fetchByPrimaryKey(newLotReservation.getPrimaryKey());

		Assert.assertEquals(existingLotReservation, newLotReservation);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		LotReservation missingLotReservation = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingLotReservation);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		LotReservation newLotReservation1 = addLotReservation();
		LotReservation newLotReservation2 = addLotReservation();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLotReservation1.getPrimaryKey());
		primaryKeys.add(newLotReservation2.getPrimaryKey());

		Map<Serializable, LotReservation> lotReservations = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, lotReservations.size());
		Assert.assertEquals(newLotReservation1,
			lotReservations.get(newLotReservation1.getPrimaryKey()));
		Assert.assertEquals(newLotReservation2,
			lotReservations.get(newLotReservation2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, LotReservation> lotReservations = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(lotReservations.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		LotReservation newLotReservation = addLotReservation();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLotReservation.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, LotReservation> lotReservations = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, lotReservations.size());
		Assert.assertEquals(newLotReservation,
			lotReservations.get(newLotReservation.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, LotReservation> lotReservations = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(lotReservations.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		LotReservation newLotReservation = addLotReservation();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLotReservation.getPrimaryKey());

		Map<Serializable, LotReservation> lotReservations = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, lotReservations.size());
		Assert.assertEquals(newLotReservation,
			lotReservations.get(newLotReservation.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = LotReservationLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<LotReservation>() {
				@Override
				public void performAction(LotReservation lotReservation) {
					Assert.assertNotNull(lotReservation);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		LotReservation newLotReservation = addLotReservation();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(LotReservation.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("reservationId",
				newLotReservation.getReservationId()));

		List<LotReservation> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		LotReservation existingLotReservation = result.get(0);

		Assert.assertEquals(existingLotReservation, newLotReservation);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(LotReservation.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("reservationId",
				RandomTestUtil.nextLong()));

		List<LotReservation> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		LotReservation newLotReservation = addLotReservation();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(LotReservation.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"reservationId"));

		Object newReservationId = newLotReservation.getReservationId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("reservationId",
				new Object[] { newReservationId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingReservationId = result.get(0);

		Assert.assertEquals(existingReservationId, newReservationId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(LotReservation.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"reservationId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("reservationId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected LotReservation addLotReservation() throws Exception {
		long pk = RandomTestUtil.nextLong();

		LotReservation lotReservation = _persistence.create(pk);

		lotReservation.setParticipantId(RandomTestUtil.nextLong());

		lotReservation.setNumberLots(RandomTestUtil.nextInt());

		lotReservation.setApproved(RandomTestUtil.randomBoolean());

		lotReservation.setReservationDate(RandomTestUtil.nextDate());

		_lotReservations.add(_persistence.update(lotReservation));

		return lotReservation;
	}

	private List<LotReservation> _lotReservations = new ArrayList<LotReservation>();
	private LotReservationPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}