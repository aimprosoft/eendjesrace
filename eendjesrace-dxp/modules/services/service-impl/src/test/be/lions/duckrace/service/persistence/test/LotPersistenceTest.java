/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.lions.duckrace.service.persistence.test;

import be.lions.duckrace.exception.NoSuchLotException;
import be.lions.duckrace.model.Lot;
import be.lions.duckrace.service.persistence.LotPersistence;
import be.lions.duckrace.service.persistence.LotUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class LotPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED,
				"be.lions.duckrace.service"));

	@Before
	public void setUp() {
		_persistence = LotUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Lot> iterator = _lots.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		String pk = RandomTestUtil.randomString();

		Lot lot = _persistence.create(pk);

		Assert.assertNotNull(lot);

		Assert.assertEquals(lot.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Lot newLot = addLot();

		_persistence.remove(newLot);

		Lot existingLot = _persistence.fetchByPrimaryKey(newLot.getPrimaryKey());

		Assert.assertNull(existingLot);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addLot();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		String pk = RandomTestUtil.randomString();

		Lot newLot = _persistence.create(pk);

		newLot.setChecksum(RandomTestUtil.randomString());

		newLot.setLotNumber(RandomTestUtil.nextInt());

		newLot.setNumber(RandomTestUtil.nextInt());

		newLot.setPressed(RandomTestUtil.randomBoolean());

		newLot.setNbTries(RandomTestUtil.nextInt());

		newLot.setLockoutDate(RandomTestUtil.nextDate());

		newLot.setParticipantId(RandomTestUtil.nextLong());

		newLot.setReservationId(RandomTestUtil.nextLong());

		newLot.setPartnerId(RandomTestUtil.nextLong());

		newLot.setOrderNumber(RandomTestUtil.randomString());

		_lots.add(_persistence.update(newLot));

		Lot existingLot = _persistence.findByPrimaryKey(newLot.getPrimaryKey());

		Assert.assertEquals(existingLot.getCode(), newLot.getCode());
		Assert.assertEquals(existingLot.getChecksum(), newLot.getChecksum());
		Assert.assertEquals(existingLot.getLotNumber(), newLot.getLotNumber());
		Assert.assertEquals(existingLot.getNumber(), newLot.getNumber());
		Assert.assertEquals(existingLot.getPressed(), newLot.getPressed());
		Assert.assertEquals(existingLot.getNbTries(), newLot.getNbTries());
		Assert.assertEquals(Time.getShortTimestamp(existingLot.getLockoutDate()),
			Time.getShortTimestamp(newLot.getLockoutDate()));
		Assert.assertEquals(existingLot.getParticipantId(),
			newLot.getParticipantId());
		Assert.assertEquals(existingLot.getReservationId(),
			newLot.getReservationId());
		Assert.assertEquals(existingLot.getPartnerId(), newLot.getPartnerId());
		Assert.assertEquals(existingLot.getOrderNumber(),
			newLot.getOrderNumber());
	}

	@Test
	public void testCountByParticipant() throws Exception {
		_persistence.countByParticipant(RandomTestUtil.nextLong());

		_persistence.countByParticipant(0L);
	}

	@Test
	public void testCountByReservation() throws Exception {
		_persistence.countByReservation(RandomTestUtil.nextLong());

		_persistence.countByReservation(0L);
	}

	@Test
	public void testCountByLotNumber() throws Exception {
		_persistence.countByLotNumber(RandomTestUtil.nextInt());

		_persistence.countByLotNumber(0);
	}

	@Test
	public void testCountByNumber() throws Exception {
		_persistence.countByNumber(RandomTestUtil.nextInt());

		_persistence.countByNumber(0);
	}

	@Test
	public void testCountByPartner() throws Exception {
		_persistence.countByPartner(RandomTestUtil.nextLong());

		_persistence.countByPartner(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Lot newLot = addLot();

		Lot existingLot = _persistence.findByPrimaryKey(newLot.getPrimaryKey());

		Assert.assertEquals(existingLot, newLot);
	}

	@Test(expected = NoSuchLotException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		String pk = RandomTestUtil.randomString();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Lot> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("DuckRace_Lot", "code",
			true, "checksum", true, "lotNumber", true, "number", true,
			"pressed", true, "nbTries", true, "lockoutDate", true,
			"participantId", true, "reservationId", true, "partnerId", true,
			"orderNumber", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Lot newLot = addLot();

		Lot existingLot = _persistence.fetchByPrimaryKey(newLot.getPrimaryKey());

		Assert.assertEquals(existingLot, newLot);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		String pk = RandomTestUtil.randomString();

		Lot missingLot = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingLot);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Lot newLot1 = addLot();
		Lot newLot2 = addLot();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLot1.getPrimaryKey());
		primaryKeys.add(newLot2.getPrimaryKey());

		Map<Serializable, Lot> lots = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, lots.size());
		Assert.assertEquals(newLot1, lots.get(newLot1.getPrimaryKey()));
		Assert.assertEquals(newLot2, lots.get(newLot2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		String pk1 = RandomTestUtil.randomString();

		String pk2 = RandomTestUtil.randomString();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Lot> lots = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(lots.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Lot newLot = addLot();

		String pk = RandomTestUtil.randomString();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLot.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Lot> lots = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, lots.size());
		Assert.assertEquals(newLot, lots.get(newLot.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Lot> lots = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(lots.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Lot newLot = addLot();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLot.getPrimaryKey());

		Map<Serializable, Lot> lots = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, lots.size());
		Assert.assertEquals(newLot, lots.get(newLot.getPrimaryKey()));
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Lot newLot = addLot();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Lot.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("code", newLot.getCode()));

		List<Lot> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Lot existingLot = result.get(0);

		Assert.assertEquals(existingLot, newLot);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Lot.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("code",
				RandomTestUtil.randomString()));

		List<Lot> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Lot newLot = addLot();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Lot.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("code"));

		Object newCode = newLot.getCode();

		dynamicQuery.add(RestrictionsFactoryUtil.in("code",
				new Object[] { newCode }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingCode = result.get(0);

		Assert.assertEquals(existingCode, newCode);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Lot.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("code"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("code",
				new Object[] { RandomTestUtil.randomString() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		Lot newLot = addLot();

		_persistence.clearCache();

		Lot existingLot = _persistence.findByPrimaryKey(newLot.getPrimaryKey());

		Assert.assertEquals(Integer.valueOf(existingLot.getLotNumber()),
			ReflectionTestUtil.<Integer>invoke(existingLot,
				"getOriginalLotNumber", new Class<?>[0]));

		Assert.assertEquals(Integer.valueOf(existingLot.getNumber()),
			ReflectionTestUtil.<Integer>invoke(existingLot,
				"getOriginalNumber", new Class<?>[0]));
	}

	protected Lot addLot() throws Exception {
		String pk = RandomTestUtil.randomString();

		Lot lot = _persistence.create(pk);

		lot.setChecksum(RandomTestUtil.randomString());

		lot.setLotNumber(RandomTestUtil.nextInt());

		lot.setNumber(RandomTestUtil.nextInt());

		lot.setPressed(RandomTestUtil.randomBoolean());

		lot.setNbTries(RandomTestUtil.nextInt());

		lot.setLockoutDate(RandomTestUtil.nextDate());

		lot.setParticipantId(RandomTestUtil.nextLong());

		lot.setReservationId(RandomTestUtil.nextLong());

		lot.setPartnerId(RandomTestUtil.nextLong());

		lot.setOrderNumber(RandomTestUtil.randomString());

		_lots.add(_persistence.update(lot));

		return lot;
	}

	private List<Lot> _lots = new ArrayList<Lot>();
	private LotPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}