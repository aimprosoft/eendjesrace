**Local Deployment:**

1) specify `liferay.tomcat.dir` property path;

2) run `mvn clean install`.

3) startup Liferay.

**Deployment to Demo Server:**

1) Install `ssh` and `sshpass`:

    - `ssh` - for connecting to remote SSH server.

        Check if it's installed already:
        
            ssh -V
            
        If not yet - install it:
        
            sudo apt-get install ssh
            
    - `sshpass` - for passing password to SSH connection command.
    
        Check if it's installed already:
        
            sshpass -V
            
        If not yet - install it:
        
            sudo apt-get install sshpass
            
2) Set `user` and `pass` in `deploy-demo.sh` file.

3) Run `./deploy-demo.sh` command.

**Gogo Shell usage:**

*Start Gogo Shell*

`telnet localhost 11311`

*List installed OSGI modules*

`lb` - list of all installed OSGI modules;

`lb | grep be.lions*` - list of custom OSGI modules;

*Start OSGI module*

`start [ID]` - start OSGI module