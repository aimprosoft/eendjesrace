**Phase I - Prepare for migration**

1. Clean LR 5.2.5-ee installation/configuration - 4h 
2. Deploying existing sources on LR 5.2.3 (portlets/hooks/themes/ext) - 16 h
3. Clean LR DXP installation/configuration - 4h

_Subtotal: 24h_

**Phase II - Migration**

1. Ext Environment migration - 20-32h
2. RegistrationPortlet migration - 3 portlets * 12-16 h = 36-48h
3. StartupHook migration - 32-40h
4. Theme migration - 24-32h

_Subtotal: 112-152h_

**Phase III - Testing**

1. Unit Tests migration - 32-40h
2. Testing functionality - 16-24h

_Subtotal: 48-64h_

**Total: 184-240h**