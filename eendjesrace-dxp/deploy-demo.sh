#!/usr/bin/env bash

######## Custom Methods ################
process_command()
{
  if [ $? -eq 0 ]
  then
    echo $1
  else
    echo $2
    exit 1
  fi
}

#Demo server props
server_user=user
server_pass=AqpFRtwEJw
server_host=192.168.215.67
server_db_user=root
server_db_pass=AqpFRtwEJw

portlet_war_path=modules/portlets/registration/target/registration-portlet-0.0.2-SNAPSHOT.war
service_api_path=modules/services/service-api/target/be.lions.duckrace.service.api-0.0.2-SNAPSHOT.jar
service_impl_path=modules/services/service-impl/target/be.lions.duckrace.service.impl-0.0.2-SNAPSHOT.jar
startup_hook_path=modules/hooks/startup-hook/target/startup-hook-0.0.2-SNAPSHOT.war
wcm_dir=modules/hooks/startup-hook/src/main/resources

theme_dir=themes/eendjes-theme
theme_path=themes/eendjes-theme/dist/eendjes-theme.war

demo_lr_path=/home/user/liferay-dxp-digital-enterprise-7.0-sp6
home_lr_path=/home/user/Projects/eendjesrace/eendjesrace-dxp

echo "[$(date -u +%H:%M:%S)] ========================================================================================= "
echo "[$(date -u +%H:%M:%S)] ============================ START DEPLOY TO SERVER SCRIPT ============================== "
echo "[$(date -u +%H:%M:%S)] ========================================================================================= "

#
# Phase 1 - files preparation
#

echo "[$(date -u +%H:%M:%S)] Executing maven build..."
mvn clean install
process_command "[$(date -u +%H:%M:%S)] Maven build finished successfully." "[$(date -u +%H:%M:%S)] Maven build failed."

echo "[$(date -u +%H:%M:%S)] Building eendjes-theme.war..."
cd ${theme_dir}
gulp build
cd ../../
process_command "[$(date -u +%H:%M:%S)] eendjes-theme.war built successfully." "[$(date -u +%H:%M:%S)] Failed to build eendjes-theme.war."

echo "[$(date -u +%H:%M:%S)] Compressing WCM files into wcm.tar.gz..."
cd ${wcm_dir}
rm -Rf wcm.tar.gz
tar czf wcm.tar.gz wcm
process_command "[$(date -u +%H:%M:%S)] wcm.tar.gz created successfully." "[$(date -u +%H:%M:%S)] Failed to create wcm.tar.gz."

#
# Phase 2 - Copying files to server
#
cd ${home_lr_path}
echo "[$(date -u +%H:%M:%S)] COPYING FILE TO DEMO SERVER..."

echo "[$(date -u +%H:%M:%S)] 1. Copying registration-portlet-0.0.2-SNAPSHOT.war..."
sshpass -p ${server_pass} scp -r ${portlet_war_path} ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] registration-portlet-0.0.2-SNAPSHOT.war copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy registration-portlet-0.0.2-SNAPSHOT.war"

echo "[$(date -u +%H:%M:%S)] 2. Copying be.lions.duckrace.service.api-0.0.2-SNAPSHOT.jar..."
sshpass -p ${server_pass} scp -r ${service_api_path} ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] be.lions.duckrace.service.api-0.0.2-SNAPSHOT.jar copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy be.lions.duckrace.service.api-0.0.2-SNAPSHOT.jar"

echo "[$(date -u +%H:%M:%S)] 3. Copying be.lions.duckrace.service.impl-0.0.2-SNAPSHOT.jar..."
sshpass -p ${server_pass} scp -r ${service_impl_path} ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] be.lions.duckrace.service.impl-0.0.2-SNAPSHOT.jar copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy be.lions.duckrace.service.impl-0.0.2-SNAPSHOT.jar"

echo "[$(date -u +%H:%M:%S)] 4. Copying eendjes-theme.war..."
sshpass -p ${server_pass} scp -r ${theme_path} ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] eendjes-theme.war copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy eendjes-theme.war"

echo "[$(date -u +%H:%M:%S)] 5. Copying startup-hook-0.0.2-SNAPSHOT.war..."
sshpass -p ${server_pass} scp -r ${startup_hook_path} ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] startup-hook-0.0.2-SNAPSHOT.war copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy startup-hook-0.0.2-SNAPSHOT.war"

echo "[$(date -u +%H:%M:%S)] 6. Copying portal-setup-wizard.properties..."
sshpass -p ${server_pass} scp -r portal-setup-wizard.properties ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] portal-setup-wizard.properties copied successfully." "[$(date -u +%H:%M:%S)] Failed to copy portal-setup-wizard.properties."

echo "[$(date -u +%H:%M:%S)] 7. Copying wcm folder..."
sshpass -p ${server_pass} scp -r ${wcm_dir}/wcm.tar.gz ${server_user}@${server_host}:/home/user/deploy
process_command "[$(date -u +%H:%M:%S)] wcm.tar.gz copied successfully." "[$(date -u +%H:%M:%S)]         Failed to copy wcm.tar.gz."

echo "[$(date -u +%H:%M:%S)] Files copied to demo server succesfully."

#
# Phase 3 - Installation on server
#

sshpass -p ${server_pass} ssh -T ${server_user}@${server_host} <<   EOTA
    echo "[$(date -u +%H:%M:%S)] Connected to demo server: "
    uname -a

    echo "[$(date -u +%H:%M:%S)] Shutdown demo server..."
    cd ${demo_lr_path}/tomcat-8.0.32/bin
    pwd
    ./shutdown.sh

    echo "[$(date -u +%H:%M:%S)] Cleaning cache..."
    cd ${demo_lr_path}/tomcat-8.0.32/work
    echo "Cleaning dir:"
    pwd
    rm -Rf *

    cd ${demo_lr_path}/tomcat-8.0.32/temp
    echo "Cleaning dir:"
    pwd
    rm -Rf *

    cd ${demo_lr_path}/tomcat-8.0.32/logs
    echo "Cleaning dir:"
    pwd
    rm -Rf *

    cd ${demo_lr_path}
    echo "Removing portal-setup-wizard.properties in dir:"
    pwd
    rm -Rf portal-setup-wizard.properties

    echo "[$(date -u +%H:%M:%S)] Copying files to deploy..."
    cd ${demo_lr_path}/deploy
    pwd
    cp -R /home/user/deploy/registration-portlet-0.0.2-SNAPSHOT.war .
    cp -R /home/user/deploy/be.lions.duckrace.service.api-0.0.2-SNAPSHOT.jar .
    cp -R /home/user/deploy/be.lions.duckrace.service.impl-0.0.2-SNAPSHOT.jar .
    cp -R /home/user/deploy/eendjes-theme.war .
    cp -R /home/user/deploy/startup-hook-0.0.2-SNAPSHOT.war .
    cd ${demo_lr_path}
    pwd
    cp -R /home/user/deploy/portal-setup-wizard.properties .
    cd ${demo_lr_path}/data
    pwd
    cp -R /home/user/deploy/wcm.tar.gz .
    tar xf wcm.tar.gz
    rm -Rf wcm.tar.gz

    echo "[$(date -u +%H:%M:%S)] Starting up Liferay..."
    cd ${demo_lr_path}/tomcat-8.0.32/bin
    pwd
    ./startup.sh
EOTA

echo "[$(date -u +%H:%M:%S)] Disconnected from Demo, current server:"
uname -a

echo "      _      __ _____ _____ ____  __ __  __  "
echo "     / /   /  _/ ____/ ____/ __ \/   \ \/ /  "
echo "    / /    / // /_  / __/ / /_/ / /| |\  /   "
echo "   / /____/ // __/ / /___/ _, _/ ___ |/ /    "
echo "  /_____/___/_/   /_____/_/ |_/_/  |_/_/     "

echo "[$(date -u +%H:%M:%S)] ========================================================================================= "
echo "[$(date -u +%H:%M:%S)] ========================== DEPLOY TO SERVER SCRIPT COMPLETED ============================ "
echo "[$(date -u +%H:%M:%S)] ========================================================================================= "