# Liferay DXP Theme Development

1. Install `Yeoman` and `Gulp`:

![Yeoman+Gulp](images/001.jpg)

2. Install `generator-liferay-theme`:

![generator-liferay-theme](images/002.jpg)

3. Generate theme with Yeoman:

![generate-theme](images/003.jpg)

4. Build theme with Gulp:

![gulp-build](images/004.jpg)

5. Deploy theme with Gulp:

![gulp-deploy](images/005.jpg)