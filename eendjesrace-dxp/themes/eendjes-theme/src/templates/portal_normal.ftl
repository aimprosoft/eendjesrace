<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
    <title>${the_title} - ${company_name}</title>

<#--<meta content="initial-scale=1.0, width=device-width" name="viewport"/>-->

<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div id="wrapper">
    <header id="banner-new" role="banner">
        <div class="theme-container">

            <div id="language-picker">

            <@languages/>
            </div>
            <div id="counter">
                <#assign lotService = serviceLocator.findService("be.lions.duckrace.service.LotLocalService")>
                <#assign lotCount = lotService.getRegisteredLotCount()>
                <#if (lotCount == 0)>
                    <p>Nog geen eendjes geregistreerd</p>
                <#else>
                    <p>Al <span id="count">${lotCount}</span> eendjes geregistreerd</p>
                </#if>
            </div>
            <img src="${images_folder}/header.png"/>

        </div>

    <#if has_navigation && is_setup_complete>
        <#include "${full_templates_path}/navigation.ftl" />
    </#if>
    </header>

    <section id="content">

        <div class="container">
        <#if !is_signed_in>
            <a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">${sign_in_text}</a>
        </#if>


        <#if selectable>
            <@liferay_util["include"] page=content_include />
        <#else>
        ${portletDisplay.recycle()}

        ${portletDisplay.setTitle(the_title)}

            <@liferay_theme["wrap-portlet"] page="portlet.ftl">
                <@liferay_util["include"] page=content_include />
            </@>
        </#if>
        </div>

    </section>
</div>

<footer id="footer" role="contentinfo">
    <div class="container text-center">
        Proudly sponsored by
        <img src="${images_folder}/aca.png" alt="ACA-IT Solutions" height="30" />
        <img src="${images_folder}/liferay.png" alt="Liferay" height="30" />
    </div>
</footer>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<!-- inject:js -->
<!-- endinject -->

</body>

</html>

<#macro languages
default_preferences = ""
>
    <#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "borderless")>

    <@liferay_portlet["runtime"]
    defaultPreferences="${freeMarkerPortletPreferences}"
    portletProviderAction=portletProviderAction.VIEW
    portletProviderClassName="com.liferay.portal.kernel.servlet.taglib.ui.LanguageEntry"
    />
    <#assign VOID = freeMarkerPortletPreferences.reset() />
</#macro>
